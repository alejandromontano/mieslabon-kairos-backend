
install:
	pip install -r requirements/production.txt

install-test:
	pip install -r requirements/test.txt

coverage:
	coverage run --source="." manage.py test ./kairos/$(app)
	
lint:
	@if [ "$(app)" = "" ]; then \
		pylint ./kairos/*/*.py --disable=C0114,R0901,C0115,E1101,R0903; \
	else \
		pylint ./kairos/$(app)/*.py --disable=C0114,R0901,C0115,E1101,R0903; \
	fi

code-checker:
	flake8 kairos/$(app) \
	--exclude .git,__pycache__,"kairos/*/migrations/" \
	--max-line-length 95 \
	--ignore=E128,E124

coverage-test:
	coverage json --fail-under=$(limit)

coverage-report:
	coverage html --skip-covered --skip-empty --precision=2

coverage-report-json:
	coverage json --pretty-print

test: lint code-checker