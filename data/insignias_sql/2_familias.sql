INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Cuidado de la salud humana', '', '', true, 1);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Estética y cuidado de la imagen personal', '', '', true, 1);

INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Ciencias de la vida', '', '', true, 2);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Cuidado de la salud', '', '', true, 2);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Ciencias de la tierra', '', '', true, 2);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Protección del medio ambiente', '', '', true, 2);


INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Educación', '', '', true, 4);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Servicios sociales', '', '', true, 4);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Ciencias sociales y del comportamiento', '', '', true, 4);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Periodismo e información', '', '', true, 4);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Humanidades', '', '', true, 4);

INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Derecho y política', '', '', true, 6);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Comercio y administración', '', '', true, 6);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Economía y finanzas', '', '', true, 6);


INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Matemáticas y estadística', '', '', true, 7);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Informática', '', '', true, 7);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Física y química', '', '', true, 7);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Ingenierías', '', '', true, 7);

INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Bellas artes', '', '', true, 5);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Artes gráficas y audiovisuales', '', '', true, 5);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Artes del espectáculo', '', '', true, 5);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Artesanias', '', '', true, 5);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Gastronomía', '', '', true, 5);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Deporte', '', '', true, 5);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Arquitectura', '', '', true, 5);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Diseño', '', '', true, 5);

INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Protección de las personas y bienes', '', '', true, 3);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Enseñanzas militar', '', '', true, 3);

INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Servicios personales', '', '', true, 8);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Servicio de transporte', '', '', true, 8);
INSERT INTO public.insignias_familia(tipo, nombre, descripcion, imagen, activo, tribu_id) 
VALUES ('familia', 'Servicios en la industria y producción', '', '', true, 8);