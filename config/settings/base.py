"""
Django settings for kairos project.

Generated by 'django-admin startproject' using Django 3.0.2.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

import json
import os
from sys import platform
from django.core.exceptions import ImproperlyConfigured
import environ

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

STATIC_SERVER_DIR = environ.Path(__file__) - 4
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

with open(os.path.join(os.path.dirname(BASE_DIR), "secrets.json")) as f:
    secrets = json.loads(f.read())


def get_secret(setting, secrets=secrets):
    """Get the secret variable or return explicit exception."""
    try:
        return secrets[setting]
    except KeyError:
        error_msg = "Definir la variable de ambiente {0}".format(setting)
        raise ImproperlyConfigured(error_msg)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_secret("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = get_secret("DEBUG")

ALLOWED_HOSTS = ['plataforma.mieslabon.com', '3.134.107.32', 'localhost', '127.0.0.1', '3.236.84.113']

SITE_ID = 1

# From Email credentials
'''DEFAULT_FROM_EMAIL = 'contame@mieslabon.com'
SERVER_EMAIL = 'contame@mieslabon.com'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'contame@mieslabon.com'
EMAIL_HOST_PASSWORD = get_secret("EMAIL_HOST_PASSWORD")
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False'''

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'in-v3.mailjet.com'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'contame@mieslabon.com'
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False
MAILJET_API_KEY = 'fc09d9f837f078781f6c98e07e82783e'
MAIJET_API_SECRECT = '8f3d18c9ee5fa7b7147c660aded67372'

# Facebook Pixel ID
FACEBOOK_PIXEL_CODE_ID = 197406858047141

# Django-cities config
CITIES_LIGHT_TRANSLATION_LANGUAGES = ['es', 'en']
# CITIES_LIGHT_CITY_SOURCES = ['http://download.geonames.org/export/dump/cities1000.zip']
# CITIES_LIGHT_INCLUDE_COUNTRIES = ['CO']
CITIES_LIGHT_INCLUDE_COUNTRIES = ['CO']
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
# Application definition
PAGINATION_SETTINGS = {
    'PAGE_RANGE_DISPLAYED': 1,
    'MARGIN_PAGES_DISPLAYED': 2,
    'SHOW_FIRST_PAGE_WHEN_INVALID': True,
}

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'rest_framework',
    'bootstrap4',
    'tempus_dominus',
    'django_select2',
    'cities_light',
    'crispy_forms',
    'facebook_pixel_code',
    'simple_history',
    'easyaudit',
    'taggit',
    'social_django',
    'ckeditor',
    'django_extensions',
    #'magiclink',

    'kairos.usuarios',
    'kairos.organizaciones',
    'kairos.perfiles_deseados',
    'kairos.experiencias_jovenes',
    'kairos.intereses',
    'kairos.retos',
    'kairos.contenido',
    'kairos.formularios',
    'kairos.tandas_formularios',
    'kairos.extra_tags',
    'kairos.tableros.tablero_organizaciones',
    'kairos.tableros.tablero_jovenes',

    'kairos.insignias',
    'kairos.equipos',
    'kairos.auditorias',
    'kairos.guia',
    'kairos.historiales',
    'kairos.respuestas_formularios',
    'kairos.calificaciones',
    'kairos.ranking',
    'kairos.pqrs',
    'kairos.jurados',
    'kairos.cuartos',
    'kairos.error',
    'kairos.habilidades_del_futuro'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'sesame.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'simple_history.middleware.HistoryRequestMiddleware',
    'easyaudit.middleware.easyaudit.EasyAuditMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(os.path.dirname(BASE_DIR), "kairos", "templates")],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        },
    },
]


WSGI_APPLICATION = 'config.wsgi.application'

# Logging errors
# https://docs.djangoproject.com/en/3.1/topics/logging/

LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'filters': {
		'require_debug_false': {
			'()': 'django.utils.log.RequireDebugFalse',
		},
		'require_debug_true': {
			'()': 'django.utils.log.RequireDebugTrue',
		},
	},
	'formatters': {
		'django.server': {
			'()': 'django.utils.log.ServerFormatter',
			'format': '[%(server_time)s] %(message)s',
		}
	},
	'handlers': {
		'console': {
			'level': 'INFO',
			'filters': ['require_debug_true'],
			'class': 'logging.StreamHandler',
		},

		'console_debug_false': {
			'level': 'ERROR',
			'filters': ['require_debug_false'],
			'class': 'logging.StreamHandler',
		},
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(os.path.dirname(BASE_DIR), 'debug.log'),
        },
		'django.server': {
			'level': 'INFO',
			'class': 'logging.StreamHandler',
			'formatter': 'django.server',
		},
	},
	'loggers': {
		'django': {
			'handlers': ['console', 'console_debug_false', 'file'],
			'level': 'INFO',
		},
		'django.server': {
			'handlers': ['django.server'],
			'level': 'INFO',
			'propagate': False,
		}
	}
}

# CSRF Error
# https://docs.djangoproject.com/en/3.1/ref/settings/&usg=ALkJrhhmEWMUHT5Dv3yS5ipZtvlwNIDKIA#std:setting-CSRF_FAILURE_VIEW

CSRF_FAILURE_VIEW = 'kairos.error.views.csrf_failure'

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': get_secret("DATABASE_DEFAULT")
}

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Customizing Auth
# https://docs.djangoproject.com/en/3.1/topics/auth/customizing/

AUTHENTICATION_BACKENDS = [
    #'magiclink.backends.MagicLinkBackend',
    'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.google.GoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
    'sesame.backends.ModelBackend'
]

# Social Login
# https://python-social-auth.readthedocs.io/en/latest/configuration/django.html

LOGIN_URL = 'inicio'
LOGIN_REDIRECT_URL = 'inicio'
AUTH_USER_MODEL = 'usuarios.Usuario'
SOCIAL_AUTH_USER_MODEL = 'usuarios.Joven'
SOCIAL_AUTH_POSTGRES_JSONFIELD = True
SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['username', 'first_name', 'email']

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_by_email',
    'social_core.pipeline.user.create_user',
    'kairos.usuarios.pipeline.cleanup_social_account',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)

# Google
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = get_secret("GOOGLE_OAUTH2_KEY")
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = get_secret("GOOGLE_OAUTH2_SECRET")

# Facebook
SOCIAL_AUTH_FACEBOOK_KEY = get_secret("FACEBOOK_KEY")
SOCIAL_AUTH_FACEBOOK_SECRET = get_secret("FACEBOOK_SECRET")
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email', 'user_link']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
  'fields': 'id, name, email, picture.type(large), link'
}
SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [
    ('name', 'name'),
    ('email', 'email'),
    ('picture', 'picture'),
    ('link', 'profile_url'),
]

# Magic Link
# https://pypi.org/project/django-sesame/

#SESAME_MAX_AGE = 120 # En segundos
SESAME_ONE_TIME = True
SESAME_TOKEN_NAME = 'url_auth_token'

'''
LOGIN_URL = 'magiclink:login'

MAGICLINK_LOGIN_TEMPLATE_NAME = 'magiclink/login.html'
MAGICLINK_LOGIN_SENT_TEMPLATE_NAME = 'magiclink/login_sent.html'
MAGICLINK_LOGIN_FAILED_TEMPLATE_NAME = 'magiclink/login_failed.html'

# Opcional:
# Si esta configuración se establece en False, se creará una cuenta de usuario la primera
# vez que un usuario solicita un enlace de inicio de sesión.
#MAGICLINK_REQUIRE_SIGNUP = True
#MAGICLINK_SIGNUP_TEMPLATE_NAME = 'magiclink/signup.html'

MAGICLINK_EMAIL_STYLES = {
    'logo_url': 'https://i.imgur.com/3rjTwGH.png',
    'background-colour': '#ffffff',
    'main-text-color': '#000000',
    'button-background-color': '#0078be',
    'button-text-color': '#ffffff',
}

# Cuánto tiempo es válido un enlace mágico antes de devolver un error
MAGICLINK_AUTH_TIMEOUT = 300  # In second - Default is 5 minutes

# La dirección de correo electrónico no distingue entre mayúsculas y minúsculas. Si esta configuración se establece en Verdadero todo
# direcciones de correo electrónico se establecerán en minúsculas antes de que se realicen las comprobaciones
MAGICLINK_IGNORE_EMAIL_CASE = True

# Al crear un usuario, asigne su correo electrónico como nombre de usuario (si el modelo de usuario
# tiene un campo de nombre de usuario)
#MAGICLINK_EMAIL_AS_USERNAME = True

# Anula la longitud predeterminada del enlace mágico
# Advertencia: Anular esta configuración tiene implicaciones de seguridad, los tokens más cortos
# son mucho más susceptibles a los ataques de fuerza bruta *
MAGICLINK_TOKEN_LENGTH  =  50

# Exigir que el correo electrónico del usuario se incluya en el enlace de verificación
# Advertencia: Si se configura como falso, los tokens son más vulnerables a la fuerza bruta
MAGICLINK_VERIFY_INCLUDE_EMAIL  =  True

# Con qué frecuencia un usuario puede solicitar un nuevo token de inicio de sesión (limitación de velocidad básica).
MAGICLINK_LOGIN_REQUEST_TIME_LIMIT  =  30   # En segundos

# Deshabilitar todos los demás tokens para un usuario cuando se solicita un token nuevo
MAGICLINK_ONE_TOKEN_PER_USER  =  False
'''

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'America/Bogota'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_ROOT = str(STATIC_SERVER_DIR('static_collected'))
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(os.path.dirname(BASE_DIR), "kairos", "static"),
]

MEDIA_URL = '/media/'
if platform == 'linux' or platform == 'linux2':
    SERVER_MEDIA_DIR = environ.Path(__file__) - 3
    MEDIA_ROOT = str(SERVER_MEDIA_DIR('media'))
else:
    MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "kairos", "media")

EXCEL_URL = get_secret("EXCEL_URL")
# Select 2
# https://django-select2.readthedocs.io/en/latest/

LIB_VERSION= '4.0.12'
CACHE_BACKEND = 'default'

CACHES = {
    "default": {
        "BACKEND": "redis_cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
    'select2': {
        "BACKEND": "redis_cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/2",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

# Set the cache backend to select2
SELECT2_CACHE_BACKEND = 'select2'

SELECT2_JS = 'plugins/select2/dist/js/select2.min.js'
SELECT2_CSS = 'plugins/select2/dist/css/select2.css'
SELECT2_I18N_PATH = 'plugins/select2/dist/js/i18n'


CACHE_PREFIX = 'select2_'

CITIES_LIGHT_INCLUDE_COUNTRIES = ['PPL', 'PPLA', 'PPLA2', 'PPLA3', 'PPLA4', 'PPLC', 'PPLF', 'PPLG', 'PPLL', 'PPLR', 'PPLS', 'STLMT', ]

MAP_WIDGETS = {
    "GooglePointFieldWidget": (
        ("zoom", 15),
        ("mapCenterLocationName", "colombia"),
        ("GooglePlaceAutocompleteOptions", {'componentRestrictions': {'country': 'co'}}),
        ("markerFitZoom", 12),
    ),
    "GOOGLE_MAP_API_KEY": "AIzaSyAGgspWVCEGTGTKSUTbymXM2Cs2AdV6FEI",
    "LANGUAGE": 'es'
}


