from django.contrib import admin
from django.urls import path, include
from kairos.usuarios.views import inicio
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path("", inicio, name='inicio'),
    path('admin/', admin.site.urls),
    path('social-auth/', include('social_django.urls', namespace="social")),
    #path('auth/', include('magiclink.urls', namespace='magiclink')),
    path('app/usuarios/', include('kairos.usuarios.urls', namespace='usuarios')),
    path('app/organizaciones/', include('kairos.organizaciones.urls', namespace='organizaciones')),
    path('app/perfiles-deseados/', include('kairos.perfiles_deseados.urls', namespace='perfiles_deseados')),
    path('app/intereses/', include('kairos.intereses.urls', namespace='intereses')),
    path('app/retos/', include('kairos.retos.urls', namespace='retos')),
    path('app/contenido/', include('kairos.contenido.urls', namespace='contenido')),
    path('app/insignias/', include('kairos.insignias.urls', namespace='insignias')),
    path('app/cuestionarios/', include('kairos.formularios.urls', namespace='formularios')),
    path('app/calificaciones/', include('kairos.calificaciones.urls', namespace='calificaciones')),
    path('app/tandas/', include('kairos.tandas_formularios.urls', namespace='tandas')),
    path('app/tablero-organizaciones/', include('kairos.tableros.tablero_organizaciones.urls', namespace='tablero_organizaciones')),
    path('app/tablero-jovenes/', include('kairos.tableros.tablero_jovenes.urls', namespace='tablero_jovenes')),
    path('app/tablero-auditorias/', include('kairos.tableros.tablero_auditorias.urls', namespace='tablero_auditorias')),
    path('app/equipos/', include('kairos.equipos.urls', namespace='equipos')),
    path('app/auditorias/', include('kairos.auditorias.urls', namespace='auditorias')),
    path('app/guia/', include('kairos.guia.urls', namespace='guia')),
    path('app/ranking/', include('kairos.ranking.urls', namespace='ranking')),
    path('app/pqrs/', include('kairos.pqrs.urls', namespace='pqrs')),
    path('app/cuartos/', include('kairos.cuartos.urls', namespace='cuartos')),
    path('app/jurados/', include('kairos.jurados.urls', namespace='jurados')),
    path('app/habilidades-del-futuro/', include('kairos.habilidades_del_futuro.urls', namespace='habilidades_del_futuro')),

    path('select2/', include('django_select2.urls')),
] +  static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Handler errors
handler404 = 'kairos.error.views.error_404'
handler500 = 'kairos.error.views.error_500'
