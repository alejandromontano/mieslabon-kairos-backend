#!/bin/sh

#Step 1 : Copy JSON files
for i in data/*.json
do
    cp -a ./data/*.json ./media
done

#Step 2: Load JSON files
for file in media/*
do
    filename=$(echo $file | sed 's/media//g')
    echo -e "\n Loading file "$file
    python manage.py loaddata ./media/$filename
done

