from django.core.management.base import BaseCommand

from kairos.insignias.models import RutaConocimiento

class Command(BaseCommand):
    help = 'Crea las nuevas rutas de conocimiento'

    def handle(self, *args, **kwargs):

        lista_nombres=[
            'Ciencias de la educación, estudia científicamente la educación',
            'Les llama la atención la bibliotecología',
            'Se interesan lo relacionado con los principios y normas que regulan las relaciones humanas',
            'Les llama la atención medir, registrar y analizar toda la parte financiera de una compañía o personas',
            'Les interesa entender la forma como se hacen las ventas de un producto o servicio',
            'Disfrutan aprender sobre la escultura',
            'Buscan conocer sobre la construcción',
            'Reconocen en el diseño de vestuario un área de interés',
            'Les llama la atención aprender sobre el diseño multimedia, incluye texto, fotografías, videos, programación, sonido, animación, manipulada y volcada en un soporte digital',
            'Disfrutan el diseño industrial, crear objetos de uso cotidiano con un sentido estético y funcional',
            'Se interesan por el diseño de espacios',

            'Diagnóstico médico y tratamientos tecnológicos',
            'Enfermería',
            'Medicina alternativa',
            'Cosmetología',
            'Se apasionan por entender el comportamiento animal',
            'Les llama la atención comprender la los formas de administrar y aprovechar los animales considerando su bienestar',
            'Les llama la atención el manejo del medio ambiente',
            'Les interesa todo lo relacionado con las diferentes formas de producción de energía',            
            'Asistencia social, busca atender las necesidades de diferentes poblaciones',
            'Trabajo social',
            'Le interesa la comprensión de la población humana usando la estadística como herramienta',
            'Se encargan de entender la política y los comportamientos asociados',
            'Buscan conocer más sobre los sucesos de la historia',
            'Les llama la atención lo relacionado con la conducta humana y los procesos mentales',            
            'Buscan conocer sobre temas de religión y teología',            
            'Les interesa la Ingeniería civil e ingeniería de materiales',
            'Disfrutan comprender todo lo relacionado con la ingeniería de telecomunicaciones',
            'Les gusta lo relacionado con la ingeniería industrial',            
            'Se enfocan en la fotografía y cinematografía',
            'Les interesa la producción de radio y televisión',
            'Tienen gusto por la impresión y publicación',
            'Les llama la atención la música',
            'Se interesan por conocer más sobre la danza',
            'Buscan conocer más sobre el arte dramática y teatro',
            'Les gusta aprender sobre artesanías',            

            'Les llama la atención la protección de personas y bienes',
            'Se interesan por comprender a profundidad el lenguaje',
            'Les apasionan las dinámicas científicas del planeta tierra',
        ]

        
        for nombre in lista_nombres:
            RutaConocimiento.objects.create(nombre=nombre, activo=True)
           








    
            

