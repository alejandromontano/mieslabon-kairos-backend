from django.core.management.base import BaseCommand

from kairos.insignias.models import RutaConocimiento
from kairos.contenido.models import Contenido
from kairos.perfiles_deseados.models import PerfilesDeseados

class Command(BaseCommand):
    help = 'Crea las nuevas rutas de conocimiento'

    def handle(self, *args, **kwargs):
       
        lista_rutas_viejas = [
            'Ciencias Forenses',
            'Seguridad Privada',
            'Policía (convivencia)',
            'Bomberos',
            'Literatura',
            'Idiomas'   ,
            'Astronomía',
            'Hidrología',
            'Ciencias Atmosféricas'
        ]       
        
        for nombre in lista_rutas_viejas:                
            ruta_nueva = RutaConocimiento.objects.get(nombre=nombre)
            ruta_nueva.delete()
                
        print('Eliminación realizada')
            
               
        