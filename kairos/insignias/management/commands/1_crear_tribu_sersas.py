from django.core.management.base import BaseCommand

from kairos.insignias.models import Tribus

class Command(BaseCommand):
    help = 'Crea la tribu de sersas'

    def handle(self, *args, **kwargs):
        Tribus.objects.create(id=8, tipo='Tribu', nombre='Sersas', descripcion='Tribu Sersas', imagen='insignias/Sersas.png', interes_aptitud='K')        
	

