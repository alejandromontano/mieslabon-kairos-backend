from django.core.management.base import BaseCommand

from kairos.insignias.models import RutaConocimiento

class Command(BaseCommand):
    help = 'Actualiza el nombre de las rutas de conocimiento'

    def handle(self, *args, **kwargs):
        lista_nombre = [{'Medicina':'Medicina tradicional'},
                {'Nutrición':'Nutrición'},
                {'Terapia y Rehabilitación':'Terapia y rehabilitación'},
                {'Farmacología':'Farmacia'},
                {'Odontología':'Servicios dentales'},
                {'Estética y Belleza':'Imagen personal'},
                {'Producción Animal':'Se interesan por comprender la composición química de los seres vivos'},
                {'Biología':'Se encargan de entender los procesos naturales​ de los organismos vivos'},
                {'Veterinaria':'Se interesan por el cuidado de la salud de los animales'},
                {'Geología':'Les llama la atención entender el suelo, las rocas y su dinámicas'},
                {'Agricultura':'Buscan conocer más sobre el manejo del suelo y el cultivo de la tierra'},
                {'Geografía':'Buscan comprender las relaciones entre los seres vivos y su entorno'},
                {'Estudios de la Sociedad':'Buscan comprender la sociedad como un todo'},
                {'Estudios del Ser Humano':'Le llama la atención entender el ser humano'},
                {'Filosofía':'Les interesa conocer sobre temas relacionados con la filosofía'},
                {'Finanza':'Les llama la atención la obtención y administración del dinero y el capital'},
                {'Economía':'Buscan entender los modos en que una sociedad busca satisfacer sus necesidades de consumo'},
                {'Matemáticas':'Les interesan las matemáticas'},
                {'Estadística y Data':'Les gusta usar las matemáticas para comprender una serie resultados que se dan en datos'},
                {'Sistemas':'Les interesa comprender el manejo de la información y su automatización '},
                {'Física y Química':'Les gusta la física, química y afines'},
                {'Energética':'Les llama la atención las ingenierías relacionadas con la química, física y electricidad'},
                {'Tecnologías Aplicadas':'Tienen un gran interés por la ingeniería mecánica y áreas relacionadas'},
                {'Culinaria':'Les interesa todo lo relacionado con la cocina y el manejo de establecimientos gastronómicos'},
                {'Deporte':'Disfrutan la educación física'},
                {'Ciencias Militares':'Tienen un gran interés por las fuerzas aéreas, terrestres y marítimas'},
                {'Educación':'Formación de personal docente'},
                {'Comunicación':'Disfrutan conocer y hacer periodismo y comunicación'},
                {'Ciencias Jurídicas y Políticas':'Les llama la atención las actividades que se asocian con la toma de decisiones en grupo u otras formas de relaciones de poder entre individuos'},
                {'Administración':'Tienen un gusto por el funcionamiento y la administración de las empresas'},
                {'Artes':'Les llama la atención el dibujo'},
                {'Arquitectura':'Les interesa la arquitectura y urbanismo'},
                {'Diseño':'Buscan descubrir sobre el diseño gráfico publicitario, esta relacionada con la forma como se comunica de forma visual una marca'},
                #{'Hacedores':'Les llama la atención todo lo relacionado con el servicio doméstico'},
                #{'Hacedores':'Les gusta lo relacionado con el servicios de belleza'},
                #{'Hacedores':'Tienen un gran interés en la hotelería, restaurantes y sector del entretenimiento'},
                #{'Hacedores':'Les llama la atención el turismo, viajes y ocio'},
                #{'Hacedores':'Tienen gran interés por el transporte fluvial, aéreo, marítimo y terrestre '},
                #{'Hacedores':'Les gusta los temas relacionados con el manejo de maquinarías y procesos para las industrias'},
                #{'Hacedores':'Manejo de maquinarías y procesos para las industrias'}
                ]
               
        for ruta_conocimiento in lista_nombre:
            nombre_anterior = list(ruta_conocimiento.keys())[0]
            ruta = RutaConocimiento.objects.get(nombre=nombre_anterior)
            ruta.nombre = ruta_conocimiento[nombre_anterior]
            ruta.save()
            print(ruta,"-->", ruta_conocimiento[nombre_anterior])

