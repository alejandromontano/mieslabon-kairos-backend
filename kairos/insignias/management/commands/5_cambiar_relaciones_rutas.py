from django.core.management.base import BaseCommand

from kairos.insignias.models import RutaConocimiento
from kairos.contenido.models import Contenido
from kairos.perfiles_deseados.models import PerfilesDeseados

class Command(BaseCommand):
    help = 'Crea las nuevas rutas de conocimiento'

    def handle(self, *args, **kwargs):

       
        lista_rutas_viejas = [
                {'Ciencias forenses':'Les llama la atención la protección de personas y bienes'},
                {'Seguridad privada':'Les llama la atención la protección de personas y bienes'},
                {'Policía (convivencia)':'Les llama la atención la protección de personas y bienes'},
                {'Bomberos':'Les llama la atención la protección de personas y bienes'},

                {'Literatura':'Se interesan por comprender a profundidad el lenguaje'},
                {'Idiomas':'Se interesan por comprender a profundidad el lenguaje'},

                {'Astronomía':'Les apasionan las dinámicas científicas del planeta tierra'},
                {'Hidrología':'Les apasionan las dinámicas científicas del planeta tierra'},
                {'Ciencias Atmosféricas':'Les apasionan las dinámicas científicas del planeta tierra'},
        ]

        print('Inicia cambio en perfiles')
        for perfil in PerfilesDeseados.objects.all():
            for elemento in lista_rutas_viejas:
                nombre_ruta = list(elemento.keys())[0]
                if perfil.rutas_conocimiento.nombre == nombre_ruta:
                    ruta_nueva = RutaConocimiento.objects.get(nombre=elemento[nombre_ruta])
                    perfil.rutas_conocimiento = ruta_nueva
                    perfil.save()
                    print(nombre_ruta, '----->', ruta_nueva)
        print('Perfiles deseados actualizados')
        

        print('Inicia cambio en contenidos')

        for contenido in Contenido.objects.all():
            for elemento in lista_rutas_viejas:
                nombre_ruta = list(elemento.keys())[0]
                #if perfil.rutas_conocimiento.nombre == nombre_ruta:
                
                if contenido.insignias_cuisana.filter(nombre=nombre_ruta):
                    ruta_anterior = contenido.insignias_cuisana.filter(nombre=nombre_ruta)[0]
                    ruta_nueva = RutaConocimiento.objects.get(nombre=elemento[nombre_ruta])
                    contenido.insignias_cuisana.remove(ruta_anterior)
                    contenido.insignias_cuisana.add(ruta_nueva)
                    
                if contenido.insignias_moterra.filter(nombre=nombre_ruta):
                    ruta_anterior = contenido.insignias_moterra.filter(nombre=nombre_ruta)[0]
                    ruta_nueva = RutaConocimiento.objects.get(nombre=elemento[nombre_ruta])
                    contenido.insignias_moterra.remove(ruta_anterior)
                    contenido.insignias_moterra.add(ruta_nueva)

                if contenido.insignias_ponor.filter(nombre=nombre_ruta):
                    ruta_anterior = contenido.insignias_ponor.filter(nombre=nombre_ruta)[0]
                    ruta_nueva = RutaConocimiento.objects.get(nombre=elemento[nombre_ruta])
                    contenido.insignias_ponor.remove(ruta_anterior)
                    contenido.insignias_ponor.add(ruta_nueva)

                if contenido.insignias_vincularem.filter(nombre=nombre_ruta):
                    ruta_anterior = contenido.insignias_vincularem.filter(nombre=nombre_ruta)[0]
                    ruta_nueva = RutaConocimiento.objects.get(nombre=elemento[nombre_ruta])
                    contenido.insignias_vincularem.remove(ruta_anterior)
                    contenido.insignias_vincularem.add(ruta_nueva)
                    
                if contenido.insignias_brianimus.filter(nombre=nombre_ruta):
                    ruta_anterior = contenido.insignias_brianimus.filter(nombre=nombre_ruta)[0]
                    ruta_nueva = RutaConocimiento.objects.get(nombre=elemento[nombre_ruta])
                    contenido.insignias_brianimus.remove(ruta_anterior)
                    contenido.insignias_brianimus.add(ruta_nueva)

                if contenido.insignias_gronor.filter(nombre=nombre_ruta):
                    ruta_anterior = contenido.insignias_gronor.filter(nombre=nombre_ruta)[0]
                    ruta_nueva = RutaConocimiento.objects.get(nombre=elemento[nombre_ruta])
                    contenido.insignias_gronor.remove(ruta_anterior)
                    contenido.insignias_gronor.add(ruta_nueva)

                if contenido.insignias_centeros.filter(nombre=nombre_ruta):
                    ruta_anterior = contenido.insignias_centeros.filter(nombre=nombre_ruta)[0]
                    ruta_nueva = RutaConocimiento.objects.get(nombre=elemento[nombre_ruta])
                    contenido.insignias_centeros.remove(ruta_anterior)
                    contenido.insignias_centeros.add(ruta_nueva)

                if contenido.insignias_sersas.filter(nombre=nombre_ruta):
                    ruta_anterior = contenido.insignias_sersas.filter(nombre=nombre_ruta)[0]
                    ruta_nueva = RutaConocimiento.objects.get(nombre=elemento[nombre_ruta])
                    contenido.insignias_sersas.remove(ruta_anterior)
                    contenido.insignias_sersas.add(ruta_nueva)
                contenido.save()
                    
        print('Cambios realizados')
            
               
        