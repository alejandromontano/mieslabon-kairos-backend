from django.core.management.base import BaseCommand

from kairos.insignias.models import Familia

class Command(BaseCommand):
    help = 'Crea las familias de sersas'

    def handle(self, *args, **kwargs):

        lista_diccionarios = [
            {'pk':1, 'tipo':'Familia', 'nombre':'Cuidado de la salud humana', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':1},
            {'pk':2, 'tipo':'Familia', 'nombre':'Estética y cuidado de la imagen personal', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':1},

            {'pk':3, 'tipo':'Familia', 'nombre':'Ciencias de la vida', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':2},
            {'pk':4, 'tipo':'Familia', 'nombre':'Cuidado de la salud animal', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':2},
            {'pk':5, 'tipo':'Familia', 'nombre':'Ciencias de la tierra', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':2},
            {'pk':6, 'tipo':'Familia', 'nombre':'Protección del medio ambiente', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':2},

            {'pk':7, 'tipo':'Familia', 'nombre':'Educación', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':4},
            {'pk':8, 'tipo':'Familia', 'nombre':'Servicios sociales', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':4},
            {'pk':9, 'tipo':'Familia', 'nombre':'Ciencias sociales y del comportamiento', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':4},
            {'pk':10, 'tipo':'Familia', 'nombre':'Periodismo e información', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':4},
            {'pk':11, 'tipo':'Familia', 'nombre':'Humanidades', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':4},

            {'pk':12, 'tipo':'Familia', 'nombre':'Derecho y política', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':6},
            {'pk':13, 'tipo':'Familia', 'nombre':'Comercio y administración', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':6},
            {'pk':14, 'tipo':'Familia', 'nombre':'Economía y finanzas', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':6},

            {'pk':15, 'tipo':'Familia', 'nombre':'Matemáticas y estadística', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':7},
            {'pk':16, 'tipo':'Familia', 'nombre':'Informática', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':7},
            {'pk':17, 'tipo':'Familia', 'nombre':'Física y química', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':7},
            {'pk':18, 'tipo':'Familia', 'nombre':'Ingenierías', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':7},

            {'pk':19, 'tipo':'Familia', 'nombre':'Bellas artes', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':5},
            {'pk':20, 'tipo':'Familia', 'nombre':'Artes gráficas y audiovisuales', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':5},
            {'pk':21, 'tipo':'Familia', 'nombre':'Artes del espectáculo', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':5},
            {'pk':22, 'tipo':'Familia', 'nombre':'Artesanías', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':5},
            {'pk':23, 'tipo':'Familia', 'nombre':'Gastronomía', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':5},
            {'pk':24, 'tipo':'Familia', 'nombre':'Deporte', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':5},
            {'pk':25, 'tipo':'Familia', 'nombre':'Arquitectura', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':5},
            {'pk':26, 'tipo':'Familia', 'nombre':'Diseño', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':5},

            {'pk':27, 'tipo':'Familia', 'nombre':'Protección de las personas y bienes', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':3},
            {'pk':28, 'tipo':'Familia', 'nombre':'Enseñanzas militar', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':3},

            {'pk':29, 'tipo':'Familia', 'nombre':'Servicios personales', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':8},
            {'pk':30, 'tipo':'Familia', 'nombre':'Servicio de transporte', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':8},
            {'pk':31, 'tipo':'Familia', 'nombre':'Servicios en la industria y producción', 'descripcion':'', 'imagen':'', 'activo':True, 'tribu_id':8},
            
        ]

        for familia in lista_diccionarios:
            Familia.objects.create(**familia)
           

    
            

