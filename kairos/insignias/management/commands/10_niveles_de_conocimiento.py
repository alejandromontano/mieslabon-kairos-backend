from django.core.management.base import BaseCommand

from kairos.insignias.models import NivelesDeConocimiento

class Command(BaseCommand):
    help = 'Actualiza el nombre de las rutas de conocimiento'

    def handle(self, *args, **kwargs):
        dic_aux = {'3':'Oficios','5':'Tecnólogo','6':'Profesional','7':'Experto','8':'Líder de Nicho'}

        niveles_de_conocimiento = NivelesDeConocimiento.objects.all()

        for key, value in dic_aux.items():
            for nivel in niveles_de_conocimiento:
                if value == nivel.nombre:
                    nivel.nombre = key
                    nivel.save()

        print("Niveles cambiados")

        NivelesDeConocimiento.objects.create(id=6, tipo='Nivel de Conocimiento', nombre='1', descripcion='Básica primaria', activo=True, imagen='')
        NivelesDeConocimiento.objects.create(id=7, tipo='Nivel de Conocimiento', nombre='2', descripcion='Básica secundaria', activo=True, imagen='')
        NivelesDeConocimiento.objects.create(id=8, tipo='Nivel de Conocimiento', nombre='4', descripcion='Media técnica', activo=True, imagen='')

        print("Niveles creados")
