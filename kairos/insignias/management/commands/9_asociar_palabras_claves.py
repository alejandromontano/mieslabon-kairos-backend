from django.core.management.base import BaseCommand

from kairos.insignias.models import RutaConocimiento, Familia

class Command(BaseCommand):
    help = 'Actualiza el nombre de las rutas de conocimiento'

    def handle(self, *args, **kwargs):
        lista_nombre = [
                {'Medicina tradicional':['medicina', 'anatomía', 'epidemiología', 'citología', 'tisiología', 'inmunología e inmunohematología', 'patología', 'anestesiología', 'pediatría', 'obstetricia', 'ginecología', 'cirugía', 'neurología', 'psiquiatría', 'radiología', 'oftalmología']},
                {'Diagnóstico médico y tratamientos tecnológicos':['optometria', 'radiologia', 'radioterapia', 'protesis', 'paramedico']},
                {'Nutrición':['Nutrición', 'dietética']},
                {'Enfermería':['enfermería', 'partería', 'enfermero']},
                {'Terapia y rehabilitación':['terapia', 'ocupacional', 'rehabilitación', 'fisiterapia', 'fonoaudialogia']},
                {'Farmacia':['farmacia', 'farmacología', 'boticario', 'farmacéutico']},
                {'Medicina alternativa':['aromaterapia', 'ayurvedica', 'holística', 'homepatia', 'acupuntura']},
                {'Servicios dentales':['odontología', 'higienista', 'dental', 'odontologo']},
                {'Imagen personal':['Imagen personal']},
                {'Cosmetología':['Cosmetología', 'estética']},
                {'Se apasionan por entender el comportamiento animal':['Zoología', 'entomologia', 'ornitología', 'acuariología', 'etología']},
                {'Se interesan por comprender la composición química de los seres vivos':['Toxicología', 'ciencias forensicas', 'genetico', 'virología', 'farmacología', 'biotecnología']},
                {'Se encargan de entender los procesos naturales​ de los organismos vivos':['Biología', 'bacteriología', 'microbiología', 'biofisica', 'botánica']},
                {'Les llama la atención comprender la los formas de administrar y aprovechar los animales considerando su bienestar':['Zootecnia', 'bovinotecnia', 'avitecnia', 'equinotecnia', 'etnología', 'zootecnia especial', 'ganadería', 'avicultura', 'piscicultura', 'porcicultura']},
                {'Se interesan por el cuidado de la salud de los animales':['Veterinaria', 'auxiliar de veterinaria', 'antamía veterinaria', 'fisiología veterinaria', 'embriología', 'farmacología veterinaria', 'legislación veterinaria', 'parasitología', 'microbiología veterinaria', 'virología', 'nutrición animal', 'inmunología veterinaria', 'histología', 'patología']},
                {'Les apasionan las dinámicas científicas del planeta tierra': ['Astronomia', 'fisica', 'ciencia espacial', 'termodinámica', 'geoquímica', 'meteorología', 'climatología', 'hidrología', 'astrofísica', 'biofísica', 'bioquímica']},
                {'Les llama la atención entender el suelo, las rocas y su dinámicas': ['Geología', 'geofísica', 'estratigrafía', 'geomorfología', 'petrografía', 'paleoclimatología', 'paleogeografía', 'petrografía', 'petrología', 'minerología', 'geografía física', 'vulcanología', 'paleoecología', 'ciencia del suelo', 'geodesia', 'ingeniería mineral', 'ingeniería de minas', 'ingeniería en petróleo',  'geotécnica']},
                {'Buscan conocer más sobre el manejo del suelo y el cultivo de la tierra':['Agronomía', 'horticultura', 'selvicultura', 'fruticultura', 'arborcultura', 'agroclimatología', 'fitotecnia y agroindustrial', 'floricultura', 'olericultura', 'agricultura']},
                {'Buscan comprender las relaciones entre los seres vivos y su entorno':['Ecología', 'oceanografía', 'biogeografía', 'geografía']},
                {'Les llama la atención el manejo del medio ambiente': ['Ciencias ambientales', 'desarollo sostenible', 'ingeniería ambiental', 'gestión ambiental', 'ingeniería forestal', 'silvicultura']},
                {'Les interesa todo lo relacionado con las diferentes formas de producción de energía': ['Ingeniería en energías renovables']},
                {'Formación de personal docente':['educación preescolar', 'jardines de infancia', 'escuelas elementales', 'asignaturas profesionales', 'prácticas y no profesionales', 'educación de adultos', 'formación de personal docente', 'formación de maestros de niños minusválidos', 'Programas generales y especializados de formación de personal docente']},
                {'Ciencias de la educación, estudia científicamente la educación': ['elaboración programas de estudio','evaluación de conocimientos',' pruebas y mediciones', 'investigaciones sobre educación', 'ciencias de la educación']},
                {'Asistencia social, busca atender las necesidades de diferentes poblaciones':['asistencia personas con discapacidad', 'asistencia a la infancia', 'servicios para jóvenes', 'servicios de gerontología', 'atención personas mayores', 'atención a la mujer', 'atención a inmigrantes y refugiados']},
                {'Trabajo social': ['orientación', 'asistencia social']},
                {'Le llama la atención entender el ser humano': ['Antropología', 'etnología', 'Antropología biológica', 'Antropología cultural',  'Antropología lingüística', 'Arqueología', 'antropología social', 'antropología organizacional']},
                {'Le interesa la comprensión de la población humana usando la estadística como herramienta': ['Demografía', 'Demografía estática', 'Demografía dinámica', 'Demografía descriptiva', 'Demografía teórica', 'Demografía cuantitativa', 'Demografía económica', 'Demografía social']},
                {'Se encargan de entender la política y los comportamientos asociados': ['civil', 'politica', 'derechos humanos', 'políticas públicas' ]},
                {'Buscan conocer más sobre los sucesos de la historia': ['Historia', 'futurología', 'arqueología', 'paleontología', 'historiografía', 'historiología', 'Egiptología', 'Ernoarqueología', 'Biocronología', 'Paleobiología', 'Tafonomía']},
                {'Se interesan por comprender a profundidad el lenguaje': ['linguistica', 'literatura', 'Fonética', 'Fonología', 'Morfología', 'Sintaxis', 'Semántica', 'Lexicología', 'pragmática', 'lexicografía']},
                {'Les llama la atención lo relacionado con la conducta humana y los procesos mentales': ['psicología', 'psicoanalisis', 'psicoterapia', 'Psicología clínica', 'Psicología cognitiva', 'Psicología del desarrollo', 'Psicología de la educación', 'psicología organizacional', 'Psicología de las organizaciones', 'Psicología del marketing', 'psicología del consumidor', 'Sexología', 'Neuropsicología', 'Psicología del deporte', 'psicología social', 'Psicología de pareja', 'psicologia familiar']},
                {'Disfrutan conocer y hacer periodismo y comunicación': ['Periodismo', 'comunicación', 'medios de comunicación', 'edición', 'comunicación audiovisual', 'comunicación social', 'semiótica']},
                {'Les llama la atención la bibliotecología': ['bibliotecas', 'archivista', 'museología']},
                {'Les interesa conocer sobre temas relacionados con la filosofía': ['Filosofía', 'ética', 'moral', 'lógica', 'Filosofía de la ciencia', 'Ontología']},
                {'Buscan conocer sobre temas de religión y teología': ['religión', 'teología', 'sagrado']},
                {'Se interesan lo relacionado con los principios y normas que regulan las relaciones humanas': ['Magistrados locales', 'notarios', 'derecho (general, internacional, laboral, marítimo, etc.)', 'jurisprudencia', 'historia del derecho']},
                {'Les llama la atención las actividades que se asocian con la toma de decisiones en grupo u otras formas de relaciones de poder entre individuos': ['Ciencias políticas', 'filosofía política', 'politicas públicas', 'relaciones internacionales']},
                {'Les llama la atención medir, registrar y analizar toda la parte financiera de una compañía o personas': ['contaduria', 'auditoria', 'impuestos', 'costos', 'revisor fiscal', 'asesor gerencial', 'asesor tributario', 'contaduría pública']},
                {'Les interesa entender la forma como se hacen las ventas de un producto o servicio': ['Mercadeo', 'publicidad', 'consumidor', 'relaciones', 'marketing', 'promoción', 'ventas', 'administración de marca', 'comercio']},
                {'Tienen un gusto por el funcionamiento y la administración de las empresas': ['administración', 'emprendimiento', 'administración pública']},
                {'Les llama la atención la obtención y administración del dinero y el capital': ['aseguradores', 'inversión', 'finanzas', 'banca', 'administración Financiera' , 'dirección Financiera', 'Economía Financiera', 'auditoría financiera', 'finanzas públicas', 'finanzas corporativas']},
                {'Buscan entender los modos en que una sociedad busca satisfacer sus necesidades de consumo': ['Economia', 'econometria', 'Economía internacional', 'economía financiera', 'economía ecológica', 'Microeconomía', 'Macroeconomía', 'economía agraria', 'economía ambiental', 'economía normativa']},
                {'Les interesan las matemáticas': ['algebra', 'geometria', 'matemáticas', 'operaciones', 'numeros', 'matemáticas aplicada', 'matemática pura', 'aritmética', 'topología']},
                {'Les gusta usar las matemáticas para comprender una serie resultados que se dan en datos': ['Probabilidad', 'muestreo', 'estadística', 'estadísitca descriptiva', 'estadística inferencial', 'Estadística matemática']},
                {'Les interesa comprender el manejo de la información y su automatización ': ['Concepción de sistemas', 'programación informática', 'procesamiento de datos', 'redes', 'sistemas operativos - elaboración de programas informáticos', 'programación de software', 'tecnología de la información', 'cibernética',  'robótica', 'computación',  'ofimática', 'telemática', 'diseño web', 'desarrollo en realidad virtual', 'inteligencia artificial', 'machine learning software', ]},
                {'Les gusta la física, química y afines': ['Física', 'química', 'Mecánica cuántica', 'acústica', 'cinemática', 'Termodinámica', 'física nuclear', 'ingeniería química', 'ingeniería de sonido']},
                {'Les llama la atención las ingenierías relacionadas con la química, física y electricidad': ['Ingeniería energética', 'ingeniería química', 'ingeniería eléctrica', 'electrónica']},
                {'Les interesa la Ingeniería civil e ingeniería de materiales': ['ingeniería civil', 'Metalistería', 'ingeniería metalúrgica', 'ingeniería cerámica', 'ingeniería de materiales', 'ingeniería de construcción']},
                {'Disfrutan comprender todo lo relacionado con la ingeniería de telecomunicaciones': ['ingeniería de telecomunicaciones']},
                {'Tienen un gran interés por la ingeniería mecánica y áreas relacionadas': ['mantenimiento de vehículos', 'mecánica cuántica', 'mecánica de fluidos', 'ingeniería mecánica']},
                {'Les gusta lo relacionado con la ingeniería industrial': ['ingeniería industrial']},
                {'Les llama la atención el dibujo': ['Dibujo', 'pintura', 'caligrafía', 'animación']},
                {'Disfrutan aprender sobre la escultura': ['Escultura']},
                {'Se enfocan en la fotografía y cinematografía': ['fotografía', 'cinematografía', 'guión cinematográfico', 'guión', 'dirección', 'realización en cine y video', 'montajes']},
                {'Les interesa la producción de radio y televisión': ['producción de radio y televisión', 'producción creativa digital', 'Producción de radio', 'producción televisiva', 'producción audiovisual', 'montajes', 'ilustración', 'ilustración digital', 'realización de televisión', 'locución de radio y televisión']},
                {'Tienen gusto por la impresión y publicación': ['impresión y publicación']},
                {'Les llama la atención la música': ['Producción de música y sonido', 'producción musical', 'dj', 'sonido para audiovisuales y espectáculos', 'producción de música electrónica', 'ingeniería acústica', 'composición musical', 'interpretación', 'musicología', 'instrumentos de viento', 'instrumentos de cuerda', 'instrumentos de percusión', 'instrumentos eléctricos', 'arte dramático']},
                {'Se interesan por conocer más sobre la danza': ['danza contemporánea', 'danza', 'coreógrafo', 'bailarín', 'expresión corporal', 'ballet', 'danza clásica', 'teatro musical', 'danza moderna', 'danza jazz', 'danza oriental', 'danza folclórica']},
                {'Buscan conocer más sobre el arte dramática y teatro': ['actuación', 'dramaturgo', 'director escénico', 'arte dramático']},
                {'Les gusta aprender sobre artesanías': ['Piel y cuero', 'textil', 'joyería', 'orfebrería', 'telas', 'telas pintadas', 'bordados', 'cerámica', 'alfarero', 'artesanía']},
                {'Les interesa todo lo relacionado con la cocina y el manejo de establecimientos gastronómicos': ['Manejo de establecimientos gastronómicos', 'cocina', 'pastelería', 'repostería', 'panadería', 'Bebidas (vinos, café, entre otros)', 'gastronomía internacional', 'chef', 'cocinero']},
                {'Disfrutan la educación física': ['Educación física', 'recreación y deportes']},
                {'Buscan conocer sobre la construcción': ['albañil', 'edificios', 'carpintería', 'ebanisteria', 'fontanería', 'hidraulico', 'construcción']},
                {'Les interesa la arquitectura y urbanismo': ['arquitectura estructural', 'arquitectura paisajística', 'planificación comunitaria', 'cartografía', 'edificación']},
                {'Reconocen en el diseño de vestuario un área de interés': ['Diseño de vestuario', 'diseño de moda']},
                {'Les llama la atención aprender sobre el diseño multimedia, incluye texto, fotografías, videos, programación, sonido, animación, manipulada y volcada en un soporte digital': ['diseño de videojuegos', 'diseño de multimedia', 'diseño web', 'diseño programas interactivos', 'usabilidad']},
                {'Buscan descubrir sobre el diseño gráfico publicitario, esta relacionada con la forma como se comunica de forma visual una marca': ['diseño tipográfico', 'diseño gráfico', 'diseño tipográfico', 'diseño gráfico publicitario']},
                {'Disfrutan el diseño industrial, crear objetos de uso cotidiano con un sentido estético y funcional': ['diseño industrial']},
                {'Se interesan por el diseño de espacios': ['diseño de interiores', 'diseño de escenarios']},
                {'Les llama la atención la protección de personas y bienes': ['servicios de policía y orden público', 'criminología', 'prevención y extinción de incendios', 'seguridad civil', 'bomberos', 'guardas', 'guardaparques', 'seguridad privada']},
                {'Tienen un gran interés por las fuerzas aéreas, terrestres y marítimas': ['Formación de marinos', 'oficiales de marina', 'náutica', 'fuerzas aéreas', 'ejército', 'ingeniería militar']},
                {'Les llama la atención todo lo relacionado con el servicio doméstico': ['lavandería y tintorería', 'ciencias del hogar', 'servicios domésticos']},
                {'Les gusta lo relacionado con el servicios de belleza': ['Maquillador', 'Peluquería', 'Maquillaje', 'shopper', 'barbero', 'pedicurista', 'manicurista', 'masajistas']},
                {'Tienen un gran interés en la hotelería, restaurantes y sector del entretenimiento': ['Hotelería', 'restaurantes', 'viajes', 'turismo',  'Alimentación' , 'bebidas', 'wedding planner', 'catering', 'receptcionista', 'asistente del servicio de habitaciones', 'mozo', 'ayudante de cocina', 'camarero', 'sommelier', 'crupier', 'bartender']},
                {'Les llama la atención el turismo, viajes y ocio': ['administración turística', 'administración de empresas turísticas', 'azafata', 'guía turístico', 'marketing turístico', 'acompañante turístico', 'asistente de agencias de viajes']},
                {'Tienen gran interés por el transporte fluvial, aéreo, marítimo y terrestre': ['Transporte fluvial', 'tripulación de aviones', 'control del tráfico aéreo', 'transporte ferroviario', 'transporte por carretera', 'servicios postales', 'transporte aéreo', 'aduanas', 'transporte marítimo']},
                {'Les gusta los temas relacionados con el manejo de maquinarías y procesos para las industrias': ['operarios de producción', 'jefe de producción', 'vendedor', 'supervisor', 'auxiliar', 'ayudante', 'confección', 'minero', 'manejo maquinaria', 'operario']},
                {'Manejo de maquinarías y procesos para las industrias': ['operarios de producción', 'jefe de producción', 'vendedor', 'supervisor', 'auxiliar', 'ayudante', 'confección', 'minero', 'manejo maquinaria', 'operario']},
                {'Buscan comprender la sociedad como un todo': ['Criminología,', 'Cultura', 'Estudio de género', 'Sociología', 'Sociología cultural', 'Sociología de la educación', 'Sociología de la literatura', 
                    'Sociología de la desviación', 'Sociología del arte', 'Sociología del derecho', 'Ciencia', 'Tecnología y sociedad', 'Dinámica social', 'Sociología económica', 'Sociología de la religión',
                    'Sociología de la comunicación', 'Sociologóa del lenguaje', 'Sociología del trabajo', 'Sociología política', 'Sociología pragmática', 'Sociología rural', 'Sociologia urbana', 'Cibersociología']},
            ]

        for ruta_conocimiento in lista_nombre:
            nombre = list(ruta_conocimiento.keys())[0]
            ruta = RutaConocimiento.objects.get(nombre=nombre)
            ruta.palabras_clave = []
            ruta.save()            
            for palabra_clave in ruta_conocimiento[nombre]:
                ruta.palabras_clave.append(palabra_clave.strip().capitalize())
                ruta.save()
                

