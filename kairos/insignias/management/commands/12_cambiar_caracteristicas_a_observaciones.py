from django.core.management.base import BaseCommand

from kairos.contenido.models import Contenido

class Command(BaseCommand):
    help = 'Actualiza el nombre de las rutas de conocimiento'

    def handle(self, *args, **kwargs):
        contenidos = Contenido.objects.all()

        for contenido in contenidos:
            caracteristicas = contenido.caracteristicas
            contenido.observaciones += '\n \n' + caracteristicas
            contenido.save()


