from django.core.management.base import BaseCommand

from kairos.contenido.models import Contenido

class Command(BaseCommand):
    help = 'Actualiza el nombre de las rutas de conocimiento'

    def handle(self, *args, **kwargs):
        contenidos = Contenido.objects.filter(tipo='ruta')

        for contenido in contenidos:
            if len(contenido.niveles_conocimiento.all()) > 1:
                for nivel in contenido.niveles_conocimiento.all():
                    if nivel != '6':
                        contenido.niveles_conocimiento.remove(nivel)

