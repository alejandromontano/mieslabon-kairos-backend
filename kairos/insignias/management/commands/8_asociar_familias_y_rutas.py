from django.core.management.base import BaseCommand

from kairos.insignias.models import RutaConocimiento, Familia

class Command(BaseCommand):
    help = 'Actualiza el nombre de las rutas de conocimiento'

    def handle(self, *args, **kwargs):
        lista_nombre = [
                # {ruta:familia}
                {'Medicina tradicional':'Cuidado de la salud humana '},
                {'Diagnóstico médico y tratamientos tecnológicos':'Cuidado de la salud humana '},
                {'Nutrición':'Cuidado de la salud humana '},
                {'Enfermería':'Cuidado de la salud humana '},
                {'Terapia y rehabilitación':'Cuidado de la salud humana '},
                {'Farmacia':'Cuidado de la salud humana '},
                {'Medicina alternativa':'Cuidado de la salud humana '},
                {'Servicios dentales':'Cuidado de la salud humana '},
                {'Imagen personal':'Estética y cuidado de la imagen personal'},
                {'Cosmetología':'Estética y cuidado de la imagen personal'},
                {'Se apasionan por entender el comportamiento animal':'Ciencias de la vida'},
                {'Se interesan por comprender la composición química de los seres vivos':'Ciencias de la vida'},
                {'Se encargan de entender los procesos naturales​ de los organismos vivos':'Ciencias de la vida'},
                {'Les llama la atención comprender la los formas de administrar y aprovechar los animales considerando su bienestar':'Cuidado de la salud animal'},
                {'Se interesan por el cuidado de la salud de los animales':'Cuidado de la salud animal'},
                {'Les apasionan las dinámicas científicas del planeta tierra':'ciencias de la tierra'},
                {'Les llama la atención entender el suelo, las rocas y su dinámicas':'ciencias de la tierra'},
                {'Buscan conocer más sobre el manejo del suelo y el cultivo de la tierra':'ciencias de la tierra'},
                {'Buscan comprender las relaciones entre los seres vivos y su entorno':'Protección del medio ambiente'},
                {'Les llama la atención el manejo del medio ambiente':'Protección del medio ambiente'},
                {'Les interesa todo lo relacionado con las diferentes formas de producción de energía':'Protección del medio ambiente'},
                {'Formación de personal docente':'Educación'},
                {'Ciencias de la educación, estudia científicamente la educación':'Educación'},
                {'Asistencia social, busca atender las necesidades de diferentes poblaciones':'Servicios sociales'},
                {'Trabajo social':'Servicios sociales'},
                {'Buscan comprender la sociedad como un todo':'Ciencias sociales y del comportamiento'},
                {'Le llama la atención entender el ser humano':'Ciencias sociales y del comportamiento'},
                {'Le interesa la comprensión de la población humana usando la estadística como herramienta':'Ciencias sociales y del comportamiento'},
                {'Se encargan de entender la política y los comportamientos asociados':'Ciencias sociales y del comportamiento'},
                {'Buscan conocer más sobre los sucesos de la historia':'Ciencias sociales y del comportamiento'},
                {'Se interesan por comprender a profundidad el lenguaje':'Ciencias sociales y del comportamiento'},
                {'Les llama la atención lo relacionado con la conducta humana y los procesos mentales':'Ciencias sociales y del comportamiento'},
                {'Disfrutan conocer y hacer periodismo y comunicación':'Periodismo e información'},
                {'Les llama la atención la bibliotecología':'Periodismo e información'},
                {'Les interesa conocer sobre temas relacionados con la filosofía':'Humanidades '},
                {'Buscan conocer sobre temas de religión y teología':'Humanidades '},
                {'Se interesan lo relacionado con los principios y normas que regulan las relaciones humanas':'Derecho y política'},
                {'Les llama la atención las actividades que se asocian con la toma de decisiones en grupo u otras formas de relaciones de poder entre individuos':'Derecho y política'},
                {'Les llama la atención medir, registrar y analizar toda la parte financiera de una compañía o personas':'Comercio y administración'},
                {'Les interesa entender la forma como se hacen las ventas de un producto o servicio':'Comercio y administración'},
                {'Tienen un gusto por el funcionamiento y la administración de las empresas':'Comercio y administración'},
                {'Les llama la atención la obtención y administración del dinero y el capital':'Economía y finanzas'},
                {'Buscan entender los modos en que una sociedad busca satisfacer sus necesidades de consumo':'Economía y finanzas'},
                {'Les interesan las matemáticas':'Matemáticas y estadística'},
                {'Les gusta usar las matemáticas para comprender una serie resultados que se dan en datos':'Matemáticas y estadística'},
                {'Les interesa comprender el manejo de la información y su automatización ':'Informática'},
                {'Les gusta la física, química y afines':'Física y química'},
                {'Les llama la atención las ingenierías relacionadas con la química, física y electricidad':'Ingenierías'},
                {'Les interesa la Ingeniería civil e ingeniería de materiales':'Ingenierías'},
                {'Disfrutan comprender todo lo relacionado con la ingeniería de telecomunicaciones':'Ingenierías'},
                {'Tienen un gran interés por la ingeniería mecánica y áreas relacionadas':'Ingenierías'},
                {'Les gusta lo relacionado con la ingeniería industrial':'Ingenierías'},
                {'Les llama la atención el dibujo':'Bellas artes'},
                {'Disfrutan aprender sobre la escultura':'Bellas artes'},
                {'Se enfocan en la fotografía y cinematografía':'Artes gráficas y audiovisuales'},
                {'Les interesa la producción de radio y televisión':'Artes gráficas y audiovisuales'},
                {'Tienen gusto por la impresión y publicación':'Artes gráficas y audiovisuales'},
                {'Les llama la atención la música':'Artes del espectáculo'},
                {'Se interesan por conocer más sobre la danza':'Artes del espectáculo'},
                {'Buscan conocer más sobre el arte dramática y teatro':'Artes del espectáculo'},
                {'Les gusta aprender sobre artesanías':'Artesanías'},
                {'Les interesa todo lo relacionado con la cocina y el manejo de establecimientos gastronómicos':'Gastronomía'},
                {'Disfrutan la educación física':'Deporte'},
                {'Buscan conocer sobre la construcción':'Arquitectura'},
                {'Les interesa la arquitectura y urbanismo':'Arquitectura'},
                {'Reconocen en el diseño de vestuario un área de interés':'Diseño'},
                {'Les llama la atención aprender sobre el diseño multimedia, incluye texto, fotografías, videos, programación, sonido, animación, manipulada y volcada en un soporte digital':'Diseño'},
                {'Buscan descubrir sobre el diseño gráfico publicitario, esta relacionada con la forma como se comunica de forma visual una marca':'Diseño'},
                {'Disfrutan el diseño industrial, crear objetos de uso cotidiano con un sentido estético y funcional':'Diseño'},
                {'Se interesan por el diseño de espacios':'Diseño'},
                {'Les llama la atención la protección de personas y bienes':'Protección de las personas y bienes'},
                {'Tienen un gran interés por las fuerzas aéreas, terrestres y marítimas':'Enseñanzas militar'},
                {'Les llama la atención todo lo relacionado con el servicio doméstico':'Servicios personales'},
                {'Les gusta lo relacionado con el servicios de belleza':'Servicios personales'},
                {'Tienen un gran interés en la hotelería, restaurantes y sector del entretenimiento':'Servicios personales'},
                {'Les llama la atención el turismo, viajes y ocio':'Servicios personales'},
                {'Tienen gran interés por el transporte fluvial, aéreo, marítimo y terrestre':'Servicio de transporte'},
                {'Les gusta los temas relacionados con el manejo de maquinarías y procesos para las industrias':'Servicios en la industria y producción'},
                {'Manejo de maquinarías y procesos para las industrias':'Servicios en la industria y producción'},

            ]
               
        for ruta_conocimiento in lista_nombre:
            nombre = list(ruta_conocimiento.keys())[0]
            ruta = RutaConocimiento.objects.get(nombre=nombre)  
            familia = Familia.objects.get(nombre=ruta_conocimiento[nombre].strip().capitalize())  
            
            ruta.familia = familia
            ruta.save()
            

