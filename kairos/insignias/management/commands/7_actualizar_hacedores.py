from django.core.management.base import BaseCommand

from kairos.insignias.models import RutaConocimiento

class Command(BaseCommand):
    help = 'Actualiza el nombre de las rutas de conocimiento'

    def handle(self, *args, **kwargs):        
        
        vincularem = RutaConocimiento.objects.get(pk=29)
        vincularem.nombre="Les llama la atención todo lo relacionado con el servicio doméstico"
        vincularem.save()

        Brianimus = RutaConocimiento.objects.get(pk=36)
        Brianimus.nombre="Tienen un gran interés en la hotelería, restaurantes y sector del entretenimiento"
        Brianimus.save()

        Moterra = RutaConocimiento.objects.get(pk=16)
        Moterra.nombre="Les llama la atención el turismo, viajes y ocio"
        Moterra.save()

        Ponor = RutaConocimiento.objects.get(pk=22)
        Ponor.nombre="Tienen gran interés por el transporte fluvial, aéreo, marítimo y terrestre"
        Ponor.save()        

        Centeros = RutaConocimiento.objects.get(pk=49)
        Centeros.nombre="Les gusta los temas relacionados con el manejo de maquinarías y procesos para las industrias"
        Centeros.save() 

        Gronor = RutaConocimiento.objects.get(pk=41)
        Gronor.nombre="Manejo de maquinarías y procesos para las industrias"
        Gronor.save()

       
        Cuisana = RutaConocimiento.objects.get(pk=8)
        Cuisana.nombre="Les gusta lo relacionado con el servicios de belleza"
        Cuisana.save()
        

       