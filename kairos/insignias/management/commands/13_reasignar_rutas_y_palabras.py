from django.core.management.base import BaseCommand

from kairos.contenido.models import Contenido
from kairos.core.utils import obtener_listado_de_palabras_limpias
from kairos.insignias.models import RutaConocimiento, Tribus

class Command(BaseCommand):
    help = 'Actualiza el nombre de las rutas de conocimiento'

    def handle(self, *args, **kwargs):
        contenidos = Contenido.objects.filter()

        for contenido in contenidos:
            nombre = contenido.nombre
            if nombre:
                lower_nombre = nombre.lower()
                palabra_capitalize = lower_nombre.capitalize()
                palabras_claves = palabra_capitalize.split(' ')
                palabras_limpias = obtener_listado_de_palabras_limpias(palabras_claves)
                RutaConocimiento.asignar_tribus_a_contenido_por_palabras_clave(palabras_limpias, contenido)
                mis_tribus = contenido.tribus()
                for mi_tribu in mis_tribus:
                    tribu = Tribus.objects.get(nombre__icontains=mi_tribu)
                    contenido.tribus_relacionadas.add(tribu)
                contenido.save()



