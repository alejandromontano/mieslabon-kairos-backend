# Modulos Django
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.base import TemplateView

# Modulos de plugin externos

# Modulos de otras apps
from kairos.core.mixins import MensajeMixin
from kairos.guia.models import VerRecurso, Recurso

# Modulos internos de la app
from .forms import *
from .models import *

class TribusListado(LoginRequiredMixin, ListView):
    model = Tribus
    context_object_name = "tribus"
    template_name = "insignias/tribus/listado.html"

    def get_queryset(self):
        tribus = Tribus.objects.all()
        return tribus


class TribusConsultar(LoginRequiredMixin, ListView):
    model = Tribus
    context_object_name = "tribus"
    template_name = "insignias/tribus/consultar.html"

    def get_queryset(self):
        tribus = Tribus.objects.all()
        return tribus


class TribuRegistrar(LoginRequiredMixin, MensajeMixin, CreateView):
    model = Tribus
    form_class = TribusModelForm
    template_name = 'insignias/tribus/registrar.html'
    success_url = reverse_lazy('insignias:registrar_tribu')
    mensaje_exito = "Tribu registrada correctamente!"
    mensaje_error = "Ha ocurrido un error al registrar la tribu. Por favor verifique los datos ingresados."

    '''def form_valid(self, form):
        context = self.get_context_data()
        rutas_conocimiento = context['rutas_conocimiento_tribu']
        with transaction.atomic():
            self.object = form.save()
            if rutas_conocimiento.is_valid():
                rutas_conocimiento.instance = self.object
                rutas_conocimiento.save()
        form.instance.tipo = 'Tribu'
        return super(TribuRegistrar, self).form_valid(form)'''


class TribuModificar(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = Tribus
    form_class = TribusModelForm
    pk_url_kwarg = 'id_tribu'
    template_name = 'insignias/tribus/modificar.html'
    success_url = reverse_lazy('insignias:listado_tribus')
    mensaje_exito = "Tribu modificada correctamente!"
    mensaje_error = "Ha ocurrido un error al modificar la tribu. Por favor verifique los datos ingresados."

   
    '''def form_valid(self, form):
        context = self.get_context_data()
        rutas_conocimiento = context['rutas_conocimiento_tribu']
        with transaction.atomic():
            self.object = form.save()
            if rutas_conocimiento.is_valid():
                rutas_conocimiento.instance = self.object
                rutas_conocimiento.save()
        form.instance.tipo = 'Tribu'
        return super(TribuModificar, self).form_valid(form)'''


class FamiliaModificar(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = Familia
    form_class = FamiliaForm
    pk_url_kwarg = 'id_familia'
    template_name = 'insignias/familia/modificar.html'
    success_url = reverse_lazy('insignias:listado_familias')
    mensaje_exito = "Familia modificada correctamente!"
    mensaje_error = "Ha ocurrido un error al modificar la Familia. Por favor verifique los datos ingresados."

      
class FamiliasListado(LoginRequiredMixin, ListView):
    model = Familia
    context_object_name = "familias"
    template_name = "insignias/familia/listado.html"

    def get_queryset(self):
        familias = Familia.objects.all()
        return familias

class FamiliaRegistrar(LoginRequiredMixin, MensajeMixin, CreateView):
    model = Familia
    form_class = FamiliaForm
    template_name = 'insignias/familia/registrar.html'
    success_url = reverse_lazy('insignias:registrar_familia')
    mensaje_exito = "Familia registrada correctamente!"
    mensaje_error = "Ha ocurrido un error al registrar la familia. Por favor verifique los datos ingresados."


class CamposListado(LoginRequiredMixin, ListView):
    model = RutaConocimiento
    context_object_name = "campos"
    template_name = "insignias/campos/listado.html"

    def get_queryset(self):
        campos = RutaConocimiento.objects.all()
        return campos

class CampoRegistrar(LoginRequiredMixin, MensajeMixin, CreateView):
    model = RutaConocimiento
    form_class = RutaConocimientoModelForm
    template_name = 'insignias/campos/registrar.html'
    success_url = reverse_lazy('insignias:registrar_campo')
    mensaje_exito = "Campo registrada correctamente!"
    mensaje_error = "Ha ocurrido un error al registrar el Campo. Por favor verifique los datos ingresados."

class CampoModificar(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = RutaConocimiento
    form_class = RutaConocimientoModelForm
    pk_url_kwarg = 'id_campo'
    template_name = 'insignias/campos/modificar.html'
    success_url = reverse_lazy('insignias:listado_campos')
    mensaje_exito = "Campo modificada correctamente!"
    mensaje_error = "Ha ocurrido un error al modificar el Campo. Por favor verifique los datos ingresados."
  
class NivelesDeConocimientoListado(LoginRequiredMixin, ListView):
    model = NivelesDeConocimiento
    context_object_name = "niveles"
    template_name = "insignias/niveles_conocimiento/listado.html"

    def get_queryset(self):
        niveles_conocimiento = NivelesDeConocimiento.objects.all()
        return niveles_conocimiento


class NivelesDeConocimientoConsultar(LoginRequiredMixin, ListView):
    model = NivelesDeConocimiento
    context_object_name = "niveles"
    template_name = "insignias/niveles_conocimiento/consultar.html"

    def get_queryset(self):
        niveles_conocimiento = NivelesDeConocimiento.objects.all()
        return niveles_conocimiento


class NivelesDeConocimientoRegistrar(LoginRequiredMixin, MensajeMixin, CreateView):
    model = NivelesDeConocimiento
    form_class = NivelesDeConocimientoModelForm
    template_name = "insignias/niveles_conocimiento/registrar.html"
    success_url = reverse_lazy('insignias:registrar_nivel')
    mensaje_exito = "Nivel de conocimiento registrado correctamente!"
    mensaje_error = "Ha ocurrido un error al registrar el nivel de conocimiento. Por favor verifique los datos ingresados."

    def form_valid(self, form):
        form.instance.tipo = 'Nivel de Conocimiento'
        return super(NivelesDeConocimientoRegistrar, self).form_valid(form)


class NivelesDeConocimientoModificar(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = NivelesDeConocimiento
    form_class = NivelesDeConocimientoModelForm
    pk_url_kwarg = 'id_nivel'
    template_name = "insignias/niveles_conocimiento/modificar.html"
    success_url = reverse_lazy('insignias:listado_niveles')
    mensaje_exito = "Nivel de conocimiento modificado correctamente!"
    mensaje_error = "Ha ocurrido un error al modificar el nivel de conocimiento. Por favor verifique los datos ingresados."

    def form_valid(self, form):
        form.instance.tipo = 'Nivel de Conocimiento'
        return super(NivelesDeConocimientoModificar, self).form_valid(form)


class RolesListado(LoginRequiredMixin, ListView):
    model = Roles
    context_object_name = "roles"
    template_name = "insignias/roles/listado.html"

    def get_queryset(self):
        roles = Roles.objects.all()
        return roles


class RolesConsultar(LoginRequiredMixin, ListView):
    model = Roles
    context_object_name = "roles"
    template_name = "insignias/roles/consultar.html"

    def get_queryset(self):
        roles = Roles.objects.all()
        return roles


class RolesRegistrar(LoginRequiredMixin, MensajeMixin, CreateView):
    model = Roles
    form_class = RolesModelForm
    template_name = "insignias/roles/registrar.html"
    success_url = reverse_lazy('insignias:registrar_rol')
    mensaje_exito = "Rol registrado correctamente!"
    mensaje_error = "Ha ocurrido un error al registrar el rol. Por favor verifique los datos ingresados."

    def form_valid(self, form):
        form.instance.tipo = 'Rol'
        return super(RolesRegistrar, self).form_valid(form)


class RolesModificar(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = Roles
    form_class = RolesModelForm
    pk_url_kwarg = 'id_rol'
    template_name = "insignias/roles/modificar.html"
    success_url = reverse_lazy('insignias:listado_roles')
    mensaje_exito = "Rol modificado correctamente!"
    mensaje_error = "Ha ocurrido un error al modificar el rol. Por favor verifique los datos ingresados."

    def form_valid(self, form):
        form.instance.tipo = 'Rol'
        return super(RolesModificar, self).form_valid(form)

class InsigniasConsultar(LoginRequiredMixin, TemplateView):
    template_name = "insignias/joven/consultar.html"

    def get_context_data(self, **kwargs):
        from kairos.insignias.models import Tribus, Roles, NivelesDeConocimiento
        from kairos.usuarios.models import Joven
        from kairos.guia.models import Recurso
        from django.db.models import Q, Count
        context = dict()
        joven_sesion_actual = Joven.objects.get(id=self.request.user.id)

        if joven_sesion_actual.joven_dueno_perfil.all().count() > 0 and joven_sesion_actual.joven_dueno_perfil.filter(favorito=True).count() > 0:
            perfiles_deseados = joven_sesion_actual.joven_dueno_perfil.get(favorito=True)
            q = Q()
            if perfiles_deseados.interes_voluntariado == True:
                q = q | Q(nombre__icontains= 'voluntario')
            if perfiles_deseados.interes_trabajo == True:
                q = q | Q(nombre__icontains='colaborador')
            if perfiles_deseados.interes_independiente == True:
                q = q | Q(nombre__icontains='independiente')
            if perfiles_deseados.interes_emprendedor == True:
                q = q | Q(nombre__icontains='emprendedor')

            insignias_tribus = []
            insignias_nivel_conocimiento = []
            
            if not perfiles_deseados.tribu_deseada is None:
                insignias_tribus = [perfiles_deseados.tribu_deseada]
            if not perfiles_deseados.nivel_conocimiento_deseado is None:
                insignias_nivel_conocimiento = [perfiles_deseados.nivel_conocimiento_deseado]
            insignias_rol = Roles.objects.filter(q)

            
            context['insignias_tribus'] = insignias_tribus
            context['insignias_rol'] = insignias_rol
            context['insignias_nivel_conocimiento'] = insignias_nivel_conocimiento

        video = Recurso.objects.get(nombre='¿Qué encuentras en insignias?')
        context['video'] = video

        return context

class TipoDeEquipoListado(LoginRequiredMixin, ListView):
    model = TipoDeEquipo
    context_object_name = "tipos_de_equipo"
    template_name = "insignias/tipo_equipo/listado.html"

    def get_queryset(self):
        tipos_de_equipo = TipoDeEquipo.objects.all()
        return tipos_de_equipo


class TipoDeEquipoConsultar(LoginRequiredMixin, ListView):
    model = TipoDeEquipo
    context_object_name = "tipos_de_equipo"
    template_name = "insignias/tipo_equipo/consultar.html"

    def get_queryset(self):
        tipos_de_equipo = TipoDeEquipo.objects.all()
        return tipos_de_equipo


class TipoDeEquipoRegistrar(LoginRequiredMixin, MensajeMixin, CreateView):
    model = TipoDeEquipo
    form_class = TipoDeEquipoForm
    template_name = "insignias/tipo_equipo/registrar.html"
    success_url = reverse_lazy('insignias:registrar_tipo_equipo')
    mensaje_exito = "Tipo de equipo registrado correctamente!"
    mensaje_error = "Ha ocurrido un error al registrar el tipo de equipo. Por favor verifique los datos ingresados."

    def form_valid(self, form):
        tipo = form.save(commit=False)
        tipo.tipo = 'Tipo De Equipo'
        tipo.save()
        return super(TipoDeEquipoRegistrar, self).form_valid(form)


class TipoDeEquipoModificar(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = TipoDeEquipo
    form_class = TipoDeEquipoForm
    pk_url_kwarg = 'id_tipo_equipo'
    template_name = "insignias/tipo_equipo/modificar.html"
    success_url = reverse_lazy('insignias:listado_tipo_equipo')
    mensaje_exito = "Tipo de equipo modificado correctamente!"
    mensaje_error = "Ha ocurrido un error al modificar el tipo de equipo. Por favor verifique los datos ingresados."

    def form_valid(self, form):
        form.instance.tipo = 'Tipo De Equipo'
        return super(TipoDeEquipoModificar, self).form_valid(form)