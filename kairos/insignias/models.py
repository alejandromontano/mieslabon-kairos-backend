# Modulos Django
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.postgres.fields import ArrayField

# Modulos de plugin externos
from simple_history.models import HistoricalRecords
import os
# Modulos de otras apps

# Modulos internos de la app

def subir_img_insignia(instance, filename):
    ext = filename.split('.')[-1]  # eg: 'jpg'
    renamed_filename = '%(tribu)s.%(ext)s' % {'tribu': instance.nombre.encode('utf-8'), 'ext': ext}
    return os.path.join('insignias/', renamed_filename)


class Insignia(models.Model):
    tipo = models.CharField(max_length=100, verbose_name="tipo")
    nombre = models.CharField(max_length=160, verbose_name="nombre de la insignia", unique=True)
    descripcion = models.CharField(max_length=300, verbose_name="descripción", null=True)
    imagen = models.FileField(upload_to=subir_img_insignia, blank=True, null=True)
    activo = models.BooleanField(verbose_name="¿La insignia se encuentra activa?", default=True)
    history = HistoricalRecords(inherit=True)

    class Meta:
        ordering = ['nombre']
        abstract = True

    def __str__(self):
        return self.nombre

@receiver(post_save, sender=Insignia, dispatch_uid="minificar_imagen_insignias")
def comprimir_imagen_insignia(sender, **kwargs):
    from kairos.core.utils import comprimir_imagen
    if kwargs["instance"].imagen:
        comprimir_imagen(kwargs["instance"].imagen.path)

class Tribus(Insignia):
    INTERESES_APTITUDES = (
        ('C', 'Administración'),
        ('H', 'Humanidades'),
        ('A', 'Artes y Deportes'),
        ('S', 'Salud'),
        ('I', 'Ingeniería'),
        ('D', 'Seguridad y Defensa'),
        ('E', 'Ciencia y Medio Ambiente'),
        ('K', 'Servicios'),
    )
    interes_aptitud = models.CharField(max_length=20, verbose_name="interés/aptitud", choices=INTERESES_APTITUDES)

    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

    @staticmethod
    def obtener_nombres_imagen_lista() -> list:
        ''' Función que retorna los nombres de las tribus en una  lista '''
        tribus = Tribus.objects.all().values("nombre", "imagen")
        return list(tribus)

    @staticmethod
    def obtener_nombre_lista() -> list:
        ''' Función que retorna los nombres de las tribus en una  lista '''
        tribus = Tribus.objects.all().values_list("nombre", flat=True)
        return list(tribus)

class Familia(Insignia):
    tribu = models.ForeignKey(Tribus, related_name="familias_de_la_tribu", on_delete=models.CASCADE,
                                 verbose_name="tribu a la que pertenece", null=True, blank=True)

class RutaConocimiento(models.Model):
    familia = models.ForeignKey(Familia, related_name="rutas_de_conocimiento_de_la_familia", on_delete=models.CASCADE,
                                 verbose_name="familia a la que pertenece", null=True, blank=True)
    nombre = models.CharField(max_length=200, verbose_name="")
    activo = models.BooleanField(verbose_name="¿El campo se encuentra activo?", default=True)
    palabras_clave = ArrayField(models.CharField(max_length=80, null=True), null=True, default=list)
    history = HistoricalRecords()

    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

    def obtener_por_id(id_campo):
        try:
            return RutaConocimiento.objects.get(id=id_campo)
        except RutaConocimiento.DoesNotExist:
            return None

    def obtener_palabras_clave(self:'RutaConocimiento') -> 'ArrayField':
        '''
            Obtiene de una ruta de conocimiento sus palabras claves
        '''
        return self.palabras_clave

    def agregar_palabra(self, palabra):
        self.palabras_clave.append(palabra.palabra)
        self.save()
        palabra.eliminar()

    def asignar_tribus_a_contenido_por_palabras_clave(palabras_clave:list, contenido):
        for palabra in palabras_clave:
            palabra = palabra.capitalize()
            rutas_de_conocimiento = RutaConocimiento.objects.filter(palabras_clave__contains=[palabra])
            if rutas_de_conocimiento:
                for ruta_de_conocimiento in rutas_de_conocimiento:
                    familia = ruta_de_conocimiento.familia
                    tribu = familia.tribu
                    if tribu.nombre == 'Centeros':
                        contenido.insignias_centeros.add(ruta_de_conocimiento)
                        contenido.centeros = True
                    if tribu.nombre == 'Cuisana':
                        contenido.insignias_cuisana.add(ruta_de_conocimiento)
                        contenido.cuisana = True
                    if tribu.nombre == 'Gronor':
                        contenido.insignias_gronor.add(ruta_de_conocimiento)
                        contenido.gronor = True
                    if tribu.nombre == 'Moterra':
                        contenido.insignias_moterra.add(ruta_de_conocimiento)
                        contenido.moterra = True
                    if tribu.nombre == 'Ponor':
                        contenido.insignias_ponor.add(ruta_de_conocimiento)
                        contenido.ponor = True
                    if tribu.nombre == 'Vincularem':
                        contenido.insignias_vincularem.add(ruta_de_conocimiento)
                        contenido.vincularem = True
                    if tribu.nombre == 'Brianimus':
                        contenido.insignias_brianimus.add(ruta_de_conocimiento)
                        contenido.brianimus = True
                    if tribu.nombre == 'Sersas':
                        contenido.insignias_sersas.add(ruta_de_conocimiento)
                        contenido.sersas = True
                    contenido.save()
            else:
                PalabraClaveExcluida.crear(palabra)

class PalabraClaveExcluida(models.Model):
    palabra = models.CharField(max_length=200, verbose_name="")

    def __str__(self):
        return self.palabra

    @staticmethod
    def obtener(id_palabra):
        try:
            return PalabraClaveExcluida.objects.get(id=id_palabra)
        except PalabraClaveExcluida.DoesNotExist:
            return None

    @staticmethod
    def crear(palabra):
        existe_palabra = PalabraClaveExcluida.objects.filter(palabra__iexact=palabra).exists()
        existe_palabra_limpieza = PalabraDeLimpieza.objects.filter(palabra__iexact=palabra).exists()
        if existe_palabra == False and existe_palabra_limpieza == False: 
            PalabraClaveExcluida.objects.create(palabra=palabra)

    @staticmethod
    def obtener_todas():
        return PalabraClaveExcluida.objects.all()

    def eliminar(self):
        self.delete()

    def crear_palabra_de_limpieza(self):
        PalabraDeLimpieza.crear(self.palabra)
        self.delete()
        
class PalabraDeLimpieza(models.Model):
    palabra = models.CharField(max_length=200, verbose_name="")

    def __str__(self):
        return self.palabra

    @staticmethod
    def crear(palabra):
        existe_palabra = PalabraDeLimpieza.objects.filter(palabra=palabra).exists()
        if existe_palabra == False: 
            PalabraDeLimpieza.objects.create(palabra=palabra)

    @staticmethod
    def obtener_todas():
        return PalabraDeLimpieza.objects.all()


class NivelesDeConocimiento(Insignia):
    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

    @staticmethod
    def obtener_por_nombre(nombre):
        try:
            return NivelesDeConocimiento.objects.get(nombre=nombre)
        except NivelesDeConocimiento.DoesNotExist:
            return None

    @staticmethod
    def obtener_activos() -> 'Queryset<NivelesDeConocimiento>':
        '''
            Función que retorna todos los niveles de conocimiento con el campo activo en true
        '''
        niveles = NivelesDeConocimiento.objects.filter(activo=True)
        return niveles

class Roles(Insignia):

    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

    @staticmethod
    def obtener_por_nombre(nombre):
        try:
            return Roles.objects.get(nombre=nombre)
        except Roles.DoesNotExist:
            return None

class TipoDeEquipo(Insignia):

    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return self.nombre
