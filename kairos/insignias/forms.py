# Modulos Django
from django import forms
from django.forms import inlineformset_factory

# Modulos de plugin externos
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML
from django_select2.forms import Select2Widget

# Modulos de otras apps

# Modulos internos de la app
from .models import *
from .insignias_formset import *

class TribusModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TribusModelForm, self).__init__(*args, **kwargs)
       
    class Meta:
        model = Tribus
        fields = ('nombre', 'interes_aptitud', 'descripcion', 'imagen', 'activo')

        widgets = {
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
            'interes_aptitud': Select2Widget()
        }


class FamiliaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FamiliaForm, self).__init__(*args, **kwargs)        

    class Meta:
        model = Familia
        fields = ('nombre', 'descripcion', 'imagen', 'activo', 'tribu')
        widgets = {
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
            'tribu': Select2Widget(),
        }

class RutaConocimientoModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(RutaConocimientoModelForm, self).__init__(*args, **kwargs)
        
       
    class Meta:
        model = RutaConocimiento
        fields = ('nombre','activo', 'familia')
        widgets = {
            'familia': Select2Widget(),
        }


RutasConocimientoFormSet = inlineformset_factory(
    Familia, RutaConocimiento, form=RutaConocimientoModelForm,
    fields=['nombre', 'activo'], extra=1, can_delete=True
)


class NivelesDeConocimientoModelForm(forms.ModelForm):

    class Meta:
        model = NivelesDeConocimiento
        fields = ['nombre', 'descripcion', 'imagen', 'activo']

        widgets = {
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
        }


class RolesModelForm(forms.ModelForm):

    class Meta:
        model = Roles
        fields = ['nombre', 'descripcion', 'imagen', 'activo']

        widgets = {
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
        }

class TipoDeEquipoForm(forms.ModelForm):

    class Meta:
        model = TipoDeEquipo
        fields = ['nombre', 'descripcion', 'imagen', 'activo']

        widgets = {
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
        }
