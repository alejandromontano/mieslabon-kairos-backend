# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "insignias"

urlpatterns = [
    path('tribus/gestionar/', TribusListado.as_view(), name='listado_tribus'),
    path('tribus/consultar/', TribusConsultar.as_view(), name='consultar_tribus'),
    path('tribus/registrar', TribuRegistrar.as_view(), name='registrar_tribu'),
    path('tribus/modificar/<int:id_tribu>', TribuModificar.as_view(), name='modificar_tribu'),

    path('familias/gestionar/', FamiliasListado.as_view(), name='listado_familias'),    
    path('familias/registrar', FamiliaRegistrar.as_view(), name='registrar_familia'),
    path('familias/modificar/<int:id_familia>', FamiliaModificar.as_view(), name='modificar_familia'),

    path('campos/gestionar/', CamposListado.as_view(), name='listado_campos'),    
    path('campos/registrar', CampoRegistrar.as_view(), name='registrar_campo'),
    path('campos/modificar/<int:id_campo>', CampoModificar.as_view(), name='modificar_campo'),

    path('niveles-conocimiento/gestionar/', NivelesDeConocimientoListado.as_view(), name='listado_niveles'),
    path('niveles-conocimiento/consultar/', NivelesDeConocimientoConsultar.as_view(), name='consultar_niveles'),
    path('niveles-conocimiento/registrar', NivelesDeConocimientoRegistrar.as_view(), name='registrar_nivel'),
    path('niveles-conocimiento/modificar/<int:id_nivel>', NivelesDeConocimientoModificar.as_view(), name='modificar_nivel'),

    path('roles/gestionar/', RolesListado.as_view(), name='listado_roles'),
    path('roles/consultar/', RolesConsultar.as_view(), name='consultar_roles'),
    path('roles/registrar', RolesRegistrar.as_view(), name='registrar_rol'),
    path('roles/modificar/<int:id_rol>', RolesModificar.as_view(), name='modificar_rol'),

    path('joven/consultar/', InsigniasConsultar.as_view(), name='consultar_insignias'),

    path('tipo-equipo/gestionar/', TipoDeEquipoListado.as_view(), name='listado_tipo_equipo'),
    path('tipo-equipo/consultar/', TipoDeEquipoConsultar.as_view(), name='consultar_tipo_equipo'),
    path('tipo-equipo/registrar', TipoDeEquipoRegistrar.as_view(), name='registrar_tipo_equipo'),
    path('tipo-equipo/modificar/<int:id_tipo_equipo>', TipoDeEquipoModificar.as_view(), name='modificar_tipo_equipo'),

]