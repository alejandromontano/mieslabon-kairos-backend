from django.apps import AppConfig


class TribusConfig(AppConfig):
    name = 'kairos.insignias'
