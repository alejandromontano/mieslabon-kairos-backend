function conocerme() {

    const $imagen_conocerme = $("#imagen_conocerme");

    if (porcentaje_completos >= 100 && porcentaje_resultados_leidos >= 100) {
        const src = $imagen_conocerme.attr("src").split("_")[0];
        $imagen_conocerme.attr("src", src + "_2.png")
    }
}

function proyectarme() {
    const porcentaje_creados = Math.round((perfiles_creados / 10) * 100);
    const porcentaje_escogidos = Math.round((perfiles_escogidos / 1) * 100);

    const $elemento_creados = $("#porcentaje_perfiles_creados");
    const $elemento_escogidos = $("#porcentaje_perfiles_escogidos");

    const $imagen_proyectarme = $("#imagen_proyectarme");


    $elemento_creados.css("width", porcentaje_creados > 100 ? 100 + "%" : porcentaje_creados + "%");
    $elemento_creados.children("strong").html(porcentaje_creados + "%")

    $elemento_escogidos.css("width", porcentaje_escogidos > 100 ? 100 + "%" : porcentaje_escogidos + "%");
    $elemento_escogidos.children("strong").html(porcentaje_escogidos + "%")

    if (porcentaje_creados >= 100 && porcentaje_escogidos >= 100) {
        const src = $imagen_proyectarme.attr("src").split("_")[0];
        $imagen_proyectarme.attr("src", src + "_2.png")
    }
}

function conectarme() {
    const $porcentaje_ro_favoritas = $("#porcentaje_ro_favoritas");
    const $porcentaje_ro_escogidas = $("#porcentaje_ro_escogidas");

    const porcentaje_favoritas = Math.round((r_o_favoritos / 30) * 100);
    const porcentaje_escogidas = Math.round((r_o_escogidos / 3) * 100);

    $porcentaje_ro_favoritas.css("width", porcentaje_favoritas + "%");
    $porcentaje_ro_favoritas.children("strong").html(porcentaje_favoritas + "%")

    $porcentaje_ro_escogidas.css("width", porcentaje_escogidas + "%");
    $porcentaje_ro_escogidas.children("strong").html(porcentaje_escogidas + "%")

    const $imagen_conectarme = $("#imagen_conectarme");

    if (porcentaje_favoritas >= 100 && porcentaje_escogidas >= 100) {
        const src = $imagen_conectarme.attr("src").split("_")[0];
        $imagen_conectarme.attr("src", src + "_2.png")
    }
}

function cargar_porcentaje() {
    const total = $barra_niveles.data("total");
    const vistos = $barra_niveles.data("vistos");
    let porcentaje = Math.round((vistos / total) * 100);
    $barra_niveles.css("width", porcentaje + "%");
    porcentaje = isNaN(porcentaje) ? 0 : porcentaje;
    $barra_niveles.html("<strong style=\"font-size: 1rem\">" + porcentaje + "%</strong>");
}

function recorrer_ver_recurso(id, videos, total) {
    let color = "dimgrey"
    let ver = {}
    for (let j = 0; j < total; j++) {
        ver = videos[j];
        if (id === ver.id) {
            color = "FF6000";
            return color
        }
    }
    return color
}

function poner_videos(datos, $card) {

    const total_videos = datos.recursos.total
    const videos = datos.recursos.recursos;
    const total_videos_vistos = datos.recursos_vistos.total
    const videos_vistos = datos.recursos_vistos.recursos;

    const total = $barra_niveles.data("total")
    const total_vistos = $barra_niveles.data("vistos")

    $barra_niveles.data("total", total + total_videos)
    $barra_niveles.data("vistos", total_vistos + total_videos_vistos)

    let color = "";
    for (let i = 0; i < total_videos; i++) {
        color = recorrer_ver_recurso(videos[i].id, videos_vistos, total_videos_vistos);
        if (videos[i].descripcion.includes('"')){
            videos[i].descripcion = videos[i].descripcion.replaceAll('"', '\\"');
        }

        if (videos[i].descripcion.includes("'")){
            videos[i].descripcion = videos[i].descripcion.replaceAll("'", '\\"');
        }

        $card.append(`<div class='row' style='padding-bottom: 5px'>
            <div class='col-auto'>
                <a href='javascript:;' 
                   onclick='cargar_video_a_modal(
                                                ${videos[i].id}, 
                                                "${videos[i].nombre}",
                                                "${videos[i].descripcion}",
                                                "${videos[i].link_video}")'>
                    <img id='video_${videos[i].id}' class=" img-thumbnail rounded float-left"
                         style="padding:4px; background-color:${color};
                         height: 40px; width: 40px" src="${src}play.png">
                </a>
            </div>
            <div class="col-8">
                <span class='text-start align-middle' data-id='${videos[i].id}'
                      style='padding-right: 0; padding-left: 0'>
                ${videos[i].nombre}
                </span>
            </div>
        `);
    }
    cargar_porcentaje();
}

function traer_datos(nombre, poder, $card_body) {
    $.get(url_api_videos_realidad, {
        realidad: nombre,
        poder: poder
    }).done((data) => {
        poner_videos(data, $card_body)
    })
}

function cargar_videos_realidad(realidad) {
    let nombre = ""
    switch (realidad) {
        case '1':
            nombre = "Realidad Verde";
            break;
        case '2':
            nombre = "Realidad Amarilla";
            break;
        case '3':
            nombre = "Realidad Azul";
            break;

    }

    $barra_niveles.data('total', 0)
    $barra_niveles.data('vistos', 0);
    $("#titulo_1").attr("src", src + "yinam_" + realidad + ".png")
    $("#titulo_2").attr("src", src + "fyli_" + realidad + ".png")
    $("#titulo_3").attr("src", src + "novus_" + realidad + ".png")
    $videos_yinam.empty();
    traer_datos(nombre, "Conocerme", $videos_yinam);
    $videos_fyli.empty();
    traer_datos(nombre, "Proyectarme", $videos_fyli);
    $videos_novus.empty();
    traer_datos(nombre, "Conectarme", $videos_novus);
}

function seleccionar_icono(realidad) {
    $("#realidad_" + realidad).attr("src", src + realidad + "_1.png");
    cargar_videos_realidad(realidad);
    $("#oculto").css("display", "");
}

function guardar_realidad(realidad) {
    $.ajax({
        type: "POST",
        url: url_guardar_realidad,
        data: {'id_joven': id_joven, 'realidad': realidad, "csrfmiddlewaretoken": token},
    });
    realidad_activo = realidad;
}

function cambiar_icono(realidad) {
    for (let i = 1; i <= 3; i++) {
        $("#realidad_" + i).attr("src", src + (i) + "_2.png");
    }
    seleccionar_icono(realidad);
    guardar_realidad(realidad);
}

function cargar_video_a_modal(id, nombre, descrip, link) {
    $('#titulo-recurso').html(nombre);
    $('#descripcion').html(descrip);
    $('#iframe_video').css("display", "");
    $('#src_video').attr("src", link);
    $("#detalle-recurso").modal('show');
    $(".btn-close").attr("onclick", "cerrar_modal(" + id + ")")
}

function cerrar_modal(id) {
    $("#detalle-recurso").modal('hide');
    $('#titulo-recurso').html('');
    $('#descripcion').html('');
    $iframe = $("#src_video").attr("src", '');

    if (id !== null) {
        $.ajax({
            type: "POST",
            url: url_cambiar_estado_video,
            data: {'valor_del_video': 1, 'id_video': id, 'id_joven': id_joven, "csrfmiddlewaretoken": token},
            success: (res) => {
                cargar_videos_realidad(realidad_activo);
            }
        });
    }
}
