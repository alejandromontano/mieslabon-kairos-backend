var tribus = ['cuisana', 'moterra', 'ponor', 'vincularem', 'brianimus', 'gronor', 'centeros', 'sersas'];

function init_tribus() {
    for(let i=0; i<tribus.length; i++){
        $("#id_"+tribus[i])[0].onchange = function(e){ 
            if($("#id_"+tribus[i])[0].checked){
                $("#id_insignias_"+tribus[i]).parent().show();
                $("#id_insignias_"+tribus[i]+"_familia").parent().show();	
            }
            else{
                $("#id_insignias_"+tribus[i]).val([]);
                $("#id_insignias_"+tribus[i]).parent().hide();
                $("#id_insignias_"+tribus[i]+"_familia").parent().hide();
            }
        };
    }
}

for(let i=0; i<tribus.length; i++){    
    if($("#id_"+tribus[i])[0].checked){               
        $("#id_insignias_"+tribus[i]).parent().show();
        $("#id_insignias_"+tribus[i]+"_familia").parent().show();
    }
    else{
        $("#id_insignias_"+tribus[i]).val([]);
        $("#id_insignias_"+tribus[i]).parent().hide();
        $("#id_insignias_"+tribus[i]+"_familia").parent().hide();
    }
}

$("#id_niveles_conocimiento").select2({
    templateResult: formatStateNiveles,
    templateSelection: formatStateNiveles,
});
$("#id_roles").select2({
    templateResult: formatStateRoles,
    templateSelection: formatStateRoles,
});
function formatStateRoles (state) {
    if (!state.id) {
        return state.text;
    }
    nombre = state.text;
    url = '/static/img/roles/'+String(state.id)+'_1.png';
    var $state = $(
        '<span><img style="height:28px; width:28px" src="'+ url +'" class="img-flag" /> ' + nombre + '</span>'
    );
    return $state;
}
function formatStateNiveles (state) {
    if (!state.id) {
        return state.text;
    }
    nombre = state.text;
    url = '/static/img/niveles_conocimiento/'+String(state.id)+'_1.png';
    var $state = $(
        '<span><img style="height:28px; width:28px" src="'+ url +'" class="img-flag" /> ' + nombre + '</span>'
    );
    return $state;
}