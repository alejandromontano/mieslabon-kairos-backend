var intervalo_verificacion;

function confirmar_reto_finalizado(detalle_id, reto_id){
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_confirmar_reto_finalizado,
        data: {
            'json_text': JSON.stringify({                    
                'id_detalle': detalle_id,
                'id_reto': reto_id,                
            })
        },
        dataType: "json",
        success: function (data) {
            url = data['url_redireccion']  
            if(url !== "Espera"){
                redirect_url = data['url_redireccion'];                    
                window.location = redirect_url; 
                intervalo_verificacion = null     
            }   
            
            if (cuarto_finalizacion){
                clearInterval(intervalo_verificacion);
                intervalo_verificacion = null
            }
        },
        error: function (data){
            if (cuarto_finalizacion){
                clearInterval(intervalo_verificacion);
                intervalo_verificacion = null                
            }
        }
    });
}


function intervalo_tiempo_estados_miembro(detalle_id, reto_id){
    if(!intervalo_verificacion && !cuarto_finalizacion){
        intervalo_verificacion = setInterval(function(){    
            confirmar_reto_finalizado(detalle_id, reto_id)
        }
        , 2000);
    }
}