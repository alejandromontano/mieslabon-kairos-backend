var intervalo;


function presentar_miembro_equipo(equipo_id, reto_id, miembro_id, estado) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: '/app/retos/api/asistencia-equipos',
        data: {
            'json_text': JSON.stringify({
                'pk_equipo': equipo_id,
                'pk_reto': reto_id,
                'pk_miembro':miembro_id,
                'estado':estado,
            })
        },
        dataType: "json",
        success: function (data) {
            if (data.result == true) {
                if (data.estado == true) {
                    toastr.success('Te has presentado correctamente!', '', {
                        "closeButton": true,
                        "progressBar": true,
                        "showEasing": "swing",
                        "extendedTimeOut": 5000,
                        "timeOut": 3000,
                    });                      
                }else{
                    toastr.success('Te has ausentado correctamente!', '', {
                        "closeButton": true,
                        "progressBar": true,
                        "showEasing": "swing",
                        "extendedTimeOut": 5000,
                        "timeOut": 3000,
                    });  
                }              
            }          
        }
    });
}



function intervalo_tiempo_estados_miembro(pk_equipo, pk_reto){
    if(!intervalo){
        intervalo = setInterval(function(){    
            cargar_estados_miembros_equipos(pk_equipo, pk_reto)
            cargar_pagina_finalizar_reto(pk_equipo, pk_reto, pk_detalle)
        }
        , 2000);
    }
}

function cargar_estados_miembros_equipos(pk_equipo, pk_reto) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: '/app/retos/api/verificar-asistencia-equipos',
        data: {
            'json_text': JSON.stringify({
                'pk_equipo': pk_equipo,
                'pk_reto': pk_reto
            })
        },
        dataType: "json",
        success: function (data) {
            detalle = data['detalle']
            contador = 1;
            $.each(detalle, function( k, v ) {
                if (v == false) {
                    $('#miembro_estado_'+ k).text('Ausente')
                    $('#presentarse_' + k).show();
                    $('#ausentarse_' + k).hide();                    
                }else{
                    $('#miembro_estado_'+ k).text('Presente')
                    $('#presencia_miembro_'+k).css('background', '#A32CDF');
                    $('#presentarse_' + k).hide();
                    $('#ausentarse_' + k).show();
                    contador++;
                }
            });

            cantidad_de_miembros = Object.keys(detalle).length;
            promedio = ((contador/cantidad_de_miembros)*100).toFixed(2)
            $('#porcentaje_miembros').text(promedio)
        }
    });
}


function cargar_pagina_finalizar_reto(pk_equipo, pk_reto, pk_detalle) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_cargar_pgina_finalizacion,
        data: {
            'json_text': JSON.stringify({
                'id_equipo': pk_equipo,
                'id_reto': pk_reto,
                'id_detalle': pk_detalle,
            })
        },
        dataType: "json",
        success: function (data) {
            url = data['url_redireccion']  
            if(url !== "Espera"){
                redirect_url = data['url_redireccion'];                    
                window.location = redirect_url;      
            }               
        }
    });
}