var intervalo_temporizador_pregunta
var faltante_pregunta = 0

function temporizador_tiempo_transcurrido(pregunta_pk){    
    bloquear_otras_preguntas(pregunta_pk)
    retos_api_guardar_tiempo_pregunta(pregunta_pk)
    cambiar_propiedades_pregunta_actual(pregunta_pk)
    if(!intervalo_temporizador_pregunta){       
        
        intervalo_temporizador_pregunta = setInterval(function() {           
            var seconds = Math.floor(faltante_pregunta/1000);            
            faltante_pregunta = faltante_pregunta - 1000
            configurar_formato_contador_preguntas(pregunta_pk, seconds)
            if(faltante_pregunta<=0){
                location.reload();
                localStorage.removeItem('pregunta_actual')
            }

        opcion_chekeada = $( "#opcion_"+pregunta_pk+":checked" ).val()        
        }, 1000);
    }
}

function configurar_formato_contador_preguntas(elemento_html, segundo){    
    if (parseInt(segundo)<10){
        $("#tiempo_"+elemento_html).text("0"+segundo)
    }else{        
        $("#tiempo_"+elemento_html).text(segundo)
    }
}

function bloquear_otras_preguntas(pregunta_pk_actual){    
    $('[id^="boton_activo_"]').prop('disabled', true);   
    $('[id^="boton_activo_"]').removeClass("bg-orange");
    $('[id^="boton_activo_"]').addClass("bg-gray");

    $('#boton_activo_'+pregunta_pk_actual).attr("disabled", false );
    $('#boton_activo_'+pregunta_pk_actual).removeClass("bg-gray");
    $('#boton_activo_'+pregunta_pk_actual).addClass("bg-orange");   
    
}

function cambiar_propiedades_pregunta_actual(pregunta_pk){    
    let boton_mensaje = $("#boton_activo_"+pregunta_pk)    
    
    
    boton_mensaje.html('Enviar')
    boton_mensaje.removeClass("bg-orange");
    boton_mensaje.addClass("bg-info");  


    
    let hay_seleccion = false
    $("#bloque_"+pregunta_pk).find("[id^=opcion]").each(function(){
        if ($(this).prop("checked") == true){
            hay_seleccion=true
        }
        
    })
    
    if(hay_seleccion){        
        boton_mensaje.attr('onClick', function(){            
            actualizar_estado_pregunta(pregunta_pk)
            window.location.reload();
        });
    }
        
    
}

function actualizar_estado_pregunta(pregunta_pk){
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_enviar_respuesta_pregunta,
        data: {
                'json_text': JSON.stringify({ 
                'id_pregunta': pregunta_pk,
                'id_detalle':detalle,
                'id_reto':reto_id,
            })
        },
        dataType: "json",
        success: function (data) {                      
            window.location.reload();                    
        }
    });
}


function retos_api_guardar_tiempo_pregunta(id_pregunta){
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_guardar_tiempo_pregunta,
        data: {
                'json_text': JSON.stringify({                        
                'id_participante': id_participante,            
                'id_detalle': detalle,
                'id_reto': reto_id,
                'id_pregunta':id_pregunta,
            })
        },
        dataType: "json",
        success: function (data) {         
            faltante_pregunta = parseInt(data['tiempo_faltante'])*1000           
        },
        
    });
        
}