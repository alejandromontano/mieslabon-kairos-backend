
var countdown = faltante_cuarto;
var intervalo_temporizador_final;

var current_timer = 0
var last_update_time = new Date().getTime()
var dt = 0

function CountdownTracker(label, value){

    var el = document.createElement('span');

    el.className = 'flip-clock__piece row no-gutter col-2 text-center';
    el.innerHTML =  '<span class="flip-clock__slot">' + label + '</span>' +
    '<b class="flip-clock__card card"><b class="card__top"></b><b class="card__bottom"></b><b class="card__back"><b class="card__bottom"></b></b></b>'
     ;
  
    this.el = el;
  
    var top = el.querySelector('.card__top'),
        bottom = el.querySelector('.card__bottom'),
        back = el.querySelector('.card__back'),
        backBottom = el.querySelector('.card__back .card__bottom');
  
    this.update = function(val){
      val = ( '0' + val ).slice(-2);
      if ( val !== this.currentValue ) {
        
        if ( this.currentValue >= 0 ) {
          back.setAttribute('data-value', this.currentValue);
          bottom.setAttribute('data-value', this.currentValue);
        }
        this.currentValue = val;
        top.innerText = this.currentValue;
        backBottom.setAttribute('data-value', this.currentValue);
  
        this.el.classList.remove('flip');
        void this.el.offsetWidth;
        this.el.classList.add('flip');
      }
    }
    
    this.update(value);
  }
  
  // Calculation adapted from https://www.sitepoint.com/build-javascript-countdown-timer-no-dependencies/
  
  function getTimeRemaining(endtime, crecimiento=false) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    return {
      'Total': t,
      'Días': Math.floor(t / (1000 * 60 * 60 * 24)),
      'Horas': Math.floor((t / (1000 * 60 * 60)) % 24),
      'Minutos': Math.floor((t / 1000 / 60) % 60),
      'Segundos': Math.floor((t / 1000) % 60)
    };
  }
  
  function getTimeRemainingInteger(time, crecimiento=false) {
    var t = time;
    let now = new Date().getTime()
    dt = now - last_update_time

    countdown += dt    
    last_update_time = now

    return {
      'Total': t,
      'Días': Math.floor(countdown / (1000 * 60 * 60 * 24)),
      'Horas': Math.floor((countdown / (1000 * 60 * 60)) % 24),
      'Minutos': Math.floor((countdown / 1000 / 60) % 60),
      'Segundos': Math.floor((countdown / 1000) % 60)
    };
  }  
   
  
  function Clock(countdown, tiempo=true, crecimiento=false) {
   
    countdown = tiempo ? new Date(Date.parse(countdown)) : countdown;    
    
    var updateFn = tiempo ? getTimeRemaining : getTimeRemainingInteger;
  
    this.el = document.createElement('div');
    this.el.className = 'flip-clock row col-12';
    
    var trackers = {},       
        t = updateFn(countdown, crecimiento),
        key, timeinterval;
  
    for ( key in t ){
      if ( key === 'Total' ) { continue; }
      trackers[key] = new CountdownTracker(key, t[key]);   
      this.el.appendChild(trackers[key].el);
    }
  
    var i = 0;
    function updateClock() {
      timeinterval = requestAnimationFrame(updateClock);
      
      // throttle so it's not constantly updating the time.
      
      if ( i++ % 10 ) { return; }
      
      var t = updateFn(countdown,crecimiento);
     
        
      
      if ( t.Total < 0 && crecimiento == false) {
        
        cancelAnimationFrame(timeinterval);
        for ( key in trackers ){
          trackers[key].update( 0 );
        }
        callback();
        return;
      }
      
      for ( key in trackers ){ 
        trackers[key].update( t[key] );
      }
    }
    
    if(crecimiento){
        tiempo_trancurrido = 1000    
    }     
    setTimeout(updateClock,tiempo_trancurrido);
    
  }
  