function initCalificacionFormulario(pk_usuario, pk_formulario = null) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_cargar_calificacion_formulario,
        data: {
            'json_text': JSON.stringify({
                'pk_usuario': pk_usuario,
                'pk_formulario': pk_formulario
            })
        },
        dataType: "json",
        success: function (data) {
            if(data.calificaciones){
                let calificaciones = data['calificaciones'];
                for(let i=0;i<calificaciones.length;i++){
                    let stars = calificaciones[i]['stars'];
                    if(stars>0){
                        for(let j=1;j<=stars;j++){
                            let id = '#star'+j+'-formulario-'+calificaciones[i]['pk'];
                            $(id).parent().addClass("activo");
                        }
                    }
                }
            }
        }
    });
}

function calificar_formulario(number, pk_form, pk_usuario){
    let elem = $('#star'+number+'-formulario-'+pk_form);
    if (!elem.parent().hasClass('activo')) data = {'pk_formulario': pk_formulario, 'pk_usuario': pk_usuario, 'star': number};
    else data = {'pk_formulario': pk_formulario,'pk_usuario': pk_usuario, 'star': number-1};
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_calificar_formulario,
        data: {
            'json_text': JSON.stringify(data)
        },
        dataType: "json",
        success: function (data) {                       
             
            for(let i=1;i<=number;i++){
                $('#star'+i+'-formulario-'+pk_form).addClass("activo");                
            }       
            for(let i=number+1;i<=5;i++){
                $('#star'+i+'-formulario-'+pk_form).removeClass("activo");                
            }   
                     
            toastr.info('Gracias por tu calificación!', '', {
                "closeButton": true,
                "progressBar": true,
                "showEasing": "swing",
                "extendedTimeOut": 5000,
                "timeOut": 3000,
            });
        }
    });
}
