function init_usuario() {
    if(document.querySelector('#id_terminos') !== null){
        document.querySelector("label[for='id_terminos']").innerHTML = 'He leído y acepto los '+terminos_text+'*';
        if(terminos_val === "True")
            document.getElementById("id_terminos").parentElement.parentElement.parentElement.parentElement.style.display = "none";
    }

    init_paises();
    document.querySelector('.custom-file-input').addEventListener('change',function(e){
        var fileName = document.getElementById("id_foto_perfil").files[0].name;
        var nextSibling = e.target.nextElementSibling;
        nextSibling.innerText = fileName;
    });
    init_cropping();
}

function set_fecha_nacimiento_maxima() {
    hoy = new Date();
    max_f = new Date();
    max_f.setFullYear(hoy.getFullYear() - 13);
    fecha_max = new Date(max_f.getTime() - max_f.getTimezoneOffset() * -60000); //normalizar hora
    $('#id_fecha_de_nacimiento').data("DateTimePicker").maxDate(fecha_max);
}

function init_paises() {
    var pais = document.getElementById("id_pais");
    pais.onchange = function(){
        $('#id_region').empty();
        $('#id_ciudad').empty();
    };
    var region = document.getElementById("id_region");
    region.onchange = function(){
        $('#id_ciudad').empty();
    };
}

function init_paises_intereses() {
    var pais = document.getElementById("id_interes_pais");
    pais.onchange = function(){
        $('#id_interes_ciudad').empty();
    };
}

function init_indicativo_telefono() {
    var id_telefono = document.querySelector("#id_telefono");
    var indicativo = document.getElementById("id_indicativo_telefono");

    var iti = window.intlTelInput(id_telefono, {
        nationalMode: true,
        initialCountry: "CO",
        preferredCountries: ['CO'],
        utilsScript: "{% static 'plugins/intl-tel-input-17.0.0/build/js/utils.js?1603274336113' %}"

    });

    id_telefono.addEventListener('countrychange', function () {
        indicativo.value = iti.getSelectedCountryData().dialCode;
    });
}

function modificar_indicativo_telefono() {
    var id_telefono = document.querySelector("#id_telefono");
    var indicativo = document.getElementById("id_indicativo_telefono");
    var countryData = window.intlTelInputGlobals.getCountryData();
    var paisActual = 'CO';

    for (var i = 0; i < countryData.length; i++) {
        if (indicativo.value == countryData[i].dialCode) {
            paisActual = countryData[i].iso2;
            break;
        }
    }

    var iti = window.intlTelInput(id_telefono, {
        nationalMode: true,
        initialCountry: paisActual,
        preferredCountries: ['CO'],
        utilsScript: "{% static 'plugins/intl-tel-input-17.0.0/build/js/utils.js?1603274336113' %}"

    });

    id_telefono.addEventListener('countrychange', function () {
        indicativo.value = iti.getSelectedCountryData().dialCode;
    });

    $('#id_indicativo_telefono').hide();
    $("label[for=id_indicativo_telefono]").hide();
}

function init_cropping() {
    /* SCRIPT TO OPEN THE MODAL WITH THE PREVIEW */
    $("#id_foto_perfil").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#image").attr("src", e.target.result);
                $("#modalCrop").modal("show");
            };
            reader.readAsDataURL(this.files[0]);
        }
    });
    var cropBoxData;
    var canvasData;
    $("#modalCrop").on("shown.bs.modal", function () {
        imagen_recortada.cropper({
            viewMode: 1,
            aspectRatio: 1/1,
            minCropBoxWidth: 600,
            minCropBoxHeight: 600,
            ready: function () {
                imagen_recortada.cropper("setCanvasData", canvasData);
                imagen_recortada.cropper("setCropBoxData", cropBoxData);
            }
        });
    }).on("hidden.bs.modal", function () {
        cropBoxData = imagen_recortada.cropper("getCropBoxData");
        canvasData = imagen_recortada.cropper("getCanvasData");
        imagen_recortada.cropper("destroy");
        $("#id_x").val("");
        $("#id_y").val("");
        $("#id_width").val("");
        $("#id_height").val("");
        if(imagen_actual == "")
            document.querySelector('.custom-file-input').nextElementSibling.innerText = 'Seleccione una imagen...';
        else
            document.querySelector('.custom-file-input').nextElementSibling.innerText = imagen_actual;
        document.getElementById("id_foto_perfil").value = "";
    });

// Enable zoom in button
    $(".js-zoom-in").click(function () {
        imagen_recortada.cropper("zoom", 0.1);
    });

// Enable zoom out button
    $(".js-zoom-out").click(function () {
        imagen_recortada.cropper("zoom", -0.1);
    });

    $(".js-crop-and-upload").click(function () {
        var cropData = imagen_recortada.cropper("getData");
        $("#id_x").val(cropData["x"]);
        $("#id_y").val(cropData["y"]);
        $("#id_height").val(cropData["height"]);
        $("#id_width").val(cropData["width"]);
        $("#form_perfil").submit();
    });
}

