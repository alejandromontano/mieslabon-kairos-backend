$("#id_nombre").on("input", function(e) {
    var val = $(this).val();
    validacionDuplicidadEquipos(val)    
    });

function validacionDuplicidadEquipos(val) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_validacion_equipos,
        data: {
            'json_text': JSON.stringify({
                'valor': val
            })
        },
        dataType: "json",
        success: function (data) {
            $('.equipos').remove();
            if (data.equipos.length > 0){
                $('#nombre_opciones').append('<div class="col-12 equipos" align="left">Estos son los equipos que ya existen:<ul id="opciones"></ul></div>');
                for (let index = 0; index < data.equipos.length; index++) {
                    $('#opciones').append('<li>' + data.equipos[index] +'</li>')
                    
                }
            }
        }
    });
}