$("#id_nombre").on("input", function(e) {
    var val = $(this).val();
    validacionDuplicidad(val)    
    });

function validacionDuplicidad(val) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_validacion_rutas,
        data: {
            'json_text': JSON.stringify({
                'valor': val
            })
        },
        dataType: "json",
        success: function (data) {
            $('.rutas').remove();
            if (data.rutas.length > 0){
                $('#nombre_opciones').append('<div class="col-12 rutas" align="left">Estas son las rutas que ya existen:<ul id="opciones"></ul></div>');
                for (let index = 0; index < data.rutas.length; index++) {
                    $('#opciones').append('<li>' + data.rutas[index] +'</li>')
                    
                }
            }
        }
    });
}