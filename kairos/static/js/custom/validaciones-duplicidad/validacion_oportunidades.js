$("#id_nombre").on("input", function(e) {
    var val = $(this).val();
    validacionDuplicidadOportunidades(val)
    });

function validacionDuplicidadOportunidades(val) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_validacion_oportunidades,
        data: {
            'json_text': JSON.stringify({
                'valor': val
            })
        },
        dataType: "json",
        success: function (data) {
            console.log(data.oportunidades.length)
            $('.oportunidades').remove();
            if (data.oportunidades.length > 0){
                $('#nombre_opciones').append('<div class="col-12 oportunidades" align="left">Estás son las oportunidades que ya existen:<ul id="opciones"></ul></div>');
                for (let index = 0; index < data.oportunidades.length; index++) {
                    $('#opciones').append('<li>' + data.oportunidades[index] +'</li>')
                    
                }
            }
        }
    });
}