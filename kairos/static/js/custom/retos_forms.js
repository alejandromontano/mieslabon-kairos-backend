function init_usuario() {
       
    document.querySelector('.custom-file-input').addEventListener('change',function(e){
        var fileName = document.getElementById("id_imagen").files[0].name;                     
        var nextSibling = e.target.nextElementSibling;
        nextSibling.innerText = fileName;
    });
    init_cropping();
}

function init_cropping() {
    
    /* SCRIPT TO OPEN THE MODAL WITH THE PREVIEW */
    $("#id_imagen").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#image").attr("src", e.target.result);                
                $("#modalCrop").modal("show");
            };          
            reader.readAsDataURL(this.files[0]);
        }
        
    });
    var cropBoxData;
    var canvasData;
   
    $("#modalCrop").on("shown.bs.modal", function () {
        imagen_recortada.cropper({
            viewMode: 1,
            aspectRatio: 1/1,
            minCropBoxWidth: 600,
            minCropBoxHeight: 600,
            ready: function () {
                imagen_recortada.cropper("setCanvasData", canvasData);
                imagen_recortada.cropper("setCropBoxData", cropBoxData);
            }
        });
    }).on("hidden.bs.modal", function () {
        cropBoxData = imagen_recortada.cropper("getCropBoxData");
        canvasData = imagen_recortada.cropper("getCanvasData");
        imagen_recortada.cropper("destroy");
        $("#id_x").val("");
        $("#id_y").val("");
        $("#id_width").val("");
        $("#id_height").val("");
        if(imagen_actual == "")
            document.querySelector('.custom-file-input').nextElementSibling.innerText = 'Seleccione una imagen...';
        else
            document.querySelector('.custom-file-input').nextElementSibling.innerText = imagen_actual;
            
        
       
        document.getElementById("id_imagen").value = "";
    });

// Enable zoom in button
    $(".js-zoom-in").click(function () {
        imagen_recortada.cropper("zoom", 0.1);
    });

// Enable zoom out button
    $(".js-zoom-out").click(function () {
        imagen_recortada.cropper("zoom", -0.1);
    });

    $(".js-crop-and-upload").click(function () {
        var cropData = imagen_recortada.cropper("getData");
        $("#id_x").val(cropData["x"]);
        $("#id_y").val(cropData["y"]);
        $("#id_height").val(cropData["height"]);
        $("#id_width").val(cropData["width"]);
        $("#form_perfil").submit();
       
    });
}