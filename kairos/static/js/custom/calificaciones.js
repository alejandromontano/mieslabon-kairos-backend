function initCalificacionesRecursos(pk_usuario) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_cargar_calificaciones_recursos,
        data: {
            'json_text': JSON.stringify({
                'pk_usuario': pk_usuario
            })
        },
        dataType: "json",
        success: function (data) {
            let calificaciones = data['calificaciones'];
            for(let i=0;i<calificaciones.length;i++){
                let stars = calificaciones[i]['stars'];
                if(stars>0){
                    for(let j=1;j<=stars;j++){
                        let id = '#star'+j+'-recurso-'+calificaciones[i]['pk'];
                        $(id).parent().addClass("activo");
                    }
                }
            }
        }
    });
}

function calificar_recurso(number, pk_recurso, pk_usuario){
    let elem = $('#star'+number+'-recurso-'+pk_recurso);
    if (!elem.parent().hasClass('activo')) data = {'pk_recurso': pk_recurso, 'pk_usuario': pk_usuario, 'star': number};
    else data = {'pk_recurso': pk_recurso,'pk_usuario': pk_usuario, 'star': number-1};
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_calificar_recurso,
        data: {
            'json_text': JSON.stringify(data)
        },
        dataType: "json",
        success: function (data) {
            if (!elem.parent().hasClass('activo'))
                for(let i=1;i<=number;i++){
                    $('#star'+i+'-recurso-'+pk_recurso).parent().addClass("activo");
                }
            else{
                for(let i=number;i<=5;i++){
                    $('#star'+i+'-recurso-'+pk_recurso).parent().removeClass("activo");
                }
            }
        }
    });
}

function initCalificacionesExperiencias(pk_usuario, pk_experiencia = null) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_cargar_calificaciones_experiencias,
        data: {
            'json_text': JSON.stringify({
                'pk_usuario': pk_usuario,
                'pk_experiencia': pk_experiencia
            })
        },
        dataType: "json",
        success: function (data) {
            let calificaciones = data['calificaciones'];
            for(let i=0;i<calificaciones.length;i++){
                let stars = calificaciones[i]['stars'];
                if(stars>0){
                    for(let j=1;j<=stars;j++){
                        let id = '#star'+j+'-experiencia-'+calificaciones[i]['pk'];
                        $(id).parent().addClass("activo");
                    }
                }
            }
        }
    });
}

function calificar_experiencia(number, pk_experiencia, pk_usuario){
    let elem = $('#star'+number+'-experiencia-'+pk_experiencia);
    if (!elem.parent().hasClass('activo')) data = {'pk_experiencia': pk_experiencia, 'pk_usuario': pk_usuario, 'star': number};
    else data = {'pk_experiencia': pk_experiencia,'pk_usuario': pk_usuario, 'star': number-1};
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_calificar_experiencia,
        data: {
            'json_text': JSON.stringify(data)
        },
        dataType: "json",
        success: function (data) {
            if (!elem.parent().hasClass('activo'))
                for(let i=1;i<=number;i++){
                    $('#star'+i+'-experiencia-'+pk_experiencia).parent().addClass("activo");
                }
            else{
                for(let i=number;i<=5;i++){
                    $('#star'+i+'-experiencia-'+pk_experiencia).parent().removeClass("activo");
                }
            }
        }
    });
}
