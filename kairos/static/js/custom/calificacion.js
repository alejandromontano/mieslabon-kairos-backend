function initCalificacionFormulario(pk_usuario, pk_formulario = null) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_cargar_calificacion_formulario,
        data: {
            'json_text': JSON.stringify({
                'pk_usuario': pk_usuario,
                'pk_formulario': pk_formulario
            })
        },
        dataType: "json",
        success: function (data) {
            if (data.calificaciones) {
                let calificaciones = data['calificaciones'];
                for (let i = 0; i < calificaciones.length; i++) {
                    let stars = calificaciones[i]['stars'];
                    if (stars > 0) {
                        for (let j = 1; j <= stars; j++) {
                            let id = '#star' + j + '-formulario-' + calificaciones[i]['pk'];
                            $(id).parent().addClass("activo");
                        }
                    }
                }
            }
        }
    });
}

function calificar_formulario(number, pk_form, pk_usuario) {
    let elem = $('#star' + number + '-formulario-' + pk_form);
    if (!elem.parent().hasClass('activo')) data = {
        'pk_formulario': pk_formulario,
        'pk_usuario': pk_usuario,
        'star': number
    };
    else data = {'pk_formulario': pk_formulario, 'pk_usuario': pk_usuario, 'star': number - 1};
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_calificar_formulario,
        data: {
            'json_text': JSON.stringify(data)
        },
        dataType: "json",
        success: function (data) {
            for (let i = 1; i <= number; i++) {
                $('#star' + i + '-formulario-' + pk_form).addClass("activo");                
            }
            for (let i = number + 1; i <= 5; i++) {
                $('#star' + i + '-formulario-' + pk_form).removeClass("activo");                
            }

            toastr.info('Gracias por tu calificación!', '', {
                "closeButton": true,
                "progressBar": true,
                "showEasing": "swing",
                "extendedTimeOut": 5000,
                "timeOut": 3000,
            });
        }
    });
}

function initCalificacionesContenido(pk_usuario, pk_contenido = null) {

    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_cargar_calificaciones_contenidos,
        data: {
            'json_text': JSON.stringify({
                'pk_usuario': pk_usuario,
                'pk_contenido': pk_contenido
            })
        },
        dataType: "json",
        success: function (data) {
            let calificaciones = data['calificaciones'];
            for (let i = 0; i < calificaciones.length; i++) {
                let stars = calificaciones[i]['stars'];
                let id = '#like' + stars + '-contenido-' + calificaciones[i]['pk'];
                if (stars === 2)
                    $(id).parent().addClass("activo");
                else
                    $(id).parent().addClass("activo");
                const $decision = $("#decision-perfil-" + calificaciones[i]['pk']);
                $decision.attr(
                    "src", "/static/img/realidades/estrella_" + calificaciones[i]['decision'] + ".png"
                );
            }
        }
    });
}

function calificar_contenido(number, pk_contenido, pk_usuario) {
    let elem = $('#like' + number + '-contenido-' + pk_contenido);
    if (!elem.parent().hasClass('activo'))
        data = {'pk_contenido': pk_contenido, 'pk_usuario': pk_usuario, 'star': number};
    else data = {'pk_contenido': pk_contenido, 'pk_usuario': pk_usuario, 'star': -1};
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_calificar_contenido,
        data: {
            'json_text': JSON.stringify(data)
        },
        dataType: "json",
        success: function (data) {
            if (!elem.parent().hasClass('activo') && !elem.parent().hasClass('activo'))
                if (number === 2) {
                    $('#like2-contenido-' + pk_contenido).parent().addClass("activo");
                    $('#like1-contenido-' + pk_contenido).parent().removeClass("activo");
                } else {
                    $('#like1-contenido-' + pk_contenido).parent().addClass("activo");
                    $('#like2-contenido-' + pk_contenido).parent().removeClass("activo");
                }
            else {
                if (number === 2)
                    $('#like2-contenido-' + pk_contenido).parent().removeClass("activo");
                else
                    $('#like1-contenido-' + pk_contenido).parent().removeClass("activo");
            }
        }
    });
}

function visitar_contenido(pk_contenido, pk_usuario) {
    data = {'pk_contenido': pk_contenido, 'pk_usuario': pk_usuario,};
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_visitar_contenido,
        data: {
            'json_text': JSON.stringify(data)
        },
        dataType: "json",
        success: function (data) {

        }
    });
}

function initContenidoVisitados(pk_usuario, pk_contenido = null) {

    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_cargar_visitados,
        data: {
            'json_text': JSON.stringify({
                'pk_usuario': pk_usuario,
                'pk_contenido': pk_contenido
            })
        },
        dataType: "json",
        success: function (data) {
            let calificaciones = data['calificaciones'];

            $('[id^="visitado-contenido-"]').each(function (index) {
                $(this).removeClass("d-none")
            });

            for (let i = 0; i < calificaciones.length; i++) {
                let visitado = calificaciones[i]['visitado'];
                let id = '#visitado-contenido-' + calificaciones[i]['pk'];
                $(id).addClass("d-none");
            }
        }
    });
}

$(".calificacion a").mouseover(function () {
    if (!$(this).parent().hasClass('activo') && !$(this).parent().hasClass('like') && !$(this).parent().hasClass('dislike')) {
        $(this).css("color", "#FFE9A4");
        hoverStars($(this));
    }
});

function hoverStars(elem) {
    if (elem.parent().prev().hasClass('calificacion') && !elem.parent().prev().hasClass('activo') && !elem.parent().prev().hasClass('like') && !elem.parent().prev().hasClass('dislike')) {
        let child = elem.parent().prev().children();
        child.css("color", "#FFE9A4");
        hoverStars(child);
    }
}

$(".calificacion a").mouseout(function () {
    if (!$(this).parent().hasClass('activo') && !$(this).parent().hasClass('like') && !$(this).parent().hasClass('dislike')) {
        $(this).css("color", "#777B6E");
        outStars($(this));
    }
});

function outStars(elem) {
    if (elem.parent().prev().hasClass('calificacion') && !elem.parent().prev().hasClass('activo') && !elem.parent().prev().hasClass('like') && !elem.parent().prev().hasClass('dislike')) {
        let child = elem.parent().prev().children();
        child.css("color", "#777B6E");
        outStars(child);
    }
}

function perfil_favorito(pk_usuario) {
    let pk_favorito = document.getElementById('pk_favorito').value;
    let elem = $('#favorite-perfil-' + pk_favorito);
    data = {'pk_favorito': pk_favorito, 'pk_usuario': pk_usuario};
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url_api_marcar_perfil_favorito,
        data: {
            'json_text': JSON.stringify(data)
        },
        dataType: "json",
        success: function (data) {
            if (elem.parent().hasClass('favorito'))
                elem.parent().removeClass('favorito');
            else {
                $('.favoritos').removeClass('favorito');
                elem.parent().addClass('favorito');
            }
            $("#favorito").modal('hide');
        }
    });
}

function ajax_decision(pk_modelo, url, contenido = "") {
    let json = {
        'pk_modelo': pk_modelo
    };
    const $img = $("#decision-perfil-" + pk_modelo);
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: url,
        data: {
            'json_text': JSON.stringify(json)
        },
        dataType: "json",
        success: function (data) {
            if (data.mensaje && data.mensaje === "limite") {
                $('#tres_decisiones').modal('show');
                return;
            }
            const src = $img.attr("src").split("_");

            if (src[1] == "0.png") {
                $img.attr("src", src[0] + "_1.png");
            } else {
                $img.attr("src", src[0] + "_0.png");
            }

            if (contenido === "contenido") {
                $('#like1-contenido-' + pk_modelo).parent().addClass("activo");
                $('#like2-contenido-' + pk_modelo).parent().removeClass("activo");
            }
        }
    });
}

function perfil_decision(pk_modelo) {
    ajax_decision(pk_modelo, url_api_decision_perfil_deseado)
}

function contenido_decision(pk_modelo) {
    ajax_decision(pk_modelo, url_api_decisiones_contenido, "contenido");
}

function modal_favorito(pk_favorito) {
    document.getElementById('pk_favorito').value = String(pk_favorito);
    $("#favorito").modal('show');
}