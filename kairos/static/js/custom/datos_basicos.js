confirmar_capacitaciones()

if($('#id_formacion_academica').val() === ""){
    $('#id_ultimo_grado').attr('required', false);
    $('#id_ultimo_grado').attr('disabled', true);
    $('#id_ultimo_grado').attr('placeholder', '');
    $('#id_ultimo_grado').val('');
}
if($('#id_capacitaciones').val() === "False"){
    $('#id_jornadas').attr('required', false);
    $('#id_jornadas').attr('placeholder', '');
    $('#id_jornadas').val('');
    $('#id_jornadas').attr('disabled', true);
}

$('.nav-item').css("width",String(100/total_forms)+'%');
window.onload = function() {
    if(document.querySelector('#id_fecha_de_nacimiento') !== null){
        set_fecha_nacimiento_maxima();
        fecha = document.querySelector("label[for='id_fecha_de_nacimiento']");
        fecha.innerHTML = "Fecha de nacimiento*";
    }
};
init_paises();
$('#id_formacion_academica')[0].onchange = function(){
    if($('#id_formacion_academica').val() !== ""){
        $('#id_ultimo_grado').attr('disabled', false);
        $('#id_ultimo_grado').attr('required', true);
        $('#id_ultimo_grado').val('');
        if($('#id_formacion_academica').val() === 'P'){
            $('#id_ultimo_grado').attr('placeholder', 'Grado 0, 1, ..., 5');
            $('#id_ultimo_grado').attr('min', 0).attr('max', 5);
        }
        else if($('#id_formacion_academica').val() === 'B'){
            $('#id_ultimo_grado').attr('placeholder', 'Grado 6, 7, ..., 12');
            $('#id_ultimo_grado').attr('min', 6).attr('max', 12);
        }
        else if($('#id_formacion_academica').val() === 'T'){
            $('#id_ultimo_grado').attr('placeholder', 'Semestre 1, 2, ..., 4');
            $('#id_ultimo_grado').attr('min', 1).attr('max', 4);
        }
        else if($('#id_formacion_academica').val() === 'PF'){
            $('#id_ultimo_grado').attr('placeholder', 'Semetre 1, 2, ..., 10');
            $('#id_ultimo_grado').attr('min', 1).attr('max', 10);
        }
        else if($('#id_formacion_academica').val() === 'M'){
            $('#id_ultimo_grado').attr('placeholder', '16 en adelante');
            $('#id_ultimo_grado').attr('min', 16);
        }
    }
    else{
        $('#id_ultimo_grado').attr('required', false);
        $('#id_ultimo_grado').attr('disabled', true)
        $('#id_ultimo_grado').attr('placeholder', '')
        $('#id_ultimo_grado').val('')
    }
};
$('#id_capacitaciones')[0].onchange = function(){
    if($('#id_capacitaciones').val() === "True"){
        $('#id_jornadas').attr('disabled', false)
    }
    else{
        $('#id_jornadas').attr('disabled', true)
        $('#id_jornadas').attr('required', false);
        $('#id_jornadas').attr('placeholder', '');
        $('#id_jornadas').val(null).trigger("change");
    }
};


function confirmar_capacitaciones(){
    let capacitaciones = $('#id_capacitaciones').val()
    let jornadas = $('#id_jornadas_del_joven')

    if (capacitaciones === 'True'){   
        jornadas.attr('disabled', false);
        jornadas.attr('required', true);               
    }else{
        jornadas.attr('disabled', true);
        jornadas.attr('required', false);               
    }
}


$('#id_capacitaciones')[0].onchange = function(){
    confirmar_capacitaciones()
}