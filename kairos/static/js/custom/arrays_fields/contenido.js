var hide = [ //Agregar un nuevo arreglo por cada modalidad nueva y poner los campos que deben esconderse
    [],
    [],
    ['id_titulo_donde', 'id_continente', 'id_paises', 'id_regiones', 'id_ciudades'],
    [],
    [],
    ['id_titulo_donde', 'id_continente', 'id_paises', 'id_regiones', 'id_ciudades'], // hasta acá son de rutas
    [],
    ['id_titulo_donde', 'id_continente', 'id_paises', 'id_regiones', 'id_ciudades'],
    [],
    [],
    ['id_titulo_donde', 'id_continente', 'id_paises', 'id_regiones', 'id_ciudades'],
    [],
    [],
    [] // hasta acá son de oportunidades
];
var verify = [ //Agregar un nuevo arreglo por cada modalidad nueva y poner los campos que deben validarse
    ['id_continente', 'id_paises','id_regiones', 'id_ciudades', 'id_organizacion', 'id_nombre', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_periodicidad_pago', 'id_observaciones_plazo', 'id_link'],
    ['id_continente', 'id_paises','id_regiones', 'id_ciudades', 'id_organizacion', 'id_nombre', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_periodicidad_pago', 'id_observaciones_plazo', 'id_link'],
    ['id_organizacion', 'id_nombre', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_periodicidad_pago', 'id_observaciones_plazo', 'id_link'],
    ['id_continente', 'id_paises', 'id_regiones', 'id_ciudades', 'id_organizacion', 'id_nombre', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
    ['id_organizacion', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
    ['id_organizacion', 'id_nombre', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
    ['id_continente', 'id_paises', 'id_organizacion', 'id_nombre', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
    ['id_organizacion', 'id_nombre', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
    ['id_continente', 'id_paises', 'id_organizacion', 'id_nombre', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
    ['id_continente','id_organizacion', 'id_nombre', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
    ['id_continente', 'id_organizacion', 'id_nombre', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
    ['id_continente', 'id_paises','id_organizacion', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
    ['id_continente', 'id_paises', 'id_organizacion', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
    ['id_continente', 'id_paises', 'id_organizacion', 'id_caracteristicas', 'id_observaciones', 'id_niveles_conocimiento', 'id_roles', 'id_observaciones_precio', 'id_observaciones_plazo', 'id_link'],
];
var show_all = ['id_titulo_donde', 'id_continente', 'id_paises', 'id_regiones', 'id_ciudades']; // agregar todos los campos que se esconden