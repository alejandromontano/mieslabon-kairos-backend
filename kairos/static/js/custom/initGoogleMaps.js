function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 19,
        center: {
            lat: 3.4842534,
            lng: -76.493965
        },
        mapTypeId: 'roadmap',
        streetViewControl: false,
        fullscreenControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
    });
    gMap = new GoogleMap(token, map, pk_usuario, solo_lectura);
    gMap.init()
}