function cargar_tabla() {
    $(document).ready(function () {
        var ids = organizaciones.split(',')
        $('#tabla').dataTable({
            "serverSide": true,
            "ajax": function (data, callback, settings) {
                var columna_filtro = data.columns[data.order[0].column].data.replace(/\./g, "__");
                var direccion = data.order[0].dir;
                $.get(url_api_listado_usuarios, {
                    id: ids,
                    draw: data.draw,
                    inicio: data.start,
                    limite: data.length,
                    filtro: data.search.value,
                    orden: columna_filtro,
                    direccion: direccion === 'desc' ? '-' : ''
                }, function (res) {
                    callback({
                        recordsTotal: res.total,
                        recordsFiltered: res.total,
                        data: res.data
                    });
                });
            },
            "columns": [
                {"data": "tipo"},
                {"data": "first_name"},
                {"data": "email"},
                {"data": "correo_verificado"},
                {"data": "creado"},
                {"data": "formularios"},
                {"data": "indicativo_telefono"},
                {"data": "telefono"},
                {"data": "is_active"},
            ],
            "columnDefs": [
                {
                    className: "position",
                    targets: [0],
                    "render": function (data, type, row) {

                        return row.tipo;
                    }
                },
                {
                    className: "position",
                    targets: [1],
                    "render": function (data, type, row) {

                        let nombre = row.first_name;
                        let apellido = row.last_name;

                        return nombre + " " + apellido;
                    }
                },
                {
                    className: "position",
                    targets: [2],
                    "render": function (data, type, row) {

                        return row.email;
                    }
                },
                {
                    className: "position",
                    targets: [3],
                    "render": function (data, type, row) {

                        let estado = row.correo_verificado;
                        if (estado == true) {
                            return "<div align='center'> <span class='label label-lime'>VERIFICADO</span> </div>";
                        } else {
                            return "<div align='center'> <span class='label label-secondary'>SIN VERIFICAR</span> </div>";
                        }
                    }
                },
                {
                    className: "position",
                    targets: [4],
                    "render": function (data, type, row) {

                        return row.creado;
                    }
                },
                {
                    orderable: false,
                    className: "position",
                    targets: [5],
                    "render": function (data, type, row) {
                        if (row.formularios) {
                            return "<div align='center'> <span class='label label-lime'>COMPLETADOS</span> </div>";
                        } else {
                            return "<div align='center'> <span class='label label-secondary'>SIN COMPLETAR</span> </div>";
                        }
                    }
                },
                {
                    className: "position",
                    targets: [6],
                    "render": function (data, type, row) {

                        return row.indicativo_telefono === null? '+': "+" + row.indicativo_telefono;
                    }
                },
                {
                    className: "position",
                    targets: [7],
                    "render": function (data, type, row) {

                        return row.telefono;
                    }
                },
                {
                    className: "position",
                    targets: [8],
                    "render": function (data, type, row) {

                        let estado = row.is_active;
                        if (estado == true) {
                            return "<div align='center'> <span class='label label-lime'>ACTIVO</span> </div>";
                        } else {
                            return "<div align='center'> <span class='label label-secondary'>INACTIVO</span> </div>";
                        }
                    }
                },
                {
                    className: "position",
                    targets: [9],
                    "render": function (data, type, row) {
                        var id_ruta = row.id;
                        urlContrasena = urlContrasena.replace(0, id_ruta);
                        urlModificar = urlModificar.replace(0, id_ruta);
                        urlCorreoVerificar = urlCorreoVerificar.replace(0, id_ruta);

                        data = '<div class="row" align="center">' + '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">' + '<a href=' + urlModificar + '><i class="fas fa-pen-alt"></i></a>' + '</div>' +
                            '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">' + '<a href=' + urlContrasena + '><i class="fas fa-sync-alt"></i></a>' + '</div>' +
                            '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">' + '<a href=' + urlCorreoVerificar + '><i class="fas fa-envelope-square"></i></a>' + '</div>' + '</div>'

                        urlContrasena = urlContrasena.replace(id_ruta, 0);
                        urlModificar = urlModificar.replace(id_ruta, 0);
                        urlCorreoVerificar = urlCorreoVerificar.replace(id_ruta, 0);
                        return data
                    }
                },

            ],
            "language": {
                "processing": "Procesando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "decimal": "",
                "infoPostFix": "",
                "thousands": ",",
                "loadingRecords": "Cargando...",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "aria": {
                    "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                buttons: {
                    copyTitle: 'Copiado a portapapeles',
                    copySuccess: {
                        _: '%d lineas copiadas',
                        1: '1 linea copiada'
                    }
                }
            },
            "iDisplayLength": 10,
            "aLengthMenu": [[10], [10]],
        });
    });
}