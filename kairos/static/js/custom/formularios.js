var paginaActual = 1;
var maxPaginas = Math.ceil(totalPreguntas/maxPreguntasPagina);
var ultimaPregunta = maxPreguntasPagina;
var primeraPregunta = 1;
var respondidas_por_pagina = 0;
var totalRespondidas = 0;
var calificado = false;
var tiempo_actual = 0;
var tiempoFinal = 0;
var hora_inicio = 0;
var hora_inicio_string= ''

function iniciarFormulario(limit) {
    $('.formulario_descripcion').hide();
    $('.formulario_preguntas').show();
    $('#siguiente').prop('disabled', true);
    if(releer === 'False'){
        $('#anterior').prop('disabled', true);
    }
    
    initListeners();
    cargar_pagina_preguntas();
    enviar_tiempo_inicio(limit);
}

function transformar_string_en_fecha(string_fecha){
    let fecha_hora = string_fecha.split(" ");

    let anho_mes_dia = fecha_hora[0].split("-"); // fecha_hora[0]: representa la fecha
    let hora_minutos_segundos = fecha_hora[1].split(":"); // fecha_hora[1]: representa la hora
    let segundos = hora_minutos_segundos[2].split(".")[0];

    return new Date(anho_mes_dia[0], (anho_mes_dia[1]-1), anho_mes_dia[2],
        hora_minutos_segundos[0], hora_minutos_segundos[1], segundos);
}

function enviar_tiempo_inicio(limit){
    if (formulario_identificador === 1 || formulario_identificador === '1'){
        $.ajax({
            url: url_formularios,
            type:'GET',
            dataType: 'json',
            success: function(response){
                hora_inicio_string = response['hora_inicio'];                
                
                hora_inicio = new Date(response['hora_inicio'])
                
                tiempoFinal = new Date(response['hora_final'])//tiempoFinal = hora_inicio + parseInt(limit)*60000;  // Ahora + x min finales
                
                fecha_ahora_mismo = new Date(response['fecha_actual']);

                let faltante_aux = tiempoFinal - fecha_ahora_mismo;
                if (faltante_aux > 0){
                    initTimer(limit);
                }else{
                    finalizar_cuestionario();
                }
            },
        })
    }else{
        initTimer(limit);
    }
}

function leer_texto(limit) {
    $('.leer').hide();
    $('.texto').show();
    $('.tiempo_lectura').show();
    tiempoFinal = new Date(new Date().getTime() + parseInt(limit)*60000).getTime(); // Ahora + x min finales
    tiempo_inicial = tiempo_actual;
    var iniciado = false;
    asyncCallCountDownREAD(iniciado);
}

function initTimer(limit){
    if(limit !== null && limit !== '' && limit !== 'None'){
        tiempo_inicial = tiempo_actual;
        asyncCallCountDown();
    }else{       
        seconds = tiempo_actual-(60*Math.floor(tiempo_actual/60));
        minutes = Math.floor(tiempo_actual/60);
        hours = Math.floor(minutes/60);
        asyncCallTimer();
    }
}

function initListeners() {
    for(let i=1; i<=totalPreguntas; i++){        
        if($('#tipo_pregunta_'+String(i)).val() === 'SM'){
            $('select[name=select_'+String(i)+']').on("change", function (e) {
                if($('select[name=select_'+String(i)+']').val() !== ""){
                    revisar_respuestas(primeraPregunta,ultimaPregunta);
                }
            });
        }
        else if($('#tipo_pregunta_'+String(i)).val() === 'E' || $('#tipo_pregunta_'+String(i)).val() === 'SN'){
            $('input[name=radio_'+String(i)+']').change(function() {
                if($('input[name=radio_'+String(i)+']:checked').val()){
                    revisar_respuestas(primeraPregunta,ultimaPregunta);
                }
            });
        }
        else if($('#tipo_pregunta_'+String(i)).val() === 'D48'){            
            $('input[name=radio_'+String(i)+'_1]').change(function() {                
                if($('input[name=radio_'+String(i)+'_1]:checked').val() && $('input[name=radio_'+String(i)+'_2]:checked').val()){                    
                    revisar_respuestas(primeraPregunta,ultimaPregunta);
                }
            });
            $('input[name=radio_'+String(i)+'_2]').change(function() {            
                if($('input[name=radio_'+String(i)+'_1]:checked').val() && $('input[name=radio_'+String(i)+'_2]:checked').val())
                    revisar_respuestas(primeraPregunta,ultimaPregunta);
            });
        }
    }
}

function preguntas_anteriores() {
    if(paginaActual > 1){
        paginaActual = paginaActual - 1;
        $('#siguiente').prop('disabled', false);
        cargar_pagina_preguntas();
    }
    else if(releer === 'True'){
        $('.formulario_preguntas').hide();
        $('.formulario_descripcion').show();
    }
}

function preguntas_siguientes() {
    if(paginaActual < maxPaginas){
        paginaActual = paginaActual + 1;
        $('#anterior').prop('disabled', false);
        cargar_pagina_preguntas();
        guardar_respuestas();
    }else{
        finalizar_cuestionario();
    }
}

function cargar_pagina_preguntas() {
    $('.preguntas').find('.pregunta').hide();
    primeraPregunta = paginaActual*maxPreguntasPagina-(maxPreguntasPagina-1);
    if(maxPreguntasPagina*paginaActual > totalPreguntas)
        ultimaPregunta = totalPreguntas;
    else
        ultimaPregunta = maxPreguntasPagina*paginaActual;
    for(let i=primeraPregunta; i<=ultimaPregunta; i++){
        $('#pregunta_'+String(i)).show();
    }
    if(paginaActual > 1)
        $('#anterior').prop('disabled',false);
    revisar_respuestas(primeraPregunta, ultimaPregunta);
}

function revisar_respuestas(pregunta_inicial, pregunta_final) {
    respondidas_por_pagina = 0;
    for(let i=pregunta_inicial; i<=pregunta_final; i++){
        if($('#tipo_pregunta_'+String(i)).val() === 'SM'){
            if($('select[name=select_'+String(i)+']').val() !== ""){
                respondidas_por_pagina = respondidas_por_pagina + 1;
            }
        }
        else if($('#tipo_pregunta_'+String(i)).val() === 'E' || $('#tipo_pregunta_'+String(i)).val() === 'SN'){
            if($('input[name=radio_'+String(i)+']:checked').val()){
                respondidas_por_pagina = respondidas_por_pagina + 1;
            }
        }
        else if($('#tipo_pregunta_'+String(i)).val() === 'D48'){
            if($('input[name=radio_'+String(i)+'_1]:checked').val() && $('input[name=radio_'+String(i)+'_2]:checked').val()){
                respondidas_por_pagina = respondidas_por_pagina + 1;
            }
        }
    }
    if(respondidas_por_pagina === (pregunta_final-pregunta_inicial+1))
        $('#siguiente').prop('disabled',false);
    else
        $('#siguiente').prop('disabled',true);
}

function guardar_respuestas() {
    let respuestas = 0;
    let respuestasJoven = [];
    for(let i=1; i<=totalPreguntas; i++){
        let res = null;
        if($('#tipo_pregunta_'+String(i)).val() === 'SM'){
            if($('select[name=select_'+String(i)+']').val() !== ""){
                res = parseInt($('select[name=select_'+String(i)+']').val());
                let pk_pregunta = $('select[name=select_'+String(i)+']').attr('id');
                respuestasJoven.push({'pk_pregunta': pk_pregunta, 'respuesta': res});
                respuestas = respuestas + 1;
            }
        }
        else if($('#tipo_pregunta_'+String(i)).val() === 'E' || $('#tipo_pregunta_'+String(i)).val() === 'SN'){
            if($('input[name=radio_'+String(i)+']:checked').val()){
                res = parseInt($('input[name=radio_'+String(i)+']:checked').val());
                let pk_pregunta = $('input[name=radio_'+String(i)+']:checked').attr('id').split('_')[0];
                respuestasJoven.push({'pk_pregunta': pk_pregunta, 'respuesta': res});
                respuestas = respuestas + 1;
            }
        }
        else if($('#tipo_pregunta_'+String(i)).val() === 'D48'){
            if($('input[name=radio_'+String(i)+'_1]:checked').val() && $('input[name=radio_'+String(i)+'_2]:checked').val()){
                res = [
                    parseInt($('input[name=radio_'+String(i)+'_1]:checked').val()),
                    parseInt($('input[name=radio_'+String(i)+'_2]:checked').val())
                ];
                let pk_pregunta = $('input[name=radio_'+String(i)+'_1]:checked').attr('id').split('_')[0];
                respuestasJoven.push({'pk_pregunta': pk_pregunta, 'respuesta': res});
                respuestas = respuestas + 1;
            }
        }
    }
    if(respuestas > 0){
        $.ajax({
            type: "POST",
            headers: {
                "X-CSRFToken": token,
                contentType: "application/json"
            },
            url: '/app/cuestionarios/api/formulario/guardar-respuestas',
            data: {
                'json_text': JSON.stringify({
                    'pk_joven': pk_joven,
                    'pk_formulario': pk_formulario,
                    'respuestas': respuestasJoven,
                    'tiempo_gastado': tiempo_actual+4,
                    'hora_inicio': hora_inicio_string,
                    'formulario_identificador': formulario_identificador,
                })
            },
            dataType: "json",
            success: function (data) {
                if(data.result === true){ // faltan preguntas por responder
                    toastr.success('Respuestas guardadas!', '', {
                        "closeButton": true,
                        "progressBar": true,
                        "showEasing": "swing",
                        "extendedTimeOut": 1000,
                        "timeOut": 1000,
                    });

                    if(data.finalizo === true){
                        toastr.success('El reto ha finalizado', '', {
                            "closeButton": true,
                            "progressBar": true,
                            "showEasing": "swing",
                            "extendedTimeOut": 2500,
                            "timeOut": 2500,
                        });
                    }
                }else if(data.result === false){ // no faltan preguntas por responder
                    toastr.info('El formulario ya ha sido completado anteriormente!!', '', {
                        "closeButton": true,
                        "progressBar": true,
                        "showEasing": "swing",
                        "extendedTimeOut": 5000,
                        "timeOut": 3000,
                    });
                }
            },
            error: function () {
                toastr.error('Ha ocurrido un error, por favor intente de nuevo!', '', {
                    "closeButton": true,
                    "progressBar": true,
                    "showEasing": "swing",
                    "extendedTimeOut": 5000,
                    "timeOut": 3000,
                });
            }
        });
    }else{
        $.ajax({
            type: "POST",
            headers: {
                "X-CSRFToken": token,
                contentType: "application/json"
            },
            url: '/app/cuestionarios/api/formulario/guardar-estado-lectura',
            data: {
                'json_text': JSON.stringify({
                    'pk_joven': pk_joven,
                    'pk_formulario': pk_formulario
                })
            },
            dataType: "json",
            success: function (data) {

            }
        });
    }
}

function cargar_respuestas_preguntas(pk_joven, pk_formulario) {
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRFToken": token,
            contentType: "application/json"
        },
        url: '/app/cuestionarios/api/formulario/data-preguntas',
        data: {
            'json_text': JSON.stringify({
                'pk_joven': pk_joven,
                'pk_formulario': pk_formulario
            })
        },
        dataType: "json",
        success: function (data) {
            let pregunta_final = -1;
            let respondidas = 0;           
            if(data.info){ // Hay respuestas
                let info_preguntas = data['info'];
                for(let i=0;i<info_preguntas.length;i++) { //recorre cada pregunta
                    let tipo = info_preguntas[i]['tipo'];
                    if (info_preguntas[i].respuesta !== null) {
                        respondidas = respondidas + 1;
                        let pk_pregunta = info_preguntas[i]['pk'];
                        let respuesta = info_preguntas[i]['respuesta'];
                        if(info_preguntas[i]['orden'] > pregunta_final){
                            pregunta_final = info_preguntas[i]['orden'];
                        }
                        if(tipo === "SM"){
                            $('#'+pk_pregunta).val(String(respuesta)).trigger('change');
                        }
                        else if(tipo === "E" || tipo === "SN"){
                            $('#'+pk_pregunta+'_'+String(respuesta)).prop('checked',true);
                        }
                        else if(tipo === "D48"){
                            let superior = '#'+String(pk_pregunta)+'_superior_'+String(respuesta[0]);
                            let inferior = '#'+String(pk_pregunta)+'_inferior_'+String(respuesta[1]);
                            $(superior).prop('checked',true);
                            $(inferior).prop('checked',true);
                        }
                    }
                }
                if(respondidas === totalPreguntas){
                    mostrar_solo_calificacion();
                }else{
                    if(pregunta_final > -1){
                        paginaActual = Math.ceil(pregunta_final/maxPreguntasPagina);
                        tiempo_actual = parseInt(data.time);
                    }
                    else if(data.read){
                        paginaActual = 1;
                        tiempo_actual = 0;
                    }
                    iniciarFormulario(data.limit);
                }
            }
        }
    });
}