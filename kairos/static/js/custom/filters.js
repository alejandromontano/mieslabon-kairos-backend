$(".filtro").click(function() {
    $(".filtro")
        .removeClass("btn-orange")
        .removeClass("btn-outline-orange")
        .removeClass("active")
        .addClass("btn-outline-orange");
    $(this)
        .removeClass("btn-outline-orange")
        .removeClass("active")
        .addClass("btn-orange")
        .addClass("active");
});

$(".filtro:eq(0)").addClass('btn-orange');

var $filtro = $(".filtro");
var $contenido = $(".contenido");

$(".contenido")
    .hide()
    .first()
    .show();
// Turn user's choice into a filter
$filtro.on("click", function(e) {
    //       var $chosenButton = $(e.target);
    var $tipo = $(this).data("target");
    $contenido.find('.card-contenido').hide();
    if($tipo === 'all'){
        $contenido
            .find('.card-contenido').fadeIn();
    }
    else {
        $contenido
            .find("." + $tipo).fadeIn();
    }
});