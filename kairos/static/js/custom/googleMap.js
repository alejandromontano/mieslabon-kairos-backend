function GoogleMap(token, mapa, pk_usuario, solo_lectura) {
    var googleMap = this;
    this.map = mapa;
    this.usuario = pk_usuario;
    this.token = token;
    this.solo_lectura = solo_lectura;
    this.marker_direccion = null;
    this.DIRECCION = "DIRECCION";
    this.geocoder = null;
    this.infowindow = null;
    this.buscador = document.getElementById('pac-input');
    this.searchBox = null;

    this.init = function() {
        googleMap.geocoder = new google.maps.Geocoder();
        googleMap.infowindow = new google.maps.InfoWindow();
        googleMap.init_buscador();
        googleMap.cargar_direccion();
    };

    this.cargar_direccion = function() {
        $.ajax({
            type: "POST",
            headers: {
                "X-CSRFToken": googleMap.token,
                contentType: "application/json"
            },
            url: url_api_cargar_direccion,
            data: {
                'json_text': JSON.stringify({
                    'pk_usuario': googleMap.usuario
                })
            },
            dataType: "json",
            success: googleMap.construir_mapa
        });
    };

    this.construir_mapa = function(data) {
        let features = data["features"];
        for (let i = 0; i < features.length; i++) {
            let feature = features[i];
            let info = feature["properties"]["info"];
            let tipo = info["tipo"];
            let direccion = info["direccion"];
            let color = info["color"];
            let geometry = feature["geometry"];
            if (tipo === googleMap.DIRECCION) {
                if (geometry["coordinates"] == null) {
                    centro = info["centro"];
                    position = new google.maps.LatLng(centro[0], centro[1]);
                } else
                    position = new google.maps.LatLng(geometry["coordinates"][0], geometry["coordinates"][1]);
                let icono = {
                    url: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|' + color, //sin numeral
                    scaledSize: new google.maps.Size(25, 35),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(10, 30),
                    labelOrigin: new google.maps.Point(10, 12)
                };
                googleMap.marker_direccion = new google.maps.Marker({
                    position: position,
                    icon: icono,
                    map: googleMap.map,
                    clickable: false,
                    optimized: false,
                    draggable: false,
                });
                googleMap.map.setCenter(position);
                googleMap.map.addListener('rightclick', googleMap.centrar_marker);
                googleMap.map.addListener('drag',googleMap.centrar_marker);
                googleMap.map.addListener('center_changed',googleMap.centrar_marker);
                googleMap.map.addListener('zoom_changed',googleMap.centrar_marker);
                googleMap.infowindow.setContent("<span>" + direccion + "</span>");
                googleMap.marker_direccion.addListener("mouseover", function () {
                    googleMap.infowindow.open(googleMap.map, googleMap.marker_direccion);
                });
                googleMap.marker_direccion.addListener("dragend", function () {
                    googleMap.geocoder.geocode({
                        location: this.position
                    }, function(results, status) {
                        if (status == "OK") {
                            googleMap.infowindow.setContent("<span>" + results[0].formatted_address + "</span>");
                        }
                    });
                });
                if (!googleMap.solo_lectura) {
                    googleMap.marker_direccion['info'] = {
                        'tipo': googleMap.DIRECCION,
                        'estado_objeto': 'SIN_MODIFICAR'
                    };
                    googleMap.marker_direccion.addListener('dragend', function() {
                        googleMap.marker_direccion['info']['estado_objeto'] = 'MODIFICADO';
                        existe_cambios = true;
                    });
                }
            } else {
                console.log("ERROR: El tipo debe estar asignado a algún valor!")
            }
        }
    };

    this.guardar_direccion = function() { // Solo se usa en el perfil del usuario (definir url)
        if (googleMap.marker_direccion == null) {
            toastr.error('No se ha definido una ubicación!', '', {
                "closeButton": true,
                "progressBar": true,
                "showEasing": "swing",
                "extendedTimeOut": 5000,
                "timeOut": 3000,
            });
            return
        }
        //Colección de la dirección
        let feature = {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    googleMap.marker_direccion.getPosition().lat(),
                    googleMap.marker_direccion.getPosition().lng()
                ]
            },
            "properties": {
                "info": {
                    "tipo": 'DIRECCION',
                    "estado_objeto": googleMap.marker_direccion["info"]["estado_objeto"]
                }
            }
        };
        $.ajax({
            type: "POST",
            headers: {
                "X-CSRFToken": googleMap.token,
                contentType: "application/json"
            },
            url: url_api_guardar_direccion,
            data: {
                'json_text': JSON.stringify({
                    "pk_usuario": googleMap.usuario,
                    "feature": feature
                })
            },
            dataType: "json",
            success: function (data) {
                document.getElementById('id_direccion').value = data['direccion'];
                toastr.success('La ubicación se guardó correctamente!', '', {
                    "closeButton": true,
                    "progressBar": true,
                    "showEasing": "swing",
                    "extendedTimeOut": 5000,
                    "timeOut": 3000,
                })
            },
            error: function (data) {
                toastr.error('Ha ocurrido un error al guardar la ubicación. Por favor inténtelo de nuevo.', '', {
                    "closeButton": true,
                    "progressBar": true,
                    "showEasing": "swing",
                    "extendedTimeOut": 5000,
                    "timeOut": 3000,
                });
            }
        });
    };

    this.init_buscador = function () {
        googleMap.searchBox = new google.maps.places.SearchBox(googleMap.buscador);
        googleMap.map.addListener('bounds_changed', function () {
            googleMap.searchBox.setBounds(googleMap.map.getBounds());
            googleMap.map.addListener('rightclick', googleMap.ubicar_marker);
        });
        markers_search = [];

        googleMap.searchBox.addListener('places_changed', function () {
            places = googleMap.searchBox.getPlaces();
            if (places.length === 0) {
                return;
            }
            if (!places[0].geometry) {
                console.log("No se encuentran las coordenadas del lugar.");
                return;
            }
            if (!googleMap.solo_lectura) {
                googleMap.geocoder.geocode({
                    location: places[0].geometry.location
                }, function (results, status) {
                    if (status == "OK") {
                        googleMap.infowindow.setContent("<span>" + results[0].formatted_address + "</span>");
                    }
                });
                googleMap.marker_direccion.setPosition(places[0].geometry.location);
                googleMap.map.setCenter(places[0].geometry.location);
                googleMap.marker_direccion['info']['estado_objeto'] = 'MODIFICADO';
                existe_cambios = true;
            }
            googleMap.map.addListener('rightclick', googleMap.ubicar_marker);
        });

        document.getElementById("btn_buscar").onclick = function () {
            google.maps.event.trigger(googleMap.buscador, 'focus', {});
            google.maps.event.trigger(googleMap.buscador, 'keydown', {
                keyCode: 13
            });
        };
    };

    this.centrar_marker = function () {
        googleMap.geocoder.geocode({
            location: map.getCenter()
        }, function (results, status) {
            if (status == "OK") {
                googleMap.infowindow.setContent("<span>" + results[0].formatted_address + "</span>");
            }
        });
        googleMap.marker_direccion.setPosition(map.getCenter());
        googleMap.marker_direccion['info']['estado_objeto'] = 'MODIFICADO';
        existe_cambios = true;
    }

    this.ubicar_marker = function(event) {
        if(!googleMap.solo_lectura) {
            googleMap.geocoder.geocode({
                location: event.latLng
            }, function(results, status) {
                if (status == "OK") {
                    googleMap.infowindow.setContent("<span>" + results[0].formatted_address + "</span>");
                }
            });
            googleMap.marker_direccion.setPosition(event.latLng);
            googleMap.marker_direccion['info']['estado_objeto'] = 'MODIFICADO';
            existe_cambios = true;
        }
    }
}