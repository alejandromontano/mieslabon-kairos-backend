var tribus = ['cuisana', 'moterra', 'ponor', 'vincularem', 'brianimus', 'gronor', 'centeros', 'sersas'];
var modalidad = parseInt($('#id_modalidad').val()) - 1;
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate

$('#id_modalidad')[0].onchange = function(){
	modalidad = parseInt($('#id_modalidad').val()) - 1;
	for(let i=0; i<show_all.length; i++){
		$('#'+show_all[i]).parent().show();
	}
	let esconder = hide[modalidad];
	for(let i=0; i<esconder.length; i++){
		$('#'+esconder[i]).parent().hide();
	}
};

$(".next").click(function(){
	current_fs = $(this).parent().parent().parent();
	if(validateForm(current_fs[0])){
		next_fs = current_fs.next();
		current_fs.hide();
		next_fs.show();
	}
});

$(".previous").click(function(){
	current_fs = $(this).parent().parent().parent();
	previous_fs = current_fs.prev();

	current_fs.hide();
	previous_fs.show();
});

$(".submit").click(function(){
	return false;
})

function validateForm(fs) {
	var y, i, valid = true;
	y = fs.getElementsByTagName("input");
	// A loop that checks every input field in the current fielset
	for (i = 0; i < y.length; i++) {				
		if (verify[modalidad].includes(String(y[i].id))){
			if(y[i].value === ""){
				y[i].className += " is-invalid";
				valid = false;
			}
			else{
				$(y[i]).removeClass("is-invalid");
			}
		}
	}
	y = fs.getElementsByTagName("textarea");
	// A loop that checks every input field in the current fielset
	for (i = 0; i < y.length; i++) {
		if (verify[modalidad].includes(String(y[i].id))){
			if (y[i].value === "") {
				y[i].className += " is-invalid";
				valid = false;
			}
			else{
				$(y[i]).removeClass("is-invalid");
			}
		}
	}
	y = fs.getElementsByTagName("select");
	for (i = 0; i < y.length; i++) {
		if (verify[modalidad].includes(String(y[i].id))){
			if (y[i].value === "") {
				y[i].nextElementSibling.className += " is-invalid";
				valid = false;
			}
			else{
				$(y[i].nextElementSibling).removeClass("is-invalid");
			}
		}
	}
	return valid;
}

function init_paises() {
	var pais = document.getElementById("id_paises");
	pais.onchange = function(){
		$('#id_regiones').empty();
		$('#id_ciudades').empty();
		obtener_regiones(pais.value);
	};
	var region = document.getElementById("id_regiones");
	region.onchange = function(){
		$('#id_ciudades').empty();
		obtener_ciudades(region.value);
	};
}

function obtener_regiones(nombre_pais){
	$.ajax({
		type: "GET",
		headers: {
			"X-CSRFToken": token,
			contentType: "application/json"
		},
		data: {
			'nombre_pais':nombre_pais
		},
		url: '/app/contenido/api/obtener-regiones',
		dataType: "json",
		success: function (data) {
			info = data.info;
			for(let i=0; i<info.length; i++){
				let elemento = $('#id_'+info[i]['elemento']);
				for(let j=0; j<info[i]['opciones'].length; j++){
					let newOption = new Option(info[i]['opciones'][j], info[i]['opciones'][j], false, false);
					elemento[0].append(newOption);
				}
			}
		}
	});
}


function obtener_ciudades(nombre_region){
	$.ajax({
		type: "GET",
		headers: {
			"X-CSRFToken": token,
			contentType: "application/json"
		},
		data: {
			'nombre_region':nombre_region
		},
		url: '/app/contenido/api/obtener-ciudades',
		dataType: "json",
		success: function (data) {
			info = data.info;
			for(let i=0; i<info.length; i++){
				let elemento = $('#id_'+info[i]['elemento']);
				for(let j=0; j<info[i]['opciones'].length; j++){
					let newOption = new Option(info[i]['opciones'][j], info[i]['opciones'][j], false, false);
					elemento[0].append(newOption);
				}
			}
		}
	});
}

function init_modalidad() {
	if(!$('#id_modalidad').val() === ''){
		modalidad = parseInt($('#id_modalidad').val()) - 1;
		for(let i=0; i<show_all.length; i++){
			$('#'+show_all[i]).parent().show();
		}
		let esconder = hide[modalidad];
		for(let i=0; i<esconder.length; i++){
			$('#'+esconder[i]).parent().hide();
		}
	}
}

function init_data() {
	precio = document.getElementById('id_precio');
	precio.setAttribute('min',0.0);	
	iconos_select2();
}

function modify_data() {
	precio = document.getElementById('id_precio');
	precio.setAttribute('min',0.0);
	iconos_select2();	
}

function fill_choices(){
	$.ajax({
		type: "GET",
		headers: {
			"X-CSRFToken": token,
			contentType: "application/json"
		},
		url: '/app/contenido/api/obtener-choices',
		dataType: "json",
		success: function (data) {
			info = data.info;
			for(let i=0; i<info.length; i++){
				let elemento = $('#id_'+info[i]['elemento']);
				for(let j=0; j<info[i]['opciones'].length; j++){
					let newOption = new Option(info[i]['opciones'][j], info[i]['opciones'][j], false, false);
					elemento[0].append(newOption);
				}
			}
		},
		error: function (data) {console.log(data)}
	});
}

function init_tags() {
	$("#id_organizacion").select2({
		tags: true
	});
	$("#id_paises").select2({
		tags: true
	});
	$("#id_regiones").select2({
		tags: true
	});
	$("#id_ciudades").select2({
		tags: true
	});
}

function iconos_select2() {
	$("#id_niveles_conocimiento").select2({
		templateResult: formatStateNiveles,
		templateSelection: formatStateNiveles,
	});
	$("#id_roles").select2({
		templateResult: formatStateRoles,
		templateSelection: formatStateRoles,
	});
	function formatStateRoles (state) {
		if (!state.id) {
			return state.text;
		}
		nombre = state.text;
		url = '/static/img/roles/'+String(state.id)+'_1.png';
		var $state = $(
			'<span><img style="height:28px; width:28px" src="'+ url +'" class="img-flag" /> ' + nombre + '</span>'
		);
		return $state;
	}
	function formatStateNiveles (state) {
		if (!state.id) {
			return state.text;
		}
		nombre = state.text;
		url = '/static/img/niveles_conocimiento/'+state.text+'_1.png';
		var $state = $(
			'<span><img style="height:28px; width:28px" src="'+ url +'" class="img-flag" /> ' + nombre + '</span>'
		);
		return $state;
	}
}
