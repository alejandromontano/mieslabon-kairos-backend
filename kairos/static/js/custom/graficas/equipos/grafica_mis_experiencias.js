am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create("chartdiv", am4charts.XYChart);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.data = rendimiento_por_asignatura;

chart.colors.step = 2;
chart.padding(30, 30, 10, 30);
chart.legend = new am4charts.Legend();
chart.legend.maxWidth = undefined;
chart.legend.scrollable = true;
chart.legend.dy = 20

var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "category";
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.renderer.minGridDistance = 30;
categoryAxis.title.dy = 15
categoryAxis.title.text = 'Asignaturas'

categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
    if (target.dataItem && target.dataItem.index & 2 == 2) {
      return dy + 15;
    }
    return dy;
  });

var label = categoryAxis.renderer.labels.template;
label.truncate = true;
label.maxWidth = 200;
label.tooltipText = "{category}";

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.min = 0;
valueAxis.max = 100;
valueAxis.strictMinMax = true;
valueAxis.calculateTotals = true;
valueAxis.renderer.minWidth = 50;
valueAxis.title.text = 'Porcentaje de Jóvenes'


var series1 = chart.series.push(new am4charts.ColumnSeries());
series1.columns.template.width = am4core.percent(80);
series1.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series1.name = "Ese curso nunca lo he visto";
series1.dataFields.categoryX = "category";
series1.dataFields.valueY = "Ese curso nunca lo he visto";
series1.dataFields.valueYShow = "totalPercent";
series1.dataItems.template.locations.categoryX = 0.5;
series1.stacked = true;
series1.tooltip.pointerOrientation = "vertical";

var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
bullet1.interactionsEnabled = false;
bullet1.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet1.label.fill = am4core.color("#ffffff");
bullet1.locationY = 0.5;

var series2 = chart.series.push(new am4charts.ColumnSeries());
series2.columns.template.width = am4core.percent(80);
series2.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series2.name = "Suelo tener dificultades, no es mi área fuerte";
series2.dataFields.categoryX = "category";
series2.dataFields.valueY = "Suelo tener dificultades, no es mi área fuerte";
series2.dataFields.valueYShow = "totalPercent";
series2.dataItems.template.locations.categoryX = 0.5;
series2.stacked = true;
series2.tooltip.pointerOrientation = "vertical";

var bullet2 = series2.bullets.push(new am4charts.LabelBullet());
bullet2.interactionsEnabled = false;
bullet2.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet2.locationY = 0.5;
bullet2.label.fill = am4core.color("#ffffff");

var series3 = chart.series.push(new am4charts.ColumnSeries());
series3.columns.template.width = am4core.percent(80);
series3.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series3.name = "Normal, logro aprobar";
series3.dataFields.categoryX = "category";
series3.dataFields.valueY = "Normal, logro aprobar";
series3.dataFields.valueYShow = "totalPercent";
series3.dataItems.template.locations.categoryX = 0.5;
series3.stacked = true;
series3.tooltip.pointerOrientation = "vertical";

var bullet3 = series3.bullets.push(new am4charts.LabelBullet());
bullet3.interactionsEnabled = false;
bullet3.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet3.locationY = 0.5;
bullet3.label.fill = am4core.color("#ffffff");

var series4 = chart.series.push(new am4charts.ColumnSeries());
series4.columns.template.width = am4core.percent(80);
series4.columns.template.tooltipText =
"{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series4.name = "Me va bien, aun cuando no es lo que más me gusta";
series4.dataFields.categoryX = "category";
series4.dataFields.valueY = "Me va bien, aun cuando no es lo que más me gusta";
series4.dataFields.valueYShow = "totalPercent";
series4.dataItems.template.locations.categoryX = 0.5;
series4.stacked = true;
series4.tooltip.pointerOrientation = "vertical";

var bullet4 = series4.bullets.push(new am4charts.LabelBullet());
bullet4.interactionsEnabled = false;
bullet4.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet4.locationY = 0.5;
bullet4.label.fill = am4core.color("#ffffff");

var series5 = chart.series.push(new am4charts.ColumnSeries());
series5.columns.template.width = am4core.percent(80);
series5.columns.template.tooltipText =
"{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series5.name = "Soy muy bueno, se me hace fácil y me gusta";
series5.dataFields.categoryX = "category";
series5.dataFields.valueY = "Soy muy bueno, se me hace fácil y me gusta";
series5.dataFields.valueYShow = "totalPercent";
series5.dataItems.template.locations.categoryX = 0.5;
series5.stacked = true;
series5.tooltip.pointerOrientation = "vertical";

var bullet5 = series5.bullets.push(new am4charts.LabelBullet());
bullet5.interactionsEnabled = false;
bullet5.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet5.locationY = 0.5;
bullet5.label.fill = am4core.color("#ffffff");

var series6 = chart.series.push(new am4charts.ColumnSeries());
series6.columns.template.width = am4core.percent(80);
series6.columns.template.tooltipText =
"{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series6.name = "Sin clasificar";
series6.dataFields.categoryX = "category";
series6.dataFields.valueY = "Sin clasificar";
series6.dataFields.valueYShow = "totalPercent";
series6.dataItems.template.locations.categoryX = 0.5;
series6.stacked = true;
series6.tooltip.pointerOrientation = "vertical";

var bullet6 = series6.bullets.push(new am4charts.LabelBullet());
bullet6.interactionsEnabled = false;
bullet6.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet6.locationY = 0.5;
bullet6.label.fill = am4core.color("#ffffff");


chart.scrollbarX = new am4core.Scrollbar();

}); // end am4core.ready()
