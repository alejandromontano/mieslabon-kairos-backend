am4core.ready(function() {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end
    
    // Create chart instance
    var chart = am4core.create("piechart1", am4charts.PieChart);
    
    // Add data
    chart.data = estilos_de_liderazgo
    
    // Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "cantidad";
    pieSeries.dataFields.category = "category";
    pieSeries.slices.template.stroke = am4core.color("#fff");
    pieSeries.slices.template.strokeOpacity = 1;
    
    // This creates initial animation
    pieSeries.hiddenState.properties.opacity = 1;
    pieSeries.hiddenState.properties.endAngle = -90;
    pieSeries.hiddenState.properties.startAngle = -90;
    
    chart.hiddenState.properties.radius = am4core.percent(0);
   
    chart.responsive.enabled = true;
    chart.responsive.useDefault = false;
    chart.resizable = true;
    chart.legend = new am4charts.Legend();
    chart.legend.maxWidth = undefined;

    pieSeries.events.on("datavalidated", function(ev) {
        ev.target.slices.each(function(slice) {
          if (slice.dataItem.values.value.percent == 0) {
            slice.dataItem.hide();
          }
        });
      });
      
    chart.responsive.rules.push({
        relevant: function(target) {
            if (target.pixelWidth <= 720) {
                return true;
            }
            return false;
        },
        state: function(target, stateId) {
            if (target instanceof am4charts.PieSeries) {
                pieSeries.ticks.template.disabled = true;
                pieSeries.alignLabels = false;
                pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";
                pieSeries.labels.template.radius = am4core.percent(-40);
                pieSeries.labels.template.fill = am4core.color("white");

                }
            return null;
        }
    });

    }); // end am4core.ready()
    