function lookUpGrade(lookupScore, grades) {
    // Only change code below this line
    for (var i = 0; i < grades.length; i++) {
        if (
            grades[i].lowScore < lookupScore &&
            grades[i].highScore >= lookupScore
        ) {
            return grades[i];
        }
    }
    return null;
}

function dibujar_tacometro(score, min, max, id, rango) {
    let data = {
        score: score,
        gradingData: [
            {
                title: "Bajo",
                color: "#ee1f25",
                lowScore: rango.bajo.low,
                highScore: rango.bajo.high
            },
            {
                title: "Medio",
                color: "#f3eb0c",
                lowScore: rango.medio.low,
                highScore: rango.medio.high
            },
            {
                title: "Alto",
                color: "#0f9747",
                lowScore: rango.alto.low,
                highScore: rango.alto.high
            },
        ]
    };
    // create chart
    let chart = am4core.create(id, am4charts.GaugeChart);
    chart.hiddenState.properties.opacity = 0;
    chart.fontSize = 5;
    chart.innerRadius = am4core.percent(80);
    chart.resizable = true;
    chart.responsive.enabled = true;
    chart.responsive.useDefault = false;

    chart.responsive.rules.push({
        relevant: function (target) {
            if (target.pixelWidth <= 1100) {
                return true
            }
            return false;
        },
        state: function (target, stateId) {
            if (target instanceof am4charts.Chart) {
                var state = target.states.create(stateId);
                state.properties.fontSize = (target.pixelWidth / 60)
                return state;
            }
            return null;
        }
    });

    /**
     * Normal axis
     */

    let axis = chart.xAxes.push(new am4charts.ValueAxis());
    axis.min = min;
    axis.max = max;
    axis.strictMinMax = true;
    axis.renderer.radius = am4core.percent(80);
    axis.renderer.inside = true;
    axis.renderer.line.strokeOpacity = 0.1;
    axis.renderer.ticks.template.disabled = false;
    axis.renderer.ticks.template.strokeOpacity = 1;
    axis.renderer.ticks.template.strokeWidth = 0.5;
    axis.renderer.ticks.template.length = 5;
    axis.renderer.grid.template.disabled = true;
    axis.renderer.labels.template.radius = am4core.percent(15);
    axis.renderer.labels.template.fontSize = "0.9em";

    /**
     * Axis for ranges
     */

    let axis2 = chart.xAxes.push(new am4charts.ValueAxis());
    axis2.min = min;
    axis2.max = max;
    axis2.strictMinMax = true;
    axis2.renderer.labels.template.disabled = true;
    axis2.renderer.ticks.template.disabled = true;
    axis2.renderer.grid.template.disabled = false;
    axis2.renderer.grid.template.opacity = 0.5;
    axis2.renderer.labels.template.bent = true;
    axis2.renderer.labels.template.fill = am4core.color("#000");
    axis2.renderer.labels.template.fontWeight = "bold";
    axis2.renderer.labels.template.fillOpacity = 0.3;


    /**
     Ranges
     */

    for (let grading of data.gradingData) {
        let range = axis2.axisRanges.create();
        range.axisFill.fill = am4core.color(grading.color);
        range.axisFill.fillOpacity = 0.8;
        range.axisFill.zIndex = -1;
        range.value = grading.lowScore > min ? grading.lowScore : min;
        range.endValue = grading.highScore < max ? grading.highScore : max;
        range.grid.strokeOpacity = 0;
        range.stroke = am4core.color(grading.color).lighten(-0.1);
        range.label.inside = true;
        range.label.text = grading.title.toUpperCase();
        range.label.inside = true;
        range.label.location = 0.5;
        range.label.inside = true;
        range.label.radius = am4core.percent(10);
        range.label.paddingBottom = -5; // ~half font size
        range.label.fontSize = "0.9em";
    }

    let matchingGrade = lookUpGrade(data.score, data.gradingData);

    /**
     * Label 1
     */

    let label = chart.radarContainer.createChild(am4core.Label);
    label.isMeasured = false;
    label.fontSize = "4em";
    label.x = am4core.percent(50);
    label.paddingBottom = 15;
    label.horizontalCenter = "middle";
    label.verticalCenter = "bottom";
    //label.dataItem = data;
    label.text = data.score.toFixed(1);
    //label.text = "{score}";
    label.fill = am4core.color(matchingGrade.color);

    /**
     * Label 2
     */

    let label2 = chart.radarContainer.createChild(am4core.Label);
    label2.isMeasured = false;
    label2.fontSize = "2em";
    label2.horizontalCenter = "middle";
    label2.verticalCenter = "bottom";
    label2.text = matchingGrade.title.toUpperCase();
    label2.fill = am4core.color(matchingGrade.color);


    /**
     * Hand
     */

    let hand = chart.hands.push(new am4charts.ClockHand());
    hand.axis = axis2;
    hand.innerRadius = am4core.percent(55);
    hand.startWidth = 8;
    hand.pin.disabled = true;
    hand.value = data.score;
    hand.fill = am4core.color("#444");
    hand.stroke = am4core.color("#000");

    hand.events.on("positionchanged", function () {
        label.text = axis2.positionToValue(hand.currentPosition).toFixed(1);
        let value2 = axis.positionToValue(hand.currentPosition);
        let matchingGrade = lookUpGrade(axis.positionToValue(hand.currentPosition), data.gradingData);
        label2.text = matchingGrade.title.toUpperCase();
        label2.fill = am4core.color(matchingGrade.color);
        label2.stroke = am4core.color(matchingGrade.color);
        label.fill = am4core.color(matchingGrade.color);
    })
}

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    let min = 0;
    let max = 5;
    let i = 1;
    for (let k in dimensiones) {
        if (k === "transformacional") {
            dibujar_tacometro(
                dimensiones[k],
                min,
                max,
                "lid-3",
                {
                    bajo: {low: 0, high: 3.65},
                    medio: {low: 3.65, high: 4.2},
                    alto: {low: 4.2, high: 5}
                }
            )
        } else if (k === "transaccional") {
            dibujar_tacometro(
                dimensiones[k],
                min,
                max,
                "lid-2",
                {
                    bajo: {low: 0, high: 3},
                    medio: {low: 3, high: 3.72},
                    alto: {low: 3.72, high: 5}
                }
            )
        } else if (k === "laizzes_faire") {
            dibujar_tacometro(
                dimensiones[k],
                min,
                max,
                "lid-1",
                {
                    bajo: {low: 0, high: 1.83},
                    medio: {low: 1.83, high: 2.83},
                    alto: {low: 2.83, high: 5}
                }
            )
        }
    }


}); // end am4core.ready()
    
    