am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create("chartdiv1", am4charts.XYChart);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.data = experiencias;

chart.colors.step = 2;
chart.padding(30, 30, 10, 30);
chart.legend = new am4charts.Legend();

var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "categoria";
categoryAxis.renderer.grid.template.location = 0;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.min = 0;
valueAxis.max = 100;
valueAxis.strictMinMax = true;
valueAxis.calculateTotals = true;
valueAxis.renderer.minWidth = 50;


var series1 = chart.series.push(new am4charts.ColumnSeries());
series1.columns.template.width = am4core.percent(80);
series1.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series1.name = "Ese curso nunca lo he visto";
series1.dataFields.categoryX = "categoria";
series1.dataFields.valueY = "valor0";
series1.dataFields.valueYShow = "totalPercent";
series1.dataItems.template.locations.categoryX = 0.5;
series1.stacked = true;
series1.tooltip.pointerOrientation = "vertical";

var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
bullet1.interactionsEnabled = false;
bullet1.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet1.label.fill = am4core.color("#ffffff");
bullet1.locationY = 0.5;

var series2 = chart.series.push(new am4charts.ColumnSeries());
series2.columns.template.width = am4core.percent(80);
series2.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series2.name = "Me va bien, aun cuando no es lo que más me gusta";
series2.dataFields.categoryX = "categoria";
series2.dataFields.valueY = "valor1";
series2.dataFields.valueYShow = "totalPercent";
series2.dataItems.template.locations.categoryX = 0.5;
series2.stacked = true;
series2.tooltip.pointerOrientation = "vertical";

var bullet2 = series2.bullets.push(new am4charts.LabelBullet());
bullet2.interactionsEnabled = false;
bullet2.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet2.locationY = 0.5;
bullet2.label.fill = am4core.color("#ffffff");

var series3 = chart.series.push(new am4charts.ColumnSeries());
series3.columns.template.width = am4core.percent(80);
series3.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series3.name = "Soy muy bueno, se me hace fácil y me gusta";
series3.dataFields.categoryX = "categoria";
series3.dataFields.valueY = "valor2";
series3.dataFields.valueYShow = "totalPercent";
series3.dataItems.template.locations.categoryX = 0.5;
series3.stacked = true;
series3.tooltip.pointerOrientation = "vertical";

var bullet3 = series3.bullets.push(new am4charts.LabelBullet());
bullet3.interactionsEnabled = false;
bullet3.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet3.locationY = 0.5;
bullet3.label.fill = am4core.color("#ffffff");

var series4 = chart.series.push(new am4charts.ColumnSeries());
series4.columns.template.width = am4core.percent(80);
series4.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
  series4.name = "Normal, logro aprobar";
  series4.dataFields.categoryX = "categoria";
  series4.dataFields.valueY = "valor3";
  series4.dataFields.valueYShow = "totalPercent";
  series4.dataItems.template.locations.categoryX = 0.5;
  series4.stacked = true;
  series4.tooltip.pointerOrientation = "vertical";

var bullet4 = series4.bullets.push(new am4charts.LabelBullet());
bullet4.interactionsEnabled = false;
bullet4.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet4.locationY = 0.5;
bullet4.label.fill = am4core.color("#ffffff");

var series5 = chart.series.push(new am4charts.ColumnSeries());
series5.columns.template.width = am4core.percent(80);
series5.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
  series5.name = "Suelo tener dificultades, no es mi área fuerte";
  series5.dataFields.categoryX = "categoria";
  series5.dataFields.valueY = "valor4";
  series5.dataFields.valueYShow = "totalPercent";
  series5.dataItems.template.locations.categoryX = 0.5;
  series5.stacked = true;
  series5.tooltip.pointerOrientation = "vertical";

var bullet5 = series5.bullets.push(new am4charts.LabelBullet());
bullet5.interactionsEnabled = false;
bullet5.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet5.locationY = 0.5;
bullet5.label.fill = am4core.color("#ffffff");

chart.scrollbarX = new am4core.Scrollbar();

}); // end am4core.ready()
