am4core.ready(function() {
 
    graficas.forEach(grafica => {
        
      
        sub_valores = new Set()   
        
       // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
      
        var chart = am4core.create(grafica[0], am4charts.XYChart);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        
        chart.data = grafica[1]["datos"]

        
              
        chart.colors.step = 2;
        chart.padding(30, 30, 10, 30);
        chart.legend = new am4charts.Legend();

        // Create axes
        //var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.minGridDistance = 50;      
        dateAxis.title.text = grafica[1]['titulo_x']
        dateAxis.title.fontSize = "20px"

        //var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());        
        valueAxis.title.text = grafica[1]['titulo_y']
        valueAxis.title.fontSize = "20px"
        
        // Create series
        var series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "visits";
        series.dataFields.dateX = "date";
        series.strokeWidth = 2;
        if(grafica[1]['continua'] === 'No'){
            series.connect = false
        }        
        series.minBulletDistance = 10;
        series.tooltipText = "{valueY}";
        series.tooltip.pointerOrientation = "vertical";
        series.tooltip.background.cornerRadius = 20;
        series.tooltip.background.fillOpacity = 0.5;
        series.tooltip.label.padding(12,12,12,12)

        // Add scrollbar
        chart.scrollbarX = new am4charts.XYChartScrollbar();
        chart.scrollbarX.series.push(series);

        // Add cursor
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.xAxis = dateAxis;
        chart.cursor.snapToSeries = series;
    });

function generateChartData() {
    var chartData = [];
    var firstDate = new Date();
    firstDate.setDate(firstDate.getDate() - 1000);
    var visits = 1200;
    for (var i = 0; i < 500; i++) {
        // we create date objects here. In your data, you can have date strings
        // and then set format of your dates using chart.dataDateFormat property,
        // however when possible, use date objects, as this will speed up chart rendering.
        var newDate = new Date(firstDate);
        newDate.setDate(newDate.getDate() + i);
        
        visits += Math.round((Math.random()<0.5?1:-1)*Math.random()*10);

        chartData.push({
            date: newDate,
            visits: visits
        });
    }
    return chartData;
}

}); // end am4core.ready()


