am4core.ready(function() {
    
    cantidad = { 
      'BAJO':0,
      'MEDIO': 0,
      'ALTO': 0
            }
    graficas.forEach(grafica => {
      sub_valores = new Set()    
      grafica[1].forEach(agrupacion =>{
        Object.keys(agrupacion).forEach(desempenio =>{          
          if(desempenio != 'category'){  
            cantidad[desempenio]+=1
          }   
        })
      })
      
      Object.keys(cantidad).forEach(categoria =>{
          if (cantidad[categoria] > 0){
            sub_valores.add(categoria);
          }
        }
      )
     // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end
    
      var chart = am4core.create(grafica[0], am4charts.XYChart);
      chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
      
      chart.data = grafica[1]
      
      
      chart.colors.step = 2;
      chart.padding(30, 30, 10, 30);
      chart.legend = new am4charts.Legend();
      
      var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "category";
      categoryAxis.renderer.grid.template.location = 0;
      
      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.min = 0;
      valueAxis.strictMinMax = true;
      valueAxis.calculateTotals = true;
      valueAxis.renderer.minWidth = 50;
        
      
      sub_valores.forEach(valor =>{
        crear_serie(chart, valor);
      })      
      
          
    
      
      
      chart.scrollbarX = new am4core.Scrollbar();
    });
    
}); // end am4core.ready()

    




function crear_serie(chart, valor_y){
  var series1 = chart.series.push(new am4charts.ColumnSeries());
  series1.columns.template.width = am4core.percent(80);
  series1.columns.template.tooltipText =
  "{name}: [bold]{valueY}[/]";
  series1.name = valor_y;
  series1.dataFields.categoryX = "category";
  series1.dataFields.valueY = valor_y;
  series1.dataItems.template.locations.categoryX = 0.5;
  series1.stacked = true;
  series1.tooltip.pointerOrientation = "vertical";  
  
  var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
  bullet1.interactionsEnabled = false;
  bullet1.label.text = "{valueY}";
  bullet1.label.fill = am4core.color("#ffffff");
  bullet1.locationY = 0.5;
}  
  
