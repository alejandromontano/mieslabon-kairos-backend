am4core.ready(function() {
    graficas.forEach(grafica => {
      orden_especifico = grafica[1]['orden_labels'][0]
      sub_valores = new Set()   
     
      if (Object.keys(orden_especifico).length>0){
        inicializar_labels_ordenados(grafica[1]["datos"], sub_valores)
      }else{
        inicializar_labels_sin_orden(grafica[1]["datos"], sub_valores)
      }
      
     // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end
    
      var chart = am4core.create(grafica[0], am4charts.XYChart);
      chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
      
      chart.data = grafica[1]["datos"]
            
      chart.colors.step = 2;
      chart.padding(30, 30, 10, 30);
      chart.legend = new am4charts.Legend();
      
      var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "category";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.title.text = grafica[1]['titulo_x']
      categoryAxis.title.fontSize = "20px"
      
      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.min = 0;
      valueAxis.title.text = grafica[1]['titulo_y']
      valueAxis.title.fontSize = "20px"
      valueAxis.strictMinMax = true;
      valueAxis.calculateTotals = true;
      valueAxis.renderer.minWidth = 50;        
      
      sub_valores.forEach(valor =>{
        crear_serie(chart, valor);
      })      
            
      chart.scrollbarX = new am4core.Scrollbar();
    });
    
});

function inicializar_labels_sin_orden(datos, sub_valores){
  datos.forEach(agrupacion =>{        
    Object.keys(agrupacion).forEach(desempenio =>{
      if(desempenio != 'category'){              
        sub_valores.add(desempenio)
      }   
    })
  })
}

function inicializar_labels_ordenados(datos, sub_valores){   
  datos.forEach(agrupacion =>{
    Object.keys(agrupacion).forEach(desempenio =>{          
      if(desempenio != 'category'){  
        orden_especifico[desempenio]+=1
      }   
    })
  })
  
  Object.keys(orden_especifico).forEach(categoria =>{            
      if (orden_especifico[categoria] > 0){
        sub_valores.add(categoria);        
      }
    }
  )
}


function crear_serie(chart, valor_y){
  var series1 = chart.series.push(new am4charts.ColumnSeries());
  series1.columns.template.width = am4core.percent(80);
  series1.columns.template.tooltipText =
  "{name}: [bold]{valueY}[/]";
  series1.name = valor_y;
  series1.dataFields.categoryX = "category";
  series1.dataFields.valueY = valor_y; 
  series1.dataItems.template.locations.categoryX = 0.5;
  series1.stacked = true;
  series1.tooltip.pointerOrientation = "vertical";  
  
  var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
  bullet1.interactionsEnabled = false;
  bullet1.label.text = "{valueY}";
  bullet1.label.fill = am4core.color("#ffffff");
  bullet1.locationY = 0.5;
}  
  
