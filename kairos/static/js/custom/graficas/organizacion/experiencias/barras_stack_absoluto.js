am4core.ready(function() {

  sub_valores = new Set()    
  experiencias_en_asignatura_colegio.forEach(agrupacion =>{
    Object.keys(agrupacion).forEach(desempenio =>{          
      if(desempenio != 'category'){             
        sub_valores.add(desempenio);
      }   
    })
  })
  // Themes begin
  am4core.useTheme(am4themes_animated);
  // Themes end
  
  // Create chart instance
  var chart = am4core.create("experiencias_en_asignatura_colegio", am4charts.XYChart);
  
  // Add data
  chart.data = experiencias_en_asignatura_colegio
  
  // Create axes
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "category";
  categoryAxis.title.text = 'Asignatura'
  categoryAxis.title.marginTop = '20px'
  categoryAxis.title.fontSize = "20px"    
  categoryAxis.renderer.grid.template.location = 0;
  categoryAxis.renderer.minGridDistance = 20;
  categoryAxis.renderer.cellStartLocation = 0.1;
  categoryAxis.renderer.cellEndLocation = 0.9;

  categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
    if (target.dataItem && target.dataItem.index & 2 == 2) {
      return dy + 25;
    }
    return dy;
  });
  
  var  valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.min = 0;
  valueAxis.title.text = 'Número de jóvenes'
  valueAxis.title.fontSize = "20px"
  
  
  sub_valores.forEach(elemento =>{    
    createSeries(elemento, elemento, true);   
  })

  chart.legend = new am4charts.Legend();
  //chart.legend.maxWidth = undefined;  
  chart.legend.maxWidth = 300; 
  //chart.legend.labels.template.maxWidth = 150;
  chart.legend.labels.template.truncate = false;
  chart.legend.labels.template.wrap = true;

  // Create series
  function createSeries(field, name, stacked) {
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = field;
    series.dataFields.categoryX = "category";
    series.name = name;
    
    series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
    series.stacked = stacked;
    series.columns.template.width = am4core.percent(95);

    var bullet = series.bullets.push(new am4charts.LabelBullet());      
    bullet.interactionsEnabled = false;
    bullet.label.text = "{valueY}";
    bullet.label.fill = am4core.color("#ffffff");
    bullet.locationY = 0.5;
  }
  // Add legend
  
  
  
}); // end am4core.ready()


