


am4core.ready(function() {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end
    
    var chart = am4core.create("info_multiple_porcentaje", am4charts.XYChart);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
    
    chart.data = informacion_multiple_porcentaje
    
    chart.colors.step = 2;
    chart.padding(30, 30, 10, 30);
    chart.legend = new am4charts.Legend();
    
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "category";
    categoryAxis.renderer.grid.template.location = 0;
    //categoryAxis.title.marginTop = 10
    categoryAxis.title.text = 'Principal actividad durante el mes pasado'
    categoryAxis.title.fontSize = "20px"

    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
      if (target.dataItem && target.dataItem.index & 2 == 2) {
        //return dy + 15;      
      }
      return dy;
    });
    
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
    valueAxis.strictMinMax = true;
    valueAxis.calculateTotals = true;
    valueAxis.renderer.minWidth = 50;
    valueAxis.title.text = 'Número de jóvenes'
    valueAxis.title.fontSize = "20px"

    
    
        
    crear_serie(chart, "Excelente");
    crear_serie(chart, "Muy buena");
    crear_serie(chart, "Buena");
    crear_serie(chart, "Regular");
    crear_serie(chart, "Mala");
    crear_serie(chart ,"NS/NR");
   
    chart.scrollbarX = new am4core.Scrollbar();
    
  }); // end am4core.ready()

    

function crear_serie(chart, valor_y){
  var series1 = chart.series.push(new am4charts.ColumnSeries());
  series1.columns.template.width = am4core.percent(80);
  series1.columns.template.tooltipText =
  "{name}: [bold]{valueY}[/]";
  series1.name = valor_y;
  series1.dataFields.categoryX = "category";
  series1.dataFields.valueY = valor_y;  
  series1.dataItems.template.locations.categoryX = 0.5;
  series1.stacked = true;
  series1.tooltip.pointerOrientation = "vertical";
  
  var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
  bullet1.interactionsEnabled = false;
  bullet1.label.text = "{valueY}";
  bullet1.label.fill = am4core.color("#ffffff");
  bullet1.locationY = 0.5;
}