am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_animated);
  // Themes end
  
  // Create chart instance
  var chart = am4core.create("info_multiple_absoluto", am4charts.XYChart);
  
  // Add data
  chart.data = informacion_multiple_absoluto
  
  // Create axes
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "category";
  categoryAxis.title.text = 'Nivel educativo alcanzado'
  categoryAxis.title.fontSize = "15px"    
  categoryAxis.renderer.grid.template.location = 0;
  categoryAxis.renderer.minGridDistance = 20;
  categoryAxis.renderer.cellStartLocation = 0.1;
  categoryAxis.renderer.cellEndLocation = 0.9;

  
  var  valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.min = 0;
  valueAxis.title.text = 'Número de jóvenes'
  valueAxis.title.fontSize = "20px"
  
  // Create series
  function createSeries(field, name, stacked) {
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = field;
    series.dataFields.categoryX = "category";
    series.name = name;
    
    
    series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
    series.stacked = stacked;
    series.columns.template.width = am4core.percent(95);

    var bullet = series.bullets.push(new am4charts.LabelBullet());      
    bullet.interactionsEnabled = false;
    bullet.label.text = "{valueY}";
    bullet.label.fill = am4core.color("#ffffff");
    bullet.locationY = 0.5;
  }
  
  createSeries("si", "Con hijos", true);
  createSeries("no", "Sin hijos", true);
  
  
  // Add legend
  chart.legend = new am4charts.Legend();
  
  }); // end am4core.ready()