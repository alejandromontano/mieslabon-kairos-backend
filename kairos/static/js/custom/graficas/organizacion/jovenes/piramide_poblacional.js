function obtenerMayorjovenes(){
    let mayor_mujer = obtenerMayorCantidadHombres("mujer")
    let mayor_hombre = obtenerMayorCantidadHombres("hombre")
    if(mayor_hombre>mayor_mujer){
        return mayor_hombre*1.1
    }else{
        return mayor_mujer*1.1
    }
}

function obtenerMayorCantidadHombres(busqueda){    
    let mayor=0
    for(let i=0;i<informacion_poblacional.length;i++){
        if (mayor<informacion_poblacional[i][busqueda]){
            mayor= informacion_poblacional[i][busqueda]    
        }               
    }
    return mayor
}

am4core.ready(function() {    
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end    
    var mainContainer = am4core.create("piramide_poblacional", am4core.Container);
    mainContainer.marginBottom = 30;
    mainContainer.marginTop = 30;
    mainContainer.width = am4core.percent(100);
    mainContainer.height = am4core.percent(100);
    mainContainer.layout = "horizontal";
            
    var maleChart = mainContainer.createChild(am4charts.XYChart);
    maleChart.paddingRight = 0;
    maleChart.data = informacion_poblacional;
    
    // Create axes
    var maleCategoryAxis = maleChart.yAxes.push(new am4charts.CategoryAxis());
    maleCategoryAxis.dataFields.category = "edad";
    maleCategoryAxis.renderer.grid.template.location = 0;
    maleCategoryAxis.renderer.inversed = false;
    maleCategoryAxis.renderer.minGridDistance = 15;
    maleCategoryAxis.title.text = 'Grupos de edad Hombre'
    maleCategoryAxis.title.fontSize = "20px"   
    
    var maleValueAxis = maleChart.xAxes.push(new am4charts.ValueAxis());
    maleValueAxis.renderer.inversed = true;
    maleValueAxis.min = 0;
    maleValueAxis.max = 10;  
    maleValueAxis.strictMinMax = true;    
    maleValueAxis.numberFormatter = new am4core.NumberFormatter();
    maleValueAxis.numberFormatter.numberFormat = "#.#'%'";
    maleValueAxis.renderer.minLabelPosition = 0.01;
    
    
    // Create series
    var maleSeries = maleChart.series.push(new am4charts.ColumnSeries());
    maleSeries.name = "Hombres";
    maleSeries.dataFields.valueX = "hombre";
    maleSeries.dataFields.valueXShow = "percent";
    maleSeries.dataFields.valueXShow = "value";
    maleSeries.calculatePercent = true;
    maleSeries.dataFields.categoryY = "edad";
    maleSeries.interpolationDuration = 1000;
    maleSeries.columns.template.tooltipText = "Hombres, edad {categoryY}: {valueX} ({valueX.percent.formatNumber('#.0')}%)";
    maleSeries.sequencedInterpolation = true;
            
    var femaleChart = mainContainer.createChild(am4charts.XYChart);
    femaleChart.paddingLeft = 0;
    femaleChart.data = JSON.parse(JSON.stringify(informacion_poblacional));
    
    // Create axes
    var femaleCategoryAxis = femaleChart.yAxes.push(new am4charts.CategoryAxis());
    femaleCategoryAxis.renderer.opposite = true;
    femaleCategoryAxis.dataFields.category = "edad";
    femaleCategoryAxis.renderer.grid.template.location = 0;
    femaleCategoryAxis.renderer.minGridDistance = 15;
    femaleCategoryAxis.title.text = 'Grupos de edad Mujeres'
    femaleCategoryAxis.title.fontSize = "20px" 
    femaleCategoryAxis.renderer.inversed = false; 
    
    var femaleValueAxis = femaleChart.xAxes.push(new am4charts.ValueAxis());
    femaleValueAxis.min = 0;
    femaleValueAxis.max = 10;// obtenerMayorjovenes();
    femaleValueAxis.strictMinMax = true;
    femaleValueAxis.numberFormatter = new am4core.NumberFormatter();
    femaleValueAxis.numberFormatter.numberFormat = "#.#'%'";
    femaleValueAxis.renderer.minLabelPosition = 0.01;
    //femaleValueAxis.renderer.grid.template.location = 100;
    
    
    // Create series
    var femaleSeries = femaleChart.series.push(new am4charts.ColumnSeries());
    femaleSeries.name = "Mujeres";
    femaleSeries.dataFields.valueX = "mujer";
    //femaleSeries.dataFields.valueXShow = "percent";
    femaleSeries.dataFields.valueXShow = "value";
    femaleSeries.calculatePercent = true;
    femaleSeries.fill = femaleChart.colors.getIndex(4);
    femaleSeries.stroke = femaleSeries.fill;
    //femaleSeries.sequencedInterpolation = true;
    femaleSeries.columns.template.tooltipText = "Mujeres, edad {categoryY}: {valueX} ({valueX.percent.formatNumber('#.0')}%)";
    femaleSeries.dataFields.categoryY = "edad";
    femaleSeries.interpolationDuration = 1000;       

    
    
    mainContainer.legend = new am4charts.Legend();
   
   
}); // end am4core.ready()