am4core.ready(function() {
    graficas.forEach(grafica => {  
             
        sub_valores = new Set()    
        grafica[1].forEach(agrupacion =>{
            Object.keys(agrupacion).forEach(desempenio =>{          
                if(desempenio != 'agrupacion'){             
                    sub_valores.add(desempenio);
                }   
            })
        })      
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
      
        
        // Create chart instance
        var chart = am4core.create(grafica[0], am4charts.XYChart);
        
        // Add data
        chart.data=grafica[1]
        
        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "agrupacion";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;
        
        var  valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
        valueAxis.renderer.opposite = true;
        
        // Create series
        function createSeries(field, name) {
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueX = field;
        series.dataFields.categoryY = "agrupacion";
        series.name = name;
        series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
        series.columns.template.height = am4core.percent(100);
        series.sequencedInterpolation = true;
        
        var valueLabel = series.bullets.push(new am4charts.LabelBullet());
        //valueLabel.label.text = "{valueX}";
        valueLabel.label.horizontalCenter = "left";
        valueLabel.label.dx = 10;
        valueLabel.label.hideOversized = false;
        valueLabel.label.truncate = false;
        
        var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
        categoryLabel.label.text = "{valueX}";
        categoryLabel.label.horizontalCenter = "right";
        categoryLabel.label.dx = -10;
        categoryLabel.label.fill = am4core.color("#fff");
        categoryLabel.label.hideOversized = false;
        categoryLabel.label.truncate = false;
        }
        
        
        sub_valores.forEach(agrupacion =>{
            createSeries(agrupacion, mayuscula_primer_letra(agrupacion));
        })       
        chart.legend = new am4charts.Legend();
           
       
    })
}); // end am4core.ready()

function mayuscula_primer_letra(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}