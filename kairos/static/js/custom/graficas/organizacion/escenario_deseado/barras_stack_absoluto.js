function obtener_total_datos(){
    suma_total = 0
    for(let i=0;i<necesidades_economicas_capacidad_inversion.length;i++){
      //suma_total += necesidades_economicas_capacidad_inversion[i]['si'];
      //suma_total += necesidades_economicas_capacidad_inversion[i]['no'];      
    }
    
    return 10;
}



am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_animated);
  // Themes end
  
  var chart = am4core.create("necesidades_economicas_capacidad_inversion", am4charts.XYChart);
  chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
  
  chart.data = necesidades_economicas_capacidad_inversion
  
  chart.colors.step = 2;
  chart.padding(30, 30, 10, 30);
  chart.legend = new am4charts.Legend();
  
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "category";
  categoryAxis.renderer.grid.template.location = 0;
  
  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());  
  valueAxis.min = 0;
  valueAxis.max = obtener_total_datos();
  valueAxis.strictMinMax = true;
  valueAxis.calculateTotals = true;
  valueAxis.renderer.minWidth = 50;
  
  
  for(let info in necesidades_economicas_capacidad_inversion[0]){                   
    if(info != 'category'){    
      crear_serie(chart, info)      
    }
  }
  
  
  
  
  
  chart.scrollbarX = new am4core.Scrollbar();
  
}); // end am4core.ready()

function crear_serie(chart, nombre){
  var series1 = chart.series.push(new am4charts.ColumnSeries());
  series1.columns.template.width = am4core.percent(80);
  series1.columns.template.tooltipText =
    "{name}: {valueY}";
  series1.name = nombre;
  series1.dataFields.categoryX = "category";
  series1.dataFields.valueY = nombre;
  series1.dataFields.valueYShow = "total";
  series1.dataItems.template.locations.categoryX = 0.5;
  series1.stacked = true;
  series1.tooltip.pointerOrientation = "vertical";
  
  var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
  bullet1.interactionsEnabled = false;
  bullet1.label.text = "{valueY}";
  bullet1.label.fill = am4core.color("#ffffff");
  bullet1.locationY = 0.5;
}