am4core.ready(function() {
  
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end
    
    var chart2 = am4core.create('vinculados_formacion_salario', am4charts.XYChart)

    chart2.colors.step = 2;
    
    chart2.legend = new am4charts.Legend()
    chart2.legend.position = 'top'
    chart2.legend.paddingBottom = 20
    chart2.legend.labels.template.maxWidth = 95
    chart2.legend.labels.template.truncate = false;
    chart2.legend.labels.template.wrap = true;
    
    var xAxis2 = chart2.xAxes.push(new am4charts.CategoryAxis())
    xAxis2.dataFields.category = 'salario'
    xAxis2.title.text = vinculados_formacion_salario['titulo_x']
    xAxis2.title.fontSize = "20px"
    xAxis2.renderer.cellStartLocation = 0.1
    xAxis2.renderer.cellEndLocation = 0.9
    xAxis2.renderer.grid.template.location = 0;
    
    var xAxis2 = chart2.yAxes.push(new am4charts.ValueAxis());
    xAxis2.title.text = vinculados_formacion_salario['titulo_y']
    xAxis2.title.fontSize = "20px"
    xAxis2.min = 0;
    
    function createSeries2(value, name) {
        var series2 = chart2.series.push(new am4charts.ColumnSeries())
        series2.dataFields.valueY = value
        series2.dataFields.categoryX = 'salario'
        series2.name = name
    
        series2.events.on("hidden", arrangeColumns2);
        series2.events.on("shown", arrangeColumns2);
    
        var bullet2 = series2.bullets.push(new am4charts.LabelBullet())
        bullet2.interactionsEnabled = false
        bullet2.dy = 30;
        bullet2.label.fill = am4core.color('#ffffff')
    
        return series2;
    }
    
    chart2.data = vinculados_formacion_salario['datos']
    
    
    createSeries2('tecnico', 'Técnico');
    createSeries2('universitario', 'Universitario');
    createSeries2('tecnologico', 'Tecnólogo');
    
    
    function arrangeColumns2() {
    
        var series2 = chart2.series.getIndex(0);
    
        var w2 = 1 - xAxis2.renderer.cellStartLocation - (1 - xAxis2.renderer.cellEndLocation);
        if (series2.dataItems.length > 1) {
            var x0 = xAxis2.getX(series2.dataItems.getIndex(0), "categoryX");
            var x1 = xAxis2.getX(series2.dataItems.getIndex(1), "categoryX");
            var delta = ((x1 - x0) / chart2.series.length) * w2;
            if (am4core.isNumber(delta)) {
                var middle = chart2.series.length / 2;
    
                var newIndex = 0;
                chart2.series.each(function(series) {
                    if (!series.isHidden && !series.isHiding) {
                        series.dummyData = newIndex;
                        newIndex++;
                    }
                    else {
                        series.dummyData = chart2.series.indexOf(series);
                    }
                })
                var visibleCount = newIndex;
                var newMiddle = visibleCount / 2;
    
                chart2.series.each(function(series) {
                    var trueIndex = chart2.series.indexOf(series);
                    var newIndex = series.dummyData;
    
                    var dx = (newIndex - trueIndex + middle - newMiddle) * delta
    
                    series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                    series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                })
            }
        }
    }
    
}); // end am4core.ready()