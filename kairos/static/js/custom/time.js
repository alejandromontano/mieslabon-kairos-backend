
var intervalo_countdown;

function asyncCallCountDown(){
    if(!intervalo_countdown){
        faltante = tiempoFinal - fecha_ahora_mismo;
        intervalo_countdown = setInterval(function() {
            tiempo_actual = tiempo_actual + 1;            
            
            var hours = Math.floor((faltante % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((faltante % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((faltante % (1000 * 60)) / 1000);
            faltante = faltante - 1000
            let hora_formato = configurar_formato_contador(hours, minutes, seconds)
            $("#timer").text(hora_formato);            
            
            if (faltante < 0) {
                clearInterval(intervalo_countdown);
                intervalo_countdown = null;
                notificar_tiempo_finalizado();
            }
            
        }, 1000);
    }
}

function configurar_formato_contador(hora, minuto, segundo){
    let hora_formato = hora+'h:'
    let minuto_formato = minuto+'m:'
    let segundo_formato = segundo+'s:'
    if (parseInt(hora)<10){
        hora_formato = "0"+hora+'h:'
    }

    if (parseInt(minuto)<10){        
        minuto_formato = "0"+minuto+'m:'
    }

    if (parseInt(segundo)<10){ 
        segundo_formato = "0"+segundo+'s'
    }

    return "Tiempo restante: "+hora_formato+minuto_formato+segundo_formato
}

function notificar_tiempo_finalizado(){
    document.getElementById("timer").style.color = 'red';
    document.getElementById("timer").innerHTML = "SE HA TERMINADO EL TIEMPO!!";
    $('#siguiente').prop('disabled',true);
    toastr.info('Se ha terminado el tiempo, tus respuestas han sido guardadas!', '', {
        "closeButton": true,
        "progressBar": true,
        "showEasing": "swing",
        "extendedTimeOut": 5000,
        "timeOut": 3000,
    });
    finalizar_cuestionario();
}

function mostrar_solo_calificacion(){
    $('.iniciar').hide();
    $('.formulario_preguntas').hide();
    $('.calificar').show();
    $('.formulario_descripcion').show();
    $('.indicaciones').hide();
}

function finalizar_cuestionario(){
    guardar_respuestas();
    mostrar_solo_calificacion();
}

function asyncCallCountDownREAD(iniciado) {
    $('.iniciar').click(function(){
        if(!iniciado){
            clearInterval(intervalo_countdown);
            guardar_respuestas();
            iniciado = true;
        }
    });

    intervalo_countdown = setInterval(function() {
        tiempo_actual = tiempo_actual + 1;
        ahora_mismo = new Date(new Date().getTime() + tiempo_inicial*1000).getTime();
        var faltante = tiempoFinal - ahora_mismo;

        // Time calculations for days, hours, minutes and seconds        
        var hours = Math.floor((faltante % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((faltante % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((faltante % (1000 * 60)) / 1000);
        document.getElementById("timer_read").innerHTML ="Tiempo restante: " + hours + "h:" + minutes + "m:" + seconds + "s";

        if (faltante < 0) {
            document.getElementById("timer_read").style.color = 'orange';
            document.getElementById("timer_read").innerHTML = "SE HA TERMINADO EL TIEMPO! RESPONDE LAS PREGUNTAS";
            $('.texto').hide();
            $('.tiempo_lectura').hide();
            guardar_respuestas();
        }
    }, 1000);
}

function asyncCallTimer() {
    intervalo_timer = setInterval(function() {
        tiempo_actual = tiempo_actual + 1;
        var hAux, mAux, sAux;
        if (seconds>59){minutes++;seconds=0;}
        if (minutes>59){hours++;minutes=0;}
        if (hours>24){hours=0;}
        seconds++;
        if (seconds<10){sAux="0"+seconds;}else{sAux=seconds;}
        if (minutes<10){mAux="0"+minutes;}else{mAux=minutes;}
        if (hours<10){hAux="0"+hours;}else{hAux=hours;}        

    }, 1000);
}