from kairos.organizaciones.models import Organizacion

def consulta_tablero_joven(id_organizaciones, filtro):
    organizacion_sesion = Organizacion.objects.get(id=id_organizaciones)
    sub_organizaciones = [organizacion_sesion]
    sub_organizaciones += list(organizacion_sesion.sub_organizaciones.all())

    lista_ids = []
    if filtro == 'Todas':
      lista_ids = [organizacion_sesion.pk]
      lista_ids += list(organizacion_sesion.sub_organizaciones.values_list('pk', flat=True))
    else:
      lista_ids = [int(filtro)]    

    nombre_organizacion = 'NOMBRE DE LA ORGANIZACIÓN'
    
    miembros_organizaciones = organizacion_sesion.obtener_miembros_de_organizaciones_cuestionarios_terminados(lista_ids)
    miembros_organizaciones = miembros_organizaciones.prefetch_related('joven_dueno_perfil').select_related('ciudad')

    jovenes_activos = organizacion_sesion.obtener_cantidad_joveves_activos(lista_ids)
    edad_promedio_jovenes = organizacion_sesion.promedio_edad_jovenes(miembros_organizaciones)
    anios_educadion_promedio = organizacion_sesion.promedio_anios_educacion(miembros_organizaciones)
    jovenes_cuestionarios_terminados = len(miembros_organizaciones)
    informacion_poblacional = organizacion_sesion.obtener_rangos_edades(miembros_organizaciones)    
    informacion_ciudades = organizacion_sesion.obtener_ciudades_jovenes(miembros_organizaciones)
    informacion_familia = organizacion_sesion.obtener_informacion_familia(miembros_organizaciones)
    informacion_salarios = organizacion_sesion.obtener_informacion_ingresos(miembros_organizaciones)  
    informacion_multiple_absoluto = organizacion_sesion.obtener_hijos_nivel_academico(miembros_organizaciones)
    informacion_multiple_porcentaje = organizacion_sesion.estado_salud_actividad(miembros_organizaciones)
    factores_de_bienestar = organizacion_sesion.obtener_mayores_factores_bienestar(miembros_organizaciones)
    
    context = {
        'informacion_poblacional': informacion_poblacional,
        'informacion_ciudades': informacion_ciudades,
        'informacion_familia': informacion_familia,
        'informacion_salarios': informacion_salarios,
        'informacion_multiple_porcentaje': informacion_multiple_porcentaje,
        'informacion_multiple_absoluto': informacion_multiple_absoluto,

        'nombre_organizacion': nombre_organizacion,
        'jovenes_activos': jovenes_activos,
        'edad_promedio_jovenes': edad_promedio_jovenes,
        'anios_educadion_promedio': anios_educadion_promedio,
        'jovenes_cuestionarios_terminados': jovenes_cuestionarios_terminados,
        'factores_de_bienestar': factores_de_bienestar,
        'sub_organizaciones': sub_organizaciones,
        'filtro':str(filtro),

    }
    return context

def tablero_competencias(id_organizaciones, filtro):
    organizacion_sesion = Organizacion.objects.get(id=id_organizaciones)
    sub_organizaciones = [organizacion_sesion]
    sub_organizaciones += list(organizacion_sesion.sub_organizaciones.all())

    if filtro == 'Todas':
      lista_ids = [organizacion_sesion.pk]
      for org in organizacion_sesion.sub_organizaciones.all():
        lista_ids.append(org.pk)
    else:
      lista_ids = [int(filtro)]

    miembros_organizaciones = organizacion_sesion.obtener_miembros_de_organizaciones_cuestionarios_terminados(lista_ids)    

    informacion_multiple_porcentaje = organizacion_sesion.obtener_calificacion_respuestas_joven(miembros_organizaciones)
    informacion_mapa_riesgo = organizacion_sesion.obtener_cruce_cualitativo_cuantitativo(miembros_organizaciones)

    informacion_desempenio_competencia = organizacion_sesion.obtener_desempenio_por_competencia(miembros_organizaciones)
    informacion_desempenio_habilidades_sociales = organizacion_sesion.obtener_desempenio_por_habilidades_sociales(miembros_organizaciones)
    jovenes_tipo_liderazgo = organizacion_sesion.obtener_jovenes_tipo_liderazgo(miembros_organizaciones)
    nivel_aptitud_area_conocimiento = organizacion_sesion.nivel_aptitud_area_conocimiento(miembros_organizaciones)

    #jovenes_alto_desempenio = organizacion_sesion.obtener_jovenes_alto_bajo_desempenio(lista_ids)['ALTO']
    #jovenes_bajo_desempenio = organizacion_sesion.obtener_jovenes_alto_bajo_desempenio(lista_ids)['BAJO']
    jovenes_alto_transaccional = organizacion_sesion.obtener_jovenes_alto_bajo_liderazgo(miembros_organizaciones)['jovenes_alto_transaccional']
    jovenes_alto_transformacional = organizacion_sesion.obtener_jovenes_alto_bajo_liderazgo(miembros_organizaciones)['jovenes_alto_transformacional']
    jovenes_bajo_rendimiento = organizacion_sesion.obtener_jovenes_alto_bajo_liderazgo(miembros_organizaciones)['jovenes_bajo_rendimiento']

    areas_interes_joven = organizacion_sesion.area_mayor_interes_jovenes(miembros_organizaciones) 
    area_mayor_interes_jovenes = areas_interes_joven[0][0]
    area_menor_interes_jovenes = areas_interes_joven[len(areas_interes_joven) - 1][0]
    #area_mayor_aptitud_jovenes = organizacion_sesion.area_mayor_menor_aptitud_jovenes(lista_ids)[0][0]
    #area_menor_aptitud_jovenes = organizacion_sesion.area_mayor_menor_aptitud_jovenes(lista_ids)[1][0]

    context = {
      'informacion_multiple_porcentaje': informacion_multiple_porcentaje,
      'informacion_mapa_riesgo': informacion_mapa_riesgo,
      'informacion_desempenio_competencia': informacion_desempenio_competencia,
      'informacion_desempenio_habilidades_sociales': informacion_desempenio_habilidades_sociales,
      'jovenes_tipo_liderazgo': jovenes_tipo_liderazgo,
      'nivel_aptitud_area_conocimiento': nivel_aptitud_area_conocimiento,

      'jovenes_alto_transaccional': jovenes_alto_transaccional,
      'jovenes_alto_transformacional': jovenes_alto_transformacional,
      'jovenes_bajo_rendimiento': jovenes_bajo_rendimiento,
      'area_mayor_interes_jovenes': area_mayor_interes_jovenes,
      'area_menor_interes_jovenes': area_menor_interes_jovenes,      
      'sub_organizaciones': sub_organizaciones,
      'filtro':str(filtro),

    }
    return context

def tablero_escenario_deseado(id_organizaciones, filtro):
    organizacion_sesion = Organizacion.objects.get(id=id_organizaciones)
    sub_organizaciones = [organizacion_sesion]
    sub_organizaciones += list(organizacion_sesion.sub_organizaciones.all())

    if filtro == 'Todas':
      lista_ids = [organizacion_sesion.pk]
      for org in organizacion_sesion.sub_organizaciones.all():
        lista_ids.append(org.pk)
    else:
      lista_ids = [int(filtro)]

    miembros_organizaciones = organizacion_sesion.obtener_miembros_de_organizaciones_cuestionarios_terminados(lista_ids)    

    jovenes_cuestionarios_terminados = len(miembros_organizaciones)
    jovenes_escenarios_deseados_favoritos = organizacion_sesion.cantidad_jovenes_escenarios_deseados_favoritos(miembros_organizaciones)

    porcentajes = organizacion_sesion.porcentaje_tribus(miembros_organizaciones)
    porcentaje_administracion = porcentajes.pop('Administración', 0)
    porcentaje_ingenieria = porcentajes.pop('Ingeniería', 0)
    porcentaje_salud = porcentajes.pop('Salud', 0)
    porcentaje_humanidades = porcentajes.pop('Humanidades', 0)
    porcentaje_artes_deportes = porcentajes.pop('Artes y Deportes', 0)
    porcentaje_ciencia_medio_ambiente = porcentajes.pop('Ciencia y Medio Ambiente', 0)
    porcentaje_seguridad_defensa = porcentajes.pop('Seguridad y Defensa', 0)

    nivel_conocimiento_alcanzar_por_tribu = organizacion_sesion.nivel_conocimiento_alcanzar_tribu(miembros_organizaciones)
    intereses_por_tribu = organizacion_sesion.intereses_por_tribus(miembros_organizaciones)
    formas_aprendizaje_preferidas_por_tribu = organizacion_sesion.deseos_aprendizaje_tribu(miembros_organizaciones)
    necesidades_economicas_capacidad_inversion = [{'category': 'P', 'si': 1, 'no': 1},
      {'category': 'T', 'si': 1, 'no': 1}, {'category': 'PF', 'si': 1, 'no': 2},
      {'category': 'T', 'si': 1, 'no': 1}, {'category': 'P', 'si': 1, 'no': 1},
      {'category': 'PF', 'si': 1, 'no': 2},
      {'category': 'PF', 'si': 1, 'no': 2}, {'category': 'B', 'si': 0, 'no': 3},
      {'category': 'B', 'si': 0, 'no': 3}
    ]

    pais_ciudad_desea_experiencia = organizacion_sesion.obtener_pais_ciudades_adquirir_experiencia(miembros_organizaciones)

    context = {
      'nivel_conocimiento_alcanzar_por_tribu': nivel_conocimiento_alcanzar_por_tribu,
      'intereses_por_tribu': intereses_por_tribu,
      'formas_aprendizaje_preferidas_por_tribu': formas_aprendizaje_preferidas_por_tribu,
      'necesidades_economicas_capacidad_inversion': necesidades_economicas_capacidad_inversion,
      'pais_ciudad_desea_experiencia': pais_ciudad_desea_experiencia,
      'jovenes_cuestionarios_terminados':jovenes_cuestionarios_terminados,
      'jovenes_escenarios_deseados_favoritos':jovenes_escenarios_deseados_favoritos,

      'porcentaje_administracion': porcentaje_administracion,
      'porcentaje_ingenieria': porcentaje_ingenieria,
      'porcentaje_salud': porcentaje_salud,
      'porcentaje_humanidades': porcentaje_humanidades,
      'porcentaje_artes_deportes': porcentaje_artes_deportes,
      'porcentaje_ciencia_medio_ambiente': porcentaje_ciencia_medio_ambiente,
      'porcentaje_seguridad_defensa': porcentaje_seguridad_defensa,
      'sub_organizaciones': sub_organizaciones,
      'filtro':str(filtro),
    }
    return context

def tablero_experiencias(id_organizaciones, filtro):
    organizacion_sesion = Organizacion.objects.get(id=id_organizaciones)
    sub_organizaciones = [organizacion_sesion]
    sub_organizaciones += list(organizacion_sesion.sub_organizaciones.all())

    if filtro == 'Todas':
      lista_ids = [organizacion_sesion.pk]
      for org in organizacion_sesion.sub_organizaciones.all():
        lista_ids.append(org.pk)
    else:
       lista_ids = [int(filtro)]

    miembros_organizaciones = organizacion_sesion.obtener_miembros_de_organizaciones_cuestionarios_terminados(lista_ids)

    experiencias_en_asignatura_colegio = organizacion_sesion.experiencias_colegio(miembros_organizaciones)
    distribucion_jovenes_por_experiencia_tribu = {'datos': organizacion_sesion.distribucion_jovenes_por_experiencia_tribu(miembros_organizaciones),
    'titulo_x': 'Experiencia',
    'titulo_y': 'Número de jóvenes',
    'orden_labels': [{"Brianimus": 0, "Centeros": 0, "Cuisana": 0, "Gronor": 0, "Moterra": 0, "Ponor": 0, "Vincularem" :0}]}

    distribucion_jovenes_por_experiencia_duracion_experiencia = {'datos': organizacion_sesion.distribucion_jovenes_por_experiencia_tiempo(miembros_organizaciones),
    'titulo_x': 'Experiencia',
    'titulo_y': 'Número de jóvenes',
    'orden_labels': [{"Menos de 1 año": 0, "Entre 1 y 2 años": 0, "Entre 2 y 4 años": 0, "Más de 5 años": 0}]}

    cantidad_voluntariados = organizacion_sesion.jovenes_en_voluntariado(miembros_organizaciones)    
    cantidad_experiencia_laboral = organizacion_sesion.jovenes_con_experiencia_laboral(miembros_organizaciones)
    cantidad_grupo_juvenil = organizacion_sesion.jovenes_con_clubes(miembros_organizaciones)

    numero_experiencias_promedio_joven = organizacion_sesion.obtener_numero_experiencias_promedio_joven(miembros_organizaciones)
    tipo_experiencia_mas_frecuente = organizacion_sesion.obtener_experiencias_ordenadas_por_frecuencia(miembros_organizaciones)[0]
    tipo_experiencia_menos_frecuente = list(reversed(organizacion_sesion.obtener_experiencias_ordenadas_por_frecuencia(miembros_organizaciones)))[0]
    experiencias_actividades = [organizacion_sesion.obtener_experiencias_estadisticas_experiencia('club_juvenil', miembros_organizaciones),
                                organizacion_sesion.obtener_experiencias_estadisticas_experiencia('voluntariado', miembros_organizaciones),
                                organizacion_sesion.obtener_experiencias_estadisticas_experiencia('colaborador', miembros_organizaciones),
                                organizacion_sesion.obtener_experiencias_estadisticas_experiencia('independiente', miembros_organizaciones),
                                organizacion_sesion.obtener_experiencias_estadisticas_experiencia('emprendimiento', miembros_organizaciones)]

    context = {

      'experiencias_en_asignatura_colegio': experiencias_en_asignatura_colegio,
      'distribucion_jovenes_por_experiencia_tribu': distribucion_jovenes_por_experiencia_tribu,
      'distribucion_jovenes_por_experiencia_duracion_experiencia':distribucion_jovenes_por_experiencia_duracion_experiencia,

      'cantidad_voluntariados': cantidad_voluntariados,
      'cantidad_experiencia_laboral': cantidad_experiencia_laboral,
      'cantidad_grupo_juvenil': cantidad_grupo_juvenil,

      'numero_experiencias_promedio_joven': numero_experiencias_promedio_joven,
      'tipo_experiencia_mas_frecuente': tipo_experiencia_mas_frecuente,
      'tipo_experiencia_menos_frecuente': tipo_experiencia_menos_frecuente,

      'experiencias_actividades': experiencias_actividades,
      'sub_organizaciones': sub_organizaciones,
      'filtro':str(filtro),
    }
    return context