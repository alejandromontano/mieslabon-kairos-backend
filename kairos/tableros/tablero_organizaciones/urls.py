# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *


app_name = "tablero_organizaciones"

urlpatterns = [
    path('visualizar_tablero/', TableroJovenes.as_view(), name='visualizar_tablero'),
    path('competencias/', TableroCompetencias.as_view(), name='tablero_competencias'),
    path('escenario_deseado/', TableroEscenarioDeseado.as_view(), name='escenario_deseado'),
    path('experiencias/', TableroExperiencias.as_view(), name='experiencias'),
    path('refactorizacion/', TableroRefactorizacion.as_view(), name='refactorizacion'),
]