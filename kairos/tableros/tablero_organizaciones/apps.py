from django.apps import AppConfig


class TableroOrganizacionesConfig(AppConfig):
    name = 'tablero_organizaciones'
