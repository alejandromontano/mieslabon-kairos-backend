# Modulos Django
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView

# Modulos de plugin externos
import json as simplejson

# Modulos de otras apps
from kairos.usuarios.models import *
from kairos.organizaciones.models import *

# Modulos internos de la app

class TableroJovenes(LoginRequiredMixin, TemplateView):
    template_name = 'tableros/tablero_organizaciones/visualizar_tablero.html'

    def dispatch(self, request, *args, **kwargs):
      if request.user.is_authenticated and request.user.tipo != 'Organización':
        messages.error(request, "Usted no está autorizado para consultar los tableros de la organización")
        return redirect("inicio")
      return super(TableroJovenes, self).dispatch(request, *args, **kwargs)
      
    def get_context_data(self, **kwargs):
      from .utils import consulta_tablero_joven
      filtro = 'Todas'
      
      if self.request.GET:
        if 'filtro' in self.request.GET:
          filtro = self.request.GET['filtro']
        else:
          filtro = list(self.request.GET)[0]

      return consulta_tablero_joven(self.request.user.id, filtro)

class TableroCompetencias(LoginRequiredMixin, TemplateView):
  template_name = 'tableros/tablero_organizaciones/tablero_competencias.html'

  def dispatch(self, request, *args, **kwargs):
    if request.user.is_authenticated and request.user.tipo != 'Organización':
      messages.error(request, "Usted no está autorizado para consultar los tableros de la organización")
      return redirect("inicio")
    return super(TableroCompetencias, self).dispatch(request, *args, **kwargs)
  
  def get_context_data(self, **kwargs):
    from .utils import tablero_competencias
    filtro = 'Todas'

    if self.request.GET:
      if 'filtro' in self.request.GET:
        filtro = self.request.GET['filtro']
      else:
        filtro = list(self.request.GET)[0]

    return tablero_competencias(self.request.user.id, filtro)
    

class TableroEscenarioDeseado(LoginRequiredMixin, TemplateView):
  template_name = 'tableros/tablero_organizaciones/tablero_escenario_deseado.html'

  def dispatch(self, request, *args, **kwargs):
    if request.user.is_authenticated and request.user.tipo != 'Organización':
      messages.error(request, "Usted no está autorizado para consultar los tableros de la organización")
      return redirect("inicio")
    return super(TableroEscenarioDeseado, self).dispatch(request, *args, **kwargs)

  def get_context_data(self, **kwargs):
    from .utils import tablero_escenario_deseado
    filtro = 'Todas'
    if self.request.GET:
      if 'filtro' in self.request.GET:
        filtro = self.request.GET['filtro']
      else:
        filtro = list(self.request.GET)[0]
    return tablero_escenario_deseado(self.request.user.id, filtro)

class TableroExperiencias(LoginRequiredMixin, TemplateView):
  template_name = 'tableros/tablero_organizaciones/tablero_experiencias.html'

  def dispatch(self, request, *args, **kwargs):
    if request.user.is_authenticated and request.user.tipo != 'Organización':
      messages.error(request, "Usted no está autorizado para consultar los tableros de la organización")
      return redirect("inicio")
    return super(TableroExperiencias, self).dispatch(request, *args, **kwargs)

  def get_context_data(self, **kwargs):    
    from .utils import tablero_experiencias
    filtro = 'Todas'
    if self.request.GET:
      if 'filtro' in self.request.GET:
        filtro = self.request.GET['filtro']
      else:
        filtro = list(self.request.GET)[0]
    return tablero_experiencias(self.request.user.id, filtro)


class TableroRefactorizacion(LoginRequiredMixin, TemplateView):
  template_name = 'tableros/tablero_organizaciones/prueba.html'

  def dispatch(self, request, *args, **kwargs):
    if request.user.is_authenticated and request.user.es_superusuario == False and request.user.tipo != 'Organización':
      messages.error(request, "Usted no está autorizado para consultar los tableros de la organización")
      return redirect("inicio")
    return super(TableroRefactorizacion, self).dispatch(request, *args, **kwargs)
 
  def get_context_data(self, **kwargs):
    context=dict()

    return context
       

    