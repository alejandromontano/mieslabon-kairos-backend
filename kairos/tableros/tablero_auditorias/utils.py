import pandas as pd 

def codigo_departamento(departamento):
    """
    Retorna el codigo del departamento correspondiente pasado como parametro
    """
    if departamento == 'ANTIOQUIA':
        return 'CO-ANT'
    elif departamento == 'ATLANTICO':
        return 'CO-ATL'
    elif departamento == 'BOGOTA DC':
        return 'CO-DC'
    elif departamento == 'BOLIVAR':
        return 'CO-BOL'
    elif departamento == 'BOYACA':
        return 'CO-BOY'
    elif departamento == 'CALDAS':
        return 'CO-CAL'
    elif departamento == 'CAQUETA':
        return 'CO-CAQ'
    elif departamento == 'CASANARE':
        return 'CO-CAS'
    elif departamento == 'CAUCA':
        return 'CO-CAU'
    elif departamento == 'CESAR':
        return 'CO-CES'
    elif departamento == 'CHOCO':
        return 'CO-CHO'
    elif departamento == 'CORDOBA':
        return 'CO-COR'
    elif departamento == 'CUNDINAMARCA':
        return 'CO-CUN'
    elif departamento == 'GUAJIRA':
        return 'CO-LAG'
    elif departamento == 'HUILA':
        return 'CO-HUI'
    elif departamento == 'MAGDALENA':
        return 'CO-MAG'
    elif departamento == 'META':
        return 'CO-MET'
    elif departamento == 'NARINO':
        return 'CO-NAR'
    elif departamento == 'NORTE DE SANTANDER':
        return 'CO-NSA'
    elif departamento == 'PUTUMAYO':
        return 'CO-PUT'
    elif departamento == 'QUINDIO':
        return 'CO-QUI'
    elif departamento == 'RISARALDA':
        return 'CO-RIS'
    elif departamento == 'SAN ANDRES Y PROVIDENCIA':
        return 'CO-SAP'
    elif departamento == 'SANTANDER':
        return 'CO-SAN'
    elif departamento == 'SUCRE':
        return 'CO-SUC'
    elif departamento == 'TOLIMA':
        return 'CO-TOL'
    elif departamento == 'VALLE DEL CAUCA':
        return 'CO-VAC'
    elif departamento == 'ARAUCA':
        return 'CO-ARA'
    elif departamento == 'AMAZONAS':
        return 'CO-AMA'
    elif departamento == 'VAUPES':
        return 'CO-VAU'
    elif departamento == 'GUAVIARE':
        return 'CO-GUV'
    elif departamento == 'VICHADA':
        return 'CO-VID'
    else:
        return 'CO-GUA'

def graduados_anios(excel_graduados, filtro):
    anios = excel_graduados.iloc[:, 6].tolist()
    formaciones = excel_graduados.iloc[:, 8].tolist()
    graduados = excel_graduados.iloc[:, 13].tolist()
    graduados_formacion_por_anio = []
    nucleos = excel_graduados.iloc[:, 11].tolist()

    anios_sin_repetir = sorted(set(anios))
    for anio in anios_sin_repetir:
      graduados_formacion_por_anio.append([0,0,0])

    if filtro == 'Todas':
        for indice in range(len(anios)):
            if formaciones[indice] != 0:
                indice_anio = anios_sin_repetir.index(anios[indice])
                if formaciones[indice] == 'FORMACION TECNICA PROFESIONAL':
                    graduados_formacion_por_anio[indice_anio][0] += graduados[indice]
                elif formaciones[indice] == 'UNIVERSITARIA':
                    graduados_formacion_por_anio[indice_anio][1] += graduados[indice]
                else:
                    graduados_formacion_por_anio[indice_anio][2] += graduados[indice]
    else:
        for indice in range(len(anios)):
            if nucleos[indice] == filtro:
                if formaciones[indice] != 0:
                    indice_anio = anios_sin_repetir.index(anios[indice])
                    if formaciones[indice] == 'FORMACION TECNICA PROFESIONAL':
                        graduados_formacion_por_anio[indice_anio][0] += graduados[indice]
                    elif formaciones[indice] == 'UNIVERSITARIA':
                        graduados_formacion_por_anio[indice_anio][1] += graduados[indice]
                    else:
                        graduados_formacion_por_anio[indice_anio][2] += graduados[indice]
    
    graduados_por_anio = []

    for indice in range(len(graduados_formacion_por_anio)):
      graduados_por_anio.append({
        'category': anios_sin_repetir[indice],
        'tecnicoprofesional': graduados_formacion_por_anio[indice][0],
        'universitaria': graduados_formacion_por_anio[indice][1],
        'tecnologica': graduados_formacion_por_anio[indice][2]
      })
    
    return graduados_por_anio

def graduados_departamento(excel_graduados, filtro):
    anios = excel_graduados.iloc[:, 6].tolist()
    anios_sin_repetir = sorted(set(anios))
    ultimo_anio = anios_sin_repetir[len(anios_sin_repetir)-1]
    departamentos = excel_graduados.iloc[:, 7].tolist()
    graduados = excel_graduados.iloc[:, 13].tolist()
    nucleos = excel_graduados.iloc[:, 11].tolist()
    graduados_departamento = []
    departamentos_cuenta ={
      'AMAZONAS': 0,
      'ANTIOQUIA': 0,
      'ARAUCA': 0,
      'ATLANTICO': 0,
      'BOGOTA DC': 0,
      'BOLIVAR': 0,
      'BOYACA': 0,
      'CALDAS': 0,
      'CAQUETA': 0,
      'CASANARE': 0,
      'CAUCA': 0,
      'CESAR': 0,
      'CHOCO': 0,
      'CORDOBA': 0,
      'CUNDINAMARCA': 0,
      'GUAINIA': 0,
      'GUAJIRA': 0,
      'GUAVIARE': 0,
      'HUILA': 0,
      'MAGDALENA': 0,
      'META': 0,
      'NARINO': 0,
      'NORTE DE SANTANDER': 0,
      'PUTUMAYO': 0,
      'QUINDIO': 0,
      'RISARALDA': 0,
      'SAN ANDRES Y PROVIDENCIA': 0,
      'SANTANDER': 0,
      'SUCRE': 0,
      'TOLIMA': 0,
      'VALLE DEL CAUCA': 0,
      'VAUPES': 0,
      'VICHADA': 0,
    }
    if filtro == 'Todas':
        for indice in range(len(departamentos)):
            if departamentos[indice] != 0:
                if anios[indice] == ultimo_anio:
                    departamentos_cuenta[departamentos[indice]] += graduados[indice]
    else:
        for indice in range(len(departamentos)):
            if departamentos[indice] != 0:
                if nucleos[indice] == filtro:
                    if anios[indice] == ultimo_anio:
                        departamentos_cuenta[departamentos[indice]] += graduados[indice]
    
    for departamento in departamentos_cuenta:
      graduados_departamento.append({
        'id': codigo_departamento(departamento),
        'value': departamentos_cuenta[departamento]
      })
    
    return graduados_departamento, ultimo_anio

def matriculados_anio(excel_matriculados, filtro):
    anios_matriculados = excel_matriculados.iloc[:, 6].tolist()
    formaciones_matriculados = excel_matriculados.iloc[:, 8].tolist()
    matriculados = excel_matriculados.iloc[:, 13].tolist()
    nucleos = excel_matriculados.iloc[:, 11].tolist()
    matriculados_formacion_por_anio = []

    anios_sin_repetir_matriculados = sorted(set(anios_matriculados))
    for anio in anios_sin_repetir_matriculados:
      matriculados_formacion_por_anio.append([0,0,0])

    if filtro == 'Todas':
        for indice in range(len(anios_matriculados)):
            if formaciones_matriculados[indice] != 0:
                indice_anio = anios_sin_repetir_matriculados.index(anios_matriculados[indice])
                if formaciones_matriculados[indice] == 'FORMACION TECNICA PROFESIONAL':
                    matriculados_formacion_por_anio[indice_anio][0] += matriculados[indice]
                elif formaciones_matriculados[indice] == 'UNIVERSITARIA':
                    matriculados_formacion_por_anio[indice_anio][1] += matriculados[indice]
                else:
                    matriculados_formacion_por_anio[indice_anio][2] += matriculados[indice]
    else:
        for indice in range(len(anios_matriculados)):
            if formaciones_matriculados[indice] != 0:
                if nucleos[indice] == filtro:
                    indice_anio = anios_sin_repetir_matriculados.index(anios_matriculados[indice])
                    if formaciones_matriculados[indice] == 'FORMACION TECNICA PROFESIONAL':
                        matriculados_formacion_por_anio[indice_anio][0] += matriculados[indice]
                    elif formaciones_matriculados[indice] == 'UNIVERSITARIA':
                        matriculados_formacion_por_anio[indice_anio][1] += matriculados[indice]
                    else:
                        matriculados_formacion_por_anio[indice_anio][2] += matriculados[indice]
    
    matriculados_por_anio = []

    for indice in range(len(matriculados_formacion_por_anio)):
      matriculados_por_anio.append({
        'category': anios_sin_repetir_matriculados[indice],
        'tecnicoprofesional': matriculados_formacion_por_anio[indice][0],
        'universitaria': matriculados_formacion_por_anio[indice][1],
        'tecnologica': matriculados_formacion_por_anio[indice][2]
      })
    
    return matriculados_por_anio

def matriculados_departamento(excel_matriculados, filtro):
    anios_matriculados = excel_matriculados.iloc[:, 6].tolist()
    anios_sin_repetir_matriculados = sorted(set(anios_matriculados))
    ultimo_anio_matriculados = anios_sin_repetir_matriculados[len(anios_sin_repetir_matriculados)-1]
    matriculados = excel_matriculados.iloc[:, 13].tolist()
    nucleos = excel_matriculados.iloc[:, 11].tolist()
    departamentos_matriculados = excel_matriculados.iloc[:, 7].tolist()
    matriculados_departamento = []
    departamentos_cuenta_matriculados ={
      'AMAZONAS': 0,
      'ANTIOQUIA': 0,
      'ARAUCA': 0,
      'ATLANTICO': 0,
      'BOGOTA DC': 0,
      'BOLIVAR': 0,
      'BOYACA': 0,
      'CALDAS': 0,
      'CAQUETA': 0,
      'CASANARE': 0,
      'CAUCA': 0,
      'CESAR': 0,
      'CHOCO': 0,
      'CORDOBA': 0,
      'CUNDINAMARCA': 0,
      'GUAINIA': 0,
      'GUAJIRA': 0,
      'GUAVIARE': 0,
      'HUILA': 0,
      'MAGDALENA': 0,
      'META': 0,
      'NARINO': 0,
      'NORTE DE SANTANDER': 0,
      'PUTUMAYO': 0,
      'QUINDIO': 0,
      'RISARALDA': 0,
      'SAN ANDRES Y PROVIDENCIA': 0,
      'SANTANDER': 0,
      'SUCRE': 0,
      'TOLIMA': 0,
      'VALLE DEL CAUCA': 0,
      'VAUPES': 0,
      'VICHADA': 0,
    }
    if filtro == 'Todas':
        for indice in range(len(departamentos_matriculados)):
            if departamentos_matriculados[indice] != 0:
                if anios_matriculados[indice] == ultimo_anio_matriculados:
                    departamentos_cuenta_matriculados[departamentos_matriculados[indice]] += matriculados[indice]
    else:
        for indice in range(len(departamentos_matriculados)):
            if departamentos_matriculados[indice] != 0:
                if nucleos[indice] == filtro:
                    if anios_matriculados[indice] == ultimo_anio_matriculados:
                        departamentos_cuenta_matriculados[departamentos_matriculados[indice]] += matriculados[indice]
    
    for departamento in departamentos_cuenta_matriculados:
      matriculados_departamento.append({
        'id': codigo_departamento(departamento),
        'value': departamentos_cuenta_matriculados[departamento]
      })
    return matriculados_departamento, ultimo_anio_matriculados

def vinculados_salario(excel_salarios, filtro):
    anios_vinculados = excel_salarios.iloc[:, 0].tolist()
    salarios = excel_salarios.iloc[:, 1].tolist()
    formaciones_vinculados = excel_salarios.iloc[:, 3].tolist()
    vinculados = excel_salarios.iloc[:, 4].tolist()
    areas_conocimiento = excel_salarios.iloc[:, 2].tolist()
    vinculados_formacion_salario = []

    anios_unicos = sorted(set(anios_vinculados))
    ultimo_anio_vinculados = anios_unicos[len(anios_unicos)-1]

    salarios_unicos= ['1 SMMLV','Entre 1 y 1,5 SMMLV','Entre 1,5 y 2 SMMLV',
                      'Entre 2 y 2 ,5 SMMLV','Entre 2,5 y 3 SMMLV','Entre 3 y 3,5 SMMLV',
                      'Entre 3,5 y 4 SMMLV','Entre 4 y 4,5 SMMLV','Entre 4,5 y 5 SMMLV',
                      'Entre 5 y 6 SMMLV', 'Entre 6 y 7 SMMLV', 'Entre 7 y 8 SMMLV',
                      'Entre 8 y 9 SMMLV', 'Entre 9 y 11 SMMLV', 'Entre 11 y 13 SMMLV',
                      'Entre 13 y 15 SMMLV', 'Más de 15 SMMLV']

    for salario in salarios_unicos:
      vinculados_formacion_salario.append([0,0,0])

    if filtro == 'Todas':
        for indice in range(len(salarios)):
            if anios_vinculados[indice] == ultimo_anio_vinculados:
                indice_salario = salarios_unicos.index(salarios[indice])
                if formaciones_vinculados[indice] == 'TECNICO':
                    vinculados_formacion_salario[indice_salario][0] += vinculados[indice]
                elif formaciones_vinculados[indice] == 'UNIVERSITARIO':
                    vinculados_formacion_salario[indice_salario][1] += vinculados[indice]
                else:
                    vinculados_formacion_salario[indice_salario][2] += vinculados[indice]
    else:
        for indice in range(len(salarios)):
            if anios_vinculados[indice] == ultimo_anio_vinculados:
                if areas_conocimiento[indice] == filtro:
                    indice_salario = salarios_unicos.index(salarios[indice])
                    if formaciones_vinculados[indice] == 'TECNICO':
                        vinculados_formacion_salario[indice_salario][0] += vinculados[indice]
                    elif formaciones_vinculados[indice] == 'UNIVERSITARIO':
                        vinculados_formacion_salario[indice_salario][1] += vinculados[indice]
                    else:
                        vinculados_formacion_salario[indice_salario][2] += vinculados[indice]

    
    vinculados_por_salario = []

    for indice in range(len(vinculados_formacion_salario)):
      vinculados_por_salario.append({
        'salario': salarios_unicos[indice],
        'tecnico': vinculados_formacion_salario[indice][0],
        'universitario': vinculados_formacion_salario[indice][1],
        'tecnologico': vinculados_formacion_salario[indice][2]
      })
    
    return vinculados_por_salario, ultimo_anio_vinculados


def areas_de_conocimiento_y_nucleos(excel):
    areas = excel.iloc[:, 10].tolist()
    nucleos = excel.iloc[:, 11].tolist()

    areas_de_conocimiento = []
    nucleos_basicos = []

    for area in areas:
        if area not in areas_de_conocimiento:
            if area is not 0:
                areas_de_conocimiento.append(area)
    
    for nucleo in nucleos:
        if nucleo not in nucleos_basicos:
            if nucleo is not 0:
                nucleos_basicos.append(nucleo)
    
    return areas_de_conocimiento, nucleos_basicos

def relacion_areas_conocimiento_y_nucleos(excel):
    areas = excel.iloc[:, 10].tolist()
    nucleos = excel.iloc[:, 11].tolist()
    areas_con_nucleos = dict()

    for indice in range(len(areas)):
        if areas[indice] not in areas_con_nucleos:
            if areas[indice] is not 0:
                areas_con_nucleos[areas[indice]] = [nucleos[indice]]
        else:
            nucleos_actuales = areas_con_nucleos[areas[indice]]
            if nucleos[indice] not in nucleos_actuales:
                if nucleos[indice] is not 0:
                    nucleos_actuales.append(nucleos[indice])
                    areas_con_nucleos[areas[indice]] = nucleos_actuales
    
    return areas_con_nucleos


def areas_conocimiento_salarios(excel):
    areas = excel.iloc[:, 2].tolist()   
    areas_de_conocimiento = []
   
    for area in areas:
        if area not in areas_de_conocimiento:
            if area is not 0:
                areas_de_conocimiento.append(area)
    
    return areas_de_conocimiento
