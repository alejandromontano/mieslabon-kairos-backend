from django.apps import AppConfig


class TableroAuditoriasConfig(AppConfig):
    name = 'tablero_auditorias'
