# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "tablero_auditorias"

urlpatterns = [
    path('tablero-general/', TableroGeneral.as_view(), name='tablero_general_auditoria'),
    path('tabla-retencion-jovenes/', TablaRetencionJovenes.as_view(), name='tabla_retencion_jovenes'),
    path('tablero-modulo/', TableroModulos.as_view(), name='tablero_modulo_auditoria'),
    path('tablero-rutas-oportunidades/', TableroRutasOportunidades.as_view(), name='tablero_rutas_oportunidades'),
    path('tablero-economico/', TableroEconomico.as_view(), name='tablero_economico'),
    path('tablero-guias/', TableroGuiasMiEslabon.as_view(), name='tablero_guias'),

    
]