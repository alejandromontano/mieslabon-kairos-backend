# Modulos Django
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView
from django.contrib import messages

# Modulos de plugin externos

# Modulos de otras apps
from kairos.auditorias.models import *
from kairos.usuarios.models import Joven

# Modulos internos de la app



class TableroGeneral(LoginRequiredMixin, TemplateView):
    template_name = 'tableros/tablero_auditorias/tablero_general.html'

    def dispatch(self, request, *args, **kwargs):
      if not self.request.user.is_authenticated or not self.request.user.is_superuser:
        messages.error(request, "Usted no está autorizado para acceder al módulo")
        return redirect("inicio")
      return super(TableroGeneral, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        cantidad_jovenes = Joven.obtener_cantidad_jovenes()
        jovenes_activos_ultima_semana = Joven.obtener_jovenes_activos_en_ultima_semana()
        ingresos_exitosos_promedio_joven_ultima_semana = Joven.obtener_promedio_de_ingresos_por_joven_en_la_ultima_semana()
        jovenes_sin_iniciar_cuestionarios = Joven.jovenes_sin_iniciar_cuestionarios()
        jovenes_iniciaron_cuestionarios = Joven.jovenes_iniciaron_cuestionarios()

        jovenes_registrados_en_plataforma = {
            'datos': Joven.obtener_jovenes_registrados_en_plataforma(),
            'titulo_x': 'Fecha',
            'titulo_y': ' ',
            'continua': 'Si'
        }

        jovenes_nuevos_registrados_en_plataforma = {
            'datos':Joven.obtener_jovenes_nuevos_registrados_en_plataforma(),
            'titulo_x': 'Fecha',
            'titulo_y': 'Número de jóvenes',
            'continua': 'Si'
          }

        jovenes_activos_en_plataforma = {
            'datos': Joven.obtener_jovenes_activos_en_plataforma(),
            'titulo_x':' Fecha',
            'titulo_y': 'Número de jóvenes activos',
            'continua': 'Si'
        }
        sesiones_activas_por_dia = {
          'datos': Joven.obtener_sesiones_activass_en_plataforma(),
          'titulo_x': 'Fecha',
          'titulo_y': 'Número de sesiones',
          'continua': 'Si'
        }

        context['cantidad_jovenes'] = cantidad_jovenes
        context['jovenes_activos_ultima_semana'] = jovenes_activos_ultima_semana
        context['ingresos_exitosos_promedio_joven_ultima_semana'] = ingresos_exitosos_promedio_joven_ultima_semana
        context['jovenes_sin_iniciar_cuestionarios'] = jovenes_sin_iniciar_cuestionarios
        context['jovenes_iniciaron_cuestionarios'] = jovenes_iniciaron_cuestionarios

        context['jovenes_registrados_en_plataforma'] = jovenes_registrados_en_plataforma
        context['jovenes_nuevos_registrados_en_plataforma'] = jovenes_nuevos_registrados_en_plataforma
        context['jovenes_activos_en_plataforma'] = jovenes_activos_en_plataforma
        context['sesiones_activas_por_dia'] = sesiones_activas_por_dia
        return context

class TableroModulos(LoginRequiredMixin, TemplateView):
    template_name = 'tableros/tablero_auditorias/tablero_modulos.html'

    def dispatch(self, request, *args, **kwargs):
      if not self.request.user.is_authenticated or not self.request.user.is_superuser:
        messages.error(request, "Usted no está autorizado para acceder al módulo")
        return redirect("inicio")
      return super(TableroModulos, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        diccionario_modulos = {
            'equipos': '/equipos/',
            'jovenes': '/amigos/',
            'rutas': '/rutas/',
            'retos': '/retos/',
            'oportunidades': '/oportunidades/',
            'insignias': '/insignias/',
            'ranking': '/ranking/'
        }

        for modulo, ruta in diccionario_modulos.items():
            context['grafica_'+modulo] = {
                'datos':Joven.obtener_numero_de_inicios_de_sesion_unicos_por_dia_experiencias(ruta),
                'titulo_x': 'Fecha',
                'titulo_y': 'Número de ingresos exitosos',
                'continua': 'No'
            }

        return context

class TableroRutasOportunidades(LoginRequiredMixin, TemplateView):
    template_name = 'tableros/tablero_auditorias/tablero_rutas_oportunidades.html'

    def dispatch(self, request, *args, **kwargs):
      if not self.request.user.is_authenticated or not self.request.user.is_superuser:
        messages.error(request, "Usted no está autorizado para acceder al módulo")
        return redirect("inicio")
      return super(TableroRutasOportunidades, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        from kairos.contenido.models import Contenido
        from django.db.models import Count

        context = super().get_context_data(**kwargs)
        datos_de_tabla_de_rutas = Joven.obtener_datos_de_tabla_de_rutas()
        datos_de_tabla_de_oportunidades = Joven.obtener_datos_de_tabla_de_oportunidades()

        cantidad_oportunidades = Contenido.obtener_contenidos('oportunidad').count()
        cantidad_oportunidades_me_gusta = Contenido.obtener_me_gusta('oportunidad').count()
        cantidad_oportunidades_no_me_gusta = Contenido.obtener_no_me_gusta('oportunidad').count()

        cantidad_rutas = Contenido.obtener_contenidos('ruta').count()
        cantidad_rutas_me_gusta = Contenido.obtener_me_gusta('ruta').count()
        cantidad_rutas_no_me_gusta = Contenido.obtener_no_me_gusta('ruta').count()

        context['cantidad_oportunidades'] = cantidad_oportunidades
        context['cantidad_oportunidades_me_gusta'] = cantidad_oportunidades_me_gusta
        context['cantidad_oportunidades_no_me_gusta'] = cantidad_oportunidades_no_me_gusta
        context['cantidad_rutas'] = cantidad_rutas
        context['cantidad_rutas_me_gusta'] = cantidad_rutas_me_gusta
        context['cantidad_rutas_no_me_gusta'] = cantidad_rutas_no_me_gusta

        context['datos_de_tabla_de_rutas'] = datos_de_tabla_de_rutas
        context['datos_de_tabla_de_oportunidades'] = datos_de_tabla_de_oportunidades

        return context

class TableroGuiasMiEslabon(LoginRequiredMixin, TemplateView):
    template_name = 'tableros/tablero_auditorias/tablero_guia_mieslabon.html'

    def dispatch(self, request, *args, **kwargs):
      if not self.request.user.is_authenticated or not self.request.user.is_superuser:
        messages.error(request, "Usted no está autorizado para acceder al módulo")
        return redirect("inicio")
      return super(TableroGuiasMiEslabon, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        datos_de_tabla_de_guia = Joven.obtener_datos_de_tabla_de_guias()

        context['datos_de_tabla_de_guia'] = datos_de_tabla_de_guia
        return context

class TablaRetencionJovenes(LoginRequiredMixin, TemplateView):
    template_name = 'tableros/tablero_auditorias/tabla_retencion_jovenes.html'

    def dispatch(self, request, *args, **kwargs):
      if not self.request.user.is_authenticated or not self.request.user.is_superuser:
        messages.error(request, "Usted no está autorizado para acceder al módulo")
        return redirect("inicio")
      return super(TablaRetencionJovenes, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        jovenes_retencion_colegios = Joven.retencion_jovenes()

        context['jovenes_retencion_colegios'] = jovenes_retencion_colegios

        return context

class TableroEconomico(LoginRequiredMixin, TemplateView):
  template_name = 'tableros/tablero_jovenes/tablero_economico.html' 
  def dispatch(self, request, *args, **kwargs):
    if not self.request.user.is_authenticated:
      messages.error(request, "Usted no está autorizado para acceder al módulo")
      return redirect("inicio")
    return super(TableroEconomico, self).dispatch(request, *args, **kwargs)

  def get_context_data(self, **kwargs):
    import pandas as pd 
    from .utils import  graduados_anios, graduados_departamento, matriculados_anio, matriculados_departamento, vinculados_salario, areas_de_conocimiento_y_nucleos, relacion_areas_conocimiento_y_nucleos, areas_conocimiento_salarios
    from config.settings.base import EXCEL_URL 

    context = super().get_context_data(**kwargs)

    excel_graduados =  pd.read_csv(EXCEL_URL + 'graduados_matriculados-graduados.csv')
    excel_matriculados =  pd.read_csv(EXCEL_URL + 'graduados_matriculados-matriculados.csv')
    excel_vinculados =  pd.read_excel(EXCEL_URL + 'ole_long.xlsx')

    datos_graduados = excel_graduados.fillna(0)
    datos_matriculados = excel_matriculados.fillna(0)
    datos_vinculados = excel_vinculados.fillna(0)

    areas_conocimiento, nucleos_basicos = areas_de_conocimiento_y_nucleos(datos_graduados)
    areas_conocimiento_salario = areas_conocimiento_salarios(datos_vinculados)
    relacion_areas_nucleos = relacion_areas_conocimiento_y_nucleos(datos_graduados)

    
    if self.request.GET:
      if 'nucleo_basico' in self.request.GET:  
        filtro=self.request.GET.get('nucleo_basico')
        context['filtrado'] = True
        context['nucleo'] = filtro
      else:
        filtro = 'Todas'
        context['filtrado'] = False
      
      if 'area_conocimiento2' in self.request.GET:
        filtro2=self.request.GET.get('area_conocimiento2')
        context['filtrado2'] = True
        context['area'] = filtro2
      else:
        filtro2 = 'Todas'
        context['filtrado2'] = False
    else:
      filtro = 'Todas'
      filtro2 = 'Todas'
      context['filtrado'] = False
      context['filtrado2'] = False

    datos_graduduados_anios, ultimo_anio_graduados = graduados_departamento(datos_graduados, filtro)
    datos_matriculados_anios, ultimo_anio_matriculados = matriculados_departamento(datos_matriculados, filtro)
    datos_vinculados_salarios, ultimo_anio_vinculados = vinculados_salario(datos_vinculados, filtro2)
    
    context['matriculados_por_anio'] = {
      'datos':matriculados_anio(datos_matriculados, filtro),
      'titulo_x': 'Años',
      'titulo_y': 'Matriculados'
    } 
    context['graduados_por_anio'] = {
      'datos': graduados_anios(datos_graduados, filtro),
      'titulo_x': 'Años',
      'titulo_y': 'Graduados'
    } 
    context['vinculados_formacion_salario'] = {
      'datos': datos_vinculados_salarios,
      'titulo_x': 'Rango salarial',
      'titulo_y': 'Vinculados'
    }
    context['graduados_por_departamento'] = datos_graduduados_anios
    context['matriculados_por_departamento'] = datos_matriculados_anios
    context['ultimo_anio_graduados'] = ultimo_anio_graduados
    context['ultimo_anio_matriculados'] = ultimo_anio_matriculados
    context['ultimo_anio_vinculados'] = ultimo_anio_vinculados
    context['areas_conocimiento'] = areas_conocimiento  
    context['areas_conocimiento_salario'] = areas_conocimiento_salario
    context['nucleos_basicos'] = nucleos_basicos
    context['relacion_areas_nucleos'] = relacion_areas_nucleos
    

    return context

# Create your views here.
