# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *


app_name = "tablero_jovenes"

urlpatterns = [
    path('<int:id_joven>/', TableroJovenes.as_view(), name='tablero_joven'),
]