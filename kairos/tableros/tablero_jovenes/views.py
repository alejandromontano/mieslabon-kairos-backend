# Modulos Django
from django.shortcuts import redirect, render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView
from django.contrib import messages

# Modulos de plugin externos
# Modulos de otras apps
from kairos.usuarios.models import *
from kairos.formularios.models import SeccionVisitada

# Modulos internos de la app


# Create your views here.
class TableroJovenes(LoginRequiredMixin, TemplateView):
    template_name = 'tableros/tablero_jovenes/tablero_jovenes.html'

    def dispatch(self, request, *args, **kwargs):
        joven = Joven.objects.get(pk=self.request.user.pk)
        if joven.tanda_formularios == None:
            messages.error(request, "No tienes asignados los cuestionarios!")
            return redirect("inicio")
        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True)
        formularios_disponibles = formularios_joven.filter(
            ~Q(respuestasjoven__joven=joven.id) | Q(respuestasjoven__joven=joven.id, respuestasjoven__completas=False)
        )
        if formularios_disponibles.count() != 0:
            messages.error(request, "No has completado todos tus cuestionarios!")
            return redirect("inicio")
        return super(TableroJovenes, self).dispatch(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        id_joven = self.request.user.id
        joven = Joven.buscar(id_joven=id_joven)
        SeccionVisitada.crear_seccion_joven(id_joven)
        joven.generar_estado_visto_resultados()

        return context