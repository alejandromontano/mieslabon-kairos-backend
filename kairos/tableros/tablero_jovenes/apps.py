from django.apps import AppConfig


class TableroJovenesConfig(AppConfig):
    name = 'tablero_jovenes'
