from django.core.management.base import BaseCommand
from kairos.retos.views import ApiNotificarCorreos

from django.contrib.sites.models import Site


class Command(BaseCommand):
    help = 'Cron que envía los correos de notificación a los jovenes miembros de un Reto cuando este acabe.'

    def handle(self, *args, **kwargs):
        import requests
        sitio = Site.objects.all().first()
        url = "http://{}/app/retos/api/notificar-correos".format(sitio.domain)
        
        response = requests.get(url)
        print(response)