# Modulos Django
from django import forms
from django.db.models import Q

# Modulos de plugin externos
from config.settings.base import BASE_DIR
from django_select2.forms import Select2Widget, Select2MultipleWidget, ModelSelect2MultipleWidget
from PIL import Image, ExifTags
from tempus_dominus.widgets import DatePicker, DateTimePicker
import os

# Modulos de otras apps
from kairos.equipos.models import Equipo
from kairos.insignias.models import Roles, Tribus, NivelesDeConocimiento
from kairos.organizaciones.models import Organizacion
from kairos.retos.models import CalificacionActoJoven, Competencia
from kairos.usuarios.models import Joven
from kairos.jurados.models import Jurado

# Modulos internos de la app
from .models import *


class CuartoActosFrom(forms.ModelForm):

    class Meta:
        model = CalificacionActoJoven
        fields = ['archivo_subido']
        

class RetosModelForm(forms.ModelForm):
    GRUPOS = (
        ("Jóvenes", "Jóvenes"),
        ("Equipos", "Equipos")
        )
    grupos_permitidos_lista = forms.ChoiceField(
        choices=GRUPOS,
        widget=Select2Widget(),
        required=True,
        label="¿Quién puede inscribirse al reto?*"
    )



    def __init__(self, *args, **kwargs):
        organizacion = None

        if 'organizacion' in kwargs:
            organizacion = kwargs.pop('organizacion')

        super(RetosModelForm, self).__init__(*args, **kwargs)


        self.fields['imagen'].widget.attrs = {
            'name': 'imagen',
            'accept': 'image/*',
            }

        self.fields['logo'].widget.attrs = {
            'class': 'custom-file-input'
            }

        self.fields["competencias"].queryset = Competencia.objects.filter(estado=True)

        if organizacion is not None:
            self.fields["organizacion"].queryset = Organizacion.objects.filter(pk=organizacion.id)
            self.fields["organizaciones_permitidas"].queryset = Organizacion.objects.exclude(pk=organizacion.id)            
            self.fields['grupos_permitidos_lista'].required = False
            self.fields['grupos_permitidos_lista'].widget.attrs['disabled'] = 'disabled'            
        else:
            self.fields["organizacion"].queryset = Organizacion.objects.filter(is_active=True)
        
       
    class Meta:
        model = Retos
        fields = ['organizacion', 'nombre', 'donde_hacerlo', 'descripcion', 'premios', 'caracteristicas', 'fecha_inicio', 'fecha_fin',
                  'precio', 'areas_conocimiento', 'niveles_conocimiento_deseado', 'roles', 'contacto', 'email', 'telefono',
                  'grupos_permitidos_lista', 'permitir_todos', 'organizaciones_permitidas', 'competencias', 'link_video', 'link_video_participacion', 
                  'link_video_reconocimiento', 'link_video_descanso', 'reglas', 'grado_dificultad', 'imagen', 'logo', 'activo']

        widgets = {
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'caracteristicas': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'premios': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'fecha_inicio': DateTimePicker(
                options={
                    'format': 'YYYY-MM-DD HH:mm',
                    'locale': 'es',
                    'sidebyside': True,
                },
                attrs={
                    'class': 'datetimepicker'
                }
            ),
            'fecha_fin': DateTimePicker(
                options={
                    'format': 'YYYY-MM-DD HH:mm',
                    'locale': 'es',
                    'sidebyside': True,
                },
                attrs={
                    'class': 'datetimepicker'                    
                }

            ),

            'areas_conocimiento':Select2MultipleWidget(),
            'competencias':Select2MultipleWidget(),
            'niveles_conocimiento_deseado': Select2MultipleWidget(),
            'organizaciones_permitidas': Select2MultipleWidget(),
            'roles': Select2MultipleWidget(),
            'organizacion':Select2Widget(),
            'grupos_permitidos_lista': Select2Widget(),
        }

    def clean(self):
        cleaned_data = super(RetosModelForm, self).clean()
        
        from django.core.exceptions import ValidationError
        from django.contrib import messages

        fecha_inicio = cleaned_data.get('fecha_inicio')        
        fecha_fin = cleaned_data.get('fecha_fin')        
         
        if fecha_inicio and fecha_fin:
            if fecha_inicio > fecha_fin:
                user_request = self.initial['request']
                msg = u"La fecha final debe ser posterior la fecha inicial."
                self.add_error('fecha_inicio', msg)
                self.add_error('fecha_fin', msg)                
                messages.error(user_request, msg)
                raise forms.ValidationError(msg)
        
        return cleaned_data
        
        




class PostulacionLiderRetoForm(forms.ModelForm):
    from kairos.equipos.models import Equipo
    class Meta:
        model = Retos
        fields = ["solicitudes_equipos"]

        widgets = {
            'solicitudes_equipos': Select2MultipleWidget(
            ),
        }
    def __init__(self, *args, **kwargs):
        lider = kwargs.pop("joven_lider")
        reto = kwargs.pop("reto")
        super(PostulacionLiderRetoForm, self).__init__(*args, **kwargs)
        self.fields["solicitudes_equipos"].queryset = Equipo.objects.filter(lider=lider).exclude(retos_equipo=reto)
        self.fields["solicitudes_equipos"].widget.attrs['required'] = 'required'

class InvitacionAdministradorRetoForm(forms.ModelForm):
    from kairos.equipos.models import Equipo
    class Meta:
        model = Retos
        fields  = ('solicitudes_equipos', 'solicitudes_jovenes')

        widgets = {
            'solicitudes_equipos': Select2MultipleWidget(),
            'solicitudes_jovenes': Select2MultipleWidget()
        }
    def __init__(self, *args, **kwargs):
        super(InvitacionAdministradorRetoForm, self).__init__(*args, **kwargs)

        self.fields["solicitudes_equipos"].required = False
        self.fields["solicitudes_jovenes"].required = False
        if 'Equipos' in self.instance.grupos_permitidos:
            self.fields["solicitudes_equipos"].queryset = Equipo.objects.exclude(retos_equipo=self.instance)


        else:
            self.fields.pop('solicitudes_equipos')
            #  self.fields['solicitudes_equipos'].widget.attrs['disabled'] = True
            #  self.fields['solicitudes_equipos'].widget = forms.HiddenInput()

        if 'Jóvenes' in self.instance.grupos_permitidos:
            self.fields["solicitudes_jovenes"].queryset = Joven.objects.exclude(retos_joven=self.instance)

        else:
            self.fields.pop('solicitudes_jovenes')
            #  self.fields['solicitudes_jovenes'].widget.attrs['disabled'] = True
            #  self.fields['solicitudes_jovenes'].widget = forms.HiddenInput()



class FormSeleccionEquipo(forms.Form):

    equipos = forms.ModelChoiceField(label="Selección del equipo que vas a representar", required=True, widget=Select2Widget(), queryset=Equipo.objects.all())
    def __init__(self, *args, **kwargs):
        joven = kwargs.pop('joven')
        reto = kwargs.pop('reto')
        super(FormSeleccionEquipo, self).__init__(*args, **kwargs)
        equipos_del_joven = joven.miembros_equipos.all()
        equipos = equipos_del_joven.filter(retos_equipo=reto)
        self.fields['equipos'].queryset = equipos

class FormSeleccionJurados(forms.Form):
    jurados = forms.ModelMultipleChoiceField(label="Selección de los jurados que van a calificar este reto", required=False, widget=Select2MultipleWidget(), queryset=Jurado.objects.all())
    def __init__(self, *args, **kwargs):
        reto = None
        if 'reto' in kwargs:
            reto = kwargs.pop('reto')
        super(FormSeleccionJurados, self).__init__(*args, **kwargs)
        from django.db.models import Q
        if reto:
            self.fields['jurados'].queryset = Jurado.objects.filter(~Q(retos_jurados=reto))


class FormSeleccionEquipos(forms.Form):
    equipos = forms.ModelMultipleChoiceField(label="Selección de los Equipos que van a ser asignados este Jurado", required=False, widget=Select2MultipleWidget(), queryset=Equipo.objects.all())
    def __init__(self, *args, **kwargs):
        reto = kwargs.pop('reto')
        jurado = kwargs.pop('jurado')
        super(FormSeleccionEquipos, self).__init__(*args, **kwargs)
        try:
            detalle = DetalleEquipoReto.objects.filter(jurado__isnull=True, reto=reto)
        except DetalleEquipoReto.DoesNotExist:
            pass
        
        lista_equipos_asignados = []
        for equipo in detalle.values('equipo'):
            lista_equipos_asignados.append(equipo['equipo'])
        print(lista_equipos_asignados)
        self.fields['equipos'].queryset = reto.equipos.filter(Q(pk__in=lista_equipos_asignados))

class FormSeleccionJovenes(forms.Form):
    jovenes = forms.ModelMultipleChoiceField(label="Selección de los Jóvenes que van a ser asignados este Jurado", required=False, widget=Select2MultipleWidget(), queryset=Joven.objects.all())
    def __init__(self, *args, **kwargs):
        reto = kwargs.pop('reto')
        jurado = kwargs.pop('jurado')
        super(FormSeleccionJovenes, self).__init__(*args, **kwargs)
        try:
            detalle = DetalleJovenReto.objects.filter(jurado__isnull=True, reto=reto)
        except DetalleJovenReto.DoesNotExist:
            pass
        
        lista_jovenes_asignados = []
        for joven in detalle.values('joven'):
            lista_jovenes_asignados.append(joven['joven'])
        print(lista_jovenes_asignados)
        self.fields['jovenes'].queryset = reto.jovenes.filter(Q(pk__in=lista_jovenes_asignados))
        

    

