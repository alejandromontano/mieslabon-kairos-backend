# Modulos Django
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.base import TemplateView
from django.forms import modelformset_factory


# Modulos de plugin externos
from rest_framework.views import APIView
from rest_framework.response import Response
import json

# Modulos de otras apps
from kairos.core.mixins import MensajeMixin
from kairos.equipos.models import Equipo
from kairos.organizaciones.forms import FormularioSolicitudReto
from kairos.retos.models import CalificacionRetos, Retos, Competencia
from kairos.usuarios.models import Usuario, Joven
from kairos.jurados.models import Jurado
from kairos.cuartos.models import Cuarto
from kairos.retos.models import DetalleEquipoReto


# Modulos internos de la app
from .forms import RetosModelForm, PostulacionLiderRetoForm, InvitacionAdministradorRetoForm, FormSeleccionEquipo, FormSeleccionJurados, FormSeleccionEquipos, FormSeleccionJovenes
from kairos.retos.forms import CuartoActosFrom
from .models import Retos, SolicitudReto, SolicitudesRetosJovenes, SolicitudesRetosEquipos#, TipoReto

class RetosListado(LoginRequiredMixin, ListView):
    model = Retos
    context_object_name = "retos"
    template_name = "retos/listado.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(RetosListado, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        retos = Retos.objects.all()
        return retos

def seleccion_equipo(request, id_reto):
    '''
        Muestra la vista que permite al Joven(Mimebro o lider de equipo) escoger con que equipo va a presentar el reto.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identicador del Reto que se va a realizar
    '''

    if not request.user.is_authenticated:
        return redirect("usuarios:iniciar_sesion")

    if request.user.is_superuser or request.user.tipo == 'Organización':
        messages.error(request, "Usted no está autorizado para realizar la acción!")
        return redirect("inicio")

    reto = None
    try:
        reto = Retos.objects.get(pk=id_reto)
    except Retos.DoesNotExist:
        messages.error(request, "El Reto no existe")
        return redirect("retos:consultar_retos")

    joven = Joven.buscar(request.user.id)
    reto = Retos.buscar(id_reto)

    if request.method == 'POST':
        form_equipos = FormSeleccionEquipo(request.POST, joven=joven, reto=reto)
        if form_equipos.is_valid():
            id_equipo = int(request.POST.get('equipos'))
            return redirect('retos:inicio_reto', id_reto=reto.id, id_equipo=id_equipo)
    else:
        form_equipos = FormSeleccionEquipo(joven=joven, reto=reto)

    return render(request, 'retos/seleccion_equipo.html', {
        'form': form_equipos,
    })


def inicio_reto(request, id_reto, id_equipo=None):
    from django.http import JsonResponse
    
    from .utils import obtener_elementos_configuracion_inicio_reto
    '''
        Muestra la vista inicial a los Jovenes antes de empezar el reto

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identicador del Reto que se va a realizar
    '''
   
    context = dict()
   
    if id_equipo:
        context = obtener_elementos_configuracion_inicio_reto(id_reto, id_equipo, request)
    else:        
        context = obtener_elementos_configuracion_inicio_reto(id_reto, request.user.id, request)

    if context['exito'] == False:
        if 'mensaje' in context.keys():
            messages.error(request, context['mensaje'])
        redireccion = context['redireccion']
        return redireccion
                
    if request.is_ajax():
        response = JsonResponse(context['fechas'])
        response.status_code = 201
        return response
    else:
        return render(request, 'retos/inicio_reto.html', context)

def cuarto_espera_equipos(request, id_reto, id_equipo):
    '''
        Muestra la vista de espera donde los miembros se reportan antes de empezar el reto como Equipo.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identicador del Reto que se va a realizar
            id_equipo: Identificadro del Equipo inscrito en el reto
    '''
    from kairos.retos.models import DetalleEquipoReto
    from django.http import JsonResponse
    from .utils import configuracion_cuarto_espera_equipos, contruir_context_cuarto_espera_equipos
    
    context = dict()
    if id_equipo:
        configuracion = configuracion_cuarto_espera_equipos(id_reto, id_equipo, request)
    else:
        configuracion = configuracion_cuarto_espera_equipos(id_reto, request.user.id, request)

    if configuracion['exito'] == False:
        if 'mensaje' in configuracion.keys():
            messages.error(request, configuracion['mensaje'])
        redireccion = configuracion['redireccion']
        return redireccion

    context = contruir_context_cuarto_espera_equipos(configuracion['reto'], configuracion['detalle'], configuracion['participante'])
 
    if request.is_ajax():
        response = JsonResponse(context['fechas'])
        response.status_code = 201
        return response
    else:
        return render(request, 'retos/cuarto_espera_equipos.html', context)

def cuarto_reconocimiento_equipos(request, id_reto, id_equipo):
    from django.http import JsonResponse
    from .utils import configuracion_cuarto_reconocimiento_equipos, contruir_context_cuarto_reconocimiento_equipos
    '''
        Muestra la vista de reconocimiento de los miembros del equipo que han finalizado el reto, para que
        puedan darse una puntuación del desempeño de cada miembro.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identicador del Reto que se va a realizar
            id_equipo: Identificadro del Equipo inscrito en el reto
    '''
    context = dict()
    if id_equipo:
        configuracion = configuracion_cuarto_reconocimiento_equipos(id_reto, id_equipo, request)
    else:
        configuracion = configuracion_cuarto_reconocimiento_equipos(id_reto, request.user.id, request)

    if configuracion['exito'] == False:
        if 'mensaje' in configuracion.keys():
            messages.error(request, configuracion['mensaje'])
        redireccion = configuracion['redireccion']
        return redireccion

    context = contruir_context_cuarto_reconocimiento_equipos(configuracion['reto'], configuracion['detalle'], configuracion['participante'])

    if request.is_ajax():
        response = JsonResponse(context['fechas'])
        response.status_code = 201
        return response
    else:
        return render(request, 'retos/cuarto_reconocimiento_equipos.html', context)


def cuarto_descanso(request, id_reto, id_equipo=None):
    from .utils import obtener_elementos_configuracion_descanso, contruir_context_descanso
    '''
        Muestra la vista descanso a los Jovenes cuando han terminado un cuarto.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identicador del Reto que se va a realizar
    '''
    context = dict()
    
    if id_equipo:        
        configuracion = obtener_elementos_configuracion_descanso(id_reto, id_equipo, request)
    else:
        configuracion = obtener_elementos_configuracion_descanso(id_reto, request.user.id, request)
    
    if configuracion['exito'] == False:
        if 'mensaje' in configuracion.keys():
            messages.error(request, configuracion['mensaje'])
        redireccion = configuracion['redireccion']
        return redireccion

    context = contruir_context_descanso(configuracion['reto'], configuracion['detalle'], configuracion['participante'])
   
    return render(request, 'retos/cuartos/descanso.html', context)

def abandonar_reto_joven(request, id_reto, id_equipo=None):
    from .utils import configuracion_abandonar_reto
    '''
        Permite abandonar el reto al joven que esta en sesion.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identicador del Reto que se va a realizar
    '''
    context = dict()
    if id_equipo:
        configuracion = configuracion_abandonar_reto(id_reto, id_equipo, request)
    else:
        configuracion = configuracion_abandonar_reto(id_reto, request.user.id, request)

    if configuracion['exito'] == False:
        if 'mensaje' in configuracion.keys():
            messages.error(request, configuracion['mensaje'])
        redireccion = configuracion['redireccion']
        return redireccion
    
    return redirect("retos:cuarto_finalizacion", id_reto)

def retos_responder_cuartos(request, id_reto, id_equipo=None):
    '''
        Muestra la vista del Cuarto actual de un reto al Joven que esta en sesion presentandolo.
        
        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identicador del Reto que se va a realizar
    '''
    from kairos.retos.models import CalificacionActoJoven
    from .utils import contruir_context_responder_cuartos, obtener_elementos_configuracion_responder_cuartos
   
    if id_equipo:
        configuracion = obtener_elementos_configuracion_responder_cuartos(id_reto, id_equipo, request)
    else:
        configuracion = obtener_elementos_configuracion_responder_cuartos(id_reto, request.user.id, request)

    if configuracion['exito'] == False:
        if 'mensaje' in configuracion.keys():
            messages.error(request, configuracion['mensaje'])
        redireccion = configuracion['redireccion']
        return redireccion
    context = contruir_context_responder_cuartos(configuracion['reto'], configuracion['detalle'], configuracion['participante'], configuracion['cuarto_actual'])
    formularios_actos = modelformset_factory(configuracion['modelo'], form=CuartoActosFrom, extra=0)
    if request.method == 'POST':
        lista_formularios = formularios_actos(request.POST, request.FILES, queryset=configuracion['calificaciones'])
        if lista_formularios.is_valid():
            for formulario in lista_formularios:
                if formulario.is_valid():
                    formulario.save()
            return context['redireccion_formulario']
    else:
        lista_formularios = formularios_actos(queryset=configuracion['calificaciones'])
    context['formularios_actos'] = lista_formularios
    context['lista_formularios'] = zip(list(configuracion['cuarto_actual'].actos.all()), list(lista_formularios))

    return render(request, context['template'], context)

def retos_cuarto_final(request, id_reto, id_equipo=None):
    from .utils import obtener_elementos_configuracion_cuarto_finalizacion
    '''
        Muestra la vista del Cuarto final donde se muestra el puntaje que obtuvo el Joven en el reto.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identicador del Reto que se va a realizar
    '''
    context = dict()
    
    if id_equipo:
        configuracion = obtener_elementos_configuracion_cuarto_finalizacion(id_reto, id_equipo, request)
    else:
        configuracion = obtener_elementos_configuracion_cuarto_finalizacion(id_reto, request.user.id, request)

    if configuracion['exito'] == False:
        if 'mensaje' in configuracion.keys():
            messages.error(request, configuracion['mensaje'])
        redireccion = configuracion['redireccion']
        return redireccion
    
    return render(request, 'retos/cuartos/finalizacion.html', configuracion)


class RetosConsultar(LoginRequiredMixin, ListView):
    model = Retos
    context_object_name = "retos"
    template_name = "retos/consultar.html"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated == False and (request.user.is_superuser == False or request.user.tipo == 'Organización'):
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(RetosConsultar, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        retos = Retos.objects.all()
        return retos


class RetosRegistrar(LoginRequiredMixin, CreateView):
    model = Retos
    form_class = RetosModelForm
    template_name = "retos/registrar.html"
    success_url = reverse_lazy('retos:consultar')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(RetosRegistrar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        import os

        reto = form.save(commit=False)
        reto.activo = True

        imagen_anterior = reto.imagen
        if not self.request.POST.get('imagen-clear') is None and imagen_anterior:
            if os.path.isfile(imagen_anterior.path):
                os.remove(imagen_anterior.path)
            reto.imagen = ""

        logo_anterior = reto.logo
        if not self.request.POST.get('logo-clear') is None and logo_anterior:
            if os.path.isfile(logo_anterior.path):
                os.remove(logo_anterior.path)
            reto.logo = ""

        form.instance.grupos_permitidos = form.cleaned_data['grupos_permitidos_lista']

        reto.save()
        reto.crear_cuarto_descanso()
        form.save_m2m()

        messages.success(self.request, "Reto registrado satisfactoriamente")
        return super(RetosRegistrar, self).form_valid(form)

    def get_initial(self):
        init = super(RetosRegistrar, self).get_initial()
        init.update({'request':self.request})
        return init

class RetosRegistrarDesdeSolicitud(LoginRequiredMixin, CreateView):
    model = Retos
    form_class = RetosModelForm
    template_name = "retos/registrar.html"
    success_url = reverse_lazy('retos:consultar')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(RetosRegistrarDesdeSolicitud, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        import os

        reto = form.save(commit=False)
        reto.activo = True

        imagen_anterior = reto.imagen
        if not self.request.POST.get('imagen-clear') is None and imagen_anterior:
            if os.path.isfile(imagen_anterior.path):
                os.remove(imagen_anterior.path)
            reto.foto_perfil = ""

        logo_anterior = reto.logo
        if not self.request.POST.get('logo-clear') is None and logo_anterior:
            if os.path.isfile(logo_anterior.path):
                os.remove(logo_anterior.path)
            reto.logo = ""

        form.instance.grupos_permitidos = form.cleaned_data['grupos_permitidos_lista']
        reto.save()
        reto.crear_cuarto_descanso()
        form.save_m2m()

        messages.success(self.request, "Reto registrado satisfactoriamente")
        return super(RetosRegistrarDesdeSolicitud, self).form_valid(form)

    def get_form_kwargs(self, **kwargs):
        from kairos.organizaciones.models import Organizacion
        from kairos.retos.models import SolicitudReto
        kwargs = super(RetosRegistrarDesdeSolicitud, self).get_form_kwargs()
        solicitud = SolicitudReto.objects.get(pk=self.kwargs['id_solicitud'])
        kwargs['organizacion'] = solicitud.organizacion
        return kwargs

    def get_initial(self):
        from kairos.organizaciones.models import Organizacion
        from kairos.retos.models import SolicitudReto
        initial = super(RetosRegistrarDesdeSolicitud, self).get_initial()
        solicitud = SolicitudReto.objects.get(pk=self.kwargs['id_solicitud'])
        initial['organizacion'] = solicitud.organizacion
        initial['grupos_permitidos_lista'] = solicitud.grupos_permitidos
        return initial



class RetosModificar(LoginRequiredMixin, UpdateView):
    model = Retos
    form_class = RetosModelForm
    pk_url_kwarg = 'id_reto'
    template_name = "retos/registrar.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(RetosModificar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        import os
        reto = form.save(commit=False)
        #form.instance.grupos_permitidos = form.cleaned_data['grupos_permitidos_lista']
        reto.save()
        form.save_m2m()

        return super(RetosModificar, self).form_valid(form)

    def get_form_kwargs(self, **kwargs):
        from kairos.organizaciones.models import Organizacion
        kwargs = super(RetosModificar, self).get_form_kwargs()
        organizacion = Retos.objects.get(pk=self.kwargs['id_reto']).organizacion
        kwargs['organizacion'] = organizacion
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modificar'] = True
        return context

    def get_initial(self):
        from kairos.organizaciones.models import Organizacion
        from kairos.retos.models import Retos
        initial = super(RetosModificar, self).get_initial()
        reto = Retos.objects.get(pk=self.kwargs['id_reto'])
        initial['organizacion'] = reto.organizacion
        initial['grupos_permitidos_lista'] = reto.grupos_permitidos
        initial.update({'request':self.request})
        return initial

       

    def get_success_url(self):
        messages.success(self.request, "Reto modificado satisfactoriamente")
        return reverse_lazy('retos:modificar', kwargs={'id_reto': self.kwargs['id_reto']})


class ConsultarListaRetos(LoginRequiredMixin, ListView):
    template_name = "retos/consultar_retos.html"
    paginate_by = 12

    def get_queryset(self):
        consulta = self.obtener_consulta()
        return consulta['retos']

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator
        context = super(ConsultarListaRetos, self).get_context_data(**kwargs)

        usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()

        pagination = Paginator(list(consulta['retos']), self.paginate_by)

        context['retos'] = pagination.page(context['page_obj'].number)
        context['tag'] = consulta['tag']

        context['tipos'] = [
            {'nombre': 'Todos los Retos'},
        ]
        if usuario_actual.tipo == 'Joven' or usuario_actual.tipo == 'Organización':
            context['tipos'] .append({'nombre': 'Mis Retos'})
            context['tipos'] .append({'nombre': 'Favoritos'})
            if usuario_actual.tipo == 'Joven':
                context['tipos'] .append({'nombre': 'Solicitudes Enviadas'})
                context['tipos'] .append({'nombre': 'Solicitudes Recibidas'})

        return context

    def obtener_consulta(self):
        from kairos.core.utils import obtener_lista_usuario_caracteristicas
        from kairos.organizaciones.models import Organizacion
        usuario = self.obtener_usuario()
        tag = 'Todos los Retos'
        retos = []

        if 'tag' in self.request.GET:
            tag = self.request.GET['tag']
        elif list(self.request.GET):
            tag = list(self.request.GET)[0]
        else:
            tag = "Todos los Retos"

        if tag == 'Mis Retos':
            retos = Retos.objects.filter(organizacion_id=usuario.id)
        elif tag == 'Favoritos':
            retos = Retos.favoritas(usuario)  # obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_lista_usuarios_solicitudes())
        elif tag == 'Todos los Retos':
            retos = Retos.objects.all()  # obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_jovenes_no_miembros())
        elif tag == 'Solicitudes Enviadas':
            if usuario.tipo == 'Joven':
                solicitudes = SolicitudesRetosJovenes.objects.filter(joven=usuario, estado='Postulado').values_list('reto__id')
                retos = Retos.objects.filter(id__in=solicitudes)
        elif tag == 'Solicitudes Recibidas':
            if usuario.tipo == 'Joven':
                solicitudes = SolicitudesRetosJovenes.objects.filter(joven=usuario, estado='Invitado').values_list('reto__id')
                retos = Retos.objects.filter(id__in=solicitudes)

        return {'tag': tag, 'retos': retos}


    def obtener_usuario(self):
        from kairos.organizaciones.models import Organizacion
        from kairos.usuarios.models import Joven
        usuario = Usuario.objects.get(id=self.request.user.id)
        if self.request.user.tipo == 'Organización':
            Organizacion.objects.get(id=self.request.user.id)
        elif self.request.user.tipo == 'Joven':
            Joven.objects.get(id=self.request.user.id)
        return usuario


class ConsultarReto(LoginRequiredMixin, TemplateView):
    template_name = "retos/consultar_reto.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect("inicio")


        if not Retos.existe_reto_por_id(kwargs['pk']):
            messages.error(request, "El Reto que intenta consultar no existe")
            return redirect("retos:consultar_retos")

        self.request.session['registro_reto'] = 0
        return super(ConsultarReto, self).dispatch(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        from django.db.models import Count
        from kairos.core.utils import relacion_con_usuario

        context = dict()
        reto = Retos.objects.get(pk=kwargs['pk'])
        context['equipos'] = []
        for equipo in reto.obtener_equipos_miembros():
            context['equipos'].append({'equipo': equipo})

        context['cantidad_equipos'] = reto.obtener_equipos_miembros().count()
        context['cantidad_jovenes'] = reto.obtener_jovenes_miembros().count()
        context['inicio_reto'] = timezone.now() >= reto.fecha_inicio        
        context['reto'] = reto
        usuario_sesion = self.obtener_usuario()
        if usuario_sesion.tipo == 'Joven':
            context.update(relacion_con_usuario(usuario_sesion, reto))

        return context

    def obtener_usuario(self):
        from kairos.usuarios.models import Joven
        from kairos.organizaciones.models import Organizacion
        usuario = self.request.user
        if usuario.tipo == 'Joven':
            usuario = Joven.objects.get(id=self.request.user.id)
        elif usuario.tipo == 'Organización':
            usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario

class ConsultarResultadosParticipantes(LoginRequiredMixin, TemplateView):
    template_name = "retos/consultar_resultados.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect("inicio")


        if not Retos.existe_reto_por_id(kwargs['id_reto']):
            messages.error(request, "El Reto que intenta consultar no existe")
            return redirect("retos:consultar_retos")
       
        return super(ConsultarResultadosParticipantes, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):                

        context = super(ConsultarResultadosParticipantes, self).get_context_data(**kwargs)        
        reto = Retos.objects.get(pk=kwargs['id_reto'])        
        context['detalles'] = reto.obtener_puntajes_participantes()
        
        return context

class ConsultarResultadosParticipante(LoginRequiredMixin, TemplateView):
    template_name = "retos/consultar_resultados_participante.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect("inicio")


        if not Retos.existe_reto_por_id(kwargs['id_reto']):
            messages.error(request, "El Reto que intenta consultar no existe")
            return redirect("retos:consultar_retos")
       
        return super(ConsultarResultadosParticipante, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):        
        from kairos.retos.models import DetalleJovenReto, Competencia, CompetenciaCalificaciones
        
        context = super(ConsultarResultadosParticipante, self).get_context_data(**kwargs)        
        reto = Retos.objects.get(pk=kwargs['id_reto'])        

        if 'Equipos' in reto.grupos_permitidos:
            equipos = Equipo.objects.filter(miembros__in=[self.request.user])            
            detalles = DetalleEquipoReto.objects.filter(equipo__in=equipos, reto=reto)
        else:
            detalles = DetalleJovenReto.objects.filter(joven=self.request.user, reto=reto)        

        context['detalles'] = detalles.order_by('-calificacion_final')
        context['reto'] = reto

        return context

class RetoApiGuardarCuartoActo(APIView):
    def post(self, request):
        from kairos.retos.models import JovenRetoCuarto, DetalleJovenReto, EquipoRetoCuarto, DetalleEquipoReto
        from .utils import calificar_actos_joven, calificar_actos_equipo
        from kairos.equipos.models import Equipo
        '''
            API que Guarda las respuestas y genera la calificacion inicial de los Cuartos tipo Acto.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    id_participante: Identificador del Equipo o Joven que presenta el reto
                    id_cuarto: Identificador del cuarto que se esta realizando por parte del participante
                    id_detalle: Identificador del modelo que guarda la calificacion y los archivos subidos
                    id_reto: Identificador del reto que se esta realizando
                }

            Retorno:
                Si el reto es para Jovenes: Retorna vista de cuarto descanso para jovenes
                Si el reto es para Equipos: Retorna vista de cuarto descanso para Equipos
        '''
        
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        id_participante = int(data['id_participante'])
        id_cuarto = int(data['id_cuarto'])
        id_detalle = int(data['id_detalle'])
        id_reto = int(data['id_reto'])
        
        
        cuarto = Cuarto.objects.get(pk=id_cuarto)
        reto = Retos.objects.get(pk=id_reto)

        if reto.grupos_permitidos == 'Jóvenes':
            participante = Joven.objects.get(pk=id_participante)
            detalle = DetalleJovenReto.objects.get(pk=id_detalle)
            cuarto_reto = JovenRetoCuarto.objects.get(cuarto=cuarto, detalle=detalle)
            calificar_actos_joven(detalle, cuarto)
        else:
            participante = Equipo.objects.get(pk=id_participante)
            detalle = DetalleEquipoReto.objects.get(pk=id_detalle)
            cuarto_reto = EquipoRetoCuarto.objects.get(cuarto=cuarto, detalle=detalle)
            calificar_actos_equipo(detalle, cuarto)

        cuarto_reto.completado = True
        cuarto_reto.fecha_finalizacion = timezone.now()
        cuarto_reto.tiempo_transcurrido = (cuarto_reto.fecha_finalizacion - cuarto_reto.fecha_inicio).seconds
        cuarto_reto.save()

        if reto.grupos_permitidos == 'Jóvenes':            
            return Response({'url_redireccion': '/app/retos/cuarto-descanso/{}'.format(reto.id)})
        else:            
            return Response({'url_redireccion': '/app/retos/cuarto-descanso/{}/{}'.format(reto.id, id_participante)})

class RetoApiCargarPaginaFinalizacionMiembros(APIView):
    def post(self, request):
        from kairos.retos.models import JovenRetoCuarto, DetalleEquipoReto, EquipoRetoCuarto
        from .utils import calificar_actos_joven, calificar_actos_equipo
        from kairos.equipos.models import Equipo
        '''
            API que Guarda las respuestas y genera la calificacion inicial de los Cuartos tipo Acto.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    id_participante: Identificador del Equipo o Joven que presenta el reto
                    id_cuarto: Identificador del cuarto que se esta realizando por parte del participante
                    id_detalle: Identificador del modelo que guarda la calificacion y los archivos subidos
                    id_reto: Identificador del reto que se esta realizando
                }

            Retorno:
                Si el reto es para Jovenes: Retorna vista de cuarto descanso para jovenes
                Si el reto es para Equipos: Retorna vista de cuarto descanso para Equipos
        '''
        
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        id_equipo = int(data['id_equipo'])
        id_detalle = int(data['id_detalle'])
        id_reto = int(data['id_reto'])

        reto = Retos.objects.get(pk=id_reto)
        equipo = Equipo.objects.get(pk=id_equipo)
        detalle = DetalleEquipoReto.objects.get(pk=id_detalle)

             
        if detalle.completado == True:            
            return Response({'url_redireccion': '/app/retos/cuarto-finalizacion/{}/{}'.format(reto.id, id_equipo)})
        elif detalle.abandonado == True:            
            return Response({'url_redireccion': '/app/retos/cuarto-finalizacion/{}/{}'.format(reto.id, id_equipo)})
        else:            
            return Response({'url_redireccion':'Espera'})

class RetoApiConfirmarRetoFinalizado(APIView):
    def post(self, request):
        from kairos.retos.models import JovenRetoCuarto, DetalleEquipoReto, DetalleJovenReto, EquipoRetoCuarto
        from .utils import calificar_actos_joven, calificar_actos_equipo
        from kairos.equipos.models import Equipo
        '''
            API que Confirma si un reto ha finalizado y redirecciona al cuarto final de ser asi.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{                    
                    id_detalle: Identificador del modelo que guarda la calificacion y los archivos subidos
                    id_reto: Identificador del reto que se esta realizando               
                }

            Retorno:
                
        '''
        
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        if 'id_detalle' in data.keys() and 'id_reto' in data.keys():
            print(data)
            id_detalle = int(data['id_detalle'])
            id_reto = int(data['id_reto'])
            
            reto = Retos.objects.get(pk=id_reto)
            if 'Equipos' in reto.grupos_permitidos:            
                detalle = DetalleEquipoReto.objects.get(pk=id_detalle)
                participante = detalle.equipo
                url = '/app/retos/cuarto-finalizacion/{}/{}'.format(reto.id, participante.pk)
            else:
                detalle = DetalleJovenReto.objects.get(pk=id_detalle)
                participante = detalle.joven
                url = '/app/retos/cuarto-finalizacion/{}'.format(reto.id)
            
            if reto.reto_finalizado():
                return Response({'url_redireccion': url})
                print(url)
        return Response({'url_redireccion':'Espera'})
             
        '''if detalle.completado == True:            
            return Response({'url_redireccion': url})
        elif detalle.abandonado == True:            
            return Response({'url_redireccion': '/app/retos/cuarto-finalizacion/{}/{}'.format(reto.id, id_equipo)})
        else:            
            return Response({'url_redireccion':'Espera'})'''

class RetoApiActualizarBotonRetoInicio(APIView):
    def post(self, request):
        from kairos.retos.models import JovenRetoCuarto, DetalleEquipoReto, EquipoRetoCuarto
        from .utils import calificar_actos_joven, calificar_actos_equipo
        from kairos.equipos.models import Equipo
        '''
            API que Actualiza el boton de inicio reto.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{                   
                    id_reto: Identificador del reto que se esta realizando
                }

            Retorno:
                Si el reto ha iniciado: {'Inicio':True}
                Si el reto NO ha iniciado: {'Inicio':False}
        '''
        
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
       
        id_reto = int(data['id_reto'])

        reto = Retos.objects.get(pk=id_reto)
        

        fecha_actual = timezone.now()
        fecha_inicio = reto.fecha_inicio
    
        if  fecha_actual <= fecha_inicio:            
            return Response({'Inicio':False})
        else:           
            return Response({'Inicio':True})

class RetoApiGuardarCuartoPregunta(APIView):
    def post(self, request):
        from kairos.retos.models import JovenRetoCuarto, DetalleJovenReto, EquipoRetoCuarto, DetalleEquipoReto
        from .utils import calificar_preguntas_joven, calificar_preguntas_equipo
        from kairos.equipos.models import Equipo
        '''
            API que Guarda las respuestas y genera la calificacion inicial de los Cuartos tipo Pregunta.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    id_participante: Identificador del Equipo o Joven que presenta el reto
                    id_cuarto: Identificador del cuarto que se esta realizando por parte del participante
                    id_detalle: Identificador del modelo que guarda la calificacion y las respuestas a las preguntas
                    id_reto: Identificador del reto que se esta realizando
                }

            Retorno:
                Si el reto es para Jovenes: Retorna vista de cuarto descanso para jovenes
                Si el reto es para Equipos: Retorna vista de cuarto descanso para Equipos
        '''
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        id_participante = int(data['id_participante'])
        id_cuarto = int(data['id_cuarto'])
        id_detalle = int(data['id_detalle'])
        id_reto = int(data['id_reto'])

        id_pregunta = int(data['id_pregunta'])
        id_opcion = int(data['id_opcion'])

        cuarto = Cuarto.objects.get(pk=id_cuarto)
        reto = Retos.objects.get(pk=id_reto)

        if reto.grupos_permitidos == 'Jóvenes':
            participante = Joven.objects.get(pk=id_participante)
            detalle = DetalleJovenReto.objects.get(pk=id_detalle)
            cuarto_reto = JovenRetoCuarto.objects.get(cuarto=cuarto, detalle=detalle)
            calificar_preguntas_joven(detalle, id_pregunta, id_opcion)
        else:
            participante = Equipo.objects.get(pk=id_participante)
            detalle = DetalleEquipoReto.objects.get(pk=id_detalle)
            cuarto_reto = EquipoRetoCuarto.objects.get(cuarto=cuarto, detalle=detalle)
            calificar_preguntas_equipo(detalle, id_pregunta, id_opcion)


        return Response()

class RetoApiEnviarRespuestaPregunta(APIView):
    def post(self, request):
        from kairos.retos.models import JovenRetoCuarto, DetalleJovenReto, EquipoRetoCuarto, DetalleEquipoReto, CalificacionPreguntaJoven, CalificacionPreguntaEquipo        
        from kairos.equipos.models import Equipo
        '''
            API que cambia el estado del detalle pregunta a completado.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    id_participante: Identificador del Equipo o Joven que presenta el reto
                    id_cuarto: Identificador del cuarto que se esta realizando por parte del participante
                    id_detalle: Identificador del modelo que guarda la calificacion y las respuestas a las preguntas
                    id_reto: Identificador del reto que se esta realizando
                }

            Retorno:
                Si el reto es para Jovenes: Retorna vista de cuarto descanso para jovenes
                Si el reto es para Equipos: Retorna vista de cuarto descanso para Equipos
        '''
        json_text = request.data.get('json_text')
        data = json.loads(json_text)

        id_detalle = int(data['id_detalle'])
        id_reto = int(data['id_reto'])
        id_pregunta = int(data['id_pregunta'])

        reto = Retos.objects.get(pk=id_reto)

        if reto.grupos_permitidos == 'Jóvenes':
            detalle = DetalleJovenReto.objects.get(pk=id_detalle)
            calificacion = CalificacionPreguntaJoven.objects.get(detalle_joven_reto=detalle, pregunta__id=id_pregunta)
        else:
            detalle = DetalleEquipoReto.objects.get(pk=id_detalle)
            calificacion = CalificacionPreguntaEquipo.objects.get(detalle_equipo_reto=detalle, pregunta__id=id_pregunta)

        calificacion.envio_pregunta = True
        calificacion.save()

        return Response()

class RetoApiCalificarCompetenciasJoven(APIView):
    def post(self, request):
        from kairos.retos.models import Competencia, CompetenciaCalificaciones        
        
        '''
            API que cambia el estado del detalle pregunta a completado.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    id_participante: Identificador del Equipo o Joven que presenta el reto
                    id_cuarto: Identificador del cuarto que se esta realizando por parte del participante
                    id_detalle: Identificador del modelo que guarda la calificacion y las respuestas a las preguntas
                    id_reto: Identificador del reto que se esta realizando
                }

            Retorno:
                Si el reto es para Jovenes: Retorna vista de cuarto descanso para jovenes
                Si el reto es para Equipos: Retorna vista de cuarto descanso para Equipos
        '''
        json_text = request.data.get('json_text')
        data = json.loads(json_text)

        id_competencia = int(data['id_competencia'])
        id_joven_calificado = int(data['id_joven'])
        id_joven_califica = int(data['id_joven_califica'])
        id_equipo = int(data['id_equipo'])
        id_reto = int(data['id_reto'])       

        try:
            calificacion = CompetenciaCalificaciones.objects.get(calificaciones__id_joven=id_joven_calificado, calificaciones__id_equipo=id_equipo, calificaciones__id_reto=id_reto)
            calificacion.calificaciones['calificacion'][str(id_joven_califica)] = id_competencia
            calificacion.save()
            
        except CompetenciaCalificaciones.DoesNotExist:
            aux = {                
                'id_reto': id_reto,
                'id_equipo': id_equipo,
                'id_joven': id_joven_calificado,
                'calificacion': {str(id_joven_califica):id_competencia}
            }
            calificacion = CompetenciaCalificaciones.objects.create(calificaciones=aux)

        return Response({'data':'data'})


class RetoApiGuardarTiempoPregunta(APIView):
    def post(self, request):
        from kairos.retos.models import JovenRetoCuarto, DetalleJovenReto, EquipoRetoCuarto, CalificacionPreguntaJoven, CalificacionPreguntaEquipo, DetalleEquipoReto
        from kairos.cuartos.models import Pregunta
        from kairos.equipos.models import Equipo
        from datetime import timedelta
        '''
            API que Guarda la hora inicial en la que se empezo a contestar una pregunta.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    id_participante: Identificador del Equipo o Joven que presenta el reto
                    id_pregunta: Identificador de la pregunta que se esta respondiendo
                    id_detalle: Identificador del modelo que guarda la calificacion y las respuestas a las preguntas
                    id_reto: Identificador del reto que se esta realizando
                }

            Retorno:
                Si el reto es para Jovenes: Retorna vista de cuarto descanso para jovenes
                Si el reto es para Equipos: Retorna vista de cuarto descanso para Equipos
        '''

        json_text = request.data.get('json_text')
        data = json.loads(json_text)

        id_participante = int(data['id_participante'])
        id_detalle = int(data['id_detalle'])
        id_reto = int(data['id_reto'])
        id_pregunta = int(data['id_pregunta'])

        pregunta = Pregunta.objects.get(id=id_pregunta)
        reto = Retos.objects.get(pk=id_reto)


        if reto.grupos_permitidos == 'Jóvenes':
            try:
                detalle = DetalleJovenReto.objects.get(pk=id_detalle)
                calificacion = CalificacionPreguntaJoven.objects.get(detalle_joven_reto=detalle, pregunta=pregunta)
            except:
                CalificacionPreguntaJoven.crear_calificacion(detalle_joven_reto=detalle, pregunta=pregunta)
                calificacion = CalificacionPreguntaJoven.objects.get(detalle_joven_reto=detalle, pregunta=pregunta)
        else:
            try:
                detalle = DetalleEquipoReto.objects.get(pk=id_detalle)
                calificacion = CalificacionPreguntaEquipo.objects.get(detalle_equipo_reto=detalle, pregunta=pregunta)
            except:
                CalificacionPreguntaEquipo.crear_calificacion(detalle_equipo_reto=detalle, pregunta=pregunta)
                calificacion = CalificacionPreguntaEquipo.objects.get(detalle_equipo_reto=detalle, pregunta=pregunta)

        if calificacion.fecha_inicio is None:
            calificacion.fecha_inicio = timezone.now()
            calificacion.fecha_finalizacion = (calificacion.fecha_inicio+timedelta(seconds=pregunta.tiempo_limite))
            calificacion.save()

        tiempo_faltante = 0
        if calificacion.fecha_finalizacion > timezone.now():
            tiempo_faltante = (calificacion.fecha_finalizacion - timezone.now()).seconds
        print("##############################")
        print(tiempo_faltante)
        print("##############################")
        return Response({'tiempo_faltante':tiempo_faltante})


class RetoApiCargarCalificaciones(APIView):
    def post(self, request):
        '''
            API que Carga la calificación(Me gusta, no me gusta) de un Usuario hacia los retos en la plataforma

            Parametros:
                request: Elemento Http que posee informacion de la sesion.

            Retorno:
                Lista de diccionarios que contienen el id del reto y la calificaciön
        '''
        json_text = request.data.get('json_text')
        pk_usuario = self.request.user.pk
        calificaciones = CalificacionRetos.objects.filter(usuario=pk_usuario)
        cals = []
        for c in calificaciones:
            cals.append({'pk': c.reto.pk, 'stars': c.calificacion})
        return Response({'calificaciones': cals})


class RetoApiCargarCalificacion(APIView):
    def post(self, request):
        '''
            API que Carga la calificación(Me gusta, no me gusta) de un Usuario hacia un retos especifico.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    pk_contenido: Identificador del reto a consultar calificación
                }
            Retorno:
                Diccionario que contienen el id del reto y la calificaciön
        '''
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_reto = data["pk_contenido"]
        pk_usuario = self.request.user.pk
        c = CalificacionRetos.objects.get(usuario=pk_usuario, reto=pk_reto)
        cal = [{'pk': c.reto.pk, 'stars': c.calificacion}]
        return Response({'calificaciones': cal})


class RetoApiCalificarContenido(APIView):
    def post(self, request):
        '''
            API que guarda la calificacion(Me gusta, no me gusta) de un Usuario hacia un retos especifico.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    pk_contenido: Identificador del reto a consultar calificación
                }
        '''
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_reto = data["pk_contenido"]
        pk_usuario = self.request.user.pk
        if "star" in data:
            calificacion, creado = CalificacionRetos.objects.get_or_create(
                reto_id=pk_reto, usuario_id=pk_usuario
            )
            if data['star'] == 0 or data['star'] == 1:
                calificacion.calificacion = data['star']
                calificacion.save()
            else:
                calificacion.delete()
        return Response()

class RetoApiRecalificarCuartoActo(APIView):
    def post(self, request):
        from kairos.retos.models import JovenRetoCuarto, DetalleJovenReto, EquipoRetoCuarto, DetalleEquipoReto
        from kairos.cuartos.models import Acto
        from .utils import recalificar_acto_joven, recalificar_acto_equipo, obtener_puntaje_final
        from kairos.equipos.models import Equipo
        '''
            API que recalifica las calificaciones de un acto.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    id_participante: Identificador del Equipo o Joven que presenta el reto
                    id_cuarto: Identificador del cuarto que se esta realizando por parte del participante
                    id_detalle: Identificador del modelo que guarda la calificacion y las respuestas a las preguntas
                    id_reto: Identificador del reto que se esta realizando
                    id_acto: identificador del acto que se esta recalificando
                    veredicto: El veredicto que dio el jurado sobre el acrhivo subido
                }
        '''
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        id_participante = int(data['id_participante'])
        id_cuarto = int(data['id_cuarto'])
        id_detalle = int(data['id_detalle'])
        id_reto = int(data['id_reto'])
        id_acto = int(data['id_acto'])
        veredicto = str(data['veredicto'])

        cuarto = Cuarto.objects.get(pk=id_cuarto)
        reto = Retos.objects.get(pk=id_reto)
        acto = Acto.objects.get(pk=id_acto)

        if reto.grupos_permitidos == 'Jóvenes':
            participante = Joven.objects.get(pk=id_participante)
            detalle = DetalleJovenReto.objects.get(pk=id_detalle)
            recalificar_acto_joven(detalle, cuarto, acto, veredicto)
        else:
            participante = Equipo.objects.get(pk=id_participante)
            detalle = DetalleEquipoReto.objects.get(pk=id_detalle)
            recalificar_acto_equipo(detalle, cuarto, acto, veredicto)

        obtener_puntaje_final(reto, detalle, recalificar=True)
        return Response()

class RetoApiActualizarRanking(APIView):
    def post(self, request):
        from kairos.retos.models import JovenRetoCuarto, DetalleJovenReto, EquipoRetoCuarto, DetalleEquipoReto
        from kairos.cuartos.models import Acto
        from .utils import obtener_puntaje_final
        from kairos.equipos.models import Equipo
        '''
            API que actualiza el ranking despues de la recalificacion del jurado.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    id_detalle: Identificador del modelo que guarda la calificacion y las respuestas a las preguntas
                    id_reto: Identificador del reto que se esta realizando
                }

            Retorno:
                Si el reto es para Jovenes: Retorna vista de cuarto descanso para jovenes
                Si el reto es para Equipos: Retorna vista de cuarto descanso para Equipos
        '''
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        id_detalle = int(data['id_detalle'])
        id_reto = int(data['id_reto'])

        reto = Retos.objects.get(pk=id_reto)

        if reto.grupos_permitidos == 'Jóvenes':
            detalle = DetalleJovenReto.objects.get(pk=id_detalle)            
        else:
            detalle = DetalleEquipoReto.objects.get(pk=id_detalle)
        
        obtener_puntaje_final(reto, detalle)

        return Response()


class ModificarSolicitudesRetos(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = SolicitudReto
    pk_url_kwarg = 'id_solicitud_reto'
    form_class = FormularioSolicitudReto
    template_name = "organizaciones/retos/gestionar_retos.html"
    success_url = reverse_lazy('organizaciones:organizacion_gestion_solicitudes_retos')
    mensaje_exito = 'Datos de Solicitud modificados correctamente!'
    mensaje_error = 'Ha ocurrido un error al guardar los nuevos datos de la Solicitud, por favor intente de nuevo!'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated and request.user.tipo != 'Organización':
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(ModificarSolicitudesRetos, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['modificar'] = True
        return data

    def get_initial(self):
        initial = super(ModificarSolicitudesRetos, self).get_initial()
        solicitud = self.get_object()
        initial['grupos_permitidos_lista'] = solicitud.grupos_permitidos
        return initial

    def form_valid(self, form):
        form.instance.grupos_permitidos = form.cleaned_data['grupos_permitidos_lista']
        return super(ModificarSolicitudesRetos, self).form_valid(form)

    def get_object(self):
        return SolicitudReto.objects.get(pk=self.kwargs['id_solicitud_reto'])

class GestionDeSolicitudesRetosOrganizaciones(LoginRequiredMixin, ListView):
    model = SolicitudReto
    context_object_name = "solicitudes_retos"
    template_name = "retos/solicitudes/solicitudes_retos_organizaciones.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated and request.user.tipo != 'Organización':
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(GestionDeSolicitudesRetosOrganizaciones, self).dispatch(request, *args, **kwargs)


class GestionDeSolicitudesRetosJovenes(LoginRequiredMixin, ListView):
    model = SolicitudesRetosJovenes
    context_object_name = "solicitudes_retos"
    template_name = "retos/solicitudes/solicitudes_jovenes.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated and request.user.tipo != 'Organización':
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(GestionDeSolicitudesRetosJovenes, self).dispatch(request, *args, **kwargs)

class GestionDeSolicitudesRetosJovenesEquipos(LoginRequiredMixin, TemplateView):
    template_name = "retos/solicitudes/solicitudes_equipos_jovenes.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated and request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        request.session['registro_reto'] = 1
        return super(GestionDeSolicitudesRetosJovenesEquipos, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = dict()
        #  usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()

        context['solicitudes'] = consulta['solicitudes']
        context['tag'] = consulta['tag']
        context['nombre_columnas'] = consulta['nombre_columnas']
        context['filtro_actual'] = consulta['filtro']
        context['tipos'] = [{'nombre': 'Todos'},
                            {'nombre': 'Solicitudes Enviadas'},
                            {'nombre': 'Solicitudes Recibidas'},
        ]

        return context

    def obtener_consulta(self):
        from kairos.usuarios.models import Joven
        from kairos.retos.models import SolicitudesRetosEquipos, SolicitudesRetosJovenes

        usuario_filtro = self.obtener_usuario_filtro()
        tag = 'Todos'
        solicitudes = []
        grupo_actual = SolicitudesRetosJovenes.objects
        filtro = 'Jóvenes'
        nombres_columnas = ['Joven', 'Reto', 'Estado', 'Fecha Creación', 'Acciones']
        if 'q' in self.request.GET:
            filtro = self.request.GET['q']
            if filtro == 'Jóvenes':
                grupo_actual = SolicitudesRetosJovenes.objects
            elif filtro == 'Equipos':
                grupo_actual = SolicitudesRetosEquipos.objects
                nombres_columnas[0] = 'Equipos'

        if self.request.GET:
            if 'tag' in self.request.GET:
                tag = self.request.GET['tag']

        if tag == 'Todos':
            solicitudes = grupo_actual.filter(**usuario_filtro)
        elif tag == 'Solicitudes Enviadas':
            solicitudes = grupo_actual.filter(estado='Invitado', **usuario_filtro)
        elif tag == 'Solicitudes Recibidas':
            solicitudes = grupo_actual.filter(estado='Postulado', **usuario_filtro)

        return {'tag': tag, 'solicitudes': solicitudes, 'nombre_columnas': nombres_columnas, 'filtro': filtro}

    def obtener_usuario_filtro(self):
        from kairos.organizaciones.models import Organizacion

        filtro = dict()
        if self.request.user.is_superuser:
            usuario = Usuario.objects.get(id=self.request.user.id)
        else:
            usuario = Organizacion.objects.get(id=self.request.user.id)
            filtro['{0}__{1}'.format('reto','organizacion')] = usuario.pk
        return filtro

# ############################################################
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''                SOLICITUDES A JOVEN                   '''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# ############################################################

# usuario_tarjeta hace referencia a donde se esta realizando la accion:
# request.session['registro_reto']=0 hace referencia al template en retos->consultar-reto.html
# request.session['registro_reto']=1 hace referencia al template en retos->solicitudes->solicitudes_equipos_jovenes.html

@login_required
def RetoJovenEnviarSolicitud(request, id_joven, id_reto):
    from kairos.retos.models import SolicitudesRetosJovenes
    '''
        Se encarga de enviar solicitudes de un Joven o Reto para postularse o ser invitado a un Reto.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_joven: Identificador Joven que envia/recibe la invitacion
            id_reto: Identificador Retos que envia/recibe la invitacion
    '''
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.is_superuser):
        if Usuario.existe_usuario(id_joven) and Retos.existe_reto_por_id(id_reto):
            joven = Joven.objects.get(id=id_joven)
            reto = Retos.objects.get(id=id_reto)

            if request.user.is_superuser:
                estado = 'Invitado'
            else:
                estado = 'Postulado'

            if SolicitudesRetosJovenes.existe_solitud(reto=reto, joven=joven) == True:
                messages.error(request, "Ya se ha enviado una solicitud")
            else:
                messages.success(request, "Solicitud enviada correctamente")
                SolicitudesRetosJovenes.objects.create(reto=reto, joven=joven, estado=estado)
        else:
            messages.error(request, "No existe el receptor")
    else:
        messages.error(request, "No esta autorizado para realizar esa acción")

    if request.session['registro_reto'] == 0:
        return redirect('retos:consultar_reto', id_reto)
    elif request.session['registro_reto'] == 1:
        return redirect('retos:solicitudes_solicitudes_jovenes_equipos')
    else:
        return redirect('retos:consultar_reto', id_reto)


@login_required
def RetoJovenRechazarSolicitud(request, id_joven, id_reto):
    from kairos.retos.models import SolicitudesRetosJovenes
    from kairos.gestor_correos.envio_correos import enviar_correo, ENIVAR_RECHAZO_JOVEN_RETO
    '''
        Se encarga de rechazar las solicitudes de un Joven o Reto enviadas o recibidas.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_joven: Identificador Joven que envia/recibe la invitacion
            id_reto: Identificador Retos que envia/recibe la invitacion
    '''
    
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización' or request.user.is_superuser):

        if Usuario.existe_usuario(id_joven) and Retos.existe_reto_por_id(id_reto):

            joven = Joven.objects.get(id=id_joven)
            reto = Retos.objects.get(id=id_reto)

            if SolicitudesRetosJovenes.existe_solitud(reto=reto, joven=joven) == True:
                mensaje = ""
                if SolicitudesRetosJovenes.objects.get(reto=reto, joven=joven).estado == 'Invitado':
                    if request.user.tipo == 'Joven':
                        mensaje = "Solicitud de miembro rechazada"
                    else:
                        mensaje = "Solicitud de miembro cancelada"
                else:
                    if request.user.tipo == 'Joven':
                        mensaje = "Solicitud de miembro cancelada"
                    else:
                        mensaje = "Solicitud de miembro rechazada"
                        enviar_correo(joven.email, reto, ENIVAR_RECHAZO_JOVEN_RETO, joven)

                SolicitudesRetosJovenes.objects.get(reto=reto, joven=joven).delete()
                messages.success(request, mensaje)
            else:
                messages.error(request, "No tienes solicitudes de miembro con el usuario")
        else:
            messages.error(request, "No existe el receptor")

    else:
        messages.error(request, "El usuario no esta autorizado para realizar esa acción.")
    
 

    if request.session['registro_reto'] == 0:
        return redirect('retos:consultar_reto', id_reto)
    elif request.session['registro_reto'] == 1:
        return redirect('retos:solicitudes_solicitudes_jovenes_equipos')
    elif request.session['registro_reto'] == 2:
        return redirect('retos:retos_invitar_equipos_jovenes', id_reto=id_reto)
    else:
        return redirect('retos:consultar_reto', id_reto)

@login_required
def RetoJovenAceptarSolicitud(request, id_joven, id_reto):
    from kairos.retos.models import SolicitudesRetosJovenes, DetalleJovenReto
    from kairos.gestor_correos.envio_correos import enviar_correo, ENIVAR_ACEPTACION_JOVEN_RETO
    '''
        Se encarga de aceptar las solicitudes de un Joven o Reto enviadas o recibidas.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_joven: Identificador Joven que envia/recibe la invitacion
            id_reto: Identificador Retos que envia/recibe la invitacion
    '''
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización' or request.user.is_superuser):

        if Usuario.existe_usuario(id_joven) and Retos.existe_reto_por_id(id_reto):

            joven = Joven.objects.get(id=id_joven)
            reto = Retos.objects.get(id=id_reto)

            if SolicitudesRetosJovenes.existe_solitud(reto=reto, joven=joven) == True:
                solicitud = SolicitudesRetosJovenes.objects.get(reto=reto, joven=joven)

                if DetalleJovenReto.existe_detalle(joven=joven, reto=reto) == False:
                    DetalleJovenReto.crear_detalle(joven=joven, reto=reto)
                else:
                    messages.error(request, "Ya estas inscrito en el Reto")

                enviar_correo(joven.email, reto, ENIVAR_ACEPTACION_JOVEN_RETO, joven)
                solicitud.delete()
                messages.success(request, "Solicitud de miembro aceptada")
            else:
                messages.error(request, "No tienes solicitudes de miembro con el usuario")

        else:
            messages.error(request, "No existe el receptor")

    else:
        messages.error(request, "El usuario no esta autorizado para esa acción")

   
    if request.session['registro_reto'] == 0:
        return redirect('retos:consultar_reto', id_reto)
    elif request.session['registro_reto'] == 1:
        return redirect('retos:solicitudes_solicitudes_jovenes_equipos')
    else:
        return redirect('retos:consultar_reto', id_reto)


@login_required
def RetoJovenFinalizarRelacion(request, id_joven, id_reto):
    '''
        Permite a un joven salir o ser expulsado de un Reto.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_joven: Identificador Joven que envia/recibe la invitacion
            id_reto: Identificador Retos que envia/recibe la invitacion
    '''
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización' or request.user.is_superuser):
        if Usuario.existe_usuario(id_joven) and Retos.existe_reto_por_id(id_reto):
            relacion = False

            joven = Joven.objects.get(id=id_joven)
            reto = Retos.objects.get(id=id_reto)

            if reto.joven_esta_inscrito(joven) == True:
                mensaje = ""

                if request.user.tipo == 'Joven':
                    mensaje = "Has dejado de la organización"
                else:
                    mensaje = "El usuario ha sido expulsado de la organización"

                reto.jovenes.remove(joven)
                messages.success(request, mensaje)
            else:
                messages.error(request, "No hay una relación de miembro con el usuario")
        else:
            messages.error(request, "No existe el receptor")
    else:
        messages.error(request, "El usuario no esta autorizado para esa acción")

    if request.session['registro_reto'] == 0:
        return redirect('retos:consultar_reto', id_reto)
    elif request.session['registro_reto'] == 1:
        return redirect('retos:solicitudes_solicitudes_jovenes_equipos')
    elif request.session['registro_reto'] == 3:
        return redirect('retos:retos_expulsar_miembros', id_reto=id_reto)
    else:
        return redirect('retos:consultar_reto', id_reto)


@login_required
def PostulacionLiderReto(request, id_joven, id_reto):
    '''
        Permite a un Lider de equipo postularse a un reto.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_joven: Identificador Joven lider de Equipo que envia/recibe la invitacion
            id_reto: Identificador Retos que envia/recibe la invitacion
    '''
    from kairos.retos.models import SolicitudesRetosEquipos
    from kairos.equipos.models import Equipo
    from django.db.models import Count
    
    if not request.user.is_authenticated and (request.user.tipo != 'Joven' or not request.user.is_superuser):
        messages.error(request, "El usuario no esta autorizado para esa acción")
        return redirect('inicio')

    reto = Retos.buscar(id_reto)
    joven_lider = Joven.buscar(id_joven)

    if reto == None or joven_lider == None:
        return redirect("inicio")

    request.session['postulacion_reto'] = id_reto
    equipos_lider = reto.equipos.all()
    equipos = reto.solicitudes_equipos.all()
    if not request.user.is_superuser:
        equipos_lider = reto.equipos.filter(lider=request.user)
        equipos = reto.solicitudes_equipos.filter(lider=request.user)
            
    relacion_equipo_reto = []
    for equipo in joven_lider.equipos_del_lider.all():
        solicitudes = SolicitudesRetosEquipos.objects.filter(reto=reto, equipo=equipo)
        if equipo in equipos_lider:
            if solicitudes:
                relacion_equipo_reto.append({'equipo':equipo, 'reto':reto, 'relacion':'Postulado'})
            else:
                relacion_equipo_reto.append({'equipo':equipo, 'reto':reto, 'relacion':'Aceptado'})
        for solicitud in solicitudes:
            if solicitud.equipo == equipo:
                relacion_equipo_reto.append({'equipo':equipo, 'reto':reto, 'relacion':solicitud.estado})

    form_postulacion = PostulacionLiderRetoForm(instance=reto, joven_lider=joven_lider, reto=reto)

    if request.method == 'POST':
        if 'enviar_postulacion' in request.POST:
            form_postulacion = PostulacionLiderRetoForm(request.POST, instance=reto, joven_lider=joven_lider, reto=reto)
            if form_postulacion.is_valid():
                for id_equipo in request.POST.getlist('solicitudes_equipos'):
                    equipo = Equipo.buscar(id_equipo)
                    if not equipo in equipos:
                        postulacion = SolicitudesRetosEquipos.objects.create(reto=reto, equipo=equipo, estado='Postulado')
                messages.success(request, 'Postulación enviada a equipo')

                return redirect('retos:retos_enviar_solicitud_equipo', id_joven=request.user.id, id_reto=id_reto)


        if 'finalizar' in request.POST:
            return redirect('retos:consultar_reto', pk=id_reto)
    return render(request, 'equipos/retos/postular_equipo.html',{
        'form_postulacion':form_postulacion,
        'equipos':equipos,
        'reto':reto,
        'relacion_equipo_reto':relacion_equipo_reto,
        })

@login_required
def InvitarEquiposJovenesAdministrador(request, id_reto):
    from kairos.retos.models import SolicitudesRetosEquipos, SolicitudesRetosJovenes
    from kairos.equipos.models import Equipo
    '''
        Permite al administrador invitar a Jovenes o Equipos a un Reto.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identificador Retos que envia/recibe la invitacion.
    '''
    if not request.user.is_authenticated and (request.user.tipo == 'Joven' or request.user.is_superuser):
        messages.error(request, "Usted no está autorizado para realizar la acción!")
        return redirect("inicio")
    request.session['registro_reto'] = 2

    reto = Retos.buscar(id_reto)
    if reto == None:
        return redirect("inicio")

    equipos = reto.solicitudes_equipos.all()
    jovenes = reto.solicitudes_jovenes.all()
    form_postulacion = InvitacionAdministradorRetoForm(instance=reto)

    if request.method == 'POST':
        if 'enviar_postulacion' in request.POST:
            form_postulacion = InvitacionAdministradorRetoForm(request.POST, instance=reto)
            if form_postulacion.is_valid():
                for id_equipo in request.POST.getlist('solicitudes_equipos'):
                    equipo = Equipo.buscar(id_equipo)
                    if not equipo in equipos:
                        postulacion = SolicitudesRetosEquipos.objects.create(reto=reto, equipo=equipo, estado='Invitado')

                for id_joven in request.POST.getlist('solicitudes_jovenes'):
                    joven = Joven.buscar(id_joven)
                    if not joven in jovenes:
                        postulacion = SolicitudesRetosJovenes.objects.create(reto=reto, joven=joven, estado='Invitado')
                messages.success(request, 'Invitaciones enviadas')

                return redirect('retos:retos_invitar_equipos_jovenes', id_reto=id_reto)

        if 'finalizar' in request.POST:
            return redirect('retos:consultar')

    return render(request, 'retos/invitaciones/invitar_equipos.html',{
        'form_postulacion': form_postulacion,
        'equipos': equipos,
        'jovenes': jovenes,
        'reto': reto
    })

@login_required
def ExpulsarEquiposJovenesAdministrador(request, id_reto):
    from kairos.retos.models import SolicitudesRetosEquipos, SolicitudesRetosJovenes
    from kairos.equipos.models import Equipo
    '''
        Permite al administrador expulsar a Jovenes o Equipos a un Reto.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identificador Retos que envia/recibe la invitacion.
    '''
    if request.user.is_authenticated == False and (request.user.is_superuser == False or request.user.tipo == 'Organización'):
        messages.error(request, "Usted no está autorizado para realizar la acción!")
        return redirect("inicio")

    request.session['registro_reto'] = 3

    reto = Retos.buscar(id_reto)
    if reto == None:
        return redirect("inicio")

    equipos = reto.equipos.all()
    jovenes = reto.jovenes.all()

    if request.method == 'POST':
        if 'finalizar' in request.POST:
            return redirect('retos:consultar')

    return render(request, 'retos/invitaciones/expulsar_miembros.html',{
        'equipos': equipos,
        'jovenes': jovenes,
        'reto': reto
    })

# ############################################################
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''                SOLICITUDES A EQUIPO                   '''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# ############################################################

# usuario_tarjeta hace referencia a donde se esta realizando la accion:
# request.session['registro_reto']=0 hace referencia al template en retos->consultar-reto.html
# request.session['registro_reto']=1 hace referencia al template en retos->solicitudes->solicitudes_equipos_jovenes.html

@login_required
def RetoEquipoRetirarPostulacion(request, id_equipo, id_reto):
    from kairos.retos.models import SolicitudesRetosEquipos
    from kairos.gestor_correos.envio_correos import enviar_correo, ENIVAR_RECHAZO_EQUIPO_RETO

    '''
        Permite a un Equipo retirar su postulacion de un Reto

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: Identificador del Equipo envio la postulación.
            id_reto: Identificador Retos que envia/recibe la invitacion.
    '''
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización' or request.user.is_superuser):

        if Equipo.existe_equipo(id_equipo) and Retos.existe_reto_por_id(id_reto):

            equipo = Equipo.objects.get(id=id_equipo)
            reto = Retos.objects.get(id=id_reto)

            if SolicitudesRetosEquipos.existe_solitud(reto=reto, equipo=equipo) == True:
                mensaje = ""
                if SolicitudesRetosEquipos.objects.get(reto=reto, equipo=equipo).estado == 'Invitado':
                    if request.user.tipo == 'Joven':
                        mensaje = "Solicitud de miembro rechazada"
                    else:
                        mensaje = "Solicitud de miembro cancelada"
                else:
                    if request.user.tipo == 'Joven':
                        mensaje = "Solicitud de miembro cancelada"
                    else:
                        mensaje = "Solicitud de miembro rechazada"
                        for joven in equipo.obtener_miembros():
                            enviar_correo(joven.email, (reto, equipo), ENIVAR_RECHAZO_EQUIPO_RETO, joven)

                SolicitudesRetosEquipos.objects.get(reto=reto, equipo=equipo).delete()
                messages.success(request, mensaje)
            else:
                messages.error(request, "No tienes solicitudes de miembro con el Equipo")
        else:
            messages.error(request, "No existe el receptor")

    else:
        messages.error(request, "El usuario no esta autorizado para realizar esa acción.")

    
    if request.session['registro_reto']  == 0:
        return redirect('retos:retos_enviar_solicitud_equipo', id_joven=request.user.id, id_reto=id_reto)
    elif request.session['registro_reto'] == 1:
        return redirect('retos:solicitudes_solicitudes_jovenes_equipos')
    elif request.session['registro_reto'] == 2:
        return redirect('retos:retos_invitar_equipos_jovenes', id_reto=id_reto)
    else:
        return redirect('retos:retos_enviar_solicitud_equipo', id_joven=request.user.id, id_reto=id_reto)

@login_required
def RetoEquipoAceptarSolicitud(request, id_equipo, id_reto):
    from kairos.retos.models import SolicitudesRetosEquipos
    from kairos.gestor_correos.envio_correos import enviar_correo, ENIVAR_ACEPTACION_EQUIPO_RETO
    '''
        Permite a un Equipo aceptar la invitación a un Reto

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: Identificador del Equipo que acepta la invitación.
            id_reto: Identificador Retos que envia/recibe la invitacion.
    '''
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización' or request.user.is_superuser):

        if Equipo.existe_equipo(id_equipo) and Retos.existe_reto_por_id(id_reto):

            equipo = Equipo.objects.get(id=id_equipo)
            reto = Retos.objects.get(id=id_reto)

            if SolicitudesRetosEquipos.existe_solitud(reto=reto, equipo=equipo) == True:
                solicitud = SolicitudesRetosEquipos.objects.get(reto=reto, equipo=equipo)

                for joven in equipo.obtener_miembros():
                    reto.agregar_joven_detalle(joven)
                    enviar_correo(joven.email, (reto, equipo), ENIVAR_ACEPTACION_EQUIPO_RETO, joven)

                reto.agregar_equipo(equipo)
                equipo.agregar_reto(reto)

                solicitud.delete()
                DetalleEquipoReto.asignar_miembros(equipo, reto)

                messages.success(request, "Solicitud de miembro aceptada")
            else:
                messages.error(request, "No tienes solicitudes de miembro con el Equipo")

        else:
            messages.error(request, "No existe el receptor")

    else:
        messages.error(request, "El usuario no esta autorizado para esa acción")

    

    if 'registro_reto' in request.session.keys() and request.session['registro_reto'] == 0:
        return redirect('retos:consultar_reto', id_reto)
    elif 'registro_reto' in request.session.keys() and request.session['registro_reto'] == 1:
        return redirect('retos:solicitudes_solicitudes_jovenes_equipos')
    else:
        return redirect('retos:consultar_reto', id_reto)


@login_required
def RetoLiderRetirarEquipo(request, id_equipo, id_reto):
    '''
        Permite a un lider de Equipo retitar a su equipo del Reto

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: Identificador del Equipo que se retira del reto.
            id_reto: Identificador Retos que envia/recibe la invitacion.
    '''
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización' or request.user.is_superuser):
        if Equipo.existe_equipo(id_equipo) and Retos.existe_reto_por_id(id_reto):
            relacion = False

            equipo = Equipo.objects.get(id=id_equipo)
            reto = Retos.objects.get(id=id_reto)

            if reto.equipo_esta_inscrito(equipo) == True:
                mensaje = ""

                if request.user.tipo == 'Joven':
                    mensaje = "El equipo ha dejado el reto"
                else:
                    mensaje = "El equipo ha sido expulsado del reto"

                for joven in equipo.obtener_miembros():
                    reto.jovenes.remove(joven)

                reto.equipos.remove(equipo)
                messages.success(request, mensaje)
            else:
                messages.error(request, "No hay una relación de miembro con el equipo")
        else:
            messages.error(request, "No existe el receptor")
    else:
        messages.error(request, "El usuario no esta autorizado para realizar esa acción")
        return redirect('inicio')

    if equipo:
        id_joven_lider = equipo.lider.id
    else:
        return redirect('retos:consultar_reto', id_reto)


    if 'postulacion_reto' in request.session.keys():
        return redirect('retos:retos_enviar_solicitud_equipo', id_joven=id_joven_lider, id_reto=request.session['postulacion_reto'])
    elif request.session['registro_reto'] == 3:
        return redirect('retos:retos_expulsar_miembros', id_reto=id_reto)
    else:
        return redirect('retos:consultar_reto', id_reto)


class RetosPaginaEspera(LoginRequiredMixin, TemplateView):
    template_name = "retos/pagina_de_espera.html"


def seleccion_jurados(request, id_reto):
    '''
        Se encarga de seleccionar los jurados para un Reto

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identificar del Reto
    '''
    if not request.user.is_authenticated:
        return redirect("usuarios:iniciar_sesion")

    if request.user.is_superuser == False:
        messages.error(request, "Usted no está autorizado para realizar la acción!")
        return redirect("inicio")

    reto = None
    try:
        reto = Retos.objects.get(pk=id_reto)
    except Retos.DoesNotExist:
        messages.error(request, "El Reto no existe")
        return redirect("retos:consultar_retos")

    reto = Retos.buscar(id_reto)

    jurados = reto.jurados.all()
    if request.method == 'POST':
        if 'seleccionar' in request.POST:
            form_jurados = FormSeleccionJurados(request.POST, reto=reto)
            if form_jurados.is_valid():
                usuarios = form_jurados.cleaned_data.get('jurados')
                for usuario in usuarios:
                    jurado = Jurado.objects.get(id=usuario.id)
                    reto.agregar_jurado(jurado.id)
                messages.success(request, "Jurados agregados correctamente!")
                return redirect('retos:seleccion_jurados', id_reto = reto.id)
            else:
                messages.error(request, "Ha ocurrido un error, revise los campos!")

        if 'finalizar' in request.POST:
            return redirect('retos:consultar')

    else:
        form_jurados = FormSeleccionJurados(reto=reto)

    return render(request, 'retos/jurados/seleccion_jurados.html', {
        'form':form_jurados,
        'jurados':jurados,
        'reto':reto,
    })

def eliminar_jurado_del_reto(request, id_reto, id_jurado):
    if not request.user.is_authenticated:
        return redirect("usuarios:iniciar_sesion")

    if request.user.is_superuser == False:
        messages.error(request, "Usted no está autorizado para realizar la acción!")
        return redirect("inicio")

    
    reto = Retos.buscar(id_reto)
    if not reto:
        messages.error(request, "El Reto no existe")
        return redirect("retos:consultar")

    jurado = Jurado.buscar(id_jurado)
    if not jurado:
        messages.error(request, "El Jurado no existe")
        return redirect("retos:seleccion_jurados")

    reto.jurados.remove(jurado)
    reto.save()

    messages.success(request, "Jurado eliminado correctamente!")
    

    return redirect('retos:seleccion_jurados', id_reto=id_reto)

def eliminar_jurado_del_participante(request, id_reto, id_jurado, id_paricipante):
    from kairos.retos.utils import obtener_detalle_participante, eliminar_detalle_de_jurado, obtener_participante

    if not request.user.is_authenticated:
        return redirect("usuarios:iniciar_sesion")

    if request.user.is_superuser == False:
        messages.error(request, "Usted no está autorizado para realizar la acción!")
        return redirect("inicio")

    
    reto = Retos.buscar(id_reto)
    if not reto:
        messages.error(request, "El Reto no existe")
        return redirect("retos:consultar")

    jurado = Jurado.buscar(id_jurado)
    if not jurado:
        messages.error(request, "El Jurado no existe")
        return redirect("retos:asignar_jurados_equipos")
    
    
    participante = obtener_participante(reto, id_paricipante)
    if not participante:
        messages.error(request, "El participante no existe")
        return redirect("retos:asignar_jurados_equipos")

    detalle = obtener_detalle_participante(reto, id_paricipante)
    eliminar_detalle_de_jurado(detalle, jurado)
    
    jurado.save()
   
    messages.success(request, "Jurado eliminado correctamente!")
    
    if 'Equipos' in reto.grupos_permitidos:
        return redirect('retos:asignar_jurados_equipos', id_reto=id_reto, id_jurado=id_jurado)
    else:
        return redirect('retos:asignar_jurados_jovenes', id_reto=id_reto, id_jurado=id_jurado)


class ConsultarJuradosReto(LoginRequiredMixin, ListView):
    model = Jurado
    template_name = "retos/jurados/listado_jurados.html"
    context_object_name = "jurados"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("usuarios:iniciar_sesion")

        if request.user.is_superuser == False:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")

        try:
            reto = Retos.objects.get(pk=self.kwargs['id_reto'])
        except Retos.DoesNotExist:
            messages.error(request, "El Reto no existe")
            return redirect("retos:consultar_retos")

        return super(ConsultarJuradosReto, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        jurados = Jurado.objects.filter(retos_jurados=self.kwargs['id_reto'])
        return jurados

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        reto = Retos.buscar(self.kwargs['id_reto'])
        if reto.fecha_fin > timezone.now():
            context['mensaje'] = 'El Reto no ha Finalizado'
        context['reto'] = reto
        return context

def asignar_jurados_equipos(request, id_reto, id_jurado):
    from django.db.models import Q
    '''
        Se encarga de seleccionar los jurados para los equipos cuando el Reto finaliza

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identificar del Reto
    '''
    if not request.user.is_authenticated:
            return redirect("usuarios:iniciar_sesion")

    if request.user.is_superuser == False:
        messages.error(request, "Usted no está autorizado para realizar la acción!")
        return redirect("inicio")

    try:
        reto = Retos.objects.get(pk=id_reto)
    except Retos.DoesNotExist:
        messages.error(request, "El Reto no existe")
        return redirect("retos:consultar_retos")

    try:
        jurado = Jurado.objects.get(id=id_jurado)
    except Retos.DoesNotExist:
        messages.error(request, "El Jurado no existe")
        return redirect("retos:consultar_retos")

    reto = Retos.buscar(id_reto)
    jurado = Jurado.objects.get(id=id_jurado)

    try:
        detalle = DetalleEquipoReto.objects.filter(jurado=jurado, reto=reto)
    except DetalleEquipoReto.DoesNotExist:
        pass

    lista_equipos_asignados = []
    for equipo in detalle.values('equipo'):
        lista_equipos_asignados.append(equipo['equipo'])

    equipos = reto.equipos.all()
    if request.method == 'POST':
        if 'seleccionar' in request.POST:
            form_equipo = FormSeleccionEquipos(request.POST, reto=reto, jurado=jurado)
            if form_equipo.is_valid():
                equipos = form_equipo.cleaned_data.get('equipos')
                for equipo in equipos:
                    try:
                        detalle = DetalleEquipoReto.objects.get(equipo=equipo, reto=reto)
                        detalle.jurado = jurado
                        detalle.save()
                    except DetalleEquipoReto.DoesNotExist:
                        detalle = DetalleEquipoReto.objects.none()
                messages.success(request, "Jurados asignados correctamente!")
                return redirect('retos:asignar_jurados_equipos', id_reto = reto.id, id_jurado=id_jurado)
            else:
                messages.error(request, "Ha ocurrido un error, revise los campos!")

        if 'finalizar' in request.POST:
            return redirect('retos:consultar')

    else:
        form_equipo = FormSeleccionEquipos(reto=reto, jurado=jurado)
    
    equipos_asignados = equipos.filter(Q(pk__in=lista_equipos_asignados))
    return render(request, 'retos/jurados/seleccion_equipos.html', {
        'form':form_equipo,
        'equipos':equipos_asignados,
        'reto':reto,
        'jurado':jurado,
    })

def asignar_jurados_jovenes(request, id_reto, id_jurado):
    from django.db.models import Q
    from kairos.retos.models import DetalleJovenReto
    '''
        Se encarga de seleccionar los jurados para los equipos cuando el Reto finaliza

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_reto: Identificar del Reto
    '''
    if not request.user.is_authenticated:
            return redirect("usuarios:iniciar_sesion")

    if request.user.is_superuser == False:
        messages.error(request, "Usted no está autorizado para realizar la acción!")
        return redirect("inicio")

    try:
        reto = Retos.objects.get(pk=id_reto)
    except Retos.DoesNotExist:
        messages.error(request, "El Reto no existe")
        return redirect("retos:consultar_retos")

    try:
        jurado = Jurado.objects.get(id=id_jurado)
    except Retos.DoesNotExist:
        messages.error(request, "El Jurado no existe")
        return redirect("retos:consultar_retos")

    reto = Retos.buscar(id_reto)
    jurado = Jurado.objects.get(id=id_jurado)

    try:
        detalle = DetalleJovenReto.objects.filter(jurado=jurado, reto=reto)
    except DetalleJovenReto.DoesNotExist:
        pass

    lista_jovenes_asignados = []
    for joven in detalle.values('joven'):
        lista_jovenes_asignados.append(joven['joven'])

    jovenes = reto.jovenes.all()
    if request.method == 'POST':
        if 'seleccionar' in request.POST:
            form_joven = FormSeleccionJovenes(request.POST, reto=reto, jurado=jurado)
            if form_joven.is_valid():
                jovenes = form_joven.cleaned_data.get('jovenes')
                for joven in jovenes:
                    try:
                        detalle = DetalleJovenReto.objects.get(joven=joven, reto=reto)
                        detalle.jurado = jurado
                        detalle.save()
                    except DetalleJovenReto.DoesNotExist:
                        detalle = DetalleJovenReto.objects.none()
                messages.success(request, "Jóvenes agregados correctamente!")
                return redirect('retos:asignar_jurados_jovenes', id_reto = reto.id, id_jurado=id_jurado)
            else:
                messages.error(request, "Ha ocurrido un error, revise los campos!")

        if 'finalizar' in request.POST:
            return redirect('retos:consultar')

    else:
        form_joven = FormSeleccionJovenes(reto=reto, jurado=jurado)    

    jovenes_asignados = jovenes.filter(Q(pk__in=lista_jovenes_asignados))
    return render(request, 'retos/jurados/seleccion_jovenes.html', {
        'form':form_joven,
        'jovenes':jovenes_asignados
    })


def eliminar_jurados(request, id_jurado, id_reto):
    '''
        Se encarga de eliminar el jurado de un reto especifico

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_jurado: Identificar del jurado a eliminar
            id_reto: Identificar del Reto
    '''
    if not request.user.is_authenticated:
        return redirect("usuarios:iniciar_sesion")

    if request.user.is_superuser == False:
        messages.error(request, "Usted no está autorizado para realizar la acción!")
        return redirect("inicio")

    jurado = Jurado.buscar(id_jurado)
    reto = Reto.buscar(id_reto)

    if jurado == None:
        messages.error(request, "El Jurado no existe")
        return redirect("retos:seleccion_jurados", id_reto=reto.id)

    reto.eliminar_jurado(jurado)
    messages.success(request, "El Jurado no existe")

    return redirect("equipos:miembros", id_equipo=equipo.id)


'''class TipoRetoListado(LoginRequiredMixin, ListView):
    model = TipoReto
    context_object_name = "tipos"
    template_name = "retos/tipos/listado.html"

    def get_queryset(self):
        tipos = TipoReto.objects.all()
        return tipos


class TipoRetoConsultar(LoginRequiredMixin, ListView):
    model = TipoReto
    context_object_name = "tipos"
    template_name = "retos/tipos/consultar.html"

    def get_queryset(self):
        tipos = TipoReto.objects.all()
        return tipos


class TipoRetoRegistrar(LoginRequiredMixin, CreateView):
    model = TipoReto
    fields = '__all__'
    template_name = "retos/tipos/registrar.html"
    success_url = reverse_lazy('retos:listado_tipos')

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Tipo de reto registrado satisfactoriamente")
        return super(TipoRetoRegistrar, self).form_valid(form)


class TipoRetoModificar(LoginRequiredMixin, UpdateView):
    model = TipoReto
    fields = '__all__'
    pk_url_kwarg = 'id_tipo'
    template_name = "retos/tipos/modificar.html"
    success_url = reverse_lazy('retos:listado_tipos')

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Tipo de reto modificado satisfactoriamente")
        return super(TipoRetoModificar, self).form_valid(form)'''

class ApiCambioDeAsistencia(APIView):

    def post(self, request):
        '''
            API que se encarga de cambiar la asistencia de los miembros del grupo en un reto.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    estado: Estado actual del joven
                    pk_miembro: Identificar de joven miembro del equipo
                    pk_equipo: Identificador del equipo
                    pk_reto: Identificador del reto que se esta realizando
                }

            Retorno:
               si recibio información: {'result': True, 'estado':miembro_estado}
               si no recibio información: {'result': False}
        '''
        json_text = request.data.get('json_text')
        request_data = json.loads(json_text)
        if request_data:
            estado = request_data["estado"]
            pk_miembro = int(request_data["pk_miembro"])
            pk_equipo = int(request_data["pk_equipo"])
            pk_reto = int(request_data["pk_reto"])
            equipo = Equipo.buscar(pk_equipo)
            reto = Retos.buscar(pk_reto)
            try:
                detalle = DetalleEquipoReto.objects.get(equipo=equipo, reto=reto)
            except DetalleEquipoReto.DoesNotExist:
                detalle = DetalleEquipoReto.objects.none()
            dict_respuestas = {}

            if detalle:
                dict_respuestas[pk_miembro] = estado
                datos_actuales = detalle.miembros_activos
                datos_actuales.update(dict_respuestas)
                detalle.save()
                if estado == False:
                    miembro_estado = False
                else:
                    miembro_estado = True
                return Response({'result': True, 'estado':miembro_estado})
        return Response({'result': False})

class ApiVerificarAsistencia(APIView):
    def post(self, request):
        '''
            API que se encarga de verificar la asistencia de los miembros del equipo.

            Parametros:
                request: Elemento Http que posee informacion de la sesion.
                data{
                    pk_equipo: Identificador del equipo
                    pk_reto: Identificador del reto que se esta realizando
                }

            Retorno:
               diccionario: {'detalle':json_detalle}
        '''

        json_text = request.data.get('json_text')
        request_data = json.loads(json_text)

        pk_equipo = int(request_data["pk_equipo"])
        pk_reto = int(request_data["pk_reto"])

        equipo = Equipo.buscar(pk_equipo)
        reto = Retos.buscar(pk_reto)

        try:
            detalle = DetalleEquipoReto.objects.get(equipo=equipo, reto=reto)
        except DetalleEquipoReto.DoesNotExist:
            detalle = DetalleEquipoReto.objects.none()

        if detalle:
            json_detalle = detalle.miembros_activos
            return Response({'detalle':json_detalle})


class ApiNotificarCorreos(APIView):
    def get(self, request):
        """
        API que se encarga de verificar si un reto ha finalizado y enviar correos a los miembros con su puntaje final.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.            

        Retorno:
            Response()
        """
        retos = Retos.objects.filter(activo=True)
                
        for reto in retos:
            if reto.reto_finalizado():
                reto.enviar_correos_notificacion_finalizacion()

        return Response()
        

class CompetenciasListado(LoginRequiredMixin, ListView):
    model = Competencia
    context_object_name = "competencias"
    template_name = "retos/competencias/listado.html"

    def get_queryset(self):
        competencias = Competencia.objects.all()
        return competencias

class CompetenciasRegistrar(LoginRequiredMixin, CreateView):
    model = Competencia
    fields = ['nombre', 'estado']
    template_name = "retos/competencias/registrar.html"
    success_url = reverse_lazy('retos:listado_competencias')

    def get_context_data(self, **kwargs):
        context = super(CompetenciasRegistrar, self).get_context_data(**kwargs)
        context['iconos'] = (
            ("fas fa-bolt", "fas fa-bolt"),
            ("fas fa-camera", "fas fa-camera"),
            ("far fa-eye", "far fa-eye"),
            ("fas fa-portrait", "fas fa-portrait"),
            ("fas fa-clipboard-check", "fas fa-clipboard-check"),
            ("fas fa-truck", "fas fa-truck"),
            ("far fa-heart", "far fa-heart"),
            ("fas fa-lightbulb", "fas fa-lightbulb"),
            ("fas fa-trophy", "fas fa-trophy"),
            ("fas fa-wrench", "fas fa-wrench"),
            ("fas fa-bullhorn", "fas fa-bullhorn"),
            ("fas fa-calculator", "fas fa-calculator"),
            ("fas fa-first-aid", "fas fa-first-aid"),
            ("fas fa-guitar", "fas fa-guitar"),
            ("fas fa-balance-scale", "fas fa-balance-scale"),
            ("fas fa-cogs", "fas fa-cogs"),
            ("fas fa-cog", "fas fa-cog"),
            ("fas fa-futbol", "fas fa-futbol"),
            ("fas fa-gem", "fas fa-gem"),
            ("fas fa-search", "fas fa-search"),
            ("fas fa-tools", "fas fa-tools"),
        )
        return context

    
    def form_valid(self, form):
        competencia = form.save(commit=False)        
        competencia.icono = self.request.POST.get('grupo_iconos')
        
        form.save()
        messages.success(self.request, "Competencia registrada satisfactoriamente")
        return super(CompetenciasRegistrar, self).form_valid(form)

class CompetenciasModificar(LoginRequiredMixin, UpdateView):
    model = Competencia
    fields = ['nombre', 'estado']
    pk_url_kwarg = 'id_competencia'
    template_name = "retos/competencias/modificar.html"
    success_url = reverse_lazy('retos:listado_competencias')

    def get_context_data(self, **kwargs):
        context = super(CompetenciasModificar, self).get_context_data(**kwargs)
        context['iconos'] = (
            ("fas fa-bolt", "fas fa-bolt"),
            ("fas fa-camera", "fas fa-camera"),
            ("far fa-eye", "far fa-eye"),
            ("fas fa-portrait", "fas fa-portrait"),
            ("fas fa-clipboard-check", "fas fa-clipboard-check"),
            ("fas fa-truck", "fas fa-truck"),
            ("far fa-heart", "far fa-heart"),
            ("fas fa-lightbulb", "fas fa-lightbulb"),
            ("fas fa-trophy", "fas fa-trophy"),
            ("fas fa-wrench", "fas fa-wrench"),
            ("fas fa-bullhorn", "fas fa-bullhorn"),
            ("fas fa-calculator", "fas fa-calculator"),
            ("fas fa-first-aid", "fas fa-first-aid"),
            ("fas fa-guitar", "fas fa-guitar"),
            ("fas fa-balance-scale", "fas fa-balance-scale"),
            ("fas fa-cogs", "fas fa-cogs"),
            ("fas fa-cog", "fas fa-cog"),
            ("fas fa-futbol", "fas fa-futbol"),
            ("fas fa-gem", "fas fa-gem"),
            ("fas fa-search", "fas fa-search"),
            ("fas fa-tools", "fas fa-tools"),
        )
        
        context['competencia'] = Competencia.objects.get(pk=self.kwargs['id_competencia'])
        return context

    def form_valid(self, form):        
        competencia = form.save(commit=False)        
        competencia.icono = self.request.POST.get('grupo_iconos')

        form.save()
        messages.success(self.request, "Competencia modificada satisfactoriamente")
        return super(CompetenciasModificar, self).form_valid(form)