# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "retos"

urlpatterns = [
    path('gestionar/', view=RetosListado.as_view(), name='listado'),
    path('consultar/', view=RetosConsultar.as_view(), name='consultar'),
    path('registrar/', view=RetosRegistrar.as_view(), name='registrar'),
    path('registrar/<int:id_solicitud>', view=RetosRegistrarDesdeSolicitud.as_view(), name='registrar_desde_solicitud'),
    path('modificar/<int:id_reto>', view=RetosModificar.as_view(), name='modificar'),

    path('responder-cuartos/<int:id_reto>', view=retos_responder_cuartos, name='retos_responder_cuartos'),
    path('responder-cuartos/<int:id_reto>/<int:id_equipo>', view=retos_responder_cuartos, name='retos_responder_cuartos'),

    path('cuarto-descanso/<int:id_reto>', view=cuarto_descanso, name='cuarto_descanso'),
    path('cuarto-descanso/<int:id_reto>/<int:id_equipo>', view=cuarto_descanso, name='cuarto_descanso'),

    path('cuarto-finalizacion/<int:id_reto>', view=retos_cuarto_final, name='cuarto_finalizacion'),
    path('cuarto-finalizacion/<int:id_reto>/<int:id_equipo>', view=retos_cuarto_final, name='cuarto_finalizacion'),
    
    path('inicio-reto/<int:id_reto>', view=inicio_reto, name='inicio_reto'),
    path('inicio-reto/<int:id_reto>/<int:id_equipo>', view=inicio_reto, name='inicio_reto'),
   
    path('abandonar-reto-joven/<int:id_reto>', view=abandonar_reto_joven, name='abandonar_reto'),
    path('abandonar-reto-joven/<int:id_reto>/<int:id_equipo>', view=abandonar_reto_joven, name='abandonar_reto'),
    
    path('cuestionario-reto/<int:id_reto>', view=retos_responder_cuartos, name='cuestionario_reto'),

    path('consultar-retos/', view=ConsultarListaRetos.as_view(), name='consultar_retos'),
    path('consulta-reto/<int:pk>', view=ConsultarReto.as_view(), name='consultar_reto'),

    path("api/cargar-calificaciones", view=RetoApiCargarCalificaciones.as_view(), name="retos_api_cargar_calificaciones"),
    path("api/cargar-calificacion", view=RetoApiCargarCalificacion.as_view(), name="retos_api_cargar_calificacion"),
    path("api/calificar", view=RetoApiCalificarContenido.as_view(), name="retos_api_calificar"),
    path("api/guardar-cuarto-acto", view=RetoApiGuardarCuartoActo.as_view(), name="retos_api_guradar_cuarto_acto"),
    path("api/guardar-pregunta", view=RetoApiGuardarCuartoPregunta.as_view(), name="retos_api_guardar_pregunta"),
    path("api/guardar-tiempo-pregunta", view=RetoApiGuardarTiempoPregunta.as_view(), name="retos_api_guardar_tiempo_pregunta"),
    path("api/recalificar-cuarto-acto", view=RetoApiRecalificarCuartoActo.as_view(), name="retos_api_recalificar_cuarto_acto"),
    path("api/actualizar-ranking", view=RetoApiActualizarRanking.as_view(), name="retos_api_actualizar_ranking"),
    path("api/enviar-respuesta-pregunta", view=RetoApiEnviarRespuestaPregunta.as_view(), name="retos_api_enviar_respuesta_pregunta"),
    path("api/calificar_competencias_joven", view=RetoApiCalificarCompetenciasJoven.as_view(), name="api_calificar_competencias"),
    
    path('retos/modificar_solicitud/<int:id_solicitud_reto>', view=ModificarSolicitudesRetos.as_view(), name='organizacion_modificar_solicitudes_retos'),
    path('solicitudes-organizaciones/', view=GestionDeSolicitudesRetosOrganizaciones.as_view(), name='solicitudes_retos_organizaciones'),
    path('solicitudes-jovenes/', view=GestionDeSolicitudesRetosJovenes.as_view(), name='solicitudes_solicitudes_jovenes'),
    path('solicitudes-jovenes-equipos/', view=GestionDeSolicitudesRetosJovenesEquipos.as_view(), name='solicitudes_solicitudes_jovenes_equipos'),

    path('enviar-solicitud-joven/<int:id_joven>/<int:id_reto>', view=RetoJovenEnviarSolicitud, name='retos_enviar_solicitud_joven'),
    path('rechazar-solicitud-joven/<int:id_joven>/<int:id_reto>', view=RetoJovenRechazarSolicitud, name='retos_rechazar_solicitud_joven'),
    path('aceptar-solicitud-joven/<int:id_joven>/<int:id_reto>', view=RetoJovenAceptarSolicitud, name='retos_aceptar_solicitud_joven'),
    path('finalizar-solicitud-joven/<int:id_joven>/<int:id_reto>', view=RetoJovenFinalizarRelacion, name='retos_finalizar_solicitud_joven'),

    path('enviar-solicitud-equipo/<int:id_joven>/<int:id_reto>', view=PostulacionLiderReto, name='retos_enviar_solicitud_equipo'),
    path('rechazar-solicitud-equipo/<int:id_equipo>/<int:id_reto>', view=RetoEquipoRetirarPostulacion, name='retos_rechazar_solicitud_equipo'),
    path('aceptar-solicitud-equipo/<int:id_equipo>/<int:id_reto>', view=RetoEquipoAceptarSolicitud, name='retos_aceptar_solicitud_equipo'),
    path('retirar-equipo-reto/<int:id_equipo>/<int:id_reto>', view=RetoLiderRetirarEquipo, name='retos_retirar_equipo'),

    path('invitar-equipos-jovenes/<int:id_reto>', view=InvitarEquiposJovenesAdministrador, name='retos_invitar_equipos_jovenes'),
    path('expulsar-miembros/<int:id_reto>', view=ExpulsarEquiposJovenesAdministrador, name='retos_expulsar_miembros'),

    path('pagina-espera', view=RetosPaginaEspera.as_view(), name='pagina_espera'),

    path('seleccion-jurados/<int:id_reto>', view=seleccion_jurados, name='seleccion_jurados'),    
    path('consultar-jurados-reto/<int:id_reto>', view=ConsultarJuradosReto.as_view(), name='consultar_jurados_reto'),
    path('eliminar-jurados-reto/<int:id_reto>/<int:id_jurado>', view=eliminar_jurado_del_reto, name='eliminar_jurado_reto'),
    path('asignar-jurados-equipo/<int:id_reto>/<int:id_jurado>', view=asignar_jurados_equipos, name='asignar_jurados_equipos'),
    path('eliminar-jurados-participante/<int:id_reto>/<int:id_jurado>/<int:id_paricipante>', view=eliminar_jurado_del_participante, name='eliminar_jurado_participante'),

    
    path('asignar-jurados-jovenes/<int:id_reto>/<int:id_jurado>', view=asignar_jurados_jovenes, name='asignar_jurados_jovenes'),

    path('seleccion-equipo/<int:id_reto>', view=seleccion_equipo, name='seleccion_equipo'),
    path('estamos-los-que-somos/<int:id_reto>/<int:id_equipo>', view=cuarto_espera_equipos, name='cuarto_espera_equipos'),
    path('reconocimiento-equipos/<int:id_reto>/<int:id_equipo>', view=cuarto_reconocimiento_equipos, name='cuarto_reconocimiento_equipos'),
    path("api/asistencia-equipos", view=ApiCambioDeAsistencia.as_view(), name="api_asistencia"),
    path("api/verificar-asistencia-equipos", view=ApiVerificarAsistencia.as_view(), name="api_verificar_asistencia"),
    path("api/cargar-pagina-finaizacion-miembros", view=RetoApiCargarPaginaFinalizacionMiembros.as_view(), name="api_cargar_pagina_finaizacion_miembros"),
    path("api/actualizar-boton-inicio-reto", view=RetoApiActualizarBotonRetoInicio.as_view(), name="api_actualizar_boton_inicio_reto"),
    path("api/notificar-correos", view=ApiNotificarCorreos.as_view(), name="api_notificar_correos"),
    path("api/confirmar-reto-finalizado", view=RetoApiConfirmarRetoFinalizado.as_view(), name="api_confirmar_reto_finalizado"),
    

    path('registrar-competencia', view=CompetenciasRegistrar.as_view(), name='registrar_competencia'),
    path('modificar-competencia/<int:id_competencia>', view=CompetenciasModificar.as_view(), name='modificar_competencia'),   
    path('listado-competencias', view=CompetenciasListado.as_view(), name='listado_competencias'),

    path('resultados-participantes/<int:id_reto>', view=ConsultarResultadosParticipantes.as_view(), name='resultados_participantes'),

    path('resultados-participante/<int:id_reto>', view=ConsultarResultadosParticipante.as_view(), name='resultados_participante'),

    # path('tipos/gestionar/', TipoRetoListado.as_view(), name='listado_tipos'),
    # path('tipos/consultar/', TipoRetoConsultar.as_view(), name='consultar_tipos'),
    # path('tipos/registrar', TipoRetoRegistrar.as_view(), name='registrar_tipo'),
    # path('tipos/modificar/<int:id_tipo>', TipoRetoModificar.as_view(), name='modificar_tipo'),
]