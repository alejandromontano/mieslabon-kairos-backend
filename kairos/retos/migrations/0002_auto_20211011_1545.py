# Generated by Django 3.0.2 on 2021-10-11 15:45

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('insignias', '0002_auto_20211011_1545'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('organizaciones', '0002_auto_20211011_1545'),
        ('cuartos', '0002_auto_20211011_1545'),
        ('jurados', '0001_initial'),
        ('usuarios', '0001_initial'),
        ('retos', '0001_initial'),
        ('equipos', '0002_auto_20211011_1545'),
        ('cities_light', '0008_city_timezone'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicitudreto',
            name='organizacion',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='organizacion_solicitud_reto', to='organizaciones.Organizacion'),
        ),
        migrations.AddField(
            model_name='solicitudesretosjovenes',
            name='joven',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='solicitudes_de_reto_joven', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='solicitudesretosjovenes',
            name='reto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='solicitudes_retos_joven', to='retos.Retos'),
        ),
        migrations.AddField(
            model_name='solicitudesretosequipos',
            name='equipo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='solicitudes_de_reto_equipo', to='equipos.Equipo'),
        ),
        migrations.AddField(
            model_name='solicitudesretosequipos',
            name='reto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='solicitudes_retos_equipo', to='retos.Retos'),
        ),
        migrations.AddField(
            model_name='retos',
            name='areas_conocimiento',
            field=models.ManyToManyField(to='insignias.Tribus', verbose_name='Áreas de conocimiento de este reto*'),
        ),
        migrations.AddField(
            model_name='retos',
            name='competencias',
            field=models.ManyToManyField(related_name='retos_competencias', to='retos.Competencia', verbose_name='Competencias asignadas*'),
        ),
        migrations.AddField(
            model_name='retos',
            name='cuartos',
            field=models.ManyToManyField(blank=True, related_name='cuartos_retos', through='retos.CuartoReto', to='cuartos.Cuarto', verbose_name='cuartos del reto'),
        ),
        migrations.AddField(
            model_name='retos',
            name='equipos',
            field=models.ManyToManyField(related_name='retos_equipo', through='retos.DetalleEquipoReto', to='equipos.Equipo', verbose_name='Equipos inscritos'),
        ),
        migrations.AddField(
            model_name='retos',
            name='jovenes',
            field=models.ManyToManyField(related_name='retos_joven', through='retos.DetalleJovenReto', to='usuarios.Joven', verbose_name='Jóvenes inscritos'),
        ),
        migrations.AddField(
            model_name='retos',
            name='jurados',
            field=models.ManyToManyField(related_name='retos_jurados', to='jurados.Jurado', verbose_name='Jurados asignados'),
        ),
        migrations.AddField(
            model_name='retos',
            name='niveles_conocimiento_deseado',
            field=models.ManyToManyField(to='insignias.NivelesDeConocimiento', verbose_name='Niveles de conocimiento de este reto*'),
        ),
        migrations.AddField(
            model_name='retos',
            name='organizacion',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='retos_organizacion', to='organizaciones.Organizacion', verbose_name='Organización*'),
        ),
        migrations.AddField(
            model_name='retos',
            name='organizaciones_permitidas',
            field=models.ManyToManyField(blank=True, related_name='retos_organizaciones_permitidas', to='organizaciones.Organizacion', verbose_name='Organizaciones permitidas'),
        ),
        migrations.AddField(
            model_name='retos',
            name='roles',
            field=models.ManyToManyField(to='insignias.Roles', verbose_name='Roles de este reto*'),
        ),
        migrations.AddField(
            model_name='retos',
            name='solicitudes_equipos',
            field=models.ManyToManyField(blank=True, related_name='retos_equipo_solicitudes', through='retos.SolicitudesRetosEquipos', to='equipos.Equipo', verbose_name='Equipos que van a ser parte del reto'),
        ),
        migrations.AddField(
            model_name='retos',
            name='solicitudes_jovenes',
            field=models.ManyToManyField(blank=True, related_name='retos_joven_solicitudes', through='retos.SolicitudesRetosJovenes', to='usuarios.Joven', verbose_name='Jovenes que van a ser parte del reto'),
        ),
        migrations.AddField(
            model_name='jovenretocuarto',
            name='cuarto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='joven_reto_cuarto_cuarto', to='cuartos.Cuarto'),
        ),
        migrations.AddField(
            model_name='jovenretocuarto',
            name='detalle',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='joven_reto_cuarto_detalle', to='retos.DetalleJovenReto'),
        ),
        migrations.AddField(
            model_name='historicalsolicitudreto',
            name='ciudad',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cities_light.City', verbose_name='Ciudad*'),
        ),
        migrations.AddField(
            model_name='historicalsolicitudreto',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalsolicitudreto',
            name='organizacion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='organizaciones.Organizacion'),
        ),
        migrations.AddField(
            model_name='historicalsolicitudesretosjovenes',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalsolicitudesretosjovenes',
            name='joven',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='historicalsolicitudesretosjovenes',
            name='reto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='retos.Retos'),
        ),
        migrations.AddField(
            model_name='historicalsolicitudesretosequipos',
            name='equipo',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='equipos.Equipo'),
        ),
        migrations.AddField(
            model_name='historicalsolicitudesretosequipos',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalsolicitudesretosequipos',
            name='reto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='retos.Retos'),
        ),
        migrations.AddField(
            model_name='historicalretos',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalretos',
            name='organizacion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='organizaciones.Organizacion', verbose_name='Organización*'),
        ),
        migrations.AddField(
            model_name='historicaldetallejovenreto',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicaldetallejovenreto',
            name='joven',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='historicaldetallejovenreto',
            name='jurado',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='jurados.Jurado'),
        ),
        migrations.AddField(
            model_name='historicaldetallejovenreto',
            name='reto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='retos.Retos'),
        ),
        migrations.AddField(
            model_name='historicaldetallejovenreto',
            name='retroalimentacion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='jurados.RetroalimentacionJuradoJoven'),
        ),
        migrations.AddField(
            model_name='historicaldetalleequiporeto',
            name='equipo',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='equipos.Equipo'),
        ),
        migrations.AddField(
            model_name='historicaldetalleequiporeto',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicaldetalleequiporeto',
            name='jurado',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='jurados.Jurado'),
        ),
        migrations.AddField(
            model_name='historicaldetalleequiporeto',
            name='reto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='retos.Retos'),
        ),
        migrations.AddField(
            model_name='historicaldetalleequiporeto',
            name='retroalimentacion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='jurados.RetroalimentacionJuradoEquipo'),
        ),
        migrations.AddField(
            model_name='historicalcuartoreto',
            name='cuarto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cuartos.Cuarto'),
        ),
        migrations.AddField(
            model_name='historicalcuartoreto',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalcuartoreto',
            name='reto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='retos.Retos'),
        ),
        migrations.AddField(
            model_name='historicalcompetencia',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalcalificacionretos',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalcalificacionretos',
            name='reto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='retos.Retos', verbose_name='reto calificado'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionretos',
            name='usuario',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='usuario que califica'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionpreguntajoven',
            name='detalle_joven_reto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='retos.DetalleJovenReto'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionpreguntajoven',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalcalificacionpreguntajoven',
            name='opcion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cuartos.Opcion'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionpreguntajoven',
            name='pregunta',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cuartos.Pregunta'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionpreguntaequipo',
            name='detalle_equipo_reto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='retos.DetalleEquipoReto'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionpreguntaequipo',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalcalificacionpreguntaequipo',
            name='opcion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cuartos.Opcion'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionpreguntaequipo',
            name='pregunta',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cuartos.Pregunta'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionactojoven',
            name='acto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cuartos.Acto'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionactojoven',
            name='detalle_joven_reto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='retos.DetalleJovenReto'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionactojoven',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalcalificacionactoequipo',
            name='acto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cuartos.Acto'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionactoequipo',
            name='detalle_equipo_reto',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='retos.DetalleEquipoReto'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionactoequipo',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='equiporetocuarto',
            name='cuarto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='equipo_reto_cuarto_cuarto', to='cuartos.Cuarto'),
        ),
        migrations.AddField(
            model_name='equiporetocuarto',
            name='detalle',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='equipo_reto_cuarto_detalle', to='retos.DetalleEquipoReto'),
        ),
        migrations.AddField(
            model_name='detallejovenreto',
            name='cuartos',
            field=models.ManyToManyField(blank=True, related_name='detalle_joven_reto_cuartos', through='retos.JovenRetoCuarto', to='cuartos.Cuarto', verbose_name='cuartos del joven'),
        ),
        migrations.AddField(
            model_name='detallejovenreto',
            name='joven',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='joven_detalle_reto', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='detallejovenreto',
            name='jurado',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='joven_reto_evaluado', to='jurados.Jurado'),
        ),
        migrations.AddField(
            model_name='detallejovenreto',
            name='reto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='detalle_joven', to='retos.Retos'),
        ),
        migrations.AddField(
            model_name='detallejovenreto',
            name='retroalimentacion',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='jurados.RetroalimentacionJuradoJoven'),
        ),
        migrations.AddField(
            model_name='detalleequiporeto',
            name='cuartos',
            field=models.ManyToManyField(blank=True, related_name='detalle_equipo_reto_cuartos', through='retos.EquipoRetoCuarto', to='cuartos.Cuarto', verbose_name='cuartos del joven'),
        ),
        migrations.AddField(
            model_name='detalleequiporeto',
            name='equipo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='equipo_detalle_reto', to='equipos.Equipo'),
        ),
        migrations.AddField(
            model_name='detalleequiporeto',
            name='jurado',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='equipo_reto_evaluado', to='jurados.Jurado'),
        ),
        migrations.AddField(
            model_name='detalleequiporeto',
            name='reto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='detalle_equipo', to='retos.Retos'),
        ),
        migrations.AddField(
            model_name='detalleequiporeto',
            name='retroalimentacion',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='jurados.RetroalimentacionJuradoEquipo'),
        ),
        migrations.AddField(
            model_name='cuartoreto',
            name='cuarto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='cuarto_reto_cuarto', to='cuartos.Cuarto'),
        ),
        migrations.AddField(
            model_name='cuartoreto',
            name='reto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='cuarto_reto_reto', to='retos.Retos'),
        ),
        migrations.AddField(
            model_name='calificacionretos',
            name='reto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calificacion_reto_del_joven', to='retos.Retos', verbose_name='reto calificado'),
        ),
        migrations.AddField(
            model_name='calificacionretos',
            name='usuario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='usuario_calificador_reto', to=settings.AUTH_USER_MODEL, verbose_name='usuario que califica'),
        ),
        migrations.AddField(
            model_name='calificacionpreguntajoven',
            name='detalle_joven_reto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calificacion_pregunta_joven_reto', to='retos.DetalleJovenReto'),
        ),
        migrations.AddField(
            model_name='calificacionpreguntajoven',
            name='opcion',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='opcion_joven_pregunta', to='cuartos.Opcion'),
        ),
        migrations.AddField(
            model_name='calificacionpreguntajoven',
            name='pregunta',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calificacion_pregunta_joven_pregunta', to='cuartos.Pregunta'),
        ),
        migrations.AddField(
            model_name='calificacionpreguntaequipo',
            name='detalle_equipo_reto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='calificacion_pregunta_equipo_reto', to='retos.DetalleEquipoReto'),
        ),
        migrations.AddField(
            model_name='calificacionpreguntaequipo',
            name='opcion',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='opcion_equipo_pregunta', to='cuartos.Opcion'),
        ),
        migrations.AddField(
            model_name='calificacionpreguntaequipo',
            name='pregunta',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='calificacion_pregunta_equipo_pregunta', to='cuartos.Pregunta'),
        ),
        migrations.AddField(
            model_name='calificacionactojoven',
            name='acto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calificacion_acto_joven_acto', to='cuartos.Acto'),
        ),
        migrations.AddField(
            model_name='calificacionactojoven',
            name='detalle_joven_reto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calificacion_acto_joven_reto', to='retos.DetalleJovenReto'),
        ),
        migrations.AddField(
            model_name='calificacionactoequipo',
            name='acto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calificacion_acto_equipo_acto', to='cuartos.Acto'),
        ),
        migrations.AddField(
            model_name='calificacionactoequipo',
            name='detalle_equipo_reto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calificacion_acto_equipo_reto', to='retos.DetalleEquipoReto'),
        ),
    ]
