# Modulos Django
from django.contrib import messages
from django.shortcuts import render, redirect
from django.utils import timezone

# Modulos de plugin externos

# Modulos de otras apps
from kairos.retos.models import DetalleJovenReto, DetalleEquipoReto, Retos, JovenRetoCuarto, CalificacionActoJoven, CuartoReto, CalificacionActoEquipo, EquipoRetoCuarto, CalificacionPreguntaJoven, CalificacionPreguntaEquipo
from kairos.usuarios.models import Joven
from kairos.cuartos.models import Cuarto, Acto, Pregunta, Opcion
from kairos.equipos.models import Equipo
    

# Modulos internos de la app

def crear_calificacion_participante(reto, cuarto, detalle):
    '''
        Selecciona que tipos de calificaciones(Acto, Pregunta) seran creadas segun el tipo de grupos
        permitidos(Jóvenes, Equipos) en el Reto.

        Parametros:
            reto: Objeto del Modelo Reto
            cuarto: Objeto del Modelo Cuarto para determinar que tipo de calificacion crear
            detalle: Objeto del Modelo Detalle(Joven, Equipo) que relaciona al participante con el Cuarto y el Reto
    '''
    if 'Equipos' in reto.grupos_permitidos:
        if cuarto.tipo.nombre == 'Pregunta':
            crear_calificacion_pregunta_participante(CalificacionPreguntaEquipo, cuarto, detalle)
        else:
            crear_calificacion_acto_participante(CalificacionActoEquipo, cuarto, detalle)
    else:
        if cuarto.tipo.nombre == 'Pregunta':
            crear_calificacion_pregunta_participante(CalificacionPreguntaJoven, cuarto, detalle)
        else:
            crear_calificacion_acto_participante(CalificacionActoJoven, cuarto, detalle)
    

def crear_calificacion_acto_participante(calificacion, cuarto, detalle):
    '''
        Crea la CalificacionActo del paricipante

        Parametros:
            calificacion: Objeto del Modelo CalificacionActoEquipo o CalificacionActoJoven
            cuarto: Objeto del Modelo Cuarto para crear las calificaciones
            detalle: Objeto del Modelo Detalle(Joven, Equipo) que relaciona al participante con el Cuarto y el Reto
    '''
    orden_aux = 0
    valor_maximo = cuarto.obtener_orden_maximo() + 1
    for actos in range(0, valor_maximo):
        try:
            acto = cuarto.actos.get(cuarto_acto_acto__orden=orden_aux)
            calificacion.crear_calificacion(detalle, acto)
        except Acto.DoesNotExist:
            continue
        finally:
            orden_aux += 1

def crear_calificacion_pregunta_participante(calificacion, cuarto, detalle):
    '''
        Crea la CalificacionPregunta del paricipante

        Parametros:
            calificacion: Objeto del Modelo CalificacionPreguntaEquipo o CalificacionPreguntaJoven
            cuarto: Objeto del Modelo Cuarto para crear las calificaciones
            detalle: Objeto del Modelo Detalle(Joven, Equipo) que relaciona al participante con el Cuarto y el Reto
    '''
    orden_aux = 0
    valor_maximo = cuarto.obtener_orden_maximo() + 1
    for preguntas in range(0, valor_maximo):
        try:
            pregunta = cuarto.preguntas.get(cuarto_pregunta_pregunta__orden=orden_aux)
            calificacion.crear_calificacion(detalle, pregunta)
        except Acto.DoesNotExist:
            continue
        finally:
            orden_aux += 1


def obtener_evidencias_subidas_participante(reto, detalle):
    '''
        Retorna la cantidad de evidancias subidas totales en todos los cuartos por parte de un participante

        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo)
        Retorno:
            cantidad: Número entero
    '''
    cantidad = 0
    cuartos_responder = Cuarto.obtener_cuartos_responder(reto, detalle)

    if 'Equipos' in reto.grupos_permitidos:
        modelo = CalificacionActoEquipo
    else:
        modelo = CalificacionActoJoven

    for cuarto in cuartos_responder:
        for acto in cuarto.actos.all():
            calificacion = modelo.buscar(detalle, acto)
            if calificacion and calificacion.archivo_subido is not None and calificacion.archivo_subido != '':
                cantidad += 1
    return cantidad

def obtener_preguntas_contestadas_participante(reto, detalle):
    '''
        Retorna la cantidad de preguntas contestadas en todos los cuartos por parte de un participante

        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo)
        Retorno:
            cantidad: Número entero
    '''
    cuartos_responder = Cuarto.obtener_cuartos_responder(reto, detalle)
    if 'Equipos' in reto.grupos_permitidos:
        modelo = CalificacionPreguntaEquipo
    else:
        modelo = CalificacionPreguntaJoven

    cantidad = 0
    for cuarto in cuartos_responder:
        for pregunta in cuarto.preguntas.all():
            calificacion = modelo.buscar(detalle, pregunta)
            if calificacion and calificacion.opcion is not None:
                cantidad += 1
           
    return cantidad

def obtener_puntaje_final(reto, detalle, recalificar=False):
    '''
        Calcula y retorna el puntaje final de un participante en el reto

        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo)
        Retorno:
            puntaje_total: Número entero
    '''
    from django.db.models import Count, Sum   
    from django.utils import timezone

    detalle.completado = True
    detalle.save()
    
    if not reto.reto_finalizado():
        return "El puntaje se calcula cuando se llegue al tiempo limite del reto "
    
    if not detalle.calificacion_final or recalificar == True:               
        puntaje_posicion = [100, 80, 60, 40, 40, 40, 40, 40, 40, 40]
        cuartos_responder = Cuarto.obtener_cuartos_responder(reto, detalle)
        
        if 'Equipos' in reto.grupos_permitidos:
            modelo_reto_cuarto = EquipoRetoCuarto
            calificacion_acto_modelo = CalificacionActoEquipo
            calificacion_pregunta_modelo = CalificacionPreguntaEquipo
        else:
            modelo_reto_cuarto = JovenRetoCuarto
            calificacion_acto_modelo = CalificacionActoJoven
            calificacion_pregunta_modelo = CalificacionPreguntaJoven

        reto_tiempo = list(modelo_reto_cuarto.objects.filter(
                cuarto__in=cuartos_responder
                ).values('detalle').annotate(tiempo=Sum('tiempo_transcurrido')).order_by('tiempo'))

        cuarto_tiempo = modelo_reto_cuarto.objects.order_by('tiempo_transcurrido')
        
        lista_detalles = []
        for detalle_cuarto in reto_tiempo:
            lista_detalles.append(detalle_cuarto['detalle'])

        puntaje_total = 0
        reto_terminado = 0
        suma_total_pesos_cuartos = 0
        posicion_ranking_tiempo_reto = 0
        for cuarto_suma in cuartos_responder:
            suma_total_pesos_cuartos += cuarto_suma.peso

        lista_cuartos_puntaje = list()
        for cuarto in cuartos_responder:
            puntaje_auxiliar = 0            
            for acto in cuarto.actos.all():
                calificaion_acto = calificacion_acto_modelo.buscar(detalle, acto)
                if calificaion_acto:
                    puntaje_auxiliar += calificaion_acto.puntaje 
            
            for pregunta in cuarto.preguntas.all():
                calificacion_pregunta = calificacion_pregunta_modelo.buscar(detalle, pregunta)
                if calificacion_pregunta:
                    puntaje_auxiliar += calificacion_pregunta.puntaje
           
            jove_reto_cuarto = modelo_reto_cuarto.buscar(cuarto, detalle)
            if jove_reto_cuarto and jove_reto_cuarto.completado == True:
                puntaje_auxiliar += (cuarto.dificultad*10)            
            
            if detalle.completado == True and detalle.abandonado == False:
                tiempo_ranking = modelo_reto_cuarto.buscar_varios(detalle, cuartos_responder).values('detalle')                
                tiempo_ranking_cuarto = modelo_reto_cuarto.buscar(cuarto, detalle)                
                posicion_ranking_tiempo = list(cuarto_tiempo.filter(cuarto=cuarto)).index(tiempo_ranking_cuarto)
                
                if posicion_ranking_tiempo < 10:
                    puntaje_ranking_tiempo = puntaje_posicion[posicion_ranking_tiempo]
                else:
                    puntaje_ranking_tiempo = 0
                
                puntaje_auxiliar += puntaje_ranking_tiempo                          
                puntaje_auxiliar = round(puntaje_auxiliar * (1+float(cuarto.peso/suma_total_pesos_cuartos)))    
                             
                lista_cuartos_puntaje.append(puntaje_auxiliar)
                posicion_ranking_tiempo_reto = lista_detalles.index(tiempo_ranking[0]['detalle'])
                
                if posicion_ranking_tiempo_reto <= 10:
                    posicion_ranking_tiempo_reto = puntaje_posicion[posicion_ranking_tiempo_reto]
                
                if detalle.esta_reto_completado():
                    reto_terminado = reto.grado_dificultad*100

        puntaje_total += posicion_ranking_tiempo_reto + reto_terminado + sum(lista_cuartos_puntaje)
        
        detalle.calificacion_final = int(puntaje_total)
        detalle.save()
    return int(detalle.calificacion_final)


def obtener_porcentaje_completado_reto(reto, detalle):
    '''
        Calcula y retorna el porcentaje actual realizado del reto por parte de un participante

        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo)
        Retorno:
            porcentaje: Número entero
    '''
    from django.db.models import Count
    
    cuartos_totales = Cuarto.obtener_cuartos_responder(reto, detalle)

    if 'Equipos' in reto.grupos_permitidos:
        modelo = EquipoRetoCuarto
    else:
        modelo = JovenRetoCuarto
    
    cuartos_contestados = modelo.obtener_cuartos_completados(detalle, cuartos_totales).count()

    porcentaje = 0
    if cuartos_contestados != 0:
        porcentaje = int((cuartos_contestados/cuartos_totales.count())*100)
    detalle.save()
    return porcentaje

def calificar_actos_joven(detalle, cuarto):
    '''
        Califica los Actos de un joven tomando encuenta si subio un archivo

        Parametros:      
            cuarto: Objecto del Modelo Cuarto que posee los actos      
            detalle: Objeto del Modelo DetalleJoven    
    '''
    for acto in cuarto.actos.all():
        try:
            calificacion = CalificacionActoJoven.objects.get(detalle_joven_reto=detalle, acto=acto)
            if calificacion.archivo_subido is not None and calificacion.archivo_subido != '':
                calificacion.puntaje = acto.calificacion_maxima
                calificacion.save()       
        except:
            pass

def calificar_actos_equipo(detalle, cuarto):
    '''
        Califica los Actos de un equipo tomando encuenta si subio un archivo

        Parametros:      
            cuarto: Objecto del Modelo Cuarto que posee los actos
            detalle: Objeto del Modelo DetalleEquipo
    '''
    for acto in cuarto.actos.all():
        try:
            calificacion = CalificacionActoEquipo.objects.get(detalle_equipo_reto=detalle, acto=acto)
            if calificacion.archivo_subido is not None and calificacion.archivo_subido != '':
                calificacion.puntaje = acto.calificacion_maxima
                calificacion.save()
        except:
            pass
        
def recalificar_acto_equipo(detalle, cuarto, acto, recalificar):
    '''
        Recalifica el puntaje que obtuvo un Equipo en un Acto dependiento de la decisión del jurado

        Parametros:            
            detalle: Objeto del Modelo DetalleEquipo
            acto: Objeto del modelo Acto que se recalificara  
            recalificar: decisión del jurado 
    '''
    calificacion = CalificacionActoEquipo.objects.get(detalle_equipo_reto=detalle, acto=acto)

    if calificacion.archivo_subido is not None and calificacion.archivo_subido != '':
        if recalificar == 'Excelente':
            calificacion.puntaje = acto.calificacion_maxima*3
        elif recalificar == 'Valida':
            calificacion.puntaje = acto.calificacion_maxima
        elif recalificar == 'Incompleta':
            calificacion.puntaje = 1
        elif recalificar == 'No Valida':
            calificacion.puntaje = 0
        calificacion.recalificado = True
        calificacion.save()
    detalle.calificado = True
    detalle.save()

def recalificar_acto_joven(detalle, cuarto_reto, acto, recalificar):
    '''
        Recalifica el puntaje que obtuvo un Joven en un Acto dependiento de la decisión del jurado

        Parametros:            
            detalle: Objeto del Modelo DetalleJoven
            acto: Objeto del modelo Acto que se recalificara  
            recalificar: decisión del jurado 
    '''
    calificacion = CalificacionActoJoven.objects.get(detalle_joven_reto=detalle, acto=acto)
    if calificacion.archivo_subido is not None and calificacion.archivo_subido != '':
        if recalificar == 'Excelente':
            calificacion.puntaje = acto.calificacion_maxima*3
        elif recalificar == 'Valida':
            calificacion.puntaje = acto.calificacion_maxima
        elif recalificar == 'Incompleta':
            calificacion.puntaje = 1
        elif recalificar == 'No Valida':
            calificacion.puntaje = 0
        calificacion.recalificado = True
        calificacion.save()
    detalle.calificado = True
    detalle.save()

def calificar_preguntas_joven(detalle, id_pregunta, id_opcion):
    '''
        Califica las preguntas de un Joven verificando que la opcion seleccionada sea la correcta

        Parametros:            
            detalle: Objeto del Modelo DetalleJoven
            id_pregunta: id de la Pregunta a calificar  
            id_opcion: id de la Opción que fue seleccionada por el Joven
    '''
    pregunta = Pregunta.objects.get(pk=id_pregunta)
    calificacion = CalificacionPreguntaJoven.buscar(detalle, pregunta)
    
    opciones_corecta = pregunta.opciones_respuestas.get(es_correcta=True)
    calificacion.opcion = Opcion.objects.get(pk=id_opcion)
    calificacion.save()
    puntaje = 0
    if opciones_corecta.pk == id_opcion:
        calificacion.puntaje = pregunta.dificultad
    
    calificacion.save()

def calificar_preguntas_equipo(detalle, id_pregunta, id_opcion):
    '''
        Califica las preguntas de un Equipo verificando que la opcion seleccionada sea la correcta

        Parametros:            
            detalle: Objeto del Modelo DetalleEquipo
            id_pregunta: id de la Pregunta a calificar  
            id_opcion: id de la Opción que fue seleccionada por el Joven
    '''
    calificacion = CalificacionPreguntaEquipo.objects.get(detalle_equipo_reto=detalle, pregunta__id=id_pregunta)
    pregunta = Pregunta.objects.get(pk=id_pregunta)
    opciones_corecta = pregunta.opciones_respuestas.get(es_correcta=True)
    calificacion.opcion = Opcion.objects.get(pk=id_opcion)
    if opciones_corecta.pk == id_opcion:
        calificacion.puntaje = pregunta.dificultad
    else:
        calificacion.puntaje = 0
    calificacion.save()

def retornar_tiempo_inicio_cuarto(reto, cuarto, detalle):
    '''
        Busca y retorna la fecha inicio de un cuarto, si el cuarto no tiene fecha inicio se le asigna la hora
        actual como fecha de incio al cuaro.

        Parametros:
            reto: Objeto del Modelo Reto
            cuarto: Objecto del Modelo Cuarto           
            detalle: Objeto del Modelo Detalle(Joven, Equipo)
        
        Retorno:
            Si existe cuarto: fechainicio del cuarto
            Si NO existe: None
    '''
    if 'Equipos' in reto.grupos_permitidos:
        modelo = EquipoRetoCuarto
    else:
        modelo = JovenRetoCuarto

    try:
        cuarto_reto = modelo.buscar(cuarto, detalle)
        if cuarto_reto and cuarto_reto.fecha_inicio is None:
            cuarto_reto.fecha_inicio = timezone.now()
            cuarto_reto.save()
        return cuarto_reto.fecha_inicio
    except modelo.DoesNotExist:
        return None

def calcular_tiempo_faltante_reto(reto):
    '''
        Calcula y retorna la cantidad de tiempo que falta para que acabe el Reto
        actual como fecha de incio al cuaro.

        Parametros:
            reto: Objeto del Modelo Reto            
        
        Retorno:
            tiempo_faltante: número entero
    '''
    tiempo_faltante = (timezone.now() - reto.fecha_inicio).seconds*1000
    return tiempo_faltante


def crear_retos_cuarto_participante(reto, detalle):
    '''
        Crea los objetos del modelo EquipoRetoCuarto o JovenRetoCuarto dependiento de
        los grupos permitidos en el Reto


        Parametros:
            reto: Objeto del Modelo Reto           
            detalle: Objeto del Modelo Detalle(Joven, Equipo)        
    '''
    cuartos = reto.cuartos.all().order_by('cuarto_reto_cuarto__orden')    
    for cuarto in cuartos:
        if 'Equipos' in reto.grupos_permitidos:
            EquipoRetoCuarto.crear(cuarto, detalle)
        else:            
            JovenRetoCuarto.crear(cuarto, detalle)

def obtener_participante(reto, id_participante):
    '''
        Retorna el participante del Reto dependiento de los grupos permitidos en el Reto
        y el id_del participante

        Parametros:
            reto: Objeto del Modelo Reto           
            id_participante: id del participante(Joven, Equipo)
    
        Retorno:
            Si reto es para Equipos: Objeto del Modelo Equipo
            Si reto es para Jóvenes: Objeto del Modelo Joven
    '''
    
    if 'Equipos' in reto.grupos_permitidos:
        return Equipo.buscar(id_participante)
    else:
        return Joven.buscar(id_participante)


def eliminar_detalle_de_jurado(detalle, jurado):

    if 'Equipos' in detalle.reto.grupos_permitidos:
        jurado.equipo_reto_evaluado.remove(detalle)
    else:
        jurado.joven_reto_evaluado.remove(detalle)



def obtener_detalle_participante(reto, id_participante):
    '''
        Retorna el Detalle(Joven, Equipo) del participante dependiento de los grupos permitidos en el Reto

        Parametros:
            reto: Objeto del Modelo Reto           
            id_participante: id del participante(Joven, Equipo)
    
        Retorno:
            Si reto es para Equipos: Objeto del Modelo DetalleEquipoReto
            Si reto es para Jóvenes: Objeto del Modelo DetalleJovenReto
    '''
    if 'Equipos' in reto.grupos_permitidos:
        return DetalleEquipoReto.buscar(id_participante, reto.pk)
    else:
        return DetalleJovenReto.buscar(id_participante, reto.pk)

def obtener_calificaciones_participante(reto, detalle, cuarto):
    '''
        Retorna la Calificacion(Joven, Equipo) del participante dependiento de los grupos permitidos en el Reto
        y el tipo de Cuarto

        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo) que relaciona al participante con el Cuarto y el Reto
            cuarto: Objeto del Modelo Cuarto para crear las calificaciones
                
        Retorno:
            Si reto es para Equipos: 
                    Si tipo cuarto es Pregunta: CalificacionPreguntaEquipo
                    Si tipo cuarto NO es pregunta: CalificacionActoEquipo
            Si reto es para Jóvenes: Objeto del Modelo DetalleJovenReto
                    Si tipo cuarto es Pregunta: CalificacionPreguntaJoven
                    Si tipo cuarto NO es pregunta: CalificacionActoJoven
    '''
    if 'Equipos' in reto.grupos_permitidos:
        if cuarto.tipo.nombre == 'Pregunta':
            return CalificacionPreguntaEquipo.buscar_varios(detalle, cuarto.preguntas.all())
        else:
            return CalificacionActoEquipo.buscar_varios(detalle, cuarto.actos.all())
    else:
        if cuarto.tipo.nombre == 'Pregunta':
            return CalificacionPreguntaJoven.buscar_varios(detalle, cuarto.preguntas.all())
        else:    
            return CalificacionActoJoven.buscar_varios(detalle, cuarto.actos.all())


def dispatch_funciones(request):
    '''
        Funcion que se encarga de realizar las verificaciones de si el usuario es superusuario, es tipo organizacion y esta autentificado y
        retornar un diccionario con las redirecciones si el usuario no cumple con los requisitos.

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
                
        Retorno:
            context: dict()
    '''
    context = dict()
    context['exito'] = False

    if not request.user.is_authenticated:
        context['redireccion'] = redirect('usuarios:iniciar_sesion')
        return context
    
    if request.user.is_superuser or request.user.tipo == 'Organización':
        context['mensaje'] = 'Usted no está autorizado para realizar la acción!'
        context['redireccion'] = redirect('inicio')
        return context
    context['exito'] = True
    return context['exito']

def verificaciones_iniciales(id_reto, id_participante, request, cuarto_final=False, v_detalle=True):
    '''
        Funcion que se encarga de realizar las verificaciones de si existe el Reto, el Participante(Joven, Equipo), el Detalle, y los Cuartos
        retornar un diccionario con las redirecciones si el usuario no cumple con los requisitos o un diccionarios con el elementos buscados.

        Parametros:
            id_reto: id del Modelo Reto
            request: Elemento Http que posee informacion de la sesion.
            id_participante: id del participante(Joven, Equipo)
            cuarto_final: Determina si se quieren buscar los cuartos, False-Buscar, True-No Buscar
            v_detalle: Determina si se quiere buscar el detalle, False-No Buscar, True-Buscar
                
        Retorno:
            context: dict()
    '''
    
    if dispatch_funciones(request) != True:
        return dispatch_funciones(request)
    
    context = dict()
    context['verificado'] = False
    reto = Retos.buscar(id_reto)
    if reto is None:
        context['mensaje'] = 'El Reto no existe'
        context['redireccion'] = redirect('retos:consultar_retos')
        return context

    participante = obtener_participante(reto, id_participante)
    detalle = obtener_detalle_participante(reto, id_participante)
        
    if participante is None:
        context['mensaje'] = 'El paricipante que intenta acceder al reto no existe'
        context['redireccion'] = redirect('retos:consultar_reto', reto.pk)
        return context

    if v_detalle:
        if detalle == None:            
            context['mensaje'] = 'Usted no está inscrito en el reto'
            context['redireccion'] = redirect('retos:consultar_reto', reto.pk)
            return context    
    
    if cuarto_final == False:
        if detalle.completado == True is None or detalle.abandonado == True:
            context['mensaje'] = 'Has finalizado el reto'
            if 'Equipos' in reto.grupos_permitidos:
                context['redireccion'] = redirect('retos:cuarto_finalizacion', reto.pk, id_participante)
            else:
                context['redireccion'] = redirect('retos:cuarto_finalizacion', reto.pk)
            return context
    
    context['verificado'] = True
    context['reto'] = reto
    context['participante'] = participante
    context['detalle'] = detalle
    return context

def configuracion_cuarto_reconocimiento_equipos(id_reto, id_participante, request):
    '''
        Configura la vista del cuato de reconocimiento de equipos retornando los objetos buscados.
        Parametros:
            id_reto: id del Modelo Reto
            request: Elemento Http que posee informacion de la sesion.
            id_participante: id del participante(Joven, Equipo)            
                
        Retorno:
            context: dict()
    '''
    from django.shortcuts import render, redirect
    context = dict()
    context['exito'] = False
    
    verificacion = verificaciones_iniciales(id_reto, id_participante, request)
    if  verificacion['verificado'] != True:
        verificacion.update(context)
        return verificacion
    else:
        reto = verificacion['reto']
        participante = verificacion['participante']
        detalle = verificacion['detalle']
    
    context['exito'] = True
    context['finalizacion'] = False
    context['reto'] = reto
    context['participante'] = participante
    context['detalle'] = detalle
    return context


def contruir_context_cuarto_reconocimiento_equipos(reto, detalle, participante):
    '''
        Costruye el context del cuarto reconocimiento equipos
        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo)
            participante: Objeto del Modelo Joven o Equipo
                
        Retorno:
            context: dict()
    '''
    context = dict()
    context['reto'] = reto
    context['equipo'] = participante
    context['miembros'] = participante.miembros.all()
    context['espera'] = 'True'
    context['fechas'] = {'fecha_actual': timezone.now(),
                        'fecha_inicio':reto.fecha_inicio,
                        'fecha_fin':reto.fecha_fin}
    return context

def configuracion_cuarto_espera_equipos(id_reto, id_participante, request):
    '''
        Configura la vista del cuarto de espera equipos retornando los objetos buscados.
        Parametros:
            id_reto: id del Modelo Reto
            request: Elemento Http que posee informacion de la sesion.
            id_participante: id del participante(Joven, Equipo)            
                
        Retorno:
            context: dict()
    '''
    from django.shortcuts import render, redirect
    context = dict()
    context['exito'] = False
    
    verificacion = verificaciones_iniciales(id_reto, id_participante, request)
    if  verificacion['verificado'] != True:
        verificacion.update(context)
        return verificacion
    else:
        reto = verificacion['reto']
        participante = verificacion['participante']
        detalle = verificacion['detalle']
        
    joven = Joven.objects.get(pk=request.user.pk)
    detalle_joven = DetalleJovenReto.buscar(joven.pk, reto.pk)
    
    if detalle_joven.abandonado == True:
        context['mensaje'] = 'Has abandonado el Reto con este Equipo'
        context['redireccion'] = redirect('retos:consultar_reto', reto.pk)
        return context

    context['exito'] = True
    context['reto'] = reto
    context['participante'] = participante
    context['detalle'] = detalle
    context['finalizacion'] = False
    context['barra'] = True
    return context

def contruir_context_cuarto_espera_equipos(reto, detalle, participante):    
    '''
        Costruye el context del cuarto espera equipos
        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo)
            participante: Objeto del Modelo Joven o Equipo            
                
        Retorno:
            context: dict()
    '''
    context = dict()
    context['reto'] = reto
    context['equipo'] = participante
    context['miembros'] = participante.miembros.all()
    context['espera'] = True
    context['detalle'] = detalle
    context['detalle_objeto'] = detalle
    context['nombre_participante'] = participante.nombre
    context['fechas'] = {'fecha_actual': timezone.now(),
                        'fecha_inicio':reto.fecha_inicio,
                        'fecha_fin':reto.fecha_fin}
    context['barra_progreso'] = False
    context['finalizacion'] = False

   
    return context

def obtener_elementos_configuracion_responder_cuartos(id_reto, id_participante, request):
    '''
        Configura la vista de responder cuartos retornando los objetos buscados.
        Parametros:
            id_reto: id del Modelo Reto
            request: Elemento Http que posee informacion de la sesion.
            id_participante: id del participante(Joven, Equipo)            
                
        Retorno:
            context: dict()
    '''
    from django.shortcuts import render, redirect
    context = dict()
    context['exito'] = False
    
    verificacion = verificaciones_iniciales(id_reto, id_participante, request)
    if  verificacion['verificado'] != True:
        verificacion.update(context)
        return verificacion
    else:
        reto = verificacion['reto']
        participante = verificacion['participante']
        detalle = verificacion['detalle']        

    crear_retos_cuarto_participante(reto, detalle)
    cuarto = Cuarto.obtener_cuarto_actual_participante(detalle, False, reto, False)

    if cuarto is None:        
        if detalle.completado == False:
            detalle.completado = True
            detalle.save()
        
        if 'Equipos' in reto.grupos_permitidos:
            context['redireccion'] = redirect('retos:cuarto_finalizacion', reto.pk, id_participante)
        else:
            context['redireccion'] = redirect('retos:cuarto_finalizacion', reto.pk)
        return context

    if 'Equipos' in reto.grupos_permitidos:
        modelo = CalificacionActoEquipo
    else:
        modelo = CalificacionActoJoven

    crear_calificacion_participante(reto, cuarto, detalle)
    calificaciones = obtener_calificaciones_participante(reto, detalle, cuarto)
   
    context['exito'] = True
    context['reto'] = reto
    context['participante'] = participante
    context['detalle'] = detalle
    context['finalizacion'] = False
    context['cuarto_actual'] = cuarto
    context['calificaciones'] = calificaciones
    context['modelo'] = modelo
    return context

def contruir_context_responder_cuartos(reto, detalle, participante, cuarto):
    '''
        Construye el context del cuarto responder cuartos
        
        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo)
            participante: Objeto del Modelo Joven o Equipo            
                
        Retorno:
            context: dict()
    '''
    from django.db.models import Count 
   
    cuarto_sin_completar = Cuarto.obtener_cuartos_sin_completar_participante(detalle, False, reto, False).count()
    
    fecha_inicio_cuarto = retornar_tiempo_inicio_cuarto(reto, cuarto, detalle)
    context = dict()
    context['cuarto'] = cuarto
    context['orden'] = cuarto.cuarto_reto_cuarto.values('orden').first()
    context['cantidad_actos'] = cuarto.actos.all().count()
    context['reto'] = reto
    context['id_participante'] = participante.pk
    context['detalle'] = detalle.pk
    context['detalle_objeto'] = detalle
    context['tiempo_transcurrido'] = (timezone.now()-fecha_inicio_cuarto).seconds*1000
    context['porcentaje'] = obtener_porcentaje_completado_reto(reto, detalle)
    context['barra_progreso'] = True
    context['finalizacion'] = False
    context['cuarto_sin_completar'] = cuarto_sin_completar

    if 'Equipos' in reto.grupos_permitidos:
        context['redireccion_formulario'] = redirect('retos:retos_responder_cuartos', reto.pk, participante.pk)
        context['nombre_participante'] = participante.nombre
        context['equipo'] = participante
    else:
        context['redireccion_formulario'] = redirect('retos:retos_responder_cuartos', reto.pk)
        context['nombre_participante'] = participante.first_name + ' ' + participante.last_name

    if cuarto.tipo.nombre == 'Preguntas':
        context['template'] = 'retos/cuartos/pagina_preguntas.html'
        context['preguntas'] = cuarto.preguntas.all()
    else:
        context['template'] = 'retos/cuartos/pagina_actos.html'
    return context

def configuracion_abandonar_reto(id_reto, id_participante, request):
    '''
        Configura la vista de abandonar reto retornando los objetos buscados.

        Parametros:
            id_reto: id del Modelo Reto
            request: Elemento Http que posee informacion de la sesion.
            id_participante: id del participante(Joven, Equipo)            
                
        Retorno:
            context: dict()
    '''
    from django.shortcuts import render, redirect

    context = dict()
    context['exito'] = False
    
    verificacion = verificaciones_iniciales(id_reto, id_participante, request)
    if  verificacion['verificado'] != True:
        verificacion.update(context)
        return verificacion
    else:
        reto = verificacion['reto']
        participante = verificacion['participante']
        detalle = verificacion['detalle']
        joven = Joven.objects.get(pk=request.user.pk)

    if 'Equipos' in reto.grupos_permitidos:        
        if participante.lider == joven:            
            detalle.abandonado = True
            detalle.save()
            for joven in participante.obtener_miembros():
                reto.eliminar_joven_detalle(joven)
            context['redireccion'] = redirect("retos:cuarto_finalizacion", id_reto, id_participante)
        else:
            detalle_joven = DetalleJovenReto.buscar(joven.pk, reto.pk)
            detalle_joven.abandonado = True
            detalle_joven.save()
            context['reto'] = reto
            context['equipo'] = participante
            context['nombre_participante'] = request.user.first_name+ ' ' +request.user.last_name
            context['redireccion'] = render(request, 'retos/cuartos/abandono_miembro_equipo.html', context)
    else:
        detalle.abandonado = True
        detalle.save()
        context['redireccion'] = redirect("retos:cuarto_finalizacion", id_reto)
    context['exito'] = True
    context['finalizacion'] = True
    return context

def obtener_elementos_configuracion_descanso(id_reto, id_participante, request):
    '''
        Configura la vista de cuarto descanso.
        
        Parametros:
            id_reto: id del Modelo Reto
            request: Elemento Http que posee informacion de la sesion.
            id_participante: id del participante(Joven, Equipo)            
                
        Retorno:
            context: dict()
    '''
    from django.shortcuts import render, redirect
   
    context = dict()
    context['exito'] = False    
   
    verificacion = verificaciones_iniciales(id_reto, id_participante, request)
    if  verificacion['verificado'] != True:
        verificacion.update(context)
        return verificacion
    else:
        reto = verificacion['reto']
        participante = verificacion['participante']
        detalle = verificacion['detalle']

    cuarto_siguiente = Cuarto.obtener_siguiente_cuarto_responder(reto, detalle)
    if cuarto_siguiente is None:
        if 'Equipos' in reto.grupos_permitidos:
            context['redireccion'] = redirect('retos:cuarto_finalizacion', id_reto, id_participante)
        else:
            context['redireccion'] = redirect('retos:cuarto_finalizacion', id_reto)
        return context

    context['exito'] = True
    context['reto'] = reto
    context['participante'] = participante
    context['detalle'] = detalle
    context['finalizacion'] = False

    return context

def contruir_context_descanso(reto, detalle, participante):
    '''
        Costruye el context del cuarto descanso.
        
        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo)
            participante: Objeto del Modelo Joven o Equipo            
                
        Retorno:
            context: dict()
    '''
    context = dict()
    cuartos_responder = Cuarto.obtener_cuartos_responder_valores(reto, detalle)
    cuarto_siguiente = Cuarto.obtener_siguiente_cuarto_responder(reto, detalle)
    cuarto_descanso = Cuarto.obtener_cuarto_descanso(reto, detalle)

    context['reto'] = reto
    context['cuartos_responder'] = cuartos_responder
    context['cuarto_siguiente'] = cuarto_siguiente.pk
    context['cuarto'] = cuarto_descanso
    context['porcentaje'] = obtener_porcentaje_completado_reto(reto, detalle)
    context['finalizacion'] = False
    
    if 'Equipos' in reto.grupos_permitidos:
        context['nombre_participante'] = participante.nombre
        context['equipo'] = participante
    else:
        context['nombre_participante'] = participante.first_name + ' ' + participante.last_name
        
    return context

def obtener_elementos_configuracion_cuarto_finalizacion(id_reto, id_participante, request):
    '''
        Configura la vista del cuarto finalización.

        Parametros:
            id_reto: id del Modelo Reto
            request: Elemento Http que posee informacion de la sesion.
            id_participante: id del participante(Joven, Equipo)            
                
        Retorno:
            context: dict()
    '''
    from django.shortcuts import render, redirect
    context = dict()
    context['exito'] = False
    
    verificacion = verificaciones_iniciales(id_reto, id_participante, request, cuarto_final=True)
    if  verificacion['verificado'] != True:
        verificacion.update(context)
        return verificacion
    else:
        reto = verificacion['reto']
        participante = verificacion['participante']
        detalle = verificacion['detalle']
    
    context['exito'] = True
    context['reto'] = reto
    context['detalle'] = detalle

    cuarto_actual = Cuarto.obtener_siguiente_cuarto_responder(reto, detalle)
    
    if cuarto_actual:      
        reto.actualizar_fecha_fin_cuarto(cuarto_actual, detalle)
    #cuarto_actual.fe

    context['resumen_reto'] = ({'tiempos_cuartos':calcular_tiempos_participante(reto, detalle)},
                            {'Evidencias Subidas':obtener_evidencias_subidas_participante(reto, detalle)},
                            {'Preguntas Contestadas':obtener_preguntas_contestadas_participante(reto, detalle)},
                            {'Cuartos Completados':calcular_cuartos_completados_participante(reto, detalle)},                            
                            {'Puntaje Final':obtener_puntaje_final(reto, detalle)})
    context['porcentaje'] = obtener_porcentaje_completado_reto(reto, detalle)

    if 'Equipos' in reto.grupos_permitidos:
        context['nombre_participante'] = participante.nombre
        context['equipo'] = participante
    else:
        context['nombre_participante'] = participante.first_name + ' ' + participante.last_name
    context['finalizacion'] = True
    return context

def obtener_elementos_configuracion_inicio_reto(id_reto, id_participante, request):
    '''
        Configura la vista del cuarto inicio reto.

        Parametros:
            id_reto: id del Modelo Reto
            request: Elemento Http que posee informacion de la sesion.
            id_participante: id del participante(Joven, Equipo)            
                
        Retorno:
            context: dict()
    '''
    from django.shortcuts import render, redirect
    context = dict()
    context['exito'] = False
    
    verificacion = verificaciones_iniciales(id_reto, id_participante, request, cuarto_final=True, v_detalle=True)
    if  verificacion['verificado'] != True:
        verificacion.update(context)
        return verificacion
    else:
        reto = verificacion['reto']
        participante = verificacion['participante']

    if 'Equipos' in reto.grupos_permitidos:        
        context['equipo'] = participante
    
    context['reto'] = reto
    context['exito'] = True
    context['fechas'] = {'fecha_actual': timezone.now(),
                        'fecha_inicio': reto.fecha_inicio,
                        'fecha_fin': reto.fecha_fin}
    return context

def calcular_tiempos_participante(reto, detalle):
    '''
        Calcula el tiempo que se tardo el participante en completar los cuartos.

        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo)                    
                
        Retorno:
            lista de diccionarios
    '''
    cuartos_responder = Cuarto.obtener_cuartos_responder(reto, detalle)
    if 'Equipos' in reto.grupos_permitidos:
        cuartos_participante = EquipoRetoCuarto.objects.filter(cuarto__in=cuartos_responder, detalle=detalle, cuarto__cuarto_reto_cuarto__reto=reto).order_by('cuarto__cuarto_reto_cuarto__orden')
    else:
        cuartos_participante = JovenRetoCuarto.objects.filter(cuarto__in=cuartos_responder, detalle=detalle, cuarto__cuarto_reto_cuarto__reto=reto).order_by('cuarto__cuarto_reto_cuarto__orden')

    
    lista_cuartos_tiempos = []
    index = 1
    tiempo_total = 0

    for cuarto in cuartos_participante:
        if cuarto.fecha_finalizacion and cuarto.fecha_inicio:
            lista_cuartos_tiempos.append({('Tiempo Cuarto ' + str(index)) : normalize_seconds(calcular_tiempo_segundos(cuarto.fecha_finalizacion, cuarto.fecha_inicio))})
            tiempo_total += calcular_tiempo_segundos(cuarto.fecha_finalizacion, cuarto.fecha_inicio)
        else:
            lista_cuartos_tiempos.append({('Tiempo Cuarto ' + str(index)) : 'Cuarto no Iniciado'})
                   
        index += 1

    return [{'Tiempo Total ': normalize_seconds(tiempo_total)}] + lista_cuartos_tiempos


def calcular_tiempo_segundos(fecha_final, fecha_inicial):
    '''
        Retorna el tiempo en segundos entre dos fechas.

        Parametros:
            fecha_inicial: Fecha inicial
            fecha_final: Fecha final                  
                
        Retorno:
            tiempo_segundos: número entero
    '''
    return (fecha_final - fecha_inicial).seconds


def normalize_seconds(seconds: int) -> tuple:    
    '''
        Configura el tiempo en segundos para darle un formato mas facil de leer para las personas

        Parametros:
            seconds: segundos a normalizar  
                
        Retorno:
            tiempo_en_formato: String
    '''
    from collections import namedtuple

    (hours, remainder) = divmod(seconds, 3600)
    (minutes, seconds) = divmod(remainder, 60)

    tupla = (hours, minutes, seconds)
    tiempo_en_formato = ""
    if tupla[0] != 0:
        if tupla[0] > 1:
            tiempo_en_formato += str(tupla[0]) + " horas "
        else:
            tiempo_en_formato += str(tupla[0]) + " hora "
    if tupla[1] != 0:
        if tupla[1] > 1:
            tiempo_en_formato += str(tupla[1]) + " minutos "
        else:
            tiempo_en_formato += str(tupla[1]) + " minuto "
    if tupla[2] != 0:
        if tupla[2] > 1:
            tiempo_en_formato += str(tupla[2]) + " segundos"
        else:
            tiempo_en_formato += str(tupla[2]) + " segundo"

    return tiempo_en_formato


def calcular_cuartos_completados_participante(reto, detalle):
    '''
        Retorna la cantidad de cuartos completados por el participante

        Parametros:
            reto: Objeto del Modelo Reto
            detalle: Objeto del Modelo Detalle(Joven, Equipo)   
                
        Retorno:
            cantidad_cuartos: número entero
    '''
    from django.db.models import Count

    cuartos_responder = Cuarto.obtener_cuartos_responder(reto, detalle)
    if 'Equipos' in reto.grupos_permitidos:
        modelo = EquipoRetoCuarto
    else:
        modelo = JovenRetoCuarto
        
    cantidad_cuartos = modelo.obtener_cuartos_completados(detalle, cuartos_responder).count()

    return cantidad_cuartos





