# Modulos Django
from django.contrib.postgres.fields import ArrayField
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.postgres.fields import JSONField

# Modulos de plugin externos
from cities_light.models import City
from ckeditor.fields import RichTextField
from datetime import datetime
from decimal import Decimal
from simple_history.models import HistoricalRecords

# Modulos de otras apps
from kairos.organizaciones.models import Organizacion
from kairos.cuartos.models import Cuarto, Acto, Pregunta
from kairos.equipos.models import Equipo
from kairos.jurados.models import Jurado, RetroalimentacionJuradoEquipo, RetroalimentacionJuradoJoven
from kairos.usuarios.models import Joven, Usuario


# Modulos internos de la app

# class TipoReto(models.Model):
#    nombre = models.CharField(max_length=50, verbose_name="nombre del tipo*")
#    descripcion = models.CharField(max_length=300, verbose_name="descripción*")
#    activo = models.BooleanField(verbose_name="¿El tipo de reto se encuentra activo?", default=True)
#    history = HistoricalRecords()

#    def __str__(self):
#        return self.nombre

def crear_ruta_imagen(instance, filename):
    return "retos/imagen/%s" % (filename.encode('ascii', 'ignore'))


def crear_ruta_logo(instance, filename):
    return "retos/logos/%s" % (filename.encode('ascii', 'ignore'))


class Retos(models.Model):
    GRUPOS = (
        ("Jóvenes", "Jóvenes"),
        ("Equipos", "Equipos")
        )
    #  tipo = models.ForeignKey(TipoReto, on_delete=models.CASCADE, verbose_name="tipo de reto*")
    jurados = models.ManyToManyField('jurados.Jurado', related_name='retos_jurados', verbose_name="Jurados asignados")
    competencias = models.ManyToManyField('Competencia', related_name='retos_competencias', verbose_name="Competencias asignadas*", blank=False)
    organizacion = models.ForeignKey('organizaciones.Organizacion', related_name='retos_organizacion', verbose_name='Organización*', null=False, blank=False, on_delete=models.CASCADE)
    organizaciones_permitidas = models.ManyToManyField('organizaciones.Organizacion', related_name="retos_organizaciones_permitidas", verbose_name="Organizaciones permitidas", blank=True)
    jovenes = models.ManyToManyField('usuarios.Joven', through='DetalleJovenReto', related_name='retos_joven', verbose_name="Jóvenes inscritos")
    solicitudes_jovenes = models.ManyToManyField(Joven, through='SolicitudesRetosJovenes', related_name="retos_joven_solicitudes", blank=True, verbose_name="Jovenes que van a ser parte del reto")
    equipos = models.ManyToManyField('equipos.Equipo', through='DetalleEquipoReto', related_name='retos_equipo', verbose_name="Equipos inscritos")
    solicitudes_equipos = models.ManyToManyField(Equipo, through='SolicitudesRetosEquipos', related_name="retos_equipo_solicitudes", blank=True, verbose_name="Equipos que van a ser parte del reto")
    cuartos = models.ManyToManyField(Cuarto, through='CuartoReto', related_name="cuartos_retos", blank=True, verbose_name="cuartos del reto")
    nombre = models.CharField(max_length=50, verbose_name="nombre del reto*", null=False, blank=False)
    descripcion = RichTextField(verbose_name="¿Cuál es el reto?*", null=False, blank=False)
    donde_hacerlo = models.CharField(max_length=300, verbose_name="¿Dónde lo vas a hacer?*", null=False, blank=False)    

    caracteristicas = models.CharField(max_length=300, verbose_name="¿Qué tienes que tener en cuenta?*", null=False, blank=False)
    #caracteristicas = models.CharField(max_length=300, verbose_name="¿Qué tienes que tener en cuenta?*", null=False, blank=False)
    fecha_inicio = models.DateTimeField(verbose_name="fecha inicio de reto", null=False, blank=False)
    fecha_fin = models.DateTimeField(verbose_name="fecha final de reto*", null=False, blank=False)
    precio = models.DecimalField(decimal_places=2, max_digits=7, validators=[MinValueValidator(Decimal('0.00'))],
                                 verbose_name="¿Cuánto dinero debo invertir?*", null=False, blank=False)
    premios = models.CharField(max_length=300, verbose_name="Premios e incentivos*")
    #link = models.CharField(max_length=100, verbose_name="Url de acceso", blank=True)
    areas_conocimiento = models.ManyToManyField('insignias.Tribus', verbose_name='Áreas de conocimiento de este reto*')
    niveles_conocimiento_deseado = models.ManyToManyField('insignias.NivelesDeConocimiento', verbose_name='Niveles de conocimiento de este reto*')
    roles = models.ManyToManyField('insignias.Roles', verbose_name='Roles de este reto*')

    logo = models.FileField(upload_to=crear_ruta_logo, null=True, blank=True)
    imagen = models.FileField(upload_to=crear_ruta_imagen, null=True, blank=True)

    contacto = models.CharField(max_length=50, verbose_name="nombre de contacto*")
    email = models.EmailField(verbose_name="correo de contacto*")
    telefono = models.CharField(max_length=20, verbose_name="teléfono de contacto*")

    grupos_permitidos = models.CharField(max_length=50, choices=GRUPOS,
                                    verbose_name='¿Quién puede inscribirse al reto?*', null=False, blank=False)

    activo = models.BooleanField(verbose_name="¿El reto se encuentra activo?", default=True)
    activo_para_jovenes = models.BooleanField(verbose_name="¿El reto es para jóvenes?", default=False)
    activo_para_equipos = models.BooleanField(verbose_name="¿El reto es para equipos?", default=False)

    link_video = models.URLField(max_length=128, verbose_name='Link del video*', null=True, blank=False)
    link_video_participacion = models.URLField(max_length=128, verbose_name='Link video cuarto participación*', null=True, blank=False)
    link_video_reconocimiento = models.URLField(max_length=128, verbose_name='Link video cuarto de reconocimiento*', null=True, blank=False)
    link_video_descanso = models.URLField(max_length=128, verbose_name='Link video cuarto de descanso*', null=True, blank=False)
    reglas = models.TextField(verbose_name='Reglas*', null=True, blank=False)
    grado_dificultad = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)],
                                                   verbose_name='Grado de dificultad (1-10)*', null=True, blank=False)
    permitir_todos = models.BooleanField(verbose_name="¿El reto se encuentra activo para todos los jóvenes y equipos?", default=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.nombre)

    @staticmethod
    def obtener_retos_activos() -> 'QuerySet[Retos]':
        """
        Retorna una lista de los retos activos.

        Retorna:
            QuerySet[Retos]: Retos activos
        """
        retos = Retos.objects.filter(activo=True)
        return retos

    @staticmethod
    def favoritas(usuario:'Usuario') -> 'QuerySet[Retos]':
        """
        Retorna una lista de los retos marcados como favoritos por el usuario.

        Parametros:
            usuario (Usuario): Usuario en sesion

        Retorna:
            QuerySet[Retos]: Retos favoritos
        """
        retos = Retos.obtener_retos_activos()
        mis_favoritos = retos.filter(calificacion_reto_del_joven__usuario=usuario, calificacion_reto_del_joven__calificacion=1)
        return mis_favoritos.distinct()

    @staticmethod
    def existe_reto_por_id(id_reto:int) -> bool:
        """
        Retorna si existe o no un reto usando el id como parametro de busqueda

        Parametros:
            id_reto (int): id reto a buscar

        Retorna:
            bool: True si existe, False si NO existe
        """
        try:
            reto = Retos.objects.get(pk=id_reto)
        except Retos.DoesNotExist:
            return False
        return True

    @staticmethod
    def buscar(id_reto:int) -> 'Reto':
        """
        Retorna el reto SI existe usando el id como parametro de busqueda

        Parametros:
            id_reto (int): id reto a buscar        

        Retorna:
            Reto: Reto buscado si existe, None si NO existe
        """
        
        try:
            reto = Retos.objects.get(id=id_reto)
            return reto
        except Retos.DoesNotExist:
            return None
    
    def obtener_puntajes_participantes(self):
        from .utils import obtener_puntaje_final
        
        if 'Equipos' == self.grupos_permitidos:
            participantes = self.equipos.all()
            lista_equipos = list(participantes.values_list('pk', flat=True))            
            detalles = DetalleEquipoReto.buscar_varios(lista_equipos, self.pk)
        else:
            participantes = self.jovenes.all()
            lista_jovenes = list(participantes.values_list('pk', flat=True))            
            detalles = DetalleJovenReto.buscar_varios(lista_jovenes, self.pk)

        for detalle in detalles:
            obtener_puntaje_final(self, detalle)
        
        return detalles.order_by('-calificacion_final')

    def reto_finalizado(self):
        from django.utils import timezone

        if timezone.now() > self.fecha_fin:
            return True
        
        return False

    def enviar_correos_notificacion_finalizacion(self):
        from kairos.gestor_correos.envio_correos import enviar_correo, NOTIFICACION_RETO_FINALIZADO
        from kairos.retos.utils import obtener_puntaje_final, obtener_detalle_participante

        equipos = list(self.obtener_equipos_miembros().values_list('miembros__email', flat=True))                   
            
        if 'Equipos' in self.grupos_permitidos:            
            ids_equipos = list(self.equipos.values_list("equipo_detalle_reto__pk", flat=True))     
            equipos = self.equipos.all()
            for equipo in equipos:     
                
                detalle = obtener_detalle_participante(self, equipo.pk)                
                if detalle.notificado == False:
                    obtener_puntaje_final(self, detalle)
                             

                    miembros = detalle.miembros_activos
                    
                    for miembro, asistencia in miembros.items():
                        
                        if asistencia:
                            joven = Joven.objects.get(pk=miembro)
                        elif equipo.lider.pk == miembro:
                            joven = Joven.objects.get(pk=equipo.lider.pk)
                        else:
                            joven = Joven.objects.none()
                            
                        if joven:                              
                            enviar_correo(joven.email, (self, equipo), NOTIFICACION_RETO_FINALIZADO, joven)
                        detalle.notificado = True
                        detalle.save()
        else:
            ids_jovenes = list(self.equipos.values_list("joven_detalle_reto__pk", flat=True))     
            jovenes = self.jovenes.all()
            for joven in jovenes:               
                detalle = obtener_detalle_participante(self, joven.pk)
                if detalle.notificado == False:
                    obtener_puntaje_final(self, detalle)                    
                    joven = detalle.joven                        
                    if joven:  
                        enviar_correo(joven.email, (self, joven), NOTIFICACION_RETO_FINALIZADO, joven)
                    detalle.notificado = True
                    detalle.save()
            
    
    def actualizar_fecha_fin_cuarto(self, cuarto:"Cuarto", detalle):
        """
        Busca el la informacion de un cuarto que esta relacionado con un participante usando el detalle como parametro de busqueda
        y actualiza su fecha de finalización.

        Args:
            cuarto (Cuarto): Cuarto al que sera usado como parametro de busqueda
            detalle (DetalleEquipoReto o DetalleJovenReto): Detalle al que sera usado como parametro de busqueda
        """
        from django.utils import timezone

        from kairos.retos.utils import retornar_tiempo_inicio_cuarto

        if 'Equipos' in self.grupos_permitidos:
            participante_cuarto_reto = EquipoRetoCuarto.objects.get(detalle=detalle, cuarto=cuarto)
        else:
            participante_cuarto_reto = JovenRetoCuarto.objects.get(detalle=detalle, cuarto=cuarto)
        
        if participante_cuarto_reto.fecha_finalizacion == None:            
            fecha_inicio_cuarto = retornar_tiempo_inicio_cuarto(self, cuarto, detalle)
            participante_cuarto_reto.fecha_finalizacion = timezone.now()
            participante_cuarto_reto.tiempo_transcurrido = (timezone.now()-fecha_inicio_cuarto).seconds*1000
            participante_cuarto_reto.save()


    def agregar_joven_detalle(self, joven):
        self.jovenes.add(joven)

    def eliminar_joven_detalle(self, joven):
        self.jovenes.remove(joven)

    def agregar_jurado(self, jurado):
        self.jurados.add(jurado)

    def eliminar_jurado(self, jurado):
        self.jurados.delete(jurado)

    def agregar_equipo(self, equipo):
        self.equipos.add(equipo)

    def obtener_equipos_miembros(self):
        return self.equipos.all()

    def obtener_jovenes_miembros(self):
        return self.jovenes.all()

    def joven_esta_inscrito(self, joven_comprobar):
        return (joven_comprobar in self.obtener_jovenes_miembros())

    def equipo_esta_inscrito(self, equipo_comprobar):
        return (equipo_comprobar in self.obtener_equipos_miembros())

    def lider_equipo_esta_inscrito(self, joven_lider_comprobar):
        try:
            equipos = joven_lider_comprobar.equipos_del_lider.all()
            estado = False
            for equipo in equipos:
                if estado == False:
                    if (equipo in self.obtener_equipos_miembros()) == True:
                        estado = True
            return estado
        except SolicitudesRetosEquipos.DoesNotExist:
            return False
        return True

    def crear_cuarto_descanso(self):
        """
            Crea el cuarto descanso de forma automatica cuando se crea un reto
        """
        from kairos.cuartos.models import Cuarto

        cuarto_descanso = Cuarto.objects.create(titulo='Cuarto descanso', indicacion="Cuarto de descanso", peso=0, dificultad=0, activo=True, es_descanso=True)
        CuartoReto.crear_cuarto_reto(cuarto_descanso, self, 0)
        self.save()

 
    def obtener_lista_ordenes_cuartos(self:'Reto') -> 'List[int]':
        """
        Retorna una lista con los ordenes que tienen los cuartos del reto

        Parametros:
            self (Reto): Reto donde se realizara la busqueda

        Returns:
            List[int]: Lista de ordenes
        """
        
        return list(self.cuartos.order_by('-cuarto_reto_cuarto__orden').values_list('cuarto_reto_cuarto__orden', flat=True))
        

@receiver(post_save, sender=Retos, dispatch_uid="minificar_imagen_retos")
def comprimir_imagen_retos(sender, **kwargs):
    from kairos.core.utils import comprimir_imagen
    if kwargs["instance"].imagen:
        comprimir_imagen(kwargs["instance"].imagen.path)
    if kwargs["instance"].logo:
        comprimir_imagen(kwargs["instance"].logo.path)

class SolicitudReto(models.Model):
    GRUPOS = (
        ("Jóvenes", "Jóvenes"),
        ("Equipos", "Equipos")
        )
    organizacion = models.ForeignKey(Organizacion, on_delete=models.CASCADE, related_name='organizacion_solicitud_reto')
    nombre_responsable = models.CharField(max_length=50, verbose_name="*Nombre del responsable", null=False, blank=False)
    correo = models.CharField(max_length=100, verbose_name="*Email", null=False, blank=False)
    telefono_regex = RegexValidator(regex=r'^\d{7,15}$',
                                    message="El número de celular debe tener entre 7 y 15 dígitos.")
    telefono = models.CharField(max_length=15, validators=[telefono_regex], verbose_name='Celular', null=True, blank=True)
    ciudad = models.ForeignKey(City, on_delete=models.CASCADE, related_name='ciudad_de_solictud_reto', verbose_name="Ciudad*")
    grupos_permitidos = ArrayField(models.CharField(max_length=50, blank=False, null=False), size=3,
                                    choices=GRUPOS,
                                    verbose_name='*¿Quiénes pueden inscribirse al reto?', null=False, blank=False)
    fecha_creacion = models.DateTimeField(null=False, blank=False, auto_now_add=True)

    mensaje = models.TextField(verbose_name="*Mensaje", null=True, blank=False)

    history = HistoricalRecords()


class CalificacionRetos(models.Model):
    reto = models.ForeignKey(Retos, on_delete=models.CASCADE, verbose_name='reto calificado',
                                  related_name='calificacion_reto_del_joven')
    usuario = models.ForeignKey('usuarios.Usuario', on_delete=models.CASCADE, verbose_name='usuario que califica',
                                related_name='usuario_calificador_reto')
    calificacion = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                               verbose_name='calificación: 1 me gusta, 0 no me gusta')
    history = HistoricalRecords()

class SolicitudesRetosEquipos(models.Model):
    equipo = models.ForeignKey(Equipo, related_name='solicitudes_de_reto_equipo', on_delete=models.CASCADE)
    reto = models.ForeignKey(Retos, related_name='solicitudes_retos_equipo', on_delete=models.CASCADE)
    estado = models.CharField(max_length=15, verbose_name='estado')
    fecha_creacion = models.DateTimeField(null=False, blank=False, auto_now_add=True)
    history = HistoricalRecords()

    @staticmethod
    def existe_solitud(reto, equipo):
        try:
            solicitud = SolicitudesRetosEquipos.objects.get(reto=reto, equipo=equipo)
            return True
        except SolicitudesRetosEquipos.DoesNotExist:
            return False
    @staticmethod
    def existe_solitud_lider(reto, joven):
        try:
            equipos = joven.equipos_del_lider.all()
            existe = False
            for equipo in equipos:
                if existe == False:
                    solicitud = SolicitudesRetosEquipos.existe_solitud(reto=reto, equipo=equipo)
                    if solicitud == True:
                        existe = True
            return existe
        except SolicitudesRetosEquipos.DoesNotExist:
            return False
        return True
    @staticmethod
    def es_solicitud_enviada(reto, equipo):
        try:
            solicitud = SolicitudesRetosEquipos.objects.get(reto=reto, equipo=equipo)
            if solicitud.estado == 'Invitado':
                return True
            else:
                return False
        except SolicitudesRetosEquipos.DoesNotExist:
            return False
        return True

    @staticmethod
    def es_solicitud_enviada_lider(reto, joven):
        try:
            equipos = joven.equipos_del_lider.all()
            estado = False
            for equipo in equipos:
                if estado == False:
                    solicitud = SolicitudesRetosEquipos.objects.get(reto=reto, equipo=equipo)
                    if solicitud.estado == 'Postulado':
                        estado = True
            return estado
        except SolicitudesRetosEquipos.DoesNotExist:
            return False
        return True

class SolicitudesRetosJovenes(models.Model):
    joven = models.ForeignKey(Joven, related_name='solicitudes_de_reto_joven', on_delete=models.CASCADE)
    reto = models.ForeignKey(Retos, related_name='solicitudes_retos_joven', on_delete=models.CASCADE)
    estado = models.CharField(max_length=15, verbose_name='estado')
    fecha_creacion = models.DateTimeField(null=False, blank=False, auto_now_add=True)
    history = HistoricalRecords()

    @staticmethod
    def existe_solitud(reto, joven):
        try:
            solicitud = SolicitudesRetosJovenes.objects.get(reto=reto, joven=joven)
        except SolicitudesRetosJovenes.DoesNotExist:
            return False
        return True

    @staticmethod
    def es_solicitud_enviada(reto, joven):
        try:
            solicitud = SolicitudesRetosJovenes.objects.get(reto=reto, joven=joven)
            if solicitud.estado == 'Invitado':
                return True
            else:
                return False
        except SolicitudesRetosJovenes.DoesNotExist:
            return False
        return True


class DetalleEquipoReto(models.Model):
    jurado = models.ForeignKey(Jurado, related_name='equipo_reto_evaluado', on_delete=models.CASCADE, null=True)
    equipo = models.ForeignKey(Equipo, related_name='equipo_detalle_reto', on_delete=models.CASCADE)
    reto = models.ForeignKey(Retos, related_name='detalle_equipo', on_delete=models.CASCADE)
    cuartos = models.ManyToManyField(Cuarto, through='EquipoRetoCuarto', related_name="detalle_equipo_reto_cuartos", blank=True, verbose_name="cuartos del joven")
    retroalimentacion = models.OneToOneField(RetroalimentacionJuradoEquipo, on_delete=models.CASCADE, null=True)
    fecha_inicio = models.DateTimeField(null=True, blank=False)
    fecha_finalizacion = models.DateTimeField(null=True, blank=False)
    calificacion_final = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(10), MinValueValidator(0)],
                                               verbose_name='Calificación final', null=True, blank=False)
    aceptado = models.BooleanField(verbose_name="¿El equipo fue aceptado?", null=True, blank=False)
    completado = models.BooleanField(verbose_name="¿El equipo completo el reto?", default=False)
    calificado = models.BooleanField(verbose_name="¿Ha sido calificado el Equipo?", default=False)
    abandonado = models.BooleanField(verbose_name="¿El equipo abandono el reto?", default=False)
    miembros_activos = JSONField(null=True)  # {id_miembro: true/false, id_miembro: true/false}
    notificado = models.BooleanField(verbose_name="¿Se ha notificado por correo de la puntuación?", default=False)
    history = HistoricalRecords()

    @staticmethod
    def buscar(id_equipo:int, id_reto:int) -> 'DetalleEquipoReto':
        """
        Retorna el DetalleEquipo usando el id_equipo y id_reto

        Parametros:
            id_equipo(int): id del equipo
            id_reto(int): id del reto

        Retorno:
            DetalleEquipoReto: Detalle encontrado
        """
        try:
            detalle = DetalleEquipoReto.objects.get(equipo__pk=id_equipo, reto__pk=id_reto)
            return detalle
        except DetalleEquipoReto.DoesNotExist:
            return None
    
    @staticmethod
    def buscar_varios(ids_equipo:'list[int]', id_reto:int) -> 'Queryset(DetalleEquipoReto)':
        """
        Retorna el DetalleEquipo usando los ids_equipo y id_reto

        Parametros:
            ids_equipo(list[int]): lista ids de los equipos
            id_reto(int): id del reto

        Returns:
            Queryset(DetalleEquipoReto): Detalles encontrados        
        """

       
        detalles = DetalleEquipoReto.objects.filter(equipo__pk__in=ids_equipo, reto__pk=id_reto)
        return detalles
       
    def esta_reto_completado(self) -> bool:
        """
        Determina si un reto fue completado tomadno encuenta la cantidad de cuartos que quedaron por reponder

        Retorno:
            bool: True si no hay cuartos por responder, False si quedaron cuartos por responder
        """
        cuartos = Cuarto.obtener_siguiente_cuarto_responder(self.reto ,self)
        if cuartos:
            return False
        return True

    @staticmethod
    def existe_detalle(equipo:'Equipo', reto:'Reto') -> bool:
        """
        Determina si existe un DetalleEquipoReto

        Parametros:
            equipo (Equipo): Equipo que sea usado para la busqueda
            reto (Reto): Reto que sea usado para la busqueda

        Retorno:
            bool: True si Existe, False si NO existe
        """
        try:
            detalle = DetalleEquipoReto.objects.get(equipo=equipo, reto=reto)
        except DetalleEquipoReto.DoesNotExist:
            return False
        return True

    @staticmethod
    def crear_detalle(equipo:'Equipo', reto:'Reto') -> None:
        """
        Crea el DetalleEquipoReto usando el equipo y el reto recibidos como parametro

        Parametros:
            equipo (Equipo): Equipo que sea usado para crear el Detalle
            reto (Reto): Reto que sea usado para crear el Detalle
        """
        DetalleEquipoReto.objects.create(equipo=equipo, reto=reto)

    @staticmethod
    def asignar_miembros(equipo, reto):
        try:
            dic_miembros = {}
            miembros = equipo.miembros.all()
            for miembro in miembros:
                dic_miembros[miembro.id] = False
            detalle = DetalleEquipoReto.objects.get(equipo=equipo, reto=reto)
            detalle.miembros_activos = dic_miembros
            detalle.save()
        except DetalleEquipoReto.DoesNotExist:
            pass


class DetalleJovenReto(models.Model):
    jurado = models.ForeignKey(Jurado, related_name='joven_reto_evaluado', on_delete=models.CASCADE, null=True)
    joven = models.ForeignKey(Joven, related_name='joven_detalle_reto', on_delete=models.CASCADE)
    reto = models.ForeignKey(Retos, related_name='detalle_joven', on_delete=models.CASCADE)
    cuartos = models.ManyToManyField(Cuarto, through='JovenRetoCuarto', related_name="detalle_joven_reto_cuartos", blank=True, verbose_name="cuartos del joven")
    retroalimentacion = models.OneToOneField(RetroalimentacionJuradoJoven, on_delete=models.CASCADE, null=True)
    fecha_inicio = models.DateTimeField(null=True, blank=False)
    fecha_finalizacion = models.DateTimeField(null=True, blank=False)
    calificacion_final = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(10), MinValueValidator(0)],
                                               verbose_name='Calificación final', null=True, blank=False)
    aceptado = models.BooleanField(verbose_name="¿El joven fue aceptado?", null=True, blank=False)
    completado = models.BooleanField(verbose_name="¿El joven completo el reto?", default=False)
    calificado = models.BooleanField(verbose_name="¿Ha sido calificado el Equipo?", default=False)
    abandonado = models.BooleanField(verbose_name="¿El joven abandono el reto?", default=False)
    notificado = models.BooleanField(verbose_name="¿Se ha notificado por correo de la puntuación?", default=False)
    history = HistoricalRecords()

    @staticmethod
    def buscar(id_joven: int, id_reto: int) -> 'DetalleJovenReto':
        """
            Retorna el DetalleEquipo usando el id_joven y id_reto

            Parametros:
                id_joven(int): id del joven
                id_reto(int): id del reto

            Retorno:
                DetalleJovenReto: DetalleJovenReto si existe, None si NO existe        
        """
        
        try:
            detalle = DetalleJovenReto.objects.get(joven__pk=id_joven, reto__pk=id_reto)
            return detalle
        except DetalleJovenReto.DoesNotExist:
            return None

    def buscar_varios(ids_joven:'list[int]', id_reto:int) -> 'Queryset(DetalleJovenReto)':
        """
            Retorna varios DetalleEquipo usando los ids_joven y id_reto

            Parametros:
                id_joven(list[int]): lista ids de los jovenes
                id_reto(int): id del reto

            Retorno:
                Queryset(DetalleJovenReto): Detalles encontrados        
        """
       
        detalles = DetalleJovenReto.objects.filter(joven__pk__in=ids_joven, reto__pk=id_reto)
        return detalles
    
    def esta_reto_completado(self) -> bool:
        """
            Determina si un reto fue completado tomadno encuenta la cantidad de cuartos que quedaron por reponder.

            Retorno:
                bool: True si no hay cuartos por responder, False si quedaron cuartos por responder
        """
        cuartos = Cuarto.obtener_siguiente_cuarto_responder(self.reto ,self)
        if cuartos:
            return False
        return True

    @staticmethod
    def existe_detalle(joven:'Joven', reto:'Reto') -> bool:
        """
            Determina si existe un DetalleJovenReto usadon a un joven y un retos como parametros de busqueda.

            Parametros:
                joven (Joven): Joven que se usara para realizar la busqueda
                reto (Reto): Reto que se usara para realizar la busqueda

            Retorno:
                bool: True si existe, False si NO existe
        """
        try:
            detalle = DetalleJovenReto.objects.get(joven=joven, reto=reto)
        except DetalleJovenReto.DoesNotExist:
            return False
        return True

    @staticmethod
    def crear_detalle(joven:'Joven', reto:'Reto') -> None:
        """
            Se encarga de crear un detalle usando un joven y un reto como parametros

            Parametros:
                joven (Joven): Joven que se usara para crear el DetalleJovenReto
                reto (Reto): Reto que se usara para crear el DetalleJovenReto
        """

        DetalleJovenReto.objects.create(joven=joven, reto=reto)

class JovenRetoCuarto(models.Model):
    cuarto = models.ForeignKey(Cuarto, related_name='joven_reto_cuarto_cuarto', on_delete=models.CASCADE)
    detalle = models.ForeignKey(DetalleJovenReto, related_name='joven_reto_cuarto_detalle', on_delete=models.CASCADE)
    fecha_inicio = models.DateTimeField(null=True, blank=False)
    fecha_finalizacion = models.DateTimeField(null=True, blank=False)
    tiempo_transcurrido = models.FloatField(default=0.0, verbose_name='Tiempo total transcurrido', null=True, blank=True)
    completado = models.BooleanField(verbose_name="¿El cuarto ha sido terminado?", default=False)

    @staticmethod
    def buscar(cuarto:'Cuarto', detalle:'DetalleJovenReto') -> 'JovenRetoCuarto':
        """
            Determina si existe un JovenRetoCuarto utilizando un cuarto y un detalle recibido como parametros.

            Parametros:
                cuarto (Cuarto): Joven que se usara para para la busqueda
                detalle (DetalleJovenReto): Detalle que se usara para la busqueda

            Retorno:
                JovenRetoCuarto: JovenRetoCuarto si existe, None si NO existe
        """
        try:
            reto_cuarto = JovenRetoCuarto.objects.get(detalle=detalle, cuarto=cuarto)
            return reto_cuarto
        except JovenRetoCuarto.DoesNotExist:
            return None

    @staticmethod
    def buscar_varios(detalle:'DetalleJovenReto', lista_cuartos:'list[Cuarto]') -> 'Queryset(JovenRetoCuarto)':
        """
            Retorna varios JovenRetoCuarto usando el detalle y una lista de cuartos recibidas como parametros

            Parametros:
                detalle(DetalleJovenReto): Detalle que se usara para la busqueda
                lista_cuartos(list[Cuarto]): lista de los cuartos que se usaran para busqueda

            Retorno:
                Queryset(JovenRetoCuarto): Queryset(JovenRetoCuarto) si existe, None si NO existe       
        """

        try:
            retos_cuartos = JovenRetoCuarto.objects.filter(detalle=detalle, cuarto__in=lista_cuartos)
            return retos_cuartos
        except JovenRetoCuarto.DoesNotExist:
            return None

    @staticmethod
    def obtener_cuartos_completados(detalle:'DetalleJovenReto', lista_cuartos:'list[Cuarto]') -> 'Queryset(JovenRetoCuarto)':
        """
            Retorna varios JovenRetoCuarto que esten completados usando el detalle y una lista de cuartos recibidas como parametros

            Parametros:
                detalle(DetalleJovenReto): Detalle que se usara para la busqueda
                lista_cuartos(list[Cuarto]): lista de los cuartos que se usaran para busqueda

            Retorno:
                Queryset(JovenRetoCuarto): Queryset(JovenRetoCuarto) si existe, None si NO existe       
        """

        return JovenRetoCuarto.objects.filter(detalle=detalle, cuarto__in=lista_cuartos, completado=True)

    @staticmethod
    def existe(cuarto: 'Cuarto', detalle: 'DetalleJovenReto') -> bool:
        """
            Determina si un JovenRetoCuarto existe usando un cuarto y un detalle como parametros de busqueda.

            Parametros:
                cuarto(Cuarto): cuarto que se usara para la busqueda
                detalle(DetalleJovenReto): Detalle que se usaran para busqueda
            
            Retorno:
                JovenRetoCuarto: True si existe, False si NO existe
        """

        try:
            cuarto_reto = JovenRetoCuarto.objects.get(cuarto=cuarto, detalle=detalle)
            return True
        except JovenRetoCuarto.DoesNotExist:
            return False


    @staticmethod
    def crear(cuarto: 'Cuarto', detalle: 'DetalleJovenReto') -> None:
        """
            Crea un JovenRetoCuarto si NO existe, usando un cuarto y un detalle.

            Parametros:
                cuarto(Cuarto): cuarto que se usara para la creación
                detalle(DetalleJovenReto): Detalle que se usaran para la creación           
        """

        if JovenRetoCuarto.existe(cuarto, detalle) == False:
            JovenRetoCuarto.objects.create(cuarto=cuarto, detalle=detalle)

class EquipoRetoCuarto(models.Model):
    cuarto = models.ForeignKey(Cuarto, related_name='equipo_reto_cuarto_cuarto', on_delete=models.CASCADE)
    detalle = models.ForeignKey(DetalleEquipoReto, related_name='equipo_reto_cuarto_detalle', on_delete=models.CASCADE)
    fecha_inicio = models.DateTimeField(null=True, blank=False)
    fecha_finalizacion = models.DateTimeField(null=True, blank=False)
    tiempo_transcurrido = models.FloatField(default=0.0, verbose_name='Tiempo total transcurrido', null=True, blank=True)
    completado = models.BooleanField(verbose_name="¿El cuarto ha sido terminado?", default=False)

    @staticmethod
    def buscar(cuarto: 'Cuarto', detalle: 'DetalleJovenReto') -> 'EquipoRetoCuarto':
        """
            Determina si un EquipoRetoCuarto existe usando un cuarto y un detalle como parametros de busqueda.

            Parametros:
                cuarto(Cuarto): cuarto que se usara para la busqueda
                detalle(DetalleJovenReto): Detalle que se usaran para busqueda
            
            Retorno:
                EquipoRetoCuarto: True si existe, False si NO existe
        """
        try:
            reto_cuarto = EquipoRetoCuarto.objects.get(detalle=detalle, cuarto=cuarto)
            return reto_cuarto
        except EquipoRetoCuarto.DoesNotExist:
            return None
    
    @staticmethod
    def buscar_varios(detalle: 'DetalleEquipoReto', lista_cuartos: 'list[Cuartos]') -> 'Queryset(EquipoRetoCuarto)':
        """
            Retorna varios EquipoRetoCuarto usando el detalle y una lista de cuartos recibidas como parametros

            Parametros:
                detalle(DetalleEquipoReto): Detalle que se usara para la busqueda
                lista_cuartos(list[Cuartos]): lista de los cuartos que se usaran para busqueda

            Retorno:
                Queryset(EquipoRetoCuarto): Queryset(EquipoRetoCuarto) si existe, None si NO existe       
        """
        try:
            retos_cuartos = EquipoRetoCuarto.objects.filter(detalle=detalle, cuarto__in=lista_cuartos)
            return retos_cuartos
        except EquipoRetoCuarto.DoesNotExist:
            return None

    @staticmethod
    def obtener_cuartos_completados(detalle: 'DetalleEquipoReto', lista_cuartos: 'list[Cuarto]') -> 'Queryset(EquipoRetoCuarto)':
        """
            Retorna varios EquipoRetoCuarto que esten completados usando el detalle y una lista de cuartos recibidas como parametros

            Parametros:
                detalle(DetalleJovenReto): Detalle que se usara para la busqueda
                lista_cuartos(list[Cuarto]): lista de los cuartos que se usaran para busqueda

            Retorno:
                Queryset(EquipoRetoCuarto): Queryset(EquipoRetoCuarto) si existe, None si NO existe       
        """
        return EquipoRetoCuarto.objects.filter(detalle=detalle, cuarto__in=lista_cuartos, completado=True)

    @staticmethod
    def existe(cuarto: 'Cuarto', detalle: 'DetalleEquipoReto') -> bool:
        """
            Determina si un EquipoRetoCuarto existe usando un cuarto y un detalle como parametros de busqueda.

            Parametros:
                cuarto(Cuarto): cuarto que se usara para la busqueda
                detalle(DetalleEquipoReto): Detalle que se usaran para busqueda
            
            Retorno:
                EquipoRetoCuarto: True si existe, False si NO existe
        """

        try:
            cuarto_reto = EquipoRetoCuarto.objects.get(cuarto=cuarto, detalle=detalle)
        except EquipoRetoCuarto.DoesNotExist:
            return False
        return True

    @staticmethod
    def crear(cuarto: 'Cuarto', detalle: 'DetalleEquipoReto') -> None:
        """
            Crea un EquipoRetoCuarto si NO existe, usando un cuarto y un detalle.

            Parametros:
                cuarto(Cuarto): cuarto que se usara para la creación
                detalle(DetalleEquipoReto): Detalle que se usaran para la creación   
        """

        if EquipoRetoCuarto.existe(cuarto, detalle) == False:
            EquipoRetoCuarto.objects.create(cuarto=cuarto, detalle=detalle)

class CuartoReto(models.Model):
    cuarto = models.ForeignKey(Cuarto, related_name='cuarto_reto_cuarto', on_delete=models.PROTECT)
    reto = models.ForeignKey(Retos, related_name='cuarto_reto_reto', on_delete=models.PROTECT)
    orden = models.PositiveIntegerField(verbose_name="Orden que tendra el Cuarto en el Reto", validators=[MinValueValidator(1)], null=False, blank=False)
    history = HistoricalRecords()

    @staticmethod
    def buscar(cuarto: 'Cuarto', reto: 'Reto') -> 'CuartoReto':
        """
            Determina si un CuartoReto existe usando un cuarto y un reto como parametros de busqueda.

            Parametros:
                cuarto(Cuarto): Cuarto que se usara para la busqueda
                reto(Reto): Reto que se usaran para busqueda
            
            Retorno:
                CuartoReto: CuartoReto si existe, None si NO existe
        """
        try:
            cuarto_reto = CuartoReto.objects.get(cuarto=cuarto, reto=reto)
            return cuarto_reto
        except CuartoReto.DoesNotExist:
            return None

    @staticmethod
    def existe_cuarto(cuarto: 'Cuarto', reto: 'Reto') -> bool:
        """
            Determina si un CuartoReto existe usando un cuarto y un reto como parametros de busqueda.

            Parametros:
                cuarto(Cuarto): Cuarto que se usara para la busqueda
                reto(Reto): Reto que se usaran para busqueda
            
            Retorno:
                bool: True si existe, False si NO existe
        """
        try:
            cuarto_reto = CuartoReto.objects.get(cuarto=cuarto, reto=reto)
        except CuartoReto.DoesNotExist:
            return False
        return True

    @staticmethod
    def crear_cuarto_reto(cuarto: 'Cuarto', reto: 'Reto', orden: int) -> None:
        """
            Crea un CuartoReto utilizando un cuarto, un reto y el orden que tendra en rel reto.

            Parametros:
                cuarto(Cuarto): Cuarto que se usara para la creación
                reto(Reto): Reto que se usaran para la creación
                orden(int): número que posiciona el cuarto en un Reto                       
        """
        CuartoReto.objects.create(cuarto=cuarto, reto=reto, orden=orden)

def subir_archivo(instance, filename):
    return "archivos_actos_calificados/%s"%(filename.encode('ascii', 'ignore'))

class CalificacionActoJoven(models.Model):
    detalle_joven_reto = models.ForeignKey(DetalleJovenReto, related_name='calificacion_acto_joven_reto', on_delete=models.CASCADE)
    acto = models.ForeignKey(Acto, related_name='calificacion_acto_joven_acto', on_delete=models.CASCADE)
    puntaje = models.PositiveIntegerField(verbose_name="Puntaje del joven en el acto", validators=[MaxValueValidator(10), MinValueValidator(0)], default=0)
    recalificado = models.BooleanField(verbose_name="¿El joven ha sido recalificado?", default=False)
    archivo_subido = models.FileField(upload_to=subir_archivo, verbose_name="Archivo para ser calificado", null=True, blank=True)
    history = HistoricalRecords()

    @staticmethod
    def buscar(detalle: 'DetalleJovenReto', acto: 'Acto') -> 'CalificacionActoJoven':
        """
            Determina si una CalificacionActoJoven existe usando un detalle y un acto como parametros de busqueda.

            Parametros:
                detalle(DetalleJovenReto): detalle que se usara para la busqueda
                acto(Acto): Acto que se usaran para busqueda
            
            Retorno:
                CalificacionActoJoven: CalificacionActoJoven si existe, None si NO existe
        """
        try:
            cuarto = CalificacionActoJoven.objects.get(detalle_joven_reto=detalle, acto=acto)
            return cuarto
        except CalificacionActoJoven.DoesNotExist:
            return None
    
    @staticmethod
    def buscar_varios(detalle: 'DetalleJovenReto', actos: 'list[Actos]') -> 'Queryset(CalificacionActoJoven)':
        """
            Retorna varios CalificacionActoJoven usando el detalle y una lista de actos recibidas como parametros

            Parametros:
                detalle(DetalleJovenReto): Detalle que se usara para la busqueda
                lista_cuartos(list[Actos]): lista de los actos que se usaran para busqueda

            Retorno:
                Queryset(CalificacionActoJoven): Queryset(CalificacionActoJoven) si existe, None si NO existe       
        """
        return CalificacionActoJoven.objects.filter(detalle_joven_reto=detalle, acto__in=actos)            

    @staticmethod
    def existe_calificacion(detalle, acto):
        try:
            cuarto_reto = CalificacionActoJoven.objects.get(detalle_joven_reto=detalle, acto=acto)
        except CalificacionActoJoven.DoesNotExist:
            return False
        return True

    @staticmethod
    def crear_calificacion(detalle, acto):
        if CalificacionActoJoven.existe_calificacion(detalle, acto) == False:
            CalificacionActoJoven.objects.create(detalle_joven_reto=detalle, acto=acto)

class CalificacionActoEquipo(models.Model):
    detalle_equipo_reto = models.ForeignKey(DetalleEquipoReto, related_name='calificacion_acto_equipo_reto', on_delete=models.CASCADE)
    acto = models.ForeignKey(Acto, related_name='calificacion_acto_equipo_acto', on_delete=models.CASCADE)
    puntaje = models.PositiveIntegerField(verbose_name="Puntaje del equipo en el acto", validators=[MaxValueValidator(10), MinValueValidator(0)], default=0)
    recalificado = models.BooleanField(verbose_name="¿El equipo ha sido recalificado?", default=False)
    archivo_subido = models.FileField(upload_to=subir_archivo, verbose_name="Archivo para ser calificado", null=True, blank=True)
    history = HistoricalRecords()

    @staticmethod
    def buscar(detalle: 'DetalleEquipoReto', acto :'Acto') -> 'CalificacionActoEquipo':
        """
            Determina si una DetalleEquipoReto existe usando un detalle y un acto como parametros de busqueda.

            Parametros:
                detalle(DetalleEquipoReto): detalle que se usara para la busqueda
                acto(Acto): Acto que se usaran para busqueda
            
            Retorno:
                CalificacionActoEquipo: CalificacionActoEquipo si existe, None si NO existe
        """
        try:
            cuarto = CalificacionActoEquipo.objects.get(detalle_equipo_reto=detalle, acto=acto)
            return cuarto
        except CalificacionActoEquipo.DoesNotExist:
            return None

    @staticmethod
    def buscar_varios(detalle: 'DetalleEquipoReto', actos: 'list[Actos]') -> 'Queryset(CalificacionActoEquipo)':
        """
            Retorna varios CalificacionActoEquipo usando el detalle y una lista de actos recibidas como parametros

            Parametros:
                detalle(DetalleEquipoReto): Detalle que se usara para la busqueda
                lista_cuartos(list[Actos]): lista de los actos que se usaran para busqueda

            Retorno:
                Queryset(CalificacionActoEquipo): Queryset(CalificacionActoEquipo) si existe, None si NO existe       
        """
        return CalificacionActoEquipo.objects.filter(detalle_equipo_reto=detalle, acto__in=actos)

    @staticmethod
    def existe_calificacion(detalle_equipo_reto: 'DetalleEquipoReto', acto: 'Acto') -> bool:
        """
            Determina si una CalificacionActoEquipo existe usando un detalle y un acto como parametros de busqueda.

            Parametros:
                detalle(DetalleEquipoReto): detalle que se usara para la busqueda
                acto(Acto): Acto que se usaran para busqueda
            
            Retorno:
                bool: True si existe, False si NO existe
        """
        try:
            cuarto_reto = CalificacionActoEquipo.objects.get(detalle_equipo_reto=detalle_equipo_reto, acto=acto)
        except CalificacionActoEquipo.DoesNotExist:
            return False
        return True

    @staticmethod
    def crear_calificacion(detalle_equipo_reto, acto) -> None:
        """
            Crea una CalificacionActoEquipo si NO existe, usando un detalle y un acto.

            Parametros:
                detalle(DetalleEquipoReto): detalle que se usara para la creación
                acto(Acto): Acto que se usaran para la creación            
        """
        if CalificacionActoEquipo.existe_calificacion(detalle_equipo_reto, acto) == False:
            CalificacionActoEquipo.objects.create(detalle_equipo_reto=detalle_equipo_reto, acto=acto)


class CalificacionPreguntaJoven(models.Model):
    detalle_joven_reto = models.ForeignKey(DetalleJovenReto, related_name='calificacion_pregunta_joven_reto', on_delete=models.CASCADE)
    pregunta = models.ForeignKey(Pregunta, related_name='calificacion_pregunta_joven_pregunta', on_delete=models.CASCADE)
    opcion = models.ForeignKey('cuartos.Opcion', related_name='opcion_joven_pregunta', on_delete=models.CASCADE, null=True, blank=True)
    puntaje = models.PositiveIntegerField(verbose_name="Puntaje del joven en la pregunta", validators=[MaxValueValidator(10), MinValueValidator(0)], default=0)
    fecha_inicio = models.DateTimeField(null=True, blank=False)
    fecha_finalizacion = models.DateTimeField(null=True, blank=False)
    envio_pregunta = models.BooleanField(verbose_name="¿El equipo ha respondido antes de que se acabe el tiempo?", default=False)
    history = HistoricalRecords()

    @staticmethod
    def buscar(detalle, pregunta):
        try:
            cuarto = CalificacionPreguntaJoven.objects.get(detalle_joven_reto=detalle, pregunta=pregunta)
            return cuarto
        except CalificacionPreguntaJoven.DoesNotExist:
            return None

    @staticmethod
    def buscar_varios(detalle: 'DetalleJovenReto', preguntas: 'list[Pregunta]') -> 'Queryset(CalificacionPreguntaJoven)':
        """
            Retorna varios CalificacionPreguntaJoven usando el detalle y una lista de actos recibidas como parametros

            Parametros:
                detalle(DetalleJovenReto): Detalle que se usara para la busqueda
                lista_cuartos(list[Pregunta]): lista de las preguntas que se usaran para busqueda

            Retorno:
                Queryset(CalificacionPreguntaJoven): Queryset(CalificacionPreguntaJoven) si existe, None si NO existe       
        """
        return CalificacionPreguntaJoven.objects.filter(detalle_joven_reto=detalle, pregunta__in=preguntas)

    @staticmethod
    def existe_calificacion(detalle_joven_reto, pregunta):
        try:
            cuarto_reto = CalificacionPreguntaJoven.objects.get(detalle_joven_reto=detalle_joven_reto, pregunta=pregunta)
        except CalificacionPreguntaJoven.DoesNotExist:
            return False
        return True

    @staticmethod
    def crear_calificacion(detalle_joven_reto, pregunta):
        if CalificacionPreguntaJoven.existe_calificacion(detalle_joven_reto, pregunta) == False:
            CalificacionPreguntaJoven.objects.create(detalle_joven_reto=detalle_joven_reto, pregunta=pregunta)

class CalificacionPreguntaEquipo(models.Model):
    detalle_equipo_reto = models.ForeignKey(DetalleEquipoReto, related_name='calificacion_pregunta_equipo_reto', on_delete=models.PROTECT)
    pregunta = models.ForeignKey(Pregunta, related_name='calificacion_pregunta_equipo_pregunta', on_delete=models.PROTECT)
    opcion = models.ForeignKey('cuartos.Opcion', related_name='opcion_equipo_pregunta', on_delete=models.PROTECT, null=True, blank=True)
    puntaje = models.PositiveIntegerField(verbose_name="Puntaje del equipo en la pregunta", validators=[MaxValueValidator(10), MinValueValidator(0)], default=0)
    fecha_inicio = models.DateTimeField(null=True, blank=False)
    fecha_finalizacion = models.DateTimeField(null=True, blank=False)
    envio_pregunta = models.BooleanField(verbose_name="¿El equipo ha respondido antes de que se acabe el tiempo?", default=False)
    history = HistoricalRecords()

    @staticmethod
    def buscar(detalle, pregunta):
        try:
            cuarto = CalificacionPreguntaEquipo.objects.get(detalle_equipo_reto=detalle, pregunta=pregunta)
            return cuarto
        except CalificacionPreguntaEquipo.DoesNotExist:
            return None

    @staticmethod
    def buscar_varios(detalle:'DetalleEquipoReto', preguntas:'list[Pregunta]') -> 'Queryset(CalificacionPreguntaEquipo)':
        """
            Retorna varios CalificacionPreguntaEquipo usando el detalle y una lista de actos recibidas como parametros

            Parametros:
                detalle(DetalleEquipoReto): Detalle que se usara para la busqueda
                lista_cuartos(list[Pregunta]): lista de las preguntas que se usaran para busqueda

            Retorno:
                Queryset(CalificacionPreguntaEquipo): Queryset(CalificacionPreguntaEquipo) si existe, None si NO existe       
        """
        return CalificacionPreguntaEquipo.objects.filter(detalle_equipo_reto=detalle, pregunta__in=preguntas)

    @staticmethod
    def existe_calificacion(detalle_equipo_reto, pregunta):
        try:
            cuarto_reto = CalificacionPreguntaEquipo.objects.get(detalle_equipo_reto=detalle_equipo_reto, pregunta=pregunta)
        except CalificacionPreguntaEquipo.DoesNotExist:
            return False
        return True

    @staticmethod
    def crear_calificacion(detalle_equipo_reto, pregunta):
        if CalificacionPreguntaEquipo.existe_calificacion(detalle_equipo_reto, pregunta) == False:
            CalificacionPreguntaEquipo.objects.create(detalle_equipo_reto=detalle_equipo_reto, pregunta=pregunta)

class Competencia(models.Model):
    GRUPOS = (
        ("fas fa-bolt", "fas fa-bolt"),
        ("fas fa-camera", "fas fa-camera"),
        ("far fa-eye", "far fa-eye"),
        ("fas fa-portrait", "fas fa-portrait"),
        ("fas fa-clipboard-check", "fas fa-clipboard-check"),
        ("fas fa-truck", "fas fa-truck"),
        ("far fa-heart", "far fa-heart"),
        ("fas fa-lightbulb", "fas fa-lightbulb"),
        ("fas fa-trophy", "fas fa-trophy"),
        ("fas fa-wrench", "fas fa-wrench"),
        ("fas fa-bullhorn", "fas fa-bullhorn"),
        ("fas fa-calculator", "fas fa-calculator"),
        ("fas fa-first-aid", "fas fa-first-aid"),
        ("fas fa-guitar", "fas fa-guitar"),
        ("fas fa-balance-scale", "fas fa-balance-scale"),
        ("fas fa-cogs", "fas fa-cogs"),
        ("fas fa-cog", "fas fa-cog"),
        ("fas fa-futbol", "fas fa-futbol"),
        ("fas fa-gem", "fas fa-gem"),
        ("fas fa-search", "fas fa-search"),
        ("fas fa-tools", "fas fa-tools"),
    )
    nombre = models.CharField(max_length=25, verbose_name='Nombre*')
    icono = models.CharField(max_length=50, verbose_name='Icono', null=True, blank=True)
    estado = models.BooleanField(verbose_name="¿La competencia esta activa?", default=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.nombre)
    
class CompetenciaCalificaciones(models.Model):
    calificaciones = JSONField(null=True) #{'id_joven':(int), 'id_competencias'=(List)}
