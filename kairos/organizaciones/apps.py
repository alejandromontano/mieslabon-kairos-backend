from django.apps import AppConfig


class OrganizacionesConfig(AppConfig):
    name = 'kairos.organizaciones'
