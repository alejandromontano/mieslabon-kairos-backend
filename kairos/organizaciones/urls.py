# Modulos Django
from django.urls import path
from django.contrib.auth.views import  PasswordResetConfirmView

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "organizaciones"

urlpatterns = [

    path('gestionar/', view=OrganizacionesListado.as_view(), name='listado'),
    path('consultar/', view=OrganizacionesConsultar.as_view(), name='consultar'),
    path('registrar/', view=RegistrarOrganizacion.as_view(), name='registrar'),
    path('modificar/<int:id_usuario>', view=ModificarOrganizacion.as_view(), name='modificar'),
    path('modificar-perfil/', view=usuarioPerfil, name='modificar_perfil_organizacion'),
    path('perfil-actual/', view=miPerfilActualDetalle, name='perfil_actual_organizacion'),
    path('asignar-jovenes/<int:id_organizacion>', view=AsignarJovenOrganizacion.as_view(), name='asignar_jovenes_organizacion'),
    
    path('mis-organizaciones/', view=OrganizacionesJovenConsultar.as_view(), name='joven_consultar'),
    path('mis-organizaciones/miembro', view=misOrganizacionesJovenConsultar, name='joven_consultar_organizaciones'),
    path('mis-organizaciones/detalle/<int:pk>', view=OrganizacionJovenDetalle.as_view(), name='joven_detalle'),
    path('mis-organizaciones/favoritas', view=organizacionesJovenFavoritas, name='joven_favoritas'),
    path('mis-organizaciones/favoritas/detalle/<int:pk>', view=OrganizacionJovenDetalle.as_view(), name='joven_detalle_favorita'),
    path('mis-organizaciones/sugeridas', view=organizacionesJovenSugeridas, name='joven_sugeridas'),
    path('mis-organizaciones/sugeridas/detalle/<int:pk>', view=OrganizacionJovenDetalle.as_view(), name='joven_detalle_sugerida'),

    #path('registrarse/', view=organizacionRegistro, name='registrarse'),

    path('tipos/gestionar/', view=TipoOrganizacionListado.as_view(), name='listado_tipos'),
    path('tipos/consultar/', view=TipoOrganizacionConsultar.as_view(), name='consultar_tipos'),
    path('tipos/registrar', view=TipoOrganizacionRegistrar.as_view(), name='registrar_tipo'),
    path('tipos/modificar/<int:id_tipo>', view=TipoOrganizacionModificar.as_view(), name='modificar_tipo'),

    path("api/porcentaje-joven", view=ApiCargarPorcentajes.as_view(), name="api_obtener_porcentaje_joven"),
    path("api/cargar-calificaciones", view=ApiCargarCalificaciones.as_view(), name="api_cargar_calificaciones"),
    path("api/cargar-calificacion", view=ApiCargarCalificacion.as_view(), name="api_cargar_calificacion"),
    path("api/calificar", view=ApiCalificarContenido.as_view(), name="api_calificar"),

    path('consultar-jovenes/', view=OrganizacionJovenesConsultar.as_view(), name='organizacion_consultar_jovenes'),
    path('consultar-equipos/', view=OrganizacionEquiposConsultar.as_view(), name='organizacion_equipos_jovenes'),

    path('jovenes/enviar-solicitud/<int:id_receptor>/<int:usuario_tarjeta>', view=OrganizacionJovenEnviarSolicitud, name='organizacion_jovenes_enviar_solicitud'),
    path('jovenes/aceptar-solicitud/<int:id_emisor>/<int:usuario_tarjeta>', view=OrganizacionJovenAceptarSolicitud, name='organizacion_jovenes_aceptar_solicitud'),
    path('jovenes/rechazar-solicitud/<int:id_emisor>/<int:usuario_tarjeta>', view=OrganizacionJovenRechazarSolicitud, name='organizacion_jovenes_rechazar_solicitud'),
    path('jovenes/finalizar-relacion/<int:id_emisor>/<int:usuario_tarjeta>', view=OrganizacionJovenFinalizarRelacion, name='organizacion_jovenes_finalizar_relacion'),

    path('invitar-joven/', view=InvitarJovenesPlataforma.as_view(), name='organizacion_invitar_joven'),
    path('asignar-joven-grado/', view=AsignarOrganizacionJovenesGrado.as_view(), name='asignar_organizacion_jovenes_grado'),

    path('contrasena-organizaciones/crear/(<uidb64>)-(<token>)/', CrearContrasenia.as_view(), name='organizacion_cambiar_contrasena'),

    path('gestion-retos/', view=CrearSolicitudReto.as_view(), name='organizacion_gestion_retos'),
    path('retos/gestionar_solicitudes/', GestionDeSolicitudesRetos.as_view(), name='organizacion_gestion_solicitudes_retos'),

    path('gestion-rutas/', view=RutasConsultar.as_view(), name='organizacion_gestion_rutas'),
    path('gestion-oportunidades/', view=OportunidadesConsultar.as_view(), name='organizacion_gestion_oportunidades'),

    path('equipos/enviar-solicitud/<int:id_organizacion>/<int:id_equipo>', view=OrganizacionEquipoEnviarSolicitud, name='organizacion_equipos_enviar_solicitud'),
    path('equipos/aceptar-solicitud/<int:id_organizacion>/<int:id_equipo>', view=OrganizacionEquipoAceptarSolicitud, name='organizacion_equipos_aceptar_solicitud'),
    path('equipos/rechazar-solicitud/<int:id_organizacion>/<int:id_equipo>', view=OrganizacionEquipoRechazarSolicitud, name='organizacion_equipos_rechazar_solicitud'),
    path('equipos/finalizar-relacion/<int:id_organizacion>/<int:id_equipo>', view=OrganizacionEquipoFinalizarRelacion, name='organizacion_equipos_finalizar_relacion'),














]