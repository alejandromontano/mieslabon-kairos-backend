# Modulos Django
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import PasswordResetConfirmView
from django.http.response import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import FormView, CreateView, UpdateView, ListView, DetailView
from django.views.generic.base import TemplateView

# Modulos de plugin externos
from rest_framework.views import APIView
from rest_framework.response import Response
import json
from openpyxl import Workbook

# Modulos de otras apps
from kairos.usuarios.views import login
from kairos.insignias.models import Tribus
from kairos.organizaciones.models import Organizacion
from kairos.guia.models import Recurso, VerRecurso
from kairos.core.mixins import MensajeMixin

# Modulos internos de la app
from .forms import *

class RegistrarOrganizacion(LoginRequiredMixin, CreateView):
    model = Organizacion
    form_class = OrganizacionModelForm
    template_name = "organizaciones/registro.html"
    # success_url = reverse_lazy('organizaciones:listado')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.es_superusuario == False:
            messages.error(request, "Usted no está autorizado para gestionar las organizaciones")
            return redirect("inicio")
        return super(RegistrarOrganizacion, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        from kairos.core.utils import crear_contrasenia
        organizacion = form.save(commit=False)
        organizacion.tipo = "Organización"
        organizacion.correo_verificado = True
        organizacion.save()
        form.save_m2m()
        mis_tribus = form.instance.tribus()
        for mi_tribu in mis_tribus:
            tribu = Tribus.objects.get(nombre__icontains=mi_tribu)
            form.instance.tribus_relacionadas.add(tribu)

        #crear_contrasenia(organizacion.email, 'MiEslabón | CREAR CONTRASEÑA', 'organizaciones/correos/crear_contrasenia.html')
        messages.success(self.request, "Organización registrada satisfactoriamente")
        return redirect('organizaciones:listado')


class CrearContrasenia(PasswordResetConfirmView):
    template_name = 'organizaciones/gestion_contrasenia/crear_contrsenia.html'

    def get_success_url(self):
        from django.utils.http import urlsafe_base64_decode
        try:
            uid = urlsafe_base64_decode(self.kwargs['uidb64'])
            usuario = Usuario._default_manager.get(pk=uid)
            usuario.correo_verificado = True
            usuario.save()
        except (TypeError, ValueError, OverflowError, Usuario.DoesNotExist):
            pass

        return reverse_lazy('usuarios:contrasena_cambiada')

class ModificarOrganizacion(LoginRequiredMixin, UpdateView):
    model = Organizacion
    form_class = OrganizacionModificarForm
    pk_url_kwarg = 'id_usuario'
    template_name = 'organizaciones/modificar.html'
    success_url = reverse_lazy('organizaciones:listado')

    def form_valid(self, form):
        mis_tribus = form.instance.tribus()
        mis_tribus_antes = form.instance.tribus_relacionadas.all()
        for mi_tribu_antes in mis_tribus_antes:
            if not str(mi_tribu_antes.nombre).lower() in mis_tribus:
                form.instance.tribus_relacionadas.remove(mi_tribu_antes)
        for mi_tribu in mis_tribus:
            tribu = Tribus.objects.get(nombre__icontains=mi_tribu)
            if not tribu in mis_tribus_antes:
                form.instance.tribus_relacionadas.add(tribu)
        messages.success(self.request, "Organización modificada satisfactoriamente")
        return super(ModificarOrganizacion, self).form_valid(form)


class OrganizacionesListado(LoginRequiredMixin, ListView):
    model = Organizacion
    template_name = "organizaciones/listado.html"
    context_object_name = "organizaciones"

    def dispatch(self, request, *args, **kwargs):        
        if request.user.is_authenticated and request.user.es_superusuario == False:
            messages.error(request, "Usted no está autorizado para gestionar las organizaciones")
            return redirect("inicio")
        return super(OrganizacionesListado, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        organizaciones = Organizacion.objects.all()
        return organizaciones


class OrganizacionesConsultar(LoginRequiredMixin, ListView):
    model = Usuario
    template_name = "organizaciones/consultar.html"
    context_object_name = "organizaciones"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.es_superusuario == False:
            messages.error(request, "Usted no está autorizado para gestionar las organizaciones")
            return redirect("inicio")
        return super(OrganizacionesConsultar, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        organizaciones = Organizacion.objects.all()
        return organizaciones


class TipoOrganizacionListado(LoginRequiredMixin, ListView):
    model = TipoOrganizacion
    context_object_name = "tipos"
    template_name = "organizaciones/tipos/listado.html"

    def get_queryset(self):
        tipos = TipoOrganizacion.objects.all()
        return tipos


class TipoOrganizacionConsultar(LoginRequiredMixin, ListView):
    model = TipoOrganizacion
    context_object_name = "tipos"
    template_name = "organizaciones/tipos/consultar.html"

    def get_queryset(self):
        tipos = TipoOrganizacion.objects.all()
        return tipos


class TipoOrganizacionRegistrar(LoginRequiredMixin, CreateView):
    model = TipoOrganizacion
    fields = '__all__'
    template_name = "organizaciones/tipos/registrar.html"
    success_url = reverse_lazy('organizaciones:listado_tipos')

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Tipo de organización registrado satisfactoriamente")
        return super(TipoOrganizacionRegistrar, self).form_valid(form)

class AsignarJovenOrganizacion(LoginRequiredMixin, UpdateView):
    model = Organizacion
    form_class = AsignarJovenesAOrganizacionForm
    pk_url_kwarg = 'id_organizacion'
    template_name = "organizaciones/jovenes/asignar_jovenes.html"
    success_url = reverse_lazy('organizaciones:listado')

    def form_valid(self, form):
        #form.save_m2m()
        messages.success(self.request, "Jóvenes asigandos satisfactoriamente")
        return super(AsignarJovenOrganizacion, self).form_valid(form)

class TipoOrganizacionModificar(LoginRequiredMixin, UpdateView):
    model = TipoOrganizacion
    fields = '__all__'
    pk_url_kwarg = 'id_tipo'
    template_name = "organizaciones/tipos/modificar.html"
    success_url = reverse_lazy('organizaciones:listado_tipos')

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Tipo de organización modificado satisfactoriamente")
        return super(TipoOrganizacionModificar, self).form_valid(form)

# ################USER VIEW##################
    '''def organizacionRegistro(request, tipo):

    form = OrganizacionRegistroForm()
    nuevo_usuario = ''
    if request.user.is_authenticated:
        messages.error(request, "Usted no está autorizado para realizar esta acción")
        return redirect('inicio')

    if request.method == 'POST':
        form = OrganizacionRegistroForm(request.POST)

        if form.is_valid():
            nueva_organizaciones = form.save(commit=False)
            nueva_organizaciones.tipo = "Organización"
            nueva_organizaciones.save()
            if request.user.is_anonymous:
                login(request, nuevo_usuario)
            messages.success(request, "Registro realizado satisfactoriamente.")
            return redirect('usuarios:perfil')
        else:
            messages.error(request, "Datos inválidos. Por favor intente de nuevo.")
            return redirect('organizaciones:registrarse')

    return render(request, 'usuarios/registrarse.html', {
        'form': form
    })'''


class OrganizacionesJovenConsultar(LoginRequiredMixin, ListView):
    template_name = "organizaciones/joven_consultar.html"
    paginate_by = 8

    def get_queryset(self):
        consulta = self.obtener_consulta()
        return consulta['organizaciones']

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator
        from kairos.guia.models import Recurso
        context = super(OrganizacionesJovenConsultar, self).get_context_data(**kwargs)

        usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()        

        pagination = Paginator(list(consulta['organizaciones']), self.paginate_by)

        context['organizaciones'] = pagination.page(context['page_obj'].number)
        context['tag'] = consulta['tag']
        context['tipos'] = [
                    {'nombre': 'Todas'},
                    {'nombre': 'Sugeridas'},
                    {'nombre': 'Favoritas'},
                ]
        if usuario_actual.tipo == 'Joven':
            context['tipos'].append({'nombre': 'Org donde estoy'})
            context['tipos'].append({'nombre': 'Solicitudes Enviadas'})
            context['tipos'].append({'nombre': 'Solicitudes Recibidas'})

        return context

    def get_success_url(self):
        if self.request.user.es_superusuario:
            return reverse_lazy('contenido:listado_oportunidades')
        else:
            return reverse_lazy('contenido:listado_mis_oportunidades_creadas')

    def obtener_consulta(self):
        from kairos.core.utils import obtener_lista_usuario_caracteristicas
        usuario = self.obtener_usuario()

        if 'tag' in self.request.GET:
            tag = self.request.GET['tag']
        elif list(self.request.GET):
            tag = list(self.request.GET)[0]
        else:
            tag = "Todas"

        if usuario.tipo != 'Organización':
            mis_organizaciones = Organizacion.objects.filter(is_active=True)
        else:
            mis_organizaciones = Organizacion.objects.filter(is_active=True).exclude(id=usuario.id)

        if tag == 'Todas':
            mis_organizaciones = mis_organizaciones
        elif tag == 'Sugeridas':
            mis_organizaciones = Organizacion.sugeridas(usuario)
        elif tag == 'Favoritas':
            mis_organizaciones = Organizacion.favoritas(usuario)
        elif tag == 'Solicitudes Enviadas':
            if usuario.tipo == 'Joven':
                solicitudes = SolicitudesOrganizacionJovenes.objects.filter(joven=usuario, estado='Postulado').values_list('organizacion__id')
                mis_organizaciones = Organizacion.objects.filter(id__in=solicitudes)
        elif tag == 'Solicitudes Recibidas':
            if usuario.tipo == 'Joven':
                solicitudes = SolicitudesOrganizacionJovenes.objects.filter(joven=usuario, estado='Invitado').values_list('organizacion__id')
                mis_organizaciones = Organizacion.objects.filter(id__in=solicitudes)
        elif tag == 'Org donde estoy':
            mis_organizaciones = usuario.organizaciones_joven.all()

        if 'q' in self.request.GET:
            query = self.request.GET['q']
            mis_organizaciones = Organizacion.get_queryset(query, mis_organizaciones)

        return {'tag': tag, 'organizaciones': mis_organizaciones}

    def obtener_usuario(self):
        usuario = Usuario.objects.get(id=self.request.user.id)
        if self.request.user.tipo == 'Joven':
            usuario = Joven.objects.get(id=self.request.user.id)
        elif self.request.user.tipo == 'Organización':
            usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario

def misOrganizacionesJovenConsultar(request) -> 'render':
    '''
        Permite obtener las organizaciones a las que el usuario actual pertenece

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    '''
    template_name = "organizaciones/joven_consultar.html"
    joven = Joven.objects.get(pk=request.user.pk)
    mis_organizaciones = joven.obtener_organizaciones().filter(is_active=True)
    
    tipos = [
        {'nombre': 'Todas'},
        {'nombre': 'Sugeridas'},
        {'nombre': 'Favoritas'}
    ]
    tag = None
    if request.GET:
        if 'tag' in request.GET:
            tag = request.GET['tag']
            mis_organizaciones = Organizacion.get_queryset_tag(tag, mis_organizaciones)
        elif 'q' in request.GET:
            query = request.GET['q']
            mis_organizaciones = Organizacion.get_queryset(query, mis_organizaciones)
    return render(request, template_name, {'organizaciones': mis_organizaciones, 'tipos': tipos, 'opcion': 'todas', 'tag': tag})


class OrganizacionJovenDetalle(DetailView):
    model = Organizacion
    template_name = "organizaciones/joven_detalle.html"
    context_object_name = "organizacion"

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated == True:
            if not Organizacion.existe_organizacion(kwargs['pk']):
                messages.error(self.request, "La organización no existe")
                return redirect('organizaciones:joven_consultar')

        return super(OrganizacionJovenDetalle, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        from kairos.core.utils import relacion_con_usuario

        context = dict()

        if self.request.user.tipo == 'Joven':
            joven_sesion = Joven.objects.get(id=self.request.user.id)
            organizacion_tarjeta = Organizacion.objects.get(id=kwargs['object'].id)
            context = relacion_con_usuario(joven_sesion, organizacion_tarjeta, estado=0)
        elif self.request.user.tipo == 'Organización':
            organizacion_sesion = Organizacion.objects.get(id=self.request.user.id)
            organizacion_tarjeta = Organizacion.objects.get(id=kwargs['object'].id)
            context = relacion_con_usuario(organizacion_sesion, organizacion_tarjeta)
        context['usuario'] = organizacion_tarjeta
        return context


def organizacionesJovenFavoritas(request) -> 'render':
    '''
        Permite obtener las organizaciones a las que el usuario actual le ha dado favorito

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    '''
    template_name = "organizaciones/joven_consultar.html"
    
    if request.user.tipo != 'Organización':
        mis_organizaciones = Organizacion.favoritas(request.user)        
    else:
        mis_organizaciones = Organizacion.favoritas(request.user).exclude(id=request.user.id)
    
    tipos = [
        {'nombre': 'Todas'},
        {'nombre': 'Sugeridas'},
        {'nombre': 'Favoritas'}
    ]
    tag = None
    if request.GET:
        if 'tag' in request.GET:
            tag = request.GET['tag']
            mis_organizaciones = Organizacion.get_queryset_tag(tag, mis_organizaciones)
        elif 'q' in request.GET:
            query = request.GET['q']
            mis_organizaciones = Organizacion.get_queryset(query, mis_organizaciones)
    return render(request, template_name, {'organizaciones': mis_organizaciones, 'tipos': tipos, 'opcion': 'favoritas', 'tag': tag})


def organizacionesJovenSugeridas(request):
    '''
        Permite obtener las organizaciones a las que el usuario actual le son sugeridas

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    '''
    template_name = "organizaciones/joven_consultar.html"
    
    if request.user.tipo != 'Organización':
        mis_organizaciones = Organizacion.sugeridas(request.user)        
    else:
        mis_organizaciones = Organizacion.sugeridas(request.user).exclude(id=request.user.id)
    
    tipos = [
        {'nombre': 'Todas'},
        {'nombre': 'Sugeridas'},
        {'nombre': 'Favoritas'}
    ]
    tag = None
    if request.GET:
        if 'tag' in request.GET:
            tag = request.GET['tag']
            mis_organizaciones = Organizacion.get_queryset_tag(tag, mis_organizaciones)
        elif 'q' in request.GET:
            query = request.GET['q']
            mis_organizaciones = Organizacion.get_queryset(query, mis_organizaciones)
    return render(request, template_name, {'organizaciones': mis_organizaciones, 'tipos': tipos, 'opcion': 'sugeridas', 'tag': tag})


class ApiCargarCalificaciones(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        pk_usuario = self.request.user.pk
        calificaciones = CalificacionOrganizacion.objects.filter(usuario=pk_usuario)
        cals = []
        for c in calificaciones:
            cals.append({'pk': c.organizacion.pk, 'stars': c.calificacion})
        return Response({'calificaciones': cals})


class ApiCargarCalificacion(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_contenido = data["pk_contenido"]
        pk_usuario = self.request.user.pk
        c = CalificacionOrganizacion.objects.get(usuario=pk_usuario, organizacion=pk_contenido)
        cal = [{'pk': c.organizacion.pk, 'stars': c.calificacion}]
        return Response({'calificaciones': cal})


class ApiCalificarContenido(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_contenido = data["pk_contenido"]
        pk_usuario = self.request.user.pk
        if "star" in data:
            calificacion, creado = CalificacionOrganizacion.objects.get_or_create(
                organizacion_id=pk_contenido, usuario_id=pk_usuario
            )
            if data['star'] == 0 or data['star'] == 1:
                calificacion.calificacion = data['star']
                calificacion.save()
            else:
                calificacion.delete()
        return Response({})

class ApiCargarPorcentajes(APIView):
    def get(self, request):
        id = request.GET.get('id')
        porcentajes = Joven.buscar(id).obtener_porcentajes()
        return Response(porcentajes)


class OrganizacionJovenesConsultar(LoginRequiredMixin, ListView):
    template_name = "organizaciones/jovenes/consultar_jovenes.html"
    paginate_by = 10

    def get_queryset(self):
        usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()
        return list(consulta['jovenes'])

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator
        context = super(OrganizacionJovenesConsultar, self).get_context_data(**kwargs)

        organizacion = self.obtener_usuario()
        consulta = self.obtener_consulta()

        paginationFiltered = Paginator(list(consulta['jovenes']), self.paginate_by)
        pagina = paginationFiltered.page(context['page_obj'].number)
        cohortes = organizacion.obtener_cohortes_todas()
        q = int(self.request.GET.get("q", 0))
        j = self.request.GET.get("j", "")
        context['jovenes'] = pagina
        context['cohortes'] = cohortes
        context['organizacion'] = organizacion
        context['q'] = q
        context['j'] = j
        context['tag'] = consulta['tag']
        context['solicitudes'] = consulta['solicitudes']

        return context

    def obtener_consulta(self):
        from kairos.core.utils import obtener_lista_usuario_caracteristicas
        q = int(self.request.GET.get("q", 0))
        j = self.request.GET.get("j", "")
        tag = self.request.GET.get("tag", "")

        usuario = self.obtener_usuario()
        cohorte = CohortesColegios.buscar(q)
        solicitudes_jovenes = obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_lista_usuarios_solicitudes())

        if tag == 'Solicitudes':
            jovenes = solicitudes_jovenes
        else:
            jovenes = usuario.obtener_jovenes_activos_cohortes(cohorte, j)

        return {'jovenes': jovenes, 'tag': tag, 'solicitudes': len(solicitudes_jovenes)}


    def obtener_usuario(self):
        usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario

class OrganizacionEquiposConsultar(LoginRequiredMixin, ListView):
    template_name = "organizaciones/equipos/consultar_equipos.html"
    paginate_by = 12

    def dispatch(self, request, *args, **kwargs):
        request.session['equipo_organizacion'] = 0
        return super(OrganizacionEquiposConsultar, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        consulta = self.obtener_consulta()
        return consulta['equipos']

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator
        context = super(OrganizacionEquiposConsultar, self).get_context_data(**kwargs)

        usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()

        pagination = Paginator(list(consulta['equipos']), self.paginate_by)

        context['equipos'] = pagination.page(context['page_obj'].number)
        context['tag'] = consulta['tag']
        context['tipos'] = [{'nombre': 'Mis Equipos'},
                        {'nombre': 'Favoritos'},
                        {'nombre': 'Todos los Equipos'},
                        {'nombre': 'Solicitudes Enviadas'},
                        {'nombre': 'Solicitudes Recibidas'}
                        ]

        return context

    def obtener_consulta(self):
        from kairos.core.utils import obtener_lista_usuario_caracteristicas
        from kairos.equipos.models import Equipo
        usuario = self.obtener_usuario()
        tag = 'Todos los Equipos'
        equipos = []

        if 'tag' in self.request.GET:
            tag = self.request.GET['tag']
        elif list(self.request.GET):
            tag = list(self.request.GET)[0]
        else:
            tag = "Todos los Equipos"

        if tag == 'Mis Equipos':
            equipos = obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_miembros_equipos())
        elif tag == 'Favoritos':
            equipos = obtener_lista_usuario_caracteristicas(usuario, Equipo.obtener_equipos_favoritos(usuario))
        elif tag == 'Todos los Equipos':
            equipos = obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_equipos_no_miembros())
        elif tag == 'Solicitudes Enviadas':
            if usuario.tipo == 'Organización':
                solicitudes = SolicitudesOrganizacionEquipos.objects.filter(organizacion=usuario, estado='Invitado').values_list('equipo__id')
                equipos_invitados = Equipo.objects.filter(id__in=solicitudes)
                equipos = obtener_lista_usuario_caracteristicas(usuario, equipos_invitados)
        elif tag == 'Solicitudes Recibidas':
            if usuario.tipo == 'Organización':
                solicitudes = SolicitudesOrganizacionEquipos.objects.filter(organizacion=usuario, estado='Postulado').values_list('equipo__id')
                equipos_postulados = Equipo.objects.filter(id__in=solicitudes)
                equipos = obtener_lista_usuario_caracteristicas(usuario, equipos_postulados)

        if 'q' in self.request.GET:
            equipos = Equipo.objects.all()
            query = self.request.GET['q']
            equipos = obtener_lista_usuario_caracteristicas(usuario, Equipo.buscador(query, equipos))

        return {'tag': tag, 'equipos': equipos}

    def obtener_usuario(self):
        usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario

# usuario_tarjeta hace referencia a donde se esta realizando la accion:
# usuario_tarjeta=0 hace referencia al template en organizaciones->jovenes->consultar-jovenes.html
# usuario_tarjeta=1 hace referencia al template en usuarios->tarjeta_presentacion.html
# usuario_tarjeta=2 hace referencia al template en organizaciones->detalle_joven.html
@login_required
def OrganizacionJovenEnviarSolicitud(request, id_receptor:int, usuario_tarjeta:int) -> 'redirect':    
    '''
        Permite enviar una SolicitudesOrganizacionJovenes de un Joven hacia una Organización o viceversa

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_receptor (int): Identificador del Usuario que recibe la solicitud
            usuario_tarjeta (int): Template donde ocurre la acción
    '''
    from kairos.organizaciones.models import SolicitudesOrganizacionJovenes

    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización'):
        if Usuario.existe_usuario(id_receptor):
            if request.user.tipo == 'Joven':
                joven = Joven.objects.get(id=request.user.id)
                organizacion = Organizacion.objects.get(id=id_receptor)
                estado = 'Postulado'
            else:
                joven = Joven.objects.get(id=id_receptor)
                organizacion = Organizacion.objects.get(id=request.user.id)
                estado = 'Invitado'

            if SolicitudesOrganizacionJovenes.existe_solitud(organizacion=organizacion, joven=joven) == True:
                messages.error(request, "Ya se ha enviado una solicitud")
            else:
                messages.success(request, "Solicitud enviada correctamente")
                SolicitudesOrganizacionJovenes.objects.create(organizacion=organizacion, joven=joven, estado=estado)
        else:
            messages.error(request, "No existe el receptor")
    else:
        messages.error(request, "No esta autorizado para realizar esa acción")

    if usuario_tarjeta == 0:
        return redirect('organizaciones:organizacion_consultar_jovenes')
    elif usuario_tarjeta == 1:
        return redirect('usuarios:tarjeta_presentacion', id_receptor)
    else:
        return redirect('organizaciones:joven_detalle', id_receptor)


@login_required
def OrganizacionJovenRechazarSolicitud(request, id_emisor:int, usuario_tarjeta:int=0) -> 'redirect':
    '''
        Permite rechazar una SolicitudesOrganizacionJovenes de un Joven hacia una Organización o viceversa

        Parametros:
            request: Elemento Http que posee información de la sesión.
            id_emisor: (int) Identificador del Usuario que recibe la solicitud
            usuario_tarjeta (int): Template donde ocurre la acción
    '''
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización'):
        if Usuario.existe_usuario(id_emisor):

            if request.user.tipo == 'Joven':
                joven = Joven.objects.get(id=request.user.id)
                organizacion = Organizacion.objects.get(id=id_emisor)
            else:
                joven = Joven.objects.get(id=id_emisor)
                organizacion = Organizacion.objects.get(id=request.user.id)

            if SolicitudesOrganizacionJovenes.existe_solitud(organizacion=organizacion, joven=joven) == True:
                mensaje = ""
                if SolicitudesOrganizacionJovenes.objects.get(organizacion=organizacion, joven=joven).estado == 'Invitado':
                    if request.user.tipo == 'Joven':
                        mensaje = "Solicitud de miembro rechazada"
                    else:
                        mensaje = "Solicitud de miembro cancelada"
                else:
                    if request.user.tipo == 'Joven':
                        mensaje = "Solicitud de miembro cancelada"
                    else:
                        mensaje = "Solicitud de miembro rechazada"

                SolicitudesOrganizacionJovenes.objects.get(organizacion=organizacion, joven=joven).delete()
                messages.success(request, mensaje)
            else:
                messages.error(request, "No tienes solicitudes de miembro con el usuario")
    else:
        messages.error(request, "El usuario no existe")

    if usuario_tarjeta == 0:
        return redirect('organizaciones:organizacion_consultar_jovenes')
    elif usuario_tarjeta == 1:
        return redirect('usuarios:tarjeta_presentacion', id_emisor)
    else:
        return redirect('organizaciones:joven_detalle', id_emisor)

@login_required
def OrganizacionJovenAceptarSolicitud(request, id_emisor:int, usuario_tarjeta:int) -> 'redirect':
    '''
        Permite aceptar una SolicitudesOrganizacionJovenes de un Joven hacia una Organización o viceversa

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_emisor (int): Identificador del Usuario que envio la solicitud
            usuario_tarjeta (int): Template donde ocurre la accion
    '''
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización'):
        if Usuario.existe_usuario(id_emisor):

            if request.user.tipo == 'Joven':
                joven = Joven.objects.get(id=request.user.id)
                organizacion = Organizacion.objects.get(id=id_emisor)
            else:
                joven = Joven.objects.get(id=id_emisor)
                organizacion = Organizacion.objects.get(id=request.user.id)

            if SolicitudesOrganizacionJovenes.existe_solitud(organizacion=organizacion, joven=joven) == True:
                solicitud = SolicitudesOrganizacionJovenes.objects.get(organizacion=organizacion, joven=joven)
                organizacion.agregar_joven(joven)
                joven.agregar_organizacion(organizacion)
                solicitud.delete()
                messages.success(request, "Solicitud de miembro aceptada")
            else:
                messages.error(request, "No tienes solicitudes de miembro con el usuario")
            
            if InformacionJovenInvitado.existe_registro(organizacion, joven.email):
                registro_joven_invitado = InformacionJovenInvitado.obtener_registro(organizacion, joven.email)
                joven.fecha_induccion = registro_joven_invitado.fecha_induccion
                joven.ultimo_grado = registro_joven_invitado.grado
                joven.save()

    else:
        messages.error(request, "El usuario no existe")

    if usuario_tarjeta == 0:
        return redirect('organizaciones:organizacion_consultar_jovenes')
    elif usuario_tarjeta == 1:
        return redirect('usuarios:tarjeta_presentacion', id_emisor)
    else:
        return redirect('organizaciones:joven_detalle', id_emisor)


@login_required
def OrganizacionJovenFinalizarRelacion(request, id_emisor:int, usuario_tarjeta:int=0) -> 'redirect':
    '''
        Permite finalizar la relacion de un Joven hacia una Organización o viceversa

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_emisor (int): Identificador del Usuario al que se quiere eliminar
            usuario_tarjeta (int): Template donde ocurre la accion
    '''
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización'):
        if Usuario.existe_usuario(id_emisor):
            relacion = False
            if request.user.tipo == 'Joven':
                joven = Joven.objects.get(id=request.user.id)
                organizacion = Organizacion.objects.get(id=id_emisor)
            else:
                joven = Joven.objects.get(id=id_emisor)
                organizacion = Organizacion.objects.get(id=request.user.id)

            if organizacion.joven_hace_parte_organizacion(joven) == True:
                mensaje = ""

                if request.user.tipo == 'Joven':
                    mensaje = "Has dejado de la organización"
                else:
                    mensaje = "El usuario ha sido expulsado de la organización"

                organizacion.jovenes.remove(joven)
                messages.success(request, mensaje)
            else:
                messages.error(request, "No hay una relación de miembro con el usuario")
    else:
        messages.error(request, "El usuario no existe")

    if usuario_tarjeta == 0:
        return redirect('organizaciones:organizacion_consultar_jovenes')
    elif usuario_tarjeta == 1:
        return redirect('usuarios:tarjeta_presentacion', id_emisor)
    else:
        return redirect('organizaciones:joven_detalle', id_emisor)


#############################################################
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''                SOLICITUDES A EQUIPOS                 '''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#############################################################

# usuario_tarjeta hace referencia a donde se esta realizando la accion:
# request.session['equipo_organizacion']=0 hace referencia al template en organizaciones->jovenes->consultar-jovenes.html
# request.session['equipo_organizacion']=1 hace referencia al template en usuarios->tarjeta_presentacion.html

@login_required
def OrganizacionEquipoEnviarSolicitud(request, id_organizacion:int, id_equipo:int) -> 'redirect':    
    '''
        Permite enviar una SolicitudesOrganizacionEquipos de un equipo hacia una organización o viceversa

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_organizacion (int): Identificador de la Organización
            id_equipo (int): Identificador del Equipo
    '''
    from kairos.organizaciones.models import SolicitudesOrganizacionEquipos

    if request.user.is_authenticated == True and (request.user.tipo == 'Organización' or request.user.tipo == 'Joven'):
        if request.user.tipo == 'Joven':
            equipo = Equipo.objects.get(id=id_equipo)
            organizacion = Organizacion.objects.get(id=id_organizacion)
            estado = 'Postulado'
        else:
            equipo = Equipo.objects.get(id=id_equipo)
            organizacion = Organizacion.objects.get(id=request.user.id)
            estado = 'Invitado'

        if SolicitudesOrganizacionEquipos.existe_solitud(organizacion=organizacion, equipo=equipo) == True:
            messages.error(request, "Ya se ha enviado una solicitud")
        else:
            messages.success(request, "Solicitud enviada correctamente")
            SolicitudesOrganizacionEquipos.objects.create(organizacion=organizacion, equipo=equipo, estado=estado)

    else:
        messages.error(request, "No esta autorizado para realizar esa acción")

    if request.session['equipo_organizacion'] == 0:
        return redirect('organizaciones:organizacion_equipos_jovenes')
    elif request.session['equipo_organizacion'] == 1:
        return redirect('equipos:detalle_equipo', id_equipo)
    elif request.session['equipo_organizacion'] == 2:
        return redirect('equipos:consultar_todos_los_equipos')


@login_required
def OrganizacionEquipoRechazarSolicitud(request, id_organizacion:int, id_equipo:int) -> 'redirect':    
    '''
        Permite rechazar una SolicitudesOrganizacionEquipos enviada/recibida de un equipo a organización o viceversa

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_organizacion (int): Identificador de la Organización
            id_equipo (int): Identificador del Equipo
    '''

    from kairos.organizaciones.models import SolicitudesOrganizacionEquipos

    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización'):

        if request.user.tipo == 'Joven':
            equipo = Equipo.objects.get(id=id_equipo)
            organizacion = Organizacion.objects.get(id=id_organizacion)
        else:
            equipo = Equipo.objects.get(id=id_equipo)
            organizacion = Organizacion.objects.get(id=request.user.id)

        if SolicitudesOrganizacionEquipos.existe_solitud(organizacion=organizacion, equipo=equipo) == True:
            mensaje = ""
            if SolicitudesOrganizacionEquipos.objects.get(organizacion=organizacion, equipo=equipo).estado == 'Invitado':
                if request.user.tipo == 'Joven':
                    mensaje = "Solicitud de miembro rechazada"
                else:
                    mensaje = "Solicitud de miembro cancelada"
            else:
                if request.user.tipo == 'Joven':
                    mensaje = "Solicitud de miembro cancelada"
                else:
                    mensaje = "Solicitud de miembro rechazada"

            SolicitudesOrganizacionEquipos.objects.get(organizacion=organizacion, equipo=equipo).delete()
            messages.success(request, mensaje)
        else:
            messages.error(request, "No tienes solicitudes de miembro con el usuario")
    else:
        messages.error(request, "El usuario no existe")

    if request.session['equipo_organizacion'] == 0:
        return redirect('organizaciones:organizacion_equipos_jovenes')
    elif request.session['equipo_organizacion'] == 1:
        return redirect('equipos:detalle_equipo', id_equipo)
    elif request.session['equipo_organizacion'] == 2:
        return redirect('equipos:consultar_todos_los_equipos')

@login_required
def OrganizacionEquipoAceptarSolicitud(request, id_organizacion:int, id_equipo:int) -> 'redirect':    
    '''
        Permite aceptar una SolicitudesOrganizacionEquipos enviada/recibida de un equipo a organización o viceversa

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_organizacion (int): Identificador de la Organización
            id_equipo (int): Identificador del Equipo
    '''
    from kairos.organizaciones.models import SolicitudesOrganizacionEquipos

    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización'):

        if request.user.tipo == 'Joven':
            equipo = Equipo.objects.get(id=id_equipo)
            organizacion = Organizacion.objects.get(id=id_organizacion)
        else:
            equipo = Equipo.objects.get(id=id_equipo)
            organizacion = Organizacion.objects.get(id=request.user.id)

        if SolicitudesOrganizacionEquipos.existe_solitud(organizacion=organizacion, equipo=equipo) == True:
            solicitud = SolicitudesOrganizacionEquipos.objects.get(organizacion=organizacion, equipo=equipo)
            organizacion.agregar_equipo(equipo)
            equipo.agregar_organizacion(organizacion)
            solicitud.delete()
            messages.success(request, "Solicitud de miembro aceptada")
        else:
            messages.error(request, "No tienes solicitudes de miembro con el usuario")

    else:
        messages.error(request, "El usuario no existe")

    if request.session['equipo_organizacion'] == 0:
        return redirect('organizaciones:organizacion_equipos_jovenes')
    elif request.session['equipo_organizacion'] == 1:
        return redirect('equipos:detalle_equipo', id_equipo)
    elif request.session['equipo_organizacion'] == 2:
        return redirect('equipos:consultar_todos_los_equipos')


@login_required
def OrganizacionEquipoFinalizarRelacion(request, id_organizacion:int, id_equipo:int) -> 'redirect':
    '''
        Permite eliminar la relacion de un equipo con una organizacion o viceversa

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_organizacion (int): Identificador de la Organización
            id_equipo (int): Identificador del Equipo
    '''
    if request.user.is_authenticated == True and (request.user.tipo == 'Joven' or request.user.tipo == 'Organización'):

        relacion = False
        if request.user.tipo == 'Joven':
            equipo = Equipo.objects.get(id=id_equipo)
            organizacion = Organizacion.objects.get(id=id_organizacion)
        else:
            equipo = Equipo.objects.get(id=id_equipo)
            organizacion = Organizacion.objects.get(id=request.user.id)

        if organizacion.equipo_hace_parte_organizacion(equipo) == True:
            mensaje = ""

            if request.user.tipo == 'Joven':
                mensaje = "Has dejado de la organización"
            else:
                mensaje = "El usuario ha sido expulsado de la organización"

            organizacion.equipos.remove(equipo)
            messages.success(request, mensaje)
        else:
            messages.error(request, "No hay una relación de miembro con el usuario")
    else:
        messages.error(request, "El usuario no existe")

    if request.session['equipo_organizacion'] == 0:
        return redirect('organizaciones:organizacion_equipos_jovenes')
    elif request.session['equipo_organizacion'] == 1:
        return redirect('equipos:detalle_equipo', id_equipo)
    elif request.session['equipo_organizacion'] == 2:
        return redirect('equipos:consultar_todos_los_equipos')



def usuarioPerfil(request) -> 'render':
    '''
        Permite a una Organización consultar y modificar su perfil

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    '''
    form = ''
    usuario = request.user

    if request.user.is_authenticated == False and request.user.tipo == 'Organización':
        messages.error(request, "Usted no está autorizado para realizar esta acción")
        return redirect('inicio')

    organizacion = Organizacion.objects.get(usuario_ptr_id=usuario.id)

    if request.method == 'GET':
        form = OrganizacionPerfilForm(instance=organizacion)

    if request.method == 'POST':
        form = OrganizacionPerfilForm(request.POST, request.FILES, instance=organizacion)

        if form.is_valid():
            usuario = form.save(commit=False)
            import os
            imagen_anterior = Usuario.objects.get(pk=form.instance.pk).foto_perfil
            if not request.POST.get('imagen-clear') is None and imagen_anterior:
                if os.path.isfile(imagen_anterior.path):
                    os.remove(imagen_anterior.path)
                usuario.foto_perfil = ""

            logo_anterior = Organizacion.objects.get(pk=form.instance.pk).logo
            if not request.POST.get('logo-clear') is None and logo_anterior:
                if os.path.isfile(logo_anterior.path):
                    os.remove(logo_anterior.path)
                usuario.logo = ""
            usuario.save()
            messages.success(request, "Perfil actualizado exitosamente")
            return redirect('organizaciones:modificar_perfil_organizacion')
        else:
            print(form.errors)
            messages.error(request, "Ha ocurrido un error. Por favor intente de nuevo.")
    from config.settings.base import MAP_WIDGETS

    GOOGLE_KEY = MAP_WIDGETS['GOOGLE_MAP_API_KEY']

    return render(request, 'organizaciones/perfil/modificar.html', {
        'form': form, 'pk_usuario': usuario.pk, 'direccion': get_object_or_404(Usuario, pk=int(usuario.pk)).direccion,
        'GOOGLE_KEY': GOOGLE_KEY
    })

def miPerfilActualDetalle(request):
    template_name = "organizaciones/perfil/detalle_actual.html"
    return render(request, template_name, {'organizacion': Organizacion.objects.get(pk=request.user.pk)})

class InvitarJovenesPlataforma(FormView, LoginRequiredMixin):

    form_class = FormularioInvitacionJoven
    template_name = 'organizaciones/jovenes/invitar_jovenes.html'

    def dispatch(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")

       
        return super(InvitarJovenesPlataforma, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        from kairos.gestor_correos.envio_correos import enviar_correo, INVITAR_JOVENES
        from kairos.organizaciones.models import SolicitudesOrganizacionJovenes, InformacionJovenInvitado, CohortesColegios
        from datetime import datetime
        import time

        if self.request.method == 'POST' and ('archivo_excel' in self.request.FILES or 'invitar_por_correo' in self.request.POST or 'invitar_eslabol' in self.request.POST):
            organizacion = Organizacion.objects.get(pk=self.request.user.id)
            
            if organizacion.tipo_organizacion.id == 4 or organizacion.tipo_organizacion.id == 8:
                if organizacion.tipo_organizacion.id == 4:
                    grado = self.request.POST['grado']
                else:
                    grado = 0
                fecha_induccion = datetime.strptime(self.request.POST['fecha_induccion'], '%Y-%m-%d').date()

            if 'archivo_excel' in self.request.FILES:
                import pandas as pd

                archivo = self.request.FILES['archivo_excel']        
                excel = pd.ExcelFile(archivo)
                hoja = excel.sheet_names[0]
                df = pd.read_excel(excel, hoja)
                lista_correos = df.iloc[:, 0].tolist()
                contador_correos = 0

                for correo in lista_correos:
                    if Joven.existe_joven_correo(correo):
                        joven = Joven.objects.get(email=correo)
                        if not SolicitudesOrganizacionJovenes.existe_solitud(organizacion=organizacion, joven=joven) and not organizacion.joven_es_miembro(joven):
                            organizacion.enviar_solicitud_miembro(joven)
                            enviar_correo(correo, organizacion, INVITAR_JOVENES, self.request.user)

                        if organizacion.tipo_organizacion.id == 4 or organizacion.tipo_organizacion.id == 8:
                            if not InformacionJovenInvitado.existe_registro(organizacion, correo):
                                if organizacion.tipo_organizacion.id == 8 and not Joven.joven_pertenece_colegio(joven):
                                    InformacionJovenInvitado.crear_registro(organizacion, correo, grado, fecha_induccion)
                                if organizacion.tipo_organizacion.id == 4:
                                    InformacionJovenInvitado.crear_registro(organizacion, correo, grado, fecha_induccion)
                    else:
                        if organizacion.tipo_organizacion.id == 4 or organizacion.tipo_organizacion.id == 8:
                            if contador_correos < 50:
                                if not InformacionJovenInvitado.existe_registro(organizacion, correo):
                                    enviar_correo(correo, organizacion, INVITAR_JOVENES, self.request.user)
                                    InformacionJovenInvitado.crear_registro(organizacion, correo, grado, fecha_induccion)
                                    contador_correos += 1
                            else:
                                time.sleep(70)
                                contador_correos = 0
                        else:
                            if contador_correos < 50:
                                enviar_correo(correo, organizacion, INVITAR_JOVENES, self.request.user)
                                contador_correos += 1
                            else:
                                time.sleep(70)
                                contador_correos = 0

                messages.success(self.request, 'Se han enviado las invitaciones desde la carga masiva correctamente')


            if 'invitar_por_correo' in self.request.POST and form.data['invitar_por_correo']:
                correos = self.request.POST['invitar_por_correo'].split(',')
                for correo in correos:
                    if organizacion.tipo_organizacion.id == 4 or organizacion.tipo_organizacion.id == 8:
                        if not InformacionJovenInvitado.existe_registro(organizacion, correo):
                            enviar_correo(correo, organizacion, INVITAR_JOVENES, self.request.user)
                            InformacionJovenInvitado.crear_registro(organizacion, correo, grado, fecha_induccion)
                    else:
                        enviar_correo(correo, organizacion, INVITAR_JOVENES, self.request.user)

                messages.success(self.request, 'Se han enviado las invitaciones desde los correos correctamente')

            if self.request.POST.getlist('invitar_eslabol'):
                lista_id = []
                for id_joven in self.request.POST['invitar_eslabol']:
                    lista_id.append(int(id_joven))

                jovenes = Joven.objects.filter(usuario_ptr_id__in=self.request.POST.getlist('invitar_eslabol'))

                organizacion = Organizacion.objects.get(id=self.request.user.id)
                for joven in jovenes:
                    if organizacion.tipo_organizacion.id == 4 or organizacion.tipo_organizacion.id == 8:
                        if not InformacionJovenInvitado.existe_registro(organizacion, joven.email):
                            if organizacion.tipo_organizacion.id == 8 and not Joven.joven_pertenece_colegio(joven):
                                InformacionJovenInvitado.crear_registro(organizacion, joven.email, grado, fecha_induccion)
                                organizacion.enviar_solicitud_miembro(joven)
                            if organizacion.tipo_organizacion.id == 4:
                                InformacionJovenInvitado.crear_registro(organizacion, joven.email, grado, fecha_induccion)
                                organizacion.enviar_solicitud_miembro(joven)         
                    else:
                        organizacion.enviar_solicitud_miembro(joven)

                messages.success(self.request, 'Se han enviado las invitaciones desde MiEslabón correctamente')

            if organizacion.tipo_organizacion.id == 4 or organizacion.tipo_organizacion.id == 8:
                if not CohortesColegios.existe_registro(organizacion, grado, fecha_induccion):
                    CohortesColegios.crear_registro(organizacion, grado, fecha_induccion)
        else:
            messages.error(self.request, 'Ha ocurrido un error al cargar el archivo, por favor intente de nuevo!')

        return redirect('organizaciones:organizacion_invitar_joven')



    def get_form_kwargs(self):
        from kairos.organizaciones.models import Organizacion

        kwargs = super(InvitarJovenesPlataforma, self).get_form_kwargs()
        kwargs['organizacion'] = Organizacion.objects.get(id=self.request.user.id)
        return kwargs

class AsignarOrganizacionJovenesGrado(LoginRequiredMixin, FormView):
    form_class = FormularioAsignarOrganizacionJovenGrado
    template_name = 'organizaciones/jovenes/asignar_jovenes_grado.html'

    def dispatch(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")

       
        return super(AsignarOrganizacionJovenesGrado, self).dispatch(request, *args, **kwargs)
    
    def form_valid(self, form):

        if self.request.method == 'POST' and 'archivo_excel' in self.request.FILES:

            from datetime import datetime    
            import pandas as pd

            id_organizacion = self.request.POST['organizacion']
            organizacion = Organizacion.buscar(int(id_organizacion))
            grado = self.request.POST['grado']
            fecha = datetime.strptime(self.request.POST['fecha_induccion'], '%Y-%m-%d').date()

            archivo = self.request.FILES['archivo_excel']        
            excel = pd.ExcelFile(archivo)
            hoja = excel.sheet_names[0]
            df = pd.read_excel(excel, hoja)
            lista_correos = df.iloc[:, 0].tolist()
            lista_no_creados = []

            for correo in lista_correos:
                print(correo)
                if Joven.existe_joven_correo(correo):
                    joven = Joven.objects.get(email=correo)
                    print('Antes',joven.ultimo_grado,',',joven.fecha_induccion)
                    if not organizacion.joven_es_miembro(joven):
                        organizacion.agregar_joven(joven)
                        joven.ultimo_grado = grado
                        joven.fecha_induccion = fecha
                        joven.save()
                    else:
                        joven.ultimo_grado = grado
                        joven.fecha_induccion = fecha
                        joven.save()
                    print('Despues',joven.ultimo_grado,',',joven.fecha_induccion)
                else:
                    lista_no_creados.append(correo)
            
            if not CohortesColegios.existe_registro(organizacion, grado, fecha):
                CohortesColegios.crear_registro(organizacion,grado,fecha)
            
            if len(lista_no_creados) > 0:

                wb = Workbook()
                ws = wb.active
                ws['A1'] = 'Correo'
                cont = 2

                for correo in lista_no_creados:
                    ws.cell(row=cont, column=1).value = correo
                    cont = cont + 1
                # Establecer nombre del archivo
                nombre_archivo = "jovenes_no_registrados.xlsx"
                # Definir que el tipo de respuesta a devolver es un archivo de microsoft excel
                response = HttpResponse(content_type="application/ms-excel")
                contenido = "attachment; filename={0}".format(nombre_archivo)
                response["Content-Disposition"] = contenido
                wb.save(response)

                messages.success(self.request, "Jovenes asignados correctamente")
                return response

            messages.success(self.request, "Jovenes asignados correctamente")
            
        else:
            messages.error(self.request, "Hubo un error")
        
        return redirect('usuarios:listado_usuarios')

class CrearSolicitudReto(LoginRequiredMixin, MensajeMixin, CreateView):
    model = SolicitudReto
    form_class = FormularioSolicitudReto
    template_name = 'organizaciones/retos/gestionar_retos.html'
    success_url = reverse_lazy('organizaciones:organizacion_gestion_solicitudes_retos')
    mensaje_exito = 'Solicitud enviada exitosamente'
    mensaje_error = 'Ha ocurrido un error. Por favor intente de nuevo!'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect('inicio')

        if not request.user.tipo == "Organización":
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect("inicio")

        return super(CrearSolicitudReto, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        from kairos.gestor_correos.envio_correos import enviar_correo, ENVIAR_SOLICITUD_RETOS

        form.instance.grupos_permitidos = form.cleaned_data['grupos_permitidos_lista']
        form.instance.organizacion = self.get_object()
        enviar_correo('contame@mieslabon.com', form.instance, ENVIAR_SOLICITUD_RETOS, self.request.user)
        return super(CrearSolicitudReto, self).form_valid(form)

    def get_object(self):
        return Organizacion.objects.get(pk=self.request.user.pk)


class RutasConsultar(LoginRequiredMixin, TemplateView):
    template_name = "organizaciones/rutas/gestionar_rutas.html"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and (request.user.tipo != 'Organización' or request.user.is_superuser):
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(RutasConsultar, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = dict()
        usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()
        context['rutas'] = consulta['rutas']
        context['tag'] = consulta['tag']
        context['tipos'] = [{'nombre': 'Mis Rutas'},
                        {'nombre': 'Favoritas'},
                        {'nombre': 'Todas las Rutas'}]

        return context

    def obtener_consulta(self):
        from kairos.core.utils import obtener_lista_usuario_caracteristicas
        from kairos.contenido.models import Contenido
        usuario = self.obtener_usuario()
        tag = 'Todas las Rutas'
        rutas = []

        if self.request.GET:
            if 'tag' in self.request.GET:
                tag = self.request.GET['tag']

        if tag == 'Mis Rutas':
            rutas = Contenido.obtener_contenidos_propias(self.request.user, 'ruta')
        elif tag == 'Favoritas':
            rutas = Contenido.favoritas(self.request.user, 'ruta')
        elif tag == 'Todas las Rutas':
            rutas = Contenido.objects.select_related('modalidad').prefetch_related('niveles_conocimiento', 'roles', 'tribus_relacionadas').filter(tipo='ruta', activo=True)


        return {'tag': tag, 'rutas': rutas}

    def obtener_usuario(self):
        usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario

class OportunidadesConsultar(LoginRequiredMixin, ListView):
    template_name = "organizaciones/oportunidades/gestion_oportuniades.html"
    paginate_by = 12

    def get_queryset(self):
        consulta = self.obtener_consulta()
        return consulta['oportunidades']

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and (request.user.tipo != 'Organización' or request.user.is_superuser):
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(OportunidadesConsultar, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator
        context = super(OportunidadesConsultar, self).get_context_data(**kwargs)
        usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()

        pagination = Paginator(list(consulta['oportunidades']), self.paginate_by)

        context['oportunidades'] = pagination.page(context['page_obj'].number)
        context['tag'] = consulta['tag']
        context['tipos'] = [{'nombre': 'Mis Oportunidades'},
                        {'nombre': 'Favoritas'},
                        {'nombre': 'Todas las Oportunidades'}]

        return context

    def obtener_consulta(self):
        from kairos.core.utils import obtener_lista_usuario_caracteristicas
        from kairos.contenido.models import Contenido
        usuario = self.obtener_usuario()
        tag = 'Todas las Oportunidades'
        oportunidades = []

        if 'tag' in self.request.GET:
            tag = self.request.GET['tag']
        elif list(self.request.GET):
            tag = list(self.request.GET)[0]
        else:
            tag = "Todas las Oportunidades"

        if tag == 'Mis Oportunidades':
            oportunidades = Contenido.obtener_contenidos_propias(usuario, 'oportunidad')
        elif tag == 'Favoritas':
            oportunidades = Contenido.favoritas(usuario, 'oportunidad')
        elif tag == 'Todas las Oportunidades':
            oportunidades = Contenido.objects.select_related('modalidad').prefetch_related('niveles_conocimiento', 'roles', 'tribus_relacionadas').filter(tipo='oportunidad', activo=True)

        return {'tag': tag, 'oportunidades': oportunidades}

    def obtener_usuario(self):
        usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario


class GestionDeSolicitudesRetos(LoginRequiredMixin, ListView):
    model = SolicitudReto
    context_object_name = "solicitudes_retos"
    template_name = "organizaciones/retos/gestionar_solicitudes_retos.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated and request.user.tipo != 'Organización':
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(GestionDeSolicitudesRetos, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        organizacion = self.request.user
        solicitudes = SolicitudReto.objects.filter(organizacion=organizacion)
        return solicitudes



