# Modulos Django
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError

# Modulos de plugin externos
from cities_light.models import Country, City, Region
from config.settings.base import BASE_DIR
from django_select2.forms import Select2MultipleWidget, Select2Widget, ModelSelect2Widget, ModelSelect2MultipleWidget
from tempus_dominus.widgets import DatePicker
from PIL import Image, ExifTags
import os

# Modulos de otras apps
from kairos.insignias.models import RutaConocimiento, NivelesDeConocimiento, Roles, Familia
from kairos.retos.models import SolicitudReto

# Modulos internos de la app
from .models import *


class DependentMultipleSelectWidget(ModelSelect2MultipleWidget):
    def filter_queryset(self, request, term, queryset=None, **dependent_fields):        
        # The super code ignores the dependent fields with multiple values
        dependent_fields_multi_value = {}
        for form_field_name, model_field_name in self.dependent_fields.items():
            if form_field_name+"[]" in request.GET and request.GET.get(form_field_name+"[]", "") != "":
                value_list = request.GET.getlist(form_field_name+"[]")
                dependent_fields_multi_value[model_field_name] = value_list
                print(value_list)

        if queryset is None:
            queryset = self.get_queryset()
        search_fields = self.get_search_fields()
        select = Q()
        term = term.replace('\t', ' ')
        term = term.replace('\n', ' ')
        for t in [t for t in term.split(' ') if not t == '']:
            select &= reduce(lambda x, y: x | Q(**{y: t}), search_fields,
                             Q(**{search_fields[0]: t}))
        
        # TODO - Q doesnt support list of values, will have to add something of our own
       
        filtro = dict()
        for atributo, valor in dependent_fields_multi_value.items():
            filtro["{}__in".format(atributo)] = valor
        ################################################################################
        
        if dependent_fields:
            select &= Q(**dependent_fields)
        if dependent_fields_multi_value:     
            queryset = queryset.filter(**filtro)

        return queryset.filter(select).distinct()

TIPOS_ID_ORGANIZACION = (
    ("NIT", "NIT"),
)

class OrganizacionModelForm(UserCreationForm):
    tipo_identificacion=forms.ChoiceField(
        choices=TIPOS_ID_ORGANIZACION,
        initial='NIT',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    insignias_cuisana_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="cuisana"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="cuisana"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    ) 
    insignias_moterra_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="moterra"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="moterra"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_ponor_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="ponor"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="ponor"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_vincularem_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="vincularem"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="vincularem"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_brianimus_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="brianimus"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="brianimus"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_gronor_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="gronor"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="gronor"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_centeros_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="centeros"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="centeros"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_sersas_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="sersas"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="sersas"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )

    def __init__(self, *args, **kwargs):
        super(OrganizacionModelForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['email'].required = True
        self.fields['first_name'].label = "Nombres*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['password1'].required = True
        self.fields['password2'].required = True
        self.fields['password1'].widget.attrs['autocomplete'] = 'off'
        self.fields['password2'].widget.attrs['autocomplete'] = 'off'
        self.fields['password1'].widget.render_value = True
        self.fields['password2'].widget.render_value = True
        self.fields['password1'].label = "Contraseña nueva*"
        self.fields['password2'].label = "Confirmar contraseña nueva*"
        self.fields['is_active'].label = "¿La organización tiene acceso a la plataforma?*"

        self.fields['is_active'].help_text = ""
        self.fields['password1'].help_text = "- La contraseña debe de ser alfanumérica y tener mínimo 8 caracteres <br>- No puede ser una contraseña usada comúnmente o similar a los datos personales"
        self.fields['password2'].help_text = "Introduzca la misma contraseña"
        self.fields["tipo_organizacion"].queryset = TipoOrganizacion.objects.filter(activo=True)
        self.fields["niveles_conocimiento"].queryset = NivelesDeConocimiento.objects.filter(activo=True)
        self.fields["roles"].queryset = Roles.objects.filter(activo=True)
        self.fields["insignias_cuisana"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="cuisana")
        self.fields["insignias_moterra"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="moterra")
        self.fields["insignias_ponor"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="ponor")
        self.fields["insignias_vincularem"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="vincularem")        
        self.fields["insignias_brianimus"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="brianimus")
        self.fields["insignias_gronor"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="gronor")
        self.fields["insignias_centeros"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="centeros")
        self.fields["insignias_sersas"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="sersas")  
        self.fields["insignias_cuisana"].required = False
        self.fields["insignias_moterra"].required = False
        self.fields["insignias_ponor"].required = False
        self.fields["insignias_vincularem"].required = False
        self.fields["insignias_brianimus"].required = False
        self.fields["insignias_gronor"].required = False
        self.fields["insignias_centeros"].required = False
        self.fields["insignias_sersas"].required = False

    class Meta:
        model = Organizacion
        fields = ('tipo_organizacion', 'first_name', 'tipo_identificacion', 'identificacion', 'email',
                  'contacto', 'telefono', 'telefono_fijo', 'pais', 'region', 'ciudad', 'direccion','pagina_web',
                  'password1', 'password2', 'niveles_conocimiento', 'roles', 'cuisana', 'moterra', 'ponor',
                  'vincularem', 'brianimus', 'gronor', 'centeros', 'sersas', 'insignias_cuisana', 'insignias_moterra',
                  'insignias_ponor', 'insignias_vincularem', 'insignias_brianimus', 'insignias_gronor',
                  'insignias_centeros', 'insignias_sersas', 'is_active', 'insignias_cuisana_familia', 'insignias_moterra_familia',
                  'insignias_ponor_familia', 'insignias_vincularem_familia', 'insignias_brianimus_familia', 'insignias_gronor_familia',
                  'insignias_centeros_familia', 'insignias_sersas_familia')
        widgets = {
            'tipo_organizacion': Select2Widget(),
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            ),
            
            'insignias_cuisana': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_cuisana_familia': 'familia_id'},                
            ),

            'insignias_moterra': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_moterra_familia': 'familia_id'},                
            ),

            'insignias_ponor': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_ponor_familia': 'familia_id'},                
            ),

            'insignias_vincularem': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_vincularem_familia': 'familia_id'},                
            ),

            'insignias_brianimus': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_brianimus_familia': 'familia_id'},                
            ),

            'insignias_gronor': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_gronor_familia': 'familia_id'},                
            ),

            'insignias_centeros': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_centeros_familia': 'familia_id'},                
            ),

            'insignias_sersas': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_sersas_familia': 'familia_id'},                
            ),
            
            'sub_organizaciones': Select2MultipleWidget(), 
            
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        if Usuario.objects.filter(email=email).exists():
            raise ValidationError('Este correo ya existe!')
        return email.lower()

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = super(OrganizacionModelForm, self).clean_password2()
        if bool(password1) ^ bool(password2):
            raise forms.ValidationError("Fill out both fields")
        return password2

class OrganizacionModificarForm(forms.ModelForm):
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_ORGANIZACION,
        initial='NIT',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    insignias_cuisana_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="cuisana"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="cuisana"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    ) 
    insignias_moterra_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="moterra"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="moterra"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_ponor_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="ponor"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="ponor"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_vincularem_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="vincularem"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="vincularem"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_brianimus_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="brianimus"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="brianimus"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_gronor_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="gronor"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="gronor"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_centeros_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="centeros"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="centeros"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )
    insignias_sersas_familia = forms.ModelMultipleChoiceField(
        queryset=Familia.objects.filter(tribu__nombre__icontains="sersas"),
        widget=DependentMultipleSelectWidget(
            queryset=Familia.objects.filter(tribu__nombre__icontains="sersas"),
            search_fields=['nombre__icontains', 'id__icontains'],            
        ),          
        required=False,
        label="Familias"
    )

    def __init__(self, *args, **kwargs):
        super(OrganizacionModificarForm, self).__init__(*args, **kwargs)
        from django.db.models import Q
        self.fields['first_name'].required = True
        self.fields['first_name'].label = "Nombres*"
        self.fields['is_active'].label = "¿La organización tiene acceso a la plataforma?*"
        self.fields['is_active'].help_text = ""
        self.fields["tipo_organizacion"].queryset = TipoOrganizacion.objects.filter(activo=True)
        self.fields["niveles_conocimiento"].queryset = NivelesDeConocimiento.objects.filter(activo=True)
        self.fields["roles"].queryset = Roles.objects.filter(activo=True)

        self.fields['insignias_cuisana_familia'].initial = Familia.objects.filter(rutas_de_conocimiento_de_la_familia__in=self.instance.insignias_cuisana.all())
        self.fields['insignias_moterra_familia'].initial = Familia.objects.filter(rutas_de_conocimiento_de_la_familia__in=self.instance.insignias_moterra.all())
        self.fields['insignias_ponor_familia'].initial = Familia.objects.filter(rutas_de_conocimiento_de_la_familia__in=self.instance.insignias_ponor.all())
        self.fields['insignias_vincularem_familia'].initial = Familia.objects.filter(rutas_de_conocimiento_de_la_familia__in=self.instance.insignias_vincularem.all())
        self.fields['insignias_brianimus_familia'].initial = Familia.objects.filter(rutas_de_conocimiento_de_la_familia__in=self.instance.insignias_brianimus.all())
        self.fields['insignias_gronor_familia'].initial = Familia.objects.filter(rutas_de_conocimiento_de_la_familia__in=self.instance.insignias_gronor.all())
        self.fields['insignias_centeros_familia'].initial = Familia.objects.filter(rutas_de_conocimiento_de_la_familia__in=self.instance.insignias_centeros.all())
        self.fields['insignias_sersas_familia'].initial = Familia.objects.filter(rutas_de_conocimiento_de_la_familia__in=self.instance.insignias_sersas.all())   

        self.fields["insignias_cuisana"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="cuisana")
        self.fields["insignias_moterra"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="moterra")
        self.fields["insignias_ponor"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="ponor")
        self.fields["insignias_vincularem"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="vincularem")
        self.fields["insignias_brianimus"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="brianimus")
        self.fields["insignias_gronor"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="gronor")
        self.fields["insignias_centeros"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="centeros")
        self.fields["insignias_sersas"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="sersas")
        self.fields["insignias_cuisana"].required = False
        self.fields["insignias_moterra"].required = False
        self.fields["insignias_ponor"].required = False
        self.fields["insignias_vincularem"].required = False
        self.fields["insignias_brianimus"].required = False
        self.fields["insignias_gronor"].required = False
        self.fields["insignias_centeros"].required = False
        self.fields["insignias_sersas"].required = False
        self.fields["sub_organizaciones"].queryset = Organizacion.objects.exclude(Q(pk=self.instance.pk) |  Q(pk__in=self.instance.organizaciones.all()))

    class Meta:
        model = Organizacion
        fields = ('tipo_organizacion', 'first_name', 'tipo_identificacion', 'identificacion', 'email',
                  'contacto', 'telefono', 'telefono_fijo', 'pais', 'region', 'ciudad', 'direccion', 'pagina_web',
                  'niveles_conocimiento', 'roles', 'cuisana', 'moterra', 'ponor', 'sersas',
                  'vincularem', 'brianimus', 'gronor', 'centeros', 'insignias_cuisana', 'insignias_moterra',
                  'insignias_ponor', 'insignias_vincularem', 'insignias_brianimus', 'insignias_gronor',
                  'insignias_centeros', 'insignias_sersas', 'sub_organizaciones', 'is_active', 
                  'insignias_cuisana_familia', 'insignias_moterra_familia', 'insignias_ponor_familia', 
                  'insignias_vincularem_familia', 'insignias_brianimus_familia', 'insignias_gronor_familia',
                  'insignias_centeros_familia', 'insignias_sersas_familia')
        widgets = {
            'tipo_organizacion': Select2Widget(),
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            ),
            'insignias_cuisana': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_cuisana_familia': 'familia_id'},                
            ),

            'insignias_moterra': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_moterra_familia': 'familia_id'},                
            ),

            'insignias_ponor': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_ponor_familia': 'familia_id'},                
            ),

            'insignias_vincularem': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_vincularem_familia': 'familia_id'},                
            ),

            'insignias_brianimus': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_brianimus_familia': 'familia_id'},                
            ),

            'insignias_gronor': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_gronor_familia': 'familia_id'},                
            ),

            'insignias_centeros': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_centeros_familia': 'familia_id'},                
            ),

            'insignias_sersas': DependentMultipleSelectWidget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains', 'id__icontains'],
                dependent_fields={'insignias_sersas_familia': 'familia_id'},                
            ),
            'sub_organizaciones': Select2MultipleWidget(),
            
        }

    def clean_email(self):
        pk_usuario = self.instance.pk
        usuario = Usuario.objects.get(id=pk_usuario)
        email = self.cleaned_data['email']
        email.lower()
        usuarios = Usuario.objects.filter(email=email)
        if usuarios.exists():
            if not usuario in usuarios:
                raise ValidationError('Este correo ya existe!')
        return email


# ##################USER VIEW#####################
class OrganizacionRegistroForm(UserCreationForm):
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_ORGANIZACION,
        initial='NIT',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    def __init__(self, *args, **kwargs):
        super(OrganizacionRegistroForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['email'].required = True

        self.fields['first_name'].label = "Nombres*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['password1'].label = "Contraseña nueva*"
        self.fields['password2'].label = "Confirmar contraseña nueva*"

        self.fields['password1'].help_text = "- La contraseña debe de ser alfanumérica y tener mínimo 8 caracteres " \
                                             "<br>- No puede ser una contraseña usada comúnmente o similar a los " \
                                             "datos personales"
        self.fields['password2'].help_text = "Introduzca la misma contraseña"
        self.fields["niveles_conocimiento"].queryset = NivelesDeConocimiento.objects.filter(activo=True)
        self.fields["roles"].queryset = Roles.objects.filter(activo=True)
        self.fields["tipo_organizacion"].queryset = TipoOrganizacion.objects.filter(activo=True)
        self.fields["insignias_cuisana"].queryset = RutaConocimiento.objects.filter(tribu__nombre__icontains="cuisana")
        self.fields["insignias_moterra"].queryset = RutaConocimiento.objects.filter(tribu__nombre__icontains="moterra")
        self.fields["insignias_ponor"].queryset = RutaConocimiento.objects.filter(tribu__nombre__icontains="ponor")
        self.fields["insignias_vincularem"].queryset = RutaConocimiento.objects.filter(
            tribu__nombre__icontains="vincularem")
        self.fields["insignias_brianimus"].queryset = RutaConocimiento.objects.filter(
            tribu__nombre__icontains="brianimus")
        self.fields["insignias_gronor"].queryset = RutaConocimiento.objects.filter(tribu__nombre__icontains="gronor")
        self.fields["insignias_centeros"].queryset = RutaConocimiento.objects.filter(
            tribu__nombre__icontains="centeros")
        self.fields["insignias_cuisana"].required = False
        self.fields["insignias_moterra"].required = False
        self.fields["insignias_ponor"].required = False
        self.fields["insignias_vincularem"].required = False
        self.fields["insignias_brianimus"].required = False
        self.fields["insignias_gronor"].required = False
        self.fields["insignias_centeros"].required = False
        self.fields['password1'].required = True
        self.fields['password2'].required = True


    class Meta:
        model = Organizacion
        fields = ('tipo_organizacion', 'first_name', 'tipo_identificacion', 'identificacion', 'email', 'pais',
                  'region', 'ciudad', 'niveles_conocimiento', 'roles', 'cuisana', 'moterra', 'ponor',
                  'vincularem', 'brianimus', 'gronor', 'centeros', 'insignias_cuisana', 'insignias_moterra',
                  'insignias_ponor', 'insignias_vincularem', 'insignias_brianimus', 'insignias_gronor',
                  'insignias_centeros', 'password1', 'password2')
        widgets = {
            'tipo_organizacion': Select2Widget(),
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            ),

            'insignias_cuisana': Select2MultipleWidget(), 'insignias_moterra': Select2MultipleWidget(),
            'insignias_ponor': Select2MultipleWidget(), 'insignias_vincularem': Select2MultipleWidget(),
            'insignias_brianimus': Select2MultipleWidget(), 'insignias_gronor': Select2MultipleWidget(),
            'insignias_centeros': Select2MultipleWidget()
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        if Usuario.objects.filter(email=email).exists():
            raise ValidationError('Este correo ya existe!')
        return email.lower()

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre


class OrganizacionPerfilForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput(), required=False)
    y = forms.FloatField(widget=forms.HiddenInput(), required=False)
    width = forms.FloatField(widget=forms.HiddenInput(), required=False)
    height = forms.FloatField(widget=forms.HiddenInput(), required=False)
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_ORGANIZACION,
        initial='NIT',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    def __init__(self, *args, **kwargs):
        super(OrganizacionPerfilForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True

        self.fields['first_name'].label = "Nombres*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields["niveles_conocimiento"].queryset = NivelesDeConocimiento.objects.filter(activo=True)
        self.fields["roles"].queryset = Roles.objects.filter(activo=True)
        self.fields["insignias_cuisana"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="cuisana")
        self.fields["insignias_moterra"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="moterra")
        self.fields["insignias_ponor"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="ponor")
        self.fields["insignias_vincularem"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="vincularem")
        self.fields["insignias_brianimus"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="brianimus")
        self.fields["insignias_gronor"].queryset = RutaConocimiento.objects.filter(familia__tribu__nombre__icontains="gronor")
        self.fields["insignias_centeros"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="centeros")
        self.fields["insignias_sersas"].queryset = RutaConocimiento.objects.filter(
            familia__tribu__nombre__icontains="sersas")
        self.fields["insignias_cuisana"].required = False
        self.fields["insignias_moterra"].required = False
        self.fields["insignias_ponor"].required = False
        self.fields["insignias_vincularem"].required = False
        self.fields["insignias_brianimus"].required = False
        self.fields["insignias_gronor"].required = False
        self.fields["insignias_centeros"].required = False
        self.fields["insignias_sersas"].required = False


    class Meta:
        model = Organizacion
        fields = ('foto_perfil', 'first_name', 'email', 'tipo_identificacion', 'identificacion', 'pais',
                  'region', 'ciudad', 'telefono', 'indicativo_telefono', 'telefono_fijo', 'pagina_web',
                  'niveles_conocimiento', 'roles', 'cuisana', 'moterra', 'ponor', 'vincularem', 'brianimus', 'gronor',
                  'centeros', 'insignias_cuisana', 'insignias_moterra', 'insignias_ponor', 'insignias_vincularem',
                  'insignias_brianimus', 'insignias_gronor', 'insignias_centeros', 'insignias_sersas', 'terminos', 'notificaciones',
                  'x', 'y', 'width', 'height', 'para_que_existimos', 'que_ofrecemos', 'que_jovenens_buscamos', 'facebook',
                  'instagram', 'twitter', 'logo'
                  )
        widgets = {

            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            ),
            'para_que_existimos': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'que_ofrecemos': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'que_jovenens_buscamos': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),

            'insignias_cuisana': Select2MultipleWidget(), 'insignias_moterra': Select2MultipleWidget(),
            'insignias_ponor': Select2MultipleWidget(), 'insignias_vincularem': Select2MultipleWidget(),
            'insignias_brianimus': Select2MultipleWidget(), 'insignias_gronor': Select2MultipleWidget(),
            'insignias_centeros': Select2MultipleWidget()
        }

    def clean_email(self):
        pk_usuario = self.instance.pk
        usuario = Usuario.objects.get(id=pk_usuario)
        email = self.cleaned_data['email']
        email.lower()
        usuarios = Usuario.objects.filter(email=email)
        if usuarios.exists():
            if not usuario in usuarios:
                raise ValidationError('Este correo ya existe!')
        return email

    def save(self, commit=True):
        usuario = super(OrganizacionPerfilForm, self).save(commit=False)
        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        if not x is None and not y is None:
            w = self.cleaned_data.get('width')
            h = self.cleaned_data.get('height')
            imagen = Image.open(usuario.foto_perfil)
            try:
                imagen_anterior = Usuario.objects.get(pk=self.instance.pk).foto_perfil
                otras = Usuario.objects.filter(foto_perfil=imagen_anterior.url)
                if os.path.isfile(imagen_anterior.path) and len(otras) <= 1:
                    os.remove(imagen_anterior.path)
                for orientation in ExifTags.TAGS.keys():
                    if ExifTags.TAGS[orientation] == 'Orientation':
                        break
                exif = dict(imagen._getexif().items())

                if exif[orientation] == 3:
                    imagen = imagen.rotate(180, expand=True)
                elif exif[orientation] == 6:
                    imagen = imagen.rotate(270, expand=True)
                elif exif[orientation] == 8:
                    imagen = imagen.rotate(90, expand=True)
            except:
                pass

            imagen_recortada = imagen.crop((x, y, w + x, h + y))
            redimensionada = imagen_recortada.resize((600, 600), Image.ANTIALIAS)
            path = os.path.join(os.path.dirname(BASE_DIR), "kairos", "media", "profile_pics", usuario.foto_perfil.name)
            usuario.save()
            if os.path.isfile(usuario.foto_perfil.path):
                path = usuario.foto_perfil.path
                os.remove(usuario.foto_perfil.path)
            redimensionada.save(path)
        return usuario



class FormularioInvitacionJoven(forms.Form):

    cargue_masivo = forms.FileField(label='Cargue masivo', required=False)
    invitar_por_correo = forms.CharField(
        required=False,
        label="Invitar por correo"
    )

    def __init__(self, *args, **kwargs):
        organizacion = kwargs.pop('organizacion')
        super(FormularioInvitacionJoven, self).__init__(*args, **kwargs)

        lista = []
        for joven in organizacion.obtener_jovenes_no_miembros():
            lista.append((joven, joven))

        self.fields["invitar_eslabol"] = forms.ModelMultipleChoiceField(widget=Select2MultipleWidget(),
                                                                        queryset=organizacion.obtener_jovenes_no_miebros_ni_invitados(), required=False, label='Invitar desde MiEslabón')
        
        if organizacion.tipo_organizacion.id == 4:
            GRADOS = (
            ("1", "1"),
            ("2", "2"),
            ("3", "3"),
            ("4", "4"),
            ("5", "5"),
            ("6", "6"),
            ("7", "7"),
            ("8", "8"),
            ("9", "9"),
            ("10", "10"),
            ("11", "11"),
            ("12", "12")
            )

            self.fields["grado"] = forms.ChoiceField(
                                    choices=GRADOS,
                                    required=True,
                                    label="Grado al que pertenecen los jóvenes"
                                    )
            
            self.fields["fecha_induccion"] = forms.DateField(
                                                widget=DatePicker(
                                                    options={
                                                        'locale': 'es',
                                                        'format': 'YYYY-MM-DD',
                                                    },
                                                    attrs={
                                                        'append': 'fa fa-calendar',
                                                        'icon_toggle': False,
                                                    },
                                                ),
                                                label="Fecha estimada de ingreso a la plataforma",
                                                required=True,
                                            )
        if organizacion.tipo_organizacion.id == 8:
            
            self.fields["fecha_induccion"] = forms.DateField(
                                                widget=DatePicker(
                                                    options={
                                                        'locale': 'es',
                                                        'format': 'YYYY-MM-DD',
                                                    },
                                                    attrs={
                                                        'append': 'fa fa-calendar',
                                                        'icon_toggle': False,
                                                    },
                                                ),
                                                label="Fecha estimada de ingreso a la plataforma",
                                                required=True,
                                            )



    class Meta:
        fields = ('invitar_eslabol', 'grado', 'fecha_induccion')

class FormularioAsignarOrganizacionJovenGrado(forms.Form):

    cargue_masivo = forms.FileField(label='Cargue masivo', required=False)

    def __init__(self, *args, **kwargs):

        super(FormularioAsignarOrganizacionJovenGrado, self).__init__(*args, **kwargs)


        self.fields["organizacion"] = forms.ModelChoiceField(widget=Select2Widget(),
                                                            queryset=Organizacion.obtener_colegios(), 
                                                            required=False, 
                                                            label='Organizacion tipo colegio')
        
        GRADOS = (
        ("1", "1"),
        ("2", "2"),
        ("3", "3"),
        ("4", "4"),
        ("5", "5"),
        ("6", "6"),
        ("7", "7"),
        ("8", "8"),
        ("9", "9"),
        ("10", "10"),
        ("11", "11"),
        ("12", "12")
        )

        self.fields["grado"] = forms.ChoiceField(
                                choices=GRADOS,
                                required=False,
                                label="Grado al que pertenecen los jóvenes"
                                )
        
        self.fields["fecha_induccion"] = forms.DateField(
                                            widget=DatePicker(
                                                options={
                                                    'locale': 'es',
                                                    'format': 'YYYY-MM-DD',
                                                },
                                                attrs={
                                                    'append': 'fa fa-calendar',
                                                    'icon_toggle': False,
                                                },
                                            ),
                                            label="Fecha estimada de ingreso a la plataforma",
                                            required=True,
                                        )
    class Meta:
        fields = ('organizacion', 'grado', 'fecha_induccion')


class FormularioSolicitudReto(forms.ModelForm):
    GRUPOS = (
        ("Jóvenes", "Jóvenes"),
        ("Equipos", "Equipos")
        )
    grupos_permitidos_lista = forms.MultipleChoiceField(
        choices=GRUPOS,
        widget=Select2MultipleWidget(),
        required=True,
        label="¿Quiénes pueden inscribirse al reto?*"
    )
    def __init__(self, *args, **kwargs):
        super(FormularioSolicitudReto, self).__init__(*args, **kwargs)

        # self.fields['organizacion'].querryset=

    class Meta:
        model = SolicitudReto
        fields = ('nombre_responsable', 'correo', 'telefono', 'ciudad', 'grupos_permitidos_lista', 'mensaje')
        widgets = {

            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                max_results=100,
            ),
            'mensaje': forms.Textarea(
                attrs={
                    'rows': 5,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'grupos_permitidos_lista': Select2MultipleWidget(),

        }


class PostulacionLiderOrganizacionForm(forms.ModelForm):
    from kairos.equipos.models import Equipo

    class Meta:
        model = Organizacion
        fields = ["solicitudes_equipos"]

        widgets = {
            'solicitudes_equipos': Select2MultipleWidget(
            ),
        }

    def __init__(self, *args, **kwargs):
        lider = kwargs.pop("joven_lider")
        super(PostulacionLiderOrganizacionForm, self).__init__(*args, **kwargs)
        miembros_organizacion = self.instance.equipos.all()
        self.fields["solicitudes_equipos"].queryset = Equipo.objects.filter(lider=lider).exclude(id__in=miembros_organizacion)

class AsignarJovenesAOrganizacionForm(forms.ModelForm):
    from kairos.organizaciones.models import Organizacion
    from kairos.usuarios.models import Joven

    class Meta:
        model = Organizacion
        fields = ["jovenes"]

        widgets = {
            'jovenes': ModelSelect2MultipleWidget(
                queryset=Joven.objects.all(),
                search_fields=['email__icontains', 'first_name__icontains', 'last_name__icontains'],
                max_results=100,
            ),
        }

    def __init__(self, *args, **kwargs):
        super(AsignarJovenesAOrganizacionForm, self).__init__(*args, **kwargs)
        miembros_organizacion = self.instance.jovenes.all()
        #self.fields["jovenes"].queryset = Joven.objects.exclude(id__in=miembros_organizacion)
        self.fields["jovenes"].label = 'Jóvenes para asignar'