def transformar_estructura_graficos(lista_datos, diccionario_categorias, valor_uno, valor_dos, key_grafico, lista=False):

    aux_diccionario = dict()
    for registro in lista_datos:
        
        categoria = diccionario_categorias[registro[valor_uno]]

        if str(registro[valor_dos]) in aux_diccionario:
            aux_diccionario[str(registro[valor_dos])].append({categoria:registro['total']})
        else:
            aux_diccionario[str(registro[valor_dos])] = list()
            aux_diccionario[str(registro[valor_dos])].append({categoria:registro['total']})

    lista_retorno = []    
    print(aux_diccionario)
    for categoria, datos in aux_diccionario.items():
        dato_uno, cantidad_uno = zip(*datos[0].items()) 
        if len(datos) != 2:
            if dato_uno[0] == 'si':
                datos.append({'no':0})
            else:
                datos.append({'si':0})
        dato_dos, cantidad_dos = zip(*datos[1].items())
        if lista == True:
            if int(categoria) < len(key_grafico[1]):
                lista_retorno.append({key_grafico[0]:key_grafico[1][int(categoria)], dato_uno[0]:cantidad_uno[0], dato_dos[0]:cantidad_dos[0]})
        else:
            lista_retorno.append({key_grafico:categoria, dato_uno[0]:cantidad_uno[0], dato_dos[0]:cantidad_dos[0]})
    
    return lista_retorno
