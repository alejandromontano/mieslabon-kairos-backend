# Modulos Django
from datetime import datetime
from itertools import count

from django.db import models
from django.db.models import Q, Prefetch, Count
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator

# Modulos de plugin externos
from cities_light.models import Country, City, Region
from simple_history.models import HistoricalRecords

# Modulos de otras apps
from kairos.usuarios.models import Usuario, Joven
from kairos.insignias.models import Tribus, Roles, NivelesDeConocimiento, RutaConocimiento
from kairos.perfiles_deseados.models import PerfilesDeseados
from kairos.equipos.models import Equipo
from .utils import transformar_estructura_graficos
from kairos.contenido.models import CalificacionContenido, Contenido
from kairos.formularios.models import Formularios, SeccionVisitada, CompetenciaVisitada
from kairos.respuestas_formularios.models import RespuestasJoven

# Modulos internos de la app
from kairos.guia.models import VerRecurso, Recurso
from kairos.habilidades_del_futuro.models import HabilidadVisitada

CONSULTA_JOVENES = Joven.objects.none()


class TipoOrganizacion(models.Model):
    nombre = models.CharField(max_length=50, verbose_name='nombre del tipo*')
    descripcion = models.CharField(max_length=300, verbose_name="descripción*")
    activo = models.BooleanField(verbose_name='¿El tipo de organización se encuentra activo?', default=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre


def crear_ruta_logo(instance, filename):
    return "logos/%s" % (filename.encode('ascii', 'ignore'))


class Organizacion(Usuario):
    sub_organizaciones = models.ManyToManyField('Organizacion', blank=True, related_name='organizaciones')
    tipo_organizacion = models.ForeignKey(TipoOrganizacion, verbose_name='tipo de organización', null=True, blank=True,
                                          on_delete=models.CASCADE)
    logo = models.FileField(upload_to=crear_ruta_logo, null=True, blank=True)
    pagina_web = models.CharField(max_length=100, verbose_name='página web', null=True, blank=True)
    jovenes = models.ManyToManyField('usuarios.Joven', related_name='organizaciones_joven',
                                     verbose_name="jóvenes en la organización")
    equipos = models.ManyToManyField('equipos.Equipo', related_name='organizaciones_equipo',
                                     verbose_name="Equipos en la organización")
    solicitudes = models.ManyToManyField(Joven, through='SolicitudesOrganizacionJovenes',
                                         related_name="organizaciones_solicitudes", blank=True,
                                         verbose_name="personas que van a ser parte del la organización")
    solicitudes_equipos = models.ManyToManyField(Equipo, through='SolicitudesOrganizacionEquipos',
                                                 related_name="organizaciones_solicitudes_equipos", blank=True,
                                                 verbose_name="Equipos que van a ser parte del la organización")
    puntaje = models.DecimalField(validators=[MinValueValidator(0.0)], verbose_name="escala máxima (si aplica)",
                                  null=True, blank=True, decimal_places=2, max_digits=5)

    contacto = models.CharField(max_length=100, verbose_name='contacto', null=True, blank=True)

    niveles_conocimiento = models.ManyToManyField('insignias.NivelesDeConocimiento',
                                                  verbose_name="niveles de conocimiento", blank=True)
    roles = models.ManyToManyField('insignias.Roles', verbose_name="roles", blank=True)
    # TRIBUS
    tribus_relacionadas = models.ManyToManyField('insignias.Tribus', related_name='tribus_de_la_organizacion')
    es_principal = models.BooleanField(
        default=False)  # ----> ATRIBUTO DISEÑADO PARA DIEFRENCIAR LA ORGANIZACION "MIESLABON" DE LAS DEMAS
    cuisana = models.BooleanField(verbose_name="cuisana", default=False)
    moterra = models.BooleanField(verbose_name="moterra", default=False)
    ponor = models.BooleanField(verbose_name="ponor", default=False)
    vincularem = models.BooleanField(verbose_name="vincularem", default=False)
    brianimus = models.BooleanField(verbose_name="brianimus", default=False)
    gronor = models.BooleanField(verbose_name="gronor", default=False)
    centeros = models.BooleanField(verbose_name="centeros", default=False)
    sersas = models.BooleanField(verbose_name="sersas", default=False)
    insignias_cuisana = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                               related_name='insignias_cuisana_organizacion')
    insignias_moterra = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                               related_name='insignias_moterra_organizacion')
    insignias_ponor = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                             related_name='insignias_ponor_organizacion')
    insignias_vincularem = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                                  related_name='insignias_vincularem_organizacion')
    insignias_brianimus = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                                 related_name='insignias_brianimus_organizacion')
    insignias_gronor = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                              related_name='insignias_gronor_organizacion')
    insignias_centeros = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                                related_name='insignias_centeros_organizacion')
    insignias_sersas = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                              related_name='insignias_sersas_organizacion')
    para_que_existimos = models.TextField(null=True, blank=True, verbose_name='*¿Para qué existimos?')
    que_ofrecemos = models.TextField(null=True, blank=True, verbose_name='*¿Qué te ofrecemos?')
    que_jovenens_buscamos = models.TextField(null=True, blank=True, verbose_name='*¿Qué jóvenes buscamos?')
    telefono_fijo_regex = RegexValidator(regex=r'^\d{7,12}$',
                                         message="El número de teléfono fijo debe tener entre 7 y 12 dígitos.")
    telefono_fijo = models.CharField(max_length=12, validators=[telefono_fijo_regex],
                                     verbose_name='número de teléfono fijo', null=True, blank=True)

    facebook = models.URLField(max_length=128, verbose_name='Red social Facebook', null=True, blank=True)
    instagram = models.URLField(max_length=128, verbose_name='Red social Instagram', null=True, blank=True)
    twitter = models.URLField(max_length=128, verbose_name='Red social Twitter', null=True, blank=True)
    whatsapp_regex = RegexValidator(regex=r'^\d{7,15}$',
                                    message="El número de Whatsapp debe tener entre 7 y 15 dígitos.")
    whatsapp = models.CharField(max_length=15, validators=[whatsapp_regex], verbose_name='Número de whatsapp',
                                null=True, blank=True)

    history = HistoricalRecords()

    def __str__(self):
        return str(self.first_name)

    def obtener_información_tablero(self, q) -> dict:
        """
            Obtiene la información que se necesita para tablero
        """
        cohorte = CohortesColegios.buscar(q)
        jovenes = self.obtener_jovenes_activos_cohortes(cohorte)
        tablero = {}
        cantidad_jovenes = jovenes.count()
        cantidad_jovenes_activos_mes = Organizacion.obtener_jovenes_activos_en_ultimo_mes(jovenes)
        contenido_escogidos, favoritas = Organizacion.obtener_opciones_que_les_gustan_y_escogidas(jovenes)
        totales = Organizacion.obtener_contenido_favorito_tribu(jovenes)
        pruebas_completadas = Organizacion.obtener_promedio_formularios_completados(jovenes)
        resultados_leidos = Organizacion.obtener_promedio_resultados_leidos(jovenes) 
        contenido_favorito = Organizacion.obtener_promedio_contenido_favorito(jovenes)
        contenido_escogido = Organizacion.obtener_promedio_contenido_escogido(jovenes)
        perfiles_escogidos, porcentaje_escogidos = Organizacion.obtener_promedio_perfiles_escogidos(jovenes)
        total_creados, perfiles_creados = Organizacion.obtener_promedio_perfiles_creados(jovenes)
        avance_niveles = Organizacion.obtener_promedio_avance_niveles(jovenes)
        oportunidades_me_gusta = Contenido.obtener_me_gusta_ordenado('oportunidad', jovenes)[:10]
        rutas_me_gusta = Contenido.obtener_me_gusta_ordenado('ruta', jovenes)[:10]
        tablero["cantidad_jovenes"] = cantidad_jovenes
        tablero["cantidad_jovenes_activos_mes"] = cantidad_jovenes_activos_mes
        tablero["contenido_escogidos"] = contenido_escogidos
        tablero["favoritas"] = favoritas
        tablero["totales"] = totales
        tablero["pruebas_completadas"] = pruebas_completadas
        tablero["resultados_leidos"] = resultados_leidos
        tablero["contenido_favorito"] = contenido_favorito
        tablero["contenido_escogido"] = contenido_escogido
        tablero["perfiles_escogidos"] = perfiles_escogidos
        tablero["porcentaje_escogidos"] = porcentaje_escogidos
        tablero["total_creados"] = total_creados
        tablero["perfiles_creados"] = perfiles_creados
        tablero["avance_niveles"] = avance_niveles
        tablero["oportunidades_me_gusta"] = oportunidades_me_gusta
        tablero["rutas_me_gusta"] = rutas_me_gusta
        return tablero

    def obtener_cohortes_todas(self) -> 'Queryset<CohortesColegios>':
        """
            Función que retorna todas las cohortes de una organización
        """
        cohortes = self.organizacion_colegio.all().order_by("grado")
        return cohortes

    @staticmethod
    def obtener_jovenes_activos_en_ultimo_mes(jovenes) -> int:
        '''
            Retorna la cantidad de jóvenes que han iniciado sesión en la ultima semana.

            Retorno:
                int: cantidad de jóvenes activos en la ultima semana
        '''
        from django.utils import timezone
        from datetime import timedelta

        hoy = timezone.now()
        fecha_hace_un_mes = hoy - timedelta(days=30)
        jovenes_activos_en_ultimo_mes = jovenes.filter(last_login__gte=fecha_hace_un_mes).count()
        return jovenes_activos_en_ultimo_mes

    @staticmethod
    def obtener_opciones_que_les_gustan_y_escogidas(jovenes) -> dict:
        '''
            Retorna la cantidad de rutas y oportunidades escogidas por los jovenes.

            Retorno:
                dict: cantidad de favoritos y escogidos
        '''

        contenidos_escogidos = 0
        rutas_oportunidades_favoritos = 0

        ids_jovenes = list(jovenes.values_list('id', flat=True))
        query = Contenido.objects.filter(
            activo=True,
            calificacion_del_joven__usuario_id__in=ids_jovenes
        )
        rutas_oportunidades_favoritos = query.filter(calificacion_del_joven__calificacion=1).count()

        contenidos_escogidos = query.filter(calificacion_del_joven__decision=1).count()

        return contenidos_escogidos, rutas_oportunidades_favoritos

    @staticmethod
    def obtener_contenido_favorito_tribu(jovenes):
        '''
            Retorna la cantidad de rutas y oportunidades favoritas de los jovenes

            Retorno:
                int: cantidad de jóvenes activos en la ultima semana
        '''
        totales = []
        ids_jovenes = list(jovenes.values_list('id', flat=True))
        lista_nombres_trius = Tribus.obtener_nombre_lista()
        tribus = Tribus.obtener_nombres_imagen_lista()
        mis_favoritos = list(Contenido.objects.filter(
            activo=True,
            calificacion_del_joven__calificacion=1,
            calificacion_del_joven__usuario_id__in=ids_jovenes,
            tribus_relacionadas__nombre__in=lista_nombres_trius
        ).values("tribus_relacionadas__nombre").annotate(total=Count("tribus_relacionadas__nombre")))

        for tribu in tribus:
            elemento = list(
                filter(
                    lambda x: x["tribus_relacionadas__nombre"] == tribu["nombre"],
                    mis_favoritos
                )
            )
            totales.append({
                "total": str(
                    elemento[0].get("total", 0) if len(elemento) > 0 else 0
                ).zfill(2),
                "imagen": tribu["imagen"]
            })
        return totales

    @staticmethod
    def obtener_promedio_contenido_favorito(jovenes) -> int:
        '''
            Retorna el promedio de rutas favoritas de los jovenes.

            Retorno:
                int: promedio de porcentajes
        '''
        porcentajes = 0
        ids_jovenes = list(jovenes.values_list('id', flat=True))
        total_jovenes = len(ids_jovenes)
        contenido_favorito = list(CalificacionContenido.objects.filter(
            calificacion=1,
            usuario_id__in=ids_jovenes
        ).values_list('usuario_id').annotate(total=Count("usuario_id")))
        for contenido in contenido_favorito:
            total = contenido[1]
            porcentaje = round((total / 3) * 100)
            porcentajes += 100 if porcentaje > 100 else porcentaje

        if total_jovenes > 0:
            porcentajes = round(porcentajes / total_jovenes)
        return porcentajes

    @staticmethod
    def obtener_promedio_contenido_escogido(jovenes) -> int:
        '''
            Retorna el promedio de rutas escogidas de los jovenes.

            Retorno:
                int: promedio de porcentajes
        '''
        porcentajes = 0
        ids_jovenes = list(jovenes.values_list('id', flat=True))
        total_jovenes = len(ids_jovenes)
        contenido_escogido = list(CalificacionContenido.objects.filter(
            decision=1,
            usuario_id__in=ids_jovenes
        ).values_list('usuario_id').annotate(total=Count("usuario_id")))
        for contenido in contenido_escogido:
            total = contenido[1]
            porcentaje = round((total / 3) * 100)
            porcentajes += 100 if porcentaje > 100 else porcentaje

        if total_jovenes > 0:
            porcentajes = round(porcentajes / total_jovenes)
        return porcentajes

    @staticmethod
    def obtener_promedio_perfiles_escogidos(jovenes) -> int:
        '''
            Retorna el promedio de perfiles escogidas de los jovenes.

            Retorno:
                int: promedio de porcentajes
        '''
        porcentajes = 0
        totales = 0
        ids_jovenes = list(jovenes.values_list('id', flat=True))
        total_jovenes = len(ids_jovenes)
        perfiles_deseados = list(PerfilesDeseados.objects.filter(
            decision=1,
            joven_id__in=ids_jovenes
        ).values_list('joven_id').annotate(total=Count("joven_id")))
        for perfil in perfiles_deseados:
            total = perfil[1]
            totales += total
            porcentaje = round((total / 1) * 100)
            porcentajes += 100 if porcentaje > 100 else porcentaje
        if total_jovenes > 0:
            porcentajes = round(porcentajes / total_jovenes)
        return totales, porcentajes

    @staticmethod
    def obtener_promedio_perfiles_creados(jovenes) -> int:
        '''
            Retorna el promedio de perfiles creados de los jovenes.

            Retorno:
                int: promedio de porcentajes
        '''
        porcentajes = 0
        totales = 0
        ids_jovenes = list(jovenes.values_list('id', flat=True))
        total_jovenes = len(ids_jovenes)
        perfiles_creados = list(PerfilesDeseados.objects.filter(
            joven_id__in=ids_jovenes
        ).values_list('joven_id').annotate(total=Count("joven_id")))
        for perfil in perfiles_creados:
            total = perfil[1]
            totales += total
            porcentaje = round((total / 10) * 100)
            porcentajes += 100 if porcentaje > 100 else porcentaje
        if total_jovenes > 0:
            porcentajes = round(porcentajes / total_jovenes)

        return totales, porcentajes

    @staticmethod
    def obtener_promedio_formularios_completados(jovenes) -> int:
        '''
            Retorna el promedio de los formularios completados por todos los jovenes

            Retorno:
                int: promedio de porcentajes
        '''
        porcentajes = 0
        respuestas = jovenes.filter(
            respuestas_joven__completas=True
        ).values("id", "formulario_experiencias", "formulario_basicos").annotate(
            total=Count("respuestas_joven")
        ).order_by("id")

        total_formularios = jovenes.filter(
            tanda_formularios__formularios__activo=True,
            id__in=list(respuestas.values_list('id', flat=True))
        ).values("id","formulario_experiencias", "formulario_basicos").annotate(
            total=Count("tanda_formularios__formularios")
        ).order_by("id")


        total_jovenes = len(list(jovenes.values_list('id', flat=True)))

        for vistos, total in zip(respuestas, total_formularios):
            completados = 0
            no_completados = 0

            # Formulario experiencias
            if vistos["formulario_experiencias"]:
                completados += 1
            elif vistos["formulario_experiencias"] is False:
                no_completados += 1

            # Formulario cuentanos sobre ti
            if vistos["formulario_basicos"]:
                completados += 1
            elif vistos["formulario_basicos"] is False:
                no_completados += 1
            total_formularios = total["total"]+completados
            if total_formularios > 0:
                porcentajes += round(((vistos["total"]+completados)/(total_formularios))*100)

        if total_jovenes > 0:
            porcentajes = round(porcentajes / total_jovenes)
        return porcentajes

    @staticmethod
    def obtener_promedio_avance_niveles(jovenes) -> int:
        '''
            Retorna el promedio del avance en niveles por todos los jovenes

            Retorno:
                int: promedio de porcentajes
        '''
        ids_jovenes = list(jovenes.values_list('id', flat=True))
        total = len(ids_jovenes)
        realidades = [
            "Realidad Verde",
            "Realidad Amarilla",
            "Realidad Azul"
        ]
        vistos = list(VerRecurso.objects.filter(
            usuario_id__in=ids_jovenes,
            recurso__tipo__nombre__in=realidades,
            observado=1
        ).values("usuario_id").annotate(total=Count("id")))

        total_videos = len(list(Recurso.objects.filter(tipo__nombre__in=realidades).values("id")))

        porcentaje = 0
        for vistos_joven in vistos:
            porcentaje += vistos_joven["total"]/total_videos

        promedio_porcentaje = 0
        if total > 0:
            promedio_porcentaje = round((porcentaje / total) * 100)

        return promedio_porcentaje

    @staticmethod
    def obtener_promedio_resultados_leidos(jovenes) -> int:
        '''
            Retorna el promedio de los resultados leídos por todos los jovenes

            Retorno:
                int: promedio de porcentajes
        '''
        porcentajes = 0
        ids_jovenes = list(jovenes.values_list('id', flat=True))
        total_jovenes = len(ids_jovenes)

        secciones = SeccionVisitada.objects.filter(usuario_id__in=ids_jovenes).order_by("usuario_id")
        competencias = CompetenciaVisitada.objects.filter(usuario_id__in=ids_jovenes).order_by("usuario_id")
        habilidades = HabilidadVisitada.objects.filter(usuario_id__in=ids_jovenes).order_by("usuario_id")
        total_secciones = list(
            secciones.values("usuario_id").annotate(total=Count("usuario_id"))
        )
        total_competencias = list(
            competencias.values("usuario_id").annotate(total=Count("usuario_id"))
        )
        total_habilidades = list(
            habilidades.values("usuario_id").annotate(total=Count("usuario_id"))
        )
        secciones_vistas = list(
            secciones.filter(
                visitado=1
            ).values("usuario_id").annotate(total=Count("usuario_id"))
        )
        competencias_vistas = list(
            competencias.filter(
                visitado=1
            ).values("usuario_id").annotate(total=Count("usuario_id"))
        )
        habilidades_vistas = list(
            habilidades.filter(
                visitado=1
            ).values("usuario_id").annotate(total=Count("usuario_id"))
        )
        total_resultados_leidos = 0
        total_resultados = 0
        for total, vistos in zip(total_secciones, secciones_vistas):
            total_resultados += total["total"]
            total_resultados_leidos += vistos["total"]

        for total, vistos in zip(total_competencias, competencias_vistas):
            total_resultados += total["total"]
            total_resultados_leidos += vistos["total"]

        for total, vistos in zip(total_habilidades, habilidades_vistas):
            total_resultados += total["total"]
            total_resultados_leidos += vistos["total"]

        porcentajes = round(total_resultados_leidos / total_resultados) if total_resultados > 0 else 0

        if total_jovenes > 0:
            porcentajes = round((porcentajes / total_jovenes)*100)

        return porcentajes

    def buscar(id_organizacion) -> 'Organizacion':
        '''
            Obtiene el objeto Organizacion a través del id_organizacion.

            Parámetros:
                id_organizacion: numero de la Organizacion a buscar.

            Retorno:
                Si existe: Organizacion
                No existe: None
        '''
        try:
            organizacion = Organizacion.objects.get(id=id_organizacion)
            return organizacion
        except Organizacion.DoesNotExist:
            return None

    def cancelar_postulacion(self, equipo) -> None:
        '''
            Cancela la postulacion de un Equipo a una Organizacion eliminando la relación.

            Parámetros:
                equipo: Objeto del modelo Equipo.
        '''
        self.solicitudes_equipos.remove(equipo)

    def completar_campos_organizacion(self) -> 'list<str>':
        """
            Retorna una lista con los campos de la organización relacionados con ¿Para que existimos?, ¿Qué te ofrecemos?, ¿Qué jóvenes buscamos?

            Retorno:
                list<str>: lista de campos
        """

        campos = []
        if self.para_que_existimos == None or self.para_que_existimos == '':
            campos.append('¿Para que existimos?')
        if self.que_ofrecemos == None or self.que_ofrecemos == '':
            campos.append('¿Qué te ofrecemos?')
        if self.que_jovenens_buscamos == None or self.que_jovenens_buscamos == '':
            campos.append('¿Qué jóvenes buscamos?')
        return campos

    def agregar_joven(self, joven: 'Joven') -> None:
        '''
            Agrega un Joven a la relacion con Organizacion.

            Parámetros:
                joven: Objeto del modelo Joven.
        '''
        self.jovenes.add(joven)

    def agregar_equipo(self, equipo: 'Equipo') -> None:
        '''
            Agrega un Equipo a la relacion con Organizacion.

            Parámetros:
                equipo: Objeto del modelo Equipo.
        '''
        self.equipos.add(equipo)

    def joven_es_miembro(self, joven_comprobar: 'joven') -> bool:
        '''
            Confirma si un Joven es miembro de la Organizacion.

            Parámetros:
                joven_comprobar: Objeto del modelo Joven.

            Retorno:
                Si es miembro: True
                No es miembro: False
        '''
        return (joven_comprobar in self.obtener_miembros())

    def equipo_es_miembro(self, equipo_comprobar: 'Equipo') -> bool:
        '''
            Confirma si un Equipo es miembro de la Organizacion.

            Parámetros:
                equipo_comprobar: Objeto del modelo Equipo.

            Retorno:
                Si es miembro: True
                No es miembro: False
        '''
        return (equipo_comprobar in self.obtener_miembros_equipos())

    def obtener_miembros_equipos(self) -> 'Queryset<Equipo>':
        """
            Retorna los equipos de esta organización

            Retorno:
                Queryset<Equipo>: Queryset<Equipo>
        """
        return self.equipos.all()

    def obtener_tribus_relacionadas(self) -> 'Queryset<Tribus>':
        """
            Retorna las tribus de esta organización

            Retorno:
                Queryset<Tribus>: Queryset<Tribus>
        """
        return self.tribus_relacionadas.all()

    def obtener_equipos_no_miembros(self) -> 'Queryset<Equipo>':
        '''
            Retorna todos los Equipos que no son miembros de la Organización.

            Parámetros:
                self: El objeto Organización que será consultado.

            Retorno:
                Queryset<Equipo>:  Queryset de los Equipos no pertenecientes a la organización
        '''

        from django.db.models import Q

        equipos = Equipo.objects.filter(~Q(id__in=[o.id for o in self.obtener_miembros_equipos()])).exclude(id=self.id)
        return equipos

    def equipo_hace_parte_organizacion(self, equipo: 'Equipo') -> bool:
        '''
            Confirma si un Equipo hace parte de la Organización.

            Parámetros:
                equipo: Objeto del modelo Equipo.

            Retorno:
                Si es miembro: True
                No es miembro: False
        '''
        from django.db.models import Count

        return (self.equipos.filter(id=equipo.id).count() > 0)

    def obtener_miembros(self) -> 'Queryset<Joven>':
        '''
            Retorna todos los Jóvenes miembros de la Organización.

            Parámetros:
                self: El objeto Organización que será consultado.

            Retorno:
                Queryset<Joven>: Queryset de Jóvenes
        '''

        return self.jovenes.all()

    def obtener_miembros_de_organizaciones(self, organizaciones: 'list<int>') -> 'Queryset<Joven>':
        '''
            Retorna todos los Jóvenes miembros activos de varias Organizaciones.

            Parámetros:
                self: Objeto que no será relevante en la consulta.
                organizaciones: Lista de ids de Organizaciones a consultar

            Retorno:
                Queryset<Joven>: Queryset de Jovenes
        '''

        return Joven.objects.filter(organizaciones_joven__pk__in=organizaciones, is_active=True).order_by().distinct()

    def joven_hace_parte_organizacion(self, joven: 'Joven') -> bool:
        '''
            Confirma si un Joven es miembro de la Organización.

            Parámetros:
                joven: Objeto del modelo Joven.

            Retorno:
                Si es miembro: True
                No es miembro: False
        '''

        from django.db.models import Count

        return (self.jovenes.filter(id=joven.id).count() > 0)

    def obtener_jovenes_no_miembros(self) -> 'Queryset<Joven>':
        '''
            Retorna todos los Jóvenes que no son miembros de la Organización.

            Parámetros:
                joven: Objeto del modelo Joven.
                self: El objeto Organización que será consultado.

            Retorno:
                Queryset<Joven>: Queryset Jovenes
        '''

        from django.db.models import Q

        usuarios = Joven.objects.filter(~Q(id__in=[o.id for o in self.obtener_miembros()])).exclude(id=self.id)
        return usuarios

    def enviar_solicitud_miembro(self, joven: 'Joven') -> None:
        '''
            Crea un objeto SolicitudesOrganizacionJovenes referenciando las solicitudes enviadas de la organización a un Joven.

            Parámetros:
                joven: Objeto del modelo Joven.
                self: El objeto Organizacion.
        '''

        SolicitudesOrganizacionJovenes.objects.create(organizacion=self, joven=joven, estado='Invitado')

    def obtener_jovenes_no_miebros_ni_invitados(self) -> 'Queryset<Joven>':
        '''
            Retorna todos los Jovenes que no son miembros de la Organizacion y que tampoco tengan invitaciones enviadas o recibidas para unirse a la Organizacion.

            Parámetros:
                self: El objeto Organizacion que sera consultado.

            Retorno:
                Queryset<Joven>: Queryset Jovenes
        '''

        from django.db.models import Q

        usuarios = Joven.objects.filter(~Q(id__in=[o.id for o in self.obtener_miembros()]) or ~Q(
            id__in=[a.id for a in self.obtener_solicitudes_miembros()])).exclude(id=self.id)
        return usuarios

    @staticmethod
    def existe_organizacion(id_organizacion: int) -> bool:
        '''
            Confirma si una Organizacion existe.

            Parámetros:
                id_organizacion: Numero de la organizacion a buscar.

            Retorno:
                Si existe: True
                No existe: False
        '''

        from django.db.models import Count

        if Organizacion.objects.filter(id=id_organizacion).count() > 0:
            return True
        else:
            return False

    def obtener_solicitudes_miembros(self) -> 'Queryset<SolicitudesOrganizacionJovenes>':
        '''
            Retorna todas las SolicitudesOrganizacionJovenes enviadas o recibidas de una Organizacion a un Joven o viceversa.

            Parámetros:
                self: El objeto Organizacion que sera consultado.

            Retorno:
                Queryset de SolicitudesOrganizacionJovenes
        '''

        return self.solicitudes.all()

    def obtener_lista_usuarios_solicitudes(self) -> 'Queryset<SolicitudesOrganizacionJovenes>':
        '''
            Retorna todas las SolicitudesOrganizacionJovenes enviadas por un Joven a una Organizacion.

            Parámetros:
                self: El objeto Organizacion que sera consultado.

            Retorno:
                Queryset de SolicitudesOrganizacionJovenes
        '''

        solicitudes = self.solicitudes.filter(solicitudes_joven__estado='Postulado')
        return solicitudes

    def obtener_lista_equipos_solicitudes(self) -> 'Queryset<SolicitudesOrganizacionJovenes>':
        '''
            Retorna todas las SolicitudesOrganizacionEquipos enviadas por un Equipo a una Organizacion.

            Parámetros:
                self: El objeto Organizacion que sera consultado.

            Retorno:
                Queryset de SolicitudesOrganizacionEquipos
        '''

        solicitudes = self.solicitudes_equipos.all()
        return solicitudes

    def tribus(self) -> 'list<str>':
        '''
            Retorna una lista de Strings con las tribus asociadas a la Organizacion.

            Parámetros:
                self: El objeto Organizacion que sera consultado.

            Retorno:
                Lista de Strings
        '''

        tribus = []
        if self.cuisana:
            tribus.append('cuisana')
        if self.moterra:
            tribus.append('moterra')
        if self.ponor:
            tribus.append('ponor')
        if self.vincularem:
            tribus.append('vincularem')
        if self.brianimus:
            tribus.append('brianimus')
        if self.gronor:
            tribus.append('gronor')
        if self.centeros:
            tribus.append('centeros')
        return tribus

    @staticmethod
    def obtener_principal() -> 'Organizacion':
        '''
            Obtiene el objeto Organizacion principal.

            Retorno:
                Si es principal: Organizacion
        '''
        organizacion = Organizacion.objects.get(es_principal=True)
        return organizacion

    @staticmethod
    def get_queryset(query: str = None, organizaciones: 'Queryset<Organizacion>' = None) -> 'Queryset<Organizacion>':
        '''
            Filtra y retorna de un grupo especifico de Organizaciones las que posean algun elemento definido en el query.

            Parámetros:
                query: String que define el filtro que tendrán las organizaciones recibidas como parametro.
                organizaciones: Queryset de Organizacion

            Retorno:
                Queryset de Organizaciones filtradas por el parametro query
        '''

        queries = query.split(' ')
        for q in queries:
            tribus = Tribus.objects.filter(nombre__icontains=q)
            roles = Roles.objects.filter(nombre__icontains=q)
            niveles_conocimiento = NivelesDeConocimiento.objects.filter(nombre__icontains=q)
            organizaciones = organizaciones.filter(
                Q(first_name__icontains=q) | Q(pais__name__icontains=q) | Q(region__name__icontains=q)
                | Q(ciudad__name__icontains=q) | Q(tipo__icontains=q)
                | Q(insignias_centeros__nombre__icontains=q)
                | Q(insignias_brianimus__nombre__icontains=q) | Q(insignias_cuisana__nombre__icontains=q)
                | Q(insignias_ponor__nombre__icontains=q) | Q(insignias_vincularem__nombre__icontains=q)
                | Q(insignias_moterra__nombre__icontains=q) | Q(insignias_gronor__nombre__icontains=q)
                | Q(roles__in=roles) | Q(tribus_relacionadas__in=tribus) | Q(
                    niveles_conocimiento__in=niveles_conocimiento)).distinct()
        return organizaciones 
        

    @ staticmethod

    def obtener_organizaciones_filtro(ids: list) -> 'Queryset<Organizacion>':
        '''
            Filtra de una lista de ids de organizaciones y retorna un Queryset de Organizaciones.

            Parámetros:
                ids (list): Lista de ids de organizaciones.
            Retorno:
                Queryset de Organizaciones filtradas por los ids.
        '''

        organizaciones = Organizacion.objects.filter(pk__in=ids)
        return organizaciones

    @staticmethod
    def sugeridas(usuario: 'Usuario') -> 'Queryset<Organizacion>':
        '''
            Filtra y retorna Organizaciones que posean tribus, niveles de conocimiento o roles similares a los del usuario recibido como parametro.

            Parámetros:
                usuario: Objeto del modelo Usuario

            Retorno:
                Queryset de Organizaciones
        '''

        mis_perfiles = PerfilesDeseados.objects.filter(joven_id=usuario.pk)
        mis_tribus = mis_perfiles.values('tribu_deseada').distinct()
        mis_niveles = mis_perfiles.values('nivel_conocimiento_deseado').distinct()
        voluntariado = mis_perfiles.filter(interes_voluntariado=True)
        laboral = mis_perfiles.filter(interes_trabajo=True)
        independiente = mis_perfiles.filter(interes_independiente=True)
        emprendedor = mis_perfiles.filter(interes_emprendedor=True)
        organizaciones_favoritas = Organizacion.objects.exclude(calificacion_del_joven__usuario=usuario,
                                                                calificacion_del_joven__calificacion=0)
        organizaciones = organizaciones_favoritas.filter(is_active=True)
        q = Q()
        if voluntariado.exists():
            q = q | Q(nombre__icontains='voluntario')
        if laboral.exists():
            q = q | Q(nombre__icontains='colaborador')
        if independiente.exists():
            q = q | Q(nombre__icontains='independiente')
        if emprendedor.exists():
            q = q | Q(nombre__icontains='emprendedor')
        mis_roles = Roles.objects.filter(q)
        mis_organizaciones = organizaciones.filter(
            Q(Q(tribus_relacionadas__in=mis_tribus) & Q(niveles_conocimiento__in=mis_niveles))
            | Q(Q(tribus_relacionadas__in=mis_tribus) & Q(roles__in=mis_roles))).distinct()
        return mis_organizaciones

    @staticmethod
    def favoritas(usuario: 'Usuario') -> 'Queryset<Organizacion>':
        '''
            Retorna las Organizaciones marcadas como favorito por un usuario.

            Parámetros:
                usuario: Objeto del modelo Usuario

            Retorno:
                Queryset de Organizaciones
        '''

        organizaciones = Organizacion.objects.filter(is_active=True)
        mis_favoritos = organizaciones.filter(calificacion_del_joven__usuario=usuario,
                                              calificacion_del_joven__calificacion=1)
        return mis_favoritos

    @staticmethod
    def get_queryset_tag(tag: str = None, organizaciones: 'Queryset<Organizacion>' = None,
                         es_organizacion: bool = False) -> 'Queryset<Organizacion>':
        '''
            Filtra y retorna de un grupo especifico de Organizaciones las que tengan un nombre similar al recibido en el parámetro tag.

            Parámetros:
                tag: String que se usara para filtrar por nombre de Organizacion
                organizaciones: Queryset de Organizaciones
                es_organizacion: Parámetro sin funcionalidad actualmente

            Retorno:
                Queryset de Organizaciones
        '''

        organizaciones = organizaciones.filter(tipo_organizacion__nombre__iexact=tag).distinct()
        return organizaciones

    # #################################################################################################################################################################
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    # #################################################################################################################################################################

    def obtener_pais_ciudades_adquirir_experiencia(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        """
            retorna una lista de diccionarios con los paises donde los miembros_organizaciones quieren adquirir experiencia.

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorna:
                List: [{'name': ciudad, 'children': [{'name': ciudad, 'value': cantidad}]}, ...]
        """

        diccionario_ciudades = dict()

        for perfil in PerfilesDeseados.objects.exclude(interes_pais_id__isnull=True).filter(favorito=True,
                                                                                            joven__in=miembros_organizaciones):
            nombre_ciudad = Country.objects.get(id=perfil.interes_pais_id).name_ascii
            if nombre_ciudad in diccionario_ciudades:
                diccionario_ciudades[nombre_ciudad] += 1
            else:
                diccionario_ciudades[nombre_ciudad] = 1
        lista_retorno = []
        for ciudad, cantidad in diccionario_ciudades.items():
            lista_retorno.append({'name': ciudad, 'children': [{'name': ciudad, 'value': cantidad}]})

        return lista_retorno

    def nivel_conocimiento_alcanzar_tribu(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        """
            Retorna una lista de diccionarios con los niveles de conocimiento que los miembros_organizaciones quieren alcanzar en una tribu.

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                Lista: [{'agrupacion': (nombre tribu), 'Experto': (cantidad), 'Oficios': (cantidad), 'Líder de Nicho': (cantidad), 'Profesional': (cantidad), 'Tecnólogo': (cantidad)}, ...]
        """

        from django.db.models import Count, Q
        from kairos.insignias.models import Tribus, NivelesDeConocimiento

        numero_asociado_nivel_conocimiento = dict()
        for nivel_conocimiento in NivelesDeConocimiento.objects.values('pk', 'nombre'):
            numero_asociado_nivel_conocimiento[str(nivel_conocimiento['pk'])] = nivel_conocimiento['nombre']

        tribus_deseadas = dict()
        for tribu in PerfilesDeseados.objects.filter(joven__in=miembros_organizaciones, favorito=True).values(
                'tribu_deseada', 'tribu_deseada__nombre').distinct():
            tribus_deseadas[str(tribu['tribu_deseada'])] = tribu['tribu_deseada__nombre']
        lista_niveles_agrupados = list(tribus_deseadas)

        niveles_tribu_pefil = PerfilesDeseados.objects.filter(joven__in=miembros_organizaciones, favorito=True
                                                              ).values('nivel_conocimiento_deseado', 'tribu_deseada'
                                                                       ).order_by('tribu_deseada').annotate(
            numero_jovenes=Count('joven', distinct=True))
        lista_niveles = dict()
        lista_niveles['Totales'] = {'agrupacion': 'Totales'}
        for registro in niveles_tribu_pefil:
            nivel, tribu, total = registro.values()

            if tribus_deseadas[str(tribu)] not in lista_niveles:
                lista_niveles[tribus_deseadas[str(tribu)]] = dict()
                lista_niveles[tribus_deseadas[str(tribu)]].update({'agrupacion': tribus_deseadas[str(tribu)]})

            lista_niveles[tribus_deseadas[str(tribu)]].update({numero_asociado_nivel_conocimiento[str(nivel)]: total})
            if numero_asociado_nivel_conocimiento[str(nivel)] not in lista_niveles['Totales']:
                lista_niveles['Totales'][numero_asociado_nivel_conocimiento[str(nivel)]] = 0

            lista_niveles['Totales'][numero_asociado_nivel_conocimiento[str(nivel)]] += total

        return list(lista_niveles.values())

    def deseos_aprendizaje_tribu(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        """
            Retorna una lista de diccionarios con los deseos de aprendizaje que los miembros_organizaciones quieren alcanzar en uns tribu.

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                Lista: [{'agrupacion': (nombre tribu), 'Educación formal': (cantidad), 'Educación informal': (cantidad), 'Virtual': (cantidad), 'Independiente': (cantidad)}, ...]
        """

        from django.db.models import Count, Q
        from kairos.insignias.models import Tribus

        from collections import Counter

        numero_asociado_deseo_aprendizaje = {
            "1": "Educación formal",
            "2": "Educación informal",
            "3": "Virtual",
            "4": "Independiente"}

        tribus_deseadas = dict()
        tribus_deseadas['Totales'] = {"agrupacion": 'Totales', '1': 0, '2': 0, '3': 0, '4': 0}
        for tribu in PerfilesDeseados.objects.filter(joven__in=miembros_organizaciones, favorito=True).order_by(
                'tribu_deseada').values('tribu_deseada', 'tribu_deseada__nombre').distinct():
            tribus_deseadas[tribu['tribu_deseada']] = {"agrupacion": tribu['tribu_deseada__nombre'], '1': 0, '2': 0,
                                                       '3': 0, '4': 0}

        aprendizaje_tribu_pefil = PerfilesDeseados.objects.filter(joven__in=miembros_organizaciones, favorito=True
                                                                  ).values('deseo_aprendizaje', 'tribu_deseada'
                                                                           ).order_by('tribu_deseada').annotate(
            numero_jovenes=Count('joven', distinct=True))

        lista_auxiliar = {}

        for registro in aprendizaje_tribu_pefil:
            aprendizajes, tribu, cantidad = registro.values()
            for aprendizaje in aprendizajes:
                tribus_deseadas[tribu][str(aprendizaje)] += cantidad
                tribus_deseadas['Totales'][str(aprendizaje)] += cantidad

        for valores in tribus_deseadas.values():
            for identificador, nombre in numero_asociado_deseo_aprendizaje.items():
                valores[nombre] = valores[str(identificador)]
                del valores[str(identificador)]

        return (list(tribus_deseadas.values()))

    def intereses_por_tribus(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        """
            Retorna una lista de diccionarios con los intereses que los miembros_organizaciones quieren alcanzar en una tribu.

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                Lista: [{'agrupacion': (nombre tribu), 'Independiente': (cantidad), 'Emprendedor': (cantidad), 'Voluntariado': (cantidad), 'Trabajo': (cantidad)}, ...]
            
        """

        from django.db.models import Count, Q
        from kairos.insignias.models import Tribus

        lista_niveles_agrupados = []
        for perfiles in list(PerfilesDeseados.objects.filter(favorito=True).values('tribu_deseada').distinct()):
            lista_niveles_agrupados.append(perfiles)

        lista_niveles = []
        jovenes = miembros_organizaciones
        for tribu in lista_niveles_agrupados:
            lista_auxilar_nivel = []
            lista_auxilar_nivel.append(['Independiente',
                                        list(PerfilesDeseados.objects.filter(joven__in=jovenes,
                                                                             tribu_deseada=tribu['tribu_deseada'],
                                                                             interes_independiente=True,
                                                                             favorito=True).values(
                                            'interes_independiente').annotate(numero=Count('joven', distinct=False)))])
            lista_auxilar_nivel.append(['Emprendedor',
                                        list(PerfilesDeseados.objects.filter(joven__in=jovenes,
                                                                             tribu_deseada=tribu['tribu_deseada'],
                                                                             interes_emprendedor=True,
                                                                             favorito=True).values(
                                            'interes_emprendedor').annotate(numero=Count('joven', distinct=False)))])
            lista_auxilar_nivel.append(['Voluntariado',
                                        list(PerfilesDeseados.objects.filter(joven__in=jovenes,
                                                                             tribu_deseada=tribu['tribu_deseada'],
                                                                             interes_voluntariado=True,
                                                                             favorito=True).values(
                                            'interes_voluntariado').annotate(numero=Count('joven', distinct=False)))])
            lista_auxilar_nivel.append(['Trabajo',
                                        list(PerfilesDeseados.objects.filter(joven__in=jovenes,
                                                                             tribu_deseada=tribu['tribu_deseada'],
                                                                             interes_trabajo=True,
                                                                             favorito=True).values(
                                            'interes_trabajo').annotate(numero=Count('joven', distinct=False)))])
            lista_niveles.append({tribu['tribu_deseada']: lista_auxilar_nivel})

        diccionario_totales = {'agrupacion': 'Totales',
                               'Independiente': PerfilesDeseados.objects.filter(joven__in=jovenes,
                                                                                interes_independiente=True,
                                                                                favorito=True).values(
                                   'interes_independiente').count(),
                               'Emprendedor': PerfilesDeseados.objects.filter(joven__in=jovenes,
                                                                              interes_emprendedor=True,
                                                                              favorito=True).values(
                                   'interes_emprendedor').count(),
                               'Voluntariado': PerfilesDeseados.objects.filter(joven__in=jovenes,
                                                                               interes_voluntariado=True,
                                                                               favorito=True).values(
                                   'interes_voluntariado').count(),
                               'Trabajo': PerfilesDeseados.objects.filter(joven__in=jovenes,
                                                                          interes_trabajo=True,
                                                                          favorito=True).values(
                                   'interes_trabajo').count()}

        lista_retorno = []
        lista_retorno.append(diccionario_totales)
        for tribu in lista_niveles_agrupados:
            for tribu_agrupada in lista_niveles:
                diccionario_auxiliar = dict()
                if tribu['tribu_deseada'] in tribu_agrupada:
                    diccionario_auxiliar = dict()
                    diccionario_auxiliar["agrupacion"] = Tribus.objects.get(id=tribu['tribu_deseada']).nombre
                    for niveles in tribu_agrupada[tribu['tribu_deseada']]:  # lista con los niveles
                        if len(niveles[1]) > 0:
                            diccionario_auxiliar[niveles[0]] = niveles[1][0]['numero']
                            # diccionario_totales[niveles[0]] += niveles[1][0]['numero']
                    lista_retorno.append(diccionario_auxiliar)

        return lista_retorno

    def porcentaje_tribus(self, miembros_organizaciones: 'Queryset<Joven>') -> 'dict':
        """
            Retorna un diccionario con el porcentaje de los intereses de los jovenes

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                dict: {'Ciencia y Medio Ambiente': <float>, 'Salud': <float>, 'Artes y Deportes': <float>, 'Seguridad y Defensa': <float>, 
                'Humanidades': <float>, 'Administración': <float>, 'Ingeniería': <float>}
        """

        from django.db.models import Count

        diccionario_interes_jovenes = {'Administración': 0,
                                       'Ingeniería': 0,
                                       'Salud': 0,
                                       'Humanidades': 0,
                                       'Artes y Deportes': 0,
                                       'Ciencia y Medio Ambiente': 0,
                                       'Seguridad y Defensa': 0,
                                       'Servicios': 0
                                       }

        if not miembros_organizaciones:
            return diccionario_interes_jovenes

        relacion_tribu_interes = {"Gronor": "Administración",
                                  "Centeros": "Ingeniería",
                                  "Cuisana": "Salud",
                                  "Vincularem": "Humanidades",
                                  "Brianimus": "Artes y Deportes",
                                  "Moterra": "Ciencia y Medio Ambiente",
                                  "Ponor": "Seguridad y Defensa",
                                  "Sersas": "Servicios"
                                  }

        lista_cantidad_tribus = miembros_organizaciones.values(
            'joven_dueno_perfil__tribu_deseada__nombre').order_by().annotate(total=Count('pk'))
        lista = []
        total = 0
        for tribus in lista_cantidad_tribus:
            tribu, cantidad = tuple(tribus.values())
            lista.append((relacion_tribu_interes[tribu], cantidad))
            total += tuple(tribus.values())[1]

        diccionario_interes_jovenes = dict()
        for registro in lista:
            tribu, cantidad = registro
            diccionario_interes_jovenes[tribu] = (cantidad / total) * 100

        return diccionario_interes_jovenes

    def obtener_colegios() -> 'Queryset<Organizacion>':
        """
            Retorna un queryset con las organizaciones de tipo colegio
        """
        colegios = Organizacion.objects.filter(tipo_organizacion__id=4)
        return colegios

    def experiencias_colegio(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        """
            Retorna una lista de diccionarios con las asignaturas y el rendimiento en estas de los miembros_organizaciones 

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                Lista: [{'category': (Nombre asignatura), 'Ese curso nunca lo he visto': (cantidad), 'Suelo tener dificultades, no es mi área fuerte': (cantidad), 
                'Normal, logro aprobar': (cantidad), "Me va bien, aun cuando no es lo que más me gusta': (cantidad), 
                'Soy muy bueno, se me hace fácil y me gusta': (cantidad), 'No me va bien, pero me gusta': (cantidad)}, ...]
        """

        from django.db.models import Count, Q

        diccionario_asignaturas = {
            'rendimiento_fisica': {'category': 'rendimiento_fisica', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0},
            'rendimiento_informatica': {'category': 'rendimiento_informatica', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0,
                                        '5': 0},
            'rendimiento_matematica': {'category': 'rendimiento_matematica', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0,
                                       '5': 0},
            'rendimiento_historia': {'category': 'rendimiento_historia', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0,
                                     '5': 0},
            'rendimiento_literatura': {'category': 'rendimiento_literatura', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0,
                                       '5': 0},
            'rendimiento_idiomas_extranjeros': {'category': 'rendimiento_idiomas_extranjeros', '0': 0, '1': 0, '2': 0,
                                                '3': 0, '4': 0, '5': 0},
            'rendimiento_idiomas_nativos': {'category': 'rendimiento_idiomas_nativos', '0': 0, '1': 0, '2': 0, '3': 0,
                                            '4': 0, '5': 0},
            'rendimiento_artes': {'category': 'rendimiento_artes', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0},
            'rendimiento_deportes': {'category': 'rendimiento_deportes', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0,
                                     '5': 0},
            'rendimiento_naturales': {'category': 'rendimiento_naturales', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0,
                                      '5': 0},
            'rendimiento_geografia': {'category': 'rendimiento_geografia', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0,
                                      '5': 0},
            'rendimiento_economia': {'category': 'rendimiento_economia', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0,
                                     '5': 0},
            'rendimiento_quimica': {'category': 'rendimiento_quimica', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0},
            'rendimiento_salud': {'category': 'rendimiento_salud', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0},
            'rendimiento_militar': {'category': 'rendimiento_militar', '0': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0},
        }

        lista_asiganturas_colegio = [
            'rendimiento_fisica',
            'rendimiento_informatica',
            'rendimiento_matematica',
            'rendimiento_historia',
            'rendimiento_literatura',
            'rendimiento_idiomas_nativos',
            'rendimiento_idiomas_extranjeros',
            'rendimiento_artes',
            'rendimiento_deportes',
            'rendimiento_naturales',
            'rendimiento_geografia',
            'rendimiento_economia',
            'rendimiento_quimica',
            'rendimiento_salud',
            'rendimiento_militar'
        ]
        diccionario_numero_asociado_rendimiento = {
            "5": "No me va bien, pero me gusta",
            "4": "Soy muy bueno, se me hace fácil y me gusta",
            "3": "Me va bien, aun cuando no es lo que más me gusta",
            "2": "Normal, logro aprobar",
            "1": "Suelo tener dificultades, no es mi área fuerte",
            "0": "Ese curso nunca lo he visto"
        }

        '''for asignaturas in lista_asiganturas_colegio:            
            for rendimiento in CONSULTA_JOVENES.values(asignaturas):                
                for asignatura, valor in rendimiento.items():
                    if valor != None:
                        diccionario_asignaturas[asignatura][str(valor)] += 1'''
        for asignatura in lista_asiganturas_colegio:
            for joven in miembros_organizaciones:
                valor = getattr(joven, asignatura)
                if valor != None:
                    diccionario_asignaturas[asignatura][str(valor)] += 1
                # print(getattr(joven, asignaturas))

        # lista_ordenada = list(reversed(sorted(aptitudes_e_intereses, key=lambda x: x["resultado"])))
        lista_retorno = []
        for asignatura in lista_asiganturas_colegio:
            tamanio = len(diccionario_asignaturas[asignatura]['category'].split('_'))
            if tamanio > 2:
                nombre_asignatura = ""
                for valor in diccionario_asignaturas[asignatura]['category'].split('_')[1:tamanio]:
                    nombre_asignatura += valor + " "
                diccionario_asignaturas[asignatura]['category'] = nombre_asignatura.capitalize()
            else:
                diccionario_asignaturas[asignatura]['category'] = \
                    diccionario_asignaturas[asignatura]['category'].split('_')[1].capitalize()

            for numero, rendimiento in list(reversed(list(diccionario_numero_asociado_rendimiento.items()))):
                if diccionario_asignaturas[asignatura][str(numero)] == 0:
                    del diccionario_asignaturas[asignatura][str(numero)]
                else:
                    diccionario_asignaturas[asignatura][diccionario_numero_asociado_rendimiento[str(numero)]] = \
                        diccionario_asignaturas[asignatura][str(numero)]
                    del diccionario_asignaturas[asignatura][str(numero)]

            lista_retorno.append(diccionario_asignaturas[asignatura])

        return lista_retorno

    def distribucion_jovenes_por_experiencia_tribu(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        """
            Retorna una lista de diccionarios con las experiencias de los miembros_organizaciones segun su tribu.

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                Lista: [{'Brianimus': (cantidad), 'Centeros': (cantidad), 'Cuisana': (cantidad), 'Gronor': (cantidad), 'Moterra': (cantidad), 'Ponor': (cantidad), 'Vincularem': (cantidad), 'category': (Nombre experiencia)}, ...]
        """

        from kairos.insignias.models import Tribus
        from django.db.models import Count

        tribus = Tribus.objects.all()
        diccionario_jovenes = {
            'Club juvenil': dict(),
            'Voluntariado': dict(),
            'Colaborador': dict(),
            'Independiente': dict(),
            'Emprendimiento': dict(),
        }

        for tribu in tribus:
            jovenes = miembros_organizaciones
            cantidad_clubes = jovenes.filter(clubes_del_joven__tribu=tribu).count()
            cantidad_voluntariados = jovenes.filter(voluntariados_del_joven__tribu=tribu).count()
            cantidad_laborales = jovenes.filter(experiencias_laborales_del_joven__tribu=tribu).count()
            cantidad_independientes = jovenes.filter(trabajos_independientes_del_joven__tribu=tribu).count()
            cantidad_emprendimientos = jovenes.filter(emprendimientos_del_joven__tribu=tribu).count()

            if cantidad_clubes > 0:
                diccionario_jovenes['Club juvenil'].update({tribu.nombre: cantidad_clubes})
            if cantidad_voluntariados > 0:
                diccionario_jovenes['Voluntariado'].update({tribu.nombre: cantidad_voluntariados})
            if cantidad_laborales > 0:
                diccionario_jovenes['Colaborador'].update({tribu.nombre: cantidad_laborales})
            if cantidad_independientes > 0:
                diccionario_jovenes['Independiente'].update({tribu.nombre: cantidad_independientes})
            if cantidad_emprendimientos > 0:
                diccionario_jovenes['Emprendimiento'].update({tribu.nombre: cantidad_emprendimientos})

        lista_retorno = []
        for experiencia, tribus in diccionario_jovenes.items():
            tribus.update({'category': experiencia})
            lista_retorno.append(tribus)

        return lista_retorno

    def distribucion_jovenes_por_experiencia_tiempo(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        """
            Retorna una lista de diccionarios con las experiencias de los miembros_organizaciones según su tiempo en estas.

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                Lista: [{'Menos de 1 año': (cantidad), 'Entre 1 y 2 años': (cantidad), 'Entre 2 y 4 años': (cantidad), 'Más de 5 años': (cantidad), 'category': (Nombre experiencia)}, ...]
        """

        from kairos.insignias.models import Tribus
        from django.db.models import Count

        diccionario_tiempo_asociado_numero = {
            "1": "Menos de 1 año",
            "2": "Entre 1 y 2 años",
            "3": "Entre 2 y 4 años",
            "4": "Más de 5 años"
        }

        diccionario_jovenes = {
            'Club juvenil': dict(),
            'Voluntariado': dict(),
            'Colaborador': dict(),
            'Independiente': dict(),
            'Emprendimiento': dict(),
        }

        for tiempo, nombre in diccionario_tiempo_asociado_numero.items():
            cantidad_clubes = miembros_organizaciones.filter(clubes_del_joven__tiempo=tiempo).count()
            cantidad_voluntariados = miembros_organizaciones.filter(voluntariados_del_joven__tiempo=tiempo).count()
            cantidad_laborales = miembros_organizaciones.filter(experiencias_laborales_del_joven__tiempo=tiempo).count()
            cantidad_independientes = miembros_organizaciones.filter(
                trabajos_independientes_del_joven__tiempo=tiempo).count()
            cantidad_emprendimientos = miembros_organizaciones.filter(emprendimientos_del_joven__tiempo=tiempo).count()

            if cantidad_clubes > 0:
                diccionario_jovenes['Club juvenil'].update({nombre: cantidad_clubes})
            if cantidad_voluntariados > 0:
                diccionario_jovenes['Voluntariado'].update({nombre: cantidad_voluntariados})
            if cantidad_laborales > 0:
                diccionario_jovenes['Colaborador'].update({nombre: cantidad_laborales})
            if cantidad_independientes > 0:
                diccionario_jovenes['Independiente'].update({nombre: cantidad_independientes})
            if cantidad_emprendimientos > 0:
                diccionario_jovenes['Emprendimiento'].update({nombre: cantidad_emprendimientos})

        lista_retorno = []
        for experiencia, tiempos in diccionario_jovenes.items():
            tiempos.update({'category': experiencia})
            lista_retorno.append(tiempos)

        return lista_retorno

    def jovenes_en_voluntariado(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list':
        """
            Retorna a los jóvenes que están en voluntariado.

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                list<int>: [<int>, <float>]
        """

        from django.db.models import Count

        cantidad = 0
        resultado = 0
        for joven in miembros_organizaciones.all():
            if len(joven.voluntariados_del_joven.all()):
                cantidad += 1

        if len(miembros_organizaciones.all()) > 0:
            resultado = (cantidad / len(miembros_organizaciones.all())) * 100

        return [cantidad, resultado]

    def jovenes_con_experiencia_laboral(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<int>':
        """
            Retorna a los jóvenes con experiencia laboral.

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                list<int>: [<int>, <float>]
        """

        from django.db.models import Count

        cantidad = 0
        resultado = 0
        for joven in miembros_organizaciones.all():
            if len(joven.experiencias_laborales_del_joven.all()):
                cantidad += 1

        if len(miembros_organizaciones.all()) > 0:
            resultado = (cantidad / len(miembros_organizaciones.all())) * 100

        return [cantidad, resultado]

    def jovenes_con_clubes(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<int>':
        """
            Retorna a los jóvenes en clubes.

            Retorno:
                list<int>: [<int>, <float>]
        """

        from django.db.models import Count

        cantidad = 0
        resultado = 0
        for joven in miembros_organizaciones.all():
            if len(joven.clubes_del_joven.all()):
                cantidad += 1

        if len(miembros_organizaciones.all()) > 0:
            resultado = (cantidad / len(miembros_organizaciones.all())) * 100

        return [cantidad, resultado]

    def obtener_numero_experiencias_promedio_joven(self, miembros_organizaciones: 'Queryset<Joven>') -> float:
        """
            Retorna el numero de experiencias promedio del joven

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                float: numero experiencias promedio joven
        """

        from django.db.models import Count, Q
        from kairos.experiencias_jovenes.models import ClubesJuveniles, Voluntariados, Laborales, Independientes, \
            Emprendimientos

        cantidad_clubes = ClubesJuveniles.objects.all().count()
        cantidad_voluntariados = Voluntariados.objects.all().count()
        cantidad_laborales = Laborales.objects.all().count()
        cantidad_independientes = Independientes.objects.all().count()
        cantidad_emprendimientos = Emprendimientos.objects.all().count()

        jovenes_con_experiencias = miembros_organizaciones.filter(is_active=True).annotate(
            clubes=Count('clubes_del_joven'),
            voluntariados=Count('voluntariados_del_joven'),
            experiencias=Count('experiencias_laborales_del_joven'),
            trabajos=Count('trabajos_independientes_del_joven'),
            emprendimientos=Count('emprendimientos_del_joven'),
        ).filter(
            Q(clubes__gt=0) |
            Q(voluntariados__gt=0) |
            Q(experiencias__gt=0) |
            Q(trabajos__gt=0) |
            Q(emprendimientos__gt=0)
        )

        cantidad_experiencias_jovenes = 0
        for joven in jovenes_con_experiencias:
            cantidad_experiencias_jovenes += joven.clubes_del_joven.all().count() + joven.voluntariados_del_joven.all().count() + joven.experiencias_laborales_del_joven.all().count() + joven.trabajos_independientes_del_joven.all().count() + joven.emprendimientos_del_joven.all().count()

        if jovenes_con_experiencias.count() > 0:
            return (cantidad_experiencias_jovenes / jovenes_con_experiencias.count())
        else:
            return 0

    def obtener_experiencias_ordenadas_por_frecuencia(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<str>':
        """
            Retorna las experiencias promedio del joven ordenadas

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar

            Retorno:
                list<str>: experiencias
        """

        from django.db.models import Count, Q
        from kairos.core.utils import ordenar_dicionario_por_valores
        from kairos.experiencias_jovenes.models import ClubesJuveniles, Voluntariados, Laborales, Independientes, \
            Emprendimientos

        jovenes = miembros_organizaciones
        diccionario_experiencias_cantidad = {
            'Club juvenil': ClubesJuveniles.objects.filter(joven__in=jovenes).distinct('joven').count(),
            'Voluntariado': Voluntariados.objects.filter(joven__in=jovenes).distinct('joven').distinct('joven').count(),
            'Colaborador': Laborales.objects.filter(joven__in=jovenes).distinct('joven').distinct('joven').count(),
            'Independiente': Independientes.objects.filter(joven__in=jovenes).distinct('joven').distinct(
                'joven').count(),
            'Emprendimiento': Emprendimientos.objects.filter(joven__in=jovenes).distinct('joven').distinct(
                'joven').count()}

        lista_retorno = ordenar_dicionario_por_valores(diccionario_experiencias_cantidad)

        if lista_retorno[0][1] == 0:
            return ['-', '-']

        return lista_retorno

    def obtener_experiencias_estadisticas_experiencia(self, experiencia: str,
                                                      miembros_organizaciones: 'Queryset<Joven>') -> 'list<int>':
        """
            Retorna las experiencias promedio del joven ordenadas

            Parámetros:
                miembros_organizaciones (Queryset<Joven>): Jóvenes por filtrar
                experiencia (str): experiencia a buscar

            Retorno:
                list<str>: experiencias
        """
        from django.db.models import Count, Q
        from kairos.core.utils import ordenar_dicionario_por_valores
        from kairos.experiencias_jovenes.models import ClubesJuveniles, Voluntariados, Laborales, Independientes, \
            Emprendimientos

        cantidad_experiencias_por_joven = 0
        jovenes = miembros_organizaciones

        if experiencia == 'club_juvenil':
            cantidad_jovenes_experiencia = jovenes.filter(is_active=True).annotate(
                clubes=Count('clubes_del_joven')).filter(clubes__gt=0).distinct().count()
            for joven in jovenes:
                cantidad_experiencias_por_joven += joven.clubes_del_joven.all().count()
            nombre = 'Club juvenil'
            mensaje = 'Jóvenes con al menos 1 experiencia en un ' + nombre
            titulo_seccion = 'Experiencias en ' + nombre
            descripcion_experiencias_por_joven = 'Número de experiencias promedio en ' + nombre + ' por joven'
            descripcion_moda = 'Moda de la duración de la experiencia en ' + nombre
        elif experiencia == 'voluntariado':
            cantidad_jovenes_experiencia = jovenes.filter(is_active=True).annotate(
                clubes=Count('voluntariados_del_joven')).filter(clubes__gt=0).distinct().count()
            for joven in jovenes:
                cantidad_experiencias_por_joven += joven.voluntariados_del_joven.all().count()
            nombre = 'Voluntariado'
            mensaje = 'Jóvenes con al menos 1 experiencia en un ' + nombre
            titulo_seccion = 'Experiencias en ' + nombre
            descripcion_experiencias_por_joven = 'Número de experiencias promedio en ' + nombre + ' por joven'
            descripcion_moda = 'Moda de la duración de la experiencia en ' + nombre
        elif experiencia == 'colaborador':
            cantidad_jovenes_experiencia = jovenes.filter(is_active=True).annotate(
                clubes=Count('experiencias_laborales_del_joven')).filter(clubes__gt=0).distinct().count()
            for joven in jovenes:
                cantidad_experiencias_por_joven += joven.experiencias_laborales_del_joven.all().count()
            nombre = 'Colaborador'
            mensaje = 'Jóvenes con al menos 1 experiencia como ' + nombre
            titulo_seccion = 'Experiencias como ' + nombre
            descripcion_experiencias_por_joven = 'Número de experiencias promedio como ' + nombre + ' por joven'
            descripcion_moda = 'Moda de la duración de la experiencia como ' + nombre
        elif experiencia == 'independiente':
            cantidad_jovenes_experiencia = jovenes.filter(is_active=True).annotate(
                clubes=Count('trabajos_independientes_del_joven')).filter(clubes__gt=0).distinct().count()
            for joven in jovenes:
                cantidad_experiencias_por_joven += joven.trabajos_independientes_del_joven.all().count()
            nombre = 'Independiente'
            mensaje = 'Jóvenes con al menos 1 experiencia como ' + nombre
            titulo_seccion = 'Experiencias como ' + nombre
            descripcion_experiencias_por_joven = 'Número de experiencias promedio como ' + nombre + ' por joven'
            descripcion_moda = 'Moda de la duración de la experiencia como ' + nombre
        elif experiencia == 'emprendimiento':
            cantidad_jovenes_experiencia = jovenes.filter(is_active=True).annotate(
                clubes=Count('emprendimientos_del_joven__tribu')).filter(clubes__gt=0).distinct().count()
            for joven in jovenes:
                cantidad_experiencias_por_joven += joven.emprendimientos_del_joven.all().count()
            nombre = 'Emprendimiento'
            mensaje = 'Jóvenes con al menos 1 experiencia como Empredendor'
            titulo_seccion = 'Experiencias en ' + nombre
            descripcion_experiencias_por_joven = 'Número de experiencias promedio en ' + nombre + ' por joven'
            descripcion_moda = 'Moda de la duración de la experiencia en ' + nombre

        experiencias_por_joven = 0.0
        if cantidad_jovenes_experiencia > 0:
            experiencias_por_joven = cantidad_experiencias_por_joven / cantidad_jovenes_experiencia

        porcentaje = 0.0
        if self.obtener_miembros().filter(is_active=True).count() > 0:
            porcentaje = (cantidad_jovenes_experiencia / self.obtener_miembros().filter(is_active=True).count()) * 100

        diccionario_retorno = dict()
        diccionario_retorno['nombre'] = nombre
        diccionario_retorno['cantidad'] = cantidad_jovenes_experiencia
        diccionario_retorno['porcentaje'] = porcentaje
        diccionario_retorno['experiencias_por_joven'] = experiencias_por_joven
        diccionario_retorno['moda'] = self.obtener_moda_tiempo_experiencia(experiencia)
        diccionario_retorno['mensaje'] = mensaje
        diccionario_retorno['titulo_seccion'] = titulo_seccion
        diccionario_retorno['descripcion_experiencias_por_joven'] = descripcion_experiencias_por_joven
        diccionario_retorno['descripcion_moda'] = descripcion_moda

        return diccionario_retorno

    def obtener_moda_tiempo_experiencia_nuevo(self, experiencia: str) -> int:
        """
            Obtiene la moda del tiempo experiencia

            Parámetros:                
                experiencia (str): experiencia a buscar

            Retorno:
                int: tiempo
        """

        from kairos.insignias.models import Tribus
        from django.db.models import Count, F
        from kairos.core.utils import ordenar_lista_diccionarios
        from kairos.experiencias_jovenes.models import ClubesJuveniles, Voluntariados, Laborales, Independientes, \
            Emprendimientos

        diccionario_tiempo_asociado_numero = {
            "1": "Menos de 1 año",
            "2": "Entre 1 y 2 años",
            "3": "Entre 2 y 4 años",
            "4": "Más de 5 años"
        }

        cantidad = 0

        if experiencia == 'club_juvenil':
            cantidad = list(self.jovenes.exclude(
                clubes_del_joven__isnull=True
            ).annotate(tiempo=F('clubes_del_joven__tiempo')
                       ).values('tiempo').order_by().annotate(Count('tiempo')))

    def obtener_moda_tiempo_experiencia(self, experiencia: str) -> int:
        """
            Obtiene la moda del tiempo experiencia

            Parámetros:                
                experiencia (str): experiencia a buscar

            Retorno:
                int: tiempo
        """

        from kairos.insignias.models import Tribus
        from django.db.models import Count, F
        from kairos.core.utils import ordenar_lista_diccionarios
        from kairos.experiencias_jovenes.models import ClubesJuveniles, Voluntariados, Laborales, Independientes, \
            Emprendimientos

        diccionario_tiempo_asociado_numero = {
            "1": "Menos de 1 año",
            "2": "Entre 1 y 2 años",
            "3": "Entre 2 y 4 años",
            "4": "Más de 5 años"
        }

        cantidad = 0

        if experiencia == 'club_juvenil':
            cantidad = list(self.jovenes.exclude(
                clubes_del_joven__isnull=True
            ).annotate(tiempo=F('clubes_del_joven__tiempo')
                       ).values('tiempo').order_by().annotate(Count('tiempo')))
        elif experiencia == 'voluntariado':
            cantidad = list(self.jovenes.exclude(
                voluntariados_del_joven__isnull=True
            ).annotate(tiempo=F('voluntariados_del_joven__tiempo')
                       ).values('tiempo').order_by().annotate(Count('tiempo')))
        elif experiencia == 'colaborador':
            cantidad = list(self.jovenes.exclude(
                experiencias_laborales_del_joven__isnull=True
            ).annotate(tiempo=F('experiencias_laborales_del_joven__tiempo')
                       ).values('tiempo').order_by().annotate(Count('tiempo')))
        elif experiencia == 'independiente':
            cantidad = list(self.jovenes.exclude(
                trabajos_independientes_del_joven__isnull=True
            ).annotate(tiempo=F('trabajos_independientes_del_joven__tiempo')
                       ).values('tiempo').order_by().annotate(Count('tiempo')))
        elif experiencia == 'emprendimiento':
            cantidad = list(self.jovenes.exclude(
                emprendimientos_del_joven__isnull=True
            ).annotate(tiempo=F('emprendimientos_del_joven__tiempo')
                       ).values('tiempo').order_by().annotate(Count('tiempo')))

        lista_ordenada = list(reversed(ordenar_lista_diccionarios(cantidad, 'tiempo__count')))
        if lista_ordenada:
            return diccionario_tiempo_asociado_numero[lista_ordenada[0]['tiempo']]
        else:
            return 0

    ###############################################################
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''   METODOS ESPECIFICOS PARA TABLERO DE JOVENES  '''''''
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ###############################################################

    def obtener_jovenes_activos(self) -> 'Queryset<Joven>':
        """
            Obtiene los jóvenes activos de la organización
            
            Retorno:
                Queryset<Joven>: Jóvenes de la organización activos
        """

        return self.jovenes.filter(is_active=True)

    def obtener_jovenes_activos_cohortes(self, cohorte: 'CohortesColegios', buscar_joven: str = "") -> 'Queryset<Joven>':
        """
            Obtiene los jóvenes activos de la organización perteneciente a la cohorte

            Retorno:
                Queryset<Joven>: Jóvenes activos de la organización
        """
        if not cohorte is None:
            fecha = cohorte.anio
            jovenes = self.jovenes.filter(
                is_active=True,
                ultimo_grado=cohorte.grado,
                creado__year=fecha.year,
                creado__month=fecha.month,
                creado__day=fecha.day
            )
        else:
            jovenes = self.jovenes.filter(is_active=True)
        if buscar_joven != "":
            jovenes = jovenes.filter(Q(first_name__icontains=buscar_joven) | Q(last_name__icontains=buscar_joven))

        return jovenes

    def obtener_cantidad_joveves_activos(self, id_organizaciones: 'list<int>') -> int:
        """
            Retorna los jóvenes de las organizacines recibida como parámetro

            Parámetros:                
                id_organizaciones (list<int>): organización a buscar
            
            Retorno:
                int: Cantidad jóvenes de la organización buscada
        """

        return self.obtener_miembros_de_organizaciones(id_organizaciones).count()

    def cantidad_jovenes_cuestionarios_terminados(self, miembros_organizaciones: 'Queryset<Joven>') -> int:
        """
            Retorna los jóvenes que han terminado los cuestionarios de las organizaciones recibida como parámetro

            Parámetros:                
                id_organizaciones (list<int>): organización a buscar
            
            Retorno:
                int: Cantidad jóvenes de la organización buscada que han terminado los cuestionarios
        """

        return miembros_organizaciones.filter(joven_dueno_perfil__isnull=False).order_by().count()

    def cantidad_jovenes_escenarios_deseados_favoritos(self, miembros_organizaciones: 'Queryset<Joven>') -> int:
        """
            Retorna los jóvenes que tienen escenarios favoritos de las organizaciones recibida como parámetro

            Parámetros:                
                id_organizaciones (list<int>): organización a buscar
            
            Retorno:
                int: Cantidad jóvenes que tienen escenarios favoritos
        """

        from django.db.models import Count
        from kairos.perfiles_deseados.models import PerfilesDeseados

        return PerfilesDeseados.objects.filter(joven__in=miembros_organizaciones, favorito=True).count()

    def obtener_miembros_de_organizaciones_cuestionarios_terminados(self, id_organizaciones: 'list<int>') -> int:
        """
            Retorna los jóvenes que han terminado los cuestionarios de las organizaciones recibida como parámetro

            Parámetros:                
                id_organizaciones (list<int>): organización a buscar
            
            Retorno:
                int: Cantidad jóvenes que han terminado los cuestionarios
        """

        from django.db.models import Count
        from collections import Counter

        jovenes_organizacion = self.obtener_miembros_de_organizaciones(id_organizaciones)
        ids_jovenes = set(jovenes_organizacion.filter(formulario_experiencias=True, formulario_cuestionarios=True,
                                                      formulario_basicos=True,
                                                      joven_dueno_perfil__isnull=False).values_list('usuario_ptr_id',
                                                                                                    flat=True))

        return Joven.objects.filter(pk__in=ids_jovenes).order_by()

    def promedio_edad_jovenes(self, miembros_organizaciones: 'Queryset<Joven>') -> float:
        """
            Retorna el promedio de edad de los jóvenes recibidos como parámetro.

            Parámetros:                
                miembros_organizaciones (Queryset<Joven>): Jóvenes a promediar edad
            
            Retorno:
                float: promedio edad jóvenes
        """

        from django.db.models import Avg, F
        from datetime import date

        promedio_edad = miembros_organizaciones.values('fecha_de_nacimiento').annotate(
            edad=(date.today() - F('fecha_de_nacimiento')) / 365).aggregate(promedio_edad=Avg('edad'))

        if promedio_edad['promedio_edad'] is None:
            return 0

        return promedio_edad['promedio_edad']

    def promedio_anios_educacion(self, miembros_organizaciones: 'Queryset<Joven>') -> float:
        """
            Retorna el promedio de años de educación de los jóvenes recibidos como parámetro.

            Parámetros:                
                miembros_organizaciones (Queryset<Joven>): Jóvenes a promediar educación
            
            Retorno:
                float: promedio años de educación jóvenes
        """

        from django.db.models import F, Q, Count, Sum, Value
        from django.db.models.functions import Coalesce

        if not miembros_organizaciones:
            return 0

        promedio = 0

        filtro_null = (Q(formacion_academica__isnull=True) | Q(formacion_academica='')) | (Q(ultimo_grado__isnull=True))

        miembros_con_formacion = miembros_organizaciones.exclude(filtro_null)
        cantidad_jovenes_con_registros_completos = miembros_con_formacion.count()

        profesional_bachillerato = miembros_con_formacion.filter(
            formacion_academica__in=['P', 'B']
        ).aggregate(suma_anios=Coalesce(Sum('ultimo_grado'), Value(0)))

        tecnico = miembros_con_formacion.filter(
            formacion_academica__in=['T', 'PF']
        ).annotate(anios_tecnico=F('ultimo_grado') / 2 + 11).aggregate(
            suma_anios=Coalesce(Sum('anios_tecnico'), Value(0)))

        mas = miembros_con_formacion.filter(
            formacion_academica__in=['M']
        ).annotate(anios_mas=F('ultimo_grado') + 16).aggregate(suma_anios=Coalesce(Sum('anios_mas'), Value(0)))

        suma = profesional_bachillerato['suma_anios'] + tecnico['suma_anios'] + mas['suma_anios']

        try:
            promedio = suma / cantidad_jovenes_con_registros_completos
        except ZeroDivisionError:
            promedio = 0

        return promedio

    def obtener_rangos_edades(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Count, Q, F, IntegerField
        from django.db.models.functions import Cast
        from django.utils import timezone

        from datetime import date

        joven_mayor_edad = miembros_organizaciones.order_by('fecha_de_nacimiento').first()
        maximo_anios = 101

        if joven_mayor_edad is None:
            return []

        maximo_anios = int((date.today() - joven_mayor_edad.fecha_de_nacimiento).days / 365)
        maximo_anios = min(maximo_anios, 101)

        edad_inicial = 11
        edad_final = 15
        diccionario_rangos = list()
        while (edad_final <= maximo_anios):
            diccionario_rangos.append(("{} - {}").format(edad_inicial, edad_final))
            edad_inicial = edad_final + 1
            edad_final += 5

        diccionario_genero = {
            'F': 'mujer',
            'M': 'hombre'
        }

        jovenes = list(miembros_organizaciones. \
                       annotate(
            edad=Cast((date.today() - F('fecha_de_nacimiento')) / 365, IntegerField())
        ). \
                       annotate(
            edad_estandarizada=Cast((F('edad') - 11) / 5, IntegerField())
        ). \
                       filter(edad__gte=11, edad__lte=maximo_anios).order_by(). \
                       values("genero", "edad_estandarizada").exclude(genero='LGBT').annotate(total=Count("edad")))

        return transformar_estructura_graficos(jovenes, diccionario_genero, 'genero', 'edad_estandarizada',
                                               ['edad', diccionario_rangos], lista=True)

    def obtener_ciudades_jovenes(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        """
            retorna las ciudades y la cantidad de jóvenes que viven en estas de los jóvenes recibidos como parámetros

            Parámetros:                
                miembros_organizaciones (Queryset<Joven>): Jóvenes

            Retorno:
                list<dict>: lista de diccionarios
                            {'name':<str>, 'children': list[{'name':str,'value': str}] }
        """
        from django.db.models import Count

        ciudades = list(miembros_organizaciones.order_by().values('ciudad__name_ascii').annotate(cantidad=Count('pk')))

        lista_ciudades_jovenes = []
        cantidad = 0
        for ciudad in ciudades:
            lista_ciudades_jovenes.append({'name': ciudad['ciudad__name_ascii'], 'children': [
                {'name': ciudad['ciudad__name_ascii'], 'value': ciudad['cantidad']}]})
            cantidad += ciudad['cantidad']

        return lista_ciudades_jovenes

    def obtener_informacion_familia(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Q, Count

        miembros = list(miembros_organizaciones.order_by('personas_en_hogar').values('vive_con_familia',
                                                                                     'personas_en_hogar').annotate(
            total=Count('pk')))

        aux_diccionario = dict()

        diccionario_genero = {
            True: 'si',
            False: 'no'
        }

        return transformar_estructura_graficos(miembros, diccionario_genero, 'vive_con_familia', 'personas_en_hogar',
                                               'category')

    def obtener_informacion_ingresos(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        """
            Retorna una lista de diccionarios con la información de ingresos de los miembros recibidos como parámetros

            Parámetros:                
                miembros_organizaciones (Queryset<Joven>): Jóvenes

            Retorno:
                list<dict>: lista de diccionarios
                            {'3 ó menos SMMLV':<int>, "Más de 3 y menos de 10 SMMLV":<int>, "Entre 10 y 30 SMMLV":<int>, "Más de 30 SMMLV":<int>, "No sé":<int>}
        """

        from django.db.models import Q, Count

        relacion_numero_rango_salarios = {
            "1": "3 ó menos SMMLV",
            "2": "Más de 3 y menos de 10 SMMLV",
            "3": "Entre 10 y 30 SMMLV",
            "4": "Más de 30 SMMLV",
            "5": "No sé",
        }

        miembros = list(
            miembros_organizaciones.values('ingresos_grupo_familiar').order_by('ingresos_grupo_familiar').annotate(
                cantidad=Count('pk')))

        for registro in miembros:
            registro['ingresos_grupo_familiar'] = relacion_numero_rango_salarios[registro['ingresos_grupo_familiar']]

        return miembros

    def obtener_hijos_nivel_academico(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':

        from django.db.models import Q, Count
        from kairos.core.utils import ordenar_lista_diccionario_patron_personalizado

        letra_asociada_a_formacion_academica = {
            "P": "Primaria",
            "B": "Bachillerato",
            "T": "Técnico",
            "PF": "Profesional",
            "M": "Más"
        }

        diccionario_hijos = {
            True: 'si',
            False: 'no'
        }

        miembros = miembros_organizaciones.values('hijos', 'formacion_academica').order_by().annotate(total=Count('pk'))

        lista_transformada = (
            transformar_estructura_graficos(miembros, diccionario_hijos, 'hijos', 'formacion_academica', 'category'))
        lista_transformada = ordenar_lista_diccionario_patron_personalizado(lista_transformada,
                                                                            ["P", "B", "T", "PF", "M"])

        for registro in lista_transformada:
            registro['category'] = letra_asociada_a_formacion_academica[registro['category']]
        return lista_transformada

    def estado_salud_actividad(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Q, Count

        diccionario_relacion_numero_categoria = {
            "1": "Trabajando en una empresa",
            "2": "Independiente",
            "3": "Buscando trabajo",
            "4": "Estudiando",
            "5": "Oficios del hogar",
            "6": "Ninguna",
            "7": "Otra"
        }
        diccionario_relacion_numero_nombre_actividades = {
            '0': 'NS/NR', '1': 'Mala', '2': 'Regular', '3': 'Buena', '4': 'Muy buena', '5': 'Excelente'
        }

        miembros = miembros_organizaciones.values('actividad_que_ocupo_tiempo', 'estado_salud').order_by().annotate(
            total=Count('pk'))
        aux_diccionario = dict()

        for registro in miembros:
            if registro['actividad_que_ocupo_tiempo'] not in aux_diccionario:
                aux_diccionario[registro['actividad_que_ocupo_tiempo']] = {
                    'category': diccionario_relacion_numero_categoria[registro['actividad_que_ocupo_tiempo']]}
            aux_diccionario[registro['actividad_que_ocupo_tiempo']].update(
                {diccionario_relacion_numero_nombre_actividades[registro['estado_salud']]: registro['total']})

        return list(aux_diccionario.values())

    def obtener_mayores_factores_bienestar(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Q, F, Count, Func
        from django.db import connection

        from collections import Counter

        lista_factores = Counter(miembros_organizaciones.annotate(
            factores=Func(F('factores_sensacion_bienestar'), function='unnest')).values_list('factores',
                                                                                             flat=True)).most_common()

        lista_factores_porcentaje = []
        for factor, cantidad in lista_factores[0:3]:
            porcentaje = 0
            if len(miembros_organizaciones) != 0:
                porcentaje = cantidad / len(miembros_organizaciones)
            lista_factores_porcentaje.append({factor: (porcentaje) * 100, 'cantidad_abosluta': cantidad})

        return lista_factores_porcentaje

    ####################################################################
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''   METODOS ESPECIFICOS PARA TABLERO DE COMPETENCIAS  ''''''''
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ####################################################################

    def obtener_calificacion_respuestas_joven(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Q, F, Count, Func, Value
        from kairos.calificaciones.models import CalificacionRespuestasJoven

        diccionario_lectura = {'category': 'Comprensión lectora'}
        diccionario_cuantitativo = {'category': 'Análisis cuantitativo'}

        calificaciones_cuantitativas = list(
            CalificacionRespuestasJoven.objects.exclude(resultadoD48__isnull=True).filter(
                respuestas__joven__in=miembros_organizaciones).annotate(
                desempenio=Func(F('resultadoD48'), Value('resultado'), function='jsonb_extract_path_text')
            ).values('desempenio').annotate(cantidad=Count('desempenio')))

        calificaciones_lectoras = list(CalificacionRespuestasJoven.objects.exclude(resultadoSM__isnull=True).filter(
            respuestas__joven__in=miembros_organizaciones).annotate(
            desempenio=Func(F('resultadoSM'), Value('resultado'), function='jsonb_extract_path_text')
        ).values('desempenio').annotate(cantidad=Count('desempenio')))

        for calificacion in calificaciones_cuantitativas:
            diccionario_cuantitativo[calificacion['desempenio']] = calificacion['cantidad']
        for calificacion in calificaciones_lectoras:
            diccionario_lectura[calificacion['desempenio']] = calificacion['cantidad']

        lista_retorno = []
        lista_retorno.append(diccionario_cuantitativo)
        lista_retorno.append(diccionario_lectura)
        return lista_retorno

    def obtener_cruce_cualitativo_cuantitativo(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Q, Count, F, Func, Value
        from kairos.respuestas_formularios.models import RespuestasJoven
        colors = {
            "critical": "#67b7dc",
            "bad": "#6794dc",
            "medium": "#6771dc",
            "good": '#8067dc',
            "very_good": '#a367dc',
        }

        diccionario_matriz = {
            'ALTO-BAJO': {'cantidad': 0, 'color': '#6771dc'},
            'ALTO-MEDIO': {'cantidad': 0, 'color': '#8067dc'},
            'ALTO-ALTO': {'cantidad': 0, 'color': '#a367dc'},

            'MEDIO-BAJO': {'cantidad': 0, 'color': '#6794dc'},
            'MEDIO-MEDIO': {'cantidad': 0, 'color': '#6771dc'},
            'MEDIO-ALTO': {'cantidad': 0, 'color': '#8067dc'},

            'BAJO-BAJO': {'cantidad': 0, 'color': '#67b7dc'},
            'BAJO-MEDIO': {'cantidad': 0, 'color': '#6794dc'},
            'BAJO-ALTO': {'cantidad': 0, 'color': '#6771dc'},

        }

        jovenes_con_ambas_calificaciones = miembros_organizaciones.filter(
            (Q(respuestas_joven__calificacion_respuesta__resultadoD48__isnull=True) & Q(
                respuestas_joven__calificacion_respuesta__resultadoSM__isnull=False)) |
            (Q(respuestas_joven__calificacion_respuesta__resultadoD48__isnull=False) & Q(
                respuestas_joven__calificacion_respuesta__resultadoSM__isnull=True))
        ).annotate(cantidad=Count('id')).filter(cantidad__gte=2)

        lista_prueba = []
        calificacion_numeros = list(RespuestasJoven.objects.filter(
            joven__in=jovenes_con_ambas_calificaciones, calificacion_respuesta__resultadoD48__isnull=False
        ).order_by('joven_id').values('calificacion_respuesta__resultadoD48').annotate(
            desempenio=Func(F('calificacion_respuesta__resultadoD48'), Value('resultado'),
                            function='jsonb_extract_path_text')
        ).values('desempenio'))

        calificacion_lectura = list(RespuestasJoven.objects.filter(
            joven__in=jovenes_con_ambas_calificaciones, calificacion_respuesta__resultadoSM__isnull=False
        ).order_by('joven_id').values('calificacion_respuesta__resultadoSM').annotate(
            desempenio=Func(F('calificacion_respuesta__resultadoSM'), Value('resultado'),
                            function='jsonb_extract_path_text')
        ).values('desempenio'))

        lista_desempenios = zip(calificacion_numeros, calificacion_lectura)
        for desempenio_numero, desempenio_lectura in lista_desempenios:
            diccionario_matriz["{}-{}".format(desempenio_lectura['desempenio'], desempenio_numero['desempenio'])][
                'cantidad'] += 1

        lista_retorno = []
        for cruce, valores in diccionario_matriz.items():
            lista_retorno.append({
                'y': cruce.split('-')[0],
                'x': cruce.split('-')[1],
                'color': valores['color'],
                'value': valores['cantidad']
            })

        return lista_retorno

    def obtener_desempenio_por_competencia(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Q, Count, Func, Value, F
        from kairos.calificaciones.models import CalificacionRespuestasJoven

        from collections import Counter

        lista_competencias = ['Autoconciencia', 'Autocontrol', 'Autoconfianza', 'Empatía', 'Motivación', 'Socializar']
        lista_retorno = []
        for competencia in lista_competencias:
            diccionario_auxiliar = dict()
            calificaciones_competencias = list(CalificacionRespuestasJoven.objects.exclude(
                puntajesSterret__isnull=True
            ).filter(respuestas__joven__in=miembros_organizaciones
                     ).values('puntajesSterret').annotate(
                desempenio=Func(F('puntajesSterret'), Value('competencias'), Value(competencia), Value('resultado'),
                                function='jsonb_extract_path_text')
            ).values('desempenio').annotate(cantidad=Count('desempenio')))

            diccionario_auxiliar['category'] = competencia
            for calificacion in calificaciones_competencias:
                diccionario_auxiliar[calificacion['desempenio']] = calificacion['cantidad']
            lista_retorno.append(diccionario_auxiliar)

        # print(lista_retorno)
        return lista_retorno

    def obtener_desempenio_por_habilidades_sociales(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Q, Count, Func, Value, F
        from kairos.calificaciones.models import CalificacionRespuestasJoven

        lista_habilidades = ['Carisma', 'Inspiración', 'Dirección',
                             'Desarrollo', 'Innovación',
                             'Negociación']
        lista_retorno = []

        for competencia in lista_habilidades:
            diccionario_auxiliar = dict()
            calificaciones_habilidades = list(CalificacionRespuestasJoven.objects.exclude(
                promediosLiderazgo__isnull=True
            ).filter(respuestas__joven__in=miembros_organizaciones
                     ).values('promediosLiderazgo').annotate(
                desempenio=Func(F('promediosLiderazgo'), Value('competencias'), Value(competencia), Value('resultado'),
                                function='jsonb_extract_path_text')
            ).values('desempenio').annotate(cantidad=Count('desempenio')))

            if competencia == 'Innovación':
                diccionario_auxiliar['category'] = 'Innovación'
            elif competencia == 'Negociación':
                diccionario_auxiliar['category'] = 'Claridad en negociar'
            else:
                diccionario_auxiliar['category'] = competencia

            for calificacion in calificaciones_habilidades:
                diccionario_auxiliar[calificacion['desempenio']] = calificacion['cantidad']
            lista_retorno.append(diccionario_auxiliar)

        return lista_retorno

    def obtener_jovenes_tipo_liderazgo(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Q, Count, Func, Value, F
        from kairos.calificaciones.models import CalificacionRespuestasJoven
        from kairos.core.utils import eliminar_elemento_diccionario_si_valor_cero

        calificaciones = CalificacionRespuestasJoven.objects.exclude(
            promediosLiderazgo__isnull=True
        ).filter(
            respuestas__joven__in=miembros_organizaciones
        ).values('promediosLiderazgo').annotate(
            desempenio_transaccional=Func(F('promediosLiderazgo'), Value('dimensiones'), Value('transaccional'),
                                          function='jsonb_extract_path_text')
        ).annotate(
            desempenio_transformacional=Func(F('promediosLiderazgo'), Value('dimensiones'), Value('transformacional'),
                                             function='jsonb_extract_path_text')
        ).values('desempenio_transaccional', 'desempenio_transformacional')

        desempenio_transaccional = {
            'category': 'Transaccional',
            'ALTO': calificaciones.values('desempenio_transaccional').filter(
                desempenio_transaccional__gte=4.20).count(),
            'MEDIO': calificaciones.values('desempenio_transaccional').filter(desempenio_transaccional__gte=3.65,
                                                                              desempenio_transaccional__lt=4.20).count(),
            'BAJO': calificaciones.values('desempenio_transaccional').filter(desempenio_transaccional__lt=3.65).count()
        }

        desempenio_transformacional = {
            'category': 'Transformacional',
            'ALTO': calificaciones.values('desempenio_transformacional').filter(
                desempenio_transformacional__gte=3.72).count(),
            'MEDIO': calificaciones.values('desempenio_transformacional').filter(desempenio_transformacional__gte=3.00,
                                                                                 desempenio_transformacional__lt=3.72).count(),
            'BAJO': calificaciones.values('desempenio_transformacional').filter(
                desempenio_transformacional__lt=3.00).count()
        }

        lista_retorno = []
        lista_retorno.append(desempenio_transformacional)
        lista_retorno.append(desempenio_transaccional)
        eliminar_elemento_diccionario_si_valor_cero(['ALTO', 'MEDIO', 'BAJO'], lista_retorno)
        return lista_retorno

    def nivel_aptitud_area_conocimiento(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Q, Count, Func, Value, F
        from kairos.calificaciones.models import CalificacionRespuestasJoven
        from kairos.core.utils import ordenar_lista_diccionario_patron_personalizado
        lista_areas = ['C', 'I', 'S', 'H', 'A', 'E', 'D']
        diccionario_relacion_letra_area_conocimiento = {'C': 'Administración',
                                                        'E': 'Ciencia y Medio Ambiente',
                                                        'H': 'Humanidades',
                                                        'S': 'Salud',
                                                        'I': 'Ingeniería',
                                                        'D': 'Seguridad y Defensa',
                                                        'A': 'Artes y Deportes'}

        lista_orden = ['Administración', 'Ciencia y Medio Ambiente', 'Humanidades', 'Salud', 'Ingeniería',
                       'Seguridad y Defensa', 'Artes y Deportes']
        lista_retorno = []
        for area in lista_areas:
            diccionario_auxiliar = dict()
            calificaciones_areas = list(CalificacionRespuestasJoven.objects.exclude(
                puntajesCHASIDE__isnull=True
            ).filter(
                respuestas__joven__in=miembros_organizaciones
            ).values('puntajesCHASIDE').annotate(
                desempenio=Func(F('puntajesCHASIDE'), Value(area), Value('resultado'),
                                function='jsonb_extract_path_text')
            ).values('desempenio').annotate(cantidad=Count('desempenio')))

            diccionario_auxiliar['category'] = diccionario_relacion_letra_area_conocimiento[area]
            for calificacion in calificaciones_areas:
                diccionario_auxiliar[calificacion['desempenio']] = calificacion['cantidad']
            lista_retorno.append(diccionario_auxiliar)

        return ordenar_lista_diccionario_patron_personalizado(lista_retorno, lista_orden)

    def obtener_jovenes_alto_bajo_desempenio(self, id_organizaciones):
        global CONSULTA_JOVENES
        from django.db.models import Q, Count, Func, Value, F, IntegerField
        from django.db.models.functions import Cast
        from kairos.calificaciones.models import CalificacionRespuestasJoven

        desempenio = CalificacionRespuestasJoven.objects.exclude(
            puntajesSterret__isnull=True
        ).filter(
            respuestas__joven__in=CONSULTA_JOVENES
        ).values('puntajesSterret').annotate(
            desempenio=Func(F('puntajesSterret'), Value('global'), function='jsonb_extract_path_text')
        ).values('desempenio').annotate(desempenio_int=Cast('desempenio', IntegerField()))

        jovenes = {
            'ALTO': desempenio.filter(desempenio_int__gt=120).count(),
            'BAJO': desempenio.filter(desempenio_int__lt=90).count(),
        }

        return jovenes

    def obtener_jovenes_alto_bajo_liderazgo(self, miembros_organizaciones: 'Queryset<Joven>') -> 'list<dict>':
        from django.db.models import Q, Count
        jovenes = self.obtener_jovenes_tipo_liderazgo(miembros_organizaciones)

        diccionario_retorno = {'jovenes_alto_transaccional': 0,
                               'jovenes_alto_transformacional': 0,
                               'jovenes_bajo_rendimiento': 0}

        for liderazgo in jovenes:
            if liderazgo['category'] == 'Transformacional':
                if 'BAJO' in liderazgo.keys():
                    diccionario_retorno['jovenes_bajo_rendimiento'] += liderazgo['BAJO']
                if 'ALTO' in liderazgo.keys():
                    diccionario_retorno['jovenes_alto_transformacional'] += liderazgo['ALTO']
            elif liderazgo['category'] == 'Transaccional':
                if 'BAJO' in liderazgo.keys():
                    diccionario_retorno['jovenes_bajo_rendimiento'] += liderazgo['BAJO']
                if 'ALTO' in liderazgo.keys():
                    diccionario_retorno['jovenes_alto_transaccional'] += liderazgo['ALTO']

        return diccionario_retorno

    def area_mayor_interes_jovenes(self, miembros_organizaciones):
        from django.db.models import Count
        diccionario_interes_jovenes = {'Administración': 0,
                                       'Ingeniería': 0,
                                       'Salud': 0,
                                       'Humanidades': 0,
                                       'Artes y Deportes': 0,
                                       'Ciencia y Medio Ambiente': 0,
                                       'Seguridad y Defensa': 0}

        relacion_tribu_interes = {
            "Gronor": "Administración",
            "Centeros": "Ingeniería",
            "Cuisana": "Salud",
            "Vincularem": "Humanidades",
            "Brianimus": "Artes y Deportes",
            "Moterra": "Ciencia y Medio Ambiente",
            "Ponor": "Seguridad y Defensa"}

        areas_interes = list(PerfilesDeseados.objects.filter(joven__in=miembros_organizaciones, favorito=True).values(
            'tribu_deseada__nombre').annotate(cantidad=Count('tribu_deseada__nombre')))
        for interes in areas_interes:
            diccionario_interes_jovenes[relacion_tribu_interes[interes['tribu_deseada__nombre']]] = interes['cantidad']

        intereses_ordenados_mayor_menor = list(
            reversed(sorted(diccionario_interes_jovenes.items(), key=lambda x: x[1])))
        if intereses_ordenados_mayor_menor[0][1] == 0:
            return ['-', '-']

        return intereses_ordenados_mayor_menor

    def area_mayor_menor_aptitud_jovenes(self, id_organizaciones):
        global CONSULTA_JOVENES
        from django.db.models import Q, Count, Func, Value, F
        from kairos.calificaciones.models import CalificacionRespuestasJoven
        from kairos.core.utils import ordenar_dicionario_por_valores
        lista_areas_letras = ['C', 'I', 'S', 'H', 'A', 'E', 'D']
        diccionario_relacion_letra_area_conocimiento = {'C': 'Administración',
                                                        'E': 'Ciencia y Medio Ambiente',
                                                        'H': 'Humanidades',
                                                        'S': 'Salud',
                                                        'I': 'Ingeniería',
                                                        'D': 'Seguridad y Defensa',
                                                        'A': 'Artes y Deportes'}

        lista_retorno = []
        for area in lista_areas_letras:
            diccionario_auxiliar = dict()
            calificaciones_areas = list(CalificacionRespuestasJoven.objects.exclude(
                puntajesCHASIDE__isnull=True
            ).filter(
                respuestas__joven__in=CONSULTA_JOVENES
            ).values('puntajesCHASIDE').annotate(
                interes=Func(F('puntajesCHASIDE'), Value(area), Value('interes'), function='jsonb_extract_path_text')
            ).values('interes').annotate(cantidad=Count('interes')))

            diccionario_auxiliar['category'] = diccionario_relacion_letra_area_conocimiento[area]
            diccionario_auxiliar['interes'] = {}
            for calificacion in calificaciones_areas:
                diccionario_auxiliar['interes'].update({calificacion['interes']: calificacion['cantidad']})
            if ordenar_dicionario_por_valores(diccionario_auxiliar['interes']):
                lista_retorno.append([diccionario_auxiliar['category'],
                                      ordenar_dicionario_por_valores(diccionario_auxiliar['interes'])[0]])

        mayor = "-"
        menor = "-"
        if lista_retorno:
            mayor = lista_retorno[0]
            for area in lista_retorno:
                if mayor[1][1] < area[1][1]:
                    mayor = area

            menor = lista_retorno[0]
            for area in lista_retorno:
                if menor[1][1] > area[1][1]:
                    menor = area

        return [mayor, menor]


@receiver(post_save, sender=Organizacion, dispatch_uid="minificar_imagen_organizacion")
def comprimir_imagen_organizacion(sender, **kwargs):
    from kairos.core.utils import comprimir_imagen
    if kwargs["instance"].logo:
        comprimir_imagen(kwargs["instance"].logo.path)


class CalificacionOrganizacion(models.Model):
    organizacion = models.ForeignKey(Organizacion, on_delete=models.CASCADE, verbose_name='organización calificada',
                                     related_name='calificacion_del_joven')
    usuario = models.ForeignKey('usuarios.Usuario', on_delete=models.CASCADE, verbose_name='usuario que califica',
                                related_name='usuario_calificador_organizacion')
    calificacion = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                               verbose_name='calificación: 1 me gusta, 0 no me gusta')
    history = HistoricalRecords()


class SolicitudesOrganizacionJovenes(models.Model):
    organizacion = models.ForeignKey(Organizacion, related_name='solicitudes_de_organizacion', on_delete=models.CASCADE)
    joven = models.ForeignKey(Joven, related_name='solicitudes_joven', on_delete=models.CASCADE)
    estado = models.CharField(max_length=15, verbose_name='estado')
    history = HistoricalRecords()

    @staticmethod
    def existe_solitud(organizacion, joven):
        '''
            Confirma si existe un solicitud enviada o recibida por una Organizacion o Joven.

            Parámetros:
                organizacion: Objeto del modelo Organizacion
                joven: Objeto del modelo Joven
            Retorno:
                Si existe: True
                No existe: False
        '''
        try:
            solicitud = SolicitudesOrganizacionJovenes.objects.get(organizacion=organizacion, joven=joven)
        except SolicitudesOrganizacionJovenes.DoesNotExist:
            return False
        return True

    @staticmethod
    def es_solicitud_enviada(organizacion, joven):
        '''
            Confirma si un Joven envio una solicitud a Organizacion especifica.

            Parámetros:
                organizacion: Objeto del modelo Organizacion
                joven: Objeto del modelo Joven
            Retorno:
                Si existe: True
                No existe: False
        '''
        try:
            solicitud = SolicitudesOrganizacionJovenes.objects.get(organizacion=organizacion, joven=joven)
            if solicitud.estado == 'Invitado':
                return True
            else:
                return False
        except SolicitudesOrganizacionJovenes.DoesNotExist:
            return False
        return True


class SolicitudesOrganizacionEquipos(models.Model):
    organizacion = models.ForeignKey(Organizacion, related_name='solicitudes_de_organizacion_equipo',
                                     on_delete=models.CASCADE)
    equipo = models.ForeignKey(Equipo, related_name='solicitudes_equipo_organizacion', on_delete=models.CASCADE)
    estado = models.CharField(max_length=15, verbose_name='estado')
    history = HistoricalRecords()

    @staticmethod
    def existe_solitud(organizacion, equipo):
        '''
            Confirma si existe un solicitud enviada o recibida por una Organizacion o Equipo.

            Parámetros:
                organizacion: Objeto del modelo Organizacion
                equipo: Objeto del modelo Equipo
            Retorno:
                Si existe: True
                No existe: False
        '''
        try:
            solicitud = SolicitudesOrganizacionEquipos.objects.get(organizacion=organizacion, equipo=equipo)
        except SolicitudesOrganizacionEquipos.DoesNotExist:
            return False
        return True

    @staticmethod
    def es_solicitud_enviada(organizacion, equipo):
        '''
            Confirma si un Equipo envio una solicitud a Organizacion especifica.

            Parámetros:
                organizacion: Objeto del modelo Organizacion
                equipo: Objeto del modelo Equipo
            Retorno:
                Si existe: True
                No existe: False
        '''
        try:
            solicitud = SolicitudesOrganizacionEquipos.objects.get(organizacion=organizacion, equipo=equipo)
            if solicitud.estado == 'Invitado':
                return True
            else:
                return False
        except SolicitudesOrganizacionEquipos.DoesNotExist:
            return False

    @staticmethod
    def crear_solicitud(organizacion, equipo):
        '''
            Crea y retorna la solicitud enviando organización y equipo como parametros.

            Parámetros:
                organizacion: Objeto del modelo Organizacion
                equipo: Objeto del modelo Equipo
            Retorno:
                SolicitudesOrganizacionEquipos
        '''
        solicitud = SolicitudesOrganizacionEquipos.objects.create(equipo=equipo, organizacion=organizacion,
                                                                  estado='Postulado')
        return solicitud


class InformacionJovenInvitado(models.Model):
    GRADOS = (
        ("1", "Primero"),
        ("2", "Segundo"),
        ("3", "Tercero"),
        ("4", "Cuarto"),
        ("5", "Quinto"),
        ("6", "Sexto"),
        ("7", "Septimo"),
        ("8", "Octavo"),
        ("9", "Noveno"),
        ("10", "Decimo"),
        ("11", "Once"),
    )
    organizacion = models.ForeignKey(Organizacion, related_name='organizacion_invitacion', on_delete=models.CASCADE)
    correo_joven = models.EmailField(verbose_name="correo_joven", null=True, blank=True)
    grado = models.CharField(max_length=10, choices=GRADOS, verbose_name='grado_joven', null=True, blank=True)
    fecha_induccion = models.DateField(verbose_name="fecha_induccion", blank=True, null=True)

    @staticmethod
    def existe_registro(organizacion, correo_joven):
        '''
            Confirma si existe un registro de un joven invitado por una organizacion.

            Parametros:
                organizacion: Objecto del modelo Organizacion
                correo_joven: Correo con el que se hara la busqueda
            Retorno:
                Si existe: True
                No existe: False
        '''

        registro = InformacionJovenInvitado.objects.filter(organizacion=organizacion, correo_joven=correo_joven)

        if registro.count() > 0:
            return True
        else:
            return False

    @staticmethod
    def crear_registro(organizacion, correo_joven, grado, fecha_induccion):
        """
            Crear el registro de un joven invitado por una organizacion tipo colegio con el grado y la fecha estimada de ingreso a la plataforma

            Parametros:
                organizacion: Objecto del modelo Organizacion
                email:
                grado:
                fecha_induccion
        """
        InformacionJovenInvitado.objects.create(organizacion=organizacion, correo_joven=correo_joven, grado=grado,
                                                fecha_induccion=fecha_induccion)

    def obtener_registro(organizacion, correo_joven):
        """
            Devuelve el registro de un joven invitado con su grado y fecha de induccion

            Parametros: 
                organizacion: Objeto del modelo Organizacion
                correo_joven: Correo del joven invitado con el que se hara la busqueda
            
            Retorno: Objeto de la clase InformacionJovenInvitado
        """
        registro = InformacionJovenInvitado.objects.filter(organizacion=organizacion, correo_joven=correo_joven).first()

        return registro


class CohortesColegios(models.Model):
    GRADOS = (
        ("1", "Primero"),
        ("2", "Segundo"),
        ("3", "Tercero"),
        ("4", "Cuarto"),
        ("5", "Quinto"),
        ("6", "Sexto"),
        ("7", "Septimo"),
        ("8", "Octavo"),
        ("9", "Noveno"),
        ("10", "Decimo"),
        ("11", "Once"),
        ("12", "Doce"),
    )
    organizacion = models.ForeignKey(Organizacion, related_name='organizacion_colegio', on_delete=models.CASCADE)
    grado = models.CharField(max_length=10, choices=GRADOS, verbose_name='grado_cohorte', null=True, blank=True)
    anio = models.DateField(verbose_name="fecha_cohorte", blank=True, null=True)

    @staticmethod
    def existe_registro(organizacion, grado, fecha_induccion):
        '''
            Confirma si existe un registro de una cohorte de una organizacion.

            Parametros:
                organizacion: Objecto del modelo Organizacion
                grado: Grado con el que se hara la busqueda
                fecha_induccion: fecha_induccion con el que se hara la busqueda
            Retorno:
                Si existe: True
                No existe: False
        '''

        registro = CohortesColegios.objects.filter(organizacion=organizacion, grado=grado, anio=fecha_induccion)

        if registro.count() > 0:
            return True
        else:
            return False

    @staticmethod
    def crear_registro(organizacion, grado, anio):
        """
            Crear el registro de una cohorte de una organizacion tipo colegio con el grado y el anio

            Parametros:
                organizacion: Objecto del modelo Organizacion
                anio: Anio de la cohorte
                grado: Grado de la cohortes
                
        """
        CohortesColegios.objects.create(organizacion=organizacion, anio=anio, grado=grado)

    @staticmethod
    def buscar(id_cohorte: int) -> 'CohortesColegios':
        try:
            cohorte = CohortesColegios.objects.get(id=id_cohorte)
        except CohortesColegios.DoesNotExist:
            return None

        return cohorte

    def __str__(self):
        return f"{self.grado} - {self.anio}"
