# Modulos Django
from kairos.core.mixins import MensajeMixin
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import  CreateView

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .models import SolicitudPQRS
from .forms import SolicitudPQRSForm

# Create your views here.
class CrearSolicitudPQRS(LoginRequiredMixin, MensajeMixin, CreateView):
    model = SolicitudPQRS
    form_class = SolicitudPQRSForm
    template_name = "pqrs/enviar.html"
    mensaje_exito = "Solicitud PQRS enviada correctamente"
    mensaje_error = "Ha ocurrido un error. Por favor verifique los datos ingresados."
    success_url = reverse_lazy('pqrs:enviar_pqrs')
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated == False:
            messages.error(request, "Usted no está autorizado para gestionar a los usuarios")
            return redirect("inicio")
        return super(CrearSolicitudPQRS, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):        
        from kairos.gestor_correos.envio_correos import enviar_correo, ENVIAR_SOLICITUD_PQRS
        correo_receptor = 'santiago.otero.figueredo@gmail.com'
        correo_emisor = self.request.user.email
        asunto = 'PQR - '+ form.instance.asunto
        mensaje = 'Tipo Usuario: '+ self.request.user.tipo + '\n\n' 'Correo Usuario: ' + correo_emisor + '\n\n' + 'Tipo de solicitud:' + form.instance.tipo_solicitud + '\n\n' + form.instance.descripcion
        archivo = None
        if self.request.FILES:
            archivo = self.request.FILES['archivo']
        
        enviar_correo('contame@mieslabon.com', archivo, ENVIAR_SOLICITUD_PQRS, mensaje=mensaje)
        return super(CrearSolicitudPQRS, self).form_valid(form)

    def get_initial(self):

        return {
            'correo': self.request.user.email,
        }

