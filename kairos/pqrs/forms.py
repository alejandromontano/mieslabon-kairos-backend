
# Modulos Django
from django import forms

# Modulos de plugin externos
from django_select2.forms import Select2Widget

# Modulos de otras apps

# Modulos internos de la app
from .models import SolicitudPQRS

class SolicitudPQRSForm(forms.ModelForm):
    TIPOS_SOLICITUDES = (
        ("Informar un problema", "Informar un problema"),
        ("Petición", "Petición"),
        ("Sugerencia", "Sugerencia"),
        ("Queja", "Queja"),
        ("Felicitaciones", "Felicitaciones"),
    )

    tipo_solicitud = forms.ChoiceField(
        choices=TIPOS_SOLICITUDES,
        widget=Select2Widget(),
        required=True,
        label="Tipo de Solicitud*"
    )

    def __init__(self, *args, **kwargs):
        super(SolicitudPQRSForm, self).__init__(*args, **kwargs)
        self.fields['correo'].widget.attrs['readonly'] = True


    class Meta:
        model = SolicitudPQRS
        fields = '__all__'