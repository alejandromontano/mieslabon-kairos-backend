
# Modulos Django
from django.urls import path
# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "pqrs"

urlpatterns = [
    path('enviar/', CrearSolicitudPQRS.as_view(), name='enviar_pqrs'),
]