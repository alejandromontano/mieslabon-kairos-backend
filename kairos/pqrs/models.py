# Modulos Django
from django.db import models

# Modulos de plugin externos
from simple_history.models import HistoricalRecords

# Modulos de otras apps

# Modulos internos de la app

def subir_archivo(instance, filename):
    return "solicitudes_pqrs/{}".format(
    filename.encode('ascii','ignore'),
    )
  
class SolicitudPQRS(models.Model):     
    TIPOS_SOLICITUDES = (
        ("Informar un problema", "Informar un problema"),
        ("Petición", "Petición"),
        ("Sugerencia", "Sugerencia"),
        ("Queja", "Queja"),
        ("Felicitaciones", "Felicitaciones"),
    )
    correo = models.EmailField(max_length=256, verbose_name="Email usuario*", null=False, blank=False)
    asunto = models.CharField(max_length=100, verbose_name="Asunto*", null=False, blank=False)
    descripcion = models.TextField(verbose_name="Descripción*", null=False, blank=False)    
    tipo_solicitud = models.CharField(max_length=25, choices=TIPOS_SOLICITUDES, verbose_name='Tipo de Solicitud*', default='Informar un problema', null=False, blank=False)
    archivo = models.FileField(verbose_name="Archivo adjunto", upload_to=subir_archivo, null=True, blank=True)
    history = HistoricalRecords()   


