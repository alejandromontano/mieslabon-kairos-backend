from django.apps import AppConfig


class UsuariosConfig(AppConfig):
    name = 'kairos.usuarios'
