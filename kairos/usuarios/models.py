# Modulos Django

from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator
from django.db.models.fields import IntegerField
from django.db.models.signals import post_save
from django.db.models import Q
from django.db import models
from django.dispatch import receiver

# Modulos de plugin externos
from djgeojson.fields import PointField
from simple_history.models import HistoricalRecords
from math import ceil

# Modulos de otras apps
from cities_light.models import Country, City, Region

from kairos.contenido.models import CalificacionContenido, Contenido
from kairos.formularios.models import Competencias, Formularios, SeccionVisitada, CompetenciaVisitada
from kairos.guia.models import Recurso, VerRecurso
from kairos.habilidades_del_futuro.models import Habilidad, HabilidadVisitada
from kairos.perfiles_deseados.models import PerfilesDeseados
from kairos.respuestas_formularios.models import RespuestasJoven

# Modulos internos de la app


SI_NO = (
    (True, "Sí"),
    (False, "No")
)

FACTORES_BIENESTAR = (
    ("Familia", "Familia"),
    ("Educación", "Educación"),
    ("Vida sentimental", "Vida sentimental"),
    ("Salud", "Salud"),
    ("Trabajo", "Trabajo"),
    ("Economía del hogar", "Economía del hogar"),
    ("Amigos", "Amigos"),
    ("Lugar en el que vive", "Lugar en el que vive")
)


def crear_ruta_foto_perfil(instance, filename):
    return "profile_pics/%s" % (filename.encode('ascii', 'ignore'))


class Usuario(AbstractUser):
    TIPOS_DE_USUARIOS = (
        ("Joven", "Joven"),
        ("Padre", "Padre/Madre de familia"),
        ("Organización", "Organización"),
        ("Superusuario", "Super usuario"),
        ("Jurado", "Jurado"),
    )
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)
    tipo = models.CharField(max_length=20, choices=TIPOS_DE_USUARIOS, verbose_name='tipo de usuario')
    correo_verificado = models.BooleanField(default=False, verbose_name="¿El correo electrónico ha sido veerificado?")
    es_superusuario = models.BooleanField(default=False, verbose_name="¿Es superusuario?")
    terminos = models.BooleanField(
        verbose_name='He leído y acepto los términos y condiciones de uso y el tratamiento de datos.*', default=False)
    notificaciones = models.BooleanField(default=True, verbose_name="¿Permitir notificaciones al correo electrónico?")
    notificaciones_whatsappp = models.BooleanField(default=True, verbose_name="¿Permitir notificaciones al WhatsApp?")

    tipo_identificacion = models.CharField(max_length=100, verbose_name='tipo de identificación*', null=True,
                                           blank=True)
    identificacion = models.CharField(max_length=100, verbose_name='número de identificación*', null=True, blank=True)

    telefono_regex = RegexValidator(regex=r'^\d{10,15}$',
                                    message="El número de celular debe tener entre 10 y 15 dígitos.")
    telefono = models.CharField(max_length=15, validators=[telefono_regex], verbose_name='número de celular', null=True,
                                blank=True)
    indicativo_telefono = models.CharField(max_length=10, blank=True, null=True)
    direccion = models.CharField(max_length=100, verbose_name='dirección de contacto', null=True, blank=True)
    ubicacion = PointField(null=True, verbose_name="ubicación del usuario en el mapa")
    foto_perfil = models.FileField(upload_to=crear_ruta_foto_perfil, null=True, blank=True)

    pais = models.ForeignKey(Country, on_delete=models.PROTECT, related_name='pais_del_usuario',
                             verbose_name="¿En qué país vives?*", null=True)
    region = models.ForeignKey(Region, on_delete=models.PROTECT, related_name='region_del_usuario',
                               verbose_name="¿En qué región vives actualmente?*", null=True)
    ciudad = models.ForeignKey(City, on_delete=models.PROTECT, related_name='ciudad_del_usuario',
                               verbose_name="¿En qué ciudad vives actualmente?*", null=True)
    history = HistoricalRecords()

    class Meta:
        ordering = ['first_name', 'last_name']

    def save(self, *args, **kwargs):
        self.username = self.email
        super(Usuario, self).save(*args, **kwargs)

    def aceptar_terminos(self):
        if self.terminos == True:
            return True
        else:
            return False

    @staticmethod
    def existe_usuario(id_usuario: int) -> bool:
        '''
            Confirma si un Usuario existe usando el id como parámetro de búsqueda.

            Parámetros:
                id_usuario (int): Numero del Usuario a buscar.

            Retorno:
                Si existe: True
                No existe: False
        '''

        from django.db.models import Count

        if Usuario.objects.filter(id=id_usuario).count() > 0:
            return True
        else:
            return False

    @staticmethod
    def existe_usuario_correo(correo: str) -> bool:
        '''
            Confirma si un Usuario existe usando el correo como parámetro de búsqueda.

            Parámetros:
                correo: correo del Usuario a buscar.

            Retorno:
                Si existe: True
                No existe: False
        '''
        from django.db.models import Count

        if Usuario.objects.filter(email=correo).count() > 0:
            return True
        else:
            return False

    def completar_campos(self) -> 'list<str>':
        '''
            Retorna una lista de Strings con los campos del perfil que un usuario no ha completado.

            Parámetros:
                self: El objeto Usuario que será consultado.

            Retorno:
                list<str>: Lista de Strings
        '''
        campos = []
        if self.tipo_identificacion == None or self.tipo_identificacion == '':
            campos.append('Tipo de identificación')
        if self.identificacion == None or self.identificacion == '':
            campos.append('Número de identificación')
        if self.region == None:
            campos.append('Región')
        if self.ciudad == None:
            campos.append('Ciudad')
        if self.ubicacion == None:
            campos.append('Ubicación')
        if self.telefono == None or self.telefono == '':
            campos.append('Teléfono')
        return campos

    @staticmethod
    def cargue_masivo(dataframe: 'dataframe') -> bool:
        '''
            confirma si el grupo de usuarios en el dataframe como parámetro fueron agregados correctamente.

            Parámetros:
                dataframe: Tabla con los usuarios a agregar
            
            Retorna:
                bool: True si el cargue masivo no tuvo errores, False si el cargue masivo tuvo errores
        '''
        import pandas as pd
        from kairos.organizaciones.models import Organizacion

        dataframe.columns = ["nombres", "apellidos", "email", "tipo_usuario"]
        result = True
        for index, row in dataframe.iterrows():
            id = None
            try:
                if row['tipo_usuario'] == "Superusuario":
                    superusuario = True
                else:
                    superusuario = False
                password = str(row['nombres'][0] + str(row['apellidos']).split()[0] + str(123)).lower()

                if row['tipo_usuario'] == "Padre":
                    usuario = Padre.objects.create_user(row['email'], row['email'], password,
                                                        es_superusuario=superusuario)
                elif row['tipo_usuario'] == "Organización":
                    usuario = Organizacion.objects.create_user(row['email'], row['email'], password,
                                                               es_superusuario=superusuario)
                else:
                    usuario = Joven.objects.create_user(row['email'], row['email'], password,
                                                        es_superusuario=superusuario)
                usuario.tipo = row['tipo_usuario'] if row['tipo_usuario'] != '' else 'Joven'
                usuario.first_name = row['nombres']
                usuario.last_name = row['apellidos']
                organizacion = Organizacion.obtener_principal()
                usuario.agregar_organizacion(organizacion)
                organizacion.agregar_joven(usuario)
                organizacion.save()
                usuario.save()
            except Exception as e:
                print(e)
                result = False
        return result

    @staticmethod
    def usuario_inicial():
        '''
            Crea un superusuario si no existe uno en la base de datos
        '''
        n_users = Usuario.objects.all().count()
        if n_users == 0:
            password = "admin"
            usuario = Usuario.objects.create_user('admin', 'admin@gmail.com', password, es_superusuario=True, pais_id=1,
                                                  region_id=1, ciudad_id=1)
            usuario.set_password(password)
            usuario.first_name = 'Administrador'
            usuario.is_superuser = True
            usuario.is_staff = True
            usuario.tipo = 'Superusuario'
            usuario.save()

    @staticmethod
    def obtener_usuario_email(email) -> 'Usuario':
        """
            Devuelve el objeto usuario por medio del email

            Parámetros:
                email (str) -> email que se usara para buscar al usuario

            Retorna:
                usuario (Usuario): usuario encontrado con el usuario.
        """
        usuario = Usuario.objects.get(email=email)
        return usuario

    @staticmethod
    def obtener_usuario_pk(pk) -> 'Usuario':
        """
            Devuelve el objeto usuario por medio de la pk(llave primaria)

            Parámetros:
                pk (int) -> pk que se usara para buscar al usuario

            Retorna:
                usuario (Usuario): usuario encontrado con el usuario.
        """
        usuario = Usuario.objects.get(pk=pk)
        return usuario


@receiver(post_save, sender=Usuario, dispatch_uid="minificar_imagen_usuario")
def comprimir_imagen_usuario(sender, **kwargs):
    from kairos.core.utils import comprimir_imagen
    if kwargs["instance"].foto_perfil:
        comprimir_imagen(kwargs["instance"].foto_perfil.path)


class Joven(Usuario):
    amigos = models.ManyToManyField('Joven', blank=True, related_name='amistades')
    GENEROS = (
        ("F", "Femenino"),
        ("M", "Masculino"),
    )
    INGRESOS = (
        ("1", "3 ó menos Salario mínimo legal vigente en tu país"),
        ("2", "Más de 3 y menos de 10 Salario mínimo legal vigente en tu país"),
        ("3", "Entre 10 y 30 Salario mínimo legal vigente en tu país"),
        ("4", "Más de 30 Salario mínimo legal vigente en tu país"),
        ("5", "No sé")
    )
    FORMACION = (
        ("P", "Primaria"),
        ("B", "Bachillerato"),
        ("T", "Técnico"),
        ("PF", "Profesional"),
    )
    VIVIR = (
        ("0", "No, estoy muy inconforme con mi bienestar"),
        ("1", "Casi nunca percibo bienestar"),
        ("2", "No me siento a gusto, ni a disgusto con mi bienestar"),
        ("3", "Casi siempre percibo bienestar en mi vida"),
        ("4", "Sí, me encanta mi vida")
    )
    JORNADAS = (
        ("Mañana", "Mañana"),
        ("Tarde", "Tarde"),
        ("Noche", "Noche"),
        ("Sábados", "Sábados")
    )
    ESTADOS_SALUD = (
        ("5", "Excelente"),
        ("4", "Muy buena"),
        ("3", "Buena"),
        ("2", "Regular"),
        ("1", "Mala"),
        ("0", "No sé"),
    )
    ACTIVIDADES = (
        ("1", "Trabajando en una empresa"),
        ("2", "Trabajando como independiente"),
        ("3", "Buscando trabajo"),
        ("4", "Estudiando"),
        ("5", "Oficios del hogar"),
        ("6", "Ninguna actividad en especial"),
        ("7", "Otra")
    )
    fecha_de_nacimiento = models.DateField(verbose_name='fecha de nacimiento*', null=True, blank=False)
    es_premium = models.BooleanField(verbose_name='¿Es premium?', default=False)
    tanda_formularios = models.ForeignKey('tandas_formularios.TandasFormularios', on_delete=models.CASCADE,
                                          related_name='tanda_del_joven',
                                          verbose_name="tanda de formularios asignada actualmente", null=True,
                                          blank=True)
    ######## DATOS BÁSICOS #########
    genero = models.CharField(max_length=10, choices=GENEROS, verbose_name='elige tu sexo*', null=True, blank=False)
    vive_con_familia = models.BooleanField(verbose_name='¿Vives con tu familia?', choices=SI_NO, default=False)
    personas_en_hogar = models.PositiveIntegerField(verbose_name="incluyéndote, ¿cuántas personas viven en tu hogar?*",
                                                    null=True, blank=False)
    ingresos_grupo_familiar = models.CharField(max_length=50, choices=INGRESOS,
                                               verbose_name='¿De cuánto son ingresos de tu grupo familiar?*',
                                               null=True, blank=False)
    formacion_academica = models.CharField(max_length=50, choices=FORMACION,
                                           verbose_name='¿En qué grado académico estás?*', null=True,
                                           blank=False)
    ultimo_grado = models.PositiveIntegerField(validators=[MinValueValidator(0)],
                                               verbose_name='¿En qué año/grado/semestre estás actualmente?',
                                               blank=False, null=True)
    fecha_induccion = models.DateField(verbose_name="fecha de induccion", blank=True, null=True)

    forma_vivir = models.CharField(max_length=50, choices=VIVIR, null=True, blank=False,
                                   verbose_name='¿En la actualidad sientes que tienes un buen vivir?*')
    capacitaciones = models.BooleanField(default=False, choices=SI_NO,
                                         verbose_name='Actualmente, ¿estás recibiendo algún tipo de capacitación o '
                                                      'educación formal?')
    jornadas = ArrayField(models.CharField(max_length=50, blank=True, null=True), size=4,
                          choices=JORNADAS,
                          verbose_name='Si está estudiando actualmente, ¿a qué jornada asiste?*', null=True,
                          blank=False)
    factores_sensacion_bienestar = ArrayField(models.CharField(max_length=50, blank=True, null=True), size=3,
                                              choices=FACTORES_BIENESTAR,
                                              verbose_name='¿Cuáles son los 3 factores que más afectan tu sensación '
                                                           'de tener bienestar?*', null=True, blank=False)
    estado_salud = models.CharField(max_length=50, choices=ESTADOS_SALUD,
                                    verbose_name='¿En general cómo dirías que es tu salud?*', null=True, blank=False)
    actividad_que_ocupo_tiempo = models.CharField(max_length=50, choices=ACTIVIDADES,
                                                  verbose_name='¿En qué actividad ocupaste la mayor parte del '
                                                               'tiempo en el mes pasado?*', null=True, blank=False)
    hijos = models.BooleanField(verbose_name='¿Tienes hijos?', choices=SI_NO, default=False)

    facebook = models.CharField(max_length=128, verbose_name='Red social Facebook', null=True, blank=False)
    instagram = models.CharField(max_length=128, verbose_name='Red social Instagram', null=True, blank=False)
    twitter = models.CharField(max_length=128, verbose_name='Red social Twitter', null=True, blank=False)

    # ######### EXPERIENCIA ACTUAL ############
    RENDIMIENTOS = (
        ("5", "No me va bien, pero me gusta"),
        ("4", "Soy muy bueno, se me hace fácil y me gusta"),
        ("3", "Me va bien, aun cuando no es lo que más me gusta"),
        ("2", "Normal, logro aprobar"),
        ("1", "Suelo tener dificultades, no es mi área fuerte"),
        ("0", "Ese curso nunca lo he visto")
    )
    rendimiento_fisica = models.CharField(max_length=10, choices=RENDIMIENTOS, verbose_name='Física', null=True,
                                          blank=False)
    rendimiento_informatica = models.CharField(max_length=10, choices=RENDIMIENTOS, verbose_name='Informática',
                                               null=True, blank=False)
    rendimiento_matematica = models.CharField(max_length=10, choices=RENDIMIENTOS,
                                              verbose_name='Matemática (álgebra, trigonometría, cálculo)', null=True,
                                              blank=False)
    rendimiento_historia = models.CharField(max_length=10, choices=RENDIMIENTOS, verbose_name='Historia', null=True,
                                            blank=False)
    rendimiento_literatura = models.CharField(max_length=10, choices=RENDIMIENTOS, verbose_name='Literatura', null=True,
                                              blank=False)
    rendimiento_idiomas_extranjeros = models.CharField(max_length=10, choices=RENDIMIENTOS,
                                                       verbose_name='Idiomas Extranjeros', null=True, blank=False)
    rendimiento_idiomas_nativos = models.CharField(max_length=10, choices=RENDIMIENTOS,
                                                   verbose_name='Idioma Nativo y Literatura (Español)', null=True,
                                                   blank=False)
    rendimiento_artes = models.CharField(max_length=10, choices=RENDIMIENTOS,
                                         verbose_name='Artes (plásticas, escénicas, música, digitales)', null=True,
                                         blank=False)
    rendimiento_deportes = models.CharField(max_length=10, choices=RENDIMIENTOS,
                                            verbose_name='Deportes y Educación Física', null=True, blank=False)
    rendimiento_naturales = models.CharField(max_length=10, choices=RENDIMIENTOS,
                                             verbose_name='Ciencias Naturales y Biología', null=True, blank=False)
    rendimiento_geografia = models.CharField(max_length=10, choices=RENDIMIENTOS, verbose_name='Geografía', null=True,
                                             blank=False)
    rendimiento_economia = models.CharField(max_length=10, choices=RENDIMIENTOS,
                                            verbose_name='Economía, Contabilidad o Administración', null=True,
                                            blank=False)
    rendimiento_quimica = models.CharField(max_length=10, choices=RENDIMIENTOS, verbose_name='Química', null=True,
                                           blank=False)
    rendimiento_salud = models.CharField(max_length=10, choices=RENDIMIENTOS, verbose_name='Salud del Ser Humano',
                                         null=True, blank=False)
    rendimiento_militar = models.CharField(max_length=10, choices=RENDIMIENTOS,
                                           verbose_name='Orden cerrado, Campamentos y Ordenamiento Militar en colegios',
                                           null=True, blank=False)

    formulario_experiencias = models.BooleanField(verbose_name="¿El formulario se encuentra completo?", default=False)
    formulario_basicos = models.BooleanField(verbose_name="¿El formulario se encuentra completo?", default=False)
    formulario_cuestionarios = models.BooleanField(verbose_name="¿Los cuestionarios se encuentran completos?",
                                                   default=False, null=True)

    nivel_realidad = models.CharField(max_length=10, verbose_name='Realidad',
                                      null=True, blank=False)
    history = HistoricalRecords()

    class Meta:
        ordering = ('fecha_de_nacimiento',)

    def crear_ver_recurso_si_no_existe(self, recurso: Recurso) -> None:
        '''
            Función que comprueba si existe VerRecursos relacionado con el recurso
            y el joven y lo crea de no existir en la base de datos.
        '''
        if not self.usuario_ha_visto_el_video_recurso_nombre(recurso=recurso):
            VerRecurso.crear_ver_recurso(recurso=recurso, joven=self)
        return None

    @staticmethod
    def obtener_query(filtro: str, correo_verificado: bool, estado: bool) -> tuple:
        '''
            Función que retorna el query necesario para filtrar los jovenes.
        '''

        query = (
                Q(tipo__icontains=filtro) |
                Q(first_name__icontains=filtro) |
                Q(last_name__icontains=filtro) |
                Q(email__icontains=filtro) |
                Q(correo_verificado=correo_verificado) |
                Q(creado__startswith=filtro) |
                Q(indicativo_telefono__icontains=filtro) |
                Q(telefono__icontains=filtro) |
                Q(is_active=estado)
        )
        return query

    @property
    def verificar_formularios(self) -> None:
        """
            Verifica si los formularios de datos básicos y experiencias actuales se encuentran completos.

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorna:
                usuario (Usuario): usuario encontrado con el usuario.
        """

        joven = self
        # Formulario experiencias actuales
        if self.experiencias_actuales_completos():
            joven.formulario_experiencias = True
            joven.save()
        else:
            joven.formulario_experiencias = False
            joven.save()

        # Formulario cuentanos sobre ti
        if self.datos_basicos_completos():
            joven.formulario_basicos = True
            joven.save()
        else:
            joven.formulario_basicos = False
            joven.save()

        # Cuestionarios
        if self.cuestionarios():
            joven.formulario_cuestionarios = True
            joven.save()
        else:
            joven.formulario_cuestionarios = False
            joven.save()

    @property
    def contar_formularios(self) -> 'tuple(int,int)':
        '''
            Retorna la cantidad de formularios terminados y sin terminar

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                tuple(int,int): (formularios_completados, formularios_no_completados)
        '''
        joven = self
        completados = 0
        no_completados = 0

        # Formulario experiencias
        if joven.formulario_experiencias:
            completados += 1
        elif joven.formulario_experiencias is False:
            no_completados += 1

        # Formulario cuentanos sobre ti
        if joven.formulario_basicos:
            completados += 1
        elif joven.formulario_basicos is False:
            no_completados += 1

        return completados, no_completados

    def es_lider(self) -> bool:
        '''
            Confirma si el Joven es líder de algún Equipo.

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Si es líder: True
                No es líder: False
        '''
        from kairos.equipos.models import Equipo

        if Equipo.objects.filter(lider=self.id).count() > 0:
            return True
        else:
            return False

    def usuario_ha_visto_el_video_recurso_nombre(self, recurso: 'Recurso') -> bool:
        '''
            Confirma si el Joven ha visto el video filtrado con recurso_nombre.

            Parámetros:
                self: El objeto Joven que será consultado.
                recurso (Recurso): recurso que se filtrará del Joven

            Retorno:
                Si ha visto el video: True
                No lo ha visto: False
        '''
        vio_video = self.usuario_que_ve_el_video.filter(recurso=recurso).exists()
        return vio_video

    def usuario_que_ve_el_video_recurso_nombre(self, recurso_nombre: str) -> 'VerRecurso':
        '''
            Busca en los objetos verRecurso asociado al Joven filtrando por recurso_nombre.

            Parámetros:
                self: El objeto Joven que será consultado.
                recurso_nombre (str): nombre del recurso que se filtrará del Joven

            Retorno:
                Si existe VerRecurso: VerRecurso
                No existe: None
        '''
        try:
            vio_video = self.usuario_que_ve_el_video.get(recurso__nombre=recurso_nombre)
            return vio_video
        except self.usuario_que_ve_el_video.model.DoesNotExist:
            return None

    @staticmethod
    def buscador(query: str = None, jovenes: 'Queryset<Jovenes>' = None) -> 'Queryset<Jovenes>':
        '''
            Filtra por nombre y retorna los Jóvenes recibidos como parámetro por su nombre, usando el query como parámetro de filtro.

            Parámetros:
                query (str): String que se usara para filtrar por nombre.
                jovenes (Queryset<Jovenes>): Queryset de Jóvenes.

            Retorno:
                Queryset de Jóvenes
        '''
        from django.db.models import Q

        queries = query.split(' ')
        for q in queries:
            jovenes = jovenes.filter(
                Q(first_name__icontains=q) | Q(last_name__icontains=q)).distinct()
        return jovenes

    @staticmethod
    def buscador_organizaciones_joven(ids_organizaciones: list) -> 'Queryset<Jovenes>':
        '''
            Filtra por organizaciones_joven__pk__in y retorna los Jóvenes que pertenezcan a las ids de las organizaciones.

            Parámetros:
                ids_organizaciones (list): Lista que se usara para filtrar por organizaciones_joven__pk__in.
            Retorno:
                Queryset de Jóvenes
        '''
        jovenes = Joven.objects.filter(organizaciones_joven__pk__in=ids_organizaciones)
        return jovenes

    def formularios_activos(self) -> 'Queryset<Formularios>':
        '''
            Filtra por activo y retorna los formularios activos.
            Retorno:
                Queryset de Formularios activos
        '''
        formularios = self.tanda_formularios.formularios.filter(activo=True).order_by('pk')
        return formularios

    @staticmethod
    def existe_joven_correo(correo: str) -> bool:
        '''
            Confirma si existe un Joven usando el correo como parámetro.

            Parámetros:
                correo: String que simboliza el correo por el cual se buscara al Joven

            Retorno:
                Si existe: True
                No existe: False
        '''
        from django.db.models import Count

        if Joven.objects.filter(email=correo).count() > 0:
            return True
        else:
            return False

    def obtener_porcentajes(self) -> dict:
        """
            Retorna los porcentajes de las pruebas completadas, resultados leídos,
            me gusta y avance en niveles
        """
        # formularios
        completos = self.obtener_pruebas_completadas()
        porcentaje_formularios_completados = self.obtener_porcentaje_pruebas_completadas()

        # perfiles deseados
        mis_perfiles_escogidos = PerfilesDeseados.obtener_perfil_decisiones(joven=self, decision=1).count()
        mis_perfiles = PerfilesDeseados.buscar_perfil(joven=self).count()

        # rutas y oportunidades
        contenidos_escogidos = CalificacionContenido.decisiones_tomadas(self.id).count()
        rutas_favoritas = Contenido.favoritas(self, 'ruta').count()
        oportunidades_favoritas = Contenido.favoritas(self, 'oportunidad').count()
        rutas_oportunidades_favoritos = rutas_favoritas + oportunidades_favoritas
        total_resultados_leidos, porcentaje_resultados_leidos = self.obtener_porcentaje_resultados_leidos()
        porcentaje_niveles = self.obtener_promedio_porcentaje_nivles()
        consolidado = {
            'completos': completos,
            'total_resultados_leidos': total_resultados_leidos,
            'porcentaje_resultados_leidos': porcentaje_resultados_leidos,
            'porcentaje_formularios_completados': porcentaje_formularios_completados,
            'rutas_oportunidades_favoritos': rutas_oportunidades_favoritos,
            'contenidos_escogidos': contenidos_escogidos,
            'mis_perfiles': mis_perfiles,
            'mis_perfiles_escogidos': mis_perfiles_escogidos,
            'porcentaje_niveles': porcentaje_niveles
        }
        return consolidado

    @staticmethod
    def obtener_jovenes() -> 'Queryset<jovenes>':
        '''
            Retorna todos los Jóvenes registrados.

            Retorno:
                Queryset<jovenes> : Queryset de jóvenes
        '''
        return Joven.objects.all()

    def obtener_organizaciones_joven(self) -> 'Queryset<organizacion>':
        '''
            Retorna todas las organizaciones asociadas al joven.

            Retorno:
                Queryset<organizacion> : Queryset de organizacion
        '''
        return self.organizaciones_joven.all()

    @staticmethod
    def obtener_cantidad_jovenes() -> int:
        '''
            Retorna la cantidad de Jóvenes registrados.

            Retorno:
                int: cantidad de jóvenes registrados
        '''
        from django.db.models import Count

        return Joven.obtener_jovenes().count()

    @staticmethod
    def joven_pertenece_colegio(joven):
        '''
            Mira si un joven pertenece a un colegio(tipo = 4).

            Parámetros:
                Objeto Joven
            Retorno:
                True si pertenece a un colegio, False de lo contrario
        '''
        colegio = joven.organizaciones_joven.filter(tipo_organizacion__id=4)
        if colegio.count() > 0:
            return True
        else:
            return False

    @staticmethod
    def obtener_jovenes_activos_en_ultima_semana() -> int:
        '''
            Retorna la cantidad de jóvenes que han iniciado sesión en la ultima semana.

            Retorno:
                int: cantidad de jóvenes activos en la ultima semana
        '''
        from django.utils import timezone
        from datetime import timedelta

        hoy = timezone.now()
        fecha_hace_siete_dias = hoy - timedelta(days=7)
        jovenes_activos_en_ultima_semana = Joven.objects.filter(last_login__gte=fecha_hace_siete_dias).count()
        return jovenes_activos_en_ultima_semana

    @staticmethod
    def obtener_promedio_de_ingresos_por_joven_en_la_ultima_semana() -> float:
        '''
            Retorna el promedio de ingresos por jóvenes en la ultima semana.

            Retorno:
                float: promedio de ingresos por jóvenes en la ultima semana.
        '''
        from datetime import datetime, timedelta
        from easyaudit.models import LoginEvent
        from django.utils import timezone
        from django.db.models import Count

        hoy = timezone.now()
        fecha_hace_siete_dias = hoy - timedelta(days=7)
        jovenes_activos_en_ultima_semana = LoginEvent.objects.filter(user__tipo="Joven",
                                                                     datetime__gte=fecha_hace_siete_dias,
                                                                     datetime__lte=hoy, login_type=0
                                                                     ).values("login_type").annotate(
            total_inicios=Count("user"), total_inicios_unicos=Count("user", distinct=True)).order_by()
        list(jovenes_activos_en_ultima_semana)

        promedio = 0
        total_inicios = 0
        total_inicios_unicos = 0

        for inicio in jovenes_activos_en_ultima_semana:
            total_inicios = inicio["total_inicios"]
            total_inicios_unicos = inicio["total_inicios_unicos"]
        if total_inicios_unicos > 0:
            promedio = (total_inicios / total_inicios_unicos)
        return promedio

    @staticmethod
    def jovenes_sin_iniciar_cuestionarios() -> 'list<int, float>':
        '''
            Retorna una lista con la cantidad Jóvenes que no han iniciado ninguno de sus cuestionaros y el porcentaje de estos.

            Retorno:
                list<int, float>: [cantidad_jovenes, porcentaje_jovenes]
        '''

        from django.db.models import Count, F, Q

        consulta = Joven.objects.filter(
            Q(respuestas_joven__completas=True) | Q(respuestas_joven__isnull=True)
        ).annotate(
            numero_formularios=Count('respuestas_joven__formulario')
        ).filter(
            numero_formularios=0
        ).values('id', 'numero_formularios').distinct().count()

        procentaje = 0.0

        try:
            procentaje = (consulta / Joven.obtener_jovenes().count()) * 100
        except ZeroDivisionError:
            procentaje = 0.0

        return [consulta, procentaje]

    @staticmethod
    def jovenes_iniciaron_cuestionarios() -> 'list<int, float>':
        '''
            Retorna una lista con la cantidad Jóvenes que han iniciado sus cuestionaros y el porcentaje de estos.

            Retorno:
                list<int, float>: [cantidad_jovenes, porcentaje_jovenes]
        '''

        from django.db.models import Count, F

        consulta = Joven.objects.filter(
            respuestas_joven__completas=True
        ).annotate(
            numero_formularios=Count('respuestas_joven__formulario')
        ).filter(
            numero_formularios__gt=0, numero_formularios__lt=5
        ).values('id', 'numero_formularios').distinct().count()

        procentaje = 0.0

        try:
            procentaje = (consulta / Joven.obtener_jovenes().count()) * 100
        except ZeroDivisionError:
            procentaje = 0.0

        return [consulta, procentaje]

    @staticmethod
    def obtener_jovenes_registrados_en_plataforma() -> 'list<dict>':
        '''
            Retorna una lista de diccionarios con la cantidad de Jóvenes registrados cada día con su fecha.

            Retorno:
                list<dict>: Lista de diccionarios con la fecha y cantidad de Jóvenes registrados en la fecha.
                            {'date':<fecha>, 'visits':<int>}
        '''
        from datetime import datetime, timedelta
        from easyaudit.models import LoginEvent
        from django.utils import timezone
        from django.db.models import Count

        lista_fechas = []
        consulta = Joven.objects.all().order_by('creado')
        fecha_mas_vieja = consulta.first().creado
        fecha_mas_nueva = consulta.last().creado
        cantidad_dias = (fecha_mas_nueva - fecha_mas_vieja).days

        lista_retorno = []
        for dia in range(0, cantidad_dias):
            fecha = Joven.objects.filter(creado__lte=fecha_mas_vieja).values('creado__date')
            lista_retorno.append({'date': fecha.order_by('creado__date').last()['creado__date'].strftime('%Y-%m-%d'),
                                  'visits': fecha.count()})
            fecha_mas_vieja = fecha_mas_vieja + timedelta(days=1)

        return lista_retorno

    @staticmethod
    def obtener_jovenes_nuevos_registrados_en_plataforma() -> 'list<dict>':
        '''
            Retorna una lista de diccionarios con la cantidad de Jóvenes registrados cada día con su fecha.

            Retorno:
                list<dict>: Lista de diccionarios con la fecha y cantidad de Jóvenes registrados en la fecha.
                            {'date':<fecha>, 'visits':<int>}
        '''
        from datetime import datetime, timedelta
        from easyaudit.models import LoginEvent
        from django.utils import timezone
        from django.db.models import Count, F

        jovenes_nuevos_por_dia = Joven.objects.all().values("creado__date").distinct().annotate(
            visits=Count("id", distinct=True)).order_by("creado__date")
        lista_retorno = []
        for dato in jovenes_nuevos_por_dia:
            lista_retorno.append({'date': dato['creado__date'].strftime('%Y-%m-%d'), 'visits': dato['visits']})

        return lista_retorno

    @staticmethod
    def obtener_jovenes_activos_en_plataforma() -> 'list<dict>':
        '''
            Retorna una lista de diccionarios con la cantidad de Jóvenes activos(Tomando en cuenta su ultimo inicio de sesión).

            Retorno:
                list<dict>: Lista de diccionarios con la fecha y cantidad de Jóvenes activos en la fecha
                            {'date':<fecha>, 'visits':<int>}
        '''
        from easyaudit.models import LoginEvent
        from django.db.models import Count

        jovenes_unicos_por_dia = LoginEvent.objects.filter(login_type=0, user__tipo="Joven").values(
            "datetime__date").distinct().annotate(visits=Count("user", distinct=True)).order_by("datetime__date")
        lista_retorno = []
        for dato in jovenes_unicos_por_dia:
            lista_retorno.append({'date': dato['datetime__date'].strftime('%Y-%m-%d'), 'visits': dato['visits']})

        return lista_retorno

    def obtener_sesiones_activass_en_plataforma():
        from easyaudit.models import LoginEvent
        from django.db.models import Count

        '''
            Retorna una lista de diccionarios con la cantidad de sesiones activas por dia.

            Retorno:
                Lista de diccionarios con la fecha y cantidad de sesiones activas en la fecha
        '''
        sesiones_por_dia = LoginEvent.objects.filter(login_type=0, user__tipo="Joven").values(
            "datetime__date").distinct().annotate(visits=Count("id", distinct=True)).order_by("datetime__date")
        lista_retorno = []
        for dato in sesiones_por_dia:
            lista_retorno.append({'date': dato['datetime__date'].strftime('%Y-%m-%d'), 'visits': dato['visits']})

        return lista_retorno

    @staticmethod
    def retencion_jovenes():
        """
            Funcion para obtener el porcentaje de estudiantes que iniciaron sesión por colegio durante 12 meses

            Parametros: ninguno

            Retorna: Lista de diccionarios con la información de cada organizacion tipo colegio y los pocentajes de cada mes
            organizaciones = Organizacion.objects.filter(Q(tipo_organizacion__id=4) | Q(tipo_organizacion__id=8))
        """
        from easyaudit.models import LoginEvent
        from django.db.models import Count
        from kairos.organizaciones.models import Organizacion, CohortesColegios, InformacionJovenInvitado
        from datetime import datetime, date, time, timedelta
        import math
        from kairos.core.utils import cambiar_decimales

        organizaciones = Organizacion.objects.filter(Q(tipo_organizacion__id=4) | Q(tipo_organizacion__id=8))
        organizaciones_jovenes_retencion = []
        for organizacion in organizaciones:

            cohortes = CohortesColegios.objects.filter(organizacion=organizacion)

            for cohorte in cohortes:
                fecha_inicio = cohorte.anio
                jovenes_de_la_induccion = organizacion.jovenes.filter(fecha_induccion=fecha_inicio,
                                                                      ultimo_grado=cohorte.grado)
                total_jovenes = jovenes_de_la_induccion.count()
                jovenes_mes = []

                cantidad_meses = {}

                for i in range(1, 13):
                    cantidad_meses['cantidad_mes_' + str(i)] = 0

                fechas_meses = {
                    'mes_0': fecha_inicio
                }
                for i in range(1, 13):
                    fechas_meses['mes_' + str(i)] = fechas_meses['mes_' + str(i - 1)] + timedelta(days=30)

                actividad_meses = {}
                for joven in jovenes_de_la_induccion:
                    actividad_meses['actividad_mes_1'] = LoginEvent.objects.filter(
                        login_type=0,
                        user=joven,
                        datetime__date__range=(
                            fechas_meses['mes_0'],
                            fechas_meses['mes_1']
                        )
                    ).count()

                    for i in range(2, 13):
                        actividad_meses['actividad_mes_' + str(i)] = LoginEvent.objects.filter(
                            login_type=0,
                            user=joven,
                            datetime__date__range=(
                                fechas_meses['mes_' + str(i - 1)] + timedelta(days=1),
                                fechas_meses['mes_' + str(i)]
                            )
                        ).count()

                    for i in range(1, 13):
                        if actividad_meses['actividad_mes_' + str(i)] > 0:
                            cantidad_meses['cantidad_mes_' + str(i)] = cantidad_meses['cantidad_mes_' + str(i)] + 1

                meses = {}
                if total_jovenes == 0:
                    for i in range(1, 13):
                        meses['mes_' + str(i)] = 0
                else:
                    for i in range(1, 13):
                        meses['mes_' + str(i)] = cambiar_decimales(
                            (cantidad_meses['cantidad_mes_' + str(i)] / total_jovenes) * 100
                        )

                jovenes_mes.append(meses)
                organizaciones_jovenes_retencion.append({
                    'organizacion': organizacion,
                    'grado': cohorte.grado,
                    'anio': fecha_inicio,
                    'datos': jovenes_mes,
                    'jovenes': total_jovenes
                })

        return organizaciones_jovenes_retencion

    @staticmethod
    def obtener_numero_de_inicios_de_sesion_unicos_por_dia_experiencias(modulo: str) -> 'list<dict>':
        '''
            Retorna una lista de diccionarios con la cantidad de Jóvenes activos por experiencias cada día.

            Parámetros:
                modulo (str): modulo que será buscado

            Retorno:
                list<dict>: Lista de diccionarios con la fecha y cantidad de Jóvenes activos en la fecha
                            {'date':<fecha>, 'visits':<int>}
        '''
        from easyaudit.models import RequestEvent
        from django.db.models import Count, Max

        inicios_de_sesion_por_experiencia = RequestEvent.objects.filter(method='GET', user__tipo="Joven",
                                                                        url__regex=modulo).values("datetime__date"
                                                                                                  ).distinct().annotate(
            visits=Count("id", distinct=True)).order_by("datetime__date")

        lista_retorno = []
        for dato in inicios_de_sesion_por_experiencia:
            lista_retorno.append({'date': dato['datetime__date'].strftime('%Y-%m-%d'), 'visits': dato['visits']})

        return lista_retorno

    @staticmethod
    def obtener_datos_de_tabla_de_rutas() -> 'list<dict>':
        '''
            Retorna una lista con la cantidad de ingresos, me gusta y no me gusta de las rutas existentes.

            Retorno:
                list<dict>: Lista con características de las rutas
                            {'id_<contenido>': 'id', 'nombre': <contenido.nombre>, 'ingresos': <int>, 'me_gusta': <int>, 'no_megusta': <int>}
        '''
        from kairos.contenido.models import CalificacionContenido, Contenido
        from django.db.models import Count, Avg, Q
        from kairos.core.utils import Round
        from easyaudit.models import RequestEvent
        from django.db.models import Count

        oportunidades_rutas_ingresadas = RequestEvent.objects.filter(
            Q(method='GET') & Q(user__tipo="Joven")
            & Q(url__regex=(r'/contenido/rutas/detalle/[0-9]'))
        ).values('url').annotate(cantidad=Count('url')).order_by()

        lista_oportunidades_rutas_ingresadas = []
        for ingresos in oportunidades_rutas_ingresadas:
            lista_oportunidades_rutas_ingresadas.append(
                {'id': ingresos['url'].split('/').pop(), 'cantidad': ingresos['cantidad']})

        me_gusta = CalificacionContenido.objects.filter(calificacion=1).values("contenido__id").distinct(
        ).annotate(cantidad=Count("usuario")).order_by()

        no_me_gusta = CalificacionContenido.objects.filter(calificacion=0).values("contenido__id").distinct(
        ).annotate(cantidad=Count("usuario")).order_by()

        lista_contenidos = []
        for contenido in Contenido.objects.filter(tipo='ruta'):
            lista_contenidos.append(
                {'id_' + str(contenido.id): 'id', 'nombre': contenido.nombre, 'ingresos': 0, 'me_gusta': 0,
                 'no_megusta': 0})

        for contenido in lista_contenidos:
            for ingreso in lista_oportunidades_rutas_ingresadas:
                if 'id_' + str(ingreso['id']) in contenido.keys():
                    contenido['ingresos'] = ingreso['cantidad']

            for evento in me_gusta:
                if 'id_' + str(evento['contenido__id']) in contenido.keys():
                    contenido['me_gusta'] = evento['cantidad']

            for evento in no_me_gusta:
                if 'id_' + str(evento['contenido__id']) in contenido.keys():
                    contenido['no_megusta'] = evento['cantidad']

        recursos_puntajes_datos_lista = list(lista_contenidos)
        return recursos_puntajes_datos_lista

    @staticmethod
    def obtener_datos_de_tabla_de_oportunidades() -> 'list<dict>':
        '''
            Retorna una lista con la cantidad de ingresos, me gusta y no me gusta de las oportunidades existentes.

            Retorno:
                list<dict>: Lista con características de las oportunidades
                            {'id_<contenido>': 'id', 'nombre': <contenido.nombre>, 'ingresos': <int>, 'me_gusta': <int>, 'no_megusta': <int>}
        '''
        from kairos.contenido.models import CalificacionContenido, Contenido
        from django.db.models import Count, Avg, Q
        from kairos.core.utils import Round
        from easyaudit.models import RequestEvent
        from django.db.models import Count

        oportunidades_rutas_ingresadas = RequestEvent.objects.filter(
            Q(method='GET') & Q(user__tipo="Joven")
            & Q(url__regex=(r'/contenido/oportunidades/detalle/[0-9]'))
        ).values('url').annotate(cantidad=Count('url')).order_by()

        lista_oportunidades_rutas_ingresadas = []
        for ingresos in oportunidades_rutas_ingresadas:
            lista_oportunidades_rutas_ingresadas.append(
                {'id': ingresos['url'].split('/').pop(), 'cantidad': ingresos['cantidad']})

        me_gusta = CalificacionContenido.objects.filter(calificacion=1).values("contenido__id").distinct(
        ).annotate(cantidad=Count("usuario")).order_by()

        no_me_gusta = CalificacionContenido.objects.filter(calificacion=0).values("contenido__id").distinct(
        ).annotate(cantidad=Count("usuario")).order_by()

        lista_contenidos = []
        for contenido in Contenido.objects.filter(tipo='oportunidad'):
            lista_contenidos.append(
                {'id_' + str(contenido.id): 'id', 'nombre': contenido.nombre, 'ingresos': 0, 'me_gusta': 0,
                 'no_megusta': 0})

        for contenido in lista_contenidos:
            for ingreso in lista_oportunidades_rutas_ingresadas:
                if 'id_' + str(ingreso['id']) in contenido.keys():
                    contenido['ingresos'] = ingreso['cantidad']

            for evento in me_gusta:
                if 'id_' + str(evento['contenido__id']) in contenido.keys():
                    contenido['me_gusta'] = evento['cantidad']

            for evento in no_me_gusta:
                if 'id_' + str(evento['contenido__id']) in contenido.keys():
                    contenido['no_megusta'] = evento['cantidad']

        recursos_puntajes_datos_lista = list(lista_contenidos)
        return recursos_puntajes_datos_lista

    @staticmethod
    def obtener_datos_de_tabla_de_guias() -> 'list<dict>':
        '''
            Retorna una lista con la cantidad de jóvenes que han puntuado y el promedio de calificación de cada guía

            Retorno:
                list<dict>: Lista con características de las guías
                            {'recurso__nombre': <str>, 'numero_de_jovenes_puntuado': <int>, 'calificacion_promedio': <float>}
        '''
        from kairos.guia.models import CalificacionRecurso
        from django.db.models import Count, Avg
        from kairos.core.utils import Round

        recursos_puntajes_datos = CalificacionRecurso.objects.exclude(calificacion=0).values(
            "recurso__nombre").distinct(
        ).annotate(numero_de_jovenes_puntuado=Count("usuario"),
                   calificacion_promedio=(Round(Avg("calificacion")))).order_by()

        recursos_puntajes_datos_lista = list(recursos_puntajes_datos)
        return recursos_puntajes_datos_lista

    def __str__(self):
        return self.get_full_name()

    def obtener_amigos(self) -> 'Queryset<Joven>':
        '''
            Retorna una lista de los amigos del joven

            Retorno:
                Queryset<Joven>: Queryset de Jóvenes
        '''
        return self.amistades.all()

    def buscar(id_joven: int) -> 'Joven':
        '''
            Retorna un Joven perteneciente al id_joven

            Parámetros:
                id_joven: id del joven que se buscara

            Retorno:
                Si existe: Joven
                Si no existe: None
        '''
        try:
            joven = Joven.objects.get(id=id_joven)
            return joven
        except Joven.DoesNotExist:
            return None

    def buscar_email(email: str) -> 'Joven':
        '''
            Retorna un Joven perteneciente al email

            Parámetros:
                email: correo del joven que se buscara

            Retorno:
                Si existe: Joven
                Si no existe: None
        '''
        try:
            joven = Joven.objects.get(email=email)
            return joven
        except Joven.DoesNotExist:
            return None

    def obtener_sugeridos(self) -> 'Queryset<Joven>':
        '''
            Retorna una lista de los jóvenes sugeridos como amistades

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Queryset<Joven>: Queryset de Jovenes
        '''
        from kairos.insignias.models import Roles

        mi_perfil_favorito = self.joven_dueno_perfil.filter(favorito=True)

        lista = list(self.obtener_amigos().values('id'))
        amigos = [d['id'] for d in lista]

        conjunto_jovenes_sugeridos = []
        if mi_perfil_favorito.exists():
            mi_perfil_favorito = mi_perfil_favorito.first()
            mi_tribu = mi_perfil_favorito.tribu_deseada
            mi_nivel = mi_perfil_favorito.nivel_conocimiento_deseado
            mi_voluntariado = mi_perfil_favorito.interes_voluntariado
            mi_laboral = mi_perfil_favorito.interes_trabajo
            mi_independiente = mi_perfil_favorito.interes_independiente
            mi_emprendedor = mi_perfil_favorito.interes_emprendedor
            jovenes_con_perfil_deseado = Joven.objects.exclude(id=self.id).exclude(id__in=amigos).filter(
                joven_dueno_perfil__favorito=True).distinct()
            for joven in jovenes_con_perfil_deseado:
                perfil_deseado_joven = joven.joven_dueno_perfil.filter(favorito=True).first()
                voluntariado = perfil_deseado_joven.interes_voluntariado
                laboral = perfil_deseado_joven.interes_trabajo
                independiente = perfil_deseado_joven.interes_independiente
                emprendedor = perfil_deseado_joven.interes_emprendedor
                if (
                        perfil_deseado_joven.tribu_deseada.pk == mi_tribu.pk and perfil_deseado_joven.nivel_conocimiento_deseado.pk == mi_nivel.pk) or (
                        perfil_deseado_joven.tribu_deseada.pk == mi_tribu.pk and (
                        voluntariado == mi_voluntariado and laboral == mi_voluntariado and independiente == mi_independiente and emprendedor == mi_emprendedor)):
                    conjunto_jovenes_sugeridos.append(joven.pk)

        return Joven.objects.filter(id__in=conjunto_jovenes_sugeridos)

    def obtener_jovenes_no_amigos(self) -> 'Queryset<Joven>':
        '''
            Retorna una lista de los jóvenes que no son amigos

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Queryset<Joven>: Queryset de Jovenes
        '''
        from django.db.models import Q

        usuarios = Joven.objects.filter(~Q(id__in=[o.id for o in self.obtener_amigos()])).exclude(id=self.id)
        return usuarios

    def obtener_solicitudes(self) -> 'Queryset<SolicitudAmistad>':
        '''
            Retorna una lista de las solicitudes enviadas y recibidas del joven

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Queryset<SolicitudAmistad>: Queryset SolicitudAmistad
        '''

        usuarios = self.obtener_solicitudes_amistad()
        return usuarios

    def obtener_solicitudes_amistad(self) -> 'Queryset<SolicitudAmistad>':
        '''
            Retorna las solicitudes enviadas y recibidas del joven

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                'Queryset<SolicitudAmistad>: Queryset SolicitudAmistad
        '''
        return self.solicitudes_recibidas.all()

    def es_amigo_de_usuario(self, usuario_comprobar) -> bool:
        '''
            Confirma si un joven es amigo del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.
                usuario_comprobar: Joven a comprobar

            Retorno:
                Si: True
                No: False
        '''

        return (usuario_comprobar in self.obtener_amigos())

    def obtener_lista_usuarios_solicitudes(self) -> 'Queryset<SolicitudAmistad>':
        '''
            Retorna una lista de los jóvenes que enviaron solicitudes de amistad

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Queryset<SolicitudAmistad>: Queryset de jóvenes
        '''

        solicitudes = self.obtener_solicitudes_amistad().values_list('emisor__id')
        return Joven.objects.filter(id__in=solicitudes)

    @staticmethod
    def existe_joven(id_joven) -> bool:
        '''
            Confirma si un joven existe

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Si: True
                No: False
        '''

        from django.db.models import Count

        if Joven.objects.filter(id=id_joven).count() > 0:
            return True
        else:
            return False

    def agregar_amigo(self, usuario_receptor: 'Joven') -> None:

        '''
            Agrega al usuario_receptor como amigo del usuario actual

            Parámetros:
                self: El objeto Joven que será consultado.
                usuario_receptor: Joven que será agregado.
        '''

        self.amistades.add(usuario_receptor)

    def agregar_organizacion(self, organizacion: 'Organizacion') -> None:
        '''
            Agrega al joven actual a la organización recibida como parámetro

            Parámetros:
                self: El objeto Joven que será consultado.
                organización: Organización que será agregado.
        '''

        self.organizaciones_joven.add(organizacion)

    def agregar_reto(self, reto: 'Reto') -> None:
        '''
            Agrega al joven dentro del reto recibido como parámetro

            Parámetros:
                self: El objeto Joven que sera consultado.
                reto: Reto que será agregado.
        '''

        self.retos_joven.add(reto)

    def obtener_organizaciones(self) -> 'Queryset<Organizacion>':
        '''
            Retorna una lista de las organizaciones a las que pertenece el Joven

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Queryset<Organizacion>: Queryset Organizacion
        '''
        return self.organizaciones_joven.all()

    def obtener_colegio(self) -> 'Organizacion':
        '''
            Retorna la organizacion tipo Colegio al que pertenece el Joven

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Objeto Organizacion
        '''
        return self.organizaciones_joven.filter(tipo_organizacion=4).first()

    def obtener_insignias(self) -> dict:
        '''
            Retorna una diccionario de las insignias que posee el joven

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                dict: diccionario de tribus, niveles, y roles
                      {tribu:'Queryset<Tribu>', nivel:Queryset<NivelesConocimientos>, roles:Queryset<Roles>}
        '''

        from django.db.models import Q
        from kairos.insignias.models import Roles

        if self.joven_dueno_perfil.filter(favorito=True).count() > 0:
            perfil_deseado = self.joven_dueno_perfil.filter(favorito=True).first()
        else:
            return []

        if perfil_deseado != None:
            diccionario_insignias = dict()
            diccionario_insignias['tribu'] = perfil_deseado.tribu_deseada
            diccionario_insignias['nivel'] = perfil_deseado.nivel_conocimiento_deseado

            voluntariado = perfil_deseado.interes_voluntariado
            laboral = perfil_deseado.interes_trabajo
            independiente = perfil_deseado.interes_independiente
            emprendedor = perfil_deseado.interes_emprendedor

            q = Q()
            if voluntariado == True:
                q = q | Q(nombre__icontains='voluntario')
            if laboral == True:
                q = q | Q(nombre__icontains='colaborador')
            if independiente == True:
                q = q | Q(nombre__icontains='independiente')
            if emprendedor == True:
                q = q | Q(nombre__icontains='emprendedor')

            diccionario_insignias['roles'] = list(Roles.objects.filter(q))

            return diccionario_insignias
        else:
            return []

    def datos_basicos_completos(self) -> bool:
        '''
            Confirma si los datos básicos del joven como genero, forma de vivir, personas en el hogar, ingresos grupo familiar, formación académica
            factores sensación de bienestar, estado de salud o actividad en la que ocupo tiempo ya tienen registrado un valor

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Están todos: True
                Falta alguno: False
        '''

        if self.genero is None or self.forma_vivir is None or self.personas_en_hogar is None \
                or self.ingresos_grupo_familiar is None or self.formacion_academica is None or self.factores_sensacion_bienestar is None \
                or self.estado_salud is None or self.actividad_que_ocupo_tiempo is None:
            return False
        else:
            return True

    def experiencias_actuales_completos(self) -> bool:
        '''
            Confirma si todas las experiencias actuales ya tienen registrado un dato

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Están todos: True
                Falta alguno: False
        '''

        if self.rendimiento_salud is None or self.rendimiento_militar is None or \
                self.rendimiento_economia is None or self.rendimiento_quimica is None or \
                self.rendimiento_geografia is None or self.rendimiento_naturales is None or \
                self.rendimiento_deportes is None or self.rendimiento_artes is None or \
                self.rendimiento_idiomas_extranjeros is None or self.rendimiento_idiomas_nativos is None or \
                self.rendimiento_matematica is None or self.rendimiento_literatura is None or \
                self.rendimiento_historia is None or self.rendimiento_fisica is None or \
                self.rendimiento_informatica is None:
            return False
        else:
            return True

    def factores_de_sensacion_bienestar(self) -> 'list<str>':
        '''
            Retorna una lista con los factores de bienestar que posee el joven

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Lista Array
        '''

        factores = []
        FACTORES = dict(FACTORES_BIENESTAR)
        if not self.factores_sensacion_bienestar is None:
            for factor in self.factores_sensacion_bienestar:
                factores.append(FACTORES[factor])
        return factores

    def cuestionarios(self) -> bool:
        '''
            Confirma si el Joven completo todos los cuestionarios de su tanda

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Están todos: True
                Falta alguno: False
        '''
        from kairos.respuestas_formularios.models import RespuestasJoven

        completos = RespuestasJoven.objects.filter(joven=self, completas=True).distinct('formulario').count()

        if completos == 5:
            return True
        else:
            return False

    def obtener_datos_tabla_aptitudes_e_intereses(self) -> 'list<dict>':
        '''
            Retorna una lista de diccionarios con el nombre de la aptitud y el resultado

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                list<dict>: Lista de diccionarios
                            {"nombre": <str>, "resultado": <float>]}
        '''
        diccionario_puntajes = {'BAJO': 0, 'MEDIO': 1, 'ALTO': 2}
        diccionario_relacion_letra_area_conocimiento = {
            'C': 'Administración',
            'I': 'Ingeniería',
            'S': 'Salud',
            'H': 'Humanidades',
            'A': 'Artes y Deportes',
            'E': 'Ciencia y Medio Ambiente',
            'D': 'Seguridad y Defensa'
        }
        aptitudes_e_intereses = []
        for repuesta in self.respuestas_joven.all():
            for calificacion in repuesta.calificacion_respuesta.exclude(puntajesCHASIDE__isnull=True).values(
                    'puntajesCHASIDE'):
                for puntaje in calificacion["puntajesCHASIDE"].items():
                    dic_aux = {"nombre": diccionario_relacion_letra_area_conocimiento[puntaje[0]],
                               "resultado": diccionario_puntajes[puntaje[1]["resultado"]]}
                    aptitudes_e_intereses.append(dic_aux)

        lista_ordenada = list(reversed(sorted(aptitudes_e_intereses, key=lambda x: x["resultado"])))

        return lista_ordenada

    @staticmethod
    def obtener_aptitudes_altas(listado_de_aptitudes):
        '''
            Retorna una lista con el nombre de las aptitudes altas

            Parámetros:
                listado_de_aptitudes: Lista con las aptitudes del joven

            Retorno:
                Lista de diccionarios
        '''
        listado_altas = []
        listado_separado = []
        for aptitud in listado_de_aptitudes:
            if aptitud["resultado"] == 2:
                listado_altas.append(aptitud["nombre"])
        listado_separado = ", ".join(listado_altas)
        return listado_separado

    @staticmethod
    def obtener_aptitudes_medias(listado_de_aptitudes):
        '''
            Retorna una lista con el nombre de las aptitudes medias

            Parámetros:
                listado_de_aptitudes: Lista con las aptitudes del joven

            Retorno:
                Lista de diccionarios
        '''
        listado_medias = []
        listado_separado = []
        for aptitud in listado_de_aptitudes:
            if aptitud["resultado"] == 1:
                listado_medias.append(aptitud["nombre"])
        listado_separado = ", ".join(listado_medias)
        return listado_separado

    @staticmethod
    def obtener_aptitudes_bajas(listado_de_aptitudes):
        '''
            Retorna una lista con el nombre de las aptitudes bajas

            Parámetros:
                listado_de_aptitudes: Lista con las aptitudes del joven

            Retorno:
                Lista de diccionarios
        '''
        listado_bajas = []
        listado_separado = []
        for aptitud in listado_de_aptitudes:
            if aptitud["resultado"] == 0:
                listado_bajas.append(aptitud["nombre"])
        listado_separado = ", ".join(listado_bajas)
        return listado_separado

    def obtener_competencias_procesamiento(self) -> 'list<int>':
        '''
            Retorna los resultados de lectura y análisis con números

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                Queryset<RespuestasJoven>: respuestas SM y D48
        '''
        from django.db.models import F, Func, Value
        from kairos.formularios.models import Competencias
        from itertools import chain

        resultadoSM = self.respuestas_joven.filter(calificacion_respuesta__resultadoSM__isnull=False).annotate(
            desempenio_SM=Func(F('calificacion_respuesta__resultadoSM'), Value('resultado'),
                               function='jsonb_extract_path_text')).filter(
            formulario__ambito__competencias__pk=1).values('formulario__ambito__competencias')

        resultadoD48 = self.respuestas_joven.filter(calificacion_respuesta__resultadoD48__isnull=False).annotate(
            desempenio_SM=Func(F('calificacion_respuesta__resultadoD48'), Value('resultado'),
                               function='jsonb_extract_path_text')).filter(
            formulario__ambito__competencias__pk=2).values('formulario__ambito__competencias')

        lista_competencias = []
        for resultado in resultadoSM:
            lista_competencias.append(resultado['formulario__ambito__competencias'])

        for resultado in resultadoD48:
            lista_competencias.append(resultado['formulario__ambito__competencias'])

        return lista_competencias

    def obtener_competencias_habilidades_sociales(self) -> 'list<dict>':
        '''
            Retorna una lista_competencias con los habilidades sociales del joven

            Parámetros:
                self: El objeto Joven que sera consultado.

            Retorno:
                list<dict>: lista de diccionarios
                            {'competencia': <competencia>, 'resultado': <str>}
        '''
        from django.db.models import F, Func, Value
        from kairos.formularios.models import Ambito
        import json

        calificaciones_habilidades_liderazgo = self.respuestas_joven.exclude(
            calificacion_respuesta__promediosLiderazgo__isnull=True).values(
            'calificacion_respuesta__promediosLiderazgo').annotate(
            cualidades=Func(F('calificacion_respuesta__promediosLiderazgo'),
                            Value('competencias'), function='jsonb_extract_path_text')).values('cualidades')

        calificaciones_habilidades_emocionales = self.respuestas_joven.exclude(
            calificacion_respuesta__puntajesSterret__isnull=True).values(
            'calificacion_respuesta__puntajesSterret').annotate(
            cualidades=Func(F('calificacion_respuesta__puntajesSterret'),
                            Value('competencias'), function='jsonb_extract_path_text')).values('cualidades')

        ambito_liderazgo = Ambito.objects.get(nombre__iexact='Liderazgo')
        ambito_emocional = Ambito.objects.get(nombre__iexact='Inteligencia Emocional')

        competenciasLiderazgo = ambito_liderazgo.competencias.filter(activo=True)
        competenciasEmocionales = ambito_emocional.competencias.filter(activo=True)

        lista_competencias = []

        if (calificaciones_habilidades_emocionales.exists() == True) and (
                calificaciones_habilidades_emocionales.exists() == True):
            dic_aux_liderazgo = json.loads(calificaciones_habilidades_liderazgo[0]['cualidades'])
            dic_aux_emocionales = json.loads(calificaciones_habilidades_emocionales[0]['cualidades'])

            for competencia in competenciasLiderazgo:
                if competencia.nombre in dic_aux_liderazgo:
                    lista_competencias.append(
                        {'competencia': competencia, 'resultado': dic_aux_liderazgo[competencia.nombre]['resultado']})
                else:
                    lista_competencias.append({'competencia': competencia, 'resultado': '-'})

            for competencia in competenciasEmocionales:
                if competencia.nombre in dic_aux_emocionales:
                    lista_competencias.append(
                        {'competencia': competencia, 'resultado': dic_aux_emocionales[competencia.nombre]['resultado']})
                else:
                    lista_competencias.append({'competencia': competencia, 'resultado': '-'})

        return lista_competencias

    def obtener_competencias_aptitudes_e_intereses(self) -> 'list<dict>':
        '''
            Retorna una lista con los resultados de las aptitudes e intereses del joven

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                list<dict>: lista de diccionarios
                            {"competencia": <competencia>, "nombre": <str>, "resultado": <int>}
        '''
        from django.db.models import F, Func, Value
        from kairos.formularios.models import Ambito, Competencias

        diccionario_relacion_letra_con_competencia = {'C': 'Gronor',
                                                      'E': 'Moterra',
                                                      'H': 'Vincularem',
                                                      'S': 'Cuisaná',
                                                      'I': 'Centeros',
                                                      'D': 'Ponor',
                                                      'A': 'Brianimus'}
        diccionario_puntajes = {'BAJO': 0, 'MEDIO': 1, 'ALTO': 2}

        competencias = Competencias.objects.filter(nombre__in=diccionario_relacion_letra_con_competencia.values())
        aptitudes_e_intereses = []

        for repuesta in self.respuestas_joven.all():
            for calificacion in repuesta.calificacion_respuesta.exclude(puntajesCHASIDE__isnull=True).values(
                    'puntajesCHASIDE'):
                for puntaje in calificacion["puntajesCHASIDE"].items():
                    for competencia in competencias:
                        if diccionario_relacion_letra_con_competencia[puntaje[0]] == competencia.nombre:
                            dic_aux = {"competencia": competencia,
                                       "nombre": diccionario_relacion_letra_con_competencia[puntaje[0]],
                                       "resultado": diccionario_puntajes[puntaje[1]["resultado"]]}
                            aptitudes_e_intereses.append(dic_aux)

        lista_ordenada = list(reversed(sorted(aptitudes_e_intereses, key=lambda x: x["resultado"])))

        return lista_ordenada

    def obtener_listado_de_experiencias(self) -> 'list<str>':
        '''
            Retorna una lista con las experiencias del joven

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                list<str>: listado de nombre de experiencias
        '''

        from kairos.formularios.models import Competencias

        lista_aux_de_experiencias = [
            'Experiencia como EMPRENDEDOR', 'Experiencia como CLUBES JUVENILES', 'Experiencia como VOLUNTARIADO',
            'Experiencia como COLABORADOR', 'Experiencia Academica Colegio', 'Experiencia como INDEPENDIENTE'
        ]
        competencias = Competencias.objects.all()
        listado_de_experiencias = []
        for competencia in competencias:
            if competencia.nombre in lista_aux_de_experiencias:
                listado_de_experiencias.append(competencia)

        return listado_de_experiencias

    def obtener_promedio_de_experiencias_clubes_juveniles(self) -> 'list<dict>':
        '''
            Retorna un diccionario con la categoría, promedio de mi eslabón, promedio de jóvenes con la misma edad y
            y la cantidad de clubes juveniles del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                'list<dict>': lista de diccionarios
                              {'categoria': <categoria>, 'mi_eslabon': <float>, 'jovenes_con_misma_edad': <float>, 'Tu': <int>}
        '''

        from kairos.experiencias_jovenes.models import ClubesJuveniles
        from django.db.models import Count, Q, F, IntegerField
        from django.db.models.functions import Cast
        from datetime import date

        categoria = 'Clubes Juveniles'

        edad = self.obtener_edad_joven()

        numero_de_jovenes = Joven.objects.all().count()
        numero_de_jovenes_misma_edad = Joven.objects.all().values('fecha_de_nacimiento').annotate(
            edad=(date.today() - F('fecha_de_nacimiento')) / 365
        ).annotate(
            edad_int=Cast('edad', IntegerField())
        ).filter(Q(edad_int=edad)).count()
        numero_de_clubes = ClubesJuveniles.objects.all().count()
        clubes_juveniles = ClubesJuveniles.objects.all()

        listado_de_clubes_por_edad = []

        for club in clubes_juveniles:
            joven = club.joven
            if edad == joven.obtener_edad_joven():
                listado_de_clubes_por_edad.append(club)

        numero_de_clubes_por_edad = 0
        if len(listado_de_clubes_por_edad) != 0:
            numero_de_clubes_por_edad = len(listado_de_clubes_por_edad)

        numero_de_clubes_del_joven = ClubesJuveniles.objects.filter(joven=self).count()

        promedio_mi_eslabon = (numero_de_clubes / numero_de_jovenes)
        promedio_jovenes_edad = (numero_de_clubes_por_edad / numero_de_jovenes_misma_edad)

        listado_experiencias = []

        listado_experiencias.append(
            {'categoria': categoria, 'mi_eslabon': promedio_mi_eslabon, 'jovenes_con_misma_edad': promedio_jovenes_edad,
             'Tu': numero_de_clubes_del_joven})
        return listado_experiencias

    def obtener_promedio_de_experiencias_emprendedor(self) -> 'list<dict>':
        '''
            Retorna un diccionario con la categoria, promedio de mi eslabon, promedio de jovenes con la misma edad y
            y la cantidad de emprendimientos del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                'list<dict>': lista de diccionarios
                              {'categoria': <categoria>, 'mi_eslabon': <float>, 'jovenes_con_misma_edad': <float>, 'Tu': <int>}
        '''
        from kairos.experiencias_jovenes.models import Emprendimientos
        from django.db.models import Count, Q, F, IntegerField
        from django.db.models.functions import Cast
        from datetime import date

        categoria = 'Emprendedor'
        edad = self.obtener_edad_joven()

        numero_de_jovenes = Joven.objects.all().count()
        numero_de_jovenes_misma_edad = Joven.objects.all().values('fecha_de_nacimiento').annotate(
            edad=(date.today() - F('fecha_de_nacimiento')) / 365
        ).annotate(
            edad_int=Cast('edad', IntegerField())
        ).filter(Q(edad_int=edad)).count()
        numero_de_emprendimientos = Emprendimientos.objects.all().count()
        emprendimientos = Emprendimientos.objects.all()

        listado_de_emprendimiento_por_edad = []

        for emprendimiento in emprendimientos:
            joven = emprendimiento.joven
            if edad == joven.obtener_edad_joven():
                listado_de_emprendimiento_por_edad.append(emprendimiento)

        numero_de_emprendimientos_por_edad = 0
        if len(listado_de_emprendimiento_por_edad) != 0:
            numero_de_emprendimientos_por_edad = len(listado_de_emprendimiento_por_edad)

        numero_de_emprendimientos_del_joven = Emprendimientos.objects.filter(joven=self).count()

        promedio_mi_eslabon = (numero_de_emprendimientos / numero_de_jovenes)
        promedio_jovenes_edad = (numero_de_emprendimientos_por_edad / numero_de_jovenes_misma_edad)

        listado_experiencias = []

        listado_experiencias.append(
            {'categoria': categoria, 'mi_eslabon': promedio_mi_eslabon, 'jovenes_con_misma_edad': promedio_jovenes_edad,
             'Tu': numero_de_emprendimientos_del_joven})
        return listado_experiencias

    def obtener_promedio_de_experiencias_voluntariado(self) -> 'list<dict>':
        '''
            Retorna un diccionario con la categoría, promedio de mi eslabón, promedio de jóvenes con la misma edad y
            y la cantidad de voluntariados del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                'list<dict>': lista de diccionarios
                              {'categoria': <categoria>, 'mi_eslabon': <float>, 'jovenes_con_misma_edad': <float>, 'Tu': <int>}
        '''

        from kairos.experiencias_jovenes.models import Voluntariados
        from django.db.models import Count, Q, F, IntegerField
        from django.db.models.functions import Cast
        from datetime import date

        categoria = 'Voluntariado'
        edad = self.obtener_edad_joven()

        numero_de_jovenes = Joven.objects.all().count()
        numero_de_jovenes_misma_edad = Joven.objects.all().values('fecha_de_nacimiento').annotate(
            edad=(date.today() - F('fecha_de_nacimiento')) / 365
        ).annotate(
            edad_int=Cast('edad', IntegerField())
        ).filter(Q(edad_int=edad)).count()
        numero_de_voluntariados = Voluntariados.objects.all().count()
        voluntariados = Voluntariados.objects.all()

        listado_de_voluntariados_por_edad = []

        for voluntariado in voluntariados:
            joven = voluntariado.joven
            if edad == joven.obtener_edad_joven():
                listado_de_voluntariados_por_edad.append(voluntariado)

        numero_de_voluntariados_por_edad = 0
        if len(listado_de_voluntariados_por_edad) != 0:
            numero_de_voluntariados_por_edad = len(listado_de_voluntariados_por_edad)

        numero_de_voluntariados_del_joven = Voluntariados.objects.filter(joven=self).count()

        promedio_mi_eslabon = (numero_de_voluntariados / numero_de_jovenes)
        promedio_jovenes_edad = (numero_de_voluntariados_por_edad / numero_de_jovenes_misma_edad)

        listado_experiencias = []

        listado_experiencias.append(
            {'categoria': categoria, 'mi_eslabon': promedio_mi_eslabon, 'jovenes_con_misma_edad': promedio_jovenes_edad,
             'Tu': numero_de_voluntariados_del_joven})
        return listado_experiencias

    def obtener_promedio_de_experiencias_colaborador(self) -> 'list<dict>':

        '''
            Retorna un diccionario con la categoría, promedio de mi eslabón, promedio de jóvenes con la misma edad y
            y la cantidad de colaborador del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                'list<dict>': lista de diccionarios
                              {'categoria': <categoria>, 'mi_eslabon': <float>, 'jovenes_con_misma_edad': <float>, 'Tu': <int>}
        '''

        from kairos.experiencias_jovenes.models import Laborales
        from django.db.models import Count, Q, F, IntegerField
        from django.db.models.functions import Cast
        from datetime import date

        categoria = 'Colaborador'
        edad = self.obtener_edad_joven()

        numero_de_jovenes = Joven.objects.all().count()
        numero_de_jovenes_misma_edad = Joven.objects.all().values('fecha_de_nacimiento').annotate(
            edad=(date.today() - F('fecha_de_nacimiento')) / 365
        ).annotate(
            edad_int=Cast('edad', IntegerField())
        ).filter(Q(edad_int=edad)).count()
        numero_de_laborales = Laborales.objects.all().count()
        laborales = Laborales.objects.all()

        listado_de_laborales_por_edad = []

        for laboral in laborales:
            joven = laboral.joven
            if edad == joven.obtener_edad_joven():
                listado_de_laborales_por_edad.append(laboral)

        numero_de_laborales_por_edad = 0
        if len(listado_de_laborales_por_edad) != 0:
            numero_de_laborales_por_edad = len(listado_de_laborales_por_edad)

        numero_de_laborales_del_joven = Laborales.objects.filter(joven=self).count()

        promedio_mi_eslabon = (numero_de_laborales / numero_de_jovenes)
        promedio_jovenes_edad = (numero_de_laborales_por_edad / numero_de_jovenes_misma_edad)

        listado_experiencias = []

        listado_experiencias.append(
            {'categoria': categoria, 'mi_eslabon': promedio_mi_eslabon, 'jovenes_con_misma_edad': promedio_jovenes_edad,
             'Tu': numero_de_laborales_del_joven})
        return listado_experiencias

    def obtener_promedio_de_experiencias_independiente(self) -> 'list<dict>':
        '''
            Retorna un diccionario con la categoría, promedio de mi eslabón, promedio de jóvenes con la misma edad y
            y la cantidad de emprendimientos independientes del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                'list<dict>': lista de diccionarios
                              {'categoria': <categoria>, 'mi_eslabon': <float>, 'jovenes_con_misma_edad': <float>, 'Tu': <int>}
        '''

        from kairos.experiencias_jovenes.models import Independientes
        from django.db.models import Count, Q, F, IntegerField
        from django.db.models.functions import Cast
        from datetime import date

        categoria = 'Colaborador'
        edad = self.obtener_edad_joven()

        numero_de_jovenes = Joven.objects.all().count()
        numero_de_jovenes_misma_edad = Joven.objects.all().values('fecha_de_nacimiento').annotate(
            edad=(date.today() - F('fecha_de_nacimiento')) / 365
        ).annotate(
            edad_int=Cast('edad', IntegerField())
        ).filter(Q(edad_int=edad)).count()
        numero_de_independientes = Independientes.objects.all().count()
        independientes = Independientes.objects.all()

        listado_de_independientes_por_edad = []

        for independiente in independientes:
            joven = independiente.joven
            if edad == joven.obtener_edad_joven():
                listado_de_independientes_por_edad.append(independiente)

        numero_de_independientes_por_edad = 0
        if len(listado_de_independientes_por_edad) != 0:
            numero_de_independientes_por_edad = len(listado_de_independientes_por_edad)

        numero_de_independientes_del_joven = Independientes.objects.filter(joven=self).count()

        promedio_mi_eslabon = (numero_de_independientes / numero_de_jovenes)
        promedio_jovenes_edad = (numero_de_independientes_por_edad / numero_de_jovenes_misma_edad)

        listado_experiencias = []

        listado_experiencias.append(
            {'categoria': categoria, 'mi_eslabon': promedio_mi_eslabon, 'jovenes_con_misma_edad': promedio_jovenes_edad,
             'Tu': numero_de_independientes_del_joven})
        return listado_experiencias

    def obtener_promedios_academicos(self, tag) -> 'list<dict>':
        """
            Retorna una listad e diccionarios con los promedios académicos propios y de otros jóvenes con mi edad.

            Retorno:
                'list<dict>': lista de diccionarios
                              {'valor0': <float>, 'valor1': <float>, 'valor2': <float>, 'valor3': <float>, 'valor4': <float>}
        """

        from django.db.models import Count, Q, F, IntegerField
        from django.db.models.functions import Cast
        from datetime import date

        jovenes_en_mi_eslabon = Joven.objects.all()
        edad = self.obtener_edad_joven()

        listado_experiencias = {
            'Física': 'rendimiento_fisica', 'Informática': 'rendimiento_informatica',
            'Matemática': 'rendimiento_matematica', 'Historia': 'rendimiento_historia',
            'Literatura': 'rendimiento_literatura',
            'Idiomas Extranjeros': 'rendimiento_idiomas_extranjeros',
            'Idioma Nativo y Literatura': 'rendimiento_idiomas_nativos', 'Artes': 'rendimiento_artes',
            'Deportes y Educación Física': 'rendimiento_deportes',
            'Ciencias Naturales y Biología': 'rendimiento_naturales', 'Geografía': 'rendimiento_geografia',
            'Economía, Contabilidad o Administracion': 'rendimiento_economia', 'Química': 'rendimiento_quimica',
            'Salud del Ser Humano': 'rendimiento_salud',
            'Orden cerrado, Campamentos y Ordenamiento Militar en colegios': 'rendimiento_militar'
        }
        parameters = [listado_experiencias[tag]]
        null_filters = {"{}__isnull".format(param): True for param in parameters}
        rendimiento_y_cantidad = Joven.objects.all().values(*parameters).exclude(**null_filters).annotate(
            cantidad=Count(*parameters)).order_by()
        ren = Joven.objects.all().values(*parameters, 'fecha_de_nacimiento').exclude(**null_filters).annotate(
            cantidad=Count(*parameters)).order_by()

        rendimiento_y_cantidad_jovenes_misma_edad = Joven.objects.all().values(*parameters,
                                                                               'fecha_de_nacimiento').exclude(
            **null_filters).annotate(
            edad=(date.today() - F('fecha_de_nacimiento')) / 365
        ).annotate(
            edad_int=Cast('edad', IntegerField())
        ).annotate(cantidad=Count(*parameters)
                   ).filter(
            Q(edad_int=edad)
        ).exclude(id=self.id).order_by()

        listado_rendimientos = []
        dic_eslabon = {
            'categoria': 'Mi eslabón'
        }
        dic_jovenes = {
            'categoria': 'Jovenes Misma Edad'
        }
        for rendimiento in rendimiento_y_cantidad:
            if rendimiento[listado_experiencias[tag]] == '0':
                dic_eslabon.update({'valor0': rendimiento['cantidad']})
            elif rendimiento[listado_experiencias[tag]] == '1':
                dic_eslabon.update({'valor1': rendimiento['cantidad']})
            elif rendimiento[listado_experiencias[tag]] == '2':
                dic_eslabon.update({'valor2': rendimiento['cantidad']})
            elif rendimiento[listado_experiencias[tag]] == '3':
                dic_eslabon.update({'valor3': rendimiento['cantidad']})
            elif rendimiento[listado_experiencias[tag]] == '4':
                dic_eslabon.update({'valor4': rendimiento['cantidad']})

        for rendimiento in rendimiento_y_cantidad_jovenes_misma_edad:
            if rendimiento[listado_experiencias[tag]] == '0':
                dic_jovenes.update({'valor0': rendimiento['cantidad']})
            elif rendimiento[listado_experiencias[tag]] == '1':
                dic_jovenes.update({'valor1': rendimiento['cantidad']})
            elif rendimiento[listado_experiencias[tag]] == '2':
                dic_jovenes.update({'valor2': rendimiento['cantidad']})
            elif rendimiento[listado_experiencias[tag]] == '3':
                dic_jovenes.update({'valor3': rendimiento['cantidad']})
            elif rendimiento[listado_experiencias[tag]] == '4':
                dic_jovenes.update({'valor4': rendimiento['cantidad']})

        listado_rendimientos.append(dic_eslabon)
        listado_rendimientos.append(dic_jovenes)
        return listado_rendimientos

    def obtener_puntaje_analisis_en_lectura(self) -> int:
        '''
            Retorna la cantidad de respuestas correctas de análisis de lectura del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                int: respuestas_correctas
        '''

        respuestas_joven = self.respuestas_joven.all()
        respuestas_correctas = 0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(resultadoSM__isnull=True).values(
                "resultadoSM")
            if calificaciones_respuesta.exists():
                for respuesta in calificaciones_respuesta:
                    respuestas_correctas = respuesta['resultadoSM']['correctas']
        return respuestas_correctas

    def obtener_puntaje_razonamiento_cuantitativo(self) -> int:
        '''
            Retorna la cantidad de respuestas correctas de análisis cuantitativo del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                int: respuestas_correctas
        '''

        respuestas_joven = self.respuestas_joven.all()
        respuestas_correctas = 0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(resultadoD48__isnull=True).values(
                "resultadoD48")
            if calificaciones_respuesta.exists():
                for respuesta in calificaciones_respuesta:
                    respuestas_correctas = respuesta['resultadoD48']['correctas']
        return respuestas_correctas

    def obtener_porcentaje_razonamiento_cuantitativo(self) -> float:
        '''
            Retorna el puntaje de análisis cuantitativo del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                float: puntaje
        '''

        respuestas_joven = self.respuestas_joven.all()
        puntaje = 0.0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(resultadoD48__isnull=True).values(
                "resultadoD48")
            if calificaciones_respuesta.exists():
                for respuesta in calificaciones_respuesta:
                    puntaje = respuesta['resultadoD48']['porcentaje']
        return puntaje

    def obtener_promedio_liderazgo(self) -> dict:
        '''
            Retorna el diccionario de la competencia dimensiones del joven actual

            Parámetros:
                self: El objeto Joven que sera consultado.

            Retorno:
                dict: diccionario de las dimensiones
        '''

        respuestas_joven = self.respuestas_joven.all()
        dimensiones = {}
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(promediosLiderazgo__isnull=True).values(
                "promediosLiderazgo")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    dimensiones = calificacion["promediosLiderazgo"]["dimensiones"]
        return dimensiones

    def obtener_promedio_liderazgo_carisma(self):
        '''
            Retorna el promedio de la competencia carisma del joven actual

            Parametros:
                self: El objeto Joven que sera consultado.

            Retorno:
                promedio: float
        '''

        respuestas_joven = self.respuestas_joven.all()
        promedio = 0.0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(promediosLiderazgo__isnull=True).values(
                "promediosLiderazgo")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    promedio = calificacion["promediosLiderazgo"]["competencias"]["Carisma"]["promedio"]
        return promedio

    def obtener_promedio_liderazgo_inspiracion(self) -> float:
        '''
            Retorna el promedio de la competencia inspiración del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                float: promedio
        '''

        respuestas_joven = self.respuestas_joven.all()
        promedio = 0.0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(promediosLiderazgo__isnull=True).values(
                "promediosLiderazgo")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    promedio = calificacion["promediosLiderazgo"]["competencias"]["Inspiración"]["promedio"]
        return promedio

    def obtener_promedio_liderazgo_direccion_rutinaria(self) -> float:
        '''
            Retorna el promedio de la competencia direccion del joven actual

            Parámetros:
                self: El objeto Joven que sera consultado.

            Retorno:
                float: promedio
        '''

        respuestas_joven = self.respuestas_joven.all()
        promedio = 0.0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(promediosLiderazgo__isnull=True).values(
                "promediosLiderazgo")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    promedio = calificacion["promediosLiderazgo"]["competencias"]["Dirección"]["promedio"]
        return promedio

    def obtener_promedio_liderazgo_deseo_que_otros_crezcan(self) -> float:
        '''
            Retorna el promedio de la competencia desarrollo del joven actual

            Parámetros:
                self: El objeto Joven que sera consultado.

            Retorno:
                float: promedio
        '''

        respuestas_joven = self.respuestas_joven.all()
        promedio = 0.0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(promediosLiderazgo__isnull=True).values(
                "promediosLiderazgo")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    promedio = calificacion["promediosLiderazgo"]["competencias"]["Desarrollo"]["promedio"]
        return promedio

    def obtener_promedio_liderazgo_innovacion(self) -> float:
        '''
            Retorna el promedio de la competencia innovación del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                float: promedio
        '''

        respuestas_joven = self.respuestas_joven.all()
        promedio = 0.0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(promediosLiderazgo__isnull=True).values(
                "promediosLiderazgo")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    promedio = calificacion["promediosLiderazgo"]["competencias"]["Innovación"]["promedio"]
        return promedio

    def obtener_promedio_liderazgo_claridad_en_negociar(self) -> float:
        '''
            Retorna el promedio de la competencia negociación del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                float: promedio
        '''

        respuestas_joven = self.respuestas_joven.all()
        promedio = 0.0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(promediosLiderazgo__isnull=True).values(
                "promediosLiderazgo")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    promedio = calificacion["promediosLiderazgo"]["competencias"]["Negociación"]["promedio"]

        return promedio

    def obtener_puntaje_habilidad_para_relacionarte_con_otros_y_contigo(self) -> str:
        '''
            Retorna la calificación de puntajeSterret del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                str: habilidad_para_relacionarte
        '''

        respuestas_joven = self.respuestas_joven.all()
        puntaje_global = 0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(puntajesSterret__isnull=True).values(
                "puntajesSterret")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    puntaje_global = calificacion["puntajesSterret"]["global"]

        if 1 <= puntaje_global < 90:
            habilidad_para_relacionarte = "BAJA"
        elif 90 <= puntaje_global <= 120:
            habilidad_para_relacionarte = "MEDIA"
        elif puntaje_global > 120:
            habilidad_para_relacionarte = "ALTA"
        else:
            habilidad_para_relacionarte = ""

        return habilidad_para_relacionarte

    def obtener_tu_estilo_de_liderazgo(self) -> str:
        '''
            Retorna el estilo de liderazgo del joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                str: estilo_de_liderazgo
        '''

        respuestas_joven = self.respuestas_joven.all()
        valor_mas_alto = ''
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(promediosLiderazgo__isnull=True).values(
                "promediosLiderazgo")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    dimensiones = calificacion["promediosLiderazgo"]["dimensiones"]
                    valor_mas_alto = max(dimensiones, key=lambda k: dimensiones[k])

        if valor_mas_alto == "laizzes_faire":
            estilo_de_liderazgo = "Todavía no tienes un estilo de liderazgo definido"
        elif valor_mas_alto == "transaccional":
            estilo_de_liderazgo = "Transaccional "
        elif valor_mas_alto == "transformacional":
            estilo_de_liderazgo = "Transformacional"
        else:
            estilo_de_liderazgo = ""
        return estilo_de_liderazgo

    def obtener_puntaje_de_auto_conciencia(self) -> int:
        '''
            Retorna el puntaje de la competencia autoconciencia del cuestionario Sterret joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                int: puntaje_autoconciencia
        '''

        respuestas_joven = self.respuestas_joven.all()
        puntaje_autoconciencia = 0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(puntajesSterret__isnull=True).values(
                "puntajesSterret")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    puntaje_autoconciencia = calificacion["puntajesSterret"]["competencias"]["Autoconciencia"][
                        "puntaje"]
        return puntaje_autoconciencia

    def obtener_puntaje_de_auto_confianza(self) -> int:
        '''
            Retorna el puntaje de la competencia autoconfianza del cuestionario Sterret joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                int: puntaje_autoconfianza
        '''

        respuestas_joven = self.respuestas_joven.all()
        puntaje_autoconfianza = 0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(puntajesSterret__isnull=True).values(
                "puntajesSterret")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    puntaje_autoconfianza = calificacion["puntajesSterret"]["competencias"]["Autoconfianza"]["puntaje"]
        return puntaje_autoconfianza

    def obtener_puntaje_de_auto_control(self) -> int:
        '''
            Retorna el puntaje de la competencia autocontrol del cuestionario Sterret joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                int: puntaje_autocontrol
        '''

        respuestas_joven = self.respuestas_joven.all()
        puntaje_autocontrol = 0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(puntajesSterret__isnull=True).values(
                "puntajesSterret")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    puntaje_autocontrol = calificacion["puntajesSterret"]["competencias"]["Autocontrol"]["puntaje"]
        return puntaje_autocontrol

    def obtener_puntaje_de_empatia(self) -> int:
        '''
            Retorna el puntaje de la competencia autocontrol del empatía Sterret joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                int: puntaje_empatia
        '''

        respuestas_joven = self.respuestas_joven.all()
        puntaje_empatia = 0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(puntajesSterret__isnull=True).values(
                "puntajesSterret")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    puntaje_empatia = calificacion["puntajesSterret"]["competencias"]["Empatía"]["puntaje"]
        return puntaje_empatia

    def obtener_puntaje_de_motivacion(self) -> int:
        '''
            Retorna el puntaje de la competencia autocontrol del motivación Sterret joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                int: puntaje_motivacion
        '''

        respuestas_joven = self.respuestas_joven.all()
        puntaje_motivacion = 0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(puntajesSterret__isnull=True).values(
                "puntajesSterret")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    puntaje_motivacion = calificacion["puntajesSterret"]["competencias"]["Motivación"]["puntaje"]
        return puntaje_motivacion

    def obtener_puntaje_de_capacidad_para_socializar(self) -> int:
        '''
            Retorna el puntaje de la competencia autocontrol del competencias Sterret joven actual

            Parámetros:
                self: El objeto Joven que será consultado.

            Retorno:
                int: puntaje_capacidad_para_socializar
        '''

        respuestas_joven = self.respuestas_joven.all()
        puntaje_capacidad_para_socializar = 0
        for respuesta in respuestas_joven:
            calificaciones_respuesta = respuesta.calificacion_respuesta.exclude(puntajesSterret__isnull=True).values(
                "puntajesSterret")
            if calificaciones_respuesta.exists():
                for calificacion in calificaciones_respuesta:
                    puntaje_capacidad_para_socializar = calificacion["puntajesSterret"]["competencias"]["Socializar"][
                        "puntaje"]
        return puntaje_capacidad_para_socializar

    @staticmethod
    def areas_interes_jovenes() -> 'list<dict>':
        """
            Retorna la cantidad de áreas de interés de los jóvenes

            list<dict>: lista de diccionarios
                        {'Administración':<int>, 'Ingeniería':<int>, Salud:<int>, 'Humanidades':<int>, 'Artes y Deportes':<int>, 'Ciencia y Medio Ambiente':<int>, 'Seguridad y Defensa':<int>}
        """
        diccionario_interes_jovenes = {'Administración': 0,
                                       'Ingeniería': 0,
                                       'Salud': 0,
                                       'Humanidades': 0,
                                       'Artes y Deportes': 0,
                                       'Ciencia y Medio Ambiente': 0,
                                       'Seguridad y Defensa': 0}

        diccionario_administracion = {'category': 'Administración', 'BAJO': 0, 'MEDIO': 0, 'ALTO': 0}
        diccionario_ingenieria = {'category': 'Ingeniería', 'BAJO': 0, 'MEDIO': 0, 'ALTO': 0}
        diccionario_salud = {'category': 'Salud', 'BAJO': 0, 'MEDIO': 0, 'ALTO': 0}
        diccionario_humanidades = {'category': 'Humanidades', 'BAJO': 0, 'MEDIO': 0, 'ALTO': 0}
        diccionario_artes_deportes = {'category': 'Artes y Deportes', 'BAJO': 0, 'MEDIO': 0, 'ALTO': 0}
        diccionario_ciencia_medio_ambiente = {'category': 'Ciencia y Medio Ambiente', 'BAJO': 0, 'MEDIO': 0, 'ALTO': 0}
        diccionario_seguridad_defensar = {'category': 'Seguridad y Defensa', 'BAJO': 0, 'MEDIO': 0, 'ALTO': 0}

        relacion_tribu_interes = {"Gronor": "Administración",
                                  "Centeros": "Ingeniería",
                                  "Cuisana": "Salud",
                                  "Vincularem": "Humanidades",
                                  "Brianimus": "Artes y Deportes",
                                  "Moterra": "Ciencia y Medio Ambiente",
                                  "Ponor": "Seguridad y Defensa"}

        for area_interes in Joven.objects.all():
            for perfil in area_interes.joven_dueno_perfil.all():
                diccionario_interes_jovenes[relacion_tribu_interes[perfil.tribu_deseada]] += 1

        intereses_ordenados_mayor_menor = list(
            reversed(sorted(diccionario_interes_jovenes.items(), key=lambda x: x[1])))

    def obtener_edad_joven(self) -> int:
        '''
            Retorna la edad del joven

            Parámetros:
                self: El objeto Joven que sera consultado.

            Retorno:
                edad: int
        '''

        from datetime import date

        return ((date.today() - self.fecha_de_nacimiento) / 365).days

    def generar_estado_visto_resultados(self):
        ids_competencias = self.obtener_competencias_procesamiento()
        for id in ids_competencias:
            competencia = Competencias.buscar(id)
            competencia.crear_competencia_visitada_joven(self.id)
        habilidades = Habilidad.obtener_activos()
        for habilidad in habilidades:
            habilidad.crear_habilidad_visitada_joven(self.id)
            for competencia in habilidad.competencias_asociadas.all():
                competencia.crear_competencia_visitada_joven(self.id)
        competencias = self.obtener_competencias_aptitudes_e_intereses()
        for competencia in competencias:
            competencia['competencia'].crear_competencia_visitada_joven(self.id)

        competencias = self.obtener_listado_de_experiencias()
        for competencia in competencias:
            competencia.crear_competencia_visitada_joven(self.id)

    def obtener_pruebas_completadas(self) -> int:
        '''
            Función que retorna el número de pruebas completadas
        '''

        completados, no_completados = self.contar_formularios

        completos = RespuestasJoven.cantidad_respuestas_completas(joven=self)
        completos += completados

        return completos

    def obtener_porcentaje_pruebas_completadas(self) -> int:
        '''
            Función que retorna el porcentaje de pruebas completadas
        '''
        completados, no_completados = self.contar_formularios
        formularios = 0
        if self.tanda_formularios:
            self.verificar_formularios
            formularios_joven = self.formularios_activos()
            formularios = Formularios.cantidad_formularios_activos(formularios=formularios_joven, joven=self)

        completos = self.respuestas_joven.filter(completas=True).distinct('formulario').count()
        completos += completados
        formularios += no_completados
        totales = formularios + completos
        porcentaje_formularios_completados = round((completos / totales) * 100)

        return porcentaje_formularios_completados

    def obtener_promedio_porcentaje_nivles(self) -> int:
        '''
            Función que retorna el porcentaje de nivel de la misión
        '''
        realidades = {
            '1': "Realidad Verde",
            '2': "Realidad Amarilla",
            '3': "Realidad Azul"
        }
        total = Recurso.obtener_total_recursos_realidad(realidades.get(self.nivel_realidad))
        vistos = self.usuario_que_ve_el_video.filter(
            observado=1,
            recurso__tipo__nombre=realidades.get(self.nivel_realidad)
        ).count()
        try:
            porcentaje = round((vistos / total) * 100)
        except ZeroDivisionError:
            return 0

        return porcentaje

    def obtener_porcentaje_resultados_leidos(self) -> dict:
        '''
            Función que retorna el porcentaje de resultados leidos

            Retorno:
                int: porcentaje de resultados leídos
        '''
        total_secciones = SeccionVisitada.obtener_todos(self.pk).count()
        total_competencias = CompetenciaVisitada.obtener_todos(self.pk).count()
        total_habilidades = HabilidadVisitada.obtener_todos(self.pk).count()
        total_secciones_visitadas = SeccionVisitada.obtener_visitados(self.pk).count()
        total_competencias_visitadas = CompetenciaVisitada.obtener_visitados(self.pk).count()
        total_habilidades_visitadas = HabilidadVisitada.obtener_visitados(self.pk).count()
        total_resultados_leidos = total_secciones_visitadas + total_habilidades_visitadas + total_competencias_visitadas
        total_resultados = total_secciones + total_habilidades + total_competencias
        porcentaje_resultados_leidos = round(
            (total_resultados_leidos / total_resultados) * 100) if total_resultados != 0 else 0
        return total_resultados_leidos, porcentaje_resultados_leidos


class Padre(Usuario):
    hijos = models.ManyToManyField(Joven, verbose_name="hijos")
    history = HistoricalRecords()

    @staticmethod
    def buscar(pk) -> 'Padre':
        """
            Devuelve el objeto padre por medio de la pk(llave primaria)

            Parámetros:
                pk (int) -> pk que se usara para buscar al usuario padre

            Retorna:
                padre (Padre): usuario padre encontrado con la pk.
        """
        padre = Padre.objects.get(usuario_ptr_id=pk)
        return padre


class SolicitudAmistad(models.Model):
    ESTADOS_SOLICITUD = (
        ('Enviado', 'Enviado'),
        ('Aceptada', 'Aceptada')
    )
    emisor = models.ForeignKey(Joven, related_name='solicitudes_enviadas', on_delete=models.CASCADE)
    receptor = models.ForeignKey(Joven, related_name='solicitudes_recibidas', on_delete=models.CASCADE)
    estado = models.CharField(max_length=10, choices=ESTADOS_SOLICITUD)
    history = HistoricalRecords()

    @staticmethod
    def existe_solitud(emisor: 'Joven', receptor: 'Joven') -> bool:
        '''
            Confirma si existe una solicitud de amistad

            Parámetros:
                emisor: Objeto del modelo Joven que envio la solicitud
                receptor: Objeto del modelo Joven que recibio la solicitud

            Retorno:
                Si existe: True
                No existe: False
        '''
        try:
            solicitud = SolicitudAmistad.objects.get(emisor=emisor, receptor=receptor)
        except SolicitudAmistad.DoesNotExist:
            return False
        return True

    @staticmethod
    def buscar_solicitud(emisor: 'Joven', receptor: 'Joven') -> 'SolicitudAmistad':
        '''
            retorna Solicitud de Amistad

            Parámetros:
                emisor: Objeto del modelo Joven que envio la solicitud
                receptor: Objeto del modelo Joven que recibio la solicitud

            Retorno:
                Si existe: SolicitudAmistad
                No existe: none
        '''
        try:
            solicitud = SolicitudAmistad.objects.get(emisor=emisor, receptor=receptor)
        except SolicitudAmistad.DoesNotExist:
            return None
        return solicitud

    @staticmethod
    def crear_solicitud(emisor: 'Joven', receptor: 'Joven') -> 'SolicitudAmistad':
        '''
            Crea y retorna Solicitud de Amistad

            Parámetros:
                emisor: Objeto del modelo Joven que envio la solicitud
                receptor: Objeto del modelo Joven que recibio la solicitud

            Retorno:
                SolicitudAmistad
        '''
        solicitud = SolicitudAmistad.objects.create(emisor=emisor, receptor=receptor, estado="Enviado")
        return solicitud


class SalarioPais(models.Model):
    TIPOS_MONEDA = (
        ('Pesos', 'Pesos'),
        ('Dólares', 'Dólares'),
        ('Euros', 'Euros'),
        ('Libras', 'Libras'),
        ('Yenes', 'Yenes'),
        ('Wones', 'Wones'),
        ('Guaraníes', 'Guaraníes'),
        ('Bolivianos', 'Bolivianos'),
        ('Soles', 'Soles'),
        ('Reales', 'Reales'),
        ('Bolívares', 'Bolívares')
    )
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)
    pais = models.ForeignKey(Country, on_delete=models.PROTECT, related_name='pais_del_salario')
    pais_nombre = models.CharField(max_length=50)
    tipo = models.CharField(max_length=50, choices=TIPOS_MONEDA)
    cantidad = models.DecimalField(max_digits=20, decimal_places=2)
    activo = models.BooleanField(verbose_name="¿El salario del pais se encuentra activo?", default=True)
    anio = models.PositiveIntegerField(verbose_name="Año de vigencia*",
                                       validators=[MinValueValidator(2020), MaxValueValidator(2050)])

    class Meta:
        ordering = ['pais_id']

    @staticmethod
    def obtener_salarios() -> 'Queryset<salario_pais>':
        '''
            Retorna todos los salarios de los paises registrados.

            Retorno:
                Queryset<salario_pais> : Queryset de salario_pais
        '''
        return SalarioPais.objects.all()

    def buscar(id: int) -> 'SalarioPais':
        '''
            Retorna un Salario del pais perteneciente al id

            Parámetros:
                id: id del salario del pais que se buscara

            Retorno:
                Si existe: SalarioPais
                Si no existe: None
        '''
        try:
            salario = SalarioPais.objects.get(id=id)
            return salario
        except SalarioPais.DoesNotExist:
            return None

    def __str__(self):



        simbolo = ""
        if self.tipo == "Libras":
            simbolo = "£"
        elif self.tipo == "Euros":
            simbolo = "€"
        elif self.tipo == "Yenes":
            simbolo = "¥"
        elif self.tipo == "Wones":
            simbolo = "₩"
        elif self.tipo == "Guaraníes":
            simbolo = "₲"
        elif self.tipo == "Bolivianos":
            simbolo = "Bs."
        elif self.tipo == "Soles":
            simbolo = "S/"
        elif self.tipo == "Reales":
            simbolo = "R$"
        elif self.tipo == "Bolívares":
            simbolo = "Bs"
        else:
            simbolo = "$"

        cadena = simbolo + " " + str(self.cantidad) + " " + str(self.tipo) + " de " + str(
            self.pais_nombre) + " - " + str(self.anio)

        return cadena
