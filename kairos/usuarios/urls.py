
# Modulos Django
from django.urls import path
# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "usuarios"

urlpatterns = [
    path('ingresar/', view=Login.as_view(), name='iniciar_sesion'),

    path('gestionar/', view=UsuariosListado.as_view(), name='listado_usuarios'),
    path('salarios/gestionar', view=SalariosListado.as_view(), name='listado_salarios'),

    path('consultar/', view=UsuariosConsultar.as_view(), name='consultar_usuarios'),
    path('jovenes/experiencia-actual/consultar/', ExperienciasActualesConsultar.as_view(), name='consultar_experiencia_jovenes'),
    path('perfil-actual', view=mi_perfil_actual_detalle, name='mi_perfil_actual'),
    path('perfil-actual/modificar/<int:pk>', view=ModificarPerfilActual.as_view(), name='modificar_mi_perfil_actual'),
    path('verificar-correo/<int:id_joven>', view=verificar_correo, name='verificar_correo'),
    path('reenviar-correo/<int:id_joven>', view=reenviar_correo, name='reenviar_correo'),

    path('registrar-superusuario/', view=RegistrarSuperUsuario.as_view(), name='registrar_superusuario'),
    path('registrar-joven/', view=RegistrarJoven.as_view(), name='registrar_joven'),
    path('registrar-padre/', view=RegistrarPadre.as_view(), name='registrar_padre'),
    path('registrar-jurado/', view=RegistrarJurado.as_view(), name='registrar_jurado'),
    path('registrar-salario/', view=RegistrarSalario.as_view(), name='registrar_salario'),
    path('registro-masivo/', view=RegistroMasivo.as_view(), name='registro_masivo'),


    path('cargar-excel/', view=cargar_excel, name='cargar_excel'),

    path('modificar/<int:id_usuario>', view=usuario_modificar, name='modificar'),
    path('modificar-salario/<int:id_salario>', view=salario_modificar, name='modificar_salario'),

    path('registrarse/<int:tipo>/<int:organizacion>', view=UsuariosRegistrar.as_view(), name='registrarse_con_organizacion'),
    path('registrarse/<int:tipo>', view=usuario_registro, name='registrarse'),
    path('ver-perfil/', view=usuario_perfil, name='perfil'),

    path('restablecer-password/<int:id_usuario>', view=restablecer_contrasena, name='restablecer_password'),
    path("salir", view=cerrar_sesion, name='cerrar_sesion'),

    path("api/maps/guardar-direccion", view=ApiGuardarDireccion.as_view(), name="api_guardar_direccion"),
    path("api/maps/cargar-direccion", view=ApiCargarDireccion.as_view(), name="api_cargar_direccion"),

    path("amigos/consultar-jovenes", view=JovenesConsultar.as_view(), name="consultar_jovenes"),
    path("amigos/enviar-solicitud/<int:id_receptor>/<int:usuario_tarjeta>", view=enviar_solicitud, name="enviar_solicitud"),
    path("amigos/aceptar-solicitud/<int:id_emisor>/<int:usuario_tarjeta>", view=aceptar_solicitud, name="aceptar_solicitud"),
    path("amigos/rechazar-solicitud/<int:id_emisor>/<int:usuario_tarjeta>", view=rechazar_solicitud, name="rechazar_solicitud"),
    path("amigos/finalizar-amistad/<int:id_emisor>/<int:usuario_tarjeta>", view=eliminar_amigo, name="finalizar_amistad"),

    path("tarjeta-presentacion/<int:id_usuario>", view=TarjetaPresentacionUsuario.as_view(), name="tarjeta_presentacion"),

    path('contrasena/solicitar/', view=solicitar_contrasena, name='solicitar_contrasena'),
    path('contrasena/crear/(<uidb64>)-(<token>)/', view=CambioContrasenia.as_view(), name='cambiar_contrasena'),
    path('verificar-correo/', view=VerificarCorreoAPIView.as_view(), name='verificar_correo'),
    path('reenviar-verificacion/', view=solicitar_correo_verificacion, name='reenviar_correo'),
    path('correo-verificado/', view=contrasena_cambiada, name='contrasena_cambiada'),


    path('invitar-joven/', view=InvitarJovenesPlataforma.as_view(), name='invitar_joven'),
    path('cambiar-estado-video/', view=verificacion_estado_videos_guia, name='cambiar_estado_video'),
    path('postular-equipo-organizacion/<int:id_organizacion>/<int:id_joven>', view=postulacion_lider_organizacion, name='enviar_postulacion'),
    path('cancelar-postulacion/<int:id_organizacion>/<int:id_equipo>', view=retirar_postulacion_lider_organizacion, name='cancelar_postulacion'),

    path('reporte_personas_excel/', MagicLink.as_view(), name="reporte_personas_excel"),

    path('api/obtener-usuarios', APIUsuariosListado.as_view(), name="api_usuarios_listado"),
    path('api/guardar-realidad', ApiGuardarRealidad.as_view(), name="api_guardar_realidad"),
    path('exportar-jovenes', view=ExportarJovenes.as_view(), name="exportar_jovenes")

]

