# Modulos Django
from django.db.models import Q
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import PasswordResetConfirmView
from django.http import JsonResponse
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.http import urlsafe_base64_decode
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView, CreateView, UpdateView, ListView
from django.views.generic import TemplateView
from django.http.response import HttpResponse

# Modulos de plugin externos
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
import json
from openpyxl import Workbook
import pandas as pd

# Modulos de otras apps
from kairos.contenido.models import CalificacionContenido, Contenido
from kairos.core.mixins import MensajeMixin
from kairos.equipos.models import Equipo
from kairos.guia.models import VerRecurso
from kairos.historiales.models import RegistroTandasJoven
from kairos.organizaciones.forms import OrganizacionPerfilForm, PostulacionLiderOrganizacionForm
from kairos.tandas_formularios.models import TandasFormularios
from kairos.insignias.models import Tribus

# Modulos internos de la app
from .forms import *
from .models import Joven, Padre, Usuario, SolicitudAmistad, SalarioPais
from .serializers import JovenSerializer


def inicio(request: 'request') -> 'redirect':
    '''
        Redirecciona al template del tablero principal con toda la información relacionada con el usuario(formularios que ha resuelto, 
        amigos, organizaciones, puntajes de los cuestionarios, insignias, cantidad de rutas y oportunidades )

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    '''

    Usuario.usuario_inicial()
    usuario = request.user

    if not request.user.is_authenticated:
        return redirect("usuarios:iniciar_sesion")

    if usuario.tipo == 'Joven':
        joven = Joven.buscar(id_joven=usuario.pk)

        porcentajes = joven.obtener_porcentajes()

        # tribus
        tribus = Tribus.obtener_nombres_imagen_lista()
        totales = []
        for tribu in tribus:
            rutas_favoritas = Contenido.favoritas_tribu(usuario, 'ruta', tribu["nombre"]).count()
            oportunidades_favoritas = Contenido.favoritas_tribu(usuario, 'oportunidad', tribu["nombre"]).count()
            total = rutas_favoritas + oportunidades_favoritas
            totales.append({
                "total": str(total).zfill(2),
                "imagen": tribu["imagen"]
            })

        return render(request, 'usuarios/jovenes/inicio.html',
                      {'joven': joven,
                       'completados': porcentajes["completos"],
                       'resultados_leidos': porcentajes["total_resultados_leidos"],
                       'porcentaje_resultados_leidos': porcentajes["porcentaje_resultados_leidos"],
                       'porcentaje_completos': porcentajes["porcentaje_formularios_completados"],
                       'totales_tribus': totales,
                       'rutas_oportunidades_favoritos': porcentajes["rutas_oportunidades_favoritos"],
                       'rutas_oportunidades_escogidos': porcentajes["contenidos_escogidos"],
                       'perfiles_creados': porcentajes["mis_perfiles"],
                       'perfiles_escogidos': porcentajes["mis_perfiles_escogidos"]
                       })

    if usuario.tipo == 'Organización':
        q = int(request.GET.get("q", 0))
        if q == -1:
            return redirect('/')

        organizacion = Organizacion.buscar(id_organizacion=usuario.pk)
        tablero = organizacion.obtener_información_tablero(q)
        cohortes = organizacion.obtener_cohortes_todas()
        return render(request, 'usuarios/jovenes/inicio.html',
                      {'organizacion': organizacion, 'cantidad_jovenes': tablero["cantidad_jovenes"],
                       'cohortes': cohortes,
                       'pruebas_completadas': tablero["pruebas_completadas"],
                       'resultados_leidos': tablero["resultados_leidos"],
                       'contenido_favorito': tablero["contenido_favorito"],
                       'contenido_escogido': tablero["contenido_escogido"],
                       'porcentaje_escogidos': tablero["porcentaje_escogidos"],
                       'perfiles_creados': tablero["perfiles_creados"],
                       'total_creados': tablero["total_creados"],
                       'avance_niveles': tablero["avance_niveles"],
                       'cantidad_activos': tablero["cantidad_jovenes_activos_mes"],
                       'escogidos': tablero["contenido_escogidos"],
                       'favoritas': tablero["favoritas"], 'totales_tribus': tablero["totales"],
                       'oportunidades_me_gusta': tablero["oportunidades_me_gusta"],
                       'rutas_me_gusta': tablero["rutas_me_gusta"],
                       'q': q
                       })

    else:
        return redirect("usuarios:perfil")


@login_required
def verificar_correo(request, id_joven):
    if request.user.is_authenticated == False and request.user.is_superuser == False:
        return redirect("usuarios:inicio")

    if Joven.existe_usuario(id_joven) == True:
        joven = Joven.buscar(id_joven=id_joven)
        joven.correo_verificado = True
        joven.save()
        messages.success(request, 'El correo del usuario ha sido verificado')
    else:
        messages.error(request, 'El usuario no existe')

    return redirect("usuarios:listado_usuarios")


class Login(FormView):
    mensaje = ""
    form_class = LoginForm
    template_name = 'usuarios/iniciar_sesion.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect("usuarios:perfil")
        return super(Login, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        usuario = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        remember_me = form.cleaned_data['remember_me']
        if usuario is not None:
            if usuario.correo_verificado == False:
                messages.error(self.request, 'Verifique su correo para poder iniciar sesión')
                return redirect("usuarios:iniciar_sesion")
            if usuario.is_active:
                form.clean_username()
                login(self.request, usuario)
                siguiente = self.request.GET.get("next", None)
                self.success_url = self.success_url if siguiente == None else siguiente
                messages.success(self.request, "Bienvenido!")
                return redirect('inicio')
            else:
                mensaje = "El usuario %s no se encuentra activo" % (usuario.nombre)
                messages.error(self.request, mensaje)

            if remember_me:
                self.request.session.set_expiry(1209600)
                self.request.session.modified = True
            else:
                self.request.session.set_expiry(0)
                self.request.session.modified = True
            return super(Login, self).form_valid(form)

        else:
            mensaje = "El usuario no existe o la contraseña es incorrecta"
            form.add_error('username', mensaje)
            form.add_error('password', mensaje)
            return super(Login, self).form_invalid(form)


@login_required
def registrar_usuario(request: 'request') -> 'redirect':
    '''
        Redirecciona a las funciones que registran al usuario según su tipo

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    '''
    if request.is_ajax():
        tipo = request.GET.get('tipo', None)
        if tipo == 'Joven':
            return redirect('usuarios:registrar_joven')
        if tipo == 'Padre':
            return redirect('usuarios:registrar_padre')
        if tipo == 'Jurado':
            return redirect('usuarios:registrar_jurado')
        else:
            messages.error(request, "Tipo de usuario inválido. Por favor intente de nuevo.")
            return JsonResponse({})


class RegistrarSuperUsuario(LoginRequiredMixin, MensajeMixin, CreateView):
    model = Usuario
    form_class = CrearUsuarioForm
    template_name = "usuarios/registrar_superusuario.html"
    mensaje_exito = "Superusuario registrado satisfactoriamente"
    mensaje_error = "Ha ocurrido un error. Por favor verifique los datos ingresados."

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.es_superusuario == False:
            messages.error(request, "Usted no está autorizado para gestionar a los usuarios")
            return redirect("inicio")
        return super(RegistrarSuperUsuario, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        usuario = form.save(commit=False)
        usuario.es_superusuario = True
        usuario.tipo = "Superusuario"
        usuario.save()
        return redirect('usuarios:listado_usuarios')


class RegistrarJoven(LoginRequiredMixin, MensajeMixin, CreateView):
    model = Joven
    form_class = JovenModelForm
    template_name = "usuarios/registro_joven.html"
    success_url = reverse_lazy('usuarios:listado_usuarios')
    mensaje_error = "Ha ocurrido un error. Por favor verifique los datos ingresados."
    mensaje_exito = "Joven registrado satisfactoriamente"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.es_superusuario == False:
            messages.error(request, "Usted no está autorizado para gestionar a los usuarios")
            return redirect("inicio")
        return super(RegistrarJoven, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        joven = form.save(commit=False)
        joven.tipo = "Joven"
        joven.save()
        try:
            organizacion = Organizacion.obtener_principal()
            joven.agregar_organizacion(organizacion)
            organizacion.agregar_joven(joven)

        except Organizacion.DoesNotExist as e:
            print(e)
        try:
            tanda_inicial = TandasFormularios.buscar(identificador='00')
            joven.tanda_formularios = tanda_inicial
            joven.save()
        except TandasFormularios.DoesNotExist as e:
            print(e)
        return super(RegistrarJoven, self).form_valid(form)


class RegistrarPadre(LoginRequiredMixin, MensajeMixin, CreateView):
    model = Padre
    form_class = PadreModelForm
    template_name = "usuarios/registro_padre.html"
    success_url = reverse_lazy('usuarios:listado_usuarios')
    mensaje_error = "Ha ocurrido un error. Por favor verifique los datos ingresados."
    mensaje_exito = "Padre registrado satisfactoriamente"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.es_superusuario == False:
            messages.error(request, "Usted no está autorizado para gestionar a los usuarios")
            return redirect("inicio")
        return super(RegistrarPadre, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        padre = form.save(commit=False)
        padre.tipo = "Padre"
        padre.save()
        return super(RegistrarPadre, self).form_valid(form)


class RegistrarJurado(LoginRequiredMixin, MensajeMixin, CreateView):
    model = Jurado
    form_class = JuradoModelForm
    template_name = "usuarios/registro_jurado.html"
    success_url = reverse_lazy('usuarios:listado_usuarios')
    mensaje_error = "Ha ocurrido un error. Por favor verifique los datos ingresados."
    mensaje_exito = "Jurado registrado satisfactoriamente"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.es_superusuario == False:
            messages.error(request, "Usted no está autorizado para gestionar a los usuarios")
            return redirect("inicio")
        return super(RegistrarJurado, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        jurado = form.save(commit=False)
        jurado.tipo = "Jurado"
        jurado.correo_verificado = True
        jurado.save()
        return super(RegistrarJurado, self).form_valid(form)


class UsuariosListado(LoginRequiredMixin, ListView):
    model = Joven
    template_name = "usuarios/listado.html"
    context_object_name = "usuarios"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.es_superusuario == False:
            messages.error(request, "Usted no está autorizado para gestionar a los usuarios")
            return redirect("inicio")
        return super(UsuariosListado, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UsuariosListado, self).get_context_data(**kwargs)

        if 'first_name' in self.request.GET:
            context['usuarios'] = Joven.buscador_organizaciones_joven(
                ids_organizaciones=self.request.GET.getlist('first_name'))
            organizaciones = Organizacion.obtener_organizaciones_filtro(ids=self.request.GET.getlist('first_name'))
            context['form'] = FiltroOrganizacionForm(initial={'first_name': organizaciones})
            context['organizaciones'] = organizaciones
            context['filtro'] = True

        else:
            context['form'] = FiltroOrganizacionForm()
            context['usuarios'] = Joven.obtener_jovenes()
        return context


class APIUsuariosListado(LoginRequiredMixin, APIView):
    """
        API para consultar todos los usuarios de la pagina o los filtrados por organizacion
    """
    renderer_classes = [JSONRenderer]

    def get(self, request, format=None):
        inicio = int(request.GET.get("inicio"))
        final = int(request.GET.get("limite"))
        filtro = str(request.GET.get("filtro"))
        orden = str(request.GET.get("orden"))
        direccion = str(request.GET.get("direccion"))

        id_organizaciones = dict(request.query_params.lists())['id[]']
        if id_organizaciones[0] == '':
            jovenes = Joven.obtener_jovenes()
        else:
            id_organizaciones.pop(len(id_organizaciones) - 1)
            jovenes = Joven.buscador_organizaciones_joven(id_organizaciones)

        estado = None
        correo_verificado = None
        if filtro.upper() == 'SIN VERIFICAR':
            correo_verificado = False
        elif filtro.upper() == 'VERIFICADO':
            correo_verificado = True
        elif filtro.upper() == 'INACTIVO':
            estado = False
        elif filtro.upper() == 'ACTIVO':
            estado = True

        query = Joven.obtener_query(filtro=filtro, correo_verificado=correo_verificado, estado=estado)
        filtrado = jovenes.filter(query).order_by(direccion + orden)

        paginado = filtrado[inicio:inicio + final]
        serializer = JovenSerializer(paginado, many=True)
        return Response({
            "draw": request.GET.get("draw"),
            "total": filtrado.count(),
            "data": serializer.data
        })


class SalariosListado(LoginRequiredMixin, ListView):
    model = SalarioPais
    template_name = "usuarios/listado_salarios.html"
    context_object_name = "salarios"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.es_superusuario == False:
            messages.error(request, "Usted no está autorizado para gestionar a los usuarios")
            return redirect("inicio")
        return super(SalariosListado, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SalariosListado, self).get_context_data(**kwargs)
        context['form'] = FiltroOrganizacionForm()
        context['salarios'] = SalarioPais.obtener_salarios()
        return context


class RegistrarSalario(LoginRequiredMixin, MensajeMixin, CreateView):
    model = SalarioPais
    form_class = SalarioModelForm
    template_name = "usuarios/registro_salario.html"
    success_url = reverse_lazy('usuarios:listado_salarios')
    mensaje_error = "Ha ocurrido un error. Por favor verifique los datos ingresados."
    mensaje_exito = "Salario registrado satisfactoriamente"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.es_superusuario == False:
            messages.error(request, "Usted no está autorizado para gestionar a los usuarios")
            return redirect("inicio")
        return super(RegistrarSalario, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        salario = form.save(commit=False)
        salario.tipo = form.cleaned_data['tipo_moneda']
        salario.pais_nombre = form.cleaned_data['pais'].name_ascii
        salario.save()
        return super(RegistrarSalario, self).form_valid(form)


class UsuariosConsultar(LoginRequiredMixin, ListView):
    model = Usuario
    template_name = "usuarios/consultar.html"
    context_object_name = "usuarios"

    def get_queryset(self):
        usuarios = Usuario.objects.exclude(tipo='Organización')
        return usuarios


def usuario_modificar(request: 'request', id_usuario: int) -> 'render':
    '''
        Permite al administrador modificar la información de un usuario.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_usuario (int): Identificador del Usuario a modificar
    '''
    form = ''
    usuario = Usuario.obtener_usuario_pk(pk=id_usuario)

    if request.user.is_superuser == False:
        messages.error(request, "Usted no está autorizado para gestionar a los usuarios")
        return redirect('inicio')

    if usuario.tipo == 'Joven':
        joven = Joven.buscar(id_joven=id_usuario)
        form = JovenModificarForm(instance=joven)
    if usuario.tipo == 'Padre':
        padre = Padre.buscar(pk=id_usuario)
        form = PadreModificarForm(instance=padre)
    if usuario.tipo == 'Jurado':
        jurado = Jurado.buscar(id_jurado=id_usuario)
        form = JuradoModificarForm(instance=jurado)

    if request.method == 'POST':
        if usuario.tipo == 'Joven':
            joven = Joven.buscar(id_joven=id_usuario)
            form = JovenModificarForm(request.POST, instance=joven)
        if usuario.tipo == 'Padre':
            padre = Padre.buscar(pk=id_usuario)
            form = PadreModificarForm(request.POST, instance=padre)
        if usuario.tipo == 'Jurado':
            jurado = Jurado.buscar(id_jurado=id_usuario)
            form = JuradoModificarForm(request.POST, instance=jurado)

        if form.is_valid():
            form.save()
            messages.success(request, "Usuario modificado satisfactoriamente")
            return redirect('usuarios:listado_usuarios')

    return render(request, 'usuarios/modificar_usuario.html', {
        'form': form
    })


def salario_modificar(request: 'request', id_salario: int) -> 'render':
    '''
        Permite al administrador modificar la información de un salario.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_salario (int): Identificador del Salario a modificar
    '''

    salario = SalarioPais.buscar(id=id_salario)
    form = SalarioModificarForm(instance=salario)

    if request.user.is_superuser == False:
        messages.error(
            request, "Usted no está autorizado para gestionar a los usuarios")
        return redirect('inicio')

    if request.method == 'POST':
        salario = SalarioPais.buscar(id=id_salario)
        form = SalarioModificarForm(request.POST, instance=salario)

        if form.is_valid():
            form.save()
            messages.success(request, "Salario modificado satisfactoriamente")
            return redirect('usuarios:listado_salarios')

    return render(request, 'usuarios/modificar_salario.html', {
        'form': form
    })


# ############VIEWS USER##########################

def usuario_registro(request, tipo: int) -> 'render':
    '''
        Permite al registrar a un Joven o Padre a la aplicación.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            tipo (int): Determina el tipo de usuario que se registra (Padre, Joven)
    '''
    from kairos.gestor_correos.envio_correos import enviar_correo, VERIFICACION_USUARIO_CONTRASENIA

    form = ''
    nuevo_usuario = ''
    if request.user.is_authenticated:
        messages.error(request, "Usted no está autorizado para realizar esta acción")
        return redirect('inicio')

    if tipo == 2:
        form = PadreRegistroForm()
    else:
        form = JovenRegistroForm()

    if request.method == 'POST':
        if tipo == 2:
            form = PadreRegistroForm(request.POST)
        else:
            form = JovenRegistroForm(request.POST)

        if form.is_valid():
            nuevo_usuario = form.save(commit=False)
            if tipo == 2:
                nuevo_usuario.tipo = "Padre"
            else:
                nuevo_usuario.tipo = "Joven"

                try:
                    tanda_inicial = TandasFormularios.buscar(identificador='00')
                    nuevo_usuario.tanda_formularios = tanda_inicial
                    nuevo_usuario.save()
                except TandasFormularios.DoesNotExist as e:
                    print(e)
                try:
                    organizacion = Organizacion.obtener_principal()
                    nuevo_usuario.agregar_organizacion(organizacion)
                    organizacion.agregar_joven(nuevo_usuario)
                except Organizacion.DoesNotExist as e:
                    print(e)

                registros_anteriores = RegistroTandasJoven.registro_tandas_filtro_joven(joven=nuevo_usuario)
                for registro in registros_anteriores:  # Deshabilitar los registros anteriores
                    registro.disponible = False
                    registro.save()
                try:
                    RegistroTandasJoven.crear_registro_tandas_joven(tanda=tanda_inicial, joven=nuevo_usuario)
                except (TypeError, ValueError) as e:
                    print(e)

            nuevo_usuario.save()
            enviar_correo(nuevo_usuario.email, None, VERIFICACION_USUARIO_CONTRASENIA, nuevo_usuario)

            messages.info(request, "Busca el correo de contame@mieslabon.com en la bandeja de entrada y en spam.")
            messages.success(request, "Se ha enviado un correo de validación.")
            return render(request, 'usuarios/correo_registro.html', {
                'usuario': nuevo_usuario
            })
        else:
            messages.error(request, "Datos inválidos. Por favor intente de nuevo.")

    return render(request, 'usuarios/registrarse.html', {
        'form': form
    })


class UsuariosRegistrar(MensajeMixin, CreateView):
    model = Joven
    template_name = "usuarios/registrarse.html"
    form_class = JovenRegistroForm
    mensaje_error = "Ha ocurrido un error. Por favor verifique los datos ingresados."
    mensaje_exito = "Joven registrado satisfactoriamente"
    success_url = reverse_lazy('inicio')

    def dispatch(self, request, *args, **kwargs):
        if not Organizacion.existe_organizacion(self.kwargs['organizacion']):
            messages.error(request, "La organización que te invito no existe")
            return redirect("inicio")

        if self.kwargs['tipo'] != 1:
            messages.error(request, "El formulario no es para registrar jóvenes")
            return redirect("inicio")
        return super(UsuariosRegistrar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        from kairos.gestor_correos.envio_correos import enviar_correo, VERIFICACION_USUARIO_CONTRASENIA
        from kairos.organizaciones.models import InformacionJovenInvitado
        usuario = form.save(commit=False)
        usuario.tipo = "Joven"
        usuario.correo_verificado = False
        usuario.save()
        form.save()

        try:
            organizacion_mi_eslabon = Organizacion.obtener_principal()
            usuario.agregar_organizacion(organizacion_mi_eslabon)
            organizacion_mi_eslabon.agregar_joven(usuario)
        except Organizacion.DoesNotExist as e:
            print(e)

        try:
            organizacion = Organizacion.buscar(id_organizacion=self.kwargs['organizacion'])
            if InformacionJovenInvitado.existe_registro(organizacion, usuario.email):
                registro_joven_invitado = InformacionJovenInvitado.obtener_registro(organizacion, usuario.email)
                usuario.fecha_induccion = registro_joven_invitado.fecha_induccion
                usuario.ultimo_grado = registro_joven_invitado.grado
                usuario.save()
            usuario.agregar_organizacion(organizacion)
            organizacion.agregar_joven(usuario)
        except Organizacion.DoesNotExist:
            messages.error(self.request, "La organización no existe")

        try:
            tanda_inicial = TandasFormularios.buscar(identificador='00')
            usuario.tanda_formularios = tanda_inicial
            usuario.save()
        except TandasFormularios.DoesNotExist as e:
            print(e)

        registros_anteriores = RegistroTandasJoven.registro_tandas_filtro_joven(joven=usuario)
        for registro in registros_anteriores:  # Deshabilitar los registros anteriores
            registro.disponible = False
            registro.save()
        try:
            RegistroTandasJoven.crear_registro_tandas_joven(tanda=tanda_inicial, joven=usuario)
        except (TypeError, ValueError) as e:
            print(e)

        super(UsuariosRegistrar, self).form_valid(form)

        enviar_correo(usuario.email, None, VERIFICACION_USUARIO_CONTRASENIA, usuario)

        messages.success(self.request, "Se ha enviado un correo de validación.")
        return redirect('inicio')


def usuario_perfil(request) -> 'render':
    '''
        Permite al consultar y modificar los datos del perfil de usuario.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    '''
    from kairos.gestor_correos.envio_correos import enviar_correo, VERIFICACION_USUARIO_CONTRASENIA

    form = ''
    usuario = request.user

    if request.user.is_authenticated == False:
        messages.error(request, "Usted no está autorizado para realizar esta acción")
        return redirect('inicio')

    if request.method == 'GET':
        if usuario.es_superusuario:
            form = SuperusuarioPerfilForm(instance=usuario)
        if usuario.tipo == 'Joven':
            joven = Joven.buscar(id_joven=usuario.id)
            form = JovenPerfilForm(instance=joven)
        if usuario.tipo == 'Padre':
            padre = Padre.buscar(pk=usuario.id)
            form = PadrePerfilForm(instance=padre)
        if usuario.tipo == 'Organización':
            organizacion = Organizacion.buscar(id_organizacion=usuario.id)
            form = OrganizacionPerfilForm(instance=organizacion)

    if request.method == 'POST':
        if usuario.es_superusuario:
            form = SuperusuarioPerfilForm(request.POST, request.FILES, instance=usuario)
        if usuario.tipo == 'Joven':
            joven = Joven.buscar(id_joven=usuario.id)
            form = JovenPerfilForm(request.POST, request.FILES, instance=joven)
        if usuario.tipo == 'Padre':
            padre = Padre.buscar(pk=usuario.id)
            form = PadrePerfilForm(request.POST, request.FILES, instance=padre)
        if usuario.tipo == 'Organización':
            organizacion = Organizacion.buscar(id_organizacion=usuario.id)
            form = OrganizacionPerfilForm(request.POST, request.FILES, instance=organizacion)

        if form.is_valid():
            usuario = form.save(commit=False)
            if request.user.es_superusuario:
                usuario_modificar = Usuario.obtener_usuario_pk(pk=request.user.id)
            if request.user.tipo == 'Joven':
                usuario_modificar = Joven.buscar(id_joven=usuario.id)
            if request.user.tipo == 'Padre':
                usuario_modificar = Padre.buscar(pk=usuario.id)
            if request.user.tipo == 'Organización':
                usuario_modificar = Organizacion.buscar(id_organizacion=usuario.id)

            if usuario_modificar.email != usuario.email:
                enviar_correo(usuario.email, None, VERIFICACION_USUARIO_CONTRASENIA, usuario)
                messages.success(request, "Correo de verificación enviado")
            import os
            imagen_anterior = Usuario.obtener_usuario_pk(pk=form.instance.pk).foto_perfil
            if not request.POST.get('imagen-clear') is None and imagen_anterior:
                if os.path.isfile(imagen_anterior.path):
                    os.remove(imagen_anterior.path)
                usuario.foto_perfil = ""
            usuario.save()

            if usuario.tipo == 'Organización':
                logo_anterior = Organizacion.buscar(id_organizacion=form.instance.pk).logo
                if not request.POST.get('logo-clear') is None and logo_anterior:
                    if os.path.isfile(logo_anterior.path):
                        os.remove(logo_anterior.path)
                    usuario.logo = ""
                usuario.save()

            messages.success(request, "Perfil actualizado exitosamente")
            return redirect('usuarios:perfil')
        else:
            print(form.errors)
            messages.error(request, "Ha ocurrido un error. Por favor intente de nuevo.")
    from config.settings.base import MAP_WIDGETS
    GOOGLE_KEY = MAP_WIDGETS['GOOGLE_MAP_API_KEY']

    return render(request, 'usuarios/mi_perfil.html', {
        'form': form, 'pk_usuario': usuario.pk, 'direccion': get_object_or_404(Usuario, pk=int(usuario.pk)).direccion,
        'GOOGLE_KEY': GOOGLE_KEY
    })


def reenviar_correo(request: 'request', id_joven: int) -> 'render':
    '''
        Permite reenviar un correo de validación al usuario.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    '''
    from kairos.gestor_correos.envio_correos import enviar_correo, VERIFICACION_USUARIO_CONTRASENIA

    if Joven.existe_usuario(id_joven) == True:
        joven = Joven.buscar(id_joven)
        enviar_correo(joven.email, None, VERIFICACION_USUARIO_CONTRASENIA, joven)
        messages.success(request, "Se ha reenviado un correo de validación.")
    else:
        messages.error(request, 'El usuario no existe')

    return render(request, 'usuarios/correo_registro.html', {
        'usuario': joven
    })


@login_required
def restablecer_contrasena(request: 'request', id_usuario: int):
    '''
        Permite al administrador cambiar la contraseña de un usuario.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_usuario (int): Identificador del usuario al que se le cambiara la contraseña
    '''
    form = ContrasenaModelForm()

    if request.user.is_superuser == False:
        messages.error(request, "Usted no está autorizado para gestionar a los usuarios")
        return redirect('inicio')

    if request.method == 'POST':
        form = ContrasenaModelForm(request.POST)
        password = request.POST['password']
        if form.is_valid():
            form.clean()
            usuario = Usuario.obtener_usuario_pk(pk=id_usuario)
            usuario.set_password(password)
            usuario.save()
            messages.success(request, "Contraseña restablecida satisfactoriamente")
            return redirect('usuarios:listado_usuarios')

    return render(request, 'usuarios/restablecer_password.html', {
        'form': form
    })


@login_required
def cerrar_sesion(request) -> 'redirect':
    """
        Permite a un usuario cerrar sesión

        Parámetros:
            request: Elemento Http que posee información de la sesión.
        
    """
    logout(request)
    messages.success(request, "Sesión cerrada correctamente")
    return redirect('usuarios:iniciar_sesion')


def solicitar_contrasena(request: 'request') -> 'render':
    '''
        Permite a un Joven solicitar restablecer su contraseña enviándole un correo.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    '''
    from kairos.gestor_correos.envio_correos import enviar_correo, SOLICITAR_CONTRASENIA

    form = CorreoModelForm()

    if request.user.is_authenticated:
        messages.error(request, "Usted no está autorizado para realizar esta acción!")
        return redirect('inicio')

    if request.method == 'POST':
        form = CorreoModelForm(request.POST)
        email = request.POST['email']
        if form.is_valid():
            form.clean()
            usuario = Usuario.obtener_usuario_email(email=email)
            enviar_correo(email, None, SOLICITAR_CONTRASENIA, usuario)
            messages.success(request,
                             "Se ha enviado un enlace al correo electrónico proporcionado para restablecer la contraseña!")
            return redirect('usuarios:iniciar_sesion')

    return render(request, 'usuarios/restablecer_contrasena/solicitar.html', {
        'form': form
    })


def solicitar_correo_verificacion(request: 'request') -> 'render':
    '''
        Permite a un Joven solicitar restablecer su contraseña enviándole un correo.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    '''
    from kairos.gestor_correos.envio_correos import enviar_correo, VERIFICACION_USUARIO_CONTRASENIA

    form = CorreoModelForm()

    if request.method == 'POST':
        form = CorreoModelForm(request.POST)
        email = request.POST['email']
        if form.is_valid():
            form.clean()
            usuario = Usuario.obtener_usuario_email(email=email)
            enviar_correo(usuario.email, None, VERIFICACION_USUARIO_CONTRASENIA, usuario)
            messages.success(request,
                             "Se ha enviado un enlace al correo electrónico proporcionado para verificar su correo!")
            return redirect('usuarios:iniciar_sesion')

    return render(request, 'usuarios/restablecer_contrasena/solicitar.html', {
        'form': form
    })


class VerificarCorreoAPIView(APIView):
    def get(self, request, *args, **kwargs):
        '''
            API que verifica el correo del usuario.

            Parámetros:
                request: Elemento Http que posee informacion de la sesion.
                request.GET: {
                    token: Token que posee la información del usuario
                }
            Retorno:
                redirect('inicio')
        '''
        from rest_framework import status
        import jwt

        from .serializers import VerficarCorreoSerializer
        from config.settings.base import SECRET_KEY

        token = request.GET['token']
        payload = jwt.decode(token, SECRET_KEY, algorithms=['HS256'])
        usuario = Usuario.obtener_usuario_email(payload['usuario'])
        serializer = VerficarCorreoSerializer(data={'token': token})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        login(request, usuario, backend='django.contrib.auth.backends.ModelBackend')

        data = {'message': 'Cuenta verificada'}
        return redirect('inicio')


class CambioContrasenia(PasswordResetConfirmView):
    template_name = 'usuarios/restablecer_contrasena/cambiar.html'

    def get_success_url(self):
        try:
            uid = urlsafe_base64_decode(self.kwargs['uidb64'])
            usuario = Usuario._default_manager.get(pk=uid)
            usuario.correo_verificado = True
            usuario.save()
        except (TypeError, ValueError, OverflowError, Usuario.DoesNotExist):
            pass

        return reverse_lazy('usuarios:contrasena_cambiada')


def contrasena_cambiada(request) -> 'render':
    """
        Vista que te redirecciona al formulario de cambio de contraseña.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    """

    return render(request, 'usuarios/restablecer_contrasena/confirmada.html', {
        'titulo': 'Contraseña Restablecida',
        'mensaje': 'Tu contraseña ha sido restablecida exitosamente.'
    })


class RegistroMasivo(LoginRequiredMixin, TemplateView):
    template_name = 'usuarios/registro_masivo.html'


def cargar_excel(request):
    """
        Vista que te redirecciona al formulario de cambio de contraseña.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
    """

    if request.method == 'POST' and request.FILES['archivo_excel']:
        archivo = request.FILES['archivo_excel']

        import pandas as pd
        excel = pd.ExcelFile(archivo)
        hoja = excel.sheet_names[0]
        df = pd.read_excel(excel, hoja, thousands=",")
        res = Usuario.cargue_masivo(df)
        if res == True:
            messages.success(request, 'Usuarios cargados exitosamente al sistema')
        else:
            messages.error(request, 'Ha ocurrido un error al cargar los usuarios al sistema')
        return redirect('usuarios:listado_usuarios')


# ###############APIS#############################
class ApiGuardarDireccion(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        feature = data["feature"]
        pk_usuario = int(data["pk_usuario"])
        usuario = get_object_or_404(Usuario, id=pk_usuario)
        geometry = feature["geometry"]
        info = feature["properties"]["info"]
        tipo = info["tipo"]
        estado_objeto = info["estado_objeto"]
        if tipo == 'DIRECCION':
            if estado_objeto != "SIN_MODIFICAR":
                usuario.ubicacion = geometry
                usuario.save()
        # Generar nueva dirección a partir de las coordenadas
        import geocoder
        from config.settings.base import MAP_WIDGETS
        GOOGLE_KEY = MAP_WIDGETS['GOOGLE_MAP_API_KEY']
        g = geocoder.google(geometry['coordinates'], method='reverse', key=GOOGLE_KEY)
        usuario.direccion = str(g.address)
        usuario.save()
        return Response({'direccion': usuario.direccion})


class ApiCargarDireccion(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        request_data = json.loads(json_text)
        pk_usuario = int(request_data["pk_usuario"])
        usuario = get_object_or_404(Usuario, id=pk_usuario)
        lat_lng, centro = None, None
        import geocoder
        from config.settings.base import MAP_WIDGETS
        GOOGLE_KEY = MAP_WIDGETS['GOOGLE_MAP_API_KEY']
        if not usuario.ubicacion is None:
            lat_lng = usuario.ubicacion['coordinates']
            direccion = usuario.direccion
        elif not usuario.direccion is None:
            g = geocoder.google(usuario.direccion, key=GOOGLE_KEY)
            lat_lng = g.latlng
            direccion = usuario.direccion
        if lat_lng is None:
            g = geocoder.google(usuario.pais.name, key=GOOGLE_KEY)
            centro = g.latlng
            direccion = usuario.pais.name

        coleccion = {
            'type': 'FeatureCollection',
            'features': []
        }
        feature_aux = {
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                "coordinates": lat_lng,
            },
            'properties': {
                'info': {
                    'tipo': 'DIRECCION',
                    'direccion': direccion,
                    'color': '0C2C37',
                    'centro': centro
                }
            }
        }
        coleccion['features'].append(feature_aux)
        return Response(coleccion)


class ExperienciasActualesConsultar(LoginRequiredMixin, ListView):
    model = Joven
    template_name = "usuarios/jovenes/experiencia_actual_listado.html"
    context_object_name = "jovenes"

    def get_queryset(self):
        jovenes = Joven.obtener_jovenes()
        return jovenes


class JovenesConsultar(LoginRequiredMixin, ListView):
    template_name = "usuarios/amigos/consultar_jovenes.html"
    paginate_by = 10
    context_object_name = 'jovenes'

    def get_queryset(self):
        consulta = self.obtener_consulta()
        return consulta['jovenes']

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator
        from kairos.guia.models import Recurso
        context = super(JovenesConsultar, self).get_context_data(**kwargs)
        usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()

        pagination = Paginator(list(consulta['jovenes']), self.paginate_by)

        context['jovenes'] = pagination.page(context['page_obj'].number)
        context['tag'] = consulta['tag']
        context['tipos'] = [
            {'nombre': 'Todos'},
            {'nombre': 'Sugeridos'},
            {'nombre': 'Mis amigos'},
            {'nombre': 'Solicitudes'},

        ]

        return context

    def get_success_url(self):
        if self.request.user.es_superusuario:
            return reverse_lazy('contenido:listado_oportunidades')
        else:
            return reverse_lazy('contenido:listado_mis_oportunidades_creadas')

    def obtener_consulta(self):
        from kairos.core.utils import obtener_lista_usuario_caracteristicas
        usuario = self.obtener_usuario()

        if 'tag' in self.request.GET:
            tag = self.request.GET['tag']
        elif list(self.request.GET):
            tag = list(self.request.GET)[0]
        else:
            tag = "Todos"

        jovenes = []

        if self.request.GET:
            if 'q' in self.request.GET:
                query = self.request.GET['q']

        if tag == 'Mis amigos':
            if 'q' in self.request.GET:
                jovenes = obtener_lista_usuario_caracteristicas(usuario,
                                                                usuario.buscador(query, usuario.obtener_amigos()))
            else:
                jovenes = obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_amigos())
        elif tag == 'Solicitudes':
            if 'q' in self.request.GET:
                jovenes = obtener_lista_usuario_caracteristicas(usuario, usuario.buscador(query,
                                                                                          usuario.obtener_lista_usuarios_solicitudes()))
            else:
                jovenes = obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_lista_usuarios_solicitudes())
        elif tag == 'Sugeridos':
            if 'q' in self.request.GET:
                jovenes = obtener_lista_usuario_caracteristicas(usuario,
                                                                usuario.buscador(query, usuario.obtener_sugeridos()))
            else:
                jovenes = obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_sugeridos())
        elif tag == 'Todos':
            if 'q' in self.request.GET:
                jovenes = obtener_lista_usuario_caracteristicas(usuario, usuario.buscador(query,
                                                                                          usuario.obtener_jovenes_no_amigos().exclude(
                                                                                              id__in=[usuario.id])))
            else:
                jovenes = obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_jovenes_no_amigos().exclude(
                    id__in=[usuario.id]))
        return {'tag': tag, 'jovenes': jovenes}

    def obtener_usuario(self):
        usuario = Joven.buscar(id_joven=self.request.user.id)
        return usuario


# usuario_tarjeta hace referencia a donde se esta realizando la accion:
# usuario_tarjeta=0 hace referencia al template en usuarios->amigos->consultar-jovenes.html
# usuario_tarjeta=1 hace referencia al template en usaurios->tarjeta_presentacion.html
@login_required
def enviar_solicitud(request, id_receptor: int, usuario_tarjeta: int) -> 'redirect':
    '''
        Permite a un Joven enviar invitación de amistad a otro Joven

        Parametros:
            request: Elemento Http que posee información de la sesión.
            id_receptor (int): Identificador del joven que recibe la solicitud.
            usuario_tarjeta (int): Template en el que esta realizándose la acción.
    '''
    if Joven.existe_joven(id_receptor):
        if request.user.is_authenticated == True:
            joven_emisor = Joven.buscar(id_joven=request.user.id)
            joven_receptor = Joven.buscar(id_joven=id_receptor)
            if SolicitudAmistad.existe_solitud(emisor=joven_emisor,
                                               receptor=joven_receptor) == True or SolicitudAmistad.existe_solitud(
                emisor=joven_receptor, receptor=joven_emisor) == True:
                messages.error(request, "Ya se ha enviado una solicitud a ese joven")
            else:
                messages.success(request, "Solicitud de amistad enviada correctamente")
                SolicitudAmistad.crear_solicitud(emisor=joven_emisor, receptor=joven_receptor)
    else:
        messages.error(request, "El joven no existe")

    if usuario_tarjeta == 0:
        return redirect('usuarios:consultar_jovenes')
    else:
        return redirect('usuarios:tarjeta_presentacion', id_receptor)


@login_required
def aceptar_solicitud(request, id_emisor: int, usuario_tarjeta: int) -> 'redirect':
    '''
        Permite a un Joven aceptar la SolicitudAmistad de otro Joven

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_emisor (int): Identificador del joven que envía la solicitud.
            usuario_tarjeta (int): Template en el que esta realizándose la acción.
    '''
    if Joven.existe_joven(id_emisor):
        if request.user.is_authenticated == True:
            joven_emisor = Joven.buscar(id_joven=id_emisor)
            joven_actual = Joven.buscar(id_joven=request.user.id)

            if SolicitudAmistad.existe_solitud(emisor=joven_emisor, receptor=joven_actual) == True:
                solicitud = SolicitudAmistad.buscar_solicitud(emisor=joven_emisor, receptor=joven_actual)
                joven_emisor = solicitud.emisor
                joven_actual = solicitud.receptor
                joven_emisor.agregar_amigo(joven_actual)
                joven_actual.agregar_amigo(joven_emisor)
                solicitud.delete()
                messages.success(request, "Solicitud de amistad aceptada")
            else:
                messages.error(request, "Este usuario no te ha enviado solicitud de amistad")
    else:
        messages.error(request, "El joven no existe")

    if usuario_tarjeta == 0:
        return redirect('usuarios:consultar_jovenes')
    else:
        return redirect('usuarios:tarjeta_presentacion', id_emisor)


@login_required
def rechazar_solicitud(request, id_emisor: int, usuario_tarjeta: int = 0) -> 'redirect':
    '''
        Permite a un Joven rechazar la SolicitudAmistad de otro Joven

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_emisor: Identificador del joven que envía la solicitud.
            usuario_tarjeta: Template en el que esta realizándose la acción.
    '''
    if Joven.existe_joven(id_emisor):
        if request.user.is_authenticated == True:
            joven_emisor = Joven.buscar(id_joven=id_emisor)
            joven_actual = Joven.buscar(id_joven=request.user.id)

            if SolicitudAmistad.existe_solitud(emisor=joven_emisor, receptor=joven_actual) == True:
                solicitud = SolicitudAmistad.buscar_solicitud(emisor=joven_emisor, receptor=joven_actual)
                solicitud.delete()
                messages.success(request, "Solicitud de amistad rechazada")
            elif SolicitudAmistad.existe_solitud(emisor=joven_actual, receptor=joven_emisor) == True:
                solicitud = SolicitudAmistad.buscar_solicitud(emisor=joven_actual, receptor=joven_emisor)
                solicitud.delete()
                messages.success(request, "Solicitud de amistad cancelada")
            else:
                messages.error(request, "Este usuario no te ha enviado solicitud de amistad")
    else:
        messages.error(request, "El joven no existe")

    if usuario_tarjeta == 0:
        return redirect('usuarios:consultar_jovenes')
    else:
        return redirect('usuarios:tarjeta_presentacion', id_emisor)


@login_required
def eliminar_amigo(request, id_emisor: int, usuario_tarjeta: int = 0) -> 'redirect':
    '''
        Permite a un Joven eliminar a un amigo.

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_emisor: Identificador del amigo a borrar.
            usuario_tarjeta: Template en el que esta realizándose la acción.
    '''
    if request.user.is_authenticated and request.user.tipo == 'Joven':
        relacion = False
        if Joven.existe_joven(id_emisor):
            joven_emisor = Joven.buscar(id_joven=id_emisor)
            joven_actual = Joven.buscar(id_joven=request.user.id)
            if joven_actual.es_amigo_de_usuario(joven_emisor) or joven_emisor.es_amigo_de_usuario(joven_actual):
                joven_actual.amistades.remove(joven_emisor)
                joven_emisor.amistades.remove(joven_actual)
                messages.success(request,
                                 "has finalizado tu amistad con el usuario " + joven_emisor.first_name + " " + joven_emisor.last_name)
            else:
                messages.error(request, "No hay una relación de amistad con el joven")
        else:
            messages.error(request, "El joven no existe")
    else:
        messages.error(request, "El usuario no tiene permisos para esa acción")

    if usuario_tarjeta == 0:
        return redirect('usuarios:consultar_jovenes')
    else:
        return redirect('usuarios:tarjeta_presentacion', id_emisor)


class TarjetaPresentacionUsuario(LoginRequiredMixin, TemplateView):
    template_name = 'usuarios/tarjeta_presentacion.html'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated == True:
            if not Joven.existe_joven(kwargs['id_usuario']):
                messages.error(self.request, "El joven no existe")
                return redirect('usuarios:consultar_jovenes')

        return super(TarjetaPresentacionUsuario, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        from kairos.core.utils import relacion_con_usuario

        context = dict()

        if self.request.user.tipo == 'Joven':
            joven_sesion = Joven.buscar(id_joven=self.request.user.id)
            joven_tarjeta = Joven.buscar(id_joven=kwargs['id_usuario'])
            context = relacion_con_usuario(joven_sesion, joven_tarjeta, estado=1)
        elif self.request.user.tipo == 'Organización':
            organizacion_sesion = Organizacion.buscar(id_organizacion=self.request.user.id)
            joven_tarjeta = Joven.buscar(id_joven=kwargs['id_usuario'])
            context = relacion_con_usuario(organizacion_sesion, joven_tarjeta, estado=1)

        return context


class ModificarPerfilActual(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = Joven
    form_class = JovenDatosBasicosForm
    template_name = 'usuarios/jovenes/perfiles/modificar_actual.html'
    mensaje_exito = 'Perfil actual guardado correctamente!'
    mensaje_error = 'Ha ocurrido un error al guardar el perfil, por favor intente de nuevo!'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect('inicio')

        if not request.user.tipo == "Joven":
            messages.error(request, "Usted no está autorizado para modificar el perfil!")
            return redirect("inicio")
        return super(ModificarPerfilActual, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.factores_sensacion_bienestar = form.cleaned_data['factores_sensaciones_bienestar']
        form.instance.jornadas = form.cleaned_data['jornadas_del_joven']
        return super(ModificarPerfilActual, self).form_valid(form)

    def get_initial(self):
        initial = super(ModificarPerfilActual, self).get_initial()
        joven = self.get_object()
        initial['factores_sensaciones_bienestar'] = joven.factores_sensacion_bienestar
        initial['jornadas_del_joven'] = joven.jornadas
        return initial

    def get_success_url(self):
        return reverse_lazy("usuarios:mi_perfil_actual")


def mi_perfil_actual_detalle(request) -> 'render':
    """
        Permite consultar el detalle del perfil del usuario

        Parámetros:
            request: Elemento Http que posee información de la sesión.                
    """
    from kairos.guia.models import Recurso

    template_name = "usuarios/jovenes/perfiles/detalle_actual.html"
    joven = Joven.buscar(id_joven=request.user.pk)

    return render(request, template_name, {'joven': joven})


class InvitarJovenesPlataforma(FormView):
    form_class = FormularioInvitacionJoven
    template_name = 'usuarios/amigos/invitar_jovenes.html'

    def form_valid(self, form):
        from kairos.usuarios.models import SolicitudAmistad
        from kairos.gestor_correos.envio_correos import enviar_correo, INVITAR_JOVENES
        if self.request.method == 'POST' and (
                'archivo_excel' in self.request.FILES or 'invitar_por_correo' in self.request.POST):
            joven = Joven.buscar(id_joven=self.request.user.id)
            if 'archivo_excel' in self.request.FILES:
                import pandas as pd

                archivo = self.request.FILES['archivo_excel']
                excel = pd.ExcelFile(archivo)
                hoja = excel.sheet_names[0]
                df = pd.read_excel(excel, hoja)
                lista_correos = df.iloc[:, 0].tolist()

                for correo in lista_correos:
                    if Joven.existe_joven_correo(correo):
                        joven_receptor = Joven.buscar_email(email=correo)
                        if not SolicitudAmistad.existe_solitud(emisor=joven,
                                                               receptor=joven_receptor) and not joven_receptor.es_amigo_de_usuario(
                            joven):
                            enviar_correo(correo, self.request.user, INVITAR_JOVENES)
                    else:
                        enviar_correo(correo, self.request.user, INVITAR_JOVENES)
                messages.success(self.request, 'Se han enviado las invitaciones desde la carga masiva correctamente')

            if 'invitar_por_correo' in self.request.POST:
                correos = self.request.POST['invitar_por_correo'].split(',')
                for correo in correos:
                    enviar_correo(correo, self.request.user, INVITAR_JOVENES)

            messages.success(self.request, 'Se han enviado las invitaciones correctamente')

        else:
            messages.error(self.request, 'Ha ocurrido un error al cargar el archivo, por favor intente de nuevo!')

        return redirect('usuarios:invitar_joven')


def verificacion_estado_videos_guia(request):
    """
        Verifica si un usuario ya ha visto el video guía de la sección

        Parámetros:
            request: Elemento Http que posee información de la sesión.                
    """
    if request.is_ajax():
        valor_del_video = request.POST.get('valor_del_video')
        video = request.POST.get('id_video')
        usuario_id = request.POST.get('id_joven')

        if valor_del_video == '1':
            VerRecurso.cambiar_estado(id_video=video, valor_del_video=valor_del_video, usuario_id=usuario_id)
    return redirect("inicio")


@login_required
def postulacion_lider_organizacion(request, id_organizacion: int, id_joven: int) -> 'render':
    """
        Envía una solicitud de un líder de equipo a una organización

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_organizacion (int): id de la organización         
            id_joven (int): id del líder de un equipo
    """
    from kairos.organizaciones.models import SolicitudesOrganizacionEquipos

    organizacion = Organizacion.buscar(id_organizacion)
    joven_lider = Joven.buscar(id_joven)

    if organizacion == None:
        return redirect("inicio")
    if joven_lider == None:
        return redirect("inicio")

    equipos = organizacion.obtener_lista_equipos_solicitudes()
    form_postulacion = PostulacionLiderOrganizacionForm(instance=organizacion, joven_lider=joven_lider)

    if request.method == 'POST':
        if 'enviar_postulacion' in request.POST:
            form_postulacion = PostulacionLiderOrganizacionForm(request.POST, instance=organizacion,
                                                                joven_lider=joven_lider)
            if form_postulacion.is_valid():
                for id_equipo in request.POST.getlist('solicitudes_equipos'):
                    equipo = Equipo.buscar(id_equipo)
                    if not equipo in equipos:
                        postulacion = SolicitudesOrganizacionEquipos.crear_solicitud(equipo=equipo,
                                                                                     organizacion=organizacion)
                messages.success(request, 'Postulación enviada a la organización')
                return redirect('usuarios:enviar_postulacion', id_organizacion=id_organizacion, id_joven=id_joven)

        if 'finalizar' in request.POST:
            return redirect("organizaciones:joven_consultar")

    return render(request, 'organizaciones/equipos/lider/postular_equipos.html', {
        'form_postulacion': form_postulacion,
        'equipos': equipos,
        'organizacion': organizacion
    })


@login_required
def retirar_postulacion_lider_organizacion(request, id_organizacion: int, id_equipo: int):
    """
        Retira una solicitud de un equipo a una organización

        Parámetros:
            request: Elemento Http que posee información de la sesión.
            id_organizacion (int): id de la organización         
            id_equipo (int): id del equipo
    """
    equipo = Equipo.buscar(id_equipo)
    organizacion = Organizacion.buscar(id_organizacion=id_organizacion)
    joven = Joven.buscar(request.user.id)

    if equipo == None:
        return redirect("inicio")

    if organizacion == None:
        return redirect("inicio")

    organizacion.cancelar_postulacion(equipo)
    messages.success(request, "Postulación retirada correctamente")
    return redirect('usuarios:enviar_postulacion', id_organizacion=id_organizacion, id_joven=joven.id)


class GestionRetosAdministrador(LoginRequiredMixin, TemplateView):
    template_name = "organizaciones/jovenes/consultar_jovenes.html"

    def get_context_data(self, **kwargs):
        context = dict()
        usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()
        context['jovenes'] = consulta['jovenes']
        context['tag'] = consulta['tag']
        context['tipos'] = [{'nombre': 'Mis Jóvenes'},
                            {'nombre': 'Solicitudes'},
                            {'nombre': 'Invitar'},
                            ]

        return context

    def obtener_consulta(self):
        from kairos.core.utils import obtener_lista_usuario_caracteristicas
        from kairos.organizaciones.models import Organizacion
        usuario = self.obtener_usuario()
        tag = 'Mis Jóvenes'
        jovenes = []

        if self.request.GET:
            if 'tag' in self.request.GET:
                tag = self.request.GET['tag']

        if tag == 'Mis Jóvenes':
            jovenes = obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_miembros())
        elif tag == 'Solicitudes':
            jovenes = obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_lista_usuarios_solicitudes())
        elif tag == 'Invitar':
            jovenes = obtener_lista_usuario_caracteristicas(usuario, usuario.obtener_jovenes_no_miembros())

        return {'tag': tag, 'jovenes': jovenes}

    def obtener_usuario(self):
        usuario = Organizacion.buscar(id_organizacion=self.request.user.id)
        return usuario


class MagicLink(TemplateView):

    def get(self, request, *args, **kwargs):
        from sesame.utils import get_query_string

        organizaciones = request.GET['name-org'].split(",")[:-1]
        if organizaciones:
            personas = Joven.buscador_organizaciones_joven(ids_organizaciones=organizaciones)
        else:
            personas = Joven.obtener_jovenes()

        # Crear el libro de trabajo
        wb = Workbook()
        # Definir como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        # Encabezados del archivo
        ws['A1'] = 'first_name'
        ws['B1'] = 'last_name'
        ws['C1'] = 'indicativo_telefono'
        ws['D1'] = 'telefono'
        ws['E1'] = 'magic_link'
        cont = 2
        for persona in personas:
            ws.cell(row=cont, column=1).value = persona.first_name
            ws.cell(row=cont, column=2).value = persona.last_name
            ws.cell(row=cont, column=3).value = persona.indicativo_telefono
            ws.cell(row=cont, column=4).value = persona.telefono
            ws.cell(row=cont, column=5).value = request.get_host() + '/' + get_query_string(persona)
            cont = cont + 1
        # Establecer nombre del archivo
        nombre_archivo = "MagicLink.xlsx"
        # Definir que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response


class ExportarJovenes(TemplateView):
    """
        Se exportan todos los jovenes con datos basicos
    """

    def get(self, request, *args, **kwargs):

        organizaciones = request.GET['name-org'].split(",")[:-1]
        if organizaciones:
            personas = Joven.buscador_organizaciones_joven(ids_organizaciones=organizaciones)
        else:
            personas = Joven.obtener_jovenes()

        wb = Workbook()
        ws = wb.active
        ws['A1'] = 'Correo'
        ws['B1'] = 'Tipo de documento'
        ws['C1'] = 'Documento'
        ws['D1'] = 'Nombres y apellidos'
        ws['E1'] = 'Genero'
        ws['F1'] = 'Fecha de nacimiento'
        ws['G1'] = 'Institucion'
        ws['H1'] = 'Grado'
        ws['I1'] = 'Indicativo'
        ws['J1'] = 'Telefono'
        ws['K1'] = 'Fecha de Registro'
        cont = 2
        for persona in personas:
            ws.cell(row=cont, column=1).value = persona.email
            ws.cell(row=cont, column=2).value = persona.tipo_identificacion
            ws.cell(row=cont, column=3).value = persona.identificacion
            ws.cell(row=cont, column=4).value = persona.get_full_name()
            ws.cell(row=cont, column=5).value = persona.genero
            if persona.fecha_de_nacimiento == None:
                ws.cell(row=cont, column=6).value = ' '
            else:
                ws.cell(row=cont, column=6).value = persona.fecha_de_nacimiento.strftime('%Y-%m-%d')
            if Joven.joven_pertenece_colegio(persona):
                ws.cell(row=cont, column=7).value = persona.obtener_colegio().first_name
            else:
                org = persona.obtener_organizaciones_joven().first()
                if org:
                    if org.es_principal:
                        ws.cell(row=cont, column=7).value = 'No pertenece'
                    else:
                        ws.cell(row=cont, column=7).value = org.first_name
                else:
                    ws.cell(row=cont, column=7).value = 'No pertenece'
            if persona.ultimo_grado == 0:
                ws.cell(row=cont, column=8).value = 'No aplica'
            else:
                ws.cell(row=cont, column=8).value = persona.ultimo_grado
            ws.cell(row=cont, column=9).value = persona.indicativo_telefono
            ws.cell(row=cont, column=10).value = persona.telefono
            ws.cell(row=cont, column=11).value = persona.creado.strftime('%Y-%m-%d')

            cont = cont + 1
        # Establecer nombre del archivo
        nombre_archivo = "jovenes.xlsx"
        # Definir que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response


class ApiGuardarRealidad(APIView):
    def post(self, request):
        id_joven = request.POST.get("id_joven")
        realidad = request.POST.get("realidad")
        joven = Joven.buscar(id_joven=id_joven)
        joven.nivel_realidad = realidad
        joven.save()
        return Response({})
