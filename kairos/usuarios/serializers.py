from django.db.models import fields
from rest_framework import serializers
from django.db.models import Q

from kairos.usuarios.models import Joven, Usuario
import jwt
from config.settings.base import SECRET_KEY

class VerficarCorreoSerializer(serializers.Serializer):

    token = serializers.CharField()

    def validate_token(self, data):
        try:
            payload = jwt.decode(data, SECRET_KEY, algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise serializers.ValidationError("Expiro el link de verificación")
        except jwt.PyJWTError:
            raise serializers.ValidationError("Link de validación invalido")
        if payload['type'] != 'confirmacion_email':
            raise serializers.ValidationError("Link de validación invalido")
        
        self.context['payload'] = payload
        return data
    
    def save(self):
        payload = self.context['payload']
        user = Usuario.objects.get(email=payload['usuario'])
        user.correo_verificado = True
        user.save()

class JovenSerializer(serializers.ModelSerializer):
    formularios = serializers.SerializerMethodField()
    class Meta:
        model = Joven
        fields = ['id', 'first_name', 'last_name', 'email', 'creado', 'correo_verificado', 'indicativo_telefono', 'telefono', 'is_active', 'tipo', 'formularios' ]
    
    def get_formularios(self, obj):
        try:
            formularios_joven = obj.tanda_formularios.formularios.filter(activo=True)
        except AttributeError:
            return False

        formularios_disponibles = formularios_joven.filter(
            ~Q(respuestasjoven__joven=obj.id) | Q(respuestasjoven__joven=obj.id, respuestasjoven__completas=False)
        )
        if formularios_disponibles.count() == 0:
            return True
        else:
            return False
    
