# Modulos internos de la app
from .models import Usuario

def cleanup_social_account(backend, uid, user=None, *args, **kwargs):
    '''
    Compruebe si el objeto de usuario existe y se acaba de crear una nueva cuenta.

            Parametros:
                backend: contiene la información del provider(google, facebook, etc.).
                uid: identificado del usuario en la plataforma del provider.
                user: Representacion del objeto del modelo escogido en la variable de ambiente SOCIAL_AUTH_USER_MODEL

            Retorno:
                user: El objeto del joven con la información por defecto

    '''
    import time
    if user and kwargs.get('is_new', False):
        user.tipo = 'Joven'
        user.correo_verificado = True
        user.terminos = True
        user.pais_id = 253
        user.region_id = 3956
        user.ciudad_id = 1112000
        user.creado = time.strftime('%Y-%m-%d %H:%M:%S')
        user.tanda_formularios_id = 1
        
        if backend.name == 'facebook':
            user.username = kwargs['details']['email']
            user.email = kwargs['details']['email']
        user.save()

    return {'user': user}
