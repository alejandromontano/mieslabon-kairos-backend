# Modulos Django
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError

# Modulos de plugin externos
from django_select2.forms import Select2MultipleWidget, Select2Widget, ModelSelect2Widget, ModelSelect2MultipleWidget
from cities_light.models import Country, City, Region
from tempus_dominus.widgets import DatePicker
from PIL import Image, ExifTags
from config.settings.base import BASE_DIR
import os

# Modulos de otras apps
from kairos.organizaciones.models import Organizacion

# Modulos internos de la app
from .models import Usuario, Joven, Padre, SalarioPais
from kairos.jurados.models import Jurado

TIPOS_ID_JOVEN = (
    ("CC", "Cédula de ciudadanía"),
    ("TI", "Tarjeta de identidad"),
    ("RC", "Registro civil"),
    ("CE", "Cédula de extranjería"),
    ("PPN", "Pasaporte"),
    ("SSN", "Seguridad social"),
    ("DNI", "Documento nacional de identidad"),
    ("LIC", "Licencia"),
)
TIPOS_ID_PADRE = (
    ("CC", "Cédula de ciudadanía"),
    ("TI", "Tarjeta de identidad"),
    ("RC", "Registro civil"),
    ("CE", "Cédula de extranjería"),
    ("PPN", "Pasaporte"),
    ("SSN", "Seguridad social"),
    ("DNI", "Documento nacional de identidad"),
    ("LIC", "Licencia"),
)
TIPOS_ID_JURADO = (
    ("CC", "Cédula de ciudadanía"),
    ("RC", "Registro civil"),
    ("CE", "Cédula de extranjería"),
    ("PPN", "Pasaporte"),
    ("SSN", "Seguridad social"),
    ("DNI", "Documento nacional de identidad"),
    ("LIC", "Licencia"),
)
TIPOS_ID_ORGANIZACION = (
    ("NIT", "NIT"),
)
TIPOS_MONEDA_SALARIO = (
    ('Pesos', 'Pesos'),
    ('Dólares', 'Dólares'),
    ('Euros', 'Euros'),
    ('Libras', 'Libras'),
    ('Yenes', 'Yenes'),
    ('Wones', 'Wones'),
    ('Guaraníes', 'Guaraníes'),
    ('Bolivianos', 'Bolivianos'),
    ('Soles', 'Soles'),
    ('Reales', 'Reales'),
    ('Bolívares', 'Bolívares')
)

class LoginForm(forms.Form):
    username = forms.CharField(label="Correo electrónico", max_length=50,
                               widget=forms.TextInput(attrs={
                                   'id': 'usernameInput',
                                   'placeholder': 'Correo electrónico',
                                   'class': 'form-control'
                               }))
    password = forms.CharField(label="Contraseña", max_length=50,
                               widget=forms.TextInput(attrs={
                                   'type': 'password',
                                   'id': 'passwordInput',
                                   'placeholder': 'Contraseña',
                                   'class': 'form-control'
                               }))
    remember_me = forms.BooleanField(required=False, widget=forms.CheckboxInput())

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = False
        self.fields['password'].label = False
        self.fields['remember_me'].label = 'Recuerdame'


    def clean_username(self):
        username = self.cleaned_data['username']
        return username.lower()



# ################ADMIN FORMS#############################
class CrearUsuarioForm(UserCreationForm):
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_PADRE,
        initial='CC',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    def __init__(self, *args, **kwargs):
        super(CrearUsuarioForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['email'].required = True

        self.fields['first_name'].label = "Nombres*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['password1'].label = "Contraseña nueva*"
        self.fields['password2'].label = "Confirmar contraseña nueva*"
        self.fields['is_active'].label = "¿El usuario tiene acceso a la plataforma?*"

        self.fields['is_active'].help_text = ""
        self.fields['password1'].help_text = "- La contraseña debe de ser alfanumérica y tener mínimo 8 caracteres " \
                                             "<br>- No puede ser una contraseña usada comúnmente o similar a " \
                                             "los datos personales"
        self.fields['password2'].help_text = "Introduzca la misma contraseña"

    class Meta:
        model = Usuario
        fields = ('first_name', 'tipo_identificacion', 'identificacion', 'email', 'pais', 'region', 'ciudad',
                  'direccion', 'telefono', 'password1', 'password2','is_active', 'indicativo_telefono')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        if Usuario.objects.filter(email=email).exists():
            raise ValidationError('Este correo ya existe!')
        return email.lower()

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido


class JovenModelForm(UserCreationForm):
    fecha_de_nacimiento = forms.DateField(
        widget=DatePicker(
            options={
                'locale': 'es',
                'format': 'YYYY-MM-DD',
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': False,
            },
        ),
    )

    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_JOVEN,
        initial='TI',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    def __init__(self, *args, **kwargs):
        super(JovenModelForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.fields["fecha_de_nacimiento"].widget.attrs.update({
            'class': 'fecha readonly',
        })
        self.fields['telefono'].label = "Número de celular*"
        self.fields['first_name'].label = "Nombres*"
        self.fields['last_name'].label = "Apellidos*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['password1'].label = "Contraseña nueva*"
        self.fields['password2'].label = "Confirmar contraseña nueva*"
        self.fields['is_active'].label = "¿El usuario tiene acceso a la plataforma?*"
        self.fields['indicativo_telefono'].initial = "57"
        self.fields['is_active'].help_text = ""
        self.fields['password1'].help_text = "- La contraseña debe de ser alfanumérica y tener mínimo 8 caracteres <br>- No puede ser una contraseña usada comúnmente o similar a los datos personales"
        self.fields['password2'].help_text = "Introduzca la misma contraseña"


    class Meta:
        model = Joven
        fields = ('first_name', 'last_name', 'tipo_identificacion', 'identificacion','fecha_de_nacimiento', 'email',
                  'pais', 'region', 'ciudad', 'telefono', 'password1', 'password2', 'es_premium', 'is_active', 'indicativo_telefono')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        if Usuario.objects.filter(email=email).exists():
            raise ValidationError('Este correo ya existe!')
        return email.lower()

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido

class SalarioModelForm(forms.ModelForm):

    tipo_moneda = forms.ChoiceField(
        choices=TIPOS_MONEDA_SALARIO,
        initial='Pesos',
        widget=Select2Widget(),
        required=True,
        label="Tipo de moneda*"
    )

    def __init__(self, *args, **kwargs):
        super(SalarioModelForm, self).__init__(*args, **kwargs)
        self.fields['cantidad'].label = "Valor del país*"
        self.fields['activo'].label = "¿El salario esta activo?*"
        self.fields['pais'].label = "País*"
        self.fields['anio'].initial = 2021

    class Meta:
        model = SalarioPais
        fields = ('pais', 'tipo_moneda','cantidad', 'anio', 'activo')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
        }

    def clean_pais(self):
        pais = self.cleaned_data['pais']
        if SalarioPais.objects.filter(pais_id=pais.id).exists():
            raise ValidationError('Este salario ya existe!')
        return pais


class SalarioModificarForm(forms.ModelForm):
    tipo_moneda = forms.ChoiceField(
        choices=TIPOS_MONEDA_SALARIO,
        initial='Pesos',
        widget=Select2Widget(),
        required=True,
        label="Tipo de moneda*"
    )

    def __init__(self, *args, **kwargs):
        super(SalarioModificarForm, self).__init__(*args, **kwargs)
        self.fields['cantidad'].label = "Valor del pais*"
        self.fields['activo'].label = "¿El salario esta activo?*"

    class Meta:
        model = SalarioPais
        fields = ('tipo_moneda','cantidad', 'anio', 'activo')



class PadreModelForm(UserCreationForm):
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_PADRE,
        initial='CC',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    def __init__(self, *args, **kwargs):
        super(PadreModelForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

        self.fields['first_name'].label = "Nombres*"
        self.fields['last_name'].label = "Apellidos*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['password1'].label = "Contraseña nueva*"
        self.fields['password2'].label = "Confirmar contraseña nueva*"
        self.fields['is_active'].label = "¿El usuario tiene acceso a la plataforma?*"
        self.fields['indicativo_telefono'].initial = "57"

        self.fields['is_active'].help_text = ""
        self.fields['password1'].help_text = "- La contraseña debe de ser alfanumérica y tener mínimo 8 caracteres <br>- No puede ser una contraseña usada comúnmente o similar a los datos personales"
        self.fields['password2'].help_text = "Introduzca la misma contraseña"
        self.fields["hijos"].queryset = Joven.objects.filter(is_active=True)

    class Meta:
        model = Padre
        fields = ('first_name', 'last_name', 'tipo_identificacion', 'identificacion', 'email', 'pais', 'region', 'ciudad',
                  'telefono', 'hijos', 'password1', 'password2', 'is_active', 'indicativo_telefono')
        widgets = {
            'hijos': Select2MultipleWidget(),
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        if Usuario.objects.filter(email=email).exists():
            raise ValidationError('Este correo ya existe!')
        return email.lower()

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido

class JuradoModelForm(UserCreationForm):
    tipo_identificacion = forms.ChoiceField(
        choices = TIPOS_ID_JURADO,
        initial='CC',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    def __init__(self, *args, **kwargs):
        super(JuradoModelForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

        self.fields['first_name'].label = "Nombres*"
        self.fields['last_name'].label = "Apellidos*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['password1'].label = "Contraseña nueva*"
        self.fields['password2'].label = "Confirmar contraseña nueva*"
        self.fields['is_active'].label = "¿El usuario tiene acceso a la plataforma?*"
        self.fields['indicativo_telefono'].initial = "57"

        self.fields['is_active'].help_text = ""
        self.fields['password1'].help_text = "- La contraseña debe de ser alfanumérica y tener mínimo 8 caracteres <br>- No puede ser una contraseña usada comúnmente o similar a los datos personales"
        self.fields['password2'].help_text = "Introduzca la misma contraseña"

    class Meta:
        model = Jurado
        fields = ('first_name', 'last_name', 'tipo_identificacion','identificacion','email','pais', 'region','ciudad',
                  'telefono', 'password1', 'password2','is_active', 'indicativo_telefono')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains','continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains','search_names__icontains','id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        if Usuario.objects.filter(email=email).exists():
            raise ValidationError('Este correo ya existe!')
        return email.lower()

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido


class JuradoModificarForm(forms.ModelForm):
    tipo_identificacion = forms.ChoiceField(
        choices = TIPOS_ID_JURADO,
        initial='CC',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    def __init__(self, *args, **kwargs):
        super(JuradoModificarForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True

        self.fields['first_name'].label = "Nombres*"
        self.fields['last_name'].label = "Apellidos*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['is_active'].label = "¿El usuario tiene acceso a la plataforma?*"
        self.fields['is_active'].help_text = ""

    class Meta:
        model = Jurado
        fields = ('first_name', 'last_name', 'tipo_identificacion','identificacion','email','pais', 'region','ciudad',
                  'telefono', 'is_active', 'indicativo_telefono')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains','continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains','search_names__icontains','id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }

    def clean_email(self):
        pk_usuario = self.instance.pk
        usuario = Usuario.objects.get(id=pk_usuario)
        email = self.cleaned_data['email']
        email.lower()
        usuarios = Usuario.objects.filter(email=email)
        if usuarios.exists():
            if not usuario in usuarios:
                raise ValidationError('Este correo ya existe!')
        return email

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido

class JovenModificarForm(forms.ModelForm):
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_JOVEN,
        initial='TI',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    fecha_de_nacimiento = forms.DateField(
        widget=DatePicker(
            options={
                'format': 'YYYY-MM-DD',
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': False,
            },
        ),
    )

    def __init__(self, *args, **kwargs):
        super(JovenModificarForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields["fecha_de_nacimiento"].widget.attrs.update({
            'class': 'fecha readonly',
        })

        self.fields['first_name'].label = "Nombres*"
        self.fields['last_name'].label = "Apellidos*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['is_active'].label = "¿El usuario tiene acceso a la plataforma?*"
        self.fields['is_active'].help_text = ""

    class Meta:
        model = Joven
        fields = ('first_name', 'last_name', 'tipo_identificacion', 'identificacion', 'fecha_de_nacimiento', 'email',
                  'pais', 'region', 'ciudad', 'telefono', 'es_premium', 'is_active', 'indicativo_telefono')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }

    def clean_email(self):
        pk_usuario = self.instance.pk
        usuario = Usuario.objects.get(id=pk_usuario)
        email = self.cleaned_data['email']
        email.lower()
        usuarios = Usuario.objects.filter(email=email)
        if usuarios.exists():
            if not usuario in usuarios:
                raise ValidationError('Este correo ya existe!')
        return email

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

class PadreModificarForm(forms.ModelForm):
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_PADRE,
        initial='CC',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    def __init__(self, *args, **kwargs):
        super(PadreModificarForm, self).__init__(*args, **kwargs)
        self.fields['hijos'].queryset = Joven.objects.all()
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True

        self.fields['first_name'].label = "Nombres*"
        self.fields['last_name'].label = "Apellidos*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['is_active'].label = "¿El usuario tiene acceso a la plataforma?*"

        self.fields['is_active'].help_text = ""

        self.fields["hijos"].queryset = Joven.objects.filter(is_active=True)

    class Meta:
        model = Padre
        fields = ('first_name', 'last_name', 'tipo_identificacion', 'identificacion', 'email', 'pais', 'region', 'ciudad',
                  'telefono', 'hijos', 'is_active', 'indicativo_telefono')
        widgets = {
            'hijos': Select2MultipleWidget(),
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }

    def clean_email(self):
        pk_usuario = self.instance.pk
        usuario = Usuario.objects.get(id=pk_usuario)
        email = self.cleaned_data['email']
        email.lower()
        usuarios = Usuario.objects.filter(email=email)
        if usuarios.exists():
            if not usuario in usuarios:
                raise ValidationError('Este correo ya existe!')
        return email

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido


class ContrasenaModelForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = Usuario
        fields = ('password',)

    def __init__(self, *args, **kwargs):
        super(ContrasenaModelForm, self).__init__(*args, **kwargs)
        self.fields['password'].label = "Nueva contraseña*"
        self.fields['confirm_password'].label = "Confirmar nueva contraseña*"

        self.fields['password'].help_text = "- La contraseña no puede ser similar a los datos personales <br>- La contraseña tiene que tener mínimo 8 caracteres <br>- La contraseña no puede ser una contraseña usada comúnmente <br>- La contraseña debe de ser alfanumérica"
        self.fields['confirm_password'].help_text = "Introduzca la misma contraseña para verificar."

    def clean(self):
        cleaned_data = super(ContrasenaModelForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password != confirm_password:
            raise ValidationError('La contraseña y su confirmación no concuerdan', code='invalid')

# #################USER FORMS#####################
class JovenRegistroForm(UserCreationForm):
    fecha_de_nacimiento = forms.DateField(
        widget=DatePicker(
            options={
                'format': 'YYYY-MM-DD',
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': False,
            },
        ),
    )

    def __init__(self, *args, **kwargs):
        super(JovenRegistroForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.fields["fecha_de_nacimiento"].widget.attrs.update({
            'class': 'fecha readonly',
        })

        self.fields['first_name'].label = "Nombres*"
        self.fields['last_name'].label = "Apellidos*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['fecha_de_nacimiento'].label = "Fecha de nacimiento*"
        self.fields['indicativo_telefono'].initial = "57"
        self.fields['fecha_de_nacimiento'].label = "Fecha de nacimiento*"
        self.fields['terminos'].required = True
        self.fields['telefono'].required = True
        self.fields['password1'].label = "Contraseña*"
        self.fields['password1'].help_text = "- La contraseña no puede ser similar a los datos personales <br>- La contraseña tiene que tener mínimo 8 caracteres <br>- La contraseña no puede ser una contraseña usada comúnmente <br>- La contraseña debe de ser alfanumérica"

    class Meta:
        model = Joven
        fields = ('first_name', 'last_name', 'email', 'fecha_de_nacimiento', 'telefono', 'terminos', 'is_active', 'indicativo_telefono','password1', 'password2')

    def clean_email(self):
        email = self.cleaned_data['email']
        if Usuario.objects.filter(email=email).exists():
            raise ValidationError('Este correo ya existe!')
        return email.lower()

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido

class PadreRegistroForm(UserCreationForm):
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_PADRE,
        initial='CC',
        widget=Select2Widget(),
        required=True,
        label='tipo de identificación*'
    )

    def __init__(self, *args, **kwargs):
        super(PadreRegistroForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

        self.fields['first_name'].label = "Nombres*"
        self.fields['last_name'].label = "Apellidos*"
        self.fields['email'].label = "Correo electrónico*"
        self.fields['password1'].label = "Contraseña nueva*"
        self.fields['password2'].label = "Confirmar contraseña nueva*"
        self.fields['indicativo_telefono'].initial = "57"
        self.fields['password1'].help_text = "- La contraseña debe de ser alfanumérica y tener mínimo 8 caracteres <br>- " \
                                             "No puede ser una contraseña usada comúnmente o similar a los datos personales"
        self.fields['password2'].help_text = "Introduzca la misma contraseña"

    class Meta:
        model = Padre
        fields = ('first_name', 'last_name', 'tipo_identificacion', 'identificacion', 'email', 'pais', 'region',
                  'ciudad', 'password1', 'password2', 'terminos')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        if Usuario.objects.filter(email=email).exists():
            raise ValidationError('Este correo ya existe!')
        return email.lower()

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido


class SuperusuarioPerfilForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput(), required=False)
    y = forms.FloatField(widget=forms.HiddenInput(), required=False)
    width = forms.FloatField(widget=forms.HiddenInput(), required=False)
    height = forms.FloatField(widget=forms.HiddenInput(), required=False)
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_PADRE,
        initial='CC',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    def __init__(self, *args, **kwargs):
        super(SuperusuarioPerfilForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True

        self.fields['first_name'].label = "Nombres*"
        self.fields['email'].label = "Correo electrónico*"

    class Meta:
        model = Usuario
        fields = ('foto_perfil', 'first_name', 'tipo_identificacion', 'identificacion', 'email', 'pais', 'region', 'ciudad',
                  'telefono','indicativo_telefono', 'terminos', 'x', 'y', 'width', 'height')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }
    def clean_email(self):
        pk_usuario = self.instance.pk
        usuario = Usuario.objects.get(id=pk_usuario)
        email = self.cleaned_data['email']
        email.lower()
        usuarios = Usuario.objects.filter(email=email)
        if usuarios.exists():
            if not usuario in usuarios:
                raise ValidationError('Este correo ya existe!')
        return email

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido

    def save(self, commit=True):
        usuario = super(SuperusuarioPerfilForm, self).save(commit=False)
        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        if not x is None and not y is None:
            w = self.cleaned_data.get('width')
            h = self.cleaned_data.get('height')
            imagen = Image.open(usuario.foto_perfil)
            try:
                imagen_anterior = Usuario.objects.get(pk=self.instance.pk).foto_perfil
                otras = Usuario.objects.filter(foto_perfil=imagen_anterior.url)
                if os.path.isfile(imagen_anterior.path) and len(otras) <= 1:
                    os.remove(imagen_anterior.path)
                for orientation in ExifTags.TAGS.keys():
                    if ExifTags.TAGS[orientation] == 'Orientation':
                        break
                exif = dict(imagen._getexif().items())

                if exif[orientation] == 3:
                    imagen = imagen.rotate(180, expand=True)
                elif exif[orientation] == 6:
                    imagen = imagen.rotate(270, expand=True)
                elif exif[orientation] == 8:
                    imagen = imagen.rotate(90, expand=True)
            except ValueError:
                pass

            imagen_recortada = imagen.crop((x, y, w + x, h + y))
            redimensionada = imagen_recortada.resize((600, 600), Image.ANTIALIAS)
            path = os.path.join(os.path.dirname(BASE_DIR), "kairos", "media", "profile_pics", usuario.foto_perfil.name)
            usuario.save()
            if os.path.isfile(usuario.foto_perfil.path):
                path = usuario.foto_perfil.path
                os.remove(usuario.foto_perfil.path)
            redimensionada.save(path)
        return usuario


class JovenPerfilForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput(), required=False)
    y = forms.FloatField(widget=forms.HiddenInput(), required=False)
    width = forms.FloatField(widget=forms.HiddenInput(), required=False)
    height = forms.FloatField(widget=forms.HiddenInput(), required=False)
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_JOVEN,
        initial='TI',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    fecha_de_nacimiento = forms.DateField(
        widget=DatePicker(
            options={
                'locale': 'es',
                'format': 'YYYY-MM-DD',
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': False,
            },
        ),
    )

    def __init__(self, *args, **kwargs):
        super(JovenPerfilForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields["fecha_de_nacimiento"].widget.attrs.update({
            'class': 'fecha readonly',
        })

        self.fields['first_name'].label = "Nombres*"
        self.fields['last_name'].label = "Apellidos*"
        self.fields['email'].label = "Correo electrónico*"

    class Meta:
        model = Joven
        fields = ('foto_perfil', 'first_name', 'last_name', 'tipo_identificacion', 'identificacion', 'fecha_de_nacimiento',
                  'email', 'pais', 'region', 'ciudad', 'telefono', 'indicativo_telefono', 'terminos', 'notificaciones', 'notificaciones_whatsappp', 'x', 'y', 'width', 'height')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }

    def clean_email(self):
        pk_usuario = self.instance.pk
        usuario = Usuario.objects.get(id=pk_usuario)
        email = self.cleaned_data['email']
        email.lower()
        usuarios = Usuario.objects.filter(email=email)
        if usuarios.exists():
            if not usuario in usuarios:
                raise ValidationError('Este correo ya existe!')
        return email

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido

    def save(self, commit=True):
        usuario = super(JovenPerfilForm, self).save(commit=False)

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        if not x is None and not y is None:
            w = self.cleaned_data.get('width')
            h = self.cleaned_data.get('height')
            imagen = Image.open(usuario.foto_perfil)
            try:
                imagen_anterior = Usuario.objects.get(pk=self.instance.pk).foto_perfil
                otras = Usuario.objects.filter(foto_perfil=imagen_anterior.url)
                if os.path.isfile(imagen_anterior.path) and len(otras) <= 1:
                    os.remove(imagen_anterior.path)
                for orientation in ExifTags.TAGS.keys():
                    if ExifTags.TAGS[orientation] == 'Orientation':
                        break
                exif = dict(imagen._getexif().items())

                if exif[orientation] == 3:
                    imagen = imagen.rotate(180, expand=True)
                elif exif[orientation] == 6:
                    imagen = imagen.rotate(270, expand=True)
                elif exif[orientation] == 8:
                    imagen = imagen.rotate(90, expand=True)
            except ValueError:
                pass

            imagen_recortada = imagen.crop((x, y, w + x, h + y))
            redimensionada = imagen_recortada.resize((600, 600), Image.ANTIALIAS)
            path = os.path.join(os.path.dirname(BASE_DIR), "kairos", "media", "profile_pics", usuario.foto_perfil.name)
            usuario.save()
            if os.path.isfile(usuario.foto_perfil.path):
                path = usuario.foto_perfil.path
                os.remove(usuario.foto_perfil.path)
            redimensionada.save(path)

        return usuario


class PadrePerfilForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput(), required=False)
    y = forms.FloatField(widget=forms.HiddenInput(), required=False)
    width = forms.FloatField(widget=forms.HiddenInput(), required=False)
    height = forms.FloatField(widget=forms.HiddenInput(), required=False)
    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_PADRE,
        initial='CC',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    def __init__(self, *args, **kwargs):
        super(PadrePerfilForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True

        self.fields['first_name'].label = "Nombres*"
        self.fields['last_name'].label = "Apellidos*"
        self.fields['email'].label = "Correo electrónico*"

    class Meta:
        model = Padre
        fields = ('foto_perfil', 'first_name', 'last_name', 'tipo_identificacion', 'identificacion', 'email', 'pais',
                  'region', 'ciudad', 'telefono', 'terminos', 'notificaciones', 'x', 'y', 'width', 'height')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            )
        }

    def clean_email(self):
        pk_usuario = self.instance.pk
        usuario = Usuario.objects.get(id=pk_usuario)
        email = self.cleaned_data['email']
        email.lower()
        usuarios = Usuario.objects.filter(email=email)
        if usuarios.exists():
            if not usuario in usuarios:
                raise ValidationError('Este correo ya existe!')
        return email

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido

    def save(self, commit=True):
        usuario = super(PadrePerfilForm, self).save(commit=False)

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        if not x is None and not y is None:
            w = self.cleaned_data.get('width')
            h = self.cleaned_data.get('height')
            imagen = Image.open(usuario.foto_perfil)
            try:
                imagen_anterior = Usuario.objects.get(pk=self.instance.pk).foto_perfil
                otras = Usuario.objects.filter(foto_perfil=imagen_anterior.url)
                if os.path.isfile(imagen_anterior.path) and len(otras) <= 1:
                    os.remove(imagen_anterior.path)
                for orientation in ExifTags.TAGS.keys():
                    if ExifTags.TAGS[orientation] == 'Orientation':
                        break
                exif = dict(imagen._getexif().items())

                if exif[orientation] == 3:
                    imagen = imagen.rotate(180, expand=True)
                elif exif[orientation] == 6:
                    imagen = imagen.rotate(270, expand=True)
                elif exif[orientation] == 8:
                    imagen = imagen.rotate(90, expand=True)
            except ValueError:
                pass

            imagen_recortada = imagen.crop((x, y, w + x, h + y))
            redimensionada = imagen_recortada.resize((600, 600), Image.ANTIALIAS)
            path = os.path.join(os.path.dirname(BASE_DIR), "kairos", "media", "profile_pics", usuario.foto_perfil.name)
            usuario.save()
            if os.path.isfile(usuario.foto_perfil.path):
                path = usuario.foto_perfil.path
                os.remove(usuario.foto_perfil.path)
            redimensionada.save(path)

        return usuario


# FORMULARIOS PARA LAS PRUEBAS
class JovenDatosBasicosForm(forms.ModelForm):
    fecha_de_nacimiento = forms.DateField(
        widget=DatePicker(
            options={
                'locale': 'es',
                'format': 'YYYY-MM-DD',
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': False,
            },
        ),
    )

    tipo_identificacion = forms.ChoiceField(
        choices=TIPOS_ID_JOVEN,
        initial='TI',
        widget=Select2Widget(),
        required=True,
        label="Tipo de identificación*"
    )

    FACTORES_BIENESTAR = (
        ("Familia", "Familia"),
        ("Educación", "Educación"),
        ("Vida sentimental", "Vida sentimental"),
        ("Salud", "Salud"),
        ("Trabajo", "Trabajo"),
        ("Economía del hogar", "Economía del hogar"),
        ("Amigos", "Amigos"),
        ("Lugar en el que vive", "Lugar en el que vive")
    )
    factores_sensaciones_bienestar = forms.MultipleChoiceField(
        choices=FACTORES_BIENESTAR,
        widget=Select2MultipleWidget(attrs={'class': 'span6', 'data-maximum-selection-length': 3}),
        required=True,
        label="¿Cuáles son los 3 factores que más afectan tu sensación de tener bienestar?*"
    )
    JORNADAS = (
        ("Mañana", "Mañana"),
        ("Tarde", "Tarde"),
        ("Noche", "Noche"),
        ("Sábados", "Sábados")
    )
    jornadas_del_joven = forms.MultipleChoiceField(
        choices=JORNADAS,
        widget=Select2MultipleWidget(),
        required=False,
        label="Si está estudiando actualmente, ¿a qué jornada asiste?*"
    )

    def __init__(self, *args, **kwargs):
        super(JovenDatosBasicosForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields["fecha_de_nacimiento"].widget.attrs.update({
            'class': 'fecha readonly',
        })
        self.fields['first_name'].label = "¿Cuál es tu nombre?*"
        self.fields['last_name'].label = "¿Cuáles son tus apellidos?*"
        instance = kwargs.get('instance', None)
        if instance == None:
            self.fields['formacion_academica'].initial = "P"

        self.fields['facebook'].label = "Perfil de Facebook"
        self.fields['instagram'].label = "Perfil de Instagram"
        self.fields['twitter'].label = "Perfil de Twitter"

        self.fields['facebook'].widget.attrs['required'] = False
        self.fields['instagram'].widget.attrs['required'] = False
        self.fields['twitter'].widget.attrs['required'] = False
        self.fields['facebook'].required = False
        self.fields['instagram'].required = False
        self.fields['twitter'].required = False

        self.fields['twitter'].widget.attrs['placeholder'] = "@nombreusuario"
        self.fields['facebook'].widget.attrs['placeholder'] = "nombredelusuario (no incluir https://www.facebook.com)"
        self.fields['instagram'].widget.attrs['placeholder'] = "nombreusuario (no usar @)"
    class Meta:
        model = Joven
        fields = ('first_name', 'last_name', 'tipo_identificacion', 'identificacion', 'genero', 'pais', 'region', 'ciudad',  'vive_con_familia', 'personas_en_hogar', 'ingresos_grupo_familiar', 'capacitaciones', 'formacion_academica',
        'ultimo_grado', 'jornadas_del_joven', 'forma_vivir', 'factores_sensaciones_bienestar', 'fecha_de_nacimiento', 'estado_salud', 'telefono', 'actividad_que_ocupo_tiempo', 'hijos', 'facebook', 'instagram', 'twitter','indicativo_telefono')
        widgets = {
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            ),
            'genero': Select2Widget(),
            'ingresos_grupo_familiar': Select2Widget(),
            'formacion_academica': Select2Widget(),
            'forma_vivir': Select2Widget(),
            'jornadas_del_joven': Select2MultipleWidget(),
            'factores_sensaciones_bienestar': Select2MultipleWidget(
                attrs={'class': 'span6', 'data-minimum-input-length': 0, 'data-maximun-input-length': 3}
            ),
            'estado_salud': Select2Widget(),
            'actividad_que_ocupo_tiempo': Select2Widget(),
            'vive_con_familia': Select2Widget(),
            'capacitaciones': Select2Widget(),
            'hijos': Select2Widget()
        }

    def clean_first_name(self):
        nombre = self.cleaned_data['first_name']
        nombre = nombre.title()
        return nombre

    def clean_last_name(self):
        apellido = self.cleaned_data['last_name']
        apellido = apellido.title()
        return apellido


class CorreoModelForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = Usuario
        fields = ('email',)

    def __init__(self, *args, **kwargs):
        super(CorreoModelForm, self).__init__(*args, **kwargs)
        self.fields['email'].label = "Correo electrónico*"

    def clean_email(self):
        email = self.cleaned_data['email']
        if not Usuario.objects.filter(email__iexact=email).exists():
            raise ValidationError('El correo electrónico no se encuentra registrado en el sistema!')


class FormularioInvitacionJoven(forms.Form):
    cargue_masivo = forms.FileField(label='Cargue masivo', required=False)

    invitar_por_correo = forms.CharField(
        label="Invitar por correo"
    )

    def __init__(self, *args, **kwargs):
        super(FormularioInvitacionJoven, self).__init__(*args, **kwargs)
        self.fields['invitar_por_correo'].required = False
        self.fields['cargue_masivo'].required = False


class FiltroOrganizacionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        organizaciones = None
        if 'organizaciones' in kwargs:
            organizaciones = kwargs.pop('organizaciones')

        super(FiltroOrganizacionForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = False

        if organizaciones:
            self.fields['first_name'].queryset = Organizacion.objets.filter(pk__in=organizaciones)
        else:
            self.fields['first_name'].queryset = Organizacion.objects.all()

    class Meta:
        model = Organizacion
        fields = ('first_name',)
        widgets = {
            'first_name': ModelSelect2MultipleWidget(
                model=Organizacion,
                queryset=Organizacion.objects.all(),
                search_fields=['email__icontains', 'first_name'],
                max_results=100,
                attrs={'data-placeholder': 'Seleccione las Organizaciones'}
                ),

        }
