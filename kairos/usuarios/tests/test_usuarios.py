from kairos.organizaciones.models import Organizacion, TipoOrganizacion
from kairos.usuarios.models import Usuario, Joven

from django.test import TestCase

class TestUsuarioModel(TestCase):

    def setUp(self):
        '''
        Instanciar objetos de prueba
        '''

        self.usuario1 = Usuario.objects.create(
            tipo="Joven",
            correo_verificado=True,
            es_superusuario=False,
            terminos=True,
            notificaciones=False,
            notificaciones_whatsappp=True,
            tipo_identificacion='CC',
            identificacion='1144177658',
            indicativo_telefono='1'
        )

    def test_usuario_crear(self):
        '''
        Probando la creación de un usuario de tipo Joven
        '''

        assert (self.usuario1.tipo) == "Joven"

    def test_usuario_existe_usuario(self):
        '''
        Probando si un usuario existe
        '''
        assert (self.usuario1.existe_usuario(self.usuario1.id)) == True
        

class JovenesTestCase(TestCase):
    def setUp(self):
        '''
        Instanciar objetos de prueba
        '''

        self.joven1 = Joven.objects.create(
            tipo="Joven",
            email="joven1@gmail.com",
            correo_verificado=True,
            es_superusuario=False,
        )
        self.joven2 = Joven.objects.create(
            tipo="Joven",
            email="joven2@gmail.com",
            correo_verificado=True,
            es_superusuario=False,
        )

    def test_joven_pertenece_colegio(self):
        """Permite probar el metodo de joven pertenece a colegio"""
        tipo_organizacion = TipoOrganizacion.objects.create(
            id=4, nombre="colegio", descripcion="una descripcion", activo=True
        )
        organizacion = Organizacion.objects.create(
            tipo_organizacion=tipo_organizacion, email="organizacion@gmail.com"
        )

        organizacion.jovenes.add(self.joven1)

        assert (Joven.joven_pertenece_colegio(self.joven1)) == True

    def test_joven_pertenece_colegio_fallido(self):
        """Permite probar el metodo de joven que no pertenece a colegio"""
        tipo_organizacion = TipoOrganizacion.objects.create(
            id=4, nombre="colegio", descripcion="una descripcion", activo=True
        )
        organizacion = Organizacion.objects.create(
            tipo_organizacion=tipo_organizacion, email="organizacion@gmail.com"
        )

        organizacion.jovenes.add(self.joven1)

        assert (Joven.joven_pertenece_colegio(self.joven2)) == False
        