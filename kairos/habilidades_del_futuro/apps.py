from django.apps import AppConfig


class HabilidadesDelFuturoConfig(AppConfig):
    name = 'kairos.habilidades_del_futuro'
