# Modulos Django
from django import forms
from django.db.models import Q
from django.forms import ModelMultipleChoiceField

# Modulos de plugin externos
from django_select2.forms import ModelSelect2MultipleWidget

# Modulos de otras apps
from kairos.formularios.models import Competencias
# Modulos internos de la app
from .models import Habilidad


class HabilidadForm(forms.ModelForm):
    lista_codigo_calificación = [
        'Números',
        'Lectura',
        'Negociación',
        'Autoconciencia',
        'Empatía',
        'Autocontrol',
        'Autoconfianza',
        'Dirección',
        'Carisma',
        'Innovación',
        'Desarrollo',
        'Motivación',
        'Inspiración',
        'Socializar',
        'Liderazgo_laizzes_faire',
        'Liderazgo_transaccional',
        'Liderazgo_transformacional'
    ]

    competencias_asociadas = ModelMultipleChoiceField(
        queryset=Competencias.objects.filter(codigo_calificacion__in=lista_codigo_calificación),
        widget=ModelSelect2MultipleWidget(
            queryset=Competencias.objects.filter(codigo_calificacion__in=lista_codigo_calificación),
            search_fields=['nombre__icontains'],
        )
    )

    class Meta:
        model = Habilidad
        fields = (
            'nombre',
            'descripcion',
            'imagen',
            'orden',
            'link_video',
            'descripcion_video',
            'como_se_mide',
            'competencias_asociadas',
            'orden',
            'activo'
        )

        widgets = {
            'descripcion': forms.Textarea(attrs={
                'rows': 3,
                'cols': 22,
                'style': 'resize:none;'
            }),
            'descripcion_video': forms.Textarea(attrs={
                'rows': 3,
                'cols': 22,
                'style': 'resize:none;'
            }),
            'como_se_mide': forms.Textarea(attrs={
                'rows': 3,
                'cols': 22,
                'style': 'resize:none;'
            }),
        }
