# Modulos Django
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import redirect
from django.views.generic import ListView, CreateView, UpdateView, TemplateView

# Modulos de plugin externos

# Modulos de otras apps
from rest_framework.views import APIView
from rest_framework.response import Response
from kairos.usuarios.models import Joven
from kairos.formularios.models import SeccionVisitada

# Modulos internos de la app
from .models import *
from .forms import HabilidadForm
from kairos.formularios.models import Competencias


class HabilidadRegistrar(LoginRequiredMixin, CreateView):
    model = Habilidad
    form_class = HabilidadForm
    template_name = "habilidades_del_futuro/registrar.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated and not request.user.tipo == "Superusuario":
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect('inicio')

        return super(HabilidadRegistrar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Habilidad registrada correctamente")
        return redirect('habilidades_del_futuro:registrar')


class ListadoHabilidad(LoginRequiredMixin, ListView):
    model = Habilidad
    context_object_name = "habilidades"
    template_name = "habilidades_del_futuro/listado.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated and not request.user.tipo == "Superusuario":
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect('inicio')

        return super(ListadoHabilidad, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        habilidades = Habilidad.objects.all()
        return habilidades


class ModificarHabilidad(LoginRequiredMixin, UpdateView):
    model = Habilidad
    pk_url_kwarg = 'id_habilidad'
    form_class = HabilidadForm
    template_name = "habilidades_del_futuro/modificar.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated and not request.user.tipo == "Superusuario":
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect('inicio')

        return super(ModificarHabilidad, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Habilidad modificada satisfactoriamente")
        return redirect("habilidades_del_futuro:listado")


class HabilidadesJovenConsultar(LoginRequiredMixin, TemplateView):
    template_name = "habilidades_del_futuro/consultar_habilidades.html"

    def dispatch(self, request, *args, **kwargs):
        joven = Joven.objects.get(pk=self.request.user.pk)
        if joven.tanda_formularios == None:
            messages.error(request, "No tienes asignados los cuestionarios!")
            return redirect("inicio")
        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True)
        formularios_disponibles = formularios_joven.filter(
            ~Q(respuestasjoven__joven=joven.id) | Q(respuestasjoven__joven=joven.id, respuestasjoven__completas=False)
        )
        if formularios_disponibles.count() != 0:
            messages.error(request, "No has completado todos tus cuestionarios!")
            return redirect("inicio")
        return super(HabilidadesJovenConsultar, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        from kairos.guia.models import Recurso
        data = super(HabilidadesJovenConsultar, self).get_context_data(**kwargs)
        id_joven = self.request.user.id
        habilidades = Habilidad.obtener_activos()
        data['habilidades'] = habilidades
        SeccionVisitada.calificar_seccion_joven(id_joven, "HABILIDADES SOCIALES")
        return data


class HabilidadDetalle(LoginRequiredMixin, TemplateView):
    template_name = "habilidades_del_futuro/detalle.html"

    def dispatch(self, request, *args, **kwargs):
        joven = Joven.objects.get(pk=self.request.user.pk)
        if joven.tanda_formularios == None:
            messages.error(request, "No tienes asignados los cuestionarios!")
            return redirect("inicio")
        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True)
        formularios_disponibles = formularios_joven.filter(
            ~Q(respuestasjoven__joven=joven.id) | Q(respuestasjoven__joven=joven.id, respuestasjoven__completas=False)
        )
        if formularios_disponibles.count() != 0:
            messages.error(request, "No has completado todos tus cuestionarios!")
            return redirect("inicio")
        return super(HabilidadDetalle, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        from kairos.guia.models import Recurso
        data = super(HabilidadDetalle, self).get_context_data(**kwargs)
        id_joven = self.request.user.id
        habilidad = Habilidad.buscar(id_habilidad=self.kwargs['id_habilidad'])
        data['habilidad'] = habilidad
        habilidad.calificar_habilidad_joven(id_joven)
        return data


class ApiResultadosCompetencia(APIView):
    def get(self, request):
        joven = Joven.objects.get(id=request.GET.get('id_joven'))
        competencia = Competencias.buscar(id_competencia=request.GET.get('id_competencia'))
        genero = joven.genero
        data = {}
        if competencia.codigo_calificacion == 'Números':
            data['edad_del_joven'] = joven.obtener_edad_joven()
            data['genero_del_joven'] = genero
            data['puntaje_razonamiento_cuantitativo'] = joven.obtener_puntaje_razonamiento_cuantitativo()
        elif competencia.codigo_calificacion == 'Lectura':
            data['puntaje_analisis_en_lectura'] = joven.obtener_puntaje_analisis_en_lectura()

        elif competencia.codigo_calificacion == 'Negociación':
            data['promedio_liderazgo_claridad_en_negociar'] = joven.obtener_promedio_liderazgo_claridad_en_negociar()

        elif competencia.codigo_calificacion == 'Autoconciencia':
            data['puntaje_de_auto_conciencia'] = joven.obtener_puntaje_de_auto_conciencia()

        elif competencia.codigo_calificacion == 'Empatía':
            data['puntaje_de_empatia'] = joven.obtener_puntaje_de_empatia()

        elif competencia.codigo_calificacion == 'Autocontrol':
            data['puntaje_de_auto_control'] = joven.obtener_puntaje_de_auto_control()

        elif competencia.codigo_calificacion == 'Autoconfianza':
            data['puntaje_de_auto_confianza'] = joven.obtener_puntaje_de_auto_confianza()

        elif competencia.codigo_calificacion == 'Dirección':
            data['promedio_liderazgo_direccion_rutinaria'] = joven.obtener_promedio_liderazgo_direccion_rutinaria()

        elif competencia.codigo_calificacion == 'Carisma':
            data['promedio_liderazgo_carisma'] = joven.obtener_promedio_liderazgo_carisma()

        elif competencia.codigo_calificacion == 'Innovación':
            data['promedio_liderazgo_innovacion'] = joven.obtener_promedio_liderazgo_innovacion()

        elif competencia.codigo_calificacion == 'Desarrollo':
            data[
                'promedio_liderazgo_deseo_que_otros_crezcan'] = joven.obtener_promedio_liderazgo_deseo_que_otros_crezcan()

        elif competencia.codigo_calificacion == 'Motivación':
            data['puntaje_de_motivacion'] = joven.obtener_puntaje_de_motivacion()

        elif competencia.codigo_calificacion == 'Inspiración':
            data['promedio_liderazgo_inspiracion'] = joven.obtener_promedio_liderazgo_inspiracion()

        elif competencia.codigo_calificacion == 'Socializar':
            data['puntaje_de_capacidad_para_socializar'] = joven.obtener_puntaje_de_capacidad_para_socializar()
        elif competencia.codigo_calificacion == 'Liderazgo_transformacional':
            data['puntaje_liderazgo'] = joven.obtener_promedio_liderazgo()["transformacional"]
        elif competencia.codigo_calificacion == 'Liderazgo_transaccional':
            data['puntaje_liderazgo'] = joven.obtener_promedio_liderazgo()["transaccional"]
        elif competencia.codigo_calificacion == 'Liderazgo_laizzes_faire':
            data['puntaje_liderazgo'] = joven.obtener_promedio_liderazgo()["laizzes_faire"]

        return Response(data)
