# Modulos Django

from django.urls import path
# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "habilidades_del_futuro"

urlpatterns = [
    path('listado', ListadoHabilidad.as_view(), name='listado'),
    path('modificar/<int:id_habilidad>', ModificarHabilidad.as_view(), name='modificar'),
    path('registrar', HabilidadRegistrar.as_view(), name='registrar'),
    path('mis-habilidades', view=HabilidadesJovenConsultar.as_view(), name='mis_habilidades'),
    path('detalle/<int:id_habilidad>', view=HabilidadDetalle.as_view(), name='detalle'),
    path("api/habilidades/competencia", view=ApiResultadosCompetencia.as_view(), name="api_habilidades_competencia"),
]
