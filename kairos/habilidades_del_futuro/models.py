# Modulos Django
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Modulos de plugin externos
from simple_history.models import HistoricalRecords
import os

# Modulos de otras apps

# Modulos internos de la app


def subir_img_insignia(instance, filename):
    return "habilidades_futuro/%s" % (filename.encode('ascii', 'ignore'))


class Habilidad(models.Model):
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)
    nombre = models.CharField(max_length=50, verbose_name="nombre de la habilidad*")
    descripcion = models.TextField(verbose_name="descripción de la habilidad*")
    imagen = models.FileField(upload_to=subir_img_insignia, verbose_name="imagen de la habilidad*")
    link_video = models.CharField(max_length=100, verbose_name="enlace a video explicativo (url)*", blank=True)
    descripcion_video = models.TextField(verbose_name="descripción del video*", blank=True)
    como_se_mide = models.TextField(verbose_name="Como se mide*", blank=True)
    competencias_asociadas = models.ManyToManyField('formularios.Competencias', related_name="habilidades_del_futuro_competencias",
                                                    verbose_name="competencias asociadas", blank=True
                                                    )
    activo = models.BooleanField(verbose_name="¿La habilidad se encuentra activa?", default=True)
    orden = models.IntegerField(verbose_name="Orden con respecto a las otras habilidades", blank=True,
                                default=0)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    def crear_habilidad_visitada_joven(self, id_usuario: int):
        '''
            Función que crea la competencia visitada para el joven
        '''
        HabilidadVisitada.objects.get_or_create(
            usuario_id=id_usuario,
            habilidad_id=self.pk
        )

    def calificar_habilidad_joven(self, id_usuario: str):
        '''
            Función que califica la competencia como visitada
        '''
        habilidad_visitada = HabilidadVisitada.objects.get(
            usuario_id=id_usuario,
            habilidad_id=self.pk
        )
        if habilidad_visitada.visitado == 0:
            habilidad_visitada.visitado = 1
            habilidad_visitada.save()

    @staticmethod
    def obtener_activos() -> 'Queryset<Habilidad>':
        '''
            Función que retorna todas las habilidades del futuro activas
            Queryset<Habilidad>: Queryset<Habilidad>
        '''
        habilidades = Habilidad.objects.filter(activo=True)
        return habilidades
    @staticmethod
    def buscar(id_habilidad:str) -> 'Habilidad':
        '''
            Funciión que busca la habilidad por id_habilidad
            y retorna la habilidad en caso de encontrarla.
        '''
        habilidad = Habilidad.objects.get(pk=id_habilidad)
        return habilidad

    class Meta:
        ordering = ['orden']


class HabilidadVisitada(models.Model):
    habilidad = models.ForeignKey('Habilidad', on_delete=models.CASCADE, verbose_name='habilidad calificado',
                                  related_name='habilidad_visitada_del_usuario')
    usuario = models.ForeignKey('usuarios.Usuario', on_delete=models.CASCADE, verbose_name='usuario que califica',
                                related_name='usuario_visitador_habilidad')
    visitado = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                           verbose_name='calificación: 1 visitado, 0 no visitado')
    history = HistoricalRecords()

    @staticmethod
    def obtener_todos(id_joven: str) -> 'Queryset<HabilidadVisitada>':
        '''
            Función que retorna todas las habilidades del joven
            Queryset<HabilidadVisitada>: Queryset<HabilidadVisitada>
        '''
        habilidades_visitadas = HabilidadVisitada.objects.filter(usuario_id=id_joven)
        return habilidades_visitadas

    @staticmethod
    def obtener_visitados(id_joven: str) -> 'Queryset<HabilidadVisitada>':
        '''
            Función que retorna todas las habilidades visitadas
            por el joven
            Queryset<HabilidadVisitada>: Queryset<HabilidadVisitada>
        '''
        habilidades_visitadas = HabilidadVisitada.obtener_todos(id_joven).filter(visitado=1)
        return habilidades_visitadas
