# Modulos Django

from django.urls import path
# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "formularios"

urlpatterns = [
    path('gestionar/', view=FormulariosListado.as_view(), name='listado'),
    path('consultar/', view=FormulariosConsultar.as_view(), name='consultar'),
    path('registrar', view=FormulariosRegistrar.as_view(), name='registrar'),
    path('modificar/<int:id_formulario>', view=FormulariosModificar.as_view(), name='modificar'),

    path('preguntas/gestionar/<int:id_formulario>', view=PreguntasListado.as_view(), name='listado_preguntas'),
    path('preguntas/consultar/<int:id_formulario>', view=PreguntasConsultar.as_view(), name='consultar_preguntas'),

    path('preguntas/registro/<int:id_formulario>', view=preguntaRegistrar, name='registrar_preguntas'),
    path('preguntas/registrar/<int:id_formulario>', view=PreguntaRegistrar.as_view(), name='registrar_pregunta'),
    path('preguntas/domino/registrar/<int:id_formulario>', view=PreguntasDominoRegistrar.as_view(), name='registrar_pregunta_domino'),
    path('preguntas/mixta/registrar/<int:id_formulario>', view=PreguntasMixtaRegistrar.as_view(), name='registrar_pregunta_mixta'),

    path('preguntas/modificar/<int:id_pregunta>', view=PreguntaModificar.as_view(), name='modificar_pregunta'),
    path('preguntas/domino/modificar/<int:id_pregunta>', view=PreguntaDominoModificar.as_view(), name='modificar_pregunta_domino'),

    path('ambitos/gestionar/', view=AmbitosListado.as_view(), name='listado_ambitos'),
    path('ambitos/consultar/', view=AmbitosConsultar.as_view(), name='consultar_ambitos'),
    path('ambitos/registrar', view=AmbitoRegistrar.as_view(), name='registrar_ambito'),
    path('ambitos/modificar/<int:id_ambito>', view=AmbitoModificar.as_view(), name='modificar_ambito'),

    path('competencias/gestionar/', view=CompetenciasListado.as_view(), name='listado_competencias'),
    path('competencias/consultar/', view=CompetenciasConsultar.as_view(), name='consultar_competencias'),
    path('competencias/registrar', view=CompetenciasRegistrar.as_view(), name='registrar_competencias'),
    path('competencias/modificar/<int:id_competencia>', view=CompetenciasModificar.as_view(), name='modificar_competencias'),

    path('responder/', view=responderFormulario, name='responder'),
    path('responder/<int:pk>/', view=responderFormularioSeleccionado, name='responder_formulario'),
    path('responder/datos-basicos', view=DatosBasicosFormulario.as_view(), name='responder_datos_basicos'),
    path('responder/experiencia-actual', view=ExperienciasActualesFormulario.as_view(), name='responder_experiencia_actual'),
    #path('responder/perfil-deseado', view=JovenPerfilDeseadoFormulario.as_view(), name='responder_perfil_deseado'),  #URL de perfil deseado
    path("api/formulario/guardar-respuestas", view=ApiGuardarRespuestas.as_view(), name="api_guardar_respuestas"),
    path("api/formulario/guardar-estado-lectura", view=ApiGuardarEstadoLectura.as_view(), name="api_guardar_estado_lectura"),
    path("api/formulario/data-preguntas", view=ApiCargarDataPreguntas.as_view(), name="api_cargar_data_preguntas"),

    path('competencias/procesamiento-de-informacion/', view=ProcesamientoDeInformacionJovenConsultar.as_view(), name='procesamiento_de_informacion'),
    path('competencias/habilidades-sociales/', view=HabilidadesSocialesJovenConsultar.as_view(), name='habilidades_sociales'),
    path('competencias/aptitudes-e-intereses/', view=AptitudesEInteresesJovenConsultar.as_view(), name='aptitudes_e_intereses'),
    path('competencias/mis-experiencias/', view=MisExperienciasJovenConsultar.as_view(), name='mi_experiencia'),
    path('competencias/detalle/<int:id_competencia>', view=CompetenciasDetalleJovenConsultar.as_view(), name='detalle_competencia'),
    path('competencias/detalle-mis-experiencias/<int:id_competencia>', view=CompetenciasMiExperienciaDetalleJovenConsultar.as_view(), name='detalle_mis_experiencias'),
    path('competencias/detalle-aptitudes-e-intereses/<int:id_competencia>/<int:resultado>', view=InteresesAptitudesDetalleJovenConsultar.as_view(), name='detalle_aptitudes_e_intereses'),

]