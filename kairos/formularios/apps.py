from django.apps import AppConfig


class FormulariosConfig(AppConfig):
    name = 'kairos.formularios'
