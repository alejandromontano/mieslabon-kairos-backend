# Modulos Django
from django.utils import timezone

# Modulos de plugin externos
from datetime import datetime, timedelta
import pytz

# Modulos de otras apps

# Modulos internos de la app
def hay_tiempo_disponible(tiempo_limite, fecha_inicio):
    '''
        Se encargar de comparar la fecha_inicio + tiempo_limite con la fecha actual para saber si hay tiempo disponible

        Parametros:           
            tiempo_limite: El timepo que se adiciona a la hora actual
            fecha_inicio: Fecha que se le sumara el tiempo_limite

        Retorno:
            Si hora_actual > hora_final: False
            No hora_actual <= hora_final: True

    '''
    hora_actual = timezone.now()

    # utc = fecha_inicio.replace(tzinfo=pytz.UTC)
    # localtz = utc.astimezone(timezone.get_current_timezone())
    # hora_inicio = datetime.strptime(localtz.strftime("%Y-%m-%d %H:%M:%S.%f"), "%Y-%m-%d %H:%M:%S.%f")

    hora_final = (fecha_inicio + timedelta(seconds=60 * tiempo_limite))

    if hora_actual > hora_final:
        return False
    else:
        return True

def convertir_numero_interval(segundos):
    segundos = segundos % (24 * 3600)
    hour = segundos // 3600
    segundos %= 3600
    minutes = segundos // 60
    segundos %= 60

    return "%d:%02d:%02d" % (hour, minutes, segundos)