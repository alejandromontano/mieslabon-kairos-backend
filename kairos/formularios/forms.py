# Modulos Django
from django import forms
from django.forms import inlineformset_factory

# Modulos de plugin externos
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML
from django_select2.forms import Select2Widget, Select2MultipleWidget

# Modulos de otras apps

# Modulos internos de la app
from .models import *
from .pregunta_respuesta import *

class FormulariosModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FormulariosModelForm, self).__init__(*args, **kwargs)
        self.fields["ambito"].queryset = Ambito.objects.filter(activo=True)
        self.fields['escala_max'].help_text = "Valoración máxima que se puede otorgar en la escala."
        self.fields['tiempo_lectura'].help_text = 'Tiempo disponible para leer el formulario.'
        self.fields['tiempo_limite'].help_text = 'Después de terminado este tiempo el cuestionario terminará.'

    class Meta:
        model = Formularios
        fields = ['ambito', 'tipo_preguntas', 'nombre', 'identificador', 'instrucciones', 'escala_max', 'descripcion',
                  'tiempo_lectura', 'tiempo_limite', 'imagen', 'credito', 'releer', 'activo']

        widgets = {
            'instrucciones': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
            'ambito': Select2Widget(),
            'tipo_preguntas': Select2Widget(),
        }

        help_texts = {
            'identificador': '1: D48, 2: Emily Sterrett, 3: CHASIDE, 4: Liderazgo, 5: Selección Múltiple'
        }


class FormulariosModificarForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FormulariosModificarForm, self).__init__(*args, **kwargs)
        self.fields["ambito"].queryset = Ambito.objects.filter(activo=True)
        self.fields['escala_max'].help_text = "Valoración máxima que se puede otorgar en la escala."
        self.fields['tiempo_lectura'].help_text = 'Tiempo disponible para leer el formulario.'
        self.fields['tiempo_limite'].help_text = 'Después de terminado este tiempo el cuestionario terminará.'

    class Meta:
        model = Formularios
        fields = ['ambito', 'nombre', 'identificador', 'instrucciones', 'escala_max', 'descripcion', 'tiempo_lectura', 'tiempo_limite',
                  'imagen', 'credito', 'releer', 'activo']

        widgets = {
            'ambito': Select2Widget(),
            'instrucciones': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            )
        }

        help_texts = {
            'identificador': '1: D48, 2: Emily Sterrett, 3: CHASIDE, 4: Liderazgo, 5: Selección Múltiple'
        }


class PreguntasModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        id_ambito = kwargs.pop('pk_ambito', None)
        super(PreguntasModelForm, self).__init__(*args, **kwargs)
        ambito = Ambito.objects.get(id=id_ambito)
        self.fields["competencia"].queryset = ambito.competencias.all()
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(Field('descripcion', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field('competencia', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field(HTML(
                '<label>Imagen</label>'
                '<div style="height: calc(1.2em + 14px + 2px); overflow:hidden;" class="custom-file">'
                '<div><input type="file" name="imagen" class="custom-file-input" id="id_imagen">'
                '<label style="height: calc(1.2em + 14px + 2px);" class="custom-file-label" '
                'for="inputGroupFile01">{{ form.imagen.value|default:"Seleccione una imagen..." }}</label></div></div>'
                '{% if form.imagen.value %}<a href="/media/{{ form.imagen.value }}" data-lightbox="gallery-group">Ver imagen actual</a>{% endif %}'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2 align='center'"),
            Div(Field(HTML(
                '<div class="form-group"><div class="form-check checkbox checkbox-css pt-0 checkbox-inline">'
                '<div class="form-check"><input type="checkbox" name="activo" class="form-check-input" id="id_activo" checked="">'
                '<label class="form-check-label" for="id_activo">¿La pregunta se encuentra activa?</label></div></div></div>'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 align='left'"),
            Div(Fieldset('Opciones de Respuesta',
                         Formset('respuestas_pregunta'), css_class="col-sm-12"), css_class="col-sm-12 mt-4 formset-preguntas"),
        )

    class Meta:
        model = Preguntas
        fields = ('descripcion', 'competencia', 'imagen', 'activo')

        widgets = {
            'competencia': Select2Widget(),
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
        }


class PreguntasDominoModelForm(forms.ModelForm):
    RESPUESTAS = (
        ("0", "Vacío"),
        ("1", "Uno"),
        ("2", "Dos"),
        ("3", "Tres"),
        ("4", "Cuatro"),
        ("5", "Cinco"),
        ("6", "Seis"),
    )
    respuesta_superior = forms.ChoiceField(choices=RESPUESTAS, label="Respuesta Pieza Superior", initial='1',
                                           required=True)
    respuesta_inferior = forms.ChoiceField(choices=RESPUESTAS, label="Respuesta Pieza Inferior", initial='1',
                                           required=True)

    def __init__(self, *args, **kwargs):
        id_ambito = kwargs.pop('pk_ambito', None)
        super(PreguntasDominoModelForm, self).__init__(*args, **kwargs)
        ambito = Ambito.objects.get(id=id_ambito)
        self.fields["competencia"].queryset = ambito.competencias.all()
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(Field('descripcion', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field('competencia', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field(HTML(
                '<label>Imagen</label>'
                '<div style="height: calc(1.2em + 14px + 2px); overflow:hidden;" class="custom-file">'
                '<div><input type="file" name="imagen" class="custom-file-input" id="id_imagen">'
                '<label style="height: calc(1.2em + 14px + 2px);" class="custom-file-label" '
                'for="inputGroupFile01">{{ form.imagen.value|default:"Seleccione una imagen..." }}</label></div></div>'
                '{% if form.imagen.value %}<a href="/media/{{ form.imagen.value }}" data-lightbox="gallery-group">Ver imagen actual</a>{% endif %}'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2 align='center'"),
            Div(Field('respuesta_superior', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-3 mb-2"),
            Div(Field('respuesta_inferior', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-3 mb-2"),
            Div(Field(HTML(
                '<div class="form-group"><div class="form-check checkbox checkbox-css pt-0 checkbox-inline">'
                '<div class="form-check"><input type="checkbox" name="activo" class="form-check-input" id="id_activo" checked="">'
                '<label class="form-check-label" for="id_activo">¿La pregunta se encuentra activa?</label></div></div></div>'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 my-3 align='left'"),
        )

    class Meta:
        model = Preguntas
        fields = ('descripcion', 'competencia', 'imagen', 'respuesta_superior', 'respuesta_inferior', 'activo')
        widgets = {
            'competencia': Select2Widget(),
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
        }


class PreguntasMixtasModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        id_ambito = kwargs.pop('pk_ambito', None)
        super(PreguntasMixtasModelForm, self).__init__(*args, **kwargs)
        ambito = Ambito.objects.get(id=id_ambito)
        self.fields["competencia"].queryset = ambito.competencias.all()
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_class = 'col-sm-12'
        self.helper.label_class = 'col-md-3 create-label'
        self.helper.field_class = 'col-sm-12'
        self.helper.layout = Layout(
            Div(Field('tipo', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field('descripcion', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field('competencia', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field(HTML(
                '<label>Imagen</label>'
                '<div style="height: calc(1.2em + 14px + 2px); overflow:hidden;" class="custom-file">'
                '<div><input type="file" name="imagen" class="custom-file-input" id="id_imagen">'
                '<label style="height: calc(1.2em + 14px + 2px);" class="custom-file-label" '
                'for="inputGroupFile01">{{ form.imagen.value|default:"Seleccione una imagen..." }}</label></div></div>'
                '{% if form.imagen.value %}<a href="/media/{{ form.imagen.value }}" data-lightbox="gallery-group">Ver imagen actual</a>{% endif %}'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2 align='center'"),
            Div(Field(HTML(
                '<div class="form-group"><div class="form-check checkbox checkbox-css pt-0 checkbox-inline">'
                '<div class="form-check"><input type="checkbox" name="activo" class="form-check-input" id="id_activo" checked="">'
                '<label class="form-check-label" for="id_activo">¿La pregunta se encuentra activa?</label></div></div></div>'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 align='left'"),
            Div(Fieldset('Opciones de Respuesta',
                         Formset('respuestas_pregunta'), css_class="col-sm-12"), css_class="col-sm-12 mt-4 formset-preguntas"),
        )

    class Meta:
        model = Preguntas
        fields = ('tipo', 'descripcion', 'competencia', 'imagen', 'activo')
        widgets = {
            'tipo': Select2Widget(),
            'competencia': Select2Widget(),
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
        }


class PreguntasModificarForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PreguntasModificarForm, self).__init__(*args, **kwargs)
        self.fields["competencia"].queryset = self.instance.formulario.ambito.competencias.all()
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(Field('descripcion', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field('competencia', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field(HTML(
                '<label>Imagen</label>'
                '<div style="height: calc(1.2em + 14px + 2px); overflow:hidden;" class="custom-file">'
                '<div><input type="file" name="imagen" class="custom-file-input" id="id_imagen">'
                '<label style="height: calc(1.2em + 14px + 2px);" class="custom-file-label" '
                'for="inputGroupFile01">{{ form.imagen.value|default:"Seleccione una imagen..." }}</label></div></div>'
                '{% if form.imagen.value %}<a href="/media/{{ form.imagen.value }}" data-lightbox="gallery-group">Ver imagen actual</a>{% endif %}'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2 align='center'"),
            Div(Field(HTML(
                '<div class="form-group"><div class="form-check checkbox checkbox-css pt-0 checkbox-inline">'
                '<div class="form-check"><input type="checkbox" name="activo" class="form-check-input" id="id_activo" checked="">'
                '<label class="form-check-label" for="id_activo">¿La pregunta se encuentra activa?</label></div></div></div>'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 align='left'"),
            Div(Fieldset('Opciones de Respuesta',
                         Formset('respuestas_pregunta'), css_class="col-sm-12"), css_class="col-sm-12 mt-4 formset-preguntas"),
        )

    class Meta:
        model = Preguntas
        fields = ('descripcion', 'competencia', 'imagen', 'activo')

        widgets = {
            'competencia': Select2Widget(),
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
        }


class PreguntasDominoModificarForm(forms.ModelForm):
    RESPUESTAS = (
        ("0", "Vacío"),
        ("1", "Uno"),
        ("2", "Dos"),
        ("3", "Tres"),
        ("4", "Cuatro"),
        ("5", "Cinco"),
        ("6", "Seis"),
    )
    respuesta_superior = forms.ChoiceField(choices=RESPUESTAS, label="Respuesta Pieza Superior", initial='1',
                                           required=True)
    respuesta_inferior = forms.ChoiceField(choices=RESPUESTAS, label="Respuesta Pieza Inferior", initial='1',
                                           required=True)

    def __init__(self, *args, **kwargs):
        super(PreguntasDominoModificarForm, self).__init__(*args, **kwargs)
        self.fields["competencia"].queryset = self.instance.formulario.ambito.competencias.all()
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(Field('descripcion', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field('competencia', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2"),
            Div(Field(HTML(
                '<label>Imagen</label>'
                '<div style="height: calc(1.2em + 14px + 2px); overflow:hidden;" class="custom-file">'
                '<div><input type="file" name="imagen" class="custom-file-input" id="id_imagen">'
                '<label style="height: calc(1.2em + 14px + 2px);" class="custom-file-label" '
                'for="inputGroupFile01">{{ form.imagen.value|default:"Seleccione una imagen..." }}</label></div></div>'
                '{% if form.imagen.value %}<a href="/media/{{ form.imagen.value }}" data-lightbox="gallery-group">Ver imagen actual</a>{% endif %}'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2 align='center'"),
            Div(Field('respuesta_superior', css_class="form-control"),
                css_class="col-sm-12 col-xs-12 col-md-6 col-lg-3 mb-2"),
            Div(Field('respuesta_inferior', css_class="form-control"),
                css_class="col-sm-12 col-xs-12 col-md-6 col-lg-3 mb-2"),
            Div(Field(HTML(
                '<div class="form-group"><div class="form-check checkbox checkbox-css pt-0 checkbox-inline">'
                '<div class="form-check"><input type="checkbox" name="activo" class="form-check-input" id="id_activo" checked="">'
                '<label class="form-check-label" for="id_activo">¿La pregunta se encuentra activa?</label></div></div></div>'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 my-3 align='left'"),
        )

    class Meta:
        model = Preguntas
        fields = ('descripcion', 'competencia', 'imagen', 'respuesta_superior', 'respuesta_inferior', 'activo')
        widgets = {
            'competencia': Select2Widget(),
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
        }


class RespuestasModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(RespuestasModelForm, self).__init__(*args, **kwargs)
        self.fields["descripcion"].widget.attrs.update({
            'class': 'form-control'
        })

    class Meta:
        model = Respuestas
        fields = ('descripcion', 'imagen', 'correcta')


RespuestasFormSet = inlineformset_factory(
    Preguntas, Respuestas, form=RespuestasModelForm,
    fields=['descripcion', 'imagen', 'correcta'], extra=1, can_delete=True
)


class AmbitosModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AmbitosModelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(Field('nombre', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-4 col-lg-4 mb-2"),
            Div(Field('descripcion', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-4 col-lg-4 mb-2"),
                        Div(Field(HTML(
                            '<div class="form-group"><div class="form-check checkbox checkbox-css pt-0 checkbox-inline">'
                            '<div class="form-check"><input type="checkbox" name="activo" class="form-check-input" id="id_activo" checked="">'
                            '<label class="form-check-label" for="id_activo">¿El ámbito se encuentra activo?</label></div></div></div>'
                        )), css_class="col-sm-12 col-xs-12 col-md-4 col-lg-4 align='left'"),
                        Div(Field('competencias', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2")
        )

    class Meta:
        model = Ambito
        fields = ('nombre', 'descripcion', 'competencias', 'activo')
        widgets = {
            'competencias': Select2MultipleWidget(),
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
        }


class CompetenciasModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CompetenciasModelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(Field('nombre', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-4 col-lg-4 mb-2"),
            Div(Field('codigo_calificacion', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-4 col-lg-4 mb-2"),
            Div(Field('explicacion', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-4 col-lg-4 mb-2"),
            Div(Field('necesidad', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-4 col-lg-4 mb-2"),
            Div(Field('desarrollo', css_class="form-control"), css_class="col-sm-12 col-xs-12 col-md-4 col-lg-4 mb-2"),
            Div(Field(HTML(
                '<label>Imagen</label>'
                '<div style="height: calc(1.2em + 14px + 2px); overflow:hidden;" class="custom-file">'
                '<div><input type="file" name="imagen" class="custom-file-input" id="id_imagen">'
                '<label style="height: calc(1.2em + 14px + 2px);" class="custom-file-label" '
                'for="inputGroupFile01">{{ form.imagen.value|default:"Seleccione una imagen..." }}</label></div></div>'
                '{% if form.imagen.value %}<a href="/media/{{ form.imagen.value }}" data-lightbox="gallery-group">Ver imagen actual</a>{% endif %}'
            )), css_class="col-sm-12 col-xs-12 col-md-6 col-lg-6 mb-2 align='center'"),
            Div(Field(HTML(
                            '<div class="form-group"><div class="form-check checkbox checkbox-css pt-0 checkbox-inline">'
                            '<div class="form-check"><input type="checkbox" name="activo" class="form-check-input" id="id_activo" checked="">'
                            '<label class="form-check-label" for="id_activo">¿La competencia se encuentra activa?</label></div></div></div>'
                        )), css_class="col-sm-12 col-xs-12 col-md-4 col-lg-4 align='left'"),
        )

    class Meta:
        model = Competencias
        fields = ('nombre', 'explicacion', 'necesidad', 'desarrollo', 'activo', 'imagen', 'codigo_calificacion')

        widgets = {
            'explicacion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
            'necesidad': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
            'desarrollo': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:both; overflow:auto'
                }
            ),
        }

class FormularioMisExperiencias(forms.Form):
    OPCIONES = [
        ('Física', 'Física'), ('Informática', 'Informática'), ('Matemática', 'Matemática'), ('Historia', 'Historia'), ('Literatura', 'Literatura'),
        ('Idiomas Extranjeros', 'Idiomas Extranjeros'), ('Idioma Nativo y Literatura', 'Idioma Nativo y Literatura'), ('Artes', 'Artes'), ('Deportes y Educación Física', 'Deportes y Educación Física'),
        ('Ciencias Naturales y Biología', 'Ciencias Naturales y Biología'), ('Geografía', 'Geografía'), ('Economía, Contabilidad o Administracion', 'Economía, Contabilidad o Administracion'), ('Química', 'Química'),
        ('Salud del Ser Humano', 'Salud del Ser Humano'), ('Orden cerrado, Campamentos y Ordenamiento Militar en colegios', 'Orden cerrado, Campamentos y Ordenamiento Militar en colegios')
    ]
    tag = forms.ChoiceField(widget=forms.Select,
                                         choices=OPCIONES)

    def __init__(self, *args, **kwargs):
        super(FormularioMisExperiencias, self).__init__(*args, **kwargs)
        self.fields['tag'].label = False