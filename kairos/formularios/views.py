# Modulos Django
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.db.models import IntegerField, Q, Value as V
from django.db.models.functions import Cast, StrIndex, Substr
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import ListView, TemplateView
from django.views.generic.edit import CreateView, UpdateView

# Modulos de plugin externos
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import json, pytz

# Modulos de otras apps
from kairos.calificaciones.views import calificarFormulario
from kairos.core.mixins import MensajeMixin
from kairos.experiencias_jovenes.forms import JovenExperienciaActualForm, ClubesFormSet, \
    VoluntariadosFormSet, LaboralesFormSet, IndependientesFormSet, EmprendimientosFormSet
from kairos.guia.models import VerRecurso, Recurso
from kairos.historiales.models import RegistroTandasJoven
from kairos.perfiles_deseados.forms import JovenPerfilDeseadoForm
from kairos.perfiles_deseados.models import PerfilesDeseados
from kairos.respuestas_formularios.models import RespuestasJoven
from kairos.usuarios.models import Joven
from kairos.usuarios.forms import JovenDatosBasicosForm
from kairos.insignias.models import NivelesDeConocimiento

# Modulos internos de la app
from .forms import *
from .models import *

class FormulariosListado(LoginRequiredMixin, ListView):
    model = Formularios
    context_object_name = "formularios"
    template_name = "formularios/listado.html"

    def get_queryset(self):
        formularios = Formularios.objects.all()
        return formularios


class FormulariosConsultar(LoginRequiredMixin, ListView):
    model = Formularios
    context_object_name = "formularios"
    template_name = "formularios/consultar.html"

    def get_queryset(self):
        formularios = Formularios.objects.all()
        return formularios


class FormulariosRegistrar(LoginRequiredMixin, CreateView):
    model = Formularios
    form_class = FormulariosModelForm
    template_name = "formularios/registrar.html"
    success_url = reverse_lazy('formularios:registrar')

    def form_valid(self, form):
        formulario = form.save(commit=False)
        formulario.activo = True
        form.save()
        messages.success(self.request, "Formulario registrado satisfactoriamente")
        return super(FormulariosRegistrar, self).form_valid(form)

    def get_success_url(self):
        if 'preguntas' in self.request.POST:
            if self.object.tipo_preguntas == 'M':
                url = reverse_lazy("formularios:registrar_pregunta_mixta", kwargs={'id_formulario': self.object.pk})
            elif self.object.tipo_preguntas == 'D48':
                url = reverse_lazy("formularios:registrar_pregunta_domino", kwargs={'id_formulario': self.object.pk})
            else:
                url = reverse_lazy("formularios:registrar_pregunta", kwargs={'id_formulario': self.object.pk})
        else:
            url = reverse_lazy("formularios:listado")
        return url

class FormulariosModificar(LoginRequiredMixin, UpdateView):
    model = Formularios
    form_class = FormulariosModificarForm
    pk_url_kwarg = 'id_formulario'
    template_name = "formularios/modificar.html"
    success_url = reverse_lazy('formularios:listado')

    def get_context_data(self, **kwargs):
        data = super(FormulariosModificar, self).get_context_data(**kwargs)
        formulario = Formularios.objects.get(id=int(self.kwargs['id_formulario']))
        data['tipo_preguntas'] = formulario.tipo_preguntas
        return data

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Formulario modificado satisfactoriamente")
        return super(FormulariosModificar, self).form_valid(form)

# -------------------PREGUNTAS

class PreguntasListado(LoginRequiredMixin, ListView):
    model = Preguntas
    context_object_name = "preguntas"
    template_name = "formularios/preguntas/listado.html"

    def get_context_data(self, **kwargs):
        data = super(PreguntasListado, self).get_context_data(**kwargs)
        formulario = Formularios.objects.get(id=int(self.kwargs['id_formulario']))
        data['formulario'] = formulario
        return data

    def get_queryset(self):
        preguntas = Preguntas.objects.annotate(part=Cast(
            Substr("identificador", StrIndex("identificador", V("_")) + 1),
            IntegerField())).order_by("part").filter(formulario__id=int(self.kwargs['id_formulario']))
        return preguntas


class PreguntasConsultar(LoginRequiredMixin, ListView):
    model = Preguntas
    context_object_name = "preguntas"
    template_name = "formularios/preguntas/consultar.html"

    def get_context_data(self, **kwargs):
        data = super(PreguntasConsultar, self).get_context_data(**kwargs)
        formulario = Formularios.objects.get(id=int(self.kwargs['id_formulario']))
        data['formulario'] = formulario.nombre
        data['id'] = formulario.id
        return data

    def get_queryset(self):
        preguntas = Preguntas.objects.annotate(part=Cast(
            Substr("identificador", StrIndex("identificador", V("_")) + 1),
            IntegerField())).order_by("part").filter(formulario__id=int(self.kwargs['id_formulario']))
        return preguntas


def preguntaRegistrar(request, id_formulario):
    '''
        Permite resgitrar una pregunta a un formulario

        Parametros:
            id_formulario: identificador del formulario
            request: Elemento Http que posee informacion de la sesion.
    '''
    formulario = Formularios.objects.get(id=int(id_formulario))

    if request.user.is_superuser == False:
        messages.error(request, "Usted no está autorizado para gestionar las preguntas")
        return redirect('inicio')

    if formulario.tipo_preguntas == "M":
        return redirect('formularios:registrar_pregunta_mixta', id_formulario)
    elif formulario.tipo_preguntas == "D48":
        return redirect('formularios:registrar_pregunta_domino', id_formulario)
    else:
        return redirect('formularios:registrar_pregunta', id_formulario)


class PreguntaRegistrar(LoginRequiredMixin, CreateView):
    model = Preguntas
    form_class = PreguntasModelForm
    template_name = 'formularios/preguntas/registrar.html'
    pk_ambito = None

    def get_form_kwargs(self):
        pk_ambito = Formularios.objects.get(pk=self.kwargs['id_formulario']).ambito.pk
        kwargs = super(PreguntaRegistrar, self).get_form_kwargs()
        kwargs['pk_ambito'] = pk_ambito
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(PreguntaRegistrar, self).get_context_data(**kwargs)
        formulario = Formularios.objects.get(id=int(self.kwargs['id_formulario']))
        data['numero'] = Preguntas.objects.filter(formulario=formulario, activo=True).count() + 1
        data['formulario'] = formulario
        if self.request.POST:
            data['respuestas_pregunta'] = RespuestasFormSet(self.request.POST)
        else:
            data['respuestas_pregunta'] = RespuestasFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        formulario = context['formulario']
        id = formulario.id
        form.instance.formulario = formulario
        form.instance.tipo = formulario.tipo_preguntas
        form.instance.identificador = str(id) + "_" + str(context['numero'])  # idformulario_numeropregunta
        respuestas = context['respuestas_pregunta']
        with transaction.atomic():
            self.object = form.save()
            if respuestas.is_valid():
                respuestas.instance = self.object
                respuestas.save()
        messages.success(self.request, "Pregunta registrada satisfactoriamente")
        return super(PreguntaRegistrar, self).form_valid(form)

    def get_success_url(self):
        if 'nueva_pregunta' in self.request.POST:
            url = reverse_lazy("formularios:registrar_pregunta", kwargs={'id_formulario': self.object.formulario.pk})
        else:
            url = reverse_lazy("formularios:listado_preguntas", kwargs={'id_formulario': self.object.formulario.pk})
        return url


class PreguntasDominoRegistrar(LoginRequiredMixin, CreateView):
    model = Preguntas
    form_class = PreguntasDominoModelForm
    template_name = 'formularios/preguntas/registrar.html'

    def get_form_kwargs(self):
        pk_ambito = Formularios.objects.get(pk=self.kwargs['id_formulario']).ambito.pk
        kwargs = super(PreguntasDominoRegistrar, self).get_form_kwargs()
        kwargs['pk_ambito'] = pk_ambito
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(PreguntasDominoRegistrar, self).get_context_data(**kwargs)
        formulario = Formularios.objects.get(id=int(self.kwargs['id_formulario']))
        data['numero'] = Preguntas.objects.filter(formulario=formulario, activo=True).count() + 1
        data['formulario'] = formulario
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        formulario = context['formulario']
        id = formulario.id
        form.instance.formulario = formulario
        form.instance.tipo = formulario.tipo_preguntas
        form.instance.identificador = str(id) + "_" + str(context['numero'])  # idformulario_numeropregunta
        respuesta_superior = form.cleaned_data['respuesta_superior']
        respuesta_inferior = form.cleaned_data['respuesta_inferior']
        self.object = form.save()
        r1 = Respuestas.objects.create(descripcion=respuesta_superior, pregunta=self.object, correcta=True)
        r1.save()
        r2 = Respuestas.objects.create(descripcion=respuesta_inferior, pregunta=self.object, correcta=True)
        r2.save()
        messages.success(self.request, "Pregunta registrada satisfactoriamente")
        return super(PreguntasDominoRegistrar, self).form_valid(form)

    def get_success_url(self):
        pregunta = Preguntas.objects.get(pk=self.object.pk)
        if 'nueva_pregunta' in self.request.POST:
            url = reverse_lazy("formularios:registrar_pregunta_domino", kwargs={'id_formulario': pregunta.formulario.pk})
        else:
            url = reverse_lazy("formularios:listado_preguntas", kwargs={'id_formulario': pregunta.formulario.pk})
        return url


class PreguntasMixtaRegistrar(LoginRequiredMixin, CreateView):
    model = Preguntas
    form_class = PreguntasMixtasModelForm
    template_name = 'formularios/preguntas/registrar.html'

    def get_form_kwargs(self):
        pk_ambito = Formularios.objects.get(pk=self.kwargs['id_formulario']).ambito.pk
        kwargs = super(PreguntasMixtaRegistrar, self).get_form_kwargs()
        kwargs['pk_ambito'] = pk_ambito
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(PreguntasMixtaRegistrar, self).get_context_data(**kwargs)
        formulario = Formularios.objects.get(id=int(self.kwargs['id_formulario']))
        data['numero'] = Preguntas.objects.filter(formulario=formulario, activo=True).count() + 1
        data['formulario'] = formulario
        if self.request.POST:
            data['respuestas_pregunta'] = RespuestasFormSet(self.request.POST)
        else:
            data['respuestas_pregunta'] = RespuestasFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        formulario = context['formulario']
        id = formulario.id
        form.instance.formulario = formulario
        form.instance.identificador = str(id) + "_" + str(context['numero'])  # idformulario_numeropregunta
        respuestas = context['respuestas_pregunta']
        with transaction.atomic():
            self.object = form.save()
            if respuestas.is_valid() and form.instance.tipo == 'SM':
                respuestas.instance = self.object
                respuestas.save()
        messages.success(self.request, "Pregunta registrada satisfactoriamente")
        return super(PreguntasMixtaRegistrar, self).form_valid(form)

    def get_success_url(self):
        pregunta = Preguntas.objects.get(pk=self.object.pk)
        if 'nueva_pregunta' in self.request.POST:
            url = reverse_lazy("formularios:registrar_pregunta_mixta", kwargs={'id_formulario': pregunta.formulario.pk})
        else:
            url = reverse_lazy("formularios:listado_preguntas", kwargs={'id_formulario': pregunta.formulario.pk})
        return url


class PreguntaModificar(LoginRequiredMixin, UpdateView):
    model = Preguntas
    form_class = PreguntasModificarForm
    pk_url_kwarg = 'id_pregunta'
    template_name = 'formularios/preguntas/modificar.html'

    def get_context_data(self, **kwargs):
        data = super(PreguntaModificar, self).get_context_data(**kwargs)
        pregunta = Preguntas.objects.get(id=int(self.kwargs['id_pregunta']))
        identificador = pregunta.identificador
        data['numero'] = identificador[identificador.find('_') + 1:]
        data['pregunta'] = pregunta
        if self.request.POST:
            data['respuestas_pregunta'] = RespuestasFormSet(self.request.POST, instance=pregunta)
        else:
            data['respuestas_pregunta'] = RespuestasFormSet(instance=pregunta)
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        respuestas = context['respuestas_pregunta']
        with transaction.atomic():
            self.object = form.save()
            if respuestas.is_valid() and form.instance.tipo == 'SM':
                respuestas.instance = self.object
                respuestas.save()
        messages.success(self.request, "Pregunta modificada satisfactoriamente")
        return super(PreguntaModificar, self).form_valid(form)

    def get_success_url(self):
        pregunta = Preguntas.objects.get(id=int(self.kwargs['id_pregunta']))
        return reverse_lazy("formularios:listado_preguntas", kwargs={'id_formulario': pregunta.formulario.id})


class PreguntaDominoModificar(LoginRequiredMixin, UpdateView):
    model = Preguntas
    form_class = PreguntasDominoModificarForm
    pk_url_kwarg = 'id_pregunta'
    template_name = 'formularios/preguntas/modificar.html'

    def get_context_data(self, **kwargs):
        data = super(PreguntaDominoModificar, self).get_context_data(**kwargs)
        pregunta = Preguntas.objects.get(id=int(self.kwargs['id_pregunta']))
        identificador = pregunta.identificador
        data['numero'] = identificador[identificador.find('_') + 1:]
        data['pregunta'] = pregunta
        return data

    def form_valid(self, form):
        respuesta_superior = form.cleaned_data['respuesta_superior']
        respuesta_inferior = form.cleaned_data['respuesta_inferior']
        Respuestas.objects.filter(pregunta=self.object.pk).delete()
        self.object = form.save()
        r1 = Respuestas.objects.create(descripcion=respuesta_superior, pregunta=self.object, correcta=True)
        r1.save()
        r2 = Respuestas.objects.create(descripcion=respuesta_inferior, pregunta=self.object, correcta=True)
        r2.save()
        messages.success(self.request, "Pregunta modificada satisfactoriamente")
        return super(PreguntaDominoModificar, self).form_valid(form)

    def get_initial(self):
        initial = super(PreguntaDominoModificar, self).get_initial()
        respuestas = Respuestas.objects.filter(pregunta=self.get_object().pk)
        respuesta_superior = respuestas.first()
        respuesta_inferior = respuestas.last()
        initial['respuesta_superior'] = respuesta_superior
        initial['respuesta_inferior'] = respuesta_inferior
        return initial

    def get_success_url(self):
        pregunta = Preguntas.objects.get(id=int(self.kwargs['id_pregunta']))
        return reverse_lazy("formularios:listado_preguntas", kwargs={'id_formulario': pregunta.formulario.id})


# #################TIPOS DE FORMULARIOS############
class AmbitosListado(LoginRequiredMixin, ListView):
    model = Ambito
    context_object_name = "ambitos"
    template_name = "formularios/ambitos/listado.html"

    def get_queryset(self):
        ambitos = Ambito.objects.all()
        return ambitos


class AmbitosConsultar(LoginRequiredMixin, ListView):
    model = Ambito
    context_object_name = "ambitos"
    template_name = "formularios/ambitos/consultar.html"

    def get_queryset(self):
        ambitos = Ambito.objects.all()
        return ambitos


class AmbitoRegistrar(LoginRequiredMixin, CreateView):
    model = Ambito
    form_class = AmbitosModelForm
    template_name = "formularios/ambitos/registrar.html"
    success_url = reverse_lazy('formularios:registrar_ambito')

    def form_valid(self, form):
        messages.success(self.request, "Ámbito registrado satisfactoriamente")
        return super(AmbitoRegistrar, self).form_valid(form)


class AmbitoModificar(LoginRequiredMixin, UpdateView):
    model = Ambito
    form_class = AmbitosModelForm
    pk_url_kwarg = 'id_ambito'
    template_name = "formularios/ambitos/modificar.html"
    success_url = reverse_lazy('formularios:listado_ambitos')

    def form_valid(self, form):
        messages.success(self.request, "Ámbito modificado satisfactoriamente")
        return super(AmbitoModificar, self).form_valid(form)

# #################TIPO DE COMPETENCIAS############
class CompetenciasListado(LoginRequiredMixin, ListView):
    model = Competencias
    context_object_name = "competencias"
    template_name = "formularios/competencias/listado.html"

    def get_queryset(self):
        competencias = Competencias.objects.all()
        return competencias


class CompetenciasConsultar(LoginRequiredMixin, ListView):
    model = Competencias
    context_object_name = "competencias"
    template_name = "formularios/competencias/consultar.html"

    def get_queryset(self):
        competencias = Competencias.objects.all()
        return competencias


class CompetenciasRegistrar(LoginRequiredMixin, CreateView):
    model = Competencias
    form_class = CompetenciasModelForm
    template_name = "formularios/competencias/registrar.html"
    success_url = reverse_lazy('formularios:registrar_competencias')

    def form_valid(self, form):
        messages.success(self.request, "Competencia registrada satisfactoriamente")
        return super(CompetenciasRegistrar, self).form_valid(form)


class CompetenciasModificar(LoginRequiredMixin, UpdateView):
    model = Competencias
    form_class = CompetenciasModelForm
    pk_url_kwarg = 'id_competencia'
    template_name = "formularios/competencias/modificar.html"
    success_url = reverse_lazy('formularios:listado_competencias')

    def form_valid(self, form):
        messages.success(self.request, "Competencia modificada satisfactoriamente")
        return super(CompetenciasModificar, self).form_valid(form)

class ProcesamientoDeInformacionJovenConsultar(LoginRequiredMixin, TemplateView):
    template_name = "competencias/consultar_procesamiento_informacion.html"

    def dispatch(self, request, *args, **kwargs):
        joven = Joven.objects.get(pk=self.request.user.pk)
        if joven.tanda_formularios == None:
            messages.error(request, "No tienes asignados los cuestionarios!")
            return redirect("inicio")
        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True)
        formularios_disponibles = formularios_joven.filter(
            ~Q(respuestasjoven__joven=joven.id) | Q(respuestasjoven__joven=joven.id, respuestasjoven__completas=False)
        )
        if formularios_disponibles.count() != 0:
            messages.error(request, "No has completado todos tus cuestionarios!")
            return redirect("inicio")
        return super(ProcesamientoDeInformacionJovenConsultar, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        from kairos.guia.models import Recurso
        data = super(ProcesamientoDeInformacionJovenConsultar, self).get_context_data(**kwargs)
        id_joven = self.request.user.id
        joven = Joven.buscar(id_joven=id_joven)

        edad_del_joven = joven.obtener_edad_joven()
        genero_del_joven = joven.genero
        puntaje_razonamiento_cuantitativo = joven.obtener_puntaje_razonamiento_cuantitativo()
        puntaje_analisis_en_lectura = joven.obtener_puntaje_analisis_en_lectura()

        SeccionVisitada.calificar_seccion_joven(id_joven, "NIVEL DE CUALIFICACIÓN")
        ids_competencias = joven.obtener_competencias_procesamiento()
        data['joven'] = joven
        data['ids_competencias'] = ids_competencias
        data['edad_del_joven'] = edad_del_joven
        data['genero_del_joven'] = [genero_del_joven]
        data['puntaje_razonamiento_cuantitativo'] = puntaje_razonamiento_cuantitativo
        data['puntaje_analisis_en_lectura'] = puntaje_analisis_en_lectura
        data['niveles_conocimiento'] = NivelesDeConocimiento.obtener_activos()
        data['primaria'] = [1, 2, 3, 4, 5]
        data['bachilerato1'] = [6, 7, 8, 9]
        data['bachilerato2'] = [10, 11, 12]
        data['D48'] = joven.obtener_porcentaje_razonamiento_cuantitativo()
        return data

class HabilidadesSocialesJovenConsultar(LoginRequiredMixin, TemplateView):
    template_name = "competencias/consultar_habilidades_sociales.html"

    def dispatch(self, request, *args, **kwargs):
        joven = Joven.objects.get(pk=self.request.user.pk)
        if joven.tanda_formularios == None:
            messages.error(request, "No tienes asignados los cuestionarios!")
            return redirect("inicio")
        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True)
        formularios_disponibles = formularios_joven.filter(
            ~Q(respuestasjoven__joven=joven.id) | Q(respuestasjoven__joven=joven.id, respuestasjoven__completas=False)
        )
        if formularios_disponibles.count() != 0:
            messages.error(request, "No has completado todos tus cuestionarios!")
            return redirect("inicio")
        return super(HabilidadesSocialesJovenConsultar, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        from kairos.guia.models import Recurso
        data = super(HabilidadesSocialesJovenConsultar, self).get_context_data(**kwargs)
        usuario = Joven.objects.get(id=self.request.user.id)

        competencias = usuario.obtener_competencias_habilidades_sociales()        
        data['competencias'] = competencias        
        return data

class AptitudesEInteresesJovenConsultar(LoginRequiredMixin, TemplateView):
    template_name = "competencias/consultar_aptitudes_e_intereses.html"

    def dispatch(self, request, *args, **kwargs):
        joven = Joven.objects.get(pk=self.request.user.pk)
        if joven.tanda_formularios == None:
            messages.error(request, "No tienes asignados los cuestionarios!")
            return redirect("inicio")
        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True)
        formularios_disponibles = formularios_joven.filter(
            ~Q(respuestasjoven__joven=joven.id) | Q(respuestasjoven__joven=joven.id, respuestasjoven__completas=False)
        )
        if formularios_disponibles.count() != 0:
            messages.error(request, "No has completado todos tus cuestionarios!")
            return redirect("inicio")
        return super(AptitudesEInteresesJovenConsultar, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        from kairos.guia.models import Recurso
        data = super(AptitudesEInteresesJovenConsultar, self).get_context_data(**kwargs)
        id_joven = self.request.user.id
        usuario = Joven.objects.get(id=self.request.user.id)

        competencias = usuario.obtener_competencias_aptitudes_e_intereses()
        data['competencias'] = competencias
        SeccionVisitada.calificar_seccion_joven(id_joven, "ÁREA Y NIVEL DE CONOCIMIENTO")

        return data

class MisExperienciasJovenConsultar(LoginRequiredMixin, TemplateView):
    template_name = "competencias/consultar_mis_experiencias.html"

    def dispatch(self, request, *args, **kwargs):
        from kairos.guia.models import Recurso
        joven = Joven.objects.get(pk=self.request.user.pk)
        if joven.tanda_formularios == None:
            messages.error(request, "No tienes asignados los cuestionarios!")
            return redirect("inicio")
        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True)
        formularios_disponibles = formularios_joven.filter(
            ~Q(respuestasjoven__joven=joven.id) | Q(respuestasjoven__joven=joven.id, respuestasjoven__completas=False)
        )
        if formularios_disponibles.count() != 0:
            messages.error(request, "No has completado todos tus cuestionarios!")
            return redirect("inicio")
        return super(MisExperienciasJovenConsultar, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(MisExperienciasJovenConsultar, self).get_context_data(**kwargs)
        id_joven = self.request.user.id
        usuario = Joven.objects.get(id=id_joven)

        competencias = usuario.obtener_listado_de_experiencias()
        data['competencias'] = competencias
        SeccionVisitada.calificar_seccion_joven(id_joven, "EXPERIENCIA")
        
        return data

class InteresesAptitudesDetalleJovenConsultar(LoginRequiredMixin, TemplateView):
    template_name = "competencias/detalle/detalle_aptitudes_e_intereses.html"

    def get_context_data(self, **kwargs):
        data = super(InteresesAptitudesDetalleJovenConsultar, self).get_context_data(**kwargs)
        id_joven = self.request.user.id
        competencia = Competencias.objects.get(id=self.kwargs['id_competencia'])
        competencia.calificar_competencia_joven(id_joven)
        resultado = self.kwargs['resultado']
        if resultado == 0:
            data['calificacion'] = 'BAJO'
        elif resultado == 1:
            data['calificacion'] = 'MEDIO'
        else:
            data['calificacion'] = 'ALTO'
        data['competencia'] = competencia
        return data


class CompetenciasMiExperienciaDetalleJovenConsultar(LoginRequiredMixin, TemplateView):
    template_name = "competencias/detalle/detalle_mis_experiencias.html"

    def get_context_data(self, **kwargs):
        data = super(CompetenciasMiExperienciaDetalleJovenConsultar, self).get_context_data(**kwargs)
        id_joven = self.request.user.id
        joven = Joven.objects.get(id=id_joven)
        promedios = []
        competencia = Competencias.objects.get(id=self.kwargs['id_competencia'])
        if competencia.nombre == 'Experiencia como CLUBES JUVENILES':
            promedios = joven.obtener_promedio_de_experiencias_clubes_juveniles()
        elif competencia.nombre == 'Experiencia como EMPRENDEDOR':
            promedios = joven.obtener_promedio_de_experiencias_emprendedor()
        elif competencia.nombre == 'Experiencia como VOLUNTARIADO':
            promedios = joven.obtener_promedio_de_experiencias_voluntariado()
        elif competencia.nombre == 'Experiencia como COLABORADOR':
            promedios = joven.obtener_promedio_de_experiencias_colaborador()
        elif competencia.nombre == 'Experiencia como INDEPENDIENTE':
            promedios = joven.obtener_promedio_de_experiencias_independiente()
        elif competencia.nombre == 'Experiencia Academica Colegio':
            tag = self.obtener_consulta()['tag']
            formulario = FormularioMisExperiencias(initial={'tag': tag})
            data['formulario'] = formulario
            data['experiencias'] = self.obtener_consulta()['promedios']
            data['tag'] = tag
        data['promedios'] = promedios
        data['competencia'] = competencia
        competencia.calificar_competencia_joven(id_joven)
        return data

    def obtener_consulta(self):
        joven = Joven.objects.get(id=self.request.user.id)
        promedios = []
        tag = 'Física'
        if self.request.GET:
            if 'tag' in self.request.GET:
                tag = self.request.GET['tag']

        promedios = joven.obtener_promedios_academicos(tag)
        return {'tag': tag, 'promedios': promedios}

class CompetenciasDetalleJovenConsultar(LoginRequiredMixin, TemplateView):
    template_name = "competencias/detalle/detalle_competencia.html"

    def get_context_data(self, **kwargs):
        data = super(CompetenciasDetalleJovenConsultar, self).get_context_data(**kwargs)
        id_joven = self.request.user.id
        joven = Joven.objects.get(id=id_joven)
        competencia = Competencias.objects.get(id=self.kwargs['id_competencia'])
        genero = joven.genero
        codigo_calificación = [
            'Números',
            'Lectura',
            'Negociación',
            'Autoconciencia',
            'Empatía',
            'Autocontrol',
            'Autoconfianza',
            'Dirección',
            'Carisma',
            'Innovación',
            'Desarrollo',
            'Motivación',
            'Inspiración',
            'Socializar',
            'Liderazgo_laizzes_faire',
            'Liderazgo_transaccional',
            'Liderazgo_transformacional'
        ]
        competencia.calificar_competencia_joven(id_joven)

        data['codigo_calificación'] = codigo_calificación

        if competencia.codigo_calificacion == 'Números':
            data['edad_del_joven'] = joven.obtener_edad_joven()
            data['genero_del_joven'] = [genero]
            data['puntaje_razonamiento_cuantitativo'] = joven.obtener_puntaje_razonamiento_cuantitativo()
        elif competencia.codigo_calificacion == 'Lectura':
            data['puntaje_analisis_en_lectura'] = joven.obtener_puntaje_analisis_en_lectura()
        elif competencia.codigo_calificacion == 'Negociación':
            data['promedio_liderazgo_claridad_en_negociar'] = joven.obtener_promedio_liderazgo_claridad_en_negociar()
        elif competencia.codigo_calificacion == 'Autoconciencia':
            data['puntaje_de_auto_conciencia'] = joven.obtener_puntaje_de_auto_conciencia()
        elif competencia.codigo_calificacion == 'Empatía':
            data['puntaje_de_empatia'] = joven.obtener_puntaje_de_empatia()
        elif competencia.codigo_calificacion == 'Autocontrol':
            data['puntaje_de_auto_control'] = joven.obtener_puntaje_de_auto_control()
        elif competencia.codigo_calificacion == 'Autoconfianza':
            data['puntaje_de_auto_confianza'] = joven.obtener_puntaje_de_auto_confianza()
        elif competencia.codigo_calificacion == 'Dirección':
            data['promedio_liderazgo_direccion_rutinaria'] = joven.obtener_promedio_liderazgo_direccion_rutinaria()
        elif competencia.codigo_calificacion == 'Carisma':
            data['promedio_liderazgo_carisma'] = joven.obtener_promedio_liderazgo_carisma()
        elif competencia.codigo_calificacion == 'Innovación':
            data['promedio_liderazgo_innovacion'] = joven.obtener_promedio_liderazgo_innovacion()
        elif competencia.codigo_calificacion == 'Desarrollo':
            data['promedio_liderazgo_deseo_que_otros_crezcan'] = joven.obtener_promedio_liderazgo_deseo_que_otros_crezcan()
        elif competencia.codigo_calificacion == 'Motivación':
            data['puntaje_de_motivacion'] = joven.obtener_puntaje_de_motivacion()
        elif competencia.codigo_calificacion == 'Inspiración':
            data['promedio_liderazgo_inspiracion'] = joven.obtener_promedio_liderazgo_inspiracion()
        elif competencia.codigo_calificacion == 'Socializar':
            data['puntaje_de_capacidad_para_socializar'] = joven.obtener_puntaje_de_capacidad_para_socializar()
        elif competencia.codigo_calificacion == 'Liderazgo_transformacional':
            data['puntaje_liderazgo'] = joven.obtener_promedio_liderazgo()["transformacional"]
        elif competencia.codigo_calificacion == 'Liderazgo_transaccional':
            data['puntaje_liderazgo'] = joven.obtener_promedio_liderazgo()["transaccional"]
        elif competencia.codigo_calificacion == 'Liderazgo_laizzes_faire':
            data['puntaje_liderazgo'] = joven.obtener_promedio_liderazgo()["laizzes_faire"]

        data['competencia'] = competencia
        back = self.request.GET.get("back")
        if back is not None:
            data['back'] = back
        return data


# ########### RESPONDIENDO LOS FORMULARIOS ###############
def responderFormulario(request):
    from kairos.formularios.utils import hay_tiempo_disponible
    '''
        Se encarga de actualizar el formulario actual que debe contestar el Joven

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
    '''
    context = dict()
    if not request.user.is_authenticated:
        messages.error(request, "Usted no está autorizado para realizar esta acción")
        return redirect('inicio')

    if not request.user.tipo == "Joven":
        messages.error(request, "Usted no está autorizado para responder este cuestionario!")
        return redirect("inicio")

    joven = Joven.objects.get(pk=request.user.pk)

    if not joven.datos_basicos_completos():
        return redirect("formularios:responder_datos_basicos")

    if not joven.experiencias_actuales_completos():
        return redirect("formularios:responder_experiencia_actual")

    if not joven.tanda_formularios:
        messages.info(request, "No hay cuestionarios disponibles!")
        return redirect("inicio")

    formularios_joven = joven.tanda_formularios.formularios.filter(activo=True)
    formularios_disponibles = formularios_joven.filter(
        ~Q(respuestasjoven__joven=joven.id) | Q(respuestasjoven__joven=joven.id, respuestasjoven__completas=False)
    )

    try:
        fecha_inicio = RespuestasJoven.objects.get(joven=joven, formulario_id=2).fecha_inicio
        if hay_tiempo_disponible(formularios_disponibles.get(identificador=1).tiempo_limite, fecha_inicio) == False:
            formulario_sin_calificar = formularios_disponibles.get(Q(identificador=1, respuestasjoven__joven=joven.id, respuestasjoven__completas=False))
            if formulario_sin_calificar != None:
                respuestas_formulario_sin_calificar = RespuestasJoven.objects.get(joven=joven.pk, formulario=formulario_sin_calificar.pk, tanda=joven.tanda_formularios.pk)
                calificarFormulario(respuestas_formulario_sin_calificar, calificacion_fuera_tiempo=True)
                respuestas_formulario_sin_calificar.completas = True
                respuestas_formulario_sin_calificar.save()
            formularios_disponibles = formularios_disponibles.exclude(identificador=1)
    except:
        pass

    if formularios_disponibles.count() == 0:
        messages.success(request, "Ya has completado todos los cuestionarios!")
        return redirect("inicio")
    else:
        formulario_actual = formularios_disponibles.first()

    if formulario_actual.tipo_preguntas == "D48":
        max_preguntas_pagina = 1
    else:
        max_preguntas_pagina = 10

    context['formulario'] = formulario_actual
    preguntas_contestadas = 0
    try:
        respuestas_formulario_actual = RespuestasJoven.objects.get(joven=joven.pk, formulario=formulario_actual.pk, tanda=joven.tanda_formularios.pk)
        preguntas_contestadas = len(respuestas_formulario_actual.data)

    except:
        pass
    context['preguntas_contestadas'] = preguntas_contestadas
    context['formularios_list'] = formularios_joven
    context['preguntas'] = Preguntas.objects.annotate(part=Cast(Substr("identificador", StrIndex("identificador", V("_")) + 1), IntegerField())).order_by("part").filter(formulario=formulario_actual.pk, activo=True)
    context['puede_releer'] = formulario_actual.releer
    context['max_preguntas_pagina'] = max_preguntas_pagina
    tiempo_limite = 0

    if formulario_actual.tiempo_limite:
        tiempo_limite = formulario_actual.tiempo_limite

    if request.is_ajax():
        hora_inicio = None
        try:
            respuestas_formulario = RespuestasJoven.objects.get(joven=joven, formulario__id=2, tanda=joven.tanda_formularios.pk)
            if respuestas_formulario.fecha_inicio is None:
                respuestas_formulario.fecha_inicio = timezone.now()
                respuestas_formulario.save()
            hora_inicio = respuestas_formulario.fecha_inicio
        except:
            hora_inicio = timezone.now()

        request.session['hora_inicio'] = str(hora_inicio)

        hora_actual = timezone.now()
        hora_final = hora_inicio + timedelta(seconds=60 * tiempo_limite)
        response = JsonResponse({
            'hora_inicio': hora_inicio,
            'hora_final': hora_final,
            'fecha_actual': timezone.now(),
        })
        response.status_code = 201
        if hora_actual > hora_final:
            return response
        else:
            return response
    else:
        return render(request, 'formularios/formularios/formularios_joven.html', context)


def responderFormularioSeleccionado(request, pk):
    if not request.user.is_authenticated:
        messages.error(request, "Usted no está autorizado para realizar esta acción")
        return redirect('inicio')

    if not request.user.tipo == "Joven":
        messages.error(request, "Usted no está autorizado para responder este cuestionario!")
        return redirect("inicio")

    joven = Joven.objects.get(pk=request.user.pk)

    if not joven.datos_basicos_completos():
        return redirect("formularios:responder_datos_basicos")

    if not joven.experiencias_actuales_completos():
        return redirect("formularios:responder_experiencia_actual")

    if not joven.tanda_formularios:
        messages.info(request, "No hay cuestionarios disponibles!")
        return redirect("inicio")

    formularios_joven = joven.tanda_formularios.formularios.filter(activo=True).order_by('pk')

    seleccionado_qs = Formularios.objects.filter(pk=pk)
    if seleccionado_qs.exists() and (seleccionado_qs.first() in formularios_joven):
        formulario_actual = seleccionado_qs.first()
        if formulario_actual.tipo_preguntas == "D48":
            max_preguntas_pagina = 1
        else:
            max_preguntas_pagina = 10

        preguntas = Preguntas.objects.annotate(
            part=Cast(Substr("identificador", StrIndex("identificador", V("_")) + 1), IntegerField())
        ).order_by("part").filter(
            formulario=formulario_actual.pk, activo=True
        )

        return render(request, 'formularios/formularios/formularios_joven.html', {
            'formulario': formulario_actual,
            'formularios_list': formularios_joven,
            'preguntas': preguntas,
            'max_preguntas_pagina': max_preguntas_pagina,
            'puede_releer': formulario_actual.releer,
        })
    else:
        messages.info(request, "Formulario no disponible!")
        return redirect("inicio")


class DatosBasicosFormulario(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = Joven
    form_class = JovenDatosBasicosForm
    template_name = 'formularios/formularios/formularios_datos_basicos_joven.html'
    success_url = reverse_lazy('formularios:responder')
    mensaje_exito = 'Datos básicos guardados correctamente!'
    mensaje_error = 'Ha ocurrido un error al guardar los datos básicos, por favor intente de nuevo!'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect('inicio')

        if not request.user.tipo == "Joven":
            messages.error(request, "Usted no está autorizado para responder este cuestionario!")
            return redirect("inicio")

        joven = Joven.objects.get(pk=request.user.pk)
        if not joven.tanda_formularios:
            messages.info(request, "No hay cuestionarios disponibles!")
            return redirect("inicio")
        return super(DatosBasicosFormulario, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.factores_sensacion_bienestar = form.cleaned_data['factores_sensaciones_bienestar']
        form.instance.jornadas = form.cleaned_data['jornadas_del_joven']
        return super(DatosBasicosFormulario, self).form_valid(form)

    def get_initial(self):
        initial = super(DatosBasicosFormulario, self).get_initial()
        joven = self.get_object()
        initial['factores_sensaciones_bienestar'] = joven.factores_sensacion_bienestar
        initial['jornadas_del_joven'] = joven.jornadas
        return initial

    def get_object(self):
        return Joven.objects.get(pk=self.request.user.pk)

    def get_context_data(self, **kwargs):
        from kairos.guia.models import Recurso
        data = super(DatosBasicosFormulario, self).get_context_data(**kwargs)
        joven = Joven.objects.get(pk=self.request.user.pk)
        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True).order_by('pk')
        
        
        data['formularios_list'] = formularios_joven
        return data


class ExperienciasActualesFormulario(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = Joven
    form_class = JovenExperienciaActualForm
    template_name = 'formularios/formularios/formularios_experiencia_actual_joven.html'
    mensaje_exito = 'Experiencia actual guardada correctamente!'
    mensaje_error = 'Ha ocurrido un error al guardar la experiencia actual, por favor intente de nuevo!'
    success_url = reverse_lazy('formularios:responder')

    def get_object(self):
        return Joven.objects.get(pk=self.request.user.pk)

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect('inicio')

        if request.user.is_authenticated and not request.user.tipo == "Joven":
            messages.error(request, "Usted no está autorizado para responder este cuestionario!")
            return redirect("inicio")

        joven = Joven.objects.get(pk=request.user.pk)
        if not joven.tanda_formularios:
            messages.info(request, "No hay cuestionarios disponibles!")
            return redirect("inicio")
        return super(ExperienciasActualesFormulario, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(ExperienciasActualesFormulario, self).get_context_data(**kwargs)
        joven = Joven.objects.get(pk=self.request.user.pk)

        if self.request.POST:
            data['clubes'] = ClubesFormSet(self.request.POST, instance=joven)
            data['voluntariados'] = VoluntariadosFormSet(self.request.POST, instance=joven)
            data['experiencias_laborales'] = LaboralesFormSet(self.request.POST, instance=joven)
            data['trabajos_independientes'] = IndependientesFormSet(self.request.POST, instance=joven)
            data['emprendimientos'] = EmprendimientosFormSet(self.request.POST, instance=joven)
        else:
            data['clubes'] = ClubesFormSet(instance=joven)
            data['voluntariados'] = VoluntariadosFormSet(instance=joven)
            data['experiencias_laborales'] = LaboralesFormSet(instance=joven)
            data['trabajos_independientes'] = IndependientesFormSet(instance=joven)
            data['emprendimientos'] = EmprendimientosFormSet(instance=joven)

        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True).order_by('pk')
        data['formularios_list'] = formularios_joven
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        clubes = context['clubes']
        voluntariados = context['voluntariados']
        laborales = context['experiencias_laborales']
        independientes = context['trabajos_independientes']
        emprendimientos = context['emprendimientos']
        with transaction.atomic():
            self.object = form.save()
            if clubes.is_valid():
                clubes.instance = self.object
                clubes.save()
            if voluntariados.is_valid():
                voluntariados.instance = self.object
                voluntariados.save()
            if laborales.is_valid():
                laborales.instance = self.object
                laborales.save()
            if independientes.is_valid():
                independientes.instance = self.object
                independientes.save()
            if emprendimientos.is_valid():
                emprendimientos.instance = self.object
                emprendimientos.save()
        return super(ExperienciasActualesFormulario, self).form_valid(form)


class JovenPerfilDeseadoFormulario(LoginRequiredMixin, MensajeMixin, CreateView):
    model = PerfilesDeseados
    form_class = JovenPerfilDeseadoForm
    template_name = 'formularios/formularios/formularios_perfil_deseado_joven.html'
    success_url = reverse_lazy('inicio')
    mensaje_exito = 'Perfil deseado guardado correctamente!'
    mensaje_error = 'Ha ocurrido un error al guardar el perfil deseado, por favor intente de nuevo!'


    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect('inicio')

        if not request.user.tipo == "Joven":
            messages.error(request, "Usted no está autorizado para responder este cuestionario!")
            return redirect("inicio")

        joven = Joven.objects.get(pk=request.user.pk)
        if not joven.tanda_formularios:
            messages.info(request, "No hay cuestionarios disponibles!")
            return redirect("inicio")
        return super(JovenPerfilDeseadoFormulario, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):

        perfil = form.save(commit=False)

        imagen_anterior = perfil.foto_perfil
        if not self.request.POST.get('imagen-clear') is None and imagen_anterior:
            if os.path.isfile(imagen_anterior.path):
                os.remove(imagen_anterior.path)
            perfil.foto_perfil = ""

        joven = Joven.objects.get(pk=self.request.user.pk)
        form.instance.joven = joven
        form.instance.deseo_aprendizaje = form.cleaned_data['deseos_aprendizaje']
        if form.cleaned_data['favorito'] == True:
            mis_perfiles = PerfilesDeseados.objects.filter(joven=joven, favorito=True)
            for mi_perfil in mis_perfiles:
                mi_perfil.favorito = False
                mi_perfil.save()
        return super(JovenPerfilDeseadoFormulario, self).form_valid(form)

    def get_context_data(self, **kwargs):
        from kairos.guia.models import Recurso
        data = super(JovenPerfilDeseadoFormulario, self).get_context_data(**kwargs)
        joven = Joven.objects.get(pk=self.request.user.pk)
        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True).order_by('pk')

        data['formularios_list'] = formularios_joven
        return data


class ApiCargarDataPreguntas(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        request_data = json.loads(json_text)

        pk_joven = int(request_data["pk_joven"])
        pk_formulario = int(request_data["pk_formulario"])

        joven = Joven.objects.get(pk=pk_joven)
        formulario = Formularios.objects.get(pk=pk_formulario)

        info_preguntas = []
        try:
            respuestas_formulario = RespuestasJoven.objects.get(joven=joven.pk, formulario=formulario.pk,
                                                                tanda=joven.tanda_formularios.pk)
            if respuestas_formulario.data:
                for pk_pregunta in respuestas_formulario.data:
                    pregunta = Preguntas.objects.get(pk=pk_pregunta)
                    respuesta = respuestas_formulario.data[pk_pregunta]
                    tipo = pregunta.tipo
                    orden = str(pregunta.identificador).split('_')[1]
                    pregunta = {
                        'pk': pk_pregunta,
                        'respuesta': respuesta,
                        'tipo': tipo,
                        'orden': int(orden)
                    }
                    info_preguntas.append(pregunta)
            return Response({'info': info_preguntas,
                             'limit': formulario.tiempo_limite,
                             'time': (timezone.now() - respuestas_formulario.fecha_inicio).total_seconds(),
                             'read': respuestas_formulario.texto_leido})
        except Exception as e:
            return Response({'limit': formulario.tiempo_limite, 'timetoread': formulario.tiempo_lectura})


class ApiGuardarRespuestas(APIView):
    def post(self, request):
        from kairos.formularios.utils import hay_tiempo_disponible

        json_text = request.data.get('json_text')
        request_data = json.loads(json_text)

        formulario_identificador = request_data["formulario_identificador"]
        hora_inicio = str(request_data["hora_inicio"])
        respuestas = request_data["respuestas"]
        tiempo_gastado = request_data["tiempo_gastado"]

        pk_joven = int(request_data["pk_joven"])
        pk_formulario = int(request_data["pk_formulario"])

        joven = Joven.objects.get(pk=pk_joven)
        formulario = Formularios.objects.get(pk=pk_formulario)

        faltan_preguntas_por_responder = True

        if 'hora_inicio' in request.session:
            hora_inicio = datetime.strptime(request.session['hora_inicio'], "%Y-%m-%d %H:%M:%S.%f")
        else:
            hora_inicio = timezone.now()

        respuestas_formulario = RespuestasJoven.objects.filter(joven=joven.pk, formulario=formulario.pk,
                                                               tanda=joven.tanda_formularios.pk).first()
        if respuestas_formulario:
            if respuestas_formulario.fecha_inicio is None:
                respuestas_formulario.fecha_inicio = hora_inicio
                respuestas_formulario.save()

            if respuestas_formulario.completas:
                faltan_preguntas_por_responder = False
        else:
            respuestas_formulario = RespuestasJoven()
            respuestas_formulario.joven = joven
            respuestas_formulario.formulario = formulario
            respuestas_formulario.tanda = joven.tanda_formularios
            respuestas_formulario.fecha_inicio = hora_inicio
            respuestas_formulario.save()

        if faltan_preguntas_por_responder:

            data = {}

            respuestas_totales = formulario.pregunta_del_formulario.filter(activo=True).count()
            for respuesta in respuestas:
                pk_pregunta = int(respuesta['pk_pregunta'])
                respuesta = respuesta['respuesta']
                data[pk_pregunta] = respuesta

            respuestas_formulario.data = data
            finalizo_reto = False
            if len(respuestas) == respuestas_totales:
                respuestas_formulario.completas = True
                finalizo_reto = True

            respuestas_formulario.tiempo_gastado = timedelta(seconds=int(tiempo_gastado))
            if formulario.tiempo_limite:

                if hay_tiempo_disponible(int(formulario.tiempo_limite), hora_inicio) == False:  # tiempo_gastado >= int(formulario.tiempo_limite)*60:
                    respuestas_formulario.completas = True

            respuestas_formulario.save()

            formularios_en_tanda_joven = joven.tanda_formularios.formularios.filter(activo=True)

            formularios_completos = formularios_en_tanda_joven.filter(respuestasjoven__joven=joven, respuestasjoven__completas=True).count()

            if formularios_completos == formularios_en_tanda_joven.count():
                try:
                    registro_tanda = RegistroTandasJoven.objects.get(joven=pk_joven, tanda=joven.tanda_formularios.pk, disponible=True)
                    registro_tanda.disponible = False
                except:
                    registro_tanda = RegistroTandasJoven.objects.create(tanda=joven.tanda_formularios, joven=joven, disponible=True)

                tiempo_total = timedelta(seconds=0)
                todas_las_respuestas = RespuestasJoven.objects.filter(joven=joven, tanda=registro_tanda.tanda)
                for resp in todas_las_respuestas:
                    tiempo_total += resp.tiempo_gastado
                registro_tanda.fecha_terminacion = timezone.now()
                registro_tanda.tiempo_gastado = tiempo_total
                registro_tanda.save()

            if respuestas_formulario.completas:
                calificarFormulario(respuestas_formulario)
            return Response({'result': True, 'finalizo': finalizo_reto})
        else:
            return Response({'result': False})


class ApiGuardarEstadoLectura(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        request_data = json.loads(json_text)
        pk_joven = int(request_data["pk_joven"])
        joven = Joven.objects.get(pk=pk_joven)
        pk_formulario = int(request_data["pk_formulario"])
        formulario = Formularios.objects.get(pk=pk_formulario)
        try:
            respuestas_formulario = RespuestasJoven.objects.get(joven=joven.pk, formulario=formulario.pk,
                                                                tanda=joven.tanda_formularios.pk)
        except:
            respuestas_formulario = RespuestasJoven()
            respuestas_formulario.joven = joven
            respuestas_formulario.formulario = formulario
            respuestas_formulario.tanda = joven.tanda_formularios
        respuestas_formulario.texto_leido = True
        respuestas_formulario.tiempo_gastado = timedelta(seconds=0)
        respuestas_formulario.save()
        return Response({'result': respuestas_formulario.texto_leido})
