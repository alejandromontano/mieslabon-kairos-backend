# Modulos Django
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MinValueValidator, MaxValueValidator

# Modulos de plugin externos
import os

# Modulos de otras apps
from simple_history.models import HistoricalRecords


# Modulos de otras apps

# Modulos internos de la app

def subir_img_formulario(instance, filename):
    ext = filename.split('.')[-1]  # eg: 'jpg'
    renamed_filename = '%(formulario)s_%(id)s.%(ext)s' % {'formulario': instance.nombre, 'id': instance.id, 'ext': ext}
    return os.path.join('formularios/', renamed_filename)


def subir_img_pregunta(instance, filename):
    ext = filename.split('.')[-1]  # eg: 'jpg'
    # eg: 'nombreformulario_idpregunta.jpg'
    renamed_filename = '%(formulario)s_%(id)s.%(ext)s' % {'formulario': instance.formulario.nombre, 'id': instance.id,
                                                          'ext': ext}
    # eg: 'formularios/preguntas/nombreformulario_id.jpg'
    return os.path.join('formularios/preguntas/', renamed_filename)


def subir_img_respuesta(instance, filename):
    ext = filename.split('.')[-1]  # eg: 'jpg'
    # eg: 'nombreformulario_idrespuesta.jpg'
    renamed_filename = '%(formulario)s_%(id)s.%(ext)s' % {'formulario': instance.formulario.nombre, 'id': instance.id,
                                                          'ext': ext}
    # eg: 'iconos_sistema/nombre.png'
    return os.path.join('formularios/respuestas/', renamed_filename)


def subir_competencia(instance, filename):
    return "competencias/{}".format(
        filename.encode('ascii', 'ignore'),
    )


class Competencias(models.Model):
    CODIGO_CALIFICACIONES = (
        ('Números', 'Números'),
        ('Lectura', 'Lectura'),
        ('Negociación', 'Negociación'),
        ('Autoconciencia', 'Autoconciencia'),
        ('Empatía', 'Autoconciencia'),
        ('Autocontrol', 'Autocontrol'),
        ('Autoconfianza', 'Autoconfianza'),
        ('Dirección', 'Dirección'),
        ('Carisma', 'Carisma'),
        ('Innovación', 'Innovación'),
        ('Desarrollo', 'Desarrollo'),
        ('Motivación', 'Motivación'),
        ('Inspiración', 'Inspiración'),
        ('Socializar', 'Socializar'),
        ('Liderazgo_laizzes_faire', 'Liderazgo Laizzes Faire'),
        ('Liderazgo_transaccional', 'Liderazgo Transaccional'),
        ('Liderazgo_transformacional', 'Liderazgo Transformacional'),
    )

    nombre = models.CharField(max_length=100, verbose_name="nombre de la competencia")
    explicacion = models.TextField(verbose_name="definición y explicación de niveles")
    necesidad = models.TextField(verbose_name="¿Para qué es necesario en la vida?")
    desarrollo = models.TextField(verbose_name="¿Cómo se puede desarrollar?")
    activo = models.BooleanField(verbose_name="¿La competencia se encuentra activa?", default=True)
    imagen = models.FileField(upload_to=subir_competencia, blank=True, null=True)
    codigo_calificacion = models.CharField(max_length=30, choices=CODIGO_CALIFICACIONES, blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    @staticmethod
    def buscar(id_competencia) -> 'Competencias':
        '''
            Función que busca una competencia por id_competencia y retorna
            la competencia.
        '''
        competencia = Competencias.objects.get(id=id_competencia)
        return competencia

    def crear_competencia_visitada_joven(self, id_usuario: int):
        '''
            Función que crea la competencia visitada para el joven
        '''
        CompetenciaVisitada.objects.get_or_create(
            usuario_id=id_usuario,
            competencia_id=self.pk
        )

    def calificar_competencia_joven(self, id_usuario: str):
        '''
            Función que califica la competencia como visitada
        '''
        try:
            competencia_visitada = CompetenciaVisitada.objects.get(
                competencia_id=self.id,
                usuario_id=id_usuario
            )
            if competencia_visitada.visitado == 0:
                competencia_visitada.visitado = 1
                competencia_visitada.save()
        except CompetenciaVisitada.DoesNotExist:
            return None


class Ambito(models.Model):
    nombre = models.CharField(max_length=50, verbose_name="nombre del ámbito")
    descripcion = models.CharField(max_length=300, verbose_name="descripción")
    activo = models.BooleanField(verbose_name="¿El ámbito se encuentra activo?", default=True)
    competencias = models.ManyToManyField(Competencias, related_name="ambito_compentecias")
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre


class Formularios(models.Model):
    ambito = models.ForeignKey('Ambito', on_delete=models.CASCADE, verbose_name="Ámbito*")
    nombre = models.CharField(max_length=50, verbose_name="nombre del formulario*")
    identificador = models.PositiveIntegerField(verbose_name='identificador*')
    TIPOS_DE_PREGUNTA = (
        ("SM", "Selección Múltiple"),
        ("E", "Escala de Valoración Numérica"),
        ("SN", "Si/No"),
        ("D48", "D48 - Dominó"),
        ("T", "Texto Libre")
    )
    tipo_preguntas = models.CharField(max_length=100, choices=TIPOS_DE_PREGUNTA,
                                      verbose_name='tipo de preguntas', default="SM")
    descripcion = models.TextField(verbose_name="insumo", blank=True, null=True)
    instrucciones = models.TextField(verbose_name="indicaciones*")
    imagen = models.FileField(upload_to=subir_img_formulario, blank=True, null=True, verbose_name="imagen instructiva")
    escala_max = models.IntegerField(validators=[MinValueValidator(2), MaxValueValidator(6)],
                                     verbose_name="escala máxima (si aplica)", null=True, blank=True)
    credito = models.CharField(max_length=200, verbose_name='crédito', null=True, blank=True)
    tiempo_lectura = models.PositiveIntegerField(validators=[MinValueValidator(1)],
                                                 verbose_name="tiempo límite para lectura del texto (minutos)",
                                                 null=True, blank=True)
    tiempo_limite = models.PositiveIntegerField(validators=[MinValueValidator(1)],
                                                verbose_name="tiempo límite para su solución (minutos)", null=True,
                                                blank=True)
    releer = models.BooleanField(verbose_name="¿El joven puede volver al texto descriptivo?", default=False)
    activo = models.BooleanField(verbose_name="¿El formulario se encuentra activo?", default=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    @staticmethod
    def cantidad_formularios_activos(formularios: 'QuerySet<Formularios>', joven: 'Joven') -> 'int':
        from django.db.models import Q
        '''
            Filtra por respuestasjoven__joven y/o respuestasjoven__completas  y retorna la cantidad total.
            Parametros:
                formularios (QuerySet<Formularios>): Formularios  del Joven.
                joven (Joven): Joven dueño de los formularios.
            Retorno:
                Cantidad (int): de cantidad de Formularios filtrados.
        '''
        cantidad_formularios = formularios.filter(~Q(respuestasjoven__joven=joven.id) |
                                                  Q(respuestasjoven__joven=joven.id,
                                                    respuestasjoven__completas=False)).order_by(
            'pk').distinct().count()
        return cantidad_formularios

    class Meta:
        ordering = ['identificador']

    # def formulario_completo(self, joven):
    #     try:
    #         RespuestasJoven.objects.get(joven=joven.pk, tanda=joven.tanda_formularios.pk, completas=True)
    #         return True
    #     except:
    #         return False


class Preguntas(models.Model):
    formulario = models.ForeignKey(Formularios, related_name="pregunta_del_formulario", on_delete=models.CASCADE,
                                   verbose_name="formulario al que pertenece*")
    competencia = models.ForeignKey(Competencias, on_delete=models.CASCADE,
                                    verbose_name="competencia que mide la pregunta", null=True, blank=False)
    identificador = models.CharField(max_length=20, verbose_name="identificador de la pregunta", null=True, blank=True,
                                     unique=True)  # idformulario_numpregunta
    TIPOS_DE_PREGUNTA = (
        ("SM", "Selección Múltiple"),
        ("E", "Escala de Valoración Numérica"),
        ("T", "Texto Libre"),
        ("SN", "Si/No"),
    )
    tipo = models.CharField(max_length=100, choices=TIPOS_DE_PREGUNTA,
                            verbose_name='tipo de pregunta', default="SM")
    descripcion = models.CharField(max_length=300, verbose_name="descripción")
    imagen = models.FileField(upload_to=subir_img_pregunta, blank=True, null=True)
    activo = models.BooleanField(verbose_name="¿La pregunta se encuentra activa?", default=True)
    history = HistoricalRecords()

    def get_correctas(self):
        respuestas = Respuestas.objects.filter(pregunta=self.id, correcta=True)
        return respuestas

    def __str__(self):
        return self.descripcion


class Respuestas(models.Model):
    pregunta = models.ForeignKey(Preguntas, related_name="respuesta_pregunta", on_delete=models.CASCADE,
                                 verbose_name="pregunta a la que pertenece*")
    descripcion = models.CharField(max_length=300, verbose_name="descripción")
    correcta = models.BooleanField(verbose_name="¿la respuesta es correcta?", default=False)
    imagen = models.FileField(upload_to=subir_img_respuesta, blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.descripcion


class SeccionVisitada(models.Model):
    nombre = models.CharField(max_length=40, verbose_name="nombre de la sección")
    usuario = models.ForeignKey('usuarios.Usuario', on_delete=models.CASCADE, verbose_name='usuario que califica',
                                related_name='usuario_visitador_seccion')
    visitado = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                           verbose_name='calificación: 1 visitado, 0 no visitado')
    history = HistoricalRecords()

    @staticmethod
    def crear_seccion_joven(id_usuario: str):
        '''
            Función que crea las 4 secciones visitadas para el joven
        '''
        secciones = [
            "NIVEL DE CUALIFICACIÓN",
            "HABILIDADES SOCIALES",
            "ÁREA Y NIVEL DE CONOCIMIENTO",
            "EXPERIENCIA"
        ]
        for seccion in secciones:
            SeccionVisitada.objects.get_or_create(
                nombre=seccion,
                usuario_id=id_usuario
            )

    @staticmethod
    def calificar_seccion_joven(id_usuario: str, nombre: str):
        '''
            Función que califica la sección como visitada
        '''
        seccion = SeccionVisitada.objects.get(
            nombre=nombre,
            usuario_id=id_usuario
        )
        if seccion.visitado == 0:
            seccion.visitado = 1
            seccion.save()

    @staticmethod
    def obtener_todos(id_joven: str) -> 'Queryset<SeccionVisitada>':
        '''
            Función que retorna todas las Secciones del joven
            Queryset<SeccionVisitada>: Queryset<Habilidad>
        '''
        secciones_visitada = SeccionVisitada.objects.filter(usuario_id=id_joven)
        return secciones_visitada

    @staticmethod
    def obtener_visitados(id_joven: str) -> 'Queryset<SeccionVisitada>':
        '''
            Función que retorna todas las secciones visitadas
            por el joven
            Queryset<SeccionVisitada>: Queryset<SeccionVisitada>
        '''
        secciones_visitada = SeccionVisitada.obtener_todos(id_joven).filter(visitado=1)
        return secciones_visitada


class CompetenciaVisitada(models.Model):
    competencia = models.ForeignKey('Competencias', on_delete=models.CASCADE, verbose_name='competencia calificado',
                                    related_name='competencia_visitada_del_usuario')
    usuario = models.ForeignKey('usuarios.Usuario', on_delete=models.CASCADE, verbose_name='usuario que califica',
                                related_name='usuario_visitador_competencia')
    visitado = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                           verbose_name='calificación: 1 visitado, 0 no visitado')
    history = HistoricalRecords()

    @staticmethod
    def obtener_todos(id_joven: str) -> 'Queryset<CompetenciaVisitada>':
        '''
            Función que retorna todas las competencias del joven
            Queryset<SeccionVisitada>: Queryset<Habilidad>
        '''
        competencias_visitadas = CompetenciaVisitada.objects.filter(usuario_id=id_joven)
        return competencias_visitadas

    @staticmethod
    def obtener_visitados(id_joven: str) -> 'Queryset<CompetenciaVisitada>':
        '''
            Función que retorna todas las competencias visitadas
            por el joven
            Queryset<SeccionVisitada>: Queryset<SeccionVisitada>
        '''
        competencias_visitadas = CompetenciaVisitada.obtener_todos(id_joven).filter(visitado=1)
        return competencias_visitadas


@receiver(post_save, sender=Respuestas, dispatch_uid="minificar_imagen_respuestas")
def comprimir_imagen_respuestas(sender, **kwargs):
    from kairos.core.utils import comprimir_imagen
    if kwargs["instance"].imagen:
        comprimir_imagen(kwargs["instance"].imagen.path)


@receiver(post_save, sender=Competencias, dispatch_uid="minificar_imagen_competencias")
def comprimir_imagen_competencias(sender, **kwargs):
    from kairos.core.utils import comprimir_imagen
    if kwargs["instance"].imagen:
        comprimir_imagen(kwargs["instance"].imagen.path)


@receiver(post_save, sender=Preguntas, dispatch_uid="minificar_imagen_preguntas")
def comprimir_imagen_preguntas(sender, **kwargs):
    from kairos.core.utils import comprimir_imagen
    if kwargs["instance"].imagen:
        comprimir_imagen(kwargs["instance"].imagen.path)


@receiver(post_save, sender=Formularios, dispatch_uid="minificar_imagen_formularios")
def comprimir_imagen_formularios(sender, **kwargs):
    from kairos.core.utils import comprimir_imagen
    if kwargs["instance"].imagen:
        comprimir_imagen(kwargs["instance"].imagen.path)
