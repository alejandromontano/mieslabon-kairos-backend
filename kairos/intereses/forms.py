
# Modulos Django
from django import forms

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .models import Intereses

class InteresesModelForm(forms.ModelForm):
    class Meta:
        model = Intereses
        fields = ['nombre', 'descripcion', 'activo']

        widgets = {
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            )
        }