# Modulos Django
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .forms import InteresesModelForm
from .models import Intereses

class InteresesListado(LoginRequiredMixin, ListView):
    model = Intereses
    context_object_name = "intereses"
    template_name = "intereses/listado.html"

    def get_queryset(self):
        intereses = Intereses.objects.all()
        return intereses

class InteresesConsultar(LoginRequiredMixin, ListView):
    model = Intereses
    context_object_name = "intereses"
    template_name = "intereses/consultar.html"

    def get_queryset(self):
        intereses = Intereses.objects.all()
        return intereses

class InteresesRegistrar(LoginRequiredMixin, CreateView):
    model = Intereses
    form_class = InteresesModelForm
    template_name = "intereses/registrar.html"
    success_url = reverse_lazy('intereses:listado')

    def form_valid(self, form):
        interes = form.save(commit=False)
        interes.activo = True
        form.save()
        messages.success(self.request, "Interés registrado satisfactoriamente")
        return super(InteresesRegistrar, self).form_valid(form)

class InteresModificar(LoginRequiredMixin, UpdateView):
    model = Intereses
    form_class = InteresesModelForm
    pk_url_kwarg = 'id_interes'
    template_name = "intereses/modificar.html"
    success_url = reverse_lazy('intereses:listado')

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Interés modificado satisfactoriamente")
        return super(InteresModificar, self).form_valid(form)

