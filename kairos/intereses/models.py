# Modulos Django
from django.db import models

# Modulos de plugin externos
from simple_history.models import HistoricalRecords

# Modulos de otras apps
# Modulos internos de la app

class Intereses(models.Model):
    nombre = models.CharField(max_length=50, verbose_name='nombre del interés*')
    descripcion = models.TextField(verbose_name='descripción*')
    activo = models.BooleanField(verbose_name="¿El interés se encuentra activo?", default=True)
    history = HistoricalRecords()
