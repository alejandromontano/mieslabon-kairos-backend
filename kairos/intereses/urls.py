
# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from . import views

from .views import (
    InteresesListado,
    InteresesConsultar,
    InteresesRegistrar,
    InteresModificar
)
app_name = "intereses"

urlpatterns = [
    path('gestionar/', InteresesListado.as_view(), name='listado'),
    path('consultar/', InteresesConsultar.as_view(), name='consultar'),
    path('registrar', InteresesRegistrar.as_view(), name='registrar'),
    path('modificar/<int:id_interes>', InteresModificar.as_view(), name='modificar'),   
]