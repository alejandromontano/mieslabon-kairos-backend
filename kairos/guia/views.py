# Modulos Django
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView

# Modulos de plugin externos
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView
import json

# Modulos de otras apps
from kairos.usuarios.models import Joven
from kairos.guia.forms import RecursoForm

# Modulos internos de la app
from .models import *
from .serializers import RecursoSerializer, RecursosVistosSerializer

class ListadoRecursos(LoginRequiredMixin, ListView):
    model = Recurso
    context_object_name = "recursos"
    template_name = "guia/listado.html"

    def get_queryset(self):
        recursos = Recurso.objects.all()
        return recursos

class RecursoRegistrar(LoginRequiredMixin, CreateView):
    model = Recurso
    form_class = RecursoForm
    template_name = "guia/registrar.html"

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Recurso registrado correctamente")
        return redirect('guia:registrar')

class ModificarRecurso(LoginRequiredMixin, UpdateView):
    model = Recurso
    pk_url_kwarg = 'id_recurso'
    form_class = RecursoForm
    template_name = "guia/modificar.html"

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Recurso modificado satisfactoriamente")
        return redirect("guia:listado")

class ListadoTipoRecursos(LoginRequiredMixin, ListView):
    model = TipoRecurso
    context_object_name = "tipos"
    template_name = "guia/tipos/listado.html"

    def get_queryset(self):
        recursos = TipoRecurso.objects.all()
        return recursos

class TipoRecursoRegistrar(LoginRequiredMixin, CreateView):
    model = TipoRecurso
    fields = '__all__'
    template_name = "guia/tipos/registrar.html"
    success_url = reverse_lazy('guia:listado_tipo')

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Tipo de recurso registrado correctamente")
        return super(TipoRecursoRegistrar, self).form_valid(form)

class ModificarTipoRecurso(LoginRequiredMixin, UpdateView):
    model = TipoRecurso
    pk_url_kwarg = 'id_tipo_recurso'
    fields = '__all__'
    template_name = "guia/tipos/modificar.html"
    success_url = reverse_lazy('guia:listado_tipo')

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Tipo de recurso modificado satisfactoriamente")
        return super(ModificarTipoRecurso, self).form_valid(form)

class ConsultarJovenesGuia(LoginRequiredMixin, ListView):
    template_name = "guia/jovenes/consultar_guia.html"
    paginate_by = 12

    def get_queryset(self):
        consulta = self.obtener_consulta()
        return consulta['recursos']

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator
        context = super(ConsultarJovenesGuia, self).get_context_data(**kwargs)
        consulta = self.obtener_consulta()

        pagination = Paginator(list(consulta['recursos']), self.paginate_by)

        context['recursos'] = pagination.page(context['page_obj'].number)
        context['tipos'] = TipoRecurso.obtener_tipos_principales()

        context['tag'] = consulta['tag']
        return context

    def obtener_consulta(self):
        tag = 'Todos'
        recursos = []
        palabra_busqueda = False

        
        if 'tag' in self.request.GET:
            tag = self.request.GET['tag']
        else:
            tag = "Todos"
        
        if 'q' in self.request.GET:
            if len(self.request.GET['qu'].split("'")) > 1:
                tag = self.request.GET['qu'].split("'")[3]

            palabra_busqueda = self.request.GET['q']
        else:
            palabra_busqueda = False

        if tag == 'Todos':
            if palabra_busqueda:
                recursos = Recurso.buscador_por_palabra(palabra=palabra_busqueda)
            else:
                recursos = Recurso.obtener_recursos_tipo(tag)
        else:
            if palabra_busqueda:
                recursos = Recurso.buscador_por_palabra(palabra=palabra_busqueda, tipo=tag)
            else:
                recursos = Recurso.obtener_recursos_tipo(tag)
        
        return {'tag': tag, 'recursos': recursos} 

class APIRecursosRealidad(LoginRequiredMixin, APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request, format=None):
        realidad = request.GET.get("realidad")
        poder = request.GET.get("poder")
        recursos = Recurso.obtener_recursos_realidad_poder(realidad, poder)
        joven = Joven.buscar(request.user.id)

        for recurso in recursos:
            joven.crear_ver_recurso_si_no_existe(recurso=recurso)

        recursos_vistos = Recurso.obtener_recursos_realidad_vistos(realidad, poder, joven.id)

        serializer = RecursoSerializer(recursos, many=True)
        serializer2 = RecursosVistosSerializer(recursos_vistos, many=True)
        return Response({
            "recursos": {
                "total": recursos.count(),
                "recursos": serializer.data,
            },
            "recursos_vistos":  {
                "total": len(recursos_vistos),
                "recursos": serializer2.data,
            },
        })


class ApiCargarCalificaciones(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_usuario = self.request.user.pk
        calificaciones = CalificacionRecurso.objects.filter(usuario=pk_usuario)
        cals = []
        for c in calificaciones:
            cals.append({'pk': c.recurso.pk, 'stars': c.calificacion})
        return Response({'calificaciones': cals})


class ApiCalificar(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_recurso = data["pk_recurso"]
        pk_usuario = self.request.user.pk
        if "star" in data:
            calificacion, creado = CalificacionRecurso.objects.get_or_create(
                recurso_id=pk_recurso, usuario_id=pk_usuario
            )
            calificacion.calificacion = data['star']
            calificacion.save()
        return Response({})