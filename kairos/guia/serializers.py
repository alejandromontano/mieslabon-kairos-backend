from django.db.models import fields
from rest_framework import serializers

from kairos.guia.models import Recurso


class RecursoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recurso
        fields = ['id', 'nombre', 'descripcion', 'link_video', 'orden']


class RecursosVistosSerializer(serializers.ModelSerializer):

    class Meta:
        model = Recurso
        fields = ['id']
