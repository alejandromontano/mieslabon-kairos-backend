# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from . import views

app_name = "guia"

urlpatterns = [
    path('listado', views.ListadoRecursos.as_view(), name='listado'),
    path('registrar', views.RecursoRegistrar.as_view(), name='registrar'),
    path('modificar/<int:id_recurso>', views.ModificarRecurso.as_view(), name='modificar'),

    path('tipos/gestionar/',  views.ListadoTipoRecursos.as_view(), name='listado_tipo'),
    path('tipos/registrar',  views.TipoRecursoRegistrar.as_view(), name='registrar_tipo'),
    path('tipos/modificar/<int:id_tipo_recurso>',  views.ModificarTipoRecurso.as_view(), name='modificar_tipo'),

    path('guia-eslabon', views.ConsultarJovenesGuia.as_view(), name='consultar_guia_jovenes'),
    path("api/cargar-calificaciones", views.ApiCargarCalificaciones.as_view(), name="api_cargar_calificaciones"),
    path("api/calificar", views.ApiCalificar.as_view(), name="api_calificar"),
    path("api/realidad", views.APIRecursosRealidad.as_view(), name="api_realidad"),
]