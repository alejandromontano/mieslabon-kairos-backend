# Modulos Django
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models import Q

# Modulos de plugin externos
from simple_history.models import HistoricalRecords

# Modulos de otras apps

# Modulos internos de la app

class TipoRecurso(models.Model):
    nombre = models.CharField(max_length=50, verbose_name='nombre del tipo*')
    descripcion = models.CharField(max_length=300, verbose_name="descripción*")
    activo = models.BooleanField(verbose_name='¿El tipo de recurso se encuentra activo?', default=True)
    es_realidad = models.BooleanField(verbose_name='¿El tipo de recurso es una realidad?', default=False)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre
    
    def obtener_tipos_principales() -> 'TipoRecurso':
        """Obtiene todos los tipos de recursos que son realidades registrados"""
        tipos = TipoRecurso.objects.filter(es_realidad=True, activo=True)
        return tipos


class Recurso(models.Model):
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)
    tipo = models.ManyToManyField(TipoRecurso, blank=True, related_name='tipo')
    nombre = models.CharField(max_length=50, verbose_name="nombre del recurso*")
    descripcion = models.TextField(verbose_name="descripción*")
    link_video = models.CharField(max_length=100, verbose_name="enlace a VIDEO (url)*", blank=True)
    activo = models.BooleanField(verbose_name="¿El recurso se encuentra activo?", default=True)
    joven = models.ManyToManyField('usuarios.Joven', through='VerRecurso', related_name="recursos_jovenes", blank=True)
    orden = models.IntegerField(verbose_name="Orden con respecto a los otros videos de la realidad", blank=True,
                                default=0)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    def buscar_nombre(nombre:str) -> 'Recurso':
        '''
            Retorna un Recurso perteneciente al nombre

            Parámetros:
                nombre: nombre del recurso que se buscará

            Retorno:
                Si existe: Recurso
                Si no existe: None
        '''
        try:
            recurso = Recurso.objects.get(nombre=nombre)
            return recurso
        except Recurso.DoesNotExist:
            return None

    @staticmethod
    def obtener_recursos_tipo(nombre_tipo: str) -> 'Recurso':
        '''
            Retorna los Recursos pertenecientes al tipo de curso del id

            Parámetros:
                id_tipo: id del tipo de recurso

            Retorno:
                Queryset de tipo Recurso
        '''
        if nombre_tipo == 'Todos':
            recursos = Recurso.objects.filter(activo=True)
        else:
            recursos = Recurso.objects.filter(tipo__nombre=nombre_tipo, activo=True)

        return recursos

    @staticmethod
    def obtener_recursos_realidad_poder(realidad: str, poder: str) -> 'Recurso':
        '''
            Retorna los Recursos pertenecientes al tipo de curso del id

            Parámetros:
                id_tipo: id del tipo de recurso

            Retorno:
                Queryset de tipo Recurso
        '''
        recursos = Recurso.obtener_recursos_tipo(poder).filter(tipo__nombre=realidad).order_by("orden")
        return recursos

    @staticmethod
    def obtener_total_recursos_realidad(realidad: str) -> int:
        '''
            Retorna los Recursos pertenecientes al tipo de curso del id

            Parámetros:
                id_tipo: id del tipo de recurso

            Retorno:
                Queryset de tipo Recurso
        '''
        recursos = Recurso.objects.filter(tipo__nombre=realidad).count()
        return recursos

    @staticmethod
    def obtener_recursos_realidad_vistos(realidad: str, poder: str, joven_id) -> 'list<Recurso>':
        '''
            Retorna los Recursos pertenecientes al tipo de curso del id

            Parámetros:
                id_tipo: id del tipo de recurso

            Retorno:
                Queryset de tipo Recurso
        '''
        recursos = Recurso.obtener_recursos_realidad_poder(realidad=realidad, poder=poder)
        vistos = []
        for recurso in recursos:
            ver = VerRecurso.objects.filter(recurso_id=recurso.id, usuario_id=joven_id, observado=1)
            if ver:
                vistos.append(recurso)
        return vistos

    @staticmethod
    def buscador_por_palabra(palabra: str, tipo=None) -> 'Queryset<Recurso>':
        '''
            Filtra y retorna de un grupo especifico de Recursos que posean algun elemento definido con la palabra y opcionalmente un tipo.

            Parámetros:
                palabra: String que define el filtro.
                tipo: String que difine el nombre de un tipo de recurso 

            Retorno:
                Queryset de Recurso filtradas por el parametro palabra
        '''
        if tipo != None:
            recursos_por_tipo = Recurso.obtener_recursos_tipo(tipo)
            recursos = recursos_por_tipo.filter(
                Q(nombre__icontains=palabra) | Q(descripcion__icontains=palabra) | Q(tipo__nombre__icontains=palabra)).distinct()
        else:
            recursos = Recurso.objects.filter(
                Q(nombre__icontains=palabra, activo=True) | Q(descripcion__icontains=palabra, activo=True) | Q(tipo__nombre__icontains=palabra, activo=True)).distinct()

        return recursos

    class Meta:
        ordering = ['nombre']

class CalificacionRecurso(models.Model):
    recurso = models.ForeignKey(Recurso,on_delete=models.CASCADE, verbose_name='recurso a calificar',
                                related_name='recurso_calificado')
    usuario = models.ForeignKey('usuarios.Usuario',on_delete=models.CASCADE, verbose_name='usuario que califica',
                                related_name='usuario_calificador_recurso')
    calificacion = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(5),MinValueValidator(0)],
                                               verbose_name='calificación de 1 a 5')
    history = HistoricalRecords()

class VerRecurso(models.Model):
    recurso = models.ForeignKey(Recurso,on_delete=models.CASCADE, verbose_name='recurso a visualizar',
                                related_name='recurso_visualizado')
    usuario = models.ForeignKey('usuarios.Joven',on_delete=models.CASCADE, verbose_name='usuario que ve el video',
                                related_name='usuario_que_ve_el_video')
    observado = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(1),MinValueValidator(0)])

    @staticmethod
    def cambiar_estado(id_video, valor_del_video, usuario_id):
        video_actual = VerRecurso.objects.get(recurso_id=id_video, usuario_id=usuario_id)
        video_actual.observado = valor_del_video
        video_actual.save()

    @staticmethod
    def crear_ver_recurso(recurso:Recurso, joven: 'Joven') -> 'VerRecurso':
        '''
            Crea un objeto VerRecurso con el recurso y usuario

            Parámetros:
                recurso: objeto de tipo Recurso
                usuario: objeto de tipo Joven
        '''
        ver_recurso = VerRecurso.objects.create(recurso=recurso, usuario=joven, observado=0)
        return ver_recurso
