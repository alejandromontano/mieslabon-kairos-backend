# Modulos Django
from django import forms
from django.core.exceptions import ValidationError

# Modulos de plugin externos
from django_select2.forms import Select2MultipleWidget
# Modulos de otras apps

# Modulos internos de la app
from .models import Recurso

class RecursoForm(forms.ModelForm):

    class Meta:
        model = Recurso
        fields = ('nombre', 'descripcion', 'tipo', 'orden', 'link_video', 'activo')

        widgets = {
            'descripcion': forms.Textarea(attrs={
                'rows': 3,
                'cols': 22,
                'style': 'resize:none;'
            }),
            'tipo': Select2MultipleWidget(), 
        }
