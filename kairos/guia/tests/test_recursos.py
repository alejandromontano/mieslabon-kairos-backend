from kairos.guia.models import TipoRecurso, Recurso

from django.test import TestCase
        
class RecursosTestCase(TestCase):

    def test_obtener_recursos_tipo(self):
        """Permite probar el metodo de obtener recursos por tipo"""
        tipo_recurso = TipoRecurso.objects.create(
            nombre="Conectarte", descripcion="una descripcion", activo=True
        )
        recurso = Recurso.objects.create(
            nombre="Video para conectarte", descripcion="una descripcion", activo=True, link_video="un link"
        )

        recurso2 = Recurso.objects.create(
            nombre="Video para conectarte 2", descripcion="una descripcion", activo=True, link_video="un link"
        )

        recurso.tipo.add(tipo_recurso)
        recurso2.tipo.add(tipo_recurso)

        self.assertEqual(list(Recurso.obtener_recursos_tipo('Conectarte')), [recurso, recurso2])


    def test_obtener_recursos_tipo_vacio(self):
        """Permite probar el metodo de obtener recursos por tipo cuando no hay recursos de este tipo"""
        tipo_recurso = TipoRecurso.objects.create(
            nombre="Conectarte", descripcion="una descripcion", activo=True
        )
        recurso = Recurso.objects.create(
            nombre="Video para conectarte", descripcion="una descripcion", activo=True, link_video="un link"
        )

        recurso2 = Recurso.objects.create(
            nombre="Video para conectarte 2", descripcion="una descripcion", activo=True, link_video="un link"
        )

        recurso.tipo.add(tipo_recurso)
        recurso2.tipo.add(tipo_recurso)

        self.assertEqual(list(Recurso.obtener_recursos_tipo('Proyectarte')), [])
    
    def test_buscador_por_palabra(self):
        """Permite probar el metodo de buscador por palabra"""
        tipo_recurso = TipoRecurso.objects.create(
            nombre="Conectarte", descripcion="una descripcion", activo=True
        )
        recurso = Recurso.objects.create(
            nombre="Video para conectarte", descripcion="una descripcion", activo=True, link_video="un link"
        )

        recurso2 = Recurso.objects.create(
            nombre="Video para conocer", descripcion="una descripcion", activo=True, link_video="un link"
        )

        recurso.tipo.add(tipo_recurso)
        recurso2.tipo.add(tipo_recurso)

        self.assertEqual(list(Recurso.buscador_por_palabra('conectarte')), [recurso, recurso2])
    
    def test_buscador_por_palabra_vacio(self):
        """Permite probar el metodo de buscador por palabra cuando no encuentra ninguna coincidencia"""
        tipo_recurso = TipoRecurso.objects.create(
            nombre="Conectarte", descripcion="una descripcion", activo=True
        )
        recurso = Recurso.objects.create(
            nombre="Video para conectarte", descripcion="una descripcion", activo=True, link_video="un link"
        )

        recurso2 = Recurso.objects.create(
            nombre="Video para conectarte 2", descripcion="una descripcion", activo=True, link_video="un link"
        )

        recurso.tipo.add(tipo_recurso)
        recurso2.tipo.add(tipo_recurso)

        self.assertEqual(list(Recurso.buscador_por_palabra('proyectarte')), [])

        