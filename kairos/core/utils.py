
from kairos.insignias.models import PalabraDeLimpieza
from django.db.models import Func
import string
import re

def ordenar_dicionario_por_valores(diccionario):
    return list(reversed(sorted(diccionario.items(), key=lambda x: x[1])))

def ordenar_lista_diccionarios(lista, valor):
    return sorted(lista, key=lambda i: i[valor])

def ordenar_lista_diccionario_patron_personalizado(lista, orden, llave_orden='category'):
    return sorted(lista, key=lambda v: orden.index(v[llave_orden]))

def eliminar_elemento_diccionario_si_valor_cero(llaves_buscar, lista):
    for elemento in lista:
        for desempenio in llaves_buscar:
            if elemento[desempenio] == 0:
                del elemento[desempenio]

def obtener_lista_usuario_caracteristicas(usuario_actual, usuarios):
    if usuario_actual.__class__.__name__ == 'Joven':
        return configuracion_caracteristicas_como_joven(usuario_actual, usuarios)
    elif usuario_actual.__class__.__name__ == 'Organizacion':
        return configuracion_caracteristicas_como_organizacion(usuario_actual, usuarios)
    elif usuario_actual.__class__.__name__ == 'Equipo':
        return configuracion_caracteristicas_como_equipo(usuario_actual, usuarios)
    else:
        return []

def configuracion_caracteristicas_como_joven(usuario_actual, usuarios_relacion):
    lista_usuarios = []
    for usuario in usuarios_relacion:
        lista_usuarios.append(relacion_con_usuario(usuario_actual, usuario))
    return lista_usuarios

def configuracion_caracteristicas_como_organizacion(usuario_actual, usuarios_relacion):
    lista_usuarios = []
    for usuario in usuarios_relacion:
        lista_usuarios.append(relacion_con_usuario(usuario_actual, usuario))

    return lista_usuarios

def configuracion_caracteristicas_como_equipo(joven_actual, equipos):
    lista_usuarios = []
    for equipo in equipos:
        lista_usuarios.append(relacion_con_usuario(joven_actual, equipo))
    return lista_usuarios

def relacion_con_usuario(usuario_actual, usuario_relacion, estado=0):
    from kairos.equipos.models import Equipo

    if usuario_actual.__class__.__name__ == 'Joven' and usuario_relacion.__class__.__name__ == 'Joven':
        return relacion_joven_con_joven(usuario_actual, usuario_relacion, estado)
    elif usuario_actual.__class__.__name__ == 'Organizacion' and usuario_relacion.__class__.__name__ == 'Joven':
        return relacion_organizacion_con_joven(usuario_actual, usuario_relacion, estado)
    elif usuario_actual.__class__.__name__ == 'Joven' and usuario_relacion.__class__.__name__ == 'Organizacion':
        if Equipo.objects.filter(lider=usuario_actual).count() > 0:
            return relacion_lider_con_organizacion(usuario_actual, usuario_relacion, estado)
        else:
            return relacion_joven_con_organizacion(usuario_actual, usuario_relacion, estado)
    elif usuario_actual.__class__.__name__ == 'Joven' and usuario_relacion.__class__.__name__ == 'Retos':
        if 'Equipo' in usuario_relacion.grupos_permitidos:
            return relacion_lider_con_reto(usuario_actual, usuario_relacion, estado)
        else:
            return relacion_joven_con_reto(usuario_actual, usuario_relacion, estado)

    elif usuario_actual.__class__.__name__ == 'Organizacion' and usuario_relacion.__class__.__name__ == 'Organizacion':
        return relacion_organizacion_con_organizacion(usuario_actual, usuario_relacion, estado)
    elif usuario_actual.__class__.__name__ == 'Organizacion' and usuario_relacion.__class__.__name__ == 'Equipo':
        return relacion_organizacion_con_equipo(usuario_actual, usuario_relacion, estado)
    elif usuario_actual.__class__.__name__ == 'Joven' and usuario_relacion.__class__.__name__ == 'Equipo':
        return relacion_joven_con_equipo(usuario_actual, usuario_relacion, estado)
    else:
        return {}

def relacion_lider_con_organizacion(joven_actual, organizacion, estado):
    from kairos.organizaciones.models import Organizacion, SolicitudesOrganizacionJovenes
    context = dict()

    mensajes = mensajes_joven_lider_con_organizacion(context)
    context = relacion_joven_con_organizacion(joven_actual, organizacion, estado)
    context['organizacion'] = organizacion
    context['mensajes'] += (mensajes)
    return context

def relacion_joven_con_joven(joven_actual, joven, estado):
    from kairos.usuarios.models import Joven, SolicitudAmistad
    context = dict()
    solicitud_enviada = SolicitudAmistad.existe_solitud(emisor=joven_actual, receptor=joven)
    solicitud_recibida = SolicitudAmistad.existe_solitud(emisor=joven, receptor=joven_actual)
    context['usuario'] = joven
    context['boton_mensaje'] = 'Ver más'
    context['relacionado'] = joven_actual.es_amigo_de_usuario(joven) or (joven_actual == joven)
    if context['relacionado'] == True:
        context['boton_mensaje'] = 'Somos amigos'
    context['solicitud_enviada'] = solicitud_enviada
    context['solicitud_recibida'] = solicitud_recibida
    context['mensajes'] = mensajes_joven_con_joven(context)
    if estado != 0:
        amigos = obtener_lista_usuario_caracteristicas(joven_actual, joven.obtener_amigos().exclude(id__in=[joven_actual.id, joven.id]))
        for amigo_joven in amigos:
            amigo_joven.update({'mensajes': mensajes_joven_con_joven(amigo_joven)})
        context['amigos'] = amigos
    return context

def relacion_organizacion_con_joven(organizacion_actual, joven, estado):
    from kairos.organizaciones.models import Organizacion, SolicitudesOrganizacionJovenes
    context = dict()
    solicitud_enviada = False
    solicitud_recibida = False
    if SolicitudesOrganizacionJovenes.existe_solitud(organizacion=organizacion_actual, joven=joven):
        if SolicitudesOrganizacionJovenes.es_solicitud_enviada(organizacion=organizacion_actual, joven=joven):
            solicitud_enviada = True
            solicitud_recibida = False
        else:
            solicitud_enviada = False
            solicitud_recibida = True

    context['usuario'] = joven
    context['boton_mensaje'] = 'Ver más'
    context['relacionado'] = organizacion_actual.joven_es_miembro(joven) or (organizacion_actual == joven)
    if context['relacionado'] == True:
        context['boton_mensaje'] = "Es miembro"
    context['relacionado'] = organizacion_actual.joven_es_miembro(joven) or (organizacion_actual == joven)
    context['solicitud_enviada'] = solicitud_enviada
    context['solicitud_recibida'] = solicitud_recibida
    context['mensajes'] = mensajes_organizacion_con_joven(context)  # mensajes_organizacion_con_joven(context)
    if estado != 0:
        amigos = obtener_lista_usuario_caracteristicas(organizacion_actual, joven.obtener_amigos().exclude(id__in=[joven.id]))
        for amigo_joven in amigos:
            amigo_joven.update({'mensajes': mensajes_organizacion_con_joven(amigo_joven)})
        context['amigos'] = amigos
    return context

def relacion_joven_con_organizacion(joven_actual, organizacion, estado):
    from kairos.organizaciones.models import Organizacion, SolicitudesOrganizacionJovenes
    context = dict()
    solicitud_enviada = False
    solicitud_recibida = False
    if SolicitudesOrganizacionJovenes.existe_solitud(organizacion=organizacion, joven=joven_actual):
        if SolicitudesOrganizacionJovenes.es_solicitud_enviada(organizacion=organizacion, joven=joven_actual):
            solicitud_enviada = True
            solicitud_recibida = False
        else:
            solicitud_enviada = False
            solicitud_recibida = True
    context['usuario'] = joven_actual
    context['boton_mensaje'] = 'Ver más'
    context['relacionado'] = organizacion.joven_es_miembro(joven_actual) or (organizacion == joven_actual)
    if context['relacionado'] == True:
        context['boton_mensaje'] = "Eres miembro"

    context['solicitud_enviada'] = solicitud_enviada
    context['solicitud_recibida'] = solicitud_recibida
    context['mensajes'] = mensajes_joven_con_organizacion(context)  # mensajes_organizacion_con_joven(context)
    if estado != 0:
        amigos = obtener_lista_usuario_caracteristicas(organizacion, joven_actual.obtener_amigos().exclude(id__in=[joven_actual.id]))
        for amigo_joven in amigos:
            amigo_joven.update({'mensajes': mensajes_joven_con_organizacion(amigo_joven)})
        context['amigos'] = amigos
    return context

def relacion_joven_con_equipo(joven_actual, equipo, estado):
    from kairos.equipos.models import Equipo, SolicitudesEquipos
    context = dict()
    solicitud_enviada = False
    solicitud_recibida = False
    if SolicitudesEquipos.existe_solitud(equipo=equipo, joven=joven_actual):
        if SolicitudesEquipos.es_solicitud_enviada(equipo=equipo, joven=joven_actual):
            solicitud_enviada = True
            solicitud_recibida = False
        else:
            solicitud_enviada = False
            solicitud_recibida = True

    context['equipo'] = equipo
    context['imagen'] = ''
    context['usuario'] = joven_actual
    context['boton_mensaje'] = 'Ver más'
    context['relacionado'] = equipo.miembro_del_equipo(joven_actual)
    if (context['relacionado'] == True) and not (context['usuario'] == context['equipo'].lider):
        context['boton_mensaje'] = "Eres miembro"
    elif (context['usuario'] == context['equipo'].lider):
        context['boton_mensaje'] = "Eres líder"
    elif (solicitud_enviada == True and solicitud_recibida == False):
        context['boton_mensaje'] = "Me postulé"
    elif (solicitud_enviada == False and solicitud_recibida == True):
        context['boton_mensaje'] = "Únetenos"
    else:
        context['boton_mensaje'] = "Ver más"

    context['solicitud_enviada'] = solicitud_enviada
    context['solicitud_recibida'] = solicitud_recibida
    context['mensajes'] = mensajes_joven_con_equipo(context)    # mensajes_organizacion_con_joven(context)

    if estado != 0:
        amigos = obtener_lista_usuario_caracteristicas(joven_actual, equipo.miembros.exclude(id=joven_actual.id))
        for amigo_joven in amigos:
            amigo_joven.update({'mensajes': mensajes_joven_con_joven(amigo_joven)})
        context['amigos'] = amigos
    return context

def relacion_organizacion_con_organizacion(organizacion_actual, organizacion, estado):
    context = dict()
    context['boton_mensaje'] = ""
    return context

def relacion_organizacion_con_equipo(organizacion_actual, equipo, estado):
    from kairos.organizaciones.models import Organizacion, SolicitudesOrganizacionEquipos
    context = dict()
    solicitud_enviada = False
    solicitud_recibida = False

    if SolicitudesOrganizacionEquipos.existe_solitud(organizacion=organizacion_actual, equipo=equipo):
        if SolicitudesOrganizacionEquipos.es_solicitud_enviada(organizacion=organizacion_actual, equipo=equipo):
            solicitud_enviada = True
            solicitud_recibida = False
        else:
            solicitud_enviada = False
            solicitud_recibida = True

    context['usuario'] = equipo
    context['equipo'] = equipo
    context['boton_mensaje'] = 'Ver más'
    context['relacionado'] = organizacion_actual.equipo_es_miembro(equipo)
    if context['relacionado'] == True:
        context['boton_mensaje'] = "Es miembro"

    context['solicitud_enviada'] = solicitud_enviada
    context['solicitud_recibida'] = solicitud_recibida
    context['mensajes'] = mensajes_organizacion_con_equipo(context)  # mensajes_organizacion_con_joven(context)
    if estado != 0:
        miembros = obtener_lista_usuario_caracteristicas(organizacion_actual, equipo.miembros.all())
        for miebro in miembros:
            miebro.update({'mensajes': mensajes_organizacion_con_equipo(miebro)})
        context['amigos'] = miembros
    return context

def relacion_como_equipo(joven_actual, joven):
    from kairos.usuarios.models import Joven, SolicitudAmistad
    context = dict()
    solicitud_enviada = SolicitudAmistad.existe_solitud(emisor=joven_actual, receptor=joven)
    solicitud_recibida = SolicitudAmistad.existe_solitud(emisor=joven, receptor=joven_actual)
    context['usuario'] = joven
    context['relacionado'] = joven_actual.es_amigo_de_usuario(joven) or (joven_actual == joven)
    context['solicitud_enviada'] = solicitud_enviada
    context['solicitud_recibida'] = solicitud_recibida
    context['mensajes'] = mensajes_joven_con_joven(context)
    return context

def relacion_joven_con_reto(joven_actual, reto, estado):
    from kairos.retos.models import SolicitudesRetosJovenes
    from kairos.equipos.models import Equipo
    from django.db.models import Count
    context = dict()
    solicitud_enviada = False
    solicitud_recibida = False
    if SolicitudesRetosJovenes.existe_solitud(reto=reto, joven=joven_actual):
        if SolicitudesRetosJovenes.es_solicitud_enviada(reto=reto, joven=joven_actual):
            solicitud_enviada = True
            solicitud_recibida = False
        else:
            solicitud_enviada = False
            solicitud_recibida = True
    context['reto'] = reto
    context['mensajes'] = []
    context['relacionado'] = reto.joven_esta_inscrito(joven_actual)
    context['solicitud_enviada'] = solicitud_enviada
    context['solicitud_recibida'] = solicitud_recibida
    if 'Jóvenes' in reto.grupos_permitidos:
        if 'Equipo' in reto.grupos_permitidos:
            context['boton_mensaje'] = 'Inscribir a mi Equipo'
        else:
            context['boton_mensaje'] = 'Inscribirme'

        context['mensajes'] = mensajes_joven_con_reto(context)  # mensajes_organizacion_con_joven(context)
    else:
        context['boton_mensaje'] = 'Reto permitido solo equipos'

    if context['relacionado'] == True:
        context['boton_mensaje'] = "Estás inscrito"

    if estado != 0:
        equipos = obtener_lista_usuario_caracteristicas(joven_actual, reto.obtener_equipos_miembros())
        for equipo in equipos:
            equipo.update({'mensajes': mensajes_joven_con_equipo(equipo)})
        context['equipos'] = equipos
    return context

def relacion_lider_con_reto(joven_actual, reto, estado):
    from kairos.retos.models import SolicitudesRetosJovenes
    context = dict()             
    context = relacion_joven_con_reto(joven_actual, reto, estado)
    mensajes = mensajes_joven_lider_con_reto(context)
    context['reto'] = reto
    context['mensajes'] += (mensajes)
    return context

def mensajes_joven_con_joven(context):
    retorno = []
    if not context['relacionado']:
        if context['solicitud_recibida']:
            retorno.append({'accion': 'Aceptar solicitud de amistad', 'url': 'usuarios:aceptar_solicitud'})
            retorno.append({'accion': 'Rechazar solicitud de amistad', 'url': 'usuarios:rechazar_solicitud'})

        if context['solicitud_enviada']:
            retorno.append({'accion': 'Cancelar solicitud de amistad', 'url': 'usuarios:rechazar_solicitud'})
        else:
            if not context['solicitud_recibida']:
                retorno.append({'accion': 'Enviar solicitud de amistad', 'url': 'usuarios:enviar_solicitud'})
    else:
        retorno.append({'accion': 'Eliminar amigo', 'url': 'usuarios:finalizar_amistad'})

    return retorno

def mensajes_joven_lider_con_organizacion(context):
    retorno = []
    retorno.append({'accion': 'Postular mi equipo', 'url': 'usuarios:enviar_postulacion', 'lider': True})
    return retorno

def mensajes_organizacion_joven(context, usuario):
    if usuario == 'Joven':
        return mensajes_joven_con_organizacion(context)
    elif usuario == 'Organizacion':
        return mensajes_organizacion_con_joven(context)


def mensajes_organizacion_con_joven(context):
    retorno = []
    if not context['relacionado']:
        if context['solicitud_recibida']:
            retorno.append({'accion': 'Aceptar solicitud como miembro', 'url': 'organizaciones:organizacion_jovenes_aceptar_solicitud', 'preguntar': False})
            retorno.append({'accion': 'Rechazar solicitud como miembro', 'url': 'organizaciones:organizacion_jovenes_rechazar_solicitud', 'preguntar': False})

        if context['solicitud_enviada']:
            retorno.append({'accion': 'Cancelar solicitud como miembro', 'url': 'organizaciones:organizacion_jovenes_rechazar_solicitud', 'preguntar': False})
        else:
            if not context['solicitud_recibida']:
                retorno.append({'accion': 'Enviar solicitud como miembro', 'url': 'organizaciones:organizacion_jovenes_enviar_solicitud', 'preguntar': False})
    else:
        retorno.append({'accion': 'Expulsar de la organización', 'url': 'organizaciones:organizacion_jovenes_finalizar_relacion', 'preguntar': False})
    return retorno

def mensajes_joven_con_organizacion(context):
    retorno = []
    if not context['relacionado']:
        if context['solicitud_recibida']:
            retorno.append({'accion': 'Cancelar solicitud como miembro', 'url': 'organizaciones:organizacion_jovenes_rechazar_solicitud', 'preguntar': False})
        if context['solicitud_enviada']:
            retorno.append({'accion': 'Aceptar solicitud como miembro', 'url': 'organizaciones:organizacion_jovenes_aceptar_solicitud', 'preguntar': False})
            retorno.append({'accion': 'Rechazar solicitud como miembro', 'url': 'organizaciones:organizacion_jovenes_rechazar_solicitud', 'preguntar': False})
        else:
            if not context['solicitud_recibida']:
                retorno.append({'accion': 'Enviar solicitud como miembro', 'url': 'organizaciones:organizacion_jovenes_enviar_solicitud', 'preguntar': False})
    else:
        retorno.append({'accion': 'Salir de la organización', 'url': 'organizaciones:organizacion_jovenes_finalizar_relacion', 'preguntar': False})
    return retorno

def mensajes_joven_con_reto(context):
    retorno = []

    if not context['relacionado']:
        if context['solicitud_recibida']:
            retorno.append({'accion': 'Cancelar inscripción', 'url': 'retos:retos_rechazar_solicitud_joven', 'preguntar': False})
        if context['solicitud_enviada']:
            retorno.append({'accion': 'Aceptar solicitud de inscripción', 'url': 'retos:retos_aceptar_solicitud_joven', 'preguntar': False})
            retorno.append({'accion': 'Rechazar solicitud de inscripción', 'url': 'retos:retos_rechazar_solicitud_joven', 'preguntar': False})
        else:
            if not context['solicitud_recibida']:
                retorno.append({'accion': 'Enviar inscripción', 'url': 'retos:retos_enviar_solicitud_joven', 'preguntar': False})
    else:
        retorno.append({'accion': 'Salir del reto', 'url': 'retos:retos_finalizar_solicitud_joven', 'preguntar': False})
    return retorno

def mensajes_joven_lider_con_reto(context):
    retorno = []   
    if not context['relacionado']:
        retorno.append({'accion':'Postular mi equipo', 'url':'retos:retos_enviar_solicitud_equipo', 'lider':True})
    return retorno

def mensajes_organizacion_con_equipo(context):
    retorno = []

    if not context['relacionado']:
        if context['solicitud_recibida']:
            retorno.append({'accion': 'Aceptar solicitud como miembro', 'url': 'organizaciones:organizacion_equipos_aceptar_solicitud', 'preguntar': False})
            retorno.append({'accion': 'Rechazar solicitud como miembro', 'url': 'organizaciones:organizacion_equipos_rechazar_solicitud', 'preguntar': False})
            context['imagen'] = 'img/estado_equipo/Unetenos.png'

        if context['solicitud_enviada']:
            retorno.append({'accion': 'Cancelar solicitud como miembro', 'url': 'organizaciones:organizacion_equipos_rechazar_solicitud', 'preguntar': False})
            context['imagen'] = 'img/estado_equipo/Mepostule.png'
        else:
            if not context['solicitud_recibida']:
                retorno.append({'accion': 'Invitar a mi organización ', 'url': 'organizaciones:organizacion_equipos_enviar_solicitud', 'preguntar': False})
                context['imagen'] = 'img/estado_equipo/Unetenos.png'
    else:
        retorno.append({'accion': 'Expulsar de la organización', 'url': 'organizaciones:organizacion_equipos_finalizar_relacion', 'preguntar': False})
        context['imagen'] = 'img/estado_equipo/Miembro.png'
    return retorno

def mensajes_joven_con_equipo(context):
    retorno = []

    if context['usuario'] == context['equipo'].lider:
        context['imagen'] = 'img/estado_equipo/Lider.png'
        return retorno

    if not context['relacionado']:
        if context['solicitud_recibida']:
            retorno.append({'accion': 'Aceptar solicitud como miembro', 'url': 'equipos:aceptar_invitacion', 'preguntar': False})
            retorno.append({'accion': 'Rechazar solicitud como miembro', 'url': 'equipos:rechazar_invitacion', 'preguntar': False})
            context['imagen'] = 'img/estado_equipo/Unetenos.png'

        if context['solicitud_enviada']:
            retorno.append({'accion': 'Cancelar solicitud como miembro', 'url': 'equipos:retirar_postulacion', 'preguntar': False})
            context['imagen'] = 'img/estado_equipo/Mepostule.png'
        else:
            if not context['solicitud_recibida']:
                retorno.append({'accion': 'Enviar solicitud como miembro', 'url': 'equipos:postularme', 'preguntar': False})
                context['imagen'] = 'img/estado_equipo/Unetenos.png'
    else:
        retorno.append({'accion': 'Abandonar equipo', 'url': 'equipos:salir_del_equipo', 'preguntar': False})
        context['imagen'] = 'img/estado_equipo/Miembro.png'

    return retorno


def enviar_correo(email, asunto, user, template_name='usuarios/correos/invitacion_inscripcion.html', solicitud_ruta=None, invitador=None, reto=None, equipo=None):
    from config.settings.base import EMAIL_HOST_USER
    from django.template.loader import render_to_string
    from django.core.mail import EmailMessage
    from django.contrib.sites.models import Site

    sitio_actual = Site.objects._get_site_by_id(1)  # current_site.name
    site_name = sitio_actual.name
    domain = sitio_actual.domain  # current_site.domain
    context = dict()

    context['domain'] = domain
    context['site_name'] = site_name
    context['protocol'] = 'https'
    context['name'] = email
    context['usuario'] = user
    if solicitud_ruta is not None:
        context['solicitud'] = solicitud_ruta
        context['usuario'] = user

    if invitador is not None:
        context['tipo_invitador'] = invitador.__class__.__name__
        context['invitador'] = invitador

    if reto is not None:
        context['reto'] = reto
        if 'Equipos' in reto.grupos_permitidos:
            context['equipo'] = equipo

    msg_html = render_to_string(template_name, context)

    msg = EmailMessage(asunto, msg_html,
                       EMAIL_HOST_USER, [email])
    msg.content_subtype = "html"
    try:
        msg.send()
    except:
        print('Error al enviar mensaje al correo %s' % email)


def generar_token_verificacion(user):
    import jwt
    from config.settings.base import SECRET_KEY
    from django.utils import timezone
    from datetime import timedelta

    tiempo_expiracion = timezone.now() + timedelta(days=3)
    payload = {
        'user': user.email,
        'exp': int(tiempo_expiracion.timestamp()),
        'type': 'email_confirmation',
    }
    token = jwt.encode(payload, SECRET_KEY, algorithm='HS256')

    return token.decode()


def enviar_correo_blanco(asunto, mensaje, email_emisor, email_receptor, archivo=None):
    from django.core.mail import EmailMessage
    from config.settings.base import EMAIL_HOST_USER
    email = EmailMessage(asunto, mensaje, EMAIL_HOST_USER, [EMAIL_HOST_USER])

    if archivo is not None:
        email.attach(archivo.name, archivo.read(), archivo.content_type)

    try:
        email.send()
    except Exception as e:
        print(e)

    
    
   
def reset_password(email, asunto, template_name='usuarios/restablecer_contrasena/correos/verificacion-suscripcion.html', id_usuario=0):
    from config.settings.base import EMAIL_HOST_USER
    from django.template.loader import render_to_string
    from django.core.mail import EmailMessage
    from django.contrib.sites.models import Site
    from django.shortcuts import get_object_or_404
    from kairos.usuarios.models import Usuario
    from django.utils.http import urlsafe_base64_encode
    from django.contrib.auth.tokens import default_token_generator

    sitio_actual = Site.objects._get_site_by_id(1)  # current_site.name
    site_name = sitio_actual.name
    domain = sitio_actual.domain # current_site.domain
    try:
        user = get_object_or_404(Usuario, email=email)
    except:
        user = get_object_or_404(Usuario, id=id_usuario)
    msg_html = render_to_string(template_name, {
        'domain': domain,
        'site_name': site_name,
        'uid': urlsafe_base64_encode(str.encode(str(user.id))),
        'token': default_token_generator.make_token(user),
        'protocol': 'https',
        'name': user.first_name + " " + user.last_name,
    })
    msg = EmailMessage(asunto, msg_html,
                       EMAIL_HOST_USER, [email])
    msg.content_subtype = "html"

    try:
        msg.send()
    except:
        print('Error al enviar mensaje al correo %s' % email)

def crear_contrasenia(email, asunto, template_name='organizaciones/correos/crear_contrasenia.html'):
    from config.settings.base import EMAIL_HOST_USER
    from django.template.loader import render_to_string
    from django.core.mail import EmailMessage
    from django.contrib.sites.models import Site
    from django.shortcuts import get_object_or_404
    from kairos.usuarios.models import Usuario
    from django.utils.http import urlsafe_base64_encode
    from django.contrib.auth.tokens import default_token_generator

    sitio_actual = Site.objects._get_site_by_id(1)  # current_site.name
    site_name = sitio_actual.name
    domain = sitio_actual.domain  # current_site.domain
    user = get_object_or_404(Usuario, email=email)
    msg_html = render_to_string(template_name, {
        'domain': domain,
        'site_name': site_name,
        'uid': urlsafe_base64_encode(str.encode(str(user.id))),
        'token': default_token_generator.make_token(user),
        'protocol': 'https',
        'name': user.first_name + " " + user.last_name,
    })
    msg = EmailMessage(asunto, msg_html,
                       EMAIL_HOST_USER, [email])
    msg.content_subtype = "html"
    try:
        msg.send()
    except:
        print('Error al enviar mensaje al correo %s' % email)

def comprimir_imagen(ruta=None):
    from django.conf import settings
    from PIL import Image

    if not ruta:
        ruta = settings.MEDIA_ROOT
    try:
        imagen = Image.open(("%s") % (ruta))
        imagen.save(("%s") % (ruta), format=imagen.format, quality=70)
    except:
        pass

class Round(Func):
    function = 'ROUND'
    template = '%(function)s(%(expressions)s, 2)'


def obtener_key(dictionary, valor):
    for key, value in dictionary.items():
        if valor == value:
            return key

    return "None"

def cambiar_decimales(numero):
    """
        Cambia la cantidad de decimales a 1 y si es 0 no le deja ningún decimal
        Parametros: el número al que se le cambian las decimales
        Retorno: número con solo 1 decimal
    """
    if numero == 0:
        return 0
    else:
        return round(numero,1)

def obtener_listado_de_palabras_limpias(palabras_clave:list) -> list:
    listado_palabras_excluidas = list(PalabraDeLimpieza.obtener_todas())
    set_listado_palabras_exluidas = set(
        listado_palabras_excluidas
        )
    lista_limpia = list(set(palabras_clave) - set_listado_palabras_exluidas)
    lista_limpia = eliminar_numeros(lista_limpia)
    lista_limpia = eliminar_caracteres(lista_limpia)
    return lista_limpia

def eliminar_numeros(lista:list) -> list:
    return [re.sub(r'[0-9]', '', palabra) for palabra in lista]

def eliminar_caracteres(lista:list) -> list:
    lista_palabras = []
    for palabra in lista:
        tabla = str.maketrans('', '', string.punctuation)
        lista_palabras.append(palabra.translate(tabla))
    return lista_palabras
