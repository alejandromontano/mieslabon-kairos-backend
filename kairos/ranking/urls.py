from django.urls import path
from . import views

app_name = "ranking"

urlpatterns = [
    path('listado', views.Ranking.as_view(), name='listado'),
]