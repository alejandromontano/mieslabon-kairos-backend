# Modulos Django
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DetailView, TemplateView

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app

class Ranking(LoginRequiredMixin, TemplateView):
    template_name = "ranking/pagina_de_espera.html"