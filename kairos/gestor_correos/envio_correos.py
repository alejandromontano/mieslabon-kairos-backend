# Modulos Django
from config.settings.base import EMAIL_HOST_USER, MAILJET_API_KEY, MAIJET_API_SECRECT
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.contrib.sites.models import Site
from django.contrib.auth.tokens import default_token_generator
from django.shortcuts import get_object_or_404
from django.utils.http import urlsafe_base64_encode
from mailjet_rest import Client

from config.settings.base import ROOT_URLCONF 
from django.urls import URLPattern, URLResolver

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from kairos.usuarios.models import Usuario

URLCONF = __import__(ROOT_URLCONF, {}, {}, [''])

INVITAR_JOVENES = 'Invitar_Jovenes'
ENVIAR_SOLICITUD_RETOS = 'Enviar_Solicitud_Retos'
ENVIAR_SOLICITUD_PQRS = 'Enviar_Solicitud_PQRS '
ENIVAR_ACEPTACION_JOVEN_RETO = 'Enviar_Aceptacion_Joven_Reto'
ENIVAR_RECHAZO_JOVEN_RETO = 'Enviar_Rechazo_Joven_Reto'
ENIVAR_ACEPTACION_EQUIPO_RETO = 'Enviar_Aceptacion_Equipo_Reto'
ENIVAR_RECHAZO_EQUIPO_RETO = 'Enviar_Rechazo_Equipo_Reto'
VERIFICACION_USUARIO_CONTRASENIA = 'Verificacion_Usuario_Contrasenia'
SOLICITAR_CONTRASENIA = 'Solicitar_Contrasenia'
NOTIFICACION_RETO_FINALIZADO = 'Notificacion_Reto_Finalizado'

DICCIONARIO_CONFIGURACION_CORREO = {
    INVITAR_JOVENES: {'template_id':1845060, 'asunto':'MiEslabón | UNETE A LA PLATAFORMA'},
    ENVIAR_SOLICITUD_RETOS: {'template_id':1845779, 'asunto':'MiEslabón | SOLICITUD DE RETO'},

    ENVIAR_SOLICITUD_PQRS: {'template_id':12547, 'asunto':'MiEslabón | SOLICITUD DE PQRS'},

    ENIVAR_ACEPTACION_JOVEN_RETO: {'template_id':1845957, 'asunto':'MiEslabón | SOLICITUD RETO ACEPTADA'},
    ENIVAR_RECHAZO_JOVEN_RETO: {'template_id':1845800, 'asunto':'MiEslabón | SOLICITUD RETO RECHAZADA'},
    ENIVAR_ACEPTACION_EQUIPO_RETO: {'template_id':1845957, 'asunto':'MiEslabón | SOLICITUD RETO ACEPTADA'},
    ENIVAR_RECHAZO_EQUIPO_RETO: {'template_id':1845800, 'asunto':'MiEslabón | SOLICITUD RETO RECHAZADA'},
    VERIFICACION_USUARIO_CONTRASENIA: {'template_id':2093380, 'asunto':'MiEslabón | VERIFICACIÓN DE EMAIL'},
    SOLICITAR_CONTRASENIA: {'template_id':1838980, 'asunto':'MiEslabón | ACTUALIZAR CONTRASEÑA'},
    NOTIFICACION_RETO_FINALIZADO: {'template_id':2209360, 'asunto':'MiEslabón | CALIFICACIÓN RETO'},
}

DICCIONARIO_URLS = {}

def crear_data_email():
    pass

def enviar_correo(email, objetos, tipo, usuario=None, mensaje=None):

    mailjet = Client(auth=(MAILJET_API_KEY, MAIJET_API_SECRECT), version='v3.1')
    data = {}
    if tipo == INVITAR_JOVENES:
        data = construir_context(EMAIL_HOST_USER, email, tipo, objetos)
    elif tipo == ENVIAR_SOLICITUD_RETOS:
        data = construir_context(EMAIL_HOST_USER, email, tipo, objetos, usuario)
    elif tipo == ENVIAR_SOLICITUD_PQRS:
        if objetos is None:
            data = construir_correo(email, mensaje)
        else:
            data = construir_correo_archivo(email, mensaje, objetos)
    elif tipo == ENIVAR_ACEPTACION_JOVEN_RETO or tipo == ENIVAR_RECHAZO_JOVEN_RETO:
        data = construir_context(EMAIL_HOST_USER, email, tipo, objetos, usuario)
    elif tipo == ENIVAR_ACEPTACION_EQUIPO_RETO or tipo == ENIVAR_RECHAZO_EQUIPO_RETO:
        data = construir_context(EMAIL_HOST_USER, email, tipo, objetos, usuario)
    elif tipo == VERIFICACION_USUARIO_CONTRASENIA or tipo == SOLICITAR_CONTRASENIA:        
        data = construir_context(EMAIL_HOST_USER, email, tipo, None, usuario)
    elif tipo == NOTIFICACION_RETO_FINALIZADO:
        data = construir_context(EMAIL_HOST_USER, email, tipo, objetos, usuario)
        
   
    try:        
        result = mailjet.send.create(data=data)    
        print('Correo enviado')
    except:
        print('Error al enviar mensaje al correo %s' % email)
    

def construir_context(email_emisor, email_receptor, tipo, objetos, usuario=None):
    
    if len(DICCIONARIO_URLS) == 0:
        list_urls(URLCONF.urlpatterns)
        for i in list_urls(URLCONF.urlpatterns):
            if len(i[0]) >= 2:                
                DICCIONARIO_URLS[i[1]] = i[0][0]+i[0][1]
        
    sitio_actual = Site.objects._get_site_by_id(1)  # current_site.name
    site_name = sitio_actual.name
    domain = sitio_actual.domain  # current_site.domain
    context = dict()
    context['domain'] = domain
    context['site_name'] = site_name
    context['protocol'] = 'https'

    if tipo == INVITAR_JOVENES:
        context = {**context, **context_invitar_jovenes(objetos)}
    elif tipo == ENVIAR_SOLICITUD_RETOS:
        context = {**context, **context_enviar_solicitiud_retos(objetos, usuario)}
    elif tipo == ENIVAR_ACEPTACION_JOVEN_RETO or tipo == ENIVAR_RECHAZO_JOVEN_RETO:
        context = {**context, **context_enviar_solicitiud_joven_retos(objetos, usuario)}
    elif tipo == ENIVAR_ACEPTACION_EQUIPO_RETO or tipo == ENIVAR_RECHAZO_EQUIPO_RETO:
        context = {**context, **context_enviar_solicitiud_equipo_retos(objetos, usuario)}
    elif tipo == SOLICITAR_CONTRASENIA:
        context = {**context, **context_solicitar_usuario_contrasenia(usuario)}
    elif tipo == VERIFICACION_USUARIO_CONTRASENIA:
        context = {**context, **context_verificacion_usuario_contrasenia(usuario)}
    elif tipo == NOTIFICACION_RETO_FINALIZADO:
        context = {**context, **context_notificacion_reto_finalizado(objetos, usuario)}

    data = {
        'Messages': [
            {
                "From": {
                        "Email": email_emisor,
                        "Name": "MiEslabón"
                },
                "To": [
                        {
                                "Email": email_receptor,
                                "Name": "You"
                        }
                ],
                "TemplateLanguage": True,
                "TemplateID": DICCIONARIO_CONFIGURACION_CORREO[tipo]['template_id'],
                "Subject": DICCIONARIO_CONFIGURACION_CORREO[tipo]['asunto'],
                "Variables": context
                
            }
        ]
    }
    
    return data

def list_urls(lis, acc=None):
    """ recursive """
    if acc is None:
        acc = []
    if not lis:
        return
    l = lis[0]
    
    if isinstance(l, URLPattern):
        yield acc + [str(l.pattern)], l.name
    elif isinstance(l, URLResolver):
        yield from list_urls(l.url_patterns, acc + [str(l.pattern)])
    yield from list_urls(lis[1:], acc)

def construir_correo(email, mensaje):
    data = {
        'Messages': [
            {
                "From": {
                        "Email": email,
                        "Name": "MiEslabón"
                },
                "To": [
                        {
                                "Email": email,
                                "Name": "You"
                        }
                ],                    
                "Subject": DICCIONARIO_CONFIGURACION_CORREO[ENVIAR_SOLICITUD_PQRS]['asunto'],
                "TextPart": "Dear passenger 1, welcome to Mailjet! May the delivery force be with you!",
                "HTMLPart": mensaje,
            }
        ]
    }    
    return data

def construir_correo_archivo(email, mensaje, archivo):
    import base64
    
    print(archivo.name)
    base64File = base64.b64encode(archivo.read()).decode('utf-8')
    
    
        
    #print(base64File)
   
    data = {
        'Messages': [
            {
                "From": {
                        "Email": email,
                        "Name": "MiEslabón"
                },
                "To": [
                        {
                                "Email": email,
                                "Name": "You"
                        }
                ],                    
                "Subject": DICCIONARIO_CONFIGURACION_CORREO[ENVIAR_SOLICITUD_PQRS]['asunto'],
                "TextPart": "Dear passenger 1, welcome to Mailjet! May the delivery force be with you!",
                "HTMLPart": mensaje,
                'Attachments': [
                    {
                        'ContentType': archivo.content_type,
                        'Filename': archivo.name,
                        'Base64Content': str(base64File)
                    }
                ]
                
            }
        ]
    }    
    return data

def context_invitar_jovenes(objetos):
    context = dict()
    if objetos is not None:
        context['tipo_invitador'] = objetos.__class__.__name__
        
    if objetos.__class__.__name__ == 'Organizacion':
        url = DICCIONARIO_URLS['registrarse_con_organizacion']
        nueva_url = url.replace('<int:tipo>', str(1)).replace('<int:organizacion>', str(objetos.pk))
        context['invitador'] = objetos.first_name
    else:
        if objetos.__class__.__name__ == 'Equipo':
            context['invitador'] = objetos.nombre
        else:
           context['invitador'] = objetos.first_name +' '+objetos.last_name
        url = DICCIONARIO_URLS['registrarse']
        nueva_url = url.replace('<int:tipo>', str(1))
    
    context['url'] = nueva_url
    
    return context

def context_enviar_solicitiud_retos(objetos, usuario):
    context = dict()
    if objetos is not None:        
        context['usuario'] = usuario.first_name+' '+usuario.last_name
        context['nombre_responsable'] = objetos.nombre_responsable
        context['correo'] = objetos.correo
        context['telefono'] = objetos.telefono
        context['mensaje'] = objetos.mensaje
    return context

def context_enviar_solicitiud_joven_retos(objetos, usuario):
    context = dict()
    if objetos is not None:        
        context['reto_grupos'] = objetos.grupos_permitidos
        context['reto_nombre'] = objetos.nombre
        context['reto_fecha'] = str(objetos.fecha_inicio).split('.')[0]
        context['usuario'] = usuario.first_name + " " + usuario.last_name
    return context

def context_enviar_solicitiud_equipo_retos(objetos, usuario):
    context = dict()
    if objetos is not None:
        context['reto_grupos'] = objetos[0].grupos_permitidos
        context['reto_nombre'] = objetos[0].nombre
        context['reto_fecha'] = str(objetos[0].fecha_inicio)
        context['equipo'] = objetos[1].nombre
        context['usuario'] = usuario.first_name + " " + usuario.last_name
    return context

def context_solicitar_usuario_contrasenia(usuario):
    url = DICCIONARIO_URLS['cambiar_contrasena']
    user = get_object_or_404(Usuario, id=usuario.pk)
    context = dict()
    uid = urlsafe_base64_encode(str.encode(str(user.id)))
    token = default_token_generator.make_token(user)
    nueva_url = url.replace('<uidb64>', '{}'.format(uid)).replace('<token>', '{}'.format(token))
    context['name'] = user.first_name + " " + user.last_name
    context['url'] = nueva_url
    return context


def context_verificacion_usuario_contrasenia(usuario):
    from django.utils import timezone
    
    from datetime import timedelta
    import jwt
    
    from config.settings.base import SECRET_KEY
    
    url = DICCIONARIO_URLS['verificar_correo']
    user = get_object_or_404(Usuario, id=usuario.pk)
    context = dict()
    fecha_expiracion = timezone.now() +timedelta(days=2)
    payload = {
        'usuario': user.email,
        'exp': int(fecha_expiracion.timestamp()),
        'type': 'confirmacion_email',
    }
    token_codificado = jwt.encode(payload, SECRET_KEY, algorithm='HS256')    
    context['name'] = user.first_name + " " + user.last_name    
    context['token'] = token_codificado.decode()
    context['url'] = url
    
    return context


def context_notificacion_reto_finalizado(objetos, usuario):
    from kairos.retos.utils import obtener_detalle_participante
    context = dict()
    if objetos is not None:
        reto = objetos[0]
        context['reto_grupos'] = reto.grupos_permitidos
        context['reto_nombre'] = reto.nombre
        context['usuario'] = usuario.first_name + " " + usuario.last_name
        

        if 'Equipos' in reto.grupos_permitidos:
            equipo = objetos[1]
            detalle = obtener_detalle_participante(reto, equipo.pk)
            print(detalle, " Equipo")
            context['reto_puntaje'] = str(detalle.calificacion_final)
            context['equipo'] = equipo.nombre            
        else:
            print(detalle, " Joven")
            detalle = obtener_detalle_participante(reto, usuario.pk)
            context['reto_puntaje'] = str(detalle.calificacion_final)

    return context
