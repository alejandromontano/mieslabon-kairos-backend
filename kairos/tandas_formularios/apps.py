from django.apps import AppConfig


class TandasFormulariosConfig(AppConfig):
    name = 'kairos.tandas_formularios'
