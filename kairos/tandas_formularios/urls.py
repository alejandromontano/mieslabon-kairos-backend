# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "tandas_formularios"

urlpatterns = [
    path('gestionar/', TandasListado.as_view(), name='listado'),
    path('consultar/', TandasConsultar.as_view(), name='consultar'),
    path('registrar', TandaRegistrar.as_view(), name='registrar'),
    path('modificar/<int:id_tanda>', TandaModificar.as_view(), name='modificar'),

    path('asignar/', jovenAsignarTanda, name='asignar'),
  #  path('asignaciones/modificar/<int:id_joven>', AsignacionTandaModificar.as_view(), name='asignaciones_modificar'),
    path('asignaciones/gestionar/', AsignacionesListado.as_view(), name='asignaciones'),
    path('deshabilitar/<int:id>', view=deshabilitarTandaJoven, name="deshabilitar"),
]