# Modulos Django
from django.db import models

# Modulos de plugin externos
from simple_history.models import HistoricalRecords

# Modulos de otras apps

# Modulos internos de la app

class TandasFormularios(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='nombre de la tanda*')
    identificador = models.CharField(max_length=10, verbose_name="identificador de la tanda*", unique=True)
    formularios = models.ManyToManyField('formularios.Formularios', related_name="formularios_de_la_tanda", verbose_name="formularios "
                                                                                                           "de la tanda*")
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)
    activo = models.BooleanField(verbose_name="¿La pregunta se encuentra activa?", default=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    def buscar(identificador:str) -> 'TandasFormularios':
        '''
            Retorna un TandasFormularios perteneciente al id

            Parámetros:
                id: id de las TandasFormularios que se buscara

            Retorno:
                Si existe: TandasFormularios
                Si no existe: None
        '''
        try:
            tandas = TandasFormularios.objects.get(identificador=identificador)
            return tandas
        except TandasFormularios.DoesNotExist:
            return None

    class Meta:
        ordering = ('nombre',)
