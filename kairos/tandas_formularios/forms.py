# Modulos Django
from django import forms

# Modulos de plugin externos
from kairos.usuarios.models import Joven
from kairos.formularios.models import Formularios

# Modulos de otras apps
from django_select2.forms import Select2MultipleWidget, ModelSelect2Widget

# Modulos internos de la app
from .models import *


class TandasModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TandasModelForm, self).__init__(*args, **kwargs)
        self.fields["formularios"].queryset = Formularios.objects.filter(activo=True, pregunta_del_formulario__isnull=False).distinct()

    class Meta:
        model = TandasFormularios
        fields = ['identificador', 'nombre', 'formularios', 'activo']

        help_texts = {
            'identificador': 'Identificador único de la tanda de cuestionarios.'
        }

        widgets = {
            'formularios': Select2MultipleWidget()
        }


class TandaJovenForm(forms.Form):
        joven = forms.ModelChoiceField(
            queryset=Joven.objects.filter(tanda_formularios__isnull=True),
            widget=ModelSelect2Widget(
                model=Joven,
                queryset=Joven.objects.filter(tanda_formularios__isnull=True),
                search_fields=['first_name__icontains', 'last_name__icontains', 'identificacion__icontains'],
            ),
            required=True,
            label="Joven*"
        )

        tanda = forms.ModelChoiceField(
            queryset=TandasFormularios.objects.filter(activo=True),
            widget=ModelSelect2Widget(
                model=TandasFormularios,
                queryset=TandasFormularios.objects.filter(activo=True),
                search_fields=['nombre__icontains'],
            ),
            required=True,
            label="Tanda de formularios*"
        )


class TandaJovenModificarForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TandaJovenModificarForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Joven
        fields = ['tanda_formularios']

        widgets = {
            'tanda_formularios': ModelSelect2Widget(
                model=TandasFormularios,
                queryset=TandasFormularios.objects.filter(activo=True),
                search_fields=['nombre__icontains'],
            )
        }