# Modulos Django
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView

# Modulos de plugin externos

# Modulos de otras apps
from kairos.core.mixins import MensajeMixin
from kairos.historiales.models import RegistroTandasJoven

# Modulos internos de la app
from .forms import *
from .models import *



class TandasListado(LoginRequiredMixin, ListView):
    model = TandasFormularios
    context_object_name = "tandas"
    template_name = "formularios/tandas/listado.html"

    def get_queryset(self):
        tandas = TandasFormularios.objects.all()
        return tandas


class TandasConsultar(LoginRequiredMixin, ListView):
    model = TandasFormularios
    context_object_name = "tandas"
    template_name = "formularios/tandas/consultar.html"

    def get_queryset(self):
        tandas = TandasFormularios.objects.all()
        return tandas


class TandaRegistrar(LoginRequiredMixin, MensajeMixin, CreateView):
    model = TandasFormularios
    form_class = TandasModelForm
    template_name = "formularios/tandas/registrar.html"
    success_url = reverse_lazy('tandas:registrar')
    mensaje_exito = "Tanda de formularios registrada correctamente!"
    mensaje_error = "No se logró registrar la tanda de formularios, por favor verifique los datos ingresados."


class TandaModificar(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = TandasFormularios
    form_class = TandasModelForm
    pk_url_kwarg = 'id_tanda'
    template_name = "formularios/tandas/modificar.html"
    success_url = reverse_lazy('tandas:listado')
    mensaje_exito = "Tanda de formularios modificada correctamente!"
    mensaje_error = "No se logró modificar la tanda de formularios, por favor verifique los datos ingresados."


def jovenAsignarTanda(request):    
    '''
        Asigna una Tanda a un Joven

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
    '''

    template = 'formularios/tandas/asignar.html'

    if not request.user.is_authenticated:
        return redirect("inicio")

    if not request.user.es_superusuario:
        messages.error(request, "Usted no está autorizado para asignar las tandas de formularios a un joven.")
        return redirect("inicio")

    if request.method == 'POST':
        form = TandaJovenForm(request.POST)
        if form.is_valid():
            joven = form.cleaned_data['joven']
            tanda = form.cleaned_data['tanda']
            joven.tanda_formularios = tanda
            registros_anteriores = RegistroTandasJoven.objects.filter(joven=joven)
            for registro in registros_anteriores: # Deshabilitar los registros anteriores
                registro.disponible = False
                registro.save()
            RegistroTandasJoven.objects.create(tanda=tanda, joven=joven)  # Crea el nuevo registro
            joven.save()
            messages.success(request, 'Asignación de tanda de formularios realizada correctamente!')
            return redirect('tandas:asignar')
        else:
            messages.error(request, 'No se logró realizar la asignación, por favor verifique los datos ingresados!')
            return render(request, template, {'form': form})

    if request.method == 'GET':
        form = TandaJovenForm()
        return render(request, template, {'form': form})


class AsignacionTandaModificar(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = Joven
    form_class = TandaJovenModificarForm
    pk_url_kwarg = 'id_joven'
    template_name = 'formularios/tandas/asignar.html'
    success_url = reverse_lazy('tandas:asignaciones')
    mensaje_exito = "Asignación de tanda de formularios modificada correctamente!"
    mensaje_error = "No se logró modificar la asignación, por favor verifique los datos ingresados."


def deshabilitarTandaJoven(request, id):
    '''
        Desabilita una tanda de formularios para un joven especifico

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id: Identicador del Joven a buscar
    '''
    joven = Joven.objects.get(pk=id)
    if joven.tanda_formularios:
        joven.tanda_formularios = None
        joven.save()
        registros_anteriores = RegistroTandasJoven.objects.filter(joven=joven)
        for registro in registros_anteriores:  # Deshabilitar los registros anteriores
            registro.disponible = False
            registro.save()
        messages.success(request, "La tanda de formularios ha sido deshabilitada para el joven %s!" % joven.get_full_name())
    return redirect('tandas:asignaciones')


class AsignacionesListado(LoginRequiredMixin, ListView):
    model = Joven
    context_object_name = "jovenes"
    template_name = "formularios/tandas/consultar_asignaciones.html"

    def get_queryset(self):
        jovenes = Joven.objects.filter(tanda_formularios__isnull=False)
        return jovenes