from django.db import models
from simple_history.models import HistoricalRecords

TIEMPO = (
    ("1", "Menos de 1 año"),
    ("2", "Entre 1 y 2 años"),
    ("3", "Entre 2 y 4 años"),
    ("4", "Más de 5 años")
)

SI_NO = (
    (True, "Sí"),
    (False, "No")
)

class ClubesJuveniles(models.Model):
    joven = models.ForeignKey('usuarios.Joven', related_name="clubes_del_joven", on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, verbose_name='Nombre del club/grupo')
    tribu = models.ForeignKey('insignias.Tribus', verbose_name='¿En cuál tribu ubicarías el club juvenil al que perteneces o pertenecías?',
                              on_delete=models.CASCADE)
    tiempo = models.CharField(max_length=10, choices=TIEMPO,
                              verbose_name='¿Cuánto tiempo perteneciste o has pertenecido al club/grupo juvenil?')
    pertenece_actualmente = models.BooleanField(verbose_name='¿Sigues perteneciendo?', null=False, blank=True, choices=SI_NO, default=False)
    history = HistoricalRecords()


class Voluntariados(models.Model):
    joven = models.ForeignKey('usuarios.Joven', related_name="voluntariados_del_joven", on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100,
                              verbose_name='Rol o cargo como voluntario')
    tribu = models.ForeignKey('insignias.Tribus', verbose_name='¿En cuál tribu ubicarías tu voluntariado?', on_delete=models.CASCADE)
    tiempo = models.CharField(max_length=10, choices=TIEMPO,
                              verbose_name='¿Cuánto tiempo fuiste o has sido voluntario?')
    pertenece_actualmente = models.BooleanField(verbose_name='¿Sigues perteneciendo?', null=False, blank=True, choices=SI_NO, default=False)
    history = HistoricalRecords()


class Laborales(models.Model):
    joven = models.ForeignKey('usuarios.Joven', related_name="experiencias_laborales_del_joven", on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, verbose_name='Cargo por el que te contrataron')
    tribu = models.ForeignKey('insignias.Tribus', verbose_name='¿En cuál tribu ubicarías el trabajo por el fuiste contratado?',
                              on_delete=models.CASCADE)
    tiempo = models.CharField(max_length=10, choices=TIEMPO, verbose_name='¿Cuánto tiempo estuviste o has estado contratado?')
    pertenece_actualmente = models.BooleanField(verbose_name='¿Sigues perteneciendo?', null=False, blank=True, choices=SI_NO, default=False)
    history = HistoricalRecords()


class Independientes(models.Model):
    joven = models.ForeignKey('usuarios.Joven', related_name="trabajos_independientes_del_joven", on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, verbose_name='Servicio/producto que producías/produces y vendías/vendes tú mismo')
    tribu = models.ForeignKey('insignias.Tribus', verbose_name='¿En cuál tribu ubicarías tu trabajo como independiente?', on_delete=models.CASCADE)
    tiempo = models.CharField(max_length=10, choices=TIEMPO, verbose_name='¿Cuánto tiempo trabajas o has trabajado como '
                                                                          'independiente?')
    pertenece_actualmente = models.BooleanField(verbose_name='¿Sigues perteneciendo?', null=False, blank=True, choices=SI_NO, default=False)
    history = HistoricalRecords()


class Emprendimientos(models.Model):
    joven = models.ForeignKey('usuarios.Joven', related_name="emprendimientos_del_joven", on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, verbose_name='¿En qué consistía tu negocio?')
    tribu = models.ForeignKey('insignias.Tribus', verbose_name='¿En cuál tribu ubicarías tu emprendimiento?', on_delete=models.CASCADE)
    tiempo = models.CharField(max_length=10, choices=TIEMPO, verbose_name='¿En qué consistía o consiste tu negocio?')
    pertenece_actualmente = models.BooleanField(verbose_name='¿Sigues perteneciendo?', null=False, blank=True, choices=SI_NO, default=False)
    history = HistoricalRecords()
