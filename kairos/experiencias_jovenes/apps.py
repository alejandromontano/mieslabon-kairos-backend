from django.apps import AppConfig


class ExperienciasJovenesConfig(AppConfig):
    name = 'kairos.experiencias_jovenes'
