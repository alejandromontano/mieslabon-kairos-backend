# Generated by Django 3.0.2 on 2021-10-11 15:45

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('experiencias_jovenes', '0001_initial'),
        ('usuarios', '0001_initial'),
        ('insignias', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='voluntariados',
            name='joven',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='voluntariados_del_joven', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='voluntariados',
            name='tribu',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='insignias.Tribus', verbose_name='¿En cuál tribu ubicarías tu voluntariado?'),
        ),
        migrations.AddField(
            model_name='laborales',
            name='joven',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='experiencias_laborales_del_joven', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='laborales',
            name='tribu',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='insignias.Tribus', verbose_name='¿En cuál tribu ubicarías el trabajo por el fuiste contratado?'),
        ),
        migrations.AddField(
            model_name='independientes',
            name='joven',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='trabajos_independientes_del_joven', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='independientes',
            name='tribu',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='insignias.Tribus', verbose_name='¿En cuál tribu ubicarías tu trabajo como independiente?'),
        ),
        migrations.AddField(
            model_name='historicalvoluntariados',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalvoluntariados',
            name='joven',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='historicalvoluntariados',
            name='tribu',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='insignias.Tribus', verbose_name='¿En cuál tribu ubicarías tu voluntariado?'),
        ),
        migrations.AddField(
            model_name='historicallaborales',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicallaborales',
            name='joven',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='historicallaborales',
            name='tribu',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='insignias.Tribus', verbose_name='¿En cuál tribu ubicarías el trabajo por el fuiste contratado?'),
        ),
        migrations.AddField(
            model_name='historicalindependientes',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalindependientes',
            name='joven',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='historicalindependientes',
            name='tribu',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='insignias.Tribus', verbose_name='¿En cuál tribu ubicarías tu trabajo como independiente?'),
        ),
        migrations.AddField(
            model_name='historicalemprendimientos',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalemprendimientos',
            name='joven',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='historicalemprendimientos',
            name='tribu',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='insignias.Tribus', verbose_name='¿En cuál tribu ubicarías tu emprendimiento?'),
        ),
        migrations.AddField(
            model_name='historicalclubesjuveniles',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalclubesjuveniles',
            name='joven',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='historicalclubesjuveniles',
            name='tribu',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='insignias.Tribus', verbose_name='¿En cuál tribu ubicarías el club juvenil al que perteneces o pertenecías?'),
        ),
        migrations.AddField(
            model_name='emprendimientos',
            name='joven',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='emprendimientos_del_joven', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='emprendimientos',
            name='tribu',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='insignias.Tribus', verbose_name='¿En cuál tribu ubicarías tu emprendimiento?'),
        ),
        migrations.AddField(
            model_name='clubesjuveniles',
            name='joven',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='clubes_del_joven', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='clubesjuveniles',
            name='tribu',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='insignias.Tribus', verbose_name='¿En cuál tribu ubicarías el club juvenil al que perteneces o pertenecías?'),
        ),
    ]
