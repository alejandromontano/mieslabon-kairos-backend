# Generated by Django 3.0.2 on 2021-10-11 15:45

from django.db import migrations, models
import simple_history.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ClubesJuveniles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='Nombre del club/grupo')),
                ('tiempo', models.CharField(choices=[('1', 'Menos de 1 año'), ('2', 'Entre 1 y 2 años'), ('3', 'Entre 2 y 4 años'), ('4', 'Más de 5 años')], max_length=10, verbose_name='¿Cuánto tiempo perteneciste o has pertenecido al club/grupo juvenil?')),
                ('pertenece_actualmente', models.BooleanField(blank=True, choices=[(True, 'Sí'), (False, 'No')], default=False, verbose_name='¿Sigues perteneciendo?')),
            ],
        ),
        migrations.CreateModel(
            name='Emprendimientos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='¿En qué consistía tu negocio?')),
                ('tiempo', models.CharField(choices=[('1', 'Menos de 1 año'), ('2', 'Entre 1 y 2 años'), ('3', 'Entre 2 y 4 años'), ('4', 'Más de 5 años')], max_length=10, verbose_name='¿En qué consistía o consiste tu negocio?')),
                ('pertenece_actualmente', models.BooleanField(blank=True, choices=[(True, 'Sí'), (False, 'No')], default=False, verbose_name='¿Sigues perteneciendo?')),
            ],
        ),
        migrations.CreateModel(
            name='HistoricalClubesJuveniles',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='Nombre del club/grupo')),
                ('tiempo', models.CharField(choices=[('1', 'Menos de 1 año'), ('2', 'Entre 1 y 2 años'), ('3', 'Entre 2 y 4 años'), ('4', 'Más de 5 años')], max_length=10, verbose_name='¿Cuánto tiempo perteneciste o has pertenecido al club/grupo juvenil?')),
                ('pertenece_actualmente', models.BooleanField(blank=True, choices=[(True, 'Sí'), (False, 'No')], default=False, verbose_name='¿Sigues perteneciendo?')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical clubes juveniles',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalEmprendimientos',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='¿En qué consistía tu negocio?')),
                ('tiempo', models.CharField(choices=[('1', 'Menos de 1 año'), ('2', 'Entre 1 y 2 años'), ('3', 'Entre 2 y 4 años'), ('4', 'Más de 5 años')], max_length=10, verbose_name='¿En qué consistía o consiste tu negocio?')),
                ('pertenece_actualmente', models.BooleanField(blank=True, choices=[(True, 'Sí'), (False, 'No')], default=False, verbose_name='¿Sigues perteneciendo?')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical emprendimientos',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalIndependientes',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='Servicio/producto que producías/produces y vendías/vendes tú mismo')),
                ('tiempo', models.CharField(choices=[('1', 'Menos de 1 año'), ('2', 'Entre 1 y 2 años'), ('3', 'Entre 2 y 4 años'), ('4', 'Más de 5 años')], max_length=10, verbose_name='¿Cuánto tiempo trabajas o has trabajado como independiente?')),
                ('pertenece_actualmente', models.BooleanField(blank=True, choices=[(True, 'Sí'), (False, 'No')], default=False, verbose_name='¿Sigues perteneciendo?')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical independientes',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalLaborales',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='Cargo por el que te contrataron')),
                ('tiempo', models.CharField(choices=[('1', 'Menos de 1 año'), ('2', 'Entre 1 y 2 años'), ('3', 'Entre 2 y 4 años'), ('4', 'Más de 5 años')], max_length=10, verbose_name='¿Cuánto tiempo estuviste o has estado contratado?')),
                ('pertenece_actualmente', models.BooleanField(blank=True, choices=[(True, 'Sí'), (False, 'No')], default=False, verbose_name='¿Sigues perteneciendo?')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical laborales',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalVoluntariados',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='Rol o cargo como voluntario')),
                ('tiempo', models.CharField(choices=[('1', 'Menos de 1 año'), ('2', 'Entre 1 y 2 años'), ('3', 'Entre 2 y 4 años'), ('4', 'Más de 5 años')], max_length=10, verbose_name='¿Cuánto tiempo fuiste o has sido voluntario?')),
                ('pertenece_actualmente', models.BooleanField(blank=True, choices=[(True, 'Sí'), (False, 'No')], default=False, verbose_name='¿Sigues perteneciendo?')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'verbose_name': 'historical voluntariados',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='Independientes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='Servicio/producto que producías/produces y vendías/vendes tú mismo')),
                ('tiempo', models.CharField(choices=[('1', 'Menos de 1 año'), ('2', 'Entre 1 y 2 años'), ('3', 'Entre 2 y 4 años'), ('4', 'Más de 5 años')], max_length=10, verbose_name='¿Cuánto tiempo trabajas o has trabajado como independiente?')),
                ('pertenece_actualmente', models.BooleanField(blank=True, choices=[(True, 'Sí'), (False, 'No')], default=False, verbose_name='¿Sigues perteneciendo?')),
            ],
        ),
        migrations.CreateModel(
            name='Laborales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='Cargo por el que te contrataron')),
                ('tiempo', models.CharField(choices=[('1', 'Menos de 1 año'), ('2', 'Entre 1 y 2 años'), ('3', 'Entre 2 y 4 años'), ('4', 'Más de 5 años')], max_length=10, verbose_name='¿Cuánto tiempo estuviste o has estado contratado?')),
                ('pertenece_actualmente', models.BooleanField(blank=True, choices=[(True, 'Sí'), (False, 'No')], default=False, verbose_name='¿Sigues perteneciendo?')),
            ],
        ),
        migrations.CreateModel(
            name='Voluntariados',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, verbose_name='Rol o cargo como voluntario')),
                ('tiempo', models.CharField(choices=[('1', 'Menos de 1 año'), ('2', 'Entre 1 y 2 años'), ('3', 'Entre 2 y 4 años'), ('4', 'Más de 5 años')], max_length=10, verbose_name='¿Cuánto tiempo fuiste o has sido voluntario?')),
                ('pertenece_actualmente', models.BooleanField(blank=True, choices=[(True, 'Sí'), (False, 'No')], default=False, verbose_name='¿Sigues perteneciendo?')),
            ],
        ),
    ]
