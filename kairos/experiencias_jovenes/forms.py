# from .models import Usuario, Joven, Padre
from django.forms import inlineformset_factory
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML
from .formsets import *
from .models import *
from kairos.usuarios.models import Joven
from kairos.insignias.models import Tribus
from django_select2.forms import Select2MultipleWidget, Select2Widget, ModelSelect2Widget


class JovenExperienciaActualForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(JovenExperienciaActualForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(Field(HTML(
                '<h4 style="color:#A32CDF">Clubes Juveniles</h4>'
                '<h5 class="mb-0">¿Estás o estuviste en un club juvenil?</h5>')), css_class="col-sm-12"),
            Div(Fieldset('', Formset('clubes'), css_class="col-xl-12"),
                css_class="col-xl-12 mt-4 formset-clubes"),
            Div(Field(HTML(
                '<h4 style="color:#A32CDF">Voluntariados</h4>'
                '<h5 class="mb-0">¿Has sido o eres voluntario? Escribe el rol o cargo como voluntario en una frase.</h5>')), css_class="col-xl-12"),
            Div(Fieldset('', Formset('voluntariados'), css_class="col-xl-12"),
                css_class="col-xl-12 mt-4 formset-voluntariados"),
            Div(Field(HTML(
                '<h4 style="color:#A32CDF">Experiencia Colaborador</h4>'
                '<h5 class="mb-0">¿Cuentas con experiencia laboral formal o informal?</h5>')), css_class="col-xl-12"),
            Div(Fieldset('', Formset('experiencias_laborales'), css_class="col-xl-12"),
                css_class="col-xl-12 mt-4 formset-laborales"),
            Div(Field(HTML(
                '<h4 style="color:#A32CDF">Experiencia Independiente</h4>'
                '<h5 class="mb-0">¿Has trabajado o estás trabajando como independiente?</h5>')), css_class="col-xl-12"),
            Div(Fieldset('', Formset('trabajos_independientes'), css_class="col-xl-12"),
                css_class="col-xl-12 mt-4 formset-independientes"),
            Div(Field(HTML(
                '<h4 style="color:#A32CDF">Experiencia Emprendedor</h4>'
                '<h5 class="mb-0">¿Te has animado a emprender un negocio en el que no trabajaras tú?</h5>')), css_class="col-xl-12"),
            Div(Fieldset('', Formset('emprendimientos'), css_class="col-xl-12"),
                css_class="col-xl-12 mt-4 formset-emprendimientos"),
            Div(Field(HTML(
                '<h4 style="color:#A32CDF">Experiencia Académica Colegio</h4>'
                '<h5 class="mb-4">¿Qué tan bueno eres en las siguientes asignaturas?</h5>'
            )), css_class="col-xl-12"),
            Div(Field('rendimiento_fisica', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_informatica', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_matematica', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_historia', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_literatura', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_idiomas_extranjeros', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_idiomas_nativos', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_artes', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_deportes', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_naturales', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_geografia', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_economia', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_quimica', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_salud', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
            Div(Field('rendimiento_militar', css_class="form-control"),
                css_class="col-sm-6 col-xs-12 col-md-4 col-lg-4 col-xl-4 mb-3"),
    )

    class Meta:
        model = Joven
        fields = ('rendimiento_fisica', 'rendimiento_informatica', 'rendimiento_matematica', 'rendimiento_historia',
                  'rendimiento_literatura', 'rendimiento_idiomas_extranjeros', 'rendimiento_idiomas_nativos',
                  'rendimiento_artes', 'rendimiento_deportes', 'rendimiento_naturales', 'rendimiento_geografia',
                  'rendimiento_economia', 'rendimiento_quimica', 'rendimiento_salud', 'rendimiento_militar')
        widgets = {
            'rendimiento_fisica': Select2Widget(), 'rendimiento_informatica': Select2Widget(),
            'rendimiento_matematica': Select2Widget(), 'rendimiento_historia': Select2Widget(),
            'rendimiento_literatura': Select2Widget(), 'rendimiento_idiomas_extranjeros': Select2Widget(),
            'rendimiento_idiomas_nativos': Select2Widget(), 'rendimiento_artes': Select2Widget(),
            'rendimiento_deportes': Select2Widget(), 'rendimiento_naturales': Select2Widget(),
            'rendimiento_geografia': Select2Widget(), 'rendimiento_economia': Select2Widget(),
            'rendimiento_quimica': Select2Widget(), 'rendimiento_salud': Select2Widget(),
            'rendimiento_militar': Select2Widget()
        }


class ClubesForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClubesForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs.update({'class': 'form-control'})
        self.fields['tribu'].widget.attrs.update({'class': 'form-control'})
        self.fields['tiempo'].widget.attrs.update({'class': 'form-control'})
        self.fields['pertenece_actualmente'].widget.attrs.update({'class': 'form-control'})
        self.fields['tribu'].queryset = Tribus.objects.filter(activo=True)

    class Meta:
        model = ClubesJuveniles
        fields = ('nombre', 'tribu', 'tiempo', 'pertenece_actualmente')
        widgets = {
            'tribu': Select2Widget(),
            'tiempo': Select2Widget(),
            'pertenece_actualmente': Select2Widget()
        }
        help_texts = {
            'tribu': 'Por ejemplo si es un club ecológico, la tribu es Moterra cuyo conocimiento es el cuidado y '
                     'la explotación responsable de los recursos naturales.',
        }


ClubesFormSet = inlineformset_factory(Joven, ClubesJuveniles, form=ClubesForm,
                                      fields=['nombre', 'tribu', 'tiempo', 'pertenece_actualmente'], extra=1, can_delete=True)


class VoluntariadosForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(VoluntariadosForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs.update({'class': 'form-control'})
        self.fields['tribu'].widget.attrs.update({'class': 'form-control'})
        self.fields['tiempo'].widget.attrs.update({'class': 'form-control'})
        self.fields['pertenece_actualmente'].widget.attrs.update({'class': 'form-control'})
        self.fields['tribu'].queryset = Tribus.objects.filter(activo=True)

    class Meta:
        model = Voluntariados
        fields = ('nombre', 'tribu', 'tiempo', 'pertenece_actualmente')
        widgets = {
            'tribu': Select2Widget(),
            'tiempo': Select2Widget(),
            'pertenece_actualmente': Select2Widget()
        }
        help_texts = {
            'tribu': 'Por ejemplo si tu voluntariado fue cuidando personas, la tribu de conocimiento es Vincularen '
                     'pero si ayudaste en el negocio de tu familia a organizar las cuentas la tribu será Gronor.'
        }


VoluntariadosFormSet = inlineformset_factory(Joven, Voluntariados, form=VoluntariadosForm,
                                             fields=['nombre', 'tribu', 'tiempo', 'pertenece_actualmente'], extra=1, can_delete=True)


class LaboralesForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(LaboralesForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs.update({'class': 'form-control'})
        self.fields['tribu'].widget.attrs.update({'class': 'form-control'})
        self.fields['tiempo'].widget.attrs.update({'class': 'form-control'})
        self.fields['pertenece_actualmente'].widget.attrs.update({'class': 'form-control'})
        self.fields['tribu'].queryset = Tribus.objects.filter(activo=True)

    class Meta:
        model = Laborales
        fields = ('nombre', 'tribu', 'tiempo', 'pertenece_actualmente')
        widgets = {
            'tribu': Select2Widget(),
            'tiempo': Select2Widget(),
            'pertenece_actualmente': Select2Widget()
        }
        help_texts = {
            'tribu': 'Por ejemplo si trabajaste en una producción audiovisual señala Brianimus, o si trabajaste en un '
                     'centro de conocimiento o con tecnología señala la tribu de los Centeros.'
        }


LaboralesFormSet = inlineformset_factory(Joven, Laborales, form=LaboralesForm,
                                         fields=['nombre', 'tribu', 'tiempo', 'pertenece_actualmente'], extra=1, can_delete=True)


class IndependientesForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(IndependientesForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs.update({'class': 'form-control'})
        self.fields['tribu'].widget.attrs.update({'class': 'form-control'})
        self.fields['tiempo'].widget.attrs.update({'class': 'form-control'})
        self.fields['pertenece_actualmente'].widget.attrs.update({'class': 'form-control'})
        self.fields['tribu'].queryset = Tribus.objects.filter(activo=True)

    class Meta:
        model = Independientes
        fields = ('nombre', 'tribu', 'tiempo', 'pertenece_actualmente')
        widgets = {
            'tribu': Select2Widget(),
            'tiempo': Select2Widget(),
            'pertenece_actualmente': Select2Widget()
        }
        help_texts = {
            'tribu': 'Por ejemplo si comerciabas con algún producto de salud señala la tribu Cuisaná.'
        }


IndependientesFormSet = inlineformset_factory(Joven, Independientes, form=IndependientesForm,
                                              fields=['nombre', 'tribu', 'tiempo', 'pertenece_actualmente'], extra=1, can_delete=True)


class EmprendimientosForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EmprendimientosForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs.update({'class': 'form-control'})
        self.fields['tribu'].widget.attrs.update({'class': 'form-control'})
        self.fields['tiempo'].widget.attrs.update({'class': 'form-control'})
        self.fields['pertenece_actualmente'].widget.attrs.update({'class': 'form-control'})
        self.fields['tribu'].queryset = Tribus.objects.filter(activo=True)

    class Meta:
        model = Emprendimientos
        fields = ('nombre', 'tribu', 'tiempo', 'pertenece_actualmente')
        widgets = {
            'tribu': Select2Widget(),
            'tiempo': Select2Widget(),
            'pertenece_actualmente': Select2Widget()
        }
        help_texts = {
            'tribu': 'Por ejemplo si tú fuiste el que pensó como ganar dinero y organizarlo, señala los Gronor, si tu '
                     'negocio era o es de comidas puedes señalar también Brianimus.',
            'nombre': 'La diferencia con el independiente es que el negocio funciona contigo o sin ti, el independiente'
                      ' vive de lo que él vende o hace.'
        }


EmprendimientosFormSet = inlineformset_factory(Joven, Emprendimientos, form=EmprendimientosForm,
                                               fields=['nombre', 'tribu', 'tiempo', 'pertenece_actualmente'], extra=1, can_delete=True)