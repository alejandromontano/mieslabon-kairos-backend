# Modulos Django
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Count, Q

# Modulos de plugin externos
from cities_light.models import Country, City, Region
from decimal import Decimal
from django_currentuser.db.models import CurrentUserField
from simple_history.models import HistoricalRecords

# Modulos de otras apps
from kairos.insignias.models import Tribus, Roles, NivelesDeConocimiento, RutaConocimiento, Familia
from kairos.perfiles_deseados.models import PerfilesDeseados


# Modulos internos de la app


class TipoModalidad(models.Model):
    nombre = models.CharField(max_length=50, verbose_name="nombre del tipo*", unique=True)
    activo = models.BooleanField(verbose_name="¿El tipo se encuentra activo?", default=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre


class Metodologia(models.Model):
    nombre = models.CharField(max_length=50, verbose_name="nombre de la metodología*", unique=True)
    activo = models.BooleanField(verbose_name="¿La metodología se encuentra activa?", default=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre


class Modalidad(models.Model):
    CONTENIDO = [
        ('ruta', 'Ruta'),
        ('oportunidad', 'Oportunidad'),
    ]
    contenido = models.CharField(max_length=20, choices=CONTENIDO, verbose_name="contenido al que pertenece*")
    tipo = models.ForeignKey('TipoModalidad', verbose_name="tipo de modalidad*", on_delete=models.CASCADE)
    metodologia = models.ForeignKey('Metodologia', verbose_name="metodología*", on_delete=models.CASCADE)
    activo = models.BooleanField(verbose_name="¿La metodología se encuentra activa?", default=True)
    codigo = models.PositiveIntegerField(verbose_name="Código de la modalidad", default=0)
    history = HistoricalRecords()

    def __str__(self):
        return self.tipo.nombre + ' - ' + self.metodologia.nombre

    def obtener_modalidad_por_codigo(codigo):
        try:
            return Modalidad.objects.get(codigo=codigo)
        except Modalidad.DoesNotExist:
            return None


class Contenido(models.Model):
    MONEDAS = [
        ('COP', 'Pesos Colombianos (COP)'),
        ('USD', 'Dólares (USD)'),
        ('PEN', 'Soles Peruanos (PEN)'),
        ('ARS', 'Pesos Argentinos (ARS)')
    ]
    PERIODICIDADES = [
        ('1', 'Único'),
        ('2', 'Mensual'),
        ('3', 'Trimestral'),
        ('4', 'Semestral'),
        ('5', 'Anual')
    ]
    NIVELES_CONOCIMIENTO = (
        ("1", "Hacedores"),
        ("2", "Expertos"),
        ("3", "Líderes de Nicho"),
        ("4", "Profesionales"),
        ("5", "Tecnólogos")
    )
    ROLES = (
        ("1", "Colaboradores"),
        ("2", "Emprendedores"),
        ("3", "Independientes"),
        ("4", "Voluntarios")
    )
    # BÁSICOS
    modalidad = models.ForeignKey('Modalidad', on_delete=models.CASCADE, verbose_name='modalidad*',
                                  related_name='modalidad_del_contenido')
    tipo = models.CharField(max_length=20, verbose_name="tipo de contenido")  # singular: ruta, oportunidad
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)
    organizacion = models.CharField(max_length=600, verbose_name="organización*")
    caracteristicas = models.TextField(verbose_name="características*")
    observaciones = models.TextField(verbose_name="observaciones*", null=False)
    continente = models.CharField(max_length=600, verbose_name='continentes')
    paises = models.CharField(max_length=600, verbose_name="paises", blank=True, null=True)
    regiones = models.CharField(max_length=600, verbose_name="regiones", blank=True, null=True)
    ciudades = models.CharField(max_length=600, verbose_name="ciudades", blank=True, null=True)
    quien_registra = CurrentUserField()
    niveles_conocimiento = models.ManyToManyField('insignias.NivelesDeConocimiento',
                                                  verbose_name="niveles de conocimiento*")
    roles = models.ManyToManyField('insignias.Roles', verbose_name="roles*")

    # DATOS DINERO
    observaciones_precio = models.TextField(verbose_name="observaciones del precio*")
    precio = models.CharField(verbose_name="precio ($)", max_length=600, blank=True, null=True)
    moneda = models.CharField(max_length=8, choices=MONEDAS, verbose_name='moneda', blank=True, null=True)
    periodicidad_pago = models.CharField(max_length=5, choices=PERIODICIDADES, verbose_name='periodicidad del pago',
                                         blank=True, null=True)
    # DATOS PLAZO
    observaciones_plazo = models.TextField(verbose_name="observaciones del plazo*")
    fecha_inicio = models.DateField(verbose_name="fecha inicio del plazo", blank=True, null=True)
    fecha_fin = models.DateField(verbose_name="fecha final del plazo", blank=True, null=True)
    es_periodico = models.BooleanField(verbose_name="¿Es periódico?", default=False)
    duracion = models.CharField(verbose_name="¿Cuánto dura?", max_length=350, blank=True, null=True)

    # DATOS DE CONTACTO
    contacto = models.CharField(max_length=100, verbose_name="contacto", blank=True, null=True)
    email = models.CharField(verbose_name="correo electrónico", max_length=200, blank=True, null=True)
    telefono = models.CharField(max_length=100, verbose_name='número de teléfono', null=True, blank=True)
    link = models.CharField(max_length=600, verbose_name="enlace* (url)")

    # TRIBUS
    tribus_relacionadas = models.ManyToManyField('insignias.Tribus', related_name='tribus_del_contenido')
    cuisana = models.BooleanField(verbose_name="cuisana", default=False)
    moterra = models.BooleanField(verbose_name="moterra", default=False)
    ponor = models.BooleanField(verbose_name="ponor", default=False)
    vincularem = models.BooleanField(verbose_name="vincularem", default=False)
    brianimus = models.BooleanField(verbose_name="brianimus", default=False)
    gronor = models.BooleanField(verbose_name="gronor", default=False)
    centeros = models.BooleanField(verbose_name="centeros", default=False)
    sersas = models.BooleanField(verbose_name="sersas", default=False)
    insignias_cuisana = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                               related_name='insignias_cuisana_contenido')
    insignias_moterra = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                               related_name='insignias_moterra_contenido')
    insignias_ponor = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                             related_name='insignias_ponor_contenido')
    insignias_vincularem = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                                  related_name='insignias_vincularem_contenido')
    insignias_brianimus = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                                 related_name='insignias_brianimus_contenido')
    insignias_gronor = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                              related_name='insignias_gronor_contenido')
    insignias_centeros = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                                related_name='insignias_centeros_contenido')
    insignias_sersas = models.ManyToManyField('insignias.RutaConocimiento', verbose_name="rutas de conocimiento",
                                              related_name='insignias_sersas_contenido')
    nombre = models.CharField(max_length=500, verbose_name="nombre*", null=True)
    activo = models.BooleanField(verbose_name="", default=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.organizacion)

    def crear_ruta(self, usuario):
        from kairos.core.utils import obtener_listado_de_palabras_limpias
        self.tipo = 'ruta'
        self.quien_registra = usuario
        self.save()
        palabras_claves = self.nombre.split(' ')
        palabras_limpias = obtener_listado_de_palabras_limpias(palabras_claves)
        RutaConocimiento.asignar_tribus_a_contenido_por_palabras_clave(palabras_limpias, self)

    def crear_oportunidad(self, usuario):
        from kairos.core.utils import obtener_listado_de_palabras_limpias
        self.tipo = 'oportunidad'
        self.quien_registra = usuario
        self.save()
        palabras_claves = self.nombre.split(' ')
        palabras_limpias = obtener_listado_de_palabras_limpias(palabras_claves)
        RutaConocimiento.asignar_tribus_a_contenido_por_palabras_clave(palabras_limpias, self)

    def obtener_tribus_relacionadas(self):
        return self.tribus_relacionadas.all()

    def obtener_rutas_de_conocimiento(self):
        rutas_cuisana = self.insignias_cuisana.all()
        rutas_moterra = rutas_cuisana | self.insignias_moterra.all()
        rutas_ponor = rutas_moterra | self.insignias_ponor.all()
        rutas_vincularem = rutas_ponor | self.insignias_vincularem.all()
        rutas_brianimus = rutas_vincularem | self.insignias_brianimus.all()
        rutas_insignias_gronor = rutas_brianimus | self.insignias_gronor.all()
        rutas_insignias_centeros = rutas_insignias_gronor | self.insignias_centeros.all()
        total_rutas = rutas_insignias_centeros | self.insignias_sersas.all()

        return total_rutas.distinct()

    def tipo_modalidad(self):
        '''
            Retorna la metodologia de la modalidad

            Parametros:
                self: El objeto Contenido que sera consultado.

            Retorno:
                metodologia: String
        '''
        return self.modalidad.metodologia

    def tribus(self):
        '''
            Retorna las tribus que estan asociadas al contenido.

            Parametros:
                self: El objeto Contenido que sera consultado.

            Retorno:
                tribus: Array de Strings
        '''
        tribus = []
        if self.cuisana:
            tribus.append('cuisana')
        if self.moterra:
            tribus.append('moterra')
        if self.ponor:
            tribus.append('ponor')
        if self.vincularem:
            tribus.append('vincularem')
        if self.brianimus:
            tribus.append('brianimus')
        if self.gronor:
            tribus.append('gronor')
        if self.centeros:
            tribus.append('centeros')
        if self.sersas:
            tribus.append('sersas')
        return tribus

    @staticmethod
    def validacion_duplicidad_rutas(texto):
        '''
            Retorna los primeros 5 Contenidos tipo Ruta que tengan un nombre similar al texto recibido como parametro

            Parametros:
                text: String que sera usado para buscar el Contenido

            Retorno:
                rutas: Array
        '''
        rutas = Contenido.objects.filter(modalidad__contenido='ruta', nombre__icontains=texto)[:5]
        return rutas

    @staticmethod
    def validacion_duplicidad_oportunidades(texto):
        '''
            Retorna los primeros 5 Contenidos tipo Oportunidad que tengan un nombre similar al texto recibido como parametro

            Parametros:
                text: String que sera usado para buscar el Contenido

            Retorno:
                rutas: Array
        '''
        oportunidades = Contenido.objects.filter(modalidad__contenido='oportunidad', nombre__icontains=texto)[:5]
        return oportunidades

    @staticmethod
    def get_queryset_modelos(query=None, modelos=None):
        '''
            Realiza una busqueda de los modelos que tengan datos relacionados con el query que se recibe como parametro

            Parametros:
                query: String tiene los datos a consultar
                modelos: Modelo al que se le realizara la busqueda

            Retorno:
                modelos: Queryset
        '''
        queries = query.split(' ')
        for q in queries:
            tribus = Tribus.objects.filter(nombre__icontains=q)
            roles = Roles.objects.filter(nombre__icontains=q)
            niveles_conocimiento = NivelesDeConocimiento.objects.filter(nombre__icontains=q)
            modelos = modelos.filter(Q(nombre__icontains=q) | Q(paises__icontains=q)
                                     | Q(regiones__icontains=q) | Q(ciudades__icontains=q)
                                     | Q(organizacion__icontains=q) | Q(observaciones__icontains=q)
                                     | Q(caracteristicas__icontains=q)
                                     | Q(roles__in=roles)
                                     | Q(tribus_relacionadas__in=tribus)
                                     ).distinct()
        return modelos

    @staticmethod
    def get_queryset_tag(tag=None, modelos=None):
        '''
            Realiza una busqueda de los modelos que tengan datos relacionados con el tag donde el tipo de modalidad sea diferente a esta

            Parametros:
                tag: String tiene los datos a consultar
                modelos: Modelo al que se le realizara la busqueda

            Retorno:
                modelos: Queryset
        '''
        modelos = modelos.filter(modalidad__tipo__nombre__iexact=tag).distinct()
        return modelos

    @staticmethod
    def cargue_masivo(dataframe, tipo):
        '''
            Realiza el cargue masivo de Contenidos con un dataframe

            Parametros:
                dataframe: Contiene los datos que se agregaran
                tipo: Determina el tipo de Contenido(Ruta, Oportunidad)

            Retorno:
                si hay errores: Mensaje con el error
        '''
        from decimal import Decimal
        import numpy as np
        from kairos.core.utils import obtener_listado_de_palabras_limpias

        dataframe = dataframe.replace(np.nan, '', regex=True)

        if tipo == 'oportunidad':

            dataframe.columns = ['Modalidad', 'Pais', 'Departamento', 'Ciudad', 'nombre_org',
                                 'nombre_item', 'Observaciones', 'Precio', 'URL', 'Contacto',
                                 'Colaboradores', 'Emprendedores', 'Independientes', 'Voluntarios'
                                 ]
            errores = []

            for index, row in dataframe.iterrows():
                try:
                    print("###########", index, '-', row)

                    if row['Modalidad']:
                        modalidad = Modalidad.obtener_modalidad_por_codigo(int(row['Modalidad']))
                        if modalidad:
                            modalidad = modalidad
                    if row['Observaciones']:
                        observaciones = row['Observaciones']
                    if row['Pais']:
                        paises = row['Pais']
                    if row['Departamento']:
                        regiones = row['Departamento']
                    if row['Ciudad']:
                        ciudades = row['Ciudad']
                    if row['nombre_org']:
                        organizacion = row['nombre_org']
                    if row['Precio']:
                        precio = row['Precio']
                    if row['URL']:
                        link = row['URL']
                    if row['Contacto']:
                        contacto = row['Contacto']
                    if row['nombre_item']:
                        nombre = row['nombre_item']

                    contenido = Contenido.objects.create(nombre=nombre, tipo=tipo,
                                                         modalidad=modalidad, observaciones=observaciones,
                                                         paises=paises, regiones=regiones, ciudades=ciudades,
                                                         organizacion=organizacion, precio=precio, link=link,
                                                         contacto=contacto
                                                         )

                    if row['Colaboradores'] != '0':
                        rol = Roles.obtener_por_nombre('Colaborador')
                        contenido.roles.add(rol)
                    if row['Emprendedores'] != '0':
                        rol = Roles.obtener_por_nombre('Emprendedor')
                        contenido.roles.add(rol)
                    if row['Independientes'] != '0':
                        rol = Roles.obtener_por_nombre('Independiente')
                        contenido.roles.add(rol)
                    if row['Voluntarios'] != '0':
                        rol = Roles.obtener_por_nombre('Voluntario')
                        contenido.roles.add(rol)
                    if row['nombre_item']:
                        palabras_claves = row['nombre_item'].split(' ')
                        palabras_limpias = obtener_listado_de_palabras_limpias(palabras_claves)
                        RutaConocimiento.asignar_tribus_a_contenido_por_palabras_clave(palabras_limpias, contenido)

                    mis_tribus = contenido.tribus()
                    for mi_tribu in mis_tribus:
                        tribu = Tribus.objects.get(nombre__icontains=mi_tribu)
                        contenido.tribus_relacionadas.add(tribu)
                    contenido.save()
                except Exception as e:
                    print(e)
                    errores.append(index + 1)
            return errores
        else:

            dataframe.columns = ['Modalidad', 'Pais', 'Departamento', 'Ciudad', 'nombre_org',
                                 'nombre_item', 'Observaciones', 'Precio', 'es_periodico', 'duracion', 'URL',
                                 'Contacto',
                                 'Nivel de conocimiento'
                                 ]
            errores = []

            for index, row in dataframe.iterrows():
                try:
                    print("###########", index, '-', row)

                    if row['Modalidad']:
                        modalidad = Modalidad.obtener_modalidad_por_codigo(row['Modalidad'])
                        if modalidad:
                            modalidad = modalidad
                    if row['Observaciones']:
                        observaciones = row['Observaciones']
                    if row['Pais']:
                        paises = row['Pais']
                    if row['Departamento']:
                        regiones = row['Departamento']
                    if row['Ciudad']:
                        ciudades = row['Ciudad']
                    if row['nombre_org']:
                        organizacion = row['nombre_org']
                    if row['Precio']:
                        precio = row['Precio']
                    if row['URL']:
                        link = row['URL']
                    if row['Contacto']:
                        contacto = row['Contacto']
                    if row['nombre_item']:
                        nombre = row['nombre_item']
                    if row['duracion']:
                        duracion = row['duracion']
                    if row['es_periodico']:
                        es_periodico = row['es_periodico']
                        if es_periodico:
                            es_periodico = True
                        else:
                            es_periodico = False

                    contenido = Contenido.objects.create(
                        nombre=nombre, tipo=tipo, modalidad=modalidad,
                        paises=paises, regiones=regiones, ciudades=ciudades, observaciones=observaciones,
                        organizacion=organizacion, precio=precio, link=link, contacto=contacto, duracion=duracion,
                        es_periodico=es_periodico
                    )

                    if row['Nivel de conocimiento']:
                        nivel = NivelesDeConocimiento.obtener_por_nombre(row['Nivel de conocimiento'])
                        contenido.niveles_conocimiento.add(nivel)
                    if row['nombre_item']:
                        palabras_claves = row['nombre_item'].split(' ')
                        palabras_limpias = obtener_listado_de_palabras_limpias(palabras_claves)
                        RutaConocimiento.asignar_tribus_a_contenido_por_palabras_clave(palabras_limpias, contenido)

                    mis_tribus = contenido.tribus()
                    for mi_tribu in mis_tribus:
                        tribu = Tribus.objects.get(nombre__icontains=mi_tribu)
                        contenido.tribus_relacionadas.add(tribu)
                    contenido.save()
                except Exception as e:
                    print(e)
                    errores.append(index + 1)
            return errores

    @staticmethod
    def sugeridas(usuario, tipo):
        '''
            Retorna los Contenidos sugeridos para un usuario especifico

            Parametros:
                usuario: Usuario a la que se le consultaran sus sugeridos
                tipo: Determina el tipo de Contenido(Ruta, Oportunidad)

            Retorno:
                Queryset: Contenidos
        '''
        mi_perfil_favorito = PerfilesDeseados.objects.filter(joven_id=usuario.pk, favorito=True)
        if mi_perfil_favorito.exists():
            mi_perfil_favorito = mi_perfil_favorito.first()
            campo = mi_perfil_favorito.rutas_conocimiento
            mi_nivel = mi_perfil_favorito.nivel_conocimiento_deseado
            contenidos_favoritos = Contenido.objects.exclude(calificacion_del_joven__usuario=usuario,
                                                             calificacion_del_joven__calificacion=0)
            contenidos = contenidos_favoritos.filter(tipo=tipo, activo=True)
            q = Q()
            if mi_perfil_favorito.interes_voluntariado:
                q = q | Q(nombre__icontains='voluntario')
            if mi_perfil_favorito.interes_trabajo:
                q = q | Q(nombre__icontains='colaborador')
            if mi_perfil_favorito.interes_independiente:
                q = q | Q(nombre__icontains='independiente')
            if mi_perfil_favorito.interes_emprendedor:
                q = q | Q(nombre__icontains='emprendedor')
            #mis_roles = Roles.objects.filter(q)

            q = Q(insignias_cuisana__id=campo.pk) | Q(insignias_moterra__id=campo.pk)\
                | Q(insignias_ponor__id=campo.pk) \
                | Q(insignias_vincularem__id=campo.pk) | \
                Q(insignias_brianimus__id=campo.pk) | \
                Q(insignias_gronor__id=campo.pk) | Q(insignias_centeros__id=campo.pk)\
                | Q(insignias_sersas__id=campo.pk)

            if tipo == 'ruta':
                mis_contenidos = contenidos.filter(
                    Q(q & Q(niveles_conocimiento__id__exact=mi_nivel.pk))).distinct()
            elif tipo == 'oportunidad':
                mis_contenidos = contenidos.filter(q).distinct()

            return mis_contenidos
        else:
            return Contenido.objects.none()

    @staticmethod
    def obtener_me_gusta(tipo) -> 'Queryset<Contenido>':
        '''
            Retorna los contenidos con el tipo recibido como parametro que tinene un me gusta

            Parametros:
                tipo: Determina el tipo de Contenido(Ruta, Oportunidad)

            Retorno:
                Queryset: Contenidos
        '''
        contenido = Contenido.objects.filter(tipo=tipo, activo=True)
        mis_favoritos = contenido.filter(calificacion_del_joven__calificacion=1)
        return mis_favoritos.distinct()

    @staticmethod
    def obtener_me_gusta_ordenado(tipo, jovenes) -> 'Queryset<Contenido>':
        '''
            Retorna los contenidos con el tipo recibido como parametro que tinene un me gusta

            Parametros:
                tipo: Determina el tipo de Contenido(Ruta, Oportunidad)

            Retorno:
                Queryset: Contenidos
        '''
        ids_jovenes = list(jovenes.values_list('id', flat=True))
        mis_favoritos = Contenido.objects.filter(
            tipo=tipo,
            activo=True,
            calificacion_del_joven__calificacion=1,
            calificacion_del_joven__usuario_id__in=ids_jovenes
        ).annotate(cant=Count('calificacion_del_joven'))

        return mis_favoritos.distinct().order_by('-cant')

    @staticmethod
    def obtener_no_me_gusta(tipo):
        '''
            Retorna los contenidos con el tipo recibido como parametro que tinene un NO me gusta

            Parametros:
                tipo: Determina el tipo de Contenido(Ruta, Oportunidad)

            Retorno:
                Queryset: Contenidos
        '''
        contenido = Contenido.objects.filter(tipo=tipo, activo=True)
        mis_favoritos = contenido.filter(calificacion_del_joven__calificacion=0)
        return mis_favoritos.distinct()

    @staticmethod
    def obtener_contenidos(tipo: str):
        '''
            Retorna los contenidos de un tipo especifico

            Parametros:
                tipo(str): Determina el tipo de Contenido(Ruta, Oportunidad)

            Retorno:
                Queryset: Contenidos
        '''
        contenido = Contenido.objects.filter(tipo=tipo, activo=True)
        return contenido

    @staticmethod
    def obtener_activos():
        contenido = Contenido.objects.filter(activo=True)
        return contenido

    @staticmethod
    def favoritas(usuario, tipo):
        '''
            Retorna los contenidos favoritos de un usuario y un tipo especifico

            Parametros:
                usuario: Usuario a la que se le consultaran sus sugeridos
                tipo: Determina el tipo de Contenido(Ruta, Oportunidad)

            Retorno:
                Queryset: Contenidos
        '''
        contenido = Contenido.objects.filter(tipo=tipo, activo=True)
        mis_favoritos = contenido.filter(calificacion_del_joven__usuario=usuario,
                                         calificacion_del_joven__calificacion=1)
        return mis_favoritos.distinct()

    @staticmethod
    def decisiones_tomadas(usuario, tipo):
        '''
            Retorna los contenidos a los que ha decidido de un usuario y un tipo especifico

            Parametros:
                usuario: Usuario a la que se le consultaran sus sugeridos
                tipo: Determina el tipo de Contenido(Ruta, Oportunidad)

            Retorno:
                Queryset: Contenidos
        '''
        contenido = Contenido.objects.filter(tipo=tipo, activo=True)
        mis_decisiones = contenido.filter(calificacion_del_joven__usuario=usuario,
                                          calificacion_del_joven__decision=1)
        return mis_decisiones.distinct()

    @staticmethod
    def favoritas_tribu(usuario, tipo, tribu):
        '''
            Retorna los contenidos favoritos de un usuario, un tipo especifico y una tribu

            Parametros:
                usuario: Usuario a la que se le consultaran sus sugeridos
                tipo: Determina el tipo de Contenido(Ruta, Oportunidad)
                tribu: Determina el tipo de tribu(Cuisana, Moterra, Ponor, Vincularem, Brianimus, Gronor, Centeros,
                       Sersas)
            Retorno:
                Queryset: Contenidos
        '''
        mis_favoritos = Contenido.favoritas(usuario, tipo).filter(tribus_relacionadas__nombre=tribu)
        return mis_favoritos.distinct()

    @staticmethod
    def obtener_cantidad_todo_tipo(tipo: str):
        '''
            Retorna los contenidos favoritos de un usuario y un tipo especifico

            Parametros:
                usuario: Usuario a la que se le consultaran sus sugeridos
                tipo(str): Determina el tipo de Contenido(Ruta, Oportunidad)

            Retorno:
                Queryset: Contenidos
        '''
        contenido = Contenido.objects.select_related('modalidad').prefetch_related('niveles_conocimiento', 'roles',
                                                                                   'tribus_relacionadas').filter(
            tipo=tipo, activo=True)
        return contenido

    @staticmethod
    def obtener_contenidos_propias(usuario, tipo):
        '''
            Retorna los contenidos creados una organizacion especifica

            Parametros:
                usuario: Usuario a la que se le consultaran sus sugeridos
                tipo: Determina el tipo de Contenido(Ruta, Oportunidad)

            Retorno:
                Queryset: Contenidos
        '''
        if usuario.is_superuser == True:
            mis_contenidos = Contenido.obtener_contenidos(tipo)
        else:
            contenido = Contenido.obtener_contenidos(tipo)
            mis_contenidos = contenido.filter(quien_registra=usuario)
        return mis_contenidos.distinct()

    @staticmethod
    def busqueda_filtro(usuario: 'Usuario', filtro: str, direccion: str, columna_orden: str) -> 'Queryset<Contenido>':
        """
            Retorna los contenidos ordenados por columna_orden y que cumplan con el filtro

            Parametros:
                usuario: Usuario que tendrá las rutas.
                filtro: Cadena que contiene la o las palabras para hacer el filtro.
                direccion: Cadena que indicará en que orden(asc o desc) debe ir la columna_orden.
                columna_orden: Cadena con el nombre de la columna para ordenar los contenidos.

            Retorno:
                Queryset: Contenidos
        """
        rutas = Contenido.obtener_contenidos_propias(usuario, 'ruta')
        filtrado = rutas.filter(
            Q(nombre__icontains=filtro) |
            Q(modalidad__tipo__nombre__icontains=filtro) |
            Q(modalidad__metodologia__nombre__icontains=filtro) |
            Q(organizacion__icontains=filtro) |
            Q(tribus_relacionadas__nombre__icontains=filtro) |
            Q(observaciones__icontains=filtro) |
            Q(precio__icontains=filtro)
        ).order_by(direccion + columna_orden)

        return filtrado


class CalificacionContenido(models.Model):
    contenido = models.ForeignKey('Contenido', on_delete=models.CASCADE, verbose_name='contenido calificado',
                                  related_name='calificacion_del_joven')
    usuario = models.ForeignKey('usuarios.Usuario', on_delete=models.CASCADE, verbose_name='usuario que califica',
                                related_name='usuario_calificador_contenido')
    calificacion = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(2), MinValueValidator(0)],
                                               verbose_name='calificación: 1 me gusta, 0 no me gusta')
    decision = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(1), MinValueValidator(0)])
    history = HistoricalRecords()

    @staticmethod
    def decisiones_tomadas(usuario_id: int) -> 'Queryset<CalificacionContenido>':
        '''
            Retorna las calificaciones a las que ha decidido un usuario

            Parametros:
                usuario: Usuario a la que se le consultaran sus sugeridos

            Retorno:
                Queryset: CalificacionContenido
        '''
        calificaciones = CalificacionContenido.objects.filter(usuario_id=usuario_id, decision=1)
        return calificaciones.distinct()


class ContenidoVisitado(models.Model):
    contenido = models.ForeignKey('Contenido', on_delete=models.CASCADE, verbose_name='contenido calificado',
                                  related_name='contenido_visitado_del_usuario')
    usuario = models.ForeignKey('usuarios.Usuario', on_delete=models.CASCADE, verbose_name='usuario que califica',
                                related_name='usuario_visitador_contenido')
    visitado = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                           verbose_name='calificación: 1 visitado, 0 no visitado')
    history = HistoricalRecords()
