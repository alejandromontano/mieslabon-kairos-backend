# Modulos Django
from django import forms
from django.db.models import Q

# Modulos de plugin externos
from django_select2.forms import Select2Widget, Select2MultipleWidget, ModelSelect2Widget, ModelSelect2MultipleWidget
from cities_light.models import City, Country, Region

# Modulos de otras apps
from kairos.insignias.models import RutaConocimiento, NivelesDeConocimiento, Roles, Familia

# Modulos internos de la app
from .models import Contenido, Modalidad, Metodologia, TipoModalidad
from .utils import *

CONTINENTES = [
    ('América del Norte', 'América del Norte'),
    ('Sur América', 'Sur América'),
    ('Europa', 'Europa'),
    ('Asia', 'Asia'),
    ('África', 'África'),
    ('Oceanía', 'Oceanía')
]


class DependentMultipleSelectWidget(ModelSelect2MultipleWidget):
    def filter_queryset(self, request, term, queryset=None, **dependent_fields):
        # The super code ignores the dependent fields with multiple values
        dependent_fields_multi_value = {}
        for form_field_name, model_field_name in self.dependent_fields.items():
            if form_field_name+"[]" in request.GET and request.GET.get(form_field_name+"[]", "") != "":
                value_list = request.GET.getlist(form_field_name+"[]")
                dependent_fields_multi_value[model_field_name] = value_list
                print(value_list)

        if queryset is None:
            queryset = self.get_queryset()
        search_fields = self.get_search_fields()
        select = Q()
        term = term.replace('\t', ' ')
        term = term.replace('\n', ' ')
        for t in [t for t in term.split(' ') if not t == '']:
            select &= reduce(lambda x, y: x | Q(**{y: t}), search_fields,
                             Q(**{search_fields[0]: t}))
        
        # TODO - Q doesnt support list of values, will have to add something of our own
       
        filtro = dict()
        for atributo, valor in dependent_fields_multi_value.items():
            filtro["{}__in".format(atributo)] = valor
        ################################################################################
        
        if dependent_fields:
            select &= Q(**dependent_fields)
        if dependent_fields_multi_value:
            queryset = queryset.filter(**filtro)

        return queryset.filter(select).distinct()

class RutaModelForm(forms.ModelForm):
    organizacion = ChoiceFieldNoValidation(choices=[], label='Organización*')
    paises = ChoiceFieldNoValidation(choices=[], label='País*')
    regiones = ChoiceFieldNoValidation(choices=[], label='Región*')
    ciudades = ChoiceFieldNoValidation(choices=[], label='Ciudad*')

    def __init__(self, *args, **kwargs):
        super(RutaModelForm, self).__init__(*args, **kwargs)
        self.fields["modalidad"].queryset = Modalidad.objects.filter(contenido='ruta', activo=True)
        self.fields["nombre"].label = 'Nombre de la ruta*'
        self.fields["activo"].label = '¿La ruta se encuentra activa?'
        self.fields["niveles_conocimiento"].queryset = NivelesDeConocimiento.objects.filter(activo=True)

        instance = kwargs.get('instance', None)
        if instance != None:
            self.fields['organizacion'].choices = [(instance.organizacion, instance.organizacion)]
            self.fields['paises'].choices = [(instance.paises, instance.paises)]
            self.fields['regiones'].choices = [(instance.regiones, instance.regiones)]
            self.fields['ciudades'].choices = [(instance.ciudades, instance.ciudades)]
    class Meta:
        model = Contenido
        fields = ['nombre', 'modalidad', 'organizacion', 'observaciones', 'paises',
                  'regiones', 'ciudades', 'activo', 'precio', 'es_periodico', 'duracion', 'contacto',
                  'link', 'niveles_conocimiento']

        widgets = {
            'observaciones': forms.Textarea(
                attrs={
                    'rows': 2,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'modalidad': Select2Widget(),
            'moneda': Select2Widget(),
        }


class AsignarOportunidadesModelForm(forms.ModelForm):
    CHOICES = []
    #for opcion in Contenido.obtener_contenidos('oportunidad').distinct('organizacion'):
    #    CHOICES.append((opcion.pk, opcion.organizacion))
    organizacion = forms.MultipleChoiceField(
        choices=CHOICES,
        widget=Select2MultipleWidget(),
        required=True,
        label="Organización*"
    )
    def __init__(self, *args, **kwargs):
        super(AsignarOportunidadesModelForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Contenido
        fields = ['organizacion']
        widgets = {
            'organizacion':Select2MultipleWidget()
        }

class AsignarRutasModelForm(forms.ModelForm):
    CHOICES = []
    #for opcion in Contenido.obtener_contenidos('ruta').distinct('organizacion'):
    #    CHOICES.append((opcion.pk, opcion.organizacion))
    organizacion = forms.MultipleChoiceField(
        choices=CHOICES,
        widget=Select2MultipleWidget(),
        required=True,
        label="Organización*"
    )
   
    def __init__(self, *args, **kwargs):
        super(AsignarRutasModelForm, self).__init__(*args, **kwargs)
        
    class Meta:
        model = Contenido
        fields = ['organizacion']
        widgets = {
            'organizacion':Select2MultipleWidget()
        }

class AsignarPalabrasForm(forms.Form):
    rutas_de_conocimiento = forms.ModelChoiceField(
        required=False,
        queryset=RutaConocimiento.objects.all(),
        label="Elija el campo al cual quiere asignar la palabra clave",
        widget=ModelSelect2Widget(
            model=RutaConocimiento,
            search_fields=['name__icontains', 'id__icontains'],
            max_results=100,
            queryset=RutaConocimiento.objects.all(),
        )
    )

    

class OportunidadModelForm(forms.ModelForm):
    organizacion = ChoiceFieldNoValidation(choices=[], label='Organización*')
    paises = ChoiceFieldNoValidation(choices=[], label='País*')
    regiones = ChoiceFieldNoValidation(choices=[], label='Región*')
    ciudades = ChoiceFieldNoValidation(choices=[], label='Ciudad*')


    def __init__(self, *args, **kwargs):
        super(OportunidadModelForm, self).__init__(*args, **kwargs)
        self.fields["modalidad"].queryset = Modalidad.objects.filter(contenido='oportunidad', activo=True)
        self.fields["nombre"].label = 'Nombre de la oportunidad*'
        self.fields["activo"].label = '¿La oportunidad se encuentra activa?'
        self.fields["roles"].queryset = Roles.objects.filter(activo=True)

        instance = kwargs.get('instance', None)
        if instance != None:
            self.fields['organizacion'].choices = [(instance.organizacion, instance.organizacion)]
            self.fields['paises'].choices = [(instance.paises, instance.paises)]
            self.fields['regiones'].choices = [(instance.regiones, instance.regiones)]
            self.fields['ciudades'].choices = [(instance.ciudades, instance.ciudades)]

    class Meta:
        model = Contenido
        fields = ['nombre', 'modalidad', 'organizacion', 'observaciones', 'paises',
                  'regiones', 'ciudades', 'activo', 'precio', 'contacto', 'link', 'roles']

        widgets = {
            'observaciones': forms.Textarea(
                attrs={
                    'rows': 2,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'modalidad': Select2Widget(),
            'periodicidad_pago': Select2Widget(),
        }


class MetodologiaModelForm(forms.ModelForm):
    class Meta:
        model = Metodologia
        fields = ['nombre', 'activo']


class TipoModalidadModelForm(forms.ModelForm):
    class Meta:
        model = TipoModalidad
        fields = ['nombre', 'activo']


class ModalidadModelForm(forms.ModelForm):
    class Meta:
        model = Modalidad
        fields = ['contenido', 'tipo', 'metodologia', 'activo']

        widgets = {
            'contenido': Select2Widget(),
            'tipo': ModelSelect2Widget(
                model=TipoModalidad,
                queryset=TipoModalidad.objects.filter(activo=True),
                search_fields=['nombre__icontains'],
                max_results=100,
            ),
            'metodologia': ModelSelect2Widget(
                model=Metodologia,
                queryset=Metodologia.objects.filter(activo=True),
                search_fields=['nombre__icontains'],
                max_results=100,
            ),
        }
