from rest_framework import serializers
from kairos.insignias.models import RutaConocimiento
from .models import Contenido


class RutaConocimientoSerializer(serializers.ModelSerializer):
    familia = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = RutaConocimiento
        fields = ['nombre', 'familia']

class RutaSerializer(serializers.ModelSerializer):
    modalidad = serializers.StringRelatedField()
    tribus_relacionadas = serializers.StringRelatedField(many=True)
    insignias_ponor = RutaConocimientoSerializer(many=True, read_only=True)
    insignias_cuisana = RutaConocimientoSerializer(many=True, read_only=True)
    insignias_moterra = RutaConocimientoSerializer(many=True, read_only=True)
    insignias_vincularem =RutaConocimientoSerializer(many=True, read_only=True)
    insignias_brianimus = RutaConocimientoSerializer(many=True, read_only=True)
    insignias_gronor = RutaConocimientoSerializer(many=True, read_only=True)
    insignias_centeros = RutaConocimientoSerializer(many=True, read_only=True)
    insignias_sersas = RutaConocimientoSerializer(many=True, read_only=True)

    class Meta:
        model = Contenido
        fields = [
            'id','nombre', 'tribus_relacionadas', 
            'modalidad', 'organizacion', 'observaciones', 
            'activo', 'precio', 'link', 'insignias_ponor',
            'insignias_cuisana', 'insignias_moterra', 'insignias_vincularem',
            'insignias_brianimus', 'insignias_gronor', 'insignias_centeros', 'insignias_sersas'
        ]


