# Modulos Django
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DetailView, TemplateView, ListView, FormView, DeleteView
from django.db.models import Q
# Modulos de plugin externos
from cities_light.models import Country, City, Region
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
import json

# Modulos de otras apps
from kairos.core.mixins import MensajeMixin
from kairos.insignias.models import PalabraClaveExcluida, RutaConocimiento, Tribus
from kairos.guia.models import VerRecurso
from kairos.organizaciones.models import Organizacion
from kairos.usuarios.models import Joven, Usuario

# Modulos internos de la app
from .forms import *
from .models import *
from .serializers import *

"""
class RutasListado(LoginRequiredMixin, ListView):
    model = Contenido
    context_object_name = "rutas"
    template_name = "rutas/listado.html"

    def get_queryset(self):
        rutas = Contenido.obtener_contenidos_propias(self.request.user, 'ruta')
        return rutas
"""


class RutasListado(LoginRequiredMixin, TemplateView):
    template_name = 'rutas/listado.html'
    mensaje_log = "Listado de rutas"


class APIRutasListado(LoginRequiredMixin, APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request, format=None):
        inicio = int(request.GET.get("inicio"))
        final = int(request.GET.get("limite"))
        filtro = str(request.GET.get("filtro"))
        orden = str(request.GET.get("orden"))
        direccion = str(request.GET.get("direccion"))

        filtrado = Contenido.busqueda_filtro(
            usuario=self.request.user,
            filtro=filtro,
            direccion=direccion,
            columna_orden=orden
        )
        paginado = filtrado[inicio:inicio + final]
        serializer = RutaSerializer(paginado, many=True)
        return Response({
            "draw": request.GET.get("draw"),
            "total": filtrado.count(),
            "data": serializer.data
        })


class RutasConsultar(LoginRequiredMixin, ListView):
    model = Contenido
    context_object_name = "rutas"
    template_name = "rutas/consultar.html"

    def get_queryset(self):
        rutas = Contenido.objects.filter(tipo='ruta')
        return rutas


class MisRutasCreadasListado(LoginRequiredMixin, ListView):
    model = Contenido
    context_object_name = "rutas"
    template_name = "rutas/listado.html"

    def get_queryset(self):
        rutas = Contenido.objects.filter(tipo='ruta', quien_registra=self.request.user)
        return rutas


class RutasRegistrar(LoginRequiredMixin, CreateView):
    model = Contenido
    form_class = RutaModelForm
    template_name = "rutas/registrar.html"
    success_url = reverse_lazy('contenido:registrar_ruta')

    def form_valid(self, form):
        usuario = self.request.user
        ruta = form.save(commit=False)
        ruta.crear_ruta(usuario)
        mis_tribus = form.instance.tribus()
        for mi_tribu in mis_tribus:
            tribu = Tribus.objects.get(nombre__icontains=mi_tribu)
            form.instance.tribus_relacionadas.add(tribu)
        form.save_m2m()
        messages.success(self.request, "Ruta registrada satisfactoriamente")
        return super(RutasRegistrar, self).form_valid(form)


class RutasModificar(LoginRequiredMixin, UpdateView):
    model = Contenido
    form_class = RutaModelForm
    pk_url_kwarg = 'id_ruta'
    template_name = "rutas/modificar.html"

    def form_valid(self, form):
        messages.success(self.request, "Ruta modificada satisfactoriamente")
        mis_tribus = form.instance.tribus()
        mis_tribus_antes = form.instance.tribus_relacionadas.all()
        for mi_tribu_antes in mis_tribus_antes:
            if not str(mi_tribu_antes.nombre).lower() in mis_tribus:
                form.instance.tribus_relacionadas.delete(mi_tribu_antes)
        for mi_tribu in mis_tribus:
            tribu = Tribus.objects.get(nombre__icontains=mi_tribu)
            if not tribu in mis_tribus_antes:
                form.instance.tribus_relacionadas.add(tribu)
        return super(RutasModificar, self).form_valid(form)

    def get_success_url(self):
        if self.request.user.es_superusuario:
            return reverse_lazy('contenido:listado_rutas')
        else:
            return reverse_lazy('contenido:listado_mis_rutas_creadas')


class AsignarRutasOrganizacion(LoginRequiredMixin, FormView):
    model = Contenido
    context_object_name = "oportunidades"
    pk_url_kwarg = 'id_organizacion'
    template_name = "oportunidades/asignar_organizacion.html"
    form_class = AsignarRutasModelForm
    success_url = reverse_lazy('organizaciones:listado')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")

        if Organizacion.existe_organizacion(self.kwargs['id_organizacion']) == False:
            messages.error(request, "La organización no existe")
            return redirect("organizaciones:listado")

        return super(AsignarRutasOrganizacion, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        elemento = form.save(commit=False)
        organizacion = Organizacion.objects.get(id=self.kwargs['id_organizacion'])
        oportunidades = self.request.POST.getlist('organizacion')

        lista_id_numerico = []
        for id_oportunidad in oportunidades:
            lista_id_numerico.append(int(id_oportunidad))

        contenidos = Contenido.obtener_contenidos('ruta').filter(id__in=lista_id_numerico)
        lista_nombres_organizaciones = []
        for contenido in contenidos:
            lista_nombres_organizaciones.append(contenido.organizacion)

        contenidos_actualizar = Contenido.obtener_contenidos('ruta').filter(
            organizacion__in=lista_nombres_organizaciones)
        for contenido_actualizar in contenidos_actualizar:
            contenido_actualizar.quien_registra = organizacion
            contenido_actualizar.save()

        messages.success(self.request, "Rutas asignadas satisfactoriamente")
        return super(AsignarRutasOrganizacion, self).form_valid(form)


class AsignarOportunidadesOrganizacion(LoginRequiredMixin, FormView):
    model = Contenido
    context_object_name = "oportunidades"
    pk_url_kwarg = 'id_organizacion'
    template_name = "oportunidades/asignar_organizacion.html"
    form_class = AsignarOportunidadesModelForm
    success_url = reverse_lazy('organizaciones:listado')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")

        if Organizacion.existe_organizacion(self.kwargs['id_organizacion']) == False:
            messages.error(request, "La organización no existe")
            return redirect("organizaciones:listado")
        return super(AsignarOportunidadesOrganizacion, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator
        from kairos.guia.models import Recurso
        context = super(AsignarOportunidadesOrganizacion, self).get_context_data(**kwargs)
        oportunidades = Contenido.obtener_contenidos('oportunidad').distinct('organizacion').values('organizacion')
        context['oportunidades'] = oportunidades
        return context

    def form_valid(self, form):
        elemento = form.save(commit=False)
        organizacion = Organizacion.objects.get(id=self.kwargs['id_organizacion'])
        oportunidades = self.request.POST.getlist('organizacion')

        lista_id_numerico = []
        for id_oportunidad in oportunidades:
            lista_id_numerico.append(int(id_oportunidad))

        contenidos = Contenido.obtener_contenidos('oportunidad').filter(id__in=lista_id_numerico)
        lista_nombres_organizaciones = []
        for contenido in contenidos:
            lista_nombres_organizaciones.append(contenido.organizacion)

        contenidos_actualizar = Contenido.obtener_contenidos('oportunidad').filter(
            organizacion__in=lista_nombres_organizaciones)
        for contenido_actualizar in contenidos_actualizar:
            contenido_actualizar.quien_registra = organizacion
            contenido_actualizar.save()

        messages.success(self.request, "Oportunidades asignadas satisfactoriamente")
        return super(AsignarOportunidadesOrganizacion, self).form_valid(form)


class OportunidadesListado(LoginRequiredMixin, ListView):
    model = Contenido
    context_object_name = "oportunidades"
    template_name = "oportunidades/listado.html"

    def get_queryset(self):
        oportunidades = Contenido.obtener_contenidos_propias(self.request.user, 'oportunidad')
        return oportunidades


class MisOportunidadesCreadasListado(LoginRequiredMixin, ListView):
    model = Contenido
    context_object_name = "oportunidades"
    template_name = "oportunidades/listado.html"

    def get_queryset(self):
        oportunidades = Contenido.objects.filter(tipo='oportunidad', quien_registra=self.request.user)
        return oportunidades


class OportunidadesConsultar(LoginRequiredMixin, ListView):
    model = Contenido
    context_object_name = "oportunidades"
    template_name = "oportunidades/consultar.html"

    def get_queryset(self):
        oportunidades = Contenido.objects.filter(tipo='oportunidad')
        return oportunidades


class OportunidadesRegistrar(LoginRequiredMixin, CreateView):
    model = Contenido
    form_class = OportunidadModelForm
    template_name = "oportunidades/registrar.html"
    success_url = reverse_lazy('contenido:registrar_oportunidad')

    def form_valid(self, form):
        usuario = self.request.user
        oportunidad = form.save(commit=False)
        oportunidad.crear_oportunidad(usuario)

        mis_tribus = form.instance.tribus()
        for mi_tribu in mis_tribus:
            tribu = Tribus.objects.get(nombre__icontains=mi_tribu)
            form.instance.tribus_relacionadas.add(tribu)
        form.save_m2m()
        messages.success(self.request, "Oportunidad registrada satisfactoriamente")
        return super(OportunidadesRegistrar, self).form_valid(form)


class OportunidadesModificar(LoginRequiredMixin, UpdateView):
    model = Contenido
    form_class = OportunidadModelForm
    pk_url_kwarg = 'id_oportunidad'
    template_name = "oportunidades/modificar.html"

    def form_valid(self, form):
        messages.success(self.request, "Oportunidad modificada satisfactoriamente")
        mis_tribus = form.instance.tribus()
        mis_tribus_antes = form.instance.tribus_relacionadas.all()
        for mi_tribu_antes in mis_tribus_antes:
            if not str(mi_tribu_antes.nombre).lower() in mis_tribus:
                form.instance.tribus_relacionadas.delete(mi_tribu_antes)
        for mi_tribu in mis_tribus:
            tribu = Tribus.objects.get(nombre__icontains=mi_tribu)
            if not tribu in mis_tribus_antes:
                form.instance.tribus_relacionadas.add(tribu)
        return super(OportunidadesModificar, self).form_valid(form)

    def get_success_url(self):
        if self.request.user.es_superusuario:
            return reverse_lazy('contenido:listado_oportunidades')
        else:
            return reverse_lazy('contenido:listado_mis_oportunidades_creadas')


class RegistroMasivoRutas(LoginRequiredMixin, TemplateView):
    template_name = 'rutas/excel.html'


def cargarExcelRutas(request):
    if request.method == 'POST' and request.FILES['archivo_excel']:
        archivo = request.FILES['archivo_excel']
        import pandas as pd
        excel = pd.ExcelFile(archivo)
        hoja = excel.sheet_names[0]
        df = pd.read_excel(excel, hoja, dtype=str)
        res = Contenido.cargue_masivo(df, 'ruta')
        print(res)
        if len(res) == 0:
            messages.success(request, 'Rutas cargadas exitosamente al sistema')
        else:
            messages.error(request, 'Ha ocurrido un error al cargar las filas %s!' % ', '.join(str(v) for v in res))
        return redirect('contenido:listado_rutas')


class RegistroMasivoOportunidades(LoginRequiredMixin, TemplateView):
    template_name = 'oportunidades/excel.html'


def cargarExcelOportunidades(request):
    if request.method == 'POST' and request.FILES['archivo_excel']:
        archivo = request.FILES['archivo_excel']

        import pandas as pd
        excel = pd.ExcelFile(archivo)
        hoja = excel.sheet_names[0]
        df = pd.read_excel(excel, hoja, dtype=str)
        res = Contenido.cargue_masivo(df, 'oportunidad')

        if len(res) == 0:
            messages.success(request, 'Oportunidades cargadas exitosamente al sistema')
        else:
            messages.error(request, 'Ha ocurrido un error al cargar las filas %s!' % ', '.join(str(v) for v in res))
        return redirect('contenido:listado_oportunidades')


class RutasJovenConsultar(LoginRequiredMixin, ListView):
    template_name = "rutas/joven_consultar.html"
    paginate_by = 12

    def get_queryset(self):
        consulta = self.obtener_consulta()
        return consulta['rutas']

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator

        context = super(RutasJovenConsultar, self).get_context_data(**kwargs)

        consulta = self.obtener_consulta()

        pagination = Paginator(list(consulta['rutas']), self.paginate_by)

        context['rutas'] = pagination.page(context['page_obj'].number)
        context['tag'] = consulta['tag']
        context['cantidad'] = consulta['cantidad']
        if self.request.user.tipo == 'Organización':
            context['tipos'] = [{'nombre': 'Mis Rutas'}, {'nombre': 'Favoritas'}, {'nombre': 'Todas'}]
        else:
            context['tipos'] = [{'nombre': 'Todas'}, {'nombre': 'Rutas para ti'}, {'nombre': 'Favoritas'}]

        return context

    def get_success_url(self):
        if self.request.user.es_superusuario:
            return reverse_lazy('contenido:listado_oportunidades')
        else:
            return reverse_lazy('contenido:listado_mis_oportunidades_creadas')

    def obtener_consulta(self):
        usuario = self.obtener_usuario()

        if 'tag' in self.request.GET:
            tag = self.request.GET['tag']
        elif list(self.request.GET):
            tag = list(self.request.GET)[0]
        else:
            tag = "Todas"

        rutas = Contenido.objects.filter(tipo='ruta', activo=True)
        sugeridas = Contenido.sugeridas(usuario, 'ruta')
        if tag == 'Todas':
            rutas = rutas.order_by('?')
        elif tag == 'Rutas para ti':
            rutas = sugeridas
        elif tag == 'Mis Rutas':
            rutas = Contenido.obtener_contenidos_propias(usuario, 'ruta')
        elif tag == 'Favoritas':
            rutas = Contenido.favoritas(usuario, 'ruta')

        if sugeridas is None:
            cantidad = 0
        else:
            cantidad = sugeridas.count()

        if 'q' in self.request.GET:
            query = self.request.GET['q']
            rutas = Contenido.get_queryset_modelos(query, rutas)

        return {'tag': tag, 'rutas': rutas, 'cantidad': cantidad}

    def obtener_usuario(self):
        usuario = self.request.user
        if usuario.tipo == 'Joven':
            usuario = Joven.objects.get(id=usuario.id)
        elif usuario.tipo == 'Organización':
            usuario = Organizacion.objects.get(id=usuario.id)
        return usuario


class RutaJovenDetalle(DetailView):
    model = Contenido
    template_name = "rutas/detalle.html"
    context_object_name = "ruta"


def rutasJovenFavoritas(request):
    from django.db.models import Case, When

    template_name = "rutas/joven_consultar.html"
    mis_rutas = Contenido.favoritas(request.user, 'ruta')
    # tipos = Modalidad.objects.filter(contenido='ruta', activo=True).values('tipo_id', 'tipo__nombre').distinct().order_by(Case(When(tipo__nombre='Rutas para ti', then=0), default=1))
    tipos = [
        {'nombre': 'Rutas para ti'},
        {'nombre': 'Favoritas'}
    ]
    tag = None
    if request.GET:
        if 'tag' in request.GET:
            tag = request.GET['tag']
            mis_rutas = Contenido.get_queryset_tag(tag, mis_rutas)
        elif 'q' in request.GET:
            query = request.GET['q']
            mis_rutas = Contenido.get_queryset_modelos(query, mis_rutas)
    return render(request, template_name, {'rutas': mis_rutas, 'tipos': tipos, 'opcion': 'favoritas', 'tag': tag})


def rutasJovenSugeridas(request):
    from django.db.models import Case, When
    template_name = "rutas/joven_consultar.html"
    mis_rutas = Contenido.sugeridas(request.user, 'ruta')
    # tipos = Modalidad.objects.filter(contenido='ruta', activo=True).values('tipo_id', 'tipo__nombre').distinct().order_by(Case(When(tipo__nombre='Rutas para ti', then=0), default=1))
    tipos = [
        {'nombre': 'Rutas para ti'},
        {'nombre': 'Favoritas'}
    ]
    tag = None
    if request.GET and mis_rutas:
        if 'tag' in request.GET:
            tag = request.GET['tag']
            mis_rutas = Contenido.get_queryset_tag(tag, mis_rutas)
        elif 'q' in request.GET:
            query = request.GET['q']
            mis_rutas = Contenido.get_queryset_modelos(query, mis_rutas)
    return render(request, template_name, {'rutas': mis_rutas, 'tipos': tipos, 'opcion': 'sugeridas', 'tag': tag})


class OportunidadesJovenConsultar(LoginRequiredMixin, ListView):
    template_name = "oportunidades/joven_consultar.html"
    paginate_by = 12

    def get_queryset(self):
        consulta = self.obtener_consulta()
        return consulta['oportunidades']

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator

        context = super(OportunidadesJovenConsultar, self).get_context_data(**kwargs)
        consulta = self.obtener_consulta()

        pagination = Paginator(list(consulta['oportunidades']), self.paginate_by)

        context['oportunidades'] = pagination.page(context['page_obj'].number)
        context['tag'] = consulta['tag']
        context['cantidad'] = consulta['cantidad']
        context['tipos'] = [
            {'nombre': 'Todas'},
            {'nombre': 'Oportunidades para ti'},
            {'nombre': 'Favoritas'}
        ]
        return context

    def get_success_url(self):
        if self.request.user.es_superusuario:
            return reverse_lazy('contenido:listado_oportunidades')
        else:
            return reverse_lazy('contenido:listado_mis_oportunidades_creadas')

    def obtener_consulta(self):
        usuario = self.obtener_usuario()

        if 'tag' in self.request.GET:
            tag = self.request.GET['tag']
        elif list(self.request.GET):
            tag = list(self.request.GET)[0]
        else:
            tag = "Todas"

        mis_oportunidades = Contenido.objects.filter(tipo='oportunidad', activo=True)

        if tag == 'Todas':
            mis_oportunidades = mis_oportunidades.order_by('?')
        elif tag == 'Oportunidades para ti':
            mis_oportunidades = Contenido.sugeridas(usuario, 'oportunidad')
        elif tag == 'Favoritas':
            mis_oportunidades = Contenido.favoritas(usuario, 'oportunidad')

        sugeridas = Contenido.sugeridas(usuario, 'oportunidad')
        if sugeridas is None:
            cantidad = 0
        else:
            cantidad = sugeridas.count()

        if 'q' in self.request.GET:
            query = self.request.GET['q']
            mis_oportunidades = Contenido.get_queryset_modelos(query, mis_oportunidades)

        return {'tag': tag, 'oportunidades': mis_oportunidades, 'cantidad': cantidad}

    def obtener_usuario(self):
        usuario = self.request.user
        if usuario.tipo == 'Joven':
            usuario = Joven.objects.get(id=usuario.id)
        elif usuario.tipo == 'Organización':
            usuario = Organizacion.objects.get(id=usuario.id)
        return usuario


class OportunidadJovenDetalle(DetailView):
    model = Contenido
    template_name = "oportunidades/detalle.html"
    context_object_name = "oportunidad"


def oportunidadesJovenFavoritas(request):
    template_name = "oportunidades/joven_consultar.html"
    mis_oportunidades = Contenido.favoritas(request.user, 'oportunidad')
    # tipos = Modalidad.objects.filter(contenido='oportunidad', activo=True).values('tipo_id', 'tipo__nombre').distinct()
    tipos = [
        {'nombre': 'Oportunidades para ti'},
        {'nombre': 'Favoritas'}
    ]
    tag = None

    if request.GET:
        if 'tag' in request.GET:
            tag = request.GET['tag']
            mis_oportunidades = Contenido.get_queryset_tag(tag, mis_oportunidades)
        elif 'q' in request.GET:
            query = request.GET['q']
            mis_oportunidades = Contenido.get_queryset_modelos(query, mis_oportunidades)
    return render(request, template_name,
                  {'oportunidades': mis_oportunidades, 'tipos': tipos, 'opcion': 'favoritas', 'tag': tag})


def oportunidadesJovenSugeridas(request):
    template_name = "oportunidades/joven_consultar.html"
    mis_oportunidades = Contenido.sugeridas(request.user, 'oportunidad')
    # tipos = Modalidad.objects.filter(contenido='oportunidad', activo=True).values('tipo_id', 'tipo__nombre').distinct()
    tipos = [
        {'nombre': 'Oportunidades para ti'},
        {'nombre': 'Favoritas'}
    ]
    tag = None
    if request.GET and mis_oportunidades:
        if 'tag' in request.GET:
            tag = request.GET['tag']
            mis_oportunidades = Contenido.get_queryset_tag(tag, mis_oportunidades)
        elif 'q' in request.GET:
            query = request.GET['q']
            mis_oportunidades = Contenido.get_queryset_modelos(query, mis_oportunidades)
    return render(request, template_name,
                  {'oportunidades': mis_oportunidades, 'tipos': tipos, 'opcion': 'sugeridas', 'tag': tag})


class ApiCargarCalificaciones(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        pk_usuario = self.request.user.pk
        calificaciones = CalificacionContenido.objects.filter(usuario=pk_usuario)
        cals = []
        for c in calificaciones:
            cals.append({'pk': c.contenido.pk, 'stars': c.calificacion, 'decision': c.decision})
        return Response({'calificaciones': cals})


class ApiValidacionRutas(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        valor = data['valor']
        listado_rutas = []
        if valor:
            rutas = Contenido.validacion_duplicidad_rutas(valor)
            for ruta in rutas:
                listado_rutas.append(ruta.nombre)
        return Response({'rutas': listado_rutas})


class ApiValidacionOportunidades(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        valor = data['valor']
        listado_oportunidades = []
        if valor:
            oportunidades = Contenido.validacion_duplicidad_oportunidades(valor)
            for oportunidad in oportunidades:
                listado_oportunidades.append(oportunidad.nombre)
        return Response({'oportunidades': listado_oportunidades})


class ApiCargarCalificacion(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_contenido = data["pk_contenido"]
        pk_usuario = self.request.user.pk
        c = CalificacionContenido.objects.get(usuario=pk_usuario, ruta=pk_contenido)
        cal = [{'pk': c.contenido.pk, 'stars': c.calificacion}]
        return Response({'calificaciones': cal})


class ApiCalificarContenido(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_contenido = data["pk_contenido"]
        pk_usuario = self.request.user.pk
        if "star" in data:
            calificacion, creado = CalificacionContenido.objects.get_or_create(
                contenido_id=pk_contenido, usuario_id=pk_usuario
            )
            if data['star'] == 1 or data['star'] == 2:
                calificacion.calificacion = data['star']
                calificacion.save()
            else:
                calificacion.calificacion = 0
                calificacion.save()

        return Response({})


class ApiDecisionContenido(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_contenido = data["pk_modelo"]
        pk_usuario = self.request.user.pk

        calificacion, creado = CalificacionContenido.objects.get_or_create(
            contenido_id=pk_contenido, usuario_id=pk_usuario
        )
        calificacion_json = {}
        if calificacion.decision == 1:
            calificacion.decision = 0
            calificacion.save()
        else:
            contenidos_decisiones = CalificacionContenido.decisiones_tomadas(usuario_id=pk_usuario)

            if contenidos_decisiones.count() == 3:
                return Response({"mensaje": "limite"})

            calificacion_json = {'pk': calificacion.contenido_id,
                                 'calificacion': calificacion.calificacion,
                                 'decision': calificacion.decision}

            calificacion.decision = 1
            calificacion.calificacion = 1
            calificacion.save()

        return Response(calificacion_json)


class ApiCargarVisitados(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        pk_usuario = self.request.user.pk
        visitados = ContenidoVisitado.objects.filter(usuario=pk_usuario)
        cals = []
        for c in visitados:
            cals.append({'pk': c.contenido.pk, 'visitado': c.visitado})
        return Response({'calificaciones': cals})


class ApiVisitarContenido(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_contenido = data["pk_contenido"]
        pk_usuario = self.request.user.pk
        visitado, creado = ContenidoVisitado.objects.get_or_create(
            contenido_id=pk_contenido, usuario_id=pk_usuario
        )
        visitado.visitado = 1
        visitado.save()

        return Response({})


class ApiObtenerChoices(APIView):
    def get(self, request):
        data = []
        organizaciones = Organizacion.objects.filter(is_active=True)
        info = {'elemento': 'organizacion'}
        opciones = []
        for org in organizaciones:
            opciones.append(org.get_full_name())
        info['opciones'] = opciones
        data.append(info)

        paises = Country.objects.all()
        info = {'elemento': 'paises'}
        opciones = []
        for op in paises:
            opciones.append(op.name)
        info['opciones'] = opciones
        data.append(info)

        return Response({'info': data})

class ApiObtenerRegiones(APIView):
    def get(self, request):
        data = []
        nombre_pais = request.GET.get('nombre_pais')
        pais = Country.objects.get(name=nombre_pais)
        regiones = Region.objects.filter(country_id=pais.id)
        info = {'elemento': 'regiones'}
        opciones = []
        for op in regiones:
            opciones.append(op.name)
        info['opciones'] = opciones
        data.append(info)

        return Response({'info': data})

class ApiObtenerCiudades(APIView):
    def get(self, request):
        data = []
        nombre_region = request.GET.get('nombre_region')
        region = Region.objects.get(name=nombre_region)
        ciudades = City.objects.filter(region_id=region.id)
        info = {'elemento': 'ciudades'}
        opciones = []
        for op in ciudades:
            opciones.append(op.name)
        info['opciones'] = opciones
        data.append(info)

        return Response({'info': data})

class ModalidadesListado(LoginRequiredMixin, ListView):
    model = Modalidad
    context_object_name = "modalidades"
    template_name = "modalidades/listado.html"

    def get_queryset(self):
        modalidades = Modalidad.objects.all()
        return modalidades


class ModalidadesConsultar(LoginRequiredMixin, ListView):
    model = Modalidad
    context_object_name = "modalidades"
    template_name = "modalidades/consultar.html"

    def get_queryset(self):
        modalidades = Modalidad.objects.all()
        return modalidades


class ModalidadRegistrar(LoginRequiredMixin, MensajeMixin, CreateView):
    model = Modalidad
    form_class = ModalidadModelForm
    template_name = "modalidades/registrar.html"
    success_url = reverse_lazy('contenido:registrar_modalidad')
    mensaje_exito = "Modalidad registrada satisfactoriamente"
    mensaje_error = "Ha ocurrido un error, por favor verifique los datos ingresados"


class ModalidadModificar(LoginRequiredMixin, UpdateView):
    model = Modalidad
    form_class = ModalidadModelForm
    pk_url_kwarg = 'id_modalidad'
    template_name = "modalidades/modificar.html"
    success_url = reverse_lazy('contenido:listado_modalidades')
    mensaje_exito = "Modalidad modificada satisfactoriamente"
    mensaje_error = "Ha ocurrido un error, por favor verifique los datos ingresados"


class TiposModalidadesListado(LoginRequiredMixin, ListView):
    model = TipoModalidad
    context_object_name = "tipos"
    template_name = "modalidades/tipos/listado.html"

    def get_queryset(self):
        tipos = TipoModalidad.objects.all()
        return tipos


class TiposModalidadesConsultar(LoginRequiredMixin, ListView):
    model = TipoModalidad
    context_object_name = "tipos"
    template_name = "modalidades/tipos/consultar.html"

    def get_queryset(self):
        tipos = TipoModalidad.objects.all()
        return tipos


class TipoModalidadRegistrar(LoginRequiredMixin, MensajeMixin, CreateView):
    model = TipoModalidad
    form_class = TipoModalidadModelForm
    template_name = "modalidades/tipos/registrar.html"
    success_url = reverse_lazy('contenido:registrar_tipo_modalidad')
    mensaje_exito = "Tipo registrado satisfactoriamente"
    mensaje_error = "Ha ocurrido un error, por favor verifique los datos ingresados"


class TipoModalidadModificar(LoginRequiredMixin, UpdateView):
    model = TipoModalidad
    form_class = TipoModalidadModelForm
    pk_url_kwarg = 'id_tipo'
    template_name = "modalidades/tipos/modificar.html"
    success_url = reverse_lazy('contenido:listado_tipos_modalidades')
    mensaje_exito = "Tipo modificado satisfactoriamente"
    mensaje_error = "Ha ocurrido un error, por favor verifique los datos ingresados"


class MetodologiasListado(LoginRequiredMixin, ListView):
    model = Metodologia
    context_object_name = "metodologias"
    template_name = "modalidades/metodologias/listado.html"

    def get_queryset(self):
        metodologias = Metodologia.objects.all()
        return metodologias


class MetodologiasConsultar(LoginRequiredMixin, ListView):
    model = Metodologia
    context_object_name = "metodologias"
    template_name = "modalidades/metodologias/consultar.html"

    def get_queryset(self):
        metodologias = Metodologia.objects.all()
        return metodologias


class MetodologiaRegistrar(LoginRequiredMixin, MensajeMixin, CreateView):
    model = Metodologia
    form_class = MetodologiaModelForm
    template_name = "modalidades/metodologias/registrar.html"
    success_url = reverse_lazy('contenido:registrar_metodologia')
    mensaje_exito = "Metodología registrada satisfactoriamente"
    mensaje_error = "Ha ocurrido un error, por favor verifique los datos ingresados"


class MetodologiaModificar(LoginRequiredMixin, UpdateView):
    model = Metodologia
    form_class = MetodologiaModelForm
    pk_url_kwarg = 'id_metodologia'
    template_name = "modalidades/metodologias/modificar.html"
    success_url = reverse_lazy('contenido:listado_metodologias')
    mensaje_exito = "Metodología modificada satisfactoriamente"
    mensaje_error = "Ha ocurrido un error, por favor verifique los datos ingresados"


class PalabrasExcluidasListado(LoginRequiredMixin, ListView):
    model = PalabraClaveExcluida
    context_object_name = "palabras"
    template_name = "palabras/listado.html"

    def get_queryset(self):
        palabras = PalabraClaveExcluida.obtener_todas()
        return palabras


class AsignarPalabraRutas(LoginRequiredMixin, FormView):
    model = RutaConocimiento
    context_object_name = "palabra_clave"
    pk_url_kwarg = 'id_palabra_clave'
    template_name = "palabras/asignar_a_ruta.html"
    form_class = AsignarPalabrasForm
    success_url = reverse_lazy('contenido:listado_palabras_excluidas')

    def form_valid(self, form):
        palabra = PalabraClaveExcluida.obtener(self.kwargs['id_palabra_clave'])
        ruta = form.cleaned_data['rutas_de_conocimiento']
        ruta.agregar_palabra(palabra)
        messages.success(self.request, "Palabra asignada satisfactoriamente")
        return super(AsignarPalabraRutas, self).form_valid(form)


class PalabraClaveExcluidaEliminar(LoginRequiredMixin, DeleteView):
    model = PalabraClaveExcluida
    pk_url_kwarg = 'id_palabra'
    template_name = "palabras/eliminar.html"

    def get_context_data(self, **kwargs):
        data = super(PalabraClaveExcluidaEliminar, self).get_context_data(**kwargs)
        palabra = PalabraClaveExcluida.obtener(self.kwargs['id_palabra'])
        data['palabra'] = palabra
        return data

    def delete(self, request, *args, **kwargs):
        palabra = self.get_object()
        palabra.crear_palabra_de_limpieza()
        messages.success(self.request, "Palabra eliminada correctamente")
        return redirect('contenido:listado_palabras_excluidas')


class ListadoPalabrasClave(LoginRequiredMixin, ListView):
    model = RutaConocimiento
    context_object_name = "palabras_clave"
    pk_url_kwarg = 'id_campo'
    template_name = "insignias/campos/listado_palabras_clave.html"

    def get_queryset(self):
        campo = RutaConocimiento.obtener_por_id(self.kwargs['id_campo'])
        palabras_clave = campo.obtener_palabras_clave()
        return palabras_clave

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        campo = RutaConocimiento.obtener_por_id(self.kwargs['id_campo'])
        context['campo'] = campo
        return context
