from django.apps import AppConfig


class ContenidoConfig(AppConfig):
    name = 'kairos.contenido'
