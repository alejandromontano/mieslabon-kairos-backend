# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "contenido"

urlpatterns = [
    path('rutas/gestionar/', view=RutasListado.as_view(), name='listado_rutas'),
    path('rutas/consultar/', view=RutasConsultar.as_view(), name='consultar_rutas'),
    path('rutas/registrar', view=RutasRegistrar.as_view(), name='registrar_ruta'),
    path('rutas/modificar/<int:id_ruta>', view=RutasModificar.as_view(), name='modificar_ruta'),
    path('rutas/registro-masivo/', view=RegistroMasivoRutas.as_view(), name='registro_masivo_rutas'),
    path('rutas/cargar-excel/', view=cargarExcelRutas, name='cargar_excel_rutas'),
    path('rutas/', view=RutasJovenConsultar.as_view(), name='joven_consultar_rutas'),
    path('rutas/detalle/<int:pk>', view=RutaJovenDetalle.as_view(), name='detalle_ruta'),
    path('rutas/favoritas', view=rutasJovenFavoritas, name='joven_favoritas_rutas'),
    path('rutas/favoritas/detalle/<int:pk>', view=RutaJovenDetalle.as_view(), name='joven_detalle_ruta_favorita'),
    path('rutas/sugeridas', view=rutasJovenSugeridas, name='joven_sugeridas_rutas'),
    path('rutas/sugeridas/detalle/<int:pk>', view=RutaJovenDetalle.as_view(), name='joven_detalle_ruta_sugerida'),
    path('rutas/gestionar', view=MisRutasCreadasListado.as_view(), name='listado_mis_rutas_creadas'),
    path('rutas/asignar-rutas/<int:id_organizacion>', view=AsignarRutasOrganizacion.as_view(), name='asignar_rutas_organizacion'),
    path('rutas/asignar-oportunidades/<int:id_organizacion>', view=AsignarOportunidadesOrganizacion.as_view(), name='asignar_oportunidades_organizacion'),

    path('oportunidades/gestionar/', view=OportunidadesListado.as_view(), name='listado_oportunidades'),
    path('oportunidades/consultar/', view=OportunidadesConsultar.as_view(), name='consultar_oportunidades'),
    path('oportunidades/registrar', view=OportunidadesRegistrar.as_view(), name='registrar_oportunidad'),
    path('oportunidades/modificar/<int:id_oportunidad>', view=OportunidadesModificar.as_view(), name='modificar_oportunidad'),
    path('oportunidades/registro-masivo/', view=RegistroMasivoOportunidades.as_view(), name='registro_masivo_oportunidades'),
    path('oportunidades/cargar-excel/', view=cargarExcelOportunidades, name='cargar_excel_oportunidades'),
    path('oportunidades/', view=OportunidadesJovenConsultar.as_view(), name='joven_consultar_oportunidades'),
    path('oportunidades/detalle/<int:pk>', view=OportunidadJovenDetalle.as_view(), name='detalle_oportunidad'),
    path('oportunidades/favoritas', view=oportunidadesJovenFavoritas, name='joven_favoritas_oportunidades'),
    path('oportunidades/favoritas/detalle/<int:pk>', view=OportunidadJovenDetalle.as_view(), name='joven_detalle_oportunidad_favorita'),
    path('oportunidades/sugeridas', view=oportunidadesJovenSugeridas, name='joven_sugeridas_oportunidades'),
    path('oportunidades/sugeridas/detalle/<int:pk>', view=OportunidadJovenDetalle.as_view(), name='joven_detalle_oportunidad_sugerida'),
    path('oportunidades/gestionar/', view=MisOportunidadesCreadasListado.as_view(), name='listado_mis_oportunidades_creadas'),

    path("api/cargar-calificaciones", view=ApiCargarCalificaciones.as_view(), name="api_cargar_calificaciones"),
    path("api/cargar-calificacion", view=ApiCargarCalificacion.as_view(), name="api_cargar_calificacion"),
    path("api/calificar", view=ApiCalificarContenido.as_view(), name="api_calificar"),
    path("api/decision", view=ApiDecisionContenido.as_view(), name="api_decision"),
    path("api/obtener-choices", view=ApiObtenerChoices.as_view(), name="api_obtener_choices"),
    path("api/validar-rutas", view=ApiValidacionRutas.as_view(), name="api_validar_rutas"),
    path("api/validar-oportunidades", view=ApiValidacionOportunidades.as_view(), name="api_validar_oportunidades"),
    path("api/obtener-regiones", view=ApiObtenerRegiones.as_view(), name="api_obtener_regiones"),
    path("api/obtener-ciudades", view=ApiObtenerCiudades.as_view(), name="api_obtener_ciudades"),

    path("api/cargar-visitados", view=ApiCargarVisitados.as_view(), name="api_cargar_visitados"),
    path("api/visitar-contenido", view=ApiVisitarContenido.as_view(), name="api_visitar_contenido"),

    path('modalidades/gestionar/', view=ModalidadesListado.as_view(), name='listado_modalidades'),
    path('modalidades/consultar/', view=ModalidadesConsultar.as_view(), name='consultar_modalidades'),
    path('modalidades/registrar', view=ModalidadRegistrar.as_view(), name='registrar_modalidad'),
    path('modalidades/modificar/<int:id_modalidad>', ModalidadModificar.as_view(), name='modificar_modalidad'),

    path('modalidades/metodologias/gestionar/', view=MetodologiasListado.as_view(), name='listado_metodologias'),
    path('modalidades/metodologias/consultar/', view=MetodologiasConsultar.as_view(), name='consultar_metodologias'),
    path('modalidades/metodologias/registrar', view=MetodologiaRegistrar.as_view(), name='registrar_metodologia'),
    path('modalidades/metodologias/modificar/<int:id_metodologia>', MetodologiaModificar.as_view(), name='modificar_metodologia'),

    path('modalidades/tipos/gestionar/', view=TiposModalidadesListado.as_view(), name='listado_tipos_modalidades'),
    path('modalidades/tipos/consultar/', view=TiposModalidadesConsultar.as_view(), name='consultar_tipos_modalidades'),
    path('modalidades/tipos/registrar', view=TipoModalidadRegistrar.as_view(), name='registrar_tipo_modalidad'),
    path('modalidades/tipos/modificar/<int:id_tipo>', view=TipoModalidadModificar.as_view(), name='modificar_tipo_modalidad'),

    path('palabras-excluidas-listado', view=PalabrasExcluidasListado.as_view(), name='listado_palabras_excluidas'),
    path('asignar-palabra-a-ruta-de-conocimiento/<int:id_palabra_clave>', view=AsignarPalabraRutas.as_view(), name='asignar_palabra_a_ruta_de_conocimiento'),

    path('palabras-clave/<int:id_campo>', view=ListadoPalabrasClave.as_view(), name='listado_palabras_clave'),
    path('eliminar-palabra-excluida/<int:id_palabra>', view=PalabraClaveExcluidaEliminar.as_view(), name='eliminar_palabra_excluida'),

    path('api/obtener-rutas', APIRutasListado.as_view(), name="api_rutas_listado"),

]