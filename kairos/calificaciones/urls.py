# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "calificaciones"

urlpatterns = [
    path("api/cargar-calificacion", view=ApiCargarCalificacionEstrellas.as_view(), name="api_cargar_calificacion"),
    path("api/calificar", view=ApiCalificarEstrellas.as_view(), name="api_calificar"),

    path('D48/consultar', view=CalificacionesD48.as_view(), name='consultar_D48'),
    path('Sterrett/consultar', view=CalificacionesSterret.as_view(), name='consultar_Sterrett'),
    path('Liderazgo/consultar', view=CalificacionesLiderazgo.as_view(), name='consultar_Liderazgo'),
    path('seleccion-multiple/consultar', view=CalificacionesSM.as_view(), name='consultar_SM'),
    path('CHASIDE/consultar', view=CalificacionesCHASIDE.as_view(), name='consultar_CHASIDE'),
]