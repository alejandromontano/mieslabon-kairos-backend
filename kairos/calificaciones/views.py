# Modulos Django
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

# Modulos de plugin externos
from datetime import datetime
from dateutil.relativedelta import relativedelta
from rest_framework.response import Response
from rest_framework.views import APIView
import json

# Modulos de otras apps
from kairos.formularios.models import Respuestas, Competencias, Preguntas, Ambito

# Modulos internos de la app
from .models import *

def calificarFormulario(respuestas_formulario:'RespuestasJoven', calificacion_fuera_tiempo:bool=False):
    """
    Califica el formulario utilizando las respuestas del usuario. Si el cuestionario tiene un tiempo limite
    se confirma que aun tenga tiempo para resolver el cuestionario, de lo contrario se califica automaticamente
    con las respuestas que tenga contestadas.

    Parametros:
        respuestas_formulario (RespuestasJoven): Objeto que contiene las repuestas de un joven.
        calificacion_fuera_tiempo (bool, opcional): Determina si al usuario se le acabo el tiempo. Por defecto es False.
    """ 
    id = respuestas_formulario.formulario.identificador
    tipo_formulario = respuestas_formulario.formulario.tipo_preguntas
    if id == 1:
        calificarD48(respuestas_formulario, calificacion_fuera_tiempo)
    elif id == 3:
        calificarSterret(respuestas_formulario)
    elif id == 5:
        calificarCHASIDE(respuestas_formulario)
    elif id == 4:
        calificarLiderazgo(respuestas_formulario)
    elif tipo_formulario == "SM" or id == 2:
        calificarSeleccionMultiple(respuestas_formulario)


def calificarD48(respuestas:'RespuestasJoven', calificacion_fuera_tiempo:bool):
    """Califica el cuestionarioD48 con las respuestas del usuario. Si el cuestionario tiene un tiempo limite
    se confirma que aun tenga tiempo para resolver el cuestionario, de lo contrario se califica automaticamente
    con las respuestas que tenga contestadas.

    Parametros:
        respuestas (RespuestasJoven): Objeto que contiene las repuestas de un joven.
        calificacion_fuera_tiempo (bool): Determina si al usuario se le acabo el tiempo.
    """
    
    preguntas_formulario = respuestas.formulario.pregunta_del_formulario.all()
    correctas_joven = 0
    hoy = datetime.now().date()
    edad_joven = relativedelta(hoy, respuestas.joven.fecha_de_nacimiento).years
    genero_joven = respuestas.joven.genero
    for pregunta in preguntas_formulario:
        respuestas_pregunta = Respuestas.objects.filter(pregunta=pregunta.pk).order_by('pk').values_list(
            'descripcion', flat=True)
        if calificacion_fuera_tiempo == False:
            pregunta_id = pregunta.pk
        else:
            pregunta_id = str(pregunta.pk)
        try:
            respuesta_joven = respuestas.data[pregunta_id]
            if respuesta_joven[0] == int(respuestas_pregunta[0]) and respuesta_joven[1] == int(respuestas_pregunta[1]):
                correctas_joven = correctas_joven + 1
        except:
            pass
    resultado = {}
    resultado['correctas'] = correctas_joven
    if genero_joven == 'M':
        if edad_joven <= 12:
            if correctas_joven <= 18:
                resultado['resultado'] = 'BAJO'
            elif correctas_joven > 18 and correctas_joven <= 25:
                resultado['resultado'] = 'MEDIO'
            else:
                resultado['resultado'] = 'ALTO'
        if edad_joven == 13:
            if correctas_joven <= 20:
                resultado['resultado'] = 'BAJO'
            elif correctas_joven > 20 and correctas_joven <= 28:
                resultado['resultado'] = 'MEDIO'
            else:
                resultado['resultado'] = 'ALTO'
        if edad_joven == 14:
            if correctas_joven <= 20:
                resultado['resultado'] = 'BAJO'
            elif correctas_joven > 20 and correctas_joven <= 30:
                resultado['resultado'] = 'MEDIO'
            else:
                resultado['resultado'] = 'ALTO'
        if edad_joven == 15:
            if correctas_joven <= 22:
                resultado['resultado'] = 'BAJO'
            elif correctas_joven > 22 and correctas_joven <= 30:
                resultado['resultado'] = 'MEDIO'
            else:
                resultado['resultado'] = 'ALTO'
        if edad_joven >= 16:
            if correctas_joven <= 24:
                resultado['resultado'] = 'BAJO'
            elif correctas_joven > 24 and correctas_joven <= 32:
                resultado['resultado'] = 'MEDIO'
            else:
                resultado['resultado'] = 'ALTO'
    elif genero_joven == 'F':
        if edad_joven <= 13:
            if correctas_joven <= 21:
                resultado['resultado'] = 'BAJO'
            elif correctas_joven > 21 and correctas_joven <= 28:
                resultado['resultado'] = 'MEDIO'
            else:
                resultado['resultado'] = 'ALTO'
        if edad_joven == 14 or edad_joven == 15:
            if correctas_joven <= 24:
                resultado['resultado'] = 'BAJO'
            elif correctas_joven > 24 and correctas_joven <= 31:
                resultado['resultado'] = 'MEDIO'
            else:
                resultado['resultado'] = 'ALTO'
        if edad_joven >= 16:
            if correctas_joven <= 25:
                resultado['resultado'] = 'BAJO'
            elif correctas_joven > 25 and correctas_joven <= 32:
                resultado['resultado'] = 'MEDIO'
            else:
                resultado['resultado'] = 'ALTO'

    resultado['respondidas'] = len(respuestas.data)
    resultado['porcentaje'] = round((correctas_joven / len(respuestas.data) * 100), 2)
    calificacion = CalificacionRespuestasJoven()
    calificacion.respuestas = respuestas
    calificacion.resultadoD48 = resultado
    calificacion.save()


def calificarSterret(respuestas:'RespuestasJoven'):
    """Califica el formulario Sterret utilizando las respuestas del usuario

    Parametros:
        respuestas (RespuestasJoven): Objeto que contiene las repuestas de un joven.
    """
    formulario = respuestas.formulario
    ambito = Ambito.objects.get(nombre__iexact='Inteligencia Emocional')
    competenciasSterret = ambito.competencias.all()
    puntajes = {}
    competencias = {}
    puntuacion_total = 0
    for competencia in competenciasSterret:
        puntuacion_aux = 0
        preguntas_competencia = Preguntas.objects.filter(formulario=formulario.pk,
                                                         competencia__codigo_calificacion__iexact=competencia.codigo_calificacion)
        competencias[competencia.codigo_calificacion] = {}
        for p in preguntas_competencia:
            puntuacion_aux = puntuacion_aux + respuestas.data[p.pk]
        competencias[competencia.codigo_calificacion]['puntaje'] = puntuacion_aux
        if puntuacion_aux <= 15:
            competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
        elif puntuacion_aux > 15 and puntuacion_aux <= 20:
            competencias[competencia.codigo_calificacion]['resultado'] = 'MEDIO'
        else:
            competencias[competencia.codigo_calificacion]['resultado'] = 'ALTO'
        puntuacion_total = puntuacion_total + puntuacion_aux

    puntajes['competencias'] = competencias
    puntajes['global'] = puntuacion_total
    calificacion = CalificacionRespuestasJoven()
    calificacion.respuestas = respuestas
    calificacion.puntajesSterret = puntajes
    calificacion.save()


def calificarLiderazgo(respuestas:'RespuestasJoven'):
    """Califica el formulario liderazgo utilizando las respuestas del usuario

    Parametros:
        respuestas (RespuestasJoven): Objeto que contiene las repuestas de un joven.
    """
    formulario = respuestas.formulario
    ambito = Ambito.objects.get(nombre__iexact='Liderazgo')
    competenciasLiderazgo = ambito.competencias.all()
    promedios = {}
    competencias = {}
    transaccional = 0
    transformacional = 0
    laissez = 0
    for competencia in competenciasLiderazgo:
        puntuacion_aux = 0
        preguntas_competencia = Preguntas.objects.filter(formulario=formulario.pk,
                                                         competencia__codigo_calificacion__iexact=competencia.codigo_calificacion)
        competencias[competencia.codigo_calificacion] = {}

        for p in preguntas_competencia:
            puntuacion_aux = puntuacion_aux + respuestas.data[p.pk]
        promedio_competencia = round(puntuacion_aux / len(preguntas_competencia), 2)
        competencias[competencia.codigo_calificacion]['promedio'] = promedio_competencia

        if competencia.codigo_calificacion == 'Carisma':
            transformacional = transformacional + promedio_competencia
            if promedio_competencia <= 3.75:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            elif promedio_competencia > 3.75 and promedio_competencia <= 4.25:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            else:
                competencias[competencia.codigo_calificacion]['resultado'] = 'ALTO'
        elif competencia.codigo_calificacion == 'Innovación':
            transformacional = transformacional + promedio_competencia
            if promedio_competencia <= 3.43:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            elif promedio_competencia > 3.43 and promedio_competencia <= 4.43:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            else:
                competencias[competencia.codigo_calificacion]['resultado'] = 'ALTO'
        elif competencia.codigo_calificacion == 'Inspiración':
            transformacional = transformacional + promedio_competencia
            if promedio_competencia <= 3.33:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            elif promedio_competencia > 3.33 and promedio_competencia <= 4.33:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            else:
                competencias[competencia.codigo_calificacion]['resultado'] = 'ALTO'
        elif competencia.codigo_calificacion == 'Desarrollo':
            transformacional = transformacional + promedio_competencia
            if promedio_competencia <= 3.67:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            elif promedio_competencia > 3.67 and promedio_competencia <= 4.67:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            else:
                competencias[competencia.codigo_calificacion]['resultado'] = 'ALTO'
        elif competencia.codigo_calificacion == 'Negociación':
            transaccional = transaccional + promedio_competencia
            if promedio_competencia <= 2.8:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            elif promedio_competencia > 2.8 and promedio_competencia <= 3.8:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            else:
                competencias[competencia.codigo_calificacion]['resultado'] = 'ALTO'
        elif competencia.codigo_calificacion == 'Dirección':
            transaccional = transaccional + promedio_competencia
            if promedio_competencia <= 3.0:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            elif promedio_competencia > 3.0 and promedio_competencia <= 3.83:
                competencias[competencia.codigo_calificacion]['resultado'] = 'BAJO'
            else:
                competencias[competencia.codigo_calificacion]['resultado'] = 'ALTO'
        elif competencia.codigo_calificacion == 'Liderazgo':
            laissez = laissez + promedio_competencia
            if promedio_competencia > 0:
                competencias[competencia.codigo_calificacion]['resultado'] = 'AUSENCIA DE LIDERAZGO'
    promedios['competencias'] = competencias
    dimensiones = {'transaccional': round(transaccional / 2, 2),
                   'transformacional': round(transformacional / 4, 2),
                   'laizzes_faire': round(laissez, 2)}
    promedios['dimensiones'] = dimensiones
    calificacion = CalificacionRespuestasJoven()
    calificacion.respuestas = respuestas
    calificacion.promediosLiderazgo = promedios
    calificacion.save()


def calificarSeleccionMultiple(respuestas:'RespuestasJoven'):
    """Califica el formulario de lectura utilizando las respuestas del usuario

    Parametros:
        respuestas (RespuestasJoven): Objeto que contiene las repuestas de un joven.
    """
    preguntas_formulario = respuestas.formulario.pregunta_del_formulario.all()
    correctas_joven = 0
    for pregunta in preguntas_formulario:
        try:
            respuestas_pregunta = Respuestas.objects.get(pregunta=pregunta.pk, correcta=True).pk
            respuesta_joven = respuestas.data[pregunta.pk]
            if respuesta_joven == respuestas_pregunta:
                correctas_joven = correctas_joven + 1
        except:
            pass
    resultado = {}
    resultado['correctas'] = correctas_joven
    if correctas_joven <= 5:
        resultado['resultado'] = 'BAJO'
    elif correctas_joven > 5 and correctas_joven <= 8:
        resultado['resultado'] = 'MEDIO'
    else:
        resultado['resultado'] = 'ALTO'
    resultado['respondidas'] = len(respuestas.data)
    resultado['porcentaje'] = round((correctas_joven / len(respuestas.data) * 100), 2)
    calificacion = CalificacionRespuestasJoven()
    calificacion.respuestas = respuestas
    calificacion.resultadoSM = resultado
    calificacion.save()


def calificarCHASIDE(respuestas:'RespuestasJoven'):
    """Califica el formulario de CHASIDE utilizando las respuestas del usuario

    Parametros:
        respuestas (RespuestasJoven): Objeto que contiene las repuestas de un joven.
    """
    formulario = respuestas.formulario
    ambito = Ambito.objects.get(nombre__iexact='Campos de conocimiento')
    competenciasCHASIDE = ambito.competencias.all()
    puntajes_competencias = {
        'C': {'aptitud': 0, 'interes': 0},
        'H': {'aptitud': 0, 'interes': 0},
        'A': {'aptitud': 0, 'interes': 0},
        'S': {'aptitud': 0, 'interes': 0},
        'I': {'aptitud': 0, 'interes': 0},
        'D': {'aptitud': 0, 'interes': 0},
        'E': {'aptitud': 0, 'interes': 0},
    }

    for competencia in competenciasCHASIDE:
        preguntas_competencia = Preguntas.objects.filter(formulario=formulario.pk, competencia__nombre=competencia.nombre)
        for pregunta in preguntas_competencia:
            respuesta_joven = respuestas.data[pregunta.pk]  # 1:Si 0:No
            if 'Interes' in competencia.nombre:
                if 'Admon' in competencia.nombre:
                    puntajes_competencias['C']['interes'] = puntajes_competencias['C']['interes'] + respuesta_joven
                elif 'Humanidades' in competencia.nombre:
                    puntajes_competencias['H']['interes'] = puntajes_competencias['H']['interes'] + respuesta_joven
                elif 'Artes' in competencia.nombre:
                    puntajes_competencias['A']['interes'] = puntajes_competencias['A']['interes'] + respuesta_joven
                elif 'Salud' in competencia.nombre:
                    puntajes_competencias['S']['interes'] = puntajes_competencias['S']['interes'] + respuesta_joven
                elif 'Ingenieria' in competencia.nombre:
                    puntajes_competencias['I']['interes'] = puntajes_competencias['I']['interes'] + respuesta_joven
                elif 'Seguridad' in competencia.nombre:
                    puntajes_competencias['D']['interes'] = puntajes_competencias['D']['interes'] + respuesta_joven
                elif 'Ciencia' in competencia.nombre:
                    puntajes_competencias['E']['interes'] = puntajes_competencias['E']['interes'] + respuesta_joven
            elif 'Aptitud':
                if 'Admon' in competencia.nombre:
                    puntajes_competencias['C']['aptitud'] = puntajes_competencias['C']['aptitud'] + respuesta_joven
                elif 'Humanidades' in competencia.nombre:
                    puntajes_competencias['H']['aptitud'] = puntajes_competencias['H']['aptitud'] + respuesta_joven
                elif 'Artes' in competencia.nombre:
                    puntajes_competencias['A']['aptitud'] = puntajes_competencias['A']['aptitud'] + respuesta_joven
                elif 'Salud' in competencia.nombre:
                    puntajes_competencias['S']['aptitud'] = puntajes_competencias['S']['aptitud'] + respuesta_joven
                elif 'Ingenieria' in competencia.nombre:
                    puntajes_competencias['I']['aptitud'] = puntajes_competencias['I']['aptitud'] + respuesta_joven
                elif 'Seguridad' in competencia.nombre:
                    puntajes_competencias['D']['aptitud'] = puntajes_competencias['D']['aptitud'] + respuesta_joven
                elif 'Ciencia' in competencia.nombre:
                    puntajes_competencias['E']['aptitud'] = puntajes_competencias['E']['aptitud'] + respuesta_joven

    def sortSecond(val):
        return val[1]
    orden = []
    for puntaje in puntajes_competencias:
        globalP = puntajes_competencias[puntaje]['interes'] + puntajes_competencias[puntaje]['aptitud']
        orden.append((puntaje, globalP))
    orden.sort(key=sortSecond, reverse=True)
    puntajes_competencias[orden[0][0]]['resultado'] = 'ALTO'
    puntajes_competencias[orden[1][0]]['resultado'] = 'ALTO'
    puntajes_competencias[orden[2][0]]['resultado'] = 'MEDIO'
    puntajes_competencias[orden[3][0]]['resultado'] = 'MEDIO'
    puntajes_competencias[orden[4][0]]['resultado'] = 'MEDIO'
    puntajes_competencias[orden[5][0]]['resultado'] = 'BAJO'
    puntajes_competencias[orden[6][0]]['resultado'] = 'BAJO'

    calificacion = CalificacionRespuestasJoven()
    calificacion.respuestas = respuestas
    calificacion.puntajesCHASIDE = puntajes_competencias
    calificacion.save()


class CalificacionesCHASIDE(LoginRequiredMixin, ListView):
    model = CalificacionRespuestasJoven
    context_object_name = "calificaciones"
    template_name = "formularios/respuestas/consultarCHASIDE.html"

    def get_queryset(self):
        calificaciones = CalificacionRespuestasJoven.objects.filter(respuestas__formulario__identificador=5)
        return calificaciones


class CalificacionesD48(LoginRequiredMixin, ListView):
    model = CalificacionRespuestasJoven
    context_object_name = "calificaciones"
    template_name = "formularios/respuestas/consultarD48.html"

    def get_queryset(self):
        calificaciones = CalificacionRespuestasJoven.objects.filter(respuestas__formulario__identificador=1)
        return calificaciones


class CalificacionesSterret(LoginRequiredMixin, ListView):
    model = CalificacionRespuestasJoven
    context_object_name = "calificaciones"
    template_name = "formularios/respuestas/consultarSterrett.html"

    def get_queryset(self):
        calificaciones = CalificacionRespuestasJoven.objects.filter(respuestas__formulario__identificador=3)
        return calificaciones


class CalificacionesLiderazgo(LoginRequiredMixin, ListView):
    model = CalificacionRespuestasJoven
    context_object_name = "calificaciones"
    template_name = "formularios/respuestas/consultarLiderazgo.html"

    def get_queryset(self):
        calificaciones = CalificacionRespuestasJoven.objects.filter(respuestas__formulario__identificador=4)
        return calificaciones


class CalificacionesSM(LoginRequiredMixin, ListView):
    model = CalificacionRespuestasJoven
    context_object_name = "calificaciones"
    template_name = "formularios/respuestas/consultarSM.html"

    def get_queryset(self):
        calificaciones = CalificacionRespuestasJoven.objects.filter(respuestas__formulario__identificador=2)
        return calificaciones


class ApiCargarCalificacionEstrellas(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_formulario = data["pk_formulario"]
        pk_usuario = self.request.user.pk
        try:
            c = CalificacionFormulario.objects.get(usuario=pk_usuario, formulario=pk_formulario)
            cal = [{'pk': c.formulario.pk, 'stars': c.calificacion}]
            return Response({'calificaciones': cal})
        except:
            return Response({'none': True})


class ApiCalificarEstrellas(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_formulario = data["pk_formulario"]
        pk_usuario = self.request.user.pk
        if "star" in data:
            calificacion, creado = CalificacionFormulario.objects.get_or_create(
                formulario_id=pk_formulario, usuario_id=pk_usuario
            )
            calificacion.calificacion = data['star']
            calificacion.save()
        return Response({})
