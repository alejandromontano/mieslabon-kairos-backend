# Modulos Django
from django.contrib.postgres.fields import JSONField
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

# Modulos de plugin externos
from simple_history.models import HistoricalRecords

# Modulos de otras apps
from kairos.usuarios.models import Usuario

# Modulos internos de la app

class CalificacionFormulario(models.Model):
    formulario = models.ForeignKey('formularios.Formularios', on_delete=models.CASCADE, verbose_name='formulario a calificar',
                                   related_name='formulario_calificado')
    usuario = models.ForeignKey('usuarios.Usuario', on_delete=models.CASCADE, verbose_name='usuario que califica',
                                related_name='usuario_calificador_formulario')
    calificacion = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(5), MinValueValidator(0)],
                                               verbose_name='calificación de 1 a 5')
    history = HistoricalRecords()


class CalificacionRespuestasJoven(models.Model):
    respuestas = models.ForeignKey('respuestas_formularios.RespuestasJoven', on_delete=models.CASCADE, verbose_name="respuestas_calificadas", related_name='calificacion_respuesta')
    resultadoD48 = JSONField(null=True)  # {'respondidas':...,'correctas':..., 'porcentaje': ...., 'resultado': ...}
    puntajesSterret = JSONField(null=True)  # {'global': ..., 'competencias': {competencia1: {puntaje: ..., resultado: ...}, competencia2: {puntaje: ..., resultado: ...}}}
    promediosLiderazgo = JSONField(null=True)  # {'dimensiones': {'transaccional': ..., 'transformacional': ...,, 'laizzes_faire': ....} 'competencias': {competencia1: {promedio: ..., resultado: ...}, competencia2: {promedio: ..., resultado: ...}}}
    resultadoSM = JSONField(null=True)  # {'respondidas':...,'correctas':..., 'porcentaje': ...., 'resultado': ...}
    puntajesCHASIDE = JSONField(null=True)  # {'orden': [('C',0),('S',1),...] 'competencias': {competencia1: {promedio: ..., resultado: ...}, competencia2: {promedio: ..., resultado: ...}}}
    history = HistoricalRecords()
