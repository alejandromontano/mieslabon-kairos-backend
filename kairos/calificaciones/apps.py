from django.apps import AppConfig


class CalificacionesConfig(AppConfig):
    name = 'kairos.calificaciones'
