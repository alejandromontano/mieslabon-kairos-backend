# Modulos Django
from django.contrib.auth.mixins import LoginRequiredMixin
from django.apps import apps
from django.conf import settings
from django.db.models import Q
from django.forms.models import model_to_dict
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, TemplateView

# Modulos de plugin externos
from django_datatables_view.base_datatable_view import BaseDatatableView
from easyaudit.models import RequestEvent, LoginEvent
from itertools import chain
# Modulos de otras apps

# Modulos internos de la app


class AuditorNavegacion(LoginRequiredMixin, TemplateView):
    template_name = "auditorias/auditor_navegacion.html"


class AuditorNavegacionData(LoginRequiredMixin, BaseDatatableView):
    model = RequestEvent
    template_name = "auditorias/auditor_navegacion.html"
    columns = ['id', 'url', 'method', 'user', 'user_tipo', 'remote_ip', 'datetime']
    order_columns = ['id', 'url', 'method', 'remote_ip', 'user', 'user', 'datetime']

    def filter_queryset(self, qs):
        search = self.request.GET.get('search[value]', None)
        if search:
            q = Q(url__icontains=search) | Q(method__icontains=search) | Q(user__email__icontains=search) | \
                Q(remote_ip__icontains=search) | Q(datetime__icontains=search)
            qs = qs.filter(q)
        return qs

    # def prepare_results(self, qs):
    #     data = []
    #     for item in qs:
    #         info = []
    #         for column in self.get_columns():
    #             info.append(self.render_column(item, column))
    #         data.append(info)
    #         print(data)
    #     return data
    def prepare_results(self, qs):
        data = []
        for item in qs:
            if self.is_data_list:
                data.append([self.render_column(item, column) for column in self._columns])
            else:
                row = {}
                for col_data in self.columns_data:
                    if col_data['name'] == 'user__tipo':
                        if self.render_column(item, 'user.tipo'):
                            row[col_data['data']] = '<span class="label label-aqua">' + self.render_column(item, 'user.tipo') + '</span>'
                        else:
                            row[col_data['data']] = '<span class="label label-aqua">Anónimo</span>'
                    else:
                        row[col_data['data']] = self.render_column(item, col_data['data'])
                data.append(row)
        return data


class AuditorSesiones(LoginRequiredMixin, ListView):
    model = LoginEvent
    template_name = "auditorias/auditor_sesiones.html"
    context_object_name = 'logs_sesiones'


class CambioInstancia(LoginRequiredMixin, TemplateView):
    template_name = "auditorias/consultar_cambio_instancia.html"
    context_object_name = 'cambio'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        pk_cambio = int(self.kwargs['pk_cambio'])
        pk_objeto = int(self.kwargs['pk_objeto'])
        nombre_modelo = str(self.kwargs['nombre_modelo'])
        nombre_app = str(self.kwargs['nombre_app'])
        data['nombre_modelo'] = nombre_modelo
        data['nombre_app'] = nombre_app
        data['pk_objeto'] = pk_objeto

        try:
            modelo = apps.get_model(nombre_app, nombre_modelo)
            objeto = get_object_or_404(modelo, pk=pk_objeto)
        except:
            raise Http404
        data['objeto'] = objeto
        objeto_cambio = objeto.history.get(pk=pk_cambio)
        data['objeto_cambio'] = objeto_cambio
        directorio_objeto = model_to_dict(objeto.history.get(pk=pk_cambio))
        objeto_dic_aux = {}
        for llave, valor in directorio_objeto.items():
            if 'history' in llave:
                continue
            objeto_dic_aux[llave] = str(valor)
        objeto_dic_aux["Id cambio"] = objeto_cambio.history_id
        objeto_dic_aux["Fecha de cambio"] = objeto_cambio.history_date
        # objeto_dic_aux["Motivo del cambio"] = objeto_cambio.history_change_reason
        objeto_dic_aux["Tipo de cambio"] = objeto_cambio.history_type
        objeto_dic_aux["Usuario actor"] = objeto_cambio.history_user

        data['objeto_cambio_dict'] = objeto_dic_aux
        return data


class CambiosInstancia(LoginRequiredMixin, ListView):
    template_name = "auditorias/consultar_cambios_instancia.html"
    context_object_name = 'cambios'

    def get_queryset(self):
        pk_objeto = int(self.kwargs['pk_objeto'])
        nombre_app = str(self.kwargs['nombre_app'])
        nombre_modelo = str(self.kwargs['nombre_modelo'])

        try:
            modelo = apps.get_model(nombre_app, nombre_modelo)
            objeto = get_object_or_404(modelo, pk=pk_objeto)
            if hasattr(objeto, 'history'):
                return objeto.history.all()
            else:
                raise Http404
        except LookupError:
            raise Http404

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        pk_objeto = int(self.kwargs['pk_objeto'])
        nombre_modelo = str(self.kwargs['nombre_modelo'])
        nombre_app = str(self.kwargs['nombre_app'])
        data['nombre_modelo'] = nombre_modelo
        data['nombre_app'] = nombre_app

        try:
            modelo = apps.get_model(nombre_app, nombre_modelo)
            objeto = get_object_or_404(modelo, pk=pk_objeto)
        except LookupError:
            raise Http404

        data['objeto'] = objeto
        return data


class CambiosModelo(LoginRequiredMixin, ListView):
    template_name = "auditorias/consultar_cambios_modelo.html"
    context_object_name = 'cambios'

    def get_queryset(self):
        nombre_modelo = str(self.kwargs['nombre_modelo'])
        nombre_app = str(self.kwargs['nombre_app'])
        try:
            modelo = apps.get_model(nombre_app, nombre_modelo)
        except LookupError:
            raise Http404
        return modelo.history.all()

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        nombre_modelo = str(self.kwargs['nombre_modelo'])
        nombre_app = str(self.kwargs['nombre_app'])
        data['nombre_modelo'] = nombre_modelo
        data['nombre_app'] = nombre_app
        return data


class CambiosAplicacion(LoginRequiredMixin, ListView):
    template_name = "auditorias/consultar_cambios_aplicacion.html"
    context_object_name = 'cambios'

    def get_queryset(self):        
        nombre_app = str(self.kwargs['nombre_app'])
        modelos = apps.all_models[nombre_app]
        modelos = modelos.copy()  # evitar que mute durante la iteracion
        cambios_querys = []
        for nombre_modelo, modelo in modelos.items():
            if hasattr(modelo, 'history'):
                cambios_querys.append(modelo.history.all())
        return chain(*cambios_querys)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        nombre_app = str(self.kwargs['nombre_app'])
        data['nombre_app'] = nombre_app
        return data


class Aplicaciones(LoginRequiredMixin, ListView):
    template_name = "auditorias/consultar_cambios_aplicaciones.html"
    context_object_name = 'aplicaciones'

    def get_queryset(self):
        aplicaciones = []
        for app_name in sorted(settings.INSTALLED_APPS):
            if 'parir' in app_name:
                app_name = app_name.split('.')[1]
                modelos = apps.all_models[app_name]
                modelos = modelos.copy()  # evitar que mute durante la iteracion
                aplicacion_aux = {'nombre_aplicacion': app_name, 'nombres_modelos': []}
                for nombre_modelo, modelo in modelos.items():
                    if hasattr(modelo, 'history'):
                        aplicacion_aux['nombres_modelos'].append(nombre_modelo)

                # Si la aplicacion tiene modelos que son auditados
                if len(aplicacion_aux['nombres_modelos']) > 0:
                    aplicaciones.append(aplicacion_aux)
        return aplicaciones
