# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = 'auditorias'
urlpatterns = [
    path("instancias/consultar-cambios-<str:nombre_app>/<str:nombre_modelo>/<int:pk_objeto>/<int:pk_cambio>", view=CambioInstancia.as_view(), name='consultar_cambio_instancia'),
    path("instancias/consultar-cambios-<str:nombre_app>/<str:nombre_modelo>/<int:pk_objeto>", view=CambiosInstancia.as_view(), name='consultar_cambios_instancia'),
    path("instancias/consultar-cambios-<str:nombre_app>/<str:nombre_modelo>", view=CambiosModelo.as_view(), name='consultar_cambios_modelo'),
    path("instancias/consultar-cambios-<str:nombre_app>", view=CambiosAplicacion.as_view(), name='consultar_cambios_aplicacion'),
    path("instancias/consultar-cambios", view=Aplicaciones.as_view(), name='consultar_cambios'),
    path("navegacion/consultar", view=AuditorNavegacion.as_view(), name='auditor_navegacion'),
    path("navegacion/consultar-data", view=AuditorNavegacionData.as_view(), name='auditor_navegacion_data'),
    path("sesiones/consultar", view=AuditorSesiones.as_view(), name='auditor_sesiones'),
]
