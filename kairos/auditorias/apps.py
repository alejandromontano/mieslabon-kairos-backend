from django.apps import AppConfig


class AuditoriasConfig(AppConfig):
    name = 'parir.auditorias'
