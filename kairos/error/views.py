from django.template import RequestContext
from django.shortcuts import render
from django.http import HttpResponseRedirect

def error_404(request, exception):
        data = {}
        return render(request,'errores/404.html', data)

def error_500(request):
        data = {}
        return render(request,'errores/500.html', data)

def csrf_failure(request, reason=""):
    context = {'message': 'You are already logged in'}
    return render(request, 'usuarios/mi_perfil.html', context)