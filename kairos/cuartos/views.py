# Modulos Django
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.base import TemplateView

from kairos.retos.models import Retos, CuartoReto, Pregunta
from kairos.cuartos.models import CuartoPregunta, Opcion

# Modulos internos de la app
from .forms import *
from .models import *

# TIPO DE CUARTOS

class TipoCuartoListado(LoginRequiredMixin, ListView):
    model = Tipo
    context_object_name = "tipos"
    template_name = "cuartos/tipos/listado.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(TipoCuartoListado, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        tipos = Tipo.objects.all()
        return tipos


class TipoCuartoRegistrar(LoginRequiredMixin, CreateView):
    model = Cuarto
    form_class = TipoCuartoModelForm
    template_name = "cuartos/tipos/registrar.html"
    success_url = reverse_lazy('cuartos:tipo_listado')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(TipoCuartoRegistrar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        messages.success(self.request, "Tipo registrado satisfactoriamente")
        return super(TipoCuartoRegistrar, self).form_valid(form)


class TipoCuartoModificar(LoginRequiredMixin, UpdateView):
    model = Tipo
    form_class = TipoCuartoModelForm
    pk_url_kwarg = 'id_tipo'
    template_name = "cuartos/tipos/registrar.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(TipoCuartoModificar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        return super(TipoCuartoModificar, self).form_valid(form)

    def get_success_url(self):
        messages.success(self.request, "Tipo modificado satisfactoriamente")
        return reverse_lazy('cuartos:tipo_listado')


## CUARTOS
class CuartoListado(LoginRequiredMixin, ListView):
    model = Cuarto
    context_object_name = "cuartos"
    template_name = "cuartos/listado.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(CuartoListado, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        reto = Retos.buscar(self.kwargs['id_reto'])
        cuartos = reto.cuartos.all()
        return cuartos

    def get_context_data(self, **kwargs):
        data = super(CuartoListado, self).get_context_data(**kwargs)
        reto = Retos.buscar(self.kwargs['id_reto'])
        data['reto'] = reto
        return data


class CuartoRegistrar(LoginRequiredMixin, CreateView):
    model = Cuarto
    form_class = CuartoModelForm
    template_name = "cuartos/registrar.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(CuartoRegistrar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):        
        reto = Retos.buscar(self.kwargs['id_reto'])
        orden = self.request.POST['orden']        
        cuarto = form.save(commit=False)
        existe = CuartoReto.existe_cuarto(cuarto, reto)
        if existe == True:
            messages.success(self.request, "Este cuarto para este reto ya existe!")
            return redirect("cuartos:listado", id_reto=reto.id)
        cuarto.save()
        CuartoReto.crear_cuarto_reto(cuarto, reto, orden)
        messages.success(self.request, "Cuarto registrado satisfactoriamente")
        return super(CuartoRegistrar, self).form_valid(form)

    def get_context_data(self, **kwargs):
        data = super(CuartoRegistrar, self).get_context_data(**kwargs)
        reto = Retos.buscar(self.kwargs['id_reto'])
        data['reto'] = reto
        return data
    
    def get_form_kwargs(self):
        kwargs = super(CuartoRegistrar, self).get_form_kwargs()      
        reto = Retos.buscar(self.kwargs['id_reto'])
        orden_maximo = reto.obtener_lista_ordenes_cuartos()
        kwargs['reto'] = reto
        kwargs['orden'] = 1
        if orden_maximo:
            kwargs['orden'] = orden_maximo[0]+1

        return kwargs

    def get_success_url(self):
        messages.success(self.request, "Cuarto registrado satisfactoriamente")
        reto = Retos.buscar(self.kwargs['id_reto'])
        return reverse_lazy('cuartos:listado', kwargs={'id_reto': reto.id})

class CuartoModificar(LoginRequiredMixin, UpdateView):
    model = Cuarto
    form_class = CuartoModelForm
    pk_url_kwarg = 'id_cuarto'
    template_name = "cuartos/modificar.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(CuartoModificar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        
        reto = Retos.buscar(self.kwargs['id_reto'])
        orden = self.request.POST['orden']        
        cuarto = form.save(commit=False)        
        existe = CuartoReto.existe_cuarto(cuarto, reto)
        if existe == True:
            cuarto_reto = CuartoReto.buscar(cuarto, reto)
            cuarto_reto.orden = orden
            cuarto_reto.save()
        cuarto.save()        
        messages.success(self.request, "Cuarto registrado satisfactoriamente")
        return redirect("cuartos:listado", id_reto=reto.id)

    
    def get_success_url(self):
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        messages.success(self.request, "Cuarto modificado satisfactoriamente")
        return reverse_lazy('cuartos:listado', kwargs={'id_reto': reto.id})

    def get_context_data(self, **kwargs):
        data = super(CuartoModificar, self).get_context_data(**kwargs)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        data['cuarto'] = cuarto
        data['reto'] = reto
        return data
    
    def get_form_kwargs(self):
        kwargs = super(CuartoModificar, self).get_form_kwargs()
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        cuarto_reto = CuartoReto.objects.get(cuarto=cuarto, reto=reto)
        kwargs['orden'] = cuarto_reto.orden
        kwargs['reto'] = reto
        return kwargs

# #ACTOS

class ActosListado(LoginRequiredMixin, ListView):
    model = Acto
    context_object_name = "actos"
    template_name = "cuartos/actos/listado.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(ActosListado, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        actos = cuarto.actos.all()
        return actos

    def get_context_data(self, **kwargs):
        data = super(ActosListado, self).get_context_data(**kwargs)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        data['cuarto'] = cuarto
        data['reto'] = reto
        return data

class ActoRegistrar(LoginRequiredMixin, CreateView):
    model = Acto
    form_class = ActoModelForm
    template_name = "cuartos/actos/registrar.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(ActoRegistrar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        reto = Retos.buscar(self.kwargs['id_reto'])
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        orden = self.request.POST['orden']
        acto = form.save(commit=False)
        existe = CuartoActo.existe_cuarto_acto(cuarto, acto)
        if existe == True:
            messages.success(self.request, "Este acto para este cuarto ya existe!")
            return redirect("cuartos:acto_listado", reto.id, cuarto.id)
        acto.save()
        CuartoActo.crear_cuarto_acto(cuarto, acto, orden)
        messages.success(self.request, "Acto registrado satisfactoriamente")
        return redirect("cuartos:acto_listado", reto.id, cuarto.id)

    def get_context_data(self, **kwargs):
        data = super(ActoRegistrar, self).get_context_data(**kwargs)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        data['cuarto'] = cuarto
        data['reto'] = reto
        return data

    def get_form_kwargs(self):
        kwargs = super(ActoRegistrar, self).get_form_kwargs()      
        reto = Retos.buscar(self.kwargs['id_reto'])
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        orden_maximo = cuarto.obtener_lista_ordenes_actos()                
        kwargs['orden'] = 1
        kwargs['cuarto'] = cuarto
        if orden_maximo:
            kwargs['orden'] = orden_maximo[0]+1

        return kwargs

class ActoModificar(LoginRequiredMixin, UpdateView):
    model = Acto
    form_class = ActoModelForm
    pk_url_kwarg = 'id_acto'
    template_name = "cuartos/actos/modificar.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(ActoModificar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        from kairos.cuartos.models import CuartoActo
        acto = form.save(commit=False)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        cuarto_acto = CuartoActo.objects.get(cuarto=cuarto, acto=acto)
        cuarto_acto.orden = int(self.request.POST['orden'])
        cuarto_acto.save()
        return super(ActoModificar, self).form_valid(form)

    def get_success_url(self):
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        messages.success(self.request, "Acto modificado satisfactoriamente")
        return reverse_lazy('cuartos:acto_listado', kwargs={'id_reto': reto.id, 'id_cuarto': cuarto.id})

    def get_context_data(self, **kwargs):
        data = super(ActoModificar, self).get_context_data(**kwargs)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        data['cuarto'] = cuarto
        data['reto'] = reto
        return data

    def get_form_kwargs(self):
        kwargs = super(ActoModificar, self).get_form_kwargs()
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        acto = Acto.buscar(self.kwargs['id_acto'])
        cuarto_acto = CuartoActo.objects.get(cuarto=cuarto, acto=acto)
        kwargs['orden'] = cuarto_acto.orden
        kwargs['cuarto'] = cuarto
        return kwargs
    
    

#Preguntas
class PreguntasListado(LoginRequiredMixin, ListView):
    model = Pregunta
    context_object_name = "preguntas"
    template_name = "cuartos/preguntas/listado.html"
    
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(PreguntasListado, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        preguntas = cuarto.preguntas.all()
        return preguntas

    def get_context_data(self, **kwargs):
        data = super(PreguntasListado, self).get_context_data(**kwargs)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        data['cuarto'] = cuarto
        data['reto'] = reto
        return data

class PreguntaRegistrar(LoginRequiredMixin ,CreateView):
    model = Pregunta
    form_class = PreguntaModelForm
    template_name = "cuartos/preguntas/registrar.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(PreguntaRegistrar, self).dispatch(request, *args, **kwargs)
   
    def form_valid(self, form):
        reto = Retos.buscar(self.kwargs['id_reto'])
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        orden = self.request.POST['orden']
        pregunta = form.save(commit=False)
        existe = CuartoPregunta.existe_cuarto_pregunta(cuarto, pregunta)
        if existe == True:
            messages.success(self.request, "Esta pregunta para este cuarto ya existe!")
            return redirect("cuartos:preguntas_listado",reto.id, cuarto.id)
        pregunta.save()
        CuartoPregunta.crear_cuarto_pregunta(cuarto, pregunta, orden)
        messages.success(self.request, "Pregunta registrada satisfactoriamente")
        return redirect("cuartos:preguntas_listado",reto.id, cuarto.id)

    def get_context_data(self, **kwargs):
        data = super(PreguntaRegistrar, self).get_context_data(**kwargs)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        data['cuarto'] = cuarto
        data['reto'] = reto
        return data

    def get_form_kwargs(self):
        kwargs = super(PreguntaRegistrar, self).get_form_kwargs()      
        reto = Retos.buscar(self.kwargs['id_reto'])
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        orden_maximo = cuarto.obtener_lista_ordenes_actos()                
        kwargs['orden'] = 1
        kwargs['cuarto'] = cuarto
        if orden_maximo:
            kwargs['orden'] = orden_maximo[0]+1

        return kwargs

class PreguntaModificar(LoginRequiredMixin, UpdateView):
    model = Pregunta
    form_class = PreguntaModelForm
    pk_url_kwarg = 'id_pregunta'
    template_name = "cuartos/preguntas/modificar.html"
    
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(PreguntaModificar, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        pregunta = form.save(commit=False)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        cuarto_acto = CuartoPregunta.objects.get(cuarto=cuarto, pregunta=pregunta)
        cuarto_acto.orden = int(self.request.POST['orden'])
        cuarto_acto.save()
        return super(PreguntaModificar, self).form_valid(form)

    def get_success_url(self):
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        messages.success(self.request, "Pregunta modificada satisfactoriamente")
        return reverse_lazy('cuartos:preguntas_listado', kwargs={'id_reto':reto.id, 'id_cuarto':cuarto.id})

    def get_context_data(self, **kwargs):
        data = super(PreguntaModificar, self).get_context_data(**kwargs)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        data['cuarto'] = cuarto
        data['reto'] = reto
        return data

    def get_form_kwargs(self):
        kwargs = super(PreguntaModificar, self).get_form_kwargs()
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        pregunta = Pregunta.buscar(self.kwargs['id_pregunta'])
        cuarto_pregunta = CuartoPregunta.objects.get(cuarto=cuarto, pregunta=pregunta)
        kwargs['orden'] = cuarto_pregunta.orden
        kwargs['cuarto'] = cuarto
        return kwargs

#Opciones
class OpcionListado(LoginRequiredMixin, ListView):
    model = Opcion
    context_object_name = "opciones"
    template_name = "cuartos/opciones/listado.html"
    
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(OpcionListado, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        pregunta = Pregunta.buscar(self.kwargs['id_pregunta'])
        opciones = pregunta.opciones_respuestas.all()
        return opciones

    def get_context_data(self, **kwargs):
        data = super(OpcionListado, self).get_context_data(**kwargs)
        pregunta = Pregunta.buscar(self.kwargs['id_pregunta'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        data['pregunta'] = pregunta
        data['reto'] = reto
        data['cuarto'] = cuarto
        
        return data

class OpcionRegistrar(LoginRequiredMixin ,CreateView):
    model = Opcion
    form_class = OpcionModelForm
    template_name = "cuartos/opciones/registrar.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(OpcionRegistrar, self).dispatch(request, *args, **kwargs)
   
    def form_valid(self, form):
        pregunta = Pregunta.buscar(self.kwargs['id_pregunta'])
        id_reto = self.kwargs['id_reto']
        id_cuarto = self.kwargs['id_cuarto']
        id_pregunta = self.kwargs['id_pregunta']
        opcion = form.save(commit=False)
        opcion.pregunta = pregunta
        opcion.save()
        messages.success(self.request, "Pregunta registrada satisfactoriamente")
        return redirect("cuartos:opcion_registrar",id_reto, id_cuarto, id_pregunta)

    def get_context_data(self, **kwargs):
        data = super(OpcionRegistrar, self).get_context_data(**kwargs)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        pregunta = Pregunta.buscar(self.kwargs['id_pregunta'])
        data['cuarto'] = cuarto
        data['reto'] = reto
        data['pregunta'] = pregunta
        return data

class OpcionModificar(LoginRequiredMixin, UpdateView):
    model = Opcion
    form_class = OpcionModelForm
    pk_url_kwarg = 'id_opcion'
    template_name = "cuartos/opciones/modificar.html"
    
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(OpcionModificar, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        pregunta = Pregunta.buscar(self.kwargs['id_pregunta'])
        messages.success(self.request, "Pregunta modificada satisfactoriamente")
        return reverse_lazy('cuartos:opciones_listado', kwargs={'id_reto':reto.id, 'id_cuarto':cuarto.id, 'id_pregunta':pregunta.id})

    def get_context_data(self, **kwargs):
        data = super(OpcionModificar, self).get_context_data(**kwargs)
        cuarto = Cuarto.buscar(self.kwargs['id_cuarto'])
        reto = Retos.buscar(self.kwargs['id_reto'])
        pregunta = Pregunta.buscar(self.kwargs['id_pregunta'])
        data['cuarto'] = cuarto
        data['reto'] = reto
        data['pregunta'] = pregunta
        data['id_opcion'] = self.kwargs['id_opcion']
        return data

   
