# Modulos Django
from django import forms

# Modulos de plugin externos
from django_select2.forms import Select2Widget
from tempus_dominus.widgets import DateTimePicker

# Modulos de otras apps

# Modulos internos de la app
from .models import *


class TipoCuartoModelForm(forms.ModelForm):

    class Meta:
        model = Tipo
        fields = ["nombre", "activo"]


class CuartoModelForm(forms.ModelForm):    
    orden = forms.IntegerField(label="Posición que tendra en el reto", required=True)

    def __init__(self, *args, **kwargs):
        orden = None
        self.reto = None
                
        if 'orden' in kwargs:
            orden = kwargs.pop('orden')        
       
        if 'reto' in kwargs:           
            self.reto = kwargs.pop('reto') 
            

        super(CuartoModelForm, self).__init__(*args, **kwargs)
        if orden:
            self.fields['orden'].initial = orden
        else:
            self.fields['orden'].initial = 0


    class Meta:
        model = Cuarto
        fields = ["titulo", "tipo", "indicacion", "ojo_a_esto", "peso",
                    "link_video", "dificultad", "orden", "activo"]

        widgets = {
            'tipo': Select2Widget(),
            'indicacion': forms.Textarea(
                attrs={
                    'rows': 2,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'ojo_a_esto': forms.Textarea(
                attrs={
                    'rows': 2,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
        }
    
    def clean_orden(self):
        from django.core.exceptions import ValidationError

        data = self.cleaned_data['orden']
        cuartos = self.reto.obtener_lista_ordenes_cuartos()        
        if self.instance.cuarto_reto_cuarto.all():
            if data != self.instance.cuarto_reto_cuarto.all().first().orden:
                if data in cuartos:            
                    raise ValidationError("La posición ya esta tomada por un otro Cuarto")
        else:
            if data in cuartos:            
                raise ValidationError("La posición ya esta tomada por un otro Cuarto")

        return data



class ActoModelForm(forms.ModelForm):

    orden = forms.IntegerField(label="Posición que tendra en el cuarto", required=True)

    class Meta:
        model = Acto
        fields = ["indicaciones", "calificacion_maxima", "orden", "archivo_apoyo", "activo"]

        widgets = {
            'indicaciones': forms.Textarea(
                attrs={
                    'rows': 2,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            )
        }

    def __init__(self, *args, **kwargs):
        orden = None
        self.cuarto = None

        if 'orden' in kwargs:
            orden = kwargs.pop('orden')
        
        if 'cuarto' in kwargs:           
            self.cuarto = kwargs.pop('cuarto')

        super(ActoModelForm, self).__init__(*args, **kwargs)
        if orden:
            self.fields['orden'].initial = orden
        else:
            self.fields['orden'].initial = 0

    def clean_orden(self):
        from django.core.exceptions import ValidationError

        data = self.cleaned_data['orden']
        
        if self.cuarto:
            actos = self.cuarto.obtener_lista_ordenes_actos()            
            if self.instance.cuarto_acto_acto.all():                
                if data != self.instance.cuarto_acto_acto.all().first().orden:
                    if data in actos:
                        raise ValidationError("La posición ya esta tomada por un otro Acto")
            else:
                if data in actos:            
                    raise ValidationError("La posición ya esta tomada por un otro Acto")

        return data


class PreguntaModelForm(forms.ModelForm):

    orden = forms.IntegerField(label="Posición que tendra en el cuarto", required=True)

    class Meta:
        model = Pregunta
        fields = ["enunciado", "tiempo_limite", "dificultad", "orden", "activo"]
        widgets = {
            'enunciado': forms.Textarea(
                attrs={
                    'rows': 2,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            )
        }
    
    def __init__(self, *args, **kwargs):
        orden = None
        self.cuarto = None
        
        if 'orden' in kwargs:
            orden = kwargs.pop('orden')
        
        if 'cuarto' in kwargs:           
            self.cuarto = kwargs.pop('cuarto')
       
        super(PreguntaModelForm, self).__init__(*args, **kwargs)
        if orden:
            self.fields['orden'].initial = orden
        else:
            self.fields['orden'].initial = 0

    def clean_orden(self):
        from django.core.exceptions import ValidationError

        data = self.cleaned_data['orden']
        
        if self.cuarto:
            preguntas = self.cuarto.obtener_lista_ordenes_actos()            
            if self.instance.cuarto_pregunta_pregunta.all():
                print("----------------", self.instance.cuarto_pregunta_pregunta.all().first().orden)
                if data != self.instance.cuarto_pregunta_pregunta.all().first().orden:
                    if data in preguntas:
                        raise ValidationError("La posición ya esta tomada por un otro Pregunta")
            else:
                if data in preguntas:            
                    raise ValidationError("La posición ya esta tomada por un otra Pregunta")

        return data

class OpcionModelForm(forms.ModelForm):

    class Meta:
        model = Opcion
        fields = ["enunciado", "es_correcta", "activo"]
        widgets = {
            'enunciado': forms.Textarea(
                attrs={
                    'rows': 2,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            )
        }
    
    def __init__(self, *args, **kwargs):        
        super(OpcionModelForm, self).__init__(*args, **kwargs)
       
