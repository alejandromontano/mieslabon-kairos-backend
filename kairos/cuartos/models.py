# Modulos Django
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.postgres.fields import JSONField

# Modulos de plugin externos
from simple_history.models import HistoricalRecords

# Modulos de otras apps

# Modulos internos de la app

def subir_arcivho_apoyo(instance, filename):
    return "archivos_actos/%s" % (filename.encode('ascii', 'ignore'))

class Tipo(models.Model):
    nombre = models.CharField(max_length=255, verbose_name="Nombre tipo", blank=False, null=False)
    activo = models.BooleanField(verbose_name="¿El tipo esta activo?", default=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

class Cuarto(models.Model):
    tipo = models.ForeignKey('Tipo', related_name="tipos_del_cuarto", verbose_name="Tipo de cuarto*", on_delete=models.CASCADE, blank=False, null=True)
    preguntas = models.ManyToManyField('Pregunta', through='CuartoPregunta', related_name="cuartos_preguntas", blank=True, verbose_name="Preguntas del cuarto")
    actos = models.ManyToManyField('Acto', through='CuartoActo', related_name="cuartos_actos", blank=True, verbose_name="Actos del cuarto")
    titulo = models.CharField(max_length=50, verbose_name="Titulo del cuarto*", blank=False, null=False)
    indicacion = models.CharField(max_length=255, verbose_name="Indicaciones del cuarto*", blank=False, null=False)
    ojo_a_esto = models.CharField(max_length=255, verbose_name="Ojo a esto*", blank=False, null=False)
    peso = models.DecimalField(max_digits=5, decimal_places=2, validators=[MaxValueValidator(100.00), MinValueValidator(1.00)], verbose_name="Peso (1-100)*")
    link_video = models.URLField(max_length=128, verbose_name='Video de instrucción', null=True, blank=True)
    dificultad = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)],
                                               verbose_name='Dificultad (1-10)*', null=False, blank=False)
    activo = models.BooleanField(verbose_name="¿El cuarto esta activo?", default=True)
    es_descanso = models.BooleanField(verbose_name="¿El es de descanso?", default=False)
    history = HistoricalRecords()

    def __str__(self):
        return self.titulo

    @staticmethod
    def obtener_cuartos_responder(reto:'Reto', detalle):
        """
        Retorna los cuartos que debe responder el participante en el Reto.

        Parametros:
            reto (Reto): Reto donde se esta participando.
            detalle ([DetalleJovenReto or DetalleEquipoReto]): Objeto que contiene la informacion del participante en el Reto.

        Retorno:
            QuerySet[Cuarto]: [description]
        """

        if 'Equipos' in reto.grupos_permitidos:
            return Cuarto.objects.filter(cuartos_retos=reto, detalle_equipo_reto_cuartos=detalle, es_descanso=False).order_by('cuarto_reto_cuarto__orden')                   
        else:
            return Cuarto.objects.filter(cuartos_retos=reto, detalle_joven_reto_cuartos=detalle, es_descanso=False).order_by('cuarto_reto_cuarto__orden')
                                              
    @staticmethod
    def obtener_cuartos_responder_valores(reto:'Reto', detalle):
        """
        Retorna Queryset de diccionarios con el id y el estado del los cuartos que un participante debe responder.

        Parametros:
            reto (Reto): Reto donde se esta participando.
            detalle ([DetalleJovenReto or DetalleEquipoReto]): Objeto que contiene la informacion del participante en el Reto.

        Retorno:
            QuerySet[dict()]: [{pk:id_cuarto, 'estado':bool}]
        """
        if 'Equipos' in reto.grupos_permitidos:
            return Cuarto.obtener_cuartos_responder(reto, detalle).values('pk', 'equipo_reto_cuarto_cuarto__completado')
        else:
            return Cuarto.obtener_cuartos_responder(reto, detalle).values('pk', 'joven_reto_cuarto_cuarto__completado')

    @staticmethod
    def obtener_siguiente_cuarto_responder(reto:'Reto', detalle) -> 'Cuarto':
        """
        Retorna el siguiente cuarto que debe responder el participante

        Parametros:
            reto (Reto): Reto donde se esta participando.
            detalle ([DetalleJovenReto or DetalleEquipoReto]): Objeto que contiene la informacion del participante en el Reto.

        Retorno:
            Cuarto: Siguiente Cuarto a responder
        """
        if 'Equipos' in reto.grupos_permitidos:
            return Cuarto.objects.filter(cuartos_retos=reto, detalle_equipo_reto_cuartos=detalle, equipo_reto_cuarto_cuarto__completado=False, es_descanso=False).order_by('cuarto_reto_cuarto__orden').first()
        else:
            return Cuarto.objects.filter(cuartos_retos=reto, detalle_joven_reto_cuartos=detalle, joven_reto_cuarto_cuarto__completado=False, es_descanso=False).order_by('cuarto_reto_cuarto__orden').first()

    @staticmethod
    def obtener_cuarto_descanso(reto:'Reto', detalle) -> 'Cuarto':
        """
        Retorna el cuarto descanso del participante.

        Parametros:
            reto (Reto): Reto donde se esta participando.
            detalle ([DetalleJovenReto or DetalleEquipoReto]): Objeto que contiene la informacion del participante en el Reto.

        Retorno:
            Cuarto: Cuarto descanso
        """
        if 'Equipos' in reto.grupos_permitidos:
            return Cuarto.objects.filter(cuartos_retos=reto, detalle_equipo_reto_cuartos=detalle, es_descanso=True).order_by('cuarto_reto_cuarto__orden').first()
        else:
            return Cuarto.objects.filter(cuartos_retos=reto, detalle_joven_reto_cuartos=detalle, es_descanso=True).order_by('cuarto_reto_cuarto__orden').first()

    @staticmethod
    def obtener_cuarto_actual_participante(detalle, cuarto_completado:bool, reto:'Reto', descanso:bool) -> 'Cuarto':
        """
        Retorna el cuarto actual en el que se encuentra el participante

        Parametros:
            detalle ([DetalleJovenReto or DetalleEquipoReto]): Objeto que contiene la informacion del participante en el Reto.
            cuarto_completado (bool): Determina si el cuarto ha sido completado.
            reto (Reto): Reto donde se esta participando.
            descanso (bool): Determina si el cuarto es de tipo descanso.

        Retorno:
            Cuarto: Cuarto actual
        """        

        if 'Equipos' in reto.grupos_permitidos:            
            return Cuarto.objects.filter(detalle_equipo_reto_cuartos=detalle, equipo_reto_cuarto_cuarto__completado=cuarto_completado, cuartos_retos=reto, es_descanso=descanso).order_by('cuarto_reto_cuarto__orden').first()
        else:
            return Cuarto.objects.filter(detalle_joven_reto_cuartos=detalle, joven_reto_cuarto_cuarto__completado=cuarto_completado, cuartos_retos=reto, es_descanso=descanso).order_by('cuarto_reto_cuarto__orden').first()

    @staticmethod
    def obtener_cuartos_sin_completar_participante(detalle, cuarto_completado:bool, reto:'Reto', descanso:bool) -> 'QuerySet[Cuarto]':
        """
        Retorna los cuartos que el participante no ha completado

        Parametros:
            detalle ([DetalleJovenReto or DetalleEquipoReto]): Objeto que contiene la informacion del participante en el Reto.
            cuarto_completado (bool): Determina si el cuarto ha sido completado.
            reto (Reto): Reto donde se esta participando.
            descanso (bool): Determina si el cuarto es de tipo descanso.

        Retorno:
            QuerySet[Cuarto]: Lista Cuartos
        """        

        if 'Equipos' in reto.grupos_permitidos:            
            return Cuarto.objects.filter(detalle_equipo_reto_cuartos=detalle, equipo_reto_cuarto_cuarto__completado=cuarto_completado, cuartos_retos=reto, es_descanso=descanso).order_by('cuarto_reto_cuarto__orden')
        else:
            return Cuarto.objects.filter(detalle_joven_reto_cuartos=detalle, joven_reto_cuarto_cuarto__completado=cuarto_completado, cuartos_retos=reto, es_descanso=descanso).order_by('cuarto_reto_cuarto__orden')


    @staticmethod
    def buscar(id_cuarto:int) -> 'Cuarto':
        """
        Retorna un Cuarto si existe, usando el id como parametro.
        
        Parametros:
            id_cuarto(int): id del cuarto a buscar

        Retorno:
            Cuarto: Si existe
            None: Si no existe
        """

        try:
            cuarto = Cuarto.objects.get(id=id_cuarto)
            return cuarto
        except Cuarto.DoesNotExist:
            return None

    def obtener_orden_maximo(self:'Cuarto') -> int:
        """
        Retorna el orden maximo que posee un cuarto.

        Parametros:
            self (Cuarto): Elemento propio en el que se realiza la busqueda

        Retorno:
            int: Orden maximo que tiene el cuarto en sus preguntas o actos
        """

        if self.tipo.nombre == 'Preguntas':
            return self.preguntas.order_by('-cuarto_pregunta_pregunta__orden').values('cuarto_pregunta_pregunta__orden').first()['cuarto_pregunta_pregunta__orden']
        else:
            return self.actos.order_by('-cuarto_acto_acto__orden').values('cuarto_acto_acto__orden').first()['cuarto_acto_acto__orden']

    def obtener_lista_ordenes_actos(self:'Cuarto') -> 'List[int]':
        """
        Retorna una lista con los ordenes que tienen los actos del cuarto

        Parametros:
            self (Cuarto): Cuarto donde se realizara la busqueda

        Returns:
            List[int]: Lista de ordenes
        """
        if self.tipo.nombre == 'Preguntas':
            return self.preguntas.order_by('-cuarto_pregunta_pregunta__orden').values_list('cuarto_pregunta_pregunta__orden', flat=True)
        else:            
            return self.actos.order_by('-cuarto_acto_acto__orden').values_list('cuarto_acto_acto__orden', flat=True)

    
class CuartoPregunta(models.Model):
    cuarto = models.ForeignKey('Cuarto', related_name='cuarto_pregunta_cuarto', on_delete=models.PROTECT)
    pregunta = models.ForeignKey('Pregunta', related_name='cuarto_pregunta_pregunta', on_delete=models.PROTECT)
    orden = models.PositiveIntegerField(verbose_name="Orden que tendrá la Pregunta en el Cuarto", validators=[MinValueValidator(1)], null=False, blank=False)
    history = HistoricalRecords()

    @staticmethod
    def existe_cuarto_pregunta(cuarto:'Cuarto', pregunta:'Pregunta') -> bool:
        """
        Confirma si existe la relacion entre un cuarto y una pregunta

        Parametros:
            cuarto (Cuarto): Cuarto donde se buscara
            pregunta (Pregunta): Pregunta que se buscara

        Retorno:
            bool: True si existe, False si no existe
        """

        try:
            cuarto_pregunta = CuartoPregunta.objects.get(cuarto=cuarto, pregunta=pregunta)
        except CuartoPregunta.DoesNotExist:
            return False
        return True

    @staticmethod
    def crear_cuarto_pregunta(cuarto:'Cuarto', pregunta:'Pregunta', orden:int):
        """
        Crea la relación de cuarto con pregunta usando un orden como diferenciador 

        Parametros:
            cuarto (Cuarto): Cuarto al que se quiere agregar la relacion
            pregunta (Pregunta): Pregunta que sera relacionada
            orden (int): 1...n 
        """
        CuartoPregunta.objects.create(cuarto=cuarto, pregunta=pregunta, orden=orden)

class CuartoActo(models.Model):
    cuarto = models.ForeignKey('Cuarto', related_name='cuarto_acto_cuarto', on_delete=models.PROTECT)
    acto = models.ForeignKey('Acto', related_name='cuarto_acto_acto', on_delete=models.PROTECT)
    orden = models.PositiveIntegerField(verbose_name="Orden que tendrá el Acto en el Cuarto", validators=[MinValueValidator(1)], null=False, blank=False)
    history = HistoricalRecords()

    @staticmethod
    def existe_cuarto_acto(cuarto:'Cuarto', acto:'Acto') -> bool:
        """
        Confirma si existe la relacion entre un cuarto y un acto

        Parametros:
            cuarto (Cuarto): Cuarto donde se buscara
            acto (Acto): Acto que se buscara

        Retorno:
            bool: True si existe, False si no existe
        """

        try:
            cuarto_acto = CuartoActo.objects.get(cuarto=cuarto, acto=acto)
        except CuartoActo.DoesNotExist:
            return False
        return True

    @staticmethod
    def crear_cuarto_acto(cuarto:'Cuarto', acto:'Acto', orden:int):
        """
        Crea la relación de cuarto con pregunta usando un orden como diferenciador 

        Parametros:
            cuarto (Cuarto): Cuarto al que se quiere agregar la relacion
            acto (Acto): Acto que sera relacionado
            orden (int): 1...n 
        """

        CuartoActo.objects.create(cuarto=cuarto, acto=acto, orden=orden)
    
    

class Acto(models.Model):
    indicaciones = models.CharField(max_length=255, verbose_name="Indicaciones del acto*", blank=False, null=False)
    calificacion_maxima = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)],
                                                      verbose_name='Dificultad (1-10)*', null=False, blank=False)
    archivo_apoyo = models.FileField(upload_to=subir_arcivho_apoyo, verbose_name="Archivo de apoyo", null=True, blank=True)
    activo = models.BooleanField(verbose_name="¿El acto esta activo?", default=True)
    history = HistoricalRecords()

    @staticmethod
    def obtener_actos_de_cuartos(lista_cuartos:'List[Cuarto]') -> 'QuerySet[Acto]':
        """
        retorna los actos que esten relacionados con los cuartos recibiudos en una lista como parametro.

        Args:
            lista_cuartos (List[Cuarto]): lista de cuartos

        Returns:
            QuerySet[Acto]: Actos
        """
        
        return Acto.objects.filter(cuarto_acto_acto__cuarto__in=lista_cuartos)

    @staticmethod
    def buscar(id_acto:int) -> 'Acto':
        """
        Retorna un Acto si existe, usando el id como parametro.
        
        Parametros:
            id_acto(int): id del acto a buscar

        Retorno:
            Acto: Si existe
            None: Si no existe
        """
        try:
            acto = Acto.objects.get(id=id_acto)
            return acto
        except Acto.DoesNotExist:
            return None

class TipoPregunta(models.Model):
    nombre = models.CharField(max_length=255, verbose_name="Nombre tipo", blank=False, null=False)
    activo = models.BooleanField(verbose_name="¿El tipo esta activo?", default=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

class Pregunta(models.Model):
    tipo = models.ForeignKey('TipoPregunta', related_name="tipos_de_pregunta", verbose_name="Tipo de pregunta*", on_delete=models.CASCADE, blank=False, null=True)
    enunciado = models.CharField(max_length=255, verbose_name="Enunciado de pregunta*", blank=False, null=False)
    #tipo = models.CharField(max_length=255, verbose_name="Tipo pregunta", blank=False, null=False)
    tiempo_limite = models.PositiveIntegerField(default=30, validators=[MaxValueValidator(999), MinValueValidator(30)],
                                               verbose_name='Tiempo limite (Segundos)*', null=False, blank=False)
    dificultad = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(0)],
                                               verbose_name='Dificultad (1-10)*', null=False, blank=False)
    activo = models.BooleanField(verbose_name="¿La pregunta esta activa?", default=True)

    history = HistoricalRecords()

    def buscar(id_pregunta:int) -> 'Pregunta':
        """
        Retorna una Pregunta si existe, usando el id como parametro.
        
        Parametros:
            id_pregunta(int): id de la pregunta a buscar

        Retorno:
            Pregunta: Si existe
            None: Si no existe
        """
        try:
            pregunta = Pregunta.objects.get(id=id_pregunta)
            return pregunta
        except Pregunta.DoesNotExist:
            return None

class Opcion(models.Model):
    pregunta = models.ForeignKey('Pregunta', related_name='opciones_respuestas', on_delete=models.CASCADE)
    enunciado = models.CharField(max_length=255, verbose_name="Enunciado de la respuesta", blank=False, null=False)
    es_correcta = models.BooleanField(verbose_name="¿Es la respuesta correcta?", default=False)
    activo = models.BooleanField(verbose_name="¿La opción esta activa?", default=True)
    history = HistoricalRecords()

class Respuesta(models.Model):
    pregunta = models.ForeignKey('Pregunta', related_name='respuestas_participantes', on_delete=models.CASCADE)
    data = JSONField(null=True)
    history = HistoricalRecords()



