# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "cuartos"


urlpatterns = [
    path('listado/<int:id_reto>', CuartoListado.as_view(), name='listado'),
    path('registrar/<int:id_reto>', CuartoRegistrar.as_view(), name='registrar'),
    path('modificar/<int:id_reto>/<int:id_cuarto>', CuartoModificar.as_view(), name='modificar'),

    path('listado-tipos', TipoCuartoListado.as_view(), name='tipo_listado'),
    path('registrar-tipo', TipoCuartoRegistrar.as_view(), name='tipo_registrar'),
    path('modificar-tipo/<int:id_tipo>', TipoCuartoModificar.as_view(), name='tipo_modificar'),
    
    path('listado-actos/<int:id_reto>/<int:id_cuarto>', ActosListado.as_view(), name='acto_listado'),
    path('registrar-acto/<int:id_reto>/<int:id_cuarto>', ActoRegistrar.as_view(), name='acto_registrar'),
    path('modificar-acto/<int:id_reto>/<int:id_acto>/<int:id_cuarto>', ActoModificar.as_view(), name='acto_modificar'),

    path('listado-preguntas/<int:id_reto>/<int:id_cuarto>', PreguntasListado.as_view(), name='preguntas_listado'),
    path('registrar-preguntas/<int:id_reto>/<int:id_cuarto>', PreguntaRegistrar.as_view(), name='pregunta_registrar'),
    path('modificar-preguntas/<int:id_reto>/<int:id_cuarto>/<int:id_pregunta>', PreguntaModificar.as_view(), name='preguntas_modificar'),

    path('listado-opciones/<int:id_reto>/<int:id_cuarto>/<int:id_pregunta>', OpcionListado.as_view(), name='opciones_listado'),
    path('registrar-opciones/<int:id_reto>/<int:id_cuarto>/<int:id_pregunta>', OpcionRegistrar.as_view(), name='opcion_registrar'),
    path('modificar-opciones/<int:id_reto>/<int:id_cuarto>/<int:id_pregunta>/<int:id_opcion>', OpcionModificar.as_view(), name='opcion_modificar'),

]