# Modulos Django
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DetailView, TemplateView

# Modulos de plugin externos
from rest_framework.response import Response
from rest_framework.views import APIView
import json

# Modulos de otras apps
from kairos.core.utils import relacion_joven_con_equipo, relacion_organizacion_con_equipo
from kairos.equipos.forms import *
from kairos.equipos.models import Equipo
from kairos.guia.models import VerRecurso, Recurso
from kairos.organizaciones.models import Organizacion
from kairos.usuarios.models import Joven, Usuario

# Modulos internos de la app

class GestionDeEquiposLider(LoginRequiredMixin, ListView):
    model = Equipo
    context_object_name = "equipos"
    template_name = "equipos/listado_del_lider.html"

    def get_queryset(self):
        lider = self.request.user
        equipos = Equipo.objects.filter(lider=lider)
        return equipos

class EquipoRegistrar(LoginRequiredMixin, CreateView):
    model = Equipo
    form_class = EquipoModelForm
    template_name = "equipos/registrar.html"

    def form_valid(self, form):
        joven = Joven.objects.get(id=self.request.user.id)
        equipo = form.save(commit=False)
        equipo.lider = joven
        equipo.save()
        equipo.miembros.add(joven)
        form.save_m2m()
        if 'modificar_equipo' in self.request.session:
            del self.request.session['modificar_equipo']
        self.request.session['registro_equipo'] = equipo.id
        messages.success(self.request, "Equipo registrado satisfactoriamente")
        return redirect('equipos:registrar_proyecto', id_equipo=equipo.id)

@login_required
def RegistroDeProyectos(request, id_equipo):
    '''
        Se encarga de registrar un poryecto a un equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo al que se le asigna el proyecto
    '''
    equipo = Equipo.buscar(id_equipo)
    usuario = Joven.objects.get(id=request.user.id)

    if equipo == None:
        return redirect("inicio")

    if usuario != equipo.lider:
        return redirect("inicio")

    proyectos_del_equipo = equipo.proyectos_del_equipo.all()

    estado_sesion = True
    if 'registro_equipo' in request.session:
        estado_sesion = True
    if 'modificar_equipo' in request.session:
        estado_sesion = False

    if 'registrar' in request.POST:
        form_proyecto = ProyectoModelForm(request.POST)
        if form_proyecto.is_valid():
            proyecto = form_proyecto.save(commit=False)
            proyecto.equipo = equipo
            proyecto.save()
        messages.success(request, "Proyectos registrados satisfactoriamente")
        return redirect("equipos:registrar_proyecto", id_equipo=equipo.id)
    else:
        form_proyecto = ProyectoModelForm()

    return render(request, 'equipos/proyectos/registrar.html', {
        'form_proyecto': form_proyecto,
        'proyectos_del_equipo': proyectos_del_equipo,
        'equipo': equipo,
        'estado_sesion': estado_sesion
    })

@login_required
def RegistroDeInvitaciones(request, id_equipo):    
    '''
        Se encarga de enviar las invitacion para unirse a un equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que envia las invitaciones         
    '''
    from kairos.gestor_correos.envio_correos import enviar_correo, INVITAR_JOVENES

    equipo = Equipo.buscar(id_equipo)

    usuario = Joven.objects.get(id=request.user.id)

    if equipo == None:
        return redirect("inicio")

    if usuario != equipo.lider:
        return redirect("inicio")

    usuario = request.user
    invitaciones_form = InvitacionesModelForm(instance=equipo, usuario=usuario)
    invitaciones_por_fuera_form = FormularioInvitacionJovenAEquipo()
    jovenes = equipo.solicitudes.all()

    estado_sesion = True
    if 'registro_equipo' in request.session:
        estado_sesion = True
    if 'modificar_equipo' in request.session:
        estado_sesion = False

    if request.method == 'POST':
        invitaciones_por_fuera_form = FormularioInvitacionJovenAEquipo(request.POST)
        if 'enviar' in request.POST:
            invitaciones_form = InvitacionesModelForm(request.POST, instance=equipo, usuario=usuario)
            if invitaciones_form.is_valid():
                invitacion = invitaciones_form.save(commit=False)
                invitacion.save()
                for id_joven in request.POST.getlist('solicitudes'):
                    joven = Joven.objects.get(id=id_joven)
                    if not joven in jovenes:
                        invitacionJoven = SolicitudesEquipos.objects.create(equipo=equipo, joven=joven, estado='Invitado')
                messages.success(request, "Invitación enviadas correctamente")
                return redirect("equipos:enviar_invitaciones", id_equipo=equipo.id)

        if 'invitar_por_correo' in request.POST:
            correos = request.POST.getlist('invitar_por_correo')[0].split(',')
            for correo in correos:
                enviar_correo(correo, equipo, INVITAR_JOVENES, request.user)                
                messages.success(request, "Invitación enviadas correctamente")

    if 'finalizar' in request.POST:
        return redirect("equipos:gestionar_equipos")

    return render(request, 'equipos/solicitudes/registrar.html', {
        'invitaciones_form': invitaciones_form,
        'invitaciones_por_fuera_form': invitaciones_por_fuera_form,
        'jovenes': jovenes,
        'equipo': equipo,
        'estado_sesion': estado_sesion
    })

@login_required
def CancelarInvitacion(request, id_joven, id_equipo):
    '''
        Se encarga de eliminar las invitaciones de uniser al quipo enviadas por un Equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que envio las invitaciones         
    '''
    equipo = Equipo.buscar(id_equipo)
    joven = Joven.objects.get(id=id_joven)

    if equipo == None:
        return redirect("inicio")

    equipo.cancelar_invitacion(joven)
    messages.success(request, "Invitación cancelada correctamente")
    return redirect("equipos:enviar_invitaciones", id_equipo=equipo.id)

@login_required
def Postularme(request, id_joven, id_equipo):
    '''
        Se encarga de postular a un joven a un Equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que envio las invitaciones
            id_joven: id del Joven que recibe la solicitud
    '''
    equipo = Equipo.buscar(id_equipo)
    joven = Joven.objects.get(id=id_joven)

    if equipo == None:
        return redirect("inicio")

    equipo.enviar_solicitud(joven)
    messages.success(request, "Postulación enviada correctamente")
    return redirect("equipos:consultar_todos_los_equipos")

class EquipoConsultar(LoginRequiredMixin, ListView):
    template_name = "equipos/consultar_todos_los_equipos.html"
    paginate_by = 12

    def get_queryset(self):
        consulta = self.obtener_consulta()
        return consulta['equipos']

    def dispatch(self, request, *args, **kwargs):
        request.session['equipo_organizacion'] = 2
        return super(EquipoConsultar, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator
        from kairos.guia.models import Recurso
        context = super(EquipoConsultar, self).get_context_data(**kwargs)
        usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()

        pagination = Paginator(list(consulta['equipos']), self.paginate_by, allow_empty_first_page=True)

        context['equipos'] = pagination.page(context['page_obj'].number)
        context['tipos'] = [
                        {'nombre': 'Todos'},
                        {'nombre': 'Sugeridos para ti'},
                        {'nombre': 'Mis equipos'},
                        {'nombre': 'Favoritos'}
        ]
        
        context['joven'] = usuario_actual
        context['tag'] = consulta['tag']        

        return context

    def obtener_consulta(self):
        from kairos.core.utils import obtener_lista_usuario_caracteristicas
        usuario = self.obtener_usuario()

        tag = "Todos"
        if 'tag' in self.request.GET:
            tag = self.request.GET['tag']
        elif list(self.request.GET):
            tag = list(self.request.GET)[0]

        equipos = []

        if tag == 'Mis equipos':
            equipos = obtener_lista_usuario_caracteristicas(usuario, Equipo.obtener_mis_equipos(usuario))
        elif tag == 'Favoritos':
            equipos = obtener_lista_usuario_caracteristicas(usuario, Equipo.obtener_equipos_favoritos(usuario))
        elif tag == 'Sugeridos para ti':
            equipos = obtener_lista_usuario_caracteristicas(usuario, Equipo.obtener_sugeridos(usuario))
        elif tag == 'Todos':
            equipos = obtener_lista_usuario_caracteristicas(usuario, Equipo.objects.all())

        if 'q' in self.request.GET:
            equipos = Equipo.objects.all()
            query = self.request.GET['q']
            equipos = obtener_lista_usuario_caracteristicas(usuario, Equipo.buscador(query, equipos))
        return {'tag': tag, 'equipos': equipos}

    def obtener_usuario(self):
        usuario = Usuario.objects.get(id=self.request.user.id)
        if self.request.user.tipo == 'Joven':
            usuario = Joven.objects.get(id=self.request.user.id)
        elif self.request.user.tipo == 'Organización':
            usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario


class ModificarProyecto(LoginRequiredMixin, UpdateView):
    model = Proyecto
    pk_url_kwarg = 'id_proyecto'
    form_class = ProyectoModelForm
    template_name = "equipos/proyectos/modificar.html"

    def form_valid(self, form):
        form.save()
        id_proyecto = self.kwargs['id_proyecto']
        proyecto = Proyecto.buscar(id_proyecto)
        equipo = proyecto.equipo
        messages.success(self.request, "Equipo modificado satisfactoriamente")
        return redirect("equipos:registrar_proyecto", id_equipo=equipo.id)

class ModificarEquipo(LoginRequiredMixin, UpdateView):
    model = Equipo
    pk_url_kwarg = 'id_equipo'
    form_class = EquipoModelForm
    template_name = "equipos/modificar.html"

    def form_valid(self, form):
        id_equipo = self.kwargs['id_equipo']
        form.save()

        if 'registrar_equipo' in self.request.session:
            del self.request.session['registrar_equipo']
        self.request.session['modificar_equipo'] = id_equipo

        messages.success(self.request, "Equipo modificado satisfactoriamente")
        return redirect("equipos:registrar_proyecto", id_equipo=id_equipo)

    def get_context_data(self, **kwargs):
        context = super(ModificarEquipo, self).get_context_data(**kwargs)
        id_equipo = self.kwargs['id_equipo']
        equipo = Equipo.buscar(id_equipo)
        context["equipo"] = equipo
        return context

class EquipoDetalle(LoginRequiredMixin, DetailView):
    model = Equipo
    template_name = "equipos/detalle/perfil_equipo.html"
    context_object_name = "equipo"
    pk_url_kwarg = 'id_equipo'

    def dispatch(self, request, *args, **kwargs):
        request.session['equipo_organizacion'] = 1
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        usuario = self.obtener_usuario()
        if equipo.miembro_del_equipo(usuario) == False:
            return redirect('equipos:quienes_somos', id_equipo=equipo.id)
        return super(EquipoDetalle, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EquipoDetalle, self).get_context_data(**kwargs)
        joven = self.obtener_usuario()
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        context = relacion_joven_con_equipo(joven, equipo, estado=1)
        return context

    def obtener_usuario(self):
        usuario = Usuario.objects.get(id=self.request.user.id)
        if self.request.user.tipo == 'Joven':
            usuario = Joven.objects.get(id=self.request.user.id)
        elif self.request.user.tipo == 'Organizacion':
            usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario

class QuienesSomosEquipoDetalle(LoginRequiredMixin, DetailView):
    model = Equipo
    template_name = "equipos/detalle/quienes_somos.html"
    context_object_name = "equipo"
    pk_url_kwarg = 'id_equipo'

    def get_context_data(self, **kwargs):
        context = super(QuienesSomosEquipoDetalle, self).get_context_data(**kwargs)
        usuario = self.obtener_usuario()
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        if usuario.__class__.__name__ == 'Joven':
            context = relacion_joven_con_equipo(usuario, equipo, estado=1)
        elif usuario.__class__.__name__ == 'Organizacion':
            context = relacion_organizacion_con_equipo(usuario, equipo, estado=1)
            context['equipo'] = equipo
        return context

    def obtener_usuario(self):
        usuario = Usuario.objects.get(id=self.request.user.id)
        if self.request.user.tipo == 'Joven':
            usuario = Joven.objects.get(id=self.request.user.id)
        elif self.request.user.tipo == 'Organización':
            usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario

class MiembrosEquipoDetalle(LoginRequiredMixin, DetailView):
    model = Equipo
    template_name = "equipos/detalle/miembros.html"
    context_object_name = "equipo"
    pk_url_kwarg = 'id_equipo'

    def get_context_data(self, **kwargs):
        context = super(MiembrosEquipoDetalle, self).get_context_data(**kwargs)
        usuario = self.obtener_usuario()
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        if usuario.__class__.__name__ == 'Joven':
            context = relacion_joven_con_equipo(usuario, equipo, estado=1)
        elif usuario.__class__.__name__ == 'Organizacion':
            context = relacion_organizacion_con_equipo(usuario, equipo, estado=1)
            context['equipo'] = equipo
        return context

    def obtener_usuario(self):
        usuario = Usuario.objects.get(id=self.request.user.id)
        if self.request.user.tipo == 'Joven':
            usuario = Joven.objects.get(id=self.request.user.id)
        elif self.request.user.tipo == 'Organización':
            usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario

class EstadoEquipoDetalle(LoginRequiredMixin, DetailView):
    model = Equipo
    template_name = "equipos/detalle/estado.html"
    context_object_name = "equipo"
    pk_url_kwarg = 'id_equipo'

    def get_context_data(self, **kwargs):
        context = super(EstadoEquipoDetalle, self).get_context_data(**kwargs)
        usuario = self.obtener_usuario()
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        if usuario.__class__.__name__ == 'Joven':
            context = relacion_joven_con_equipo(usuario, equipo, estado=1)
        elif usuario.__class__.__name__ == 'Organizacion':
            context = relacion_organizacion_con_equipo(usuario, equipo, estado=1)
            context['equipo'] = equipo
        return context

    def obtener_usuario(self):
        usuario = Usuario.objects.get(id=self.request.user.id)
        if self.request.user.tipo == 'Joven':
            usuario = Joven.objects.get(id=self.request.user.id)
        elif self.request.user.tipo == 'Organización':
            usuario = Organizacion.objects.get(id=self.request.user.id)
        return usuario

class ApiCargarCalificaciones(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        pk_usuario = self.request.user.pk
        calificaciones = CalificacionEquipo.objects.filter(usuario=pk_usuario)
        cals = []
        for c in calificaciones:
            cals.append({'pk': c.equipo.pk, 'stars': c.calificacion})
        return Response({'calificaciones': cals})


class ApiCargarCalificacion(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_contenido = data["pk_contenido"]
        pk_usuario = self.request.user.pk
        c = CalificacionEquipo.objects.get(usuario=pk_usuario, equipo=pk_contenido)
        cal = [{'pk': c.equipo.pk, 'stars': c.calificacion}]
        return Response({'calificaciones': cal})


class ApiCalificarContenido(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_contenido = data["pk_contenido"]
        pk_usuario = self.request.user.pk
        if "star" in data:
            calificacion, creado = CalificacionEquipo.objects.get_or_create(
                equipo_id=pk_contenido, usuario_id=pk_usuario
            )
            if data['star'] == 0 or data['star'] == 1:
                calificacion.calificacion = data['star']
                calificacion.save()
            else:
                calificacion.delete()
        return Response({})

@login_required
def AceptarInvitacion(request, id_joven, id_equipo):
    '''
        Se encarga de Aceptar la invitacion de un Equipo a un Joven

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que envio las invitaciones
            id_joven: id del Joven que recibio la solicitud
    '''
    equipo = Equipo.buscar(id_equipo)
    joven = Joven.objects.get(id=id_joven)
    usuario = Joven.objects.get(id=request.user.id)

    if equipo == None:
        return redirect("inicio")

    if usuario != joven:
        return redirect("inicio")

    equipo.aceptar_solicitud(joven)
    messages.success(request, "Invitación aceptada correctamente")
    return redirect("equipos:consultar_todos_los_equipos")

@login_required
def AceptarPostulacion(request, id_joven, id_equipo):
    '''
        Se encarga de Aceptar la invitacion de un Joven a un Equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que recibio la solicitud
            id_joven: id del Joven que envio la solicitud
    '''
    equipo = Equipo.buscar(id_equipo)
    joven = Joven.objects.get(id=id_joven)
    usuario = Joven.objects.get(id=request.user.id)

    if equipo == None:
        return redirect("inicio")

    if usuario != equipo.lider:
        return redirect("inicio")

    equipo.aceptar_solicitud(joven)
    messages.success(request, "Solicitud aceptada correctamente")
    return redirect("equipos:miembros", id_equipo=equipo.id)

@login_required
def EliminarDelEquipo(request, id_joven, id_equipo):
    '''
        Se encarga de Expulsar a un Joven del Equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que expulsara
            id_joven: id del Joven que sera expulsado
    '''
    equipo = Equipo.buscar(id_equipo)
    joven = Joven.objects.get(id=id_joven)
    usuario = Joven.objects.get(id=request.user.id)

    if equipo == None:
        return redirect("inicio")

    if usuario != equipo.lider:
        return redirect("inicio")

    equipo.expulsar_miembro(joven)
    messages.success(request, "Miembro eliminado correctamente")
    return redirect("equipos:miembros", id_equipo=equipo.id)

@login_required
def RechazarPostulacion(request, id_joven, id_equipo):
    '''
        Se encarga de Rechazar la invitacion de un Joven a un Equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que recibio la solicitud
            id_joven: id del Joven que envio la solicitud
    '''
    equipo = Equipo.buscar(id_equipo)
    joven = Joven.objects.get(id=id_joven)
    usuario = Joven.objects.get(id=request.user.id)

    if equipo == None:
        return redirect("inicio")

    if usuario != equipo.lider:
        return redirect("inicio")

    equipo.rechazar_postulacion(joven)
    messages.success(request, "Solicitud rechazada correctamente")
    return redirect("equipos:miembros", id_equipo=equipo.id)

@login_required
def RetirarPostulacion(request, id_joven, id_equipo):
    '''
        Se encarga de Retirar la mpostulacion que envio un Joven a un Equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que recibio la solicitud
            id_joven: id del Joven que envio la solicitud
    '''
    equipo = Equipo.buscar(id_equipo)
    joven = Joven.objects.get(id=id_joven)
    usuario = Joven.objects.get(id=request.user.id)

    if equipo == None:
        return redirect("inicio")

    if usuario != joven:
        return redirect("inicio")

    equipo.retirar_postulacion(joven)
    messages.success(request, "Postulación retirada correctamente")
    return redirect("equipos:consultar_todos_los_equipos")

@login_required
def RechazarInvitacion(request, id_joven, id_equipo):
    '''
        Se encarga de Rechazar la invitacion de un Equipo a un Joven

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que envio la solicitud
            id_joven: id del Joven que recibio la solicitud
    '''

    equipo = Equipo.buscar(id_equipo)
    joven = Joven.objects.get(id=id_joven)
    usuario = Joven.objects.get(id=request.user.id)

    if equipo == None:
        return redirect("inicio")

    if usuario != joven:
        return redirect("inicio")

    equipo.rechazar_invitacion(joven)
    messages.success(request, "Invitación rechazada correctamente")
    return redirect("equipos:detalle_equipo", id_equipo=equipo.id)

@login_required
def SalirDelEquipo(request, id_joven, id_equipo):
    '''
        Permite a un Joven salir del Equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo
            id_joven: id del Joven que quiere salir
    '''
    equipo = Equipo.buscar(id_equipo)
    joven = Joven.objects.get(id=id_joven)
    usuario = Joven.objects.get(id=request.user.id)

    if equipo == None:
        return redirect("inicio")

    if usuario != joven:
        return redirect("inicio")

    equipo.salir_del_equipo(joven)
    messages.success(request, "Saliste del equipo correctamente")
    return redirect("equipos:consultar_todos_los_equipos")

@login_required
def CambiarLider(request, id_joven, id_equipo):
    '''
        Permite a un cambiar al lider de un equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo
            id_joven: id del Joven que sera nuevo lider
    '''
    equipo = Equipo.buscar(id_equipo)
    joven = Joven.objects.get(id=id_joven)
    usuario = Joven.objects.get(id=request.user.id)

    if equipo == None:
        return redirect("inicio")

    if usuario != equipo.lider:
        return redirect("inicio")

    equipo.cambio_de_lider(joven)
    messages.success(request, "Cambio de líder efectuado correctamente")
    return redirect("equipos:miembros", id_equipo=equipo.id)

class TableroEquipoCompetenciasCognitivas(LoginRequiredMixin, TemplateView):
    template_name = 'equipos/tableros/tablero_competencias_cognitivas.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        competencias_cognitivas = equipo.obtener_competencias_cognitivas()
        context['competencias_cognitivas'] = competencias_cognitivas
        return context

class TableroEquipoCompetenciasHabilidadesSociales(LoginRequiredMixin, TemplateView):
    template_name = 'equipos/tableros/tablero_competencias_habilidades_sociales.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        competencias_habilidades_sociales = equipo.obtener_desempenio_por_habilidades_sociales_equipo()
        competencias_habilidades_sterret = equipo.obtener_desempenio_por_sterret()
        competencia_sterret_global = equipo.obtener_puntaje_global_sterret()
        estilos_de_liderazgo = equipo.obtener_estilos_de_liderazgo()
        context['competencias_habilidades_sociales'] = competencias_habilidades_sociales
        context['competencia_sterret'] = competencia_sterret_global
        context['estilos_de_liderazgo'] = estilos_de_liderazgo
        context['competencias_habilidades_sterret'] = competencias_habilidades_sterret
        return context

class TableroEquipoCompetenciasAptitudesEIntereses(LoginRequiredMixin, TemplateView):
    template_name = 'equipos/tableros/tablero_competencias_aptitudes_e_intereses.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        competencias_aptitudes_e_intereses = equipo.obtener_desempenio_por_aptitudes_e_intereses()
        context['competencias_aptitudes_e_intereses'] = competencias_aptitudes_e_intereses
        return context

class TableroEquipoCompetenciasExperiencias(LoginRequiredMixin, TemplateView):
    template_name = 'equipos/tableros/tablero_competencias_experiencias.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        rendimiento_por_asignatura = equipo.obtener_rendimiento_por_asignatura()
        numero_de_experiencias_del_equipo = equipo.obtener_numero_de_experiencias_del_equipo()
        context['rendimiento_por_asignatura'] = rendimiento_por_asignatura
        context['numero_de_experiencias_del_equipo'] = numero_de_experiencias_del_equipo
        return context


class GestionDeSolicitudesEquipos(LoginRequiredMixin, TemplateView):
    template_name = "equipos/solicitudes/solicitudes_equipo.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated and request.user.is_superuser:
            messages.error(request, "Usted no está autorizado para realizar la acción!")
            return redirect("inicio")
        return super(GestionDeSolicitudesEquipos, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = dict()
        # usuario_actual = self.obtener_usuario()
        consulta = self.obtener_consulta()

        context['solicitudes'] = consulta['solicitudes']
        context['tag'] = consulta['tag']
        context['nombre_columnas'] = consulta['nombre_columnas']
        context['filtro_actual'] = consulta['filtro']
        context['tipos'] = [
            {'nombre': 'Todos'},
        ]
        return context

    def obtener_consulta(self):
        from kairos.usuarios.models import Joven
        from kairos.retos.models import SolicitudesRetosEquipos
        from kairos.organizaciones.models import SolicitudesOrganizacionEquipos

        tag = 'Todos'
        solicitudes = []
        grupo_actual = SolicitudesRetosEquipos.objects
        filtro = 'Retos'
        usuario = Joven.objects.get(pk=self.request.user.pk)
        nombres_columnas = ['Equipo', 'Reto', 'Estado', 'Acciones']
        if 'q' in self.request.GET:
            filtro = self.request.GET['q']

            if filtro == 'Retos':
                grupo_actual = SolicitudesRetosEquipos.objects
            elif filtro == 'Organizaciones':
                grupo_actual = SolicitudesOrganizacionEquipos.objects
                nombres_columnas[1] = 'Organizacion'

        if self.request.GET:
            if 'tag' in self.request.GET:
                tag = self.request.GET['tag']
        if tag == 'Todos':
            if filtro == 'Retos':
                solicitudes = grupo_actual.filter(estado='Invitado', equipo__in=usuario.equipos_del_lider.all())
            else:
                solicitudes = grupo_actual.filter(estado='Invitado', equipo__in=usuario.equipos_del_lider.all())

        return {'tag': tag, 'solicitudes': solicitudes, 'nombre_columnas': nombres_columnas, 'filtro': filtro}


@login_required
def RechazarInvitacionOrganizacion(request, id_organizacion, id_equipo):
    '''
        Permite a un cambiar al lider de un equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo
            id_joven: id del Joven que sera nuevo lider
    '''
    from kairos.organizaciones.models import SolicitudesOrganizacionEquipos

    equipo = Equipo.buscar(id_equipo)
    organizacion = Organizacion.buscar(id_organizacion)
    usuario = Joven.objects.get(id=request.user.id)

    if usuario.es_lider() == False:
        return redirect("inicio")

    if equipo == None:
        return redirect("inicio")

    if organizacion == None:
        return redirect("inicio")

    solicitud = SolicitudesOrganizacionEquipos.objects.get(organizacion=organizacion, equipo=equipo)
    solicitud.delete()

    messages.success(request, "Invitación rechazada correctamente")
    return redirect("equipos:gestionar_solicitudes")

@login_required
def AceptarInvitacionOrganizacion(request, id_organizacion, id_equipo):
    '''
        Se encarga de Aceptar la invitacion de una Organizacion a un Equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que recibio la solicitud
            id_organizacion: id de la Organizacion que envio la invitacion
    '''
    from kairos.organizaciones.models import SolicitudesOrganizacionEquipos

    equipo = Equipo.buscar(id_equipo)
    organizacion = Organizacion.buscar(id_organizacion)
    usuario = Joven.objects.get(id=request.user.id)

    if usuario.es_lider() == False:
        return redirect("inicio")

    if equipo == None:
        return redirect("inicio")

    if organizacion == None:
        return redirect("inicio")

    solicitud = SolicitudesOrganizacionEquipos.objects.get(organizacion=organizacion, equipo=equipo)
    solicitud.delete()
    organizacion.equipos.add(equipo)

    messages.success(request, "Invitación aceptada correctamente")
    return redirect("equipos:gestionar_solicitudes")

@login_required
def RechazarInvitacionReto(request, id_reto, id_equipo):
    '''
        Se encarga de Rechazar la invitacion de un Reto a un Equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que recibio la solicitud
            id_reto: id del Reto que envio la invitacion
    '''
    from kairos.retos.models import SolicitudesRetosEquipos, Retos

    equipo = Equipo.buscar(id_equipo)
    reto = Retos.buscar(id_reto)
    usuario = Joven.objects.get(id=request.user.id)

    if usuario.es_lider() == False:
        return redirect("inicio")

    if equipo == None:
        return redirect("inicio")

    if reto == None:
        return redirect("inicio")

    solicitud = SolicitudesRetosEquipos.objects.get(reto=reto, equipo=equipo)
    solicitud.delete()

    messages.success(request, "Invitación rechazada correctamente")
    return redirect("equipos:gestionar_solicitudes")


@login_required
def AceptarInvitacionReto(request, id_reto, id_equipo):
    '''
        Se encarga de Aceptar la invitacion de un Reto a un Equipo

        Parametros:
            request: Elemento Http que posee informacion de la sesion.
            id_equipo: id del Equipo que recibio la solicitud
            id_reto: id del Reto que envio la invitacion
    '''
    from kairos.retos.models import SolicitudesRetosEquipos, Retos, DetalleEquipoReto

    equipo = Equipo.buscar(id_equipo)
    reto = Retos.buscar(id_reto)
    usuario = Joven.objects.get(id=request.user.id)

    if usuario.es_lider() == False:
        return redirect("inicio")

    if equipo == None:
        return redirect("inicio")

    if reto == None:
        return redirect("inicio")

    solicitud = SolicitudesRetosEquipos.objects.get(reto=reto, equipo=equipo)
    solicitud.delete()
    reto.equipos.add(equipo)

    DetalleEquipoReto.asignar_miembros(equipo, reto)

    messages.success(request, "Invitación aceptada correctamente")
    return redirect("equipos:gestionar_solicitudes")

class ApiValidacionEquipos(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        valor = data['valor']
        listado_equipos = []
        if valor:
            equipos = Equipo.validacion_duplicidad_equipos(valor)
            for equipo in equipos:
                listado_equipos.append(equipo.nombre)
        return Response({'equipos': listado_equipos})