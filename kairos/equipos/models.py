# Modulos Django
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

# Modulos de plugin externos
from cities_light.models import Country, City, Region
from simple_history.models import HistoricalRecords
# Modulos de otras apps
from kairos.usuarios.models import Joven

# Modulos internos de la app

def ruta_archivos_logo_y_foto_equipo(instance, filename):
    return "equipos/{}".format(
        filename.encode('ascii', 'ignore'),
    )

class Equipo(models.Model):
    lider = models.ForeignKey('usuarios.Joven', related_name="equipos_del_lider", verbose_name="líder del equipo", on_delete=models.PROTECT)
    miembros = models.ManyToManyField('usuarios.Joven', related_name="miembros_equipos", verbose_name="miembros del equipo")
    solicitudes = models.ManyToManyField('usuarios.Joven', through='SolicitudesEquipos', related_name="equipos_solicitudes", blank=True, verbose_name="personas que van a ser parte del equipo")
    tribus = models.ManyToManyField('insignias.Tribus', related_name="equipos_tribus", verbose_name="seleccionar tribu*")
    tipo = models.ManyToManyField('insignias.TipoDeEquipo', related_name="equipos_tipos", verbose_name="seleccionar tipo de equipo*")
    nombre = models.CharField(max_length=100, verbose_name="nombre del equipo*")
    descripcion = models.TextField(verbose_name="¿Qué hace este equipo?*")
    objetivos = models.TextField(verbose_name="¿Nuestros objetivos?*")
    pais = models.ForeignKey(Country, on_delete=models.PROTECT, related_name='pais_del_equipo',
                             verbose_name="¿En qué país se encuentra?*", default=1)
    region = models.ForeignKey(Region, on_delete=models.PROTECT, related_name='region_del_equipo',
                               verbose_name="¿En qué región se encuentra?*", null=True, blank=False)
    ciudad = models.ForeignKey(City, on_delete=models.PROTECT, related_name='ciudad_del_equipo',
                               verbose_name="¿En qué ciudad se encuentra?*", null=True, blank=False)
    direccion = models.CharField(max_length=100, verbose_name="dirección donde se reúnen frecuentemente", blank=True)
    logo = models.FileField(upload_to=ruta_archivos_logo_y_foto_equipo, blank=True, null=True)
    foto_del_equipo = models.FileField(upload_to=ruta_archivos_logo_y_foto_equipo, blank=True, null=True)
    url_instagram = models.CharField(max_length=600, verbose_name="instagram (url)", blank=True, null=True)
    url_facebook = models.CharField(max_length=600, verbose_name="facebook (url)", blank=True, null=True)
    url_twitter = models.CharField(max_length=600, verbose_name="twitter (url)", blank=True, null=True)
    url_mail = models.CharField(max_length=600, verbose_name="correo", blank=True, null=True)
    url_pagina_web = models.CharField(max_length=600, verbose_name="página web (url)", blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.nombre)

    @staticmethod
    def validacion_duplicidad_equipos(texto:str) -> 'QuerySet[Equipo]':
        equipos = Equipo.objects.filter(nombre__icontains=texto)[:5]
        return equipos

    @staticmethod
    def buscar(id_equipo:int) -> 'Equipo':
        """
        Retorna un Equipo si existe, usando el id como parametro.
        
        Parametros:
            id_equipo(int): id del Equipo a buscar

        Retorno:
            Equipo: Si existe
            None: Si no existe
        """

        try:
            equipo = Equipo.objects.get(id=id_equipo)
            return equipo
        except Equipo.DoesNotExist:
            return None

    @staticmethod
    def existe_equipo(id_equipo:int) -> bool:
        """
        Retorna si existe un Equipo, usando el id como parametro.
        
        Parametros:
            id_equipo(int): id del Equipo a buscar

        Retorno:
            bool: True Si existe, False Si no existe           
        """

        from django.db.models import Count

        if Equipo.objects.filter(id=id_equipo).count() > 0:
            return True
        else:
            return False

    def agregar_reto(self, reto:'Reto'):
        self.retos_equipo.add(reto)

    def obtener_miembros(self):
        return self.miembros.all()

    def miembro_del_equipo(self, joven:'Joven') -> bool:
        """Confirma si el joven es miembro del equipo.

        Args:
            joven (Joven): Joven a confirmar

        Returns:
            bool: True si es miembro, False si NO es miembro
        """
        if joven in self.miembros.all():
            return True
        return False

    def cancelar_invitacion(self, joven:'Joven'):
        self.solicitudes.remove(joven)

    def retirar_postulacion(self, joven:'Joven'):
        self.solicitudes.remove(joven)

    def enviar_solicitud(self, joven:'Joven'):
        invitacionJoven = SolicitudesEquipos.objects.create(equipo=self, joven=joven, estado='Postulado')

    def agregar_organizacion(self, organizacion:'Organizacion'):
        self.organizaciones_equipo.add(organizacion)

    def aceptar_solicitud(self, joven:'Joven'):
        self.miembros.add(joven)
        self.solicitudes.remove(joven)

    def expulsar_miembro(self, joven:'Joven'):
        self.miembros.remove(joven)

    def rechazar_postulacion(self, joven:'Joven'):
        self.solicitudes.remove(joven)

    def salir_del_equipo(self, joven:'Joven'):
        self.miembros.remove(joven)

    def rechazar_invitacion(self, joven:'Joven'):
        self.solicitudes.remove(joven)


    def obtener_competencias_cognitivas(self) -> 'List(Dict)':
        """
        Retorna una lista de diccionarios con la informacion de  todos los jovenes miembros de un equipo en el
        analisis cuantitativo y caulitativo.

        Retorna:
            List(Dict): [{'category': (str), 'BAJO': (int), 'Sin calificación': (int), 'ALTO': (int), 'MEDIO': (int)}...]
        """
        from django.db.models import Q, F, Count, Func, Value, Sum, IntegerField
        from django.db.models.functions import Cast
        from kairos.calificaciones.models import CalificacionRespuestasJoven

        diccionario_lectura = {'category': 'Comprensión lectora'}
        diccionario_cuantitativo = {'category': 'Análisis cuantitativo'}
        cantidad_de_miembros = self.miembros.all().count()

        jovenes_con_calificacion = CalificacionRespuestasJoven.objects.exclude(resultadoD48__isnull=True).filter(respuestas__joven__in=self.miembros.all())

        calificaciones_cuantitativas = list(CalificacionRespuestasJoven.objects.exclude(resultadoD48__isnull=True).filter(respuestas__joven__in=self.miembros.all()).annotate(
            desempenio=Func(F('resultadoD48'), Value('resultado'), function='jsonb_extract_path_text')
        ).annotate(correctas=Func(F('resultadoD48'), Value('correctas'), function='jsonb_extract_path_text')).
                                            values('desempenio', 'correctas').annotate(cantidad=Count('desempenio')))

        calificaciones_lectoras = list(CalificacionRespuestasJoven.objects.exclude(resultadoSM__isnull=True).filter(respuestas__joven__in=self.miembros.all()).annotate(
            desempenio=Func(F('resultadoSM'), Value('resultado'), function='jsonb_extract_path_text')).annotate(correctas=Func(F('resultadoSM'), Value
                                                                                                                               ('correctas'), function='jsonb_extract_path_text')
                                                                                                                ).values('desempenio', 'correctas').annotate(cantidad=Count('desempenio'))
        )

        listado_resultados = ['ALTO', 'BAJO', 'MEDIO']
        listado_cuantitativo = []
        listado_lector = []
        contador = 0
        contador_lector = 0
        cantidad_de_correctas_cuantitativas = 0
        cantidad_de_correctas_lector = 0

        for calificacion in calificaciones_cuantitativas:
            listado_cuantitativo.append(calificacion['desempenio'])
            diccionario_cuantitativo[calificacion['desempenio']] = calificacion['cantidad']
            contador += calificacion['cantidad']
            if calificacion['correctas'] != None:
                cantidad_de_correctas_cuantitativas += int(calificacion['correctas'])

        diferencia = list(set(listado_resultados) - set(listado_cuantitativo))
        sin_resultado = 'Sin calificación'

        if len(diferencia) > 0:
            for diferente in diferencia:
                diccionario_cuantitativo[sin_resultado] = (cantidad_de_miembros - contador)
                diccionario_cuantitativo[diferente] = 0

        for calificacion in calificaciones_lectoras:
            listado_lector.append(calificacion['desempenio'])
            diccionario_lectura[calificacion['desempenio']] = calificacion['cantidad']
            contador_lector += calificacion['cantidad']
            if calificacion['correctas'] != None:
                cantidad_de_correctas_lector += int(calificacion['correctas'])

        diferencia_lector = list(set(listado_resultados) - set(listado_lector))

        if len(diferencia_lector) > 0:
            for diferente in diferencia_lector:
                diccionario_lectura[sin_resultado] = (cantidad_de_miembros - contador_lector)
                diccionario_lectura[diferente] = 0

        if jovenes_con_calificacion.count() > 0:
            diccionario_lectura['promedio_cuantitativo'] = (cantidad_de_correctas_lector / jovenes_con_calificacion.count())

        lista_retorno = []
        lista_retorno.append(diccionario_cuantitativo)
        lista_retorno.append(diccionario_lectura)
        
        return lista_retorno

    def obtener_desempenio_por_habilidades_sociales_equipo(self) -> 'List(Dict)':
        """
        Retorna una lista de diccionarios con la informacion de  todos los jovenes miembros de un equipo en habilidades sociales.

        Retorna:
            List(Dict): [{'category': (str), 'promedio_jovenes': (float)}...]
        """
        from django.db.models import Q, Count, Func, Value, F, Sum, FloatField, Avg
        from kairos.calificaciones.models import CalificacionRespuestasJoven
        from django.db.models.functions import Cast

        lista_habilidades = ['Carisma', 'Inspiración', 'Dirección',
                             'Desarrollo', 'Innovación',
                             'Negociación']
        lista_retorno = []

        jovenes_con_calificacion = CalificacionRespuestasJoven.objects.exclude(resultadoD48__isnull=True).filter(respuestas__joven__in=self.miembros.all())

        for competencia in lista_habilidades:
            diccionario_auxiliar = dict()
            calificaciones_habilidades = list(CalificacionRespuestasJoven.objects.exclude(
                promediosLiderazgo__isnull=True
            ).filter(respuestas__joven__in=self.miembros.all()
                     ).values('promediosLiderazgo').annotate(
                         promedio=Func(F('promediosLiderazgo'), Value('competencias'), Value(competencia), Value('promedio'), function='jsonb_extract_path_text')).values('promedio'))

            if competencia == 'Innovación':
                diccionario_auxiliar['category'] = 'Innovación'
            elif competencia == 'Negociación':
                diccionario_auxiliar['category'] = 'Claridad en negociar'
            else:
                diccionario_auxiliar['category'] = competencia

            total_calificacion = 0
            promedio = 0
            for calificacion in calificaciones_habilidades:
                total_calificacion += float(calificacion['promedio'])

            if jovenes_con_calificacion.count() > 0:
                promedio = total_calificacion / jovenes_con_calificacion.count()

            diccionario_auxiliar['promedio_jovenes'] = promedio
            lista_retorno.append(diccionario_auxiliar)
        
        return lista_retorno

    def obtener_desempenio_por_sterret(self) -> 'List(Dict)':
        """
        Retorna una lista de diccionarios con la informacion de  todos los jovenes miembros de un equipo en habilidades sociales.

        Retorna:
            List(Dict): [{'category': (str), 'promedio_jovenes': (float)}...]
        """
        from django.db.models import Q, Count, Func, Value, F
        from kairos.calificaciones.models import CalificacionRespuestasJoven

        lista_competencias = ['Autoconciencia', 'Autocontrol', 'Autoconfianza', 'Empatía', 'Motivación', 'Socializar']
        lista_retorno = []

        jovenes_con_calificacion = CalificacionRespuestasJoven.objects.exclude(resultadoD48__isnull=True).filter(respuestas__joven__in=self.miembros.all())

        for competencia in lista_competencias:
            diccionario_auxiliar = dict()
            calificaciones_competencias = list(CalificacionRespuestasJoven.objects.exclude(
                puntajesSterret__isnull=True
            ).filter(respuestas__joven__in=self.obtener_miembros()
                    ).values('puntajesSterret').annotate(
                             desempenio=Func(F('puntajesSterret'), Value('competencias'), Value(competencia), Value('puntaje'),
                                             function='jsonb_extract_path_text')).values('desempenio').annotate(cantidad=Count('desempenio')))

            diccionario_auxiliar['category'] = competencia

            total_calificacion = 0
            promedio = 0
            for calificacion in calificaciones_competencias:
                total_calificacion += float(calificacion['desempenio'])

            if jovenes_con_calificacion.count() > 0:
                promedio = total_calificacion / jovenes_con_calificacion.count()

            diccionario_auxiliar['promedio_jovenes'] = promedio
            lista_retorno.append(diccionario_auxiliar)

             
        return lista_retorno

    def obtener_puntaje_global_sterret(self) -> 'List(Dict)':
        """
        Retorna una lista de diccionarios con la informacion de todos los jovenes miembros de un equipo
        categorizandolos en la cantidad de jovenes con ALTO, MEDIO y BAJO

        Retorna:
            List(Dict): {'categoria': (str), 'cantidad': (int)}
        """
        from django.db.models import Q, Count, Func, Value, F, Sum, IntegerField, Avg
        from kairos.calificaciones.models import CalificacionRespuestasJoven
        from django.db.models.functions import Cast

        cantidad_de_miembros = self.miembros.all().count()

        lista_retorno = []
        desempenio = CalificacionRespuestasJoven.objects.exclude(
            puntajesSterret__isnull=True
        ).filter(
            respuestas__joven__in=self.miembros.all()
        ).values('puntajesSterret').annotate(
            desempenio=Func(F('puntajesSterret'), Value('global'), function='jsonb_extract_path_text')
        ).values('desempenio').annotate(desempenio_int=Cast('desempenio', IntegerField()))

        jovenes_con_calificacion = (desempenio.filter(desempenio_int__gt=120).count() + desempenio.filter(desempenio_int__lt=90).count()
                                    + desempenio.filter(desempenio_int__gt=90, desempenio_int__lt=120).count())

        sin_calificacion = (cantidad_de_miembros - jovenes_con_calificacion)

        jovenes_alto = {'categoria': 'ALTO', 'cantidad': desempenio.filter(desempenio_int__gt=120).count()}
        jovenes_bajo = {'categoria': 'BAJO', 'cantidad': desempenio.filter(desempenio_int__lt=90).count()}
        jovenes_medio = {'categoria': 'MEDIO', 'cantidad': desempenio.filter(desempenio_int__gt=90, desempenio_int__lt=120).count()}
        sin_calificacion_dic = {'categoria': 'SIN CALIFICACIÓN', 'cantidad': sin_calificacion}
        lista_retorno.append(jovenes_alto)
        lista_retorno.append(jovenes_bajo)
        lista_retorno.append(jovenes_medio)
        lista_retorno.append(sin_calificacion_dic)
        
        return lista_retorno

    def obtener_estilos_de_liderazgo(self) -> 'List(Dict)':
        """
        Retorna una lista de diccionarios con la informacion de todos los jovenes miembros de un equipo
        categorizandolos en la cantidad de jovenes con tipo de liderazgo.

        Retorna:
            List(Dict): [{'category': estilo liderazgo(str), 'cantidad': (int)}...]
        """
        from django.db.models import Q, Count, Func, Value, F
        from django.db.models.functions import Greatest
        from kairos.calificaciones.models import CalificacionRespuestasJoven

        listado_retorno = []
        cantidad_de_miembros = self.miembros.all().count()

        calificaciones = CalificacionRespuestasJoven.objects.exclude(
            promediosLiderazgo__isnull=True
            ).filter(
                respuestas__joven__in=self.miembros.all()
                ).values('promediosLiderazgo').annotate(
                    desempenio_transaccional=Func(F('promediosLiderazgo'), Value('dimensiones'), Value('transaccional'), function='jsonb_extract_path_text')
                    ).annotate(
                        desempenio_transformacional=Func(F('promediosLiderazgo'), Value('dimensiones'), Value('transformacional'), function='jsonb_extract_path_text')
                        ).annotate(
                             desempenio_laizzes_faire=Func(F('promediosLiderazgo'), Value('dimensiones'), Value('laizzes_faire'),
                                                           function='jsonb_extract_path_text')
                             ).values('desempenio_transaccional', 'desempenio_transformacional', 'desempenio_laizzes_faire')

        cantidad_transaccional = 0
        cantidad_transformacional = 0
        cantidad_laizzes_faire = 0
        jovenes_con_calificacion = 0

        for calificacion in calificaciones:
            calificacion_maxima = max(calificacion, key=calificacion.get)
            if calificacion_maxima == 'desempenio_transaccional':
                cantidad_transaccional += 1
                jovenes_con_calificacion += 1
            elif calificacion_maxima == 'desempenio_transformacional':
                cantidad_transformacional += 1
                jovenes_con_calificacion += 1
            elif calificacion_maxima == 'desempenio_laizzes_faire':
                cantidad_laizzes_faire += 1
                jovenes_con_calificacion += 1

        sin_calificacion = (cantidad_de_miembros - jovenes_con_calificacion)

        listado_retorno.append({'category': 'TRANSACCIONAL', 'cantidad': cantidad_transaccional})
        listado_retorno.append({'category': 'TRANSFORMACIONAL', 'cantidad': cantidad_transformacional})
        listado_retorno.append({'category': 'SIN ESTILO DE LIDERAZGO DEFINIDO', 'cantidad': cantidad_laizzes_faire})
        listado_retorno.append({'category': 'SIN CALIFICACIÓN', 'cantidad': sin_calificacion})
        
        return listado_retorno


    def obtener_desempenio_por_aptitudes_e_intereses(self) -> 'List(Dict)':
        """
        Retorna una lista de diccionarios con la informacion de todos los jovenes miembros de un equipo por su 
        desempeño en intereses y aptitudes.

        Retorna:
            List(Dict): [{'category': tribu(str), 'ALTO': (int), 'BAJO': (int), 'MEDIO': (int), 'SIN CLASIFICACIÓN': (int)}...]
        """
        from django.db.models import Q, Count, Func, Value, F
        from django.db.models.functions import Greatest
        from kairos.calificaciones.models import CalificacionRespuestasJoven

        lista_areas = ['C', 'I', 'S', 'H', 'A', 'E', 'D']
        diccionario_relacion_letra_con_competencia = {'C': 'Gronor',
                                                        'E': 'Moterra',
                                                        'H': 'Vincularem',
                                                        'S': 'Cuisana',
                                                        'I': 'Centeros',
                                                        'D': 'Ponor',
                                                        'A': 'Brianimus'}

        lista_retorno = []
        for area in lista_areas:
            diccionario_auxiliar = dict()
            calificaciones_areas = list(CalificacionRespuestasJoven.objects.exclude(
                puntajesCHASIDE__isnull=True
            ).filter(
                respuestas__joven__in=self.miembros.all()
            ).values('puntajesCHASIDE').annotate(
                desempenio=Func(F('puntajesCHASIDE'), Value(area), Value('resultado'), function='jsonb_extract_path_text')
            ).values('desempenio').annotate(cantidad=Count('desempenio')))

            diccionario_auxiliar['category'] = diccionario_relacion_letra_con_competencia[area]
            for calificacion in calificaciones_areas:
                diccionario_auxiliar[calificacion['desempenio']] = calificacion['cantidad']
            lista_retorno.append(diccionario_auxiliar)

        cantidad_de_jovenes_con_calificacion = CalificacionRespuestasJoven.objects.exclude(
            puntajesCHASIDE__isnull=True
            ).filter(
                respuestas__joven__in=self.miembros.all()
                ).count()
        cantidad_de_miembros = self.miembros.all().count()

        listado_resultados = ['ALTO', 'BAJO', 'MEDIO', 'SIN CLASIFICACIÓN']
        listado_resultados_actuales = []
        listado_lector = []
        sin_calificacion = (cantidad_de_miembros - cantidad_de_jovenes_con_calificacion)
        for calificacion in lista_retorno:
            for resultado in listado_resultados:
                if not resultado in calificacion:
                    if resultado != 'SIN CLASIFICACIÓN':
                        calificacion[resultado] = 0
                    else:
                        calificacion[resultado] = sin_calificacion

        return lista_retorno

    def obtener_rendimiento_por_asignatura(self) -> 'List(Dict)':
        """
        Retorna una lista de diccionarios con la informacion de todos los jovenes miembros de un equipo por su 
        rendimiento en asignaturas.

        Retorna:
            List(Dict): [{'category': experiencias(str), 'No me va bien, pero me gusta': (int), 'Sin clasificar': (int)},...]
        """

        from django.db.models import Q, Count, Func, Value, F
        from kairos.core.utils import obtener_key

        miembros_del_equipo = self.miembros.all()
        cantidad_de_miembros = miembros_del_equipo.count()
        listado_experiencias = {
            'Física': 'rendimiento_fisica', 'Informática': 'rendimiento_informatica', 'Matemática': 'rendimiento_matematica', 'Historia': 'rendimiento_historia', 'Literatura': 'rendimiento_literatura',
            'Idiomas Extranjeros': 'rendimiento_idiomas_extranjeros', 'Idioma Nativo y Literatura': 'rendimiento_idiomas_nativos', 'Artes': 'rendimiento_artes', 'Deportes y Educación Física': 'rendimiento_deportes',
            'Ciencias Naturales y Biología': 'rendimiento_naturales', 'Geografía': 'rendimiento_geografia', 'Economía, Contabilidad o Administracion': 'rendimiento_economia', 'Química': 'rendimiento_quimica', 'Salud del Ser Humano': 'rendimiento_salud',
            'Orden cerrado, Campamentos y Ordenamiento Militar en colegios': 'rendimiento_militar'
        }
        diccionario_numero_asociado_rendimiento = {
            "5": "No me va bien, pero me gusta",
            "4": "Soy muy bueno, se me hace fácil y me gusta",
            "3": "Me va bien, aun cuando no es lo que más me gusta",
            "2": "Normal, logro aprobar",
            "1": "Suelo tener dificultades, no es mi área fuerte",
            "0": "Ese curso nunca lo he visto"
        }
        listado_retorno = []
        categoria_sin_calificar = 'Sin clasificar'
        jovenes_no_calificados = 0

        for experiencia in listado_experiencias.values():
            dic_aux = {'category': ''}
            parameters = [experiencia]
            null_filters = {"{}__isnull".format(param): True for param in parameters}
            rendimiento_y_cantidad = miembros_del_equipo.values(*parameters).exclude(**null_filters).annotate(cantidad=Count(*parameters)).order_by(*parameters)
            total_jovenes_calificados = miembros_del_equipo.exclude(**null_filters).count()
            jovenes_no_calificados = (cantidad_de_miembros - total_jovenes_calificados)
            dic_aux['category'] = obtener_key(listado_experiencias, experiencia)
            listado_retorno.append(dic_aux)
            for rendimiento in rendimiento_y_cantidad:
                rendimiento_asociado = diccionario_numero_asociado_rendimiento[rendimiento[experiencia]]
                dic_aux[rendimiento_asociado] = rendimiento['cantidad']
                rendimiento[experiencia] = rendimiento_asociado
            dic_aux[categoria_sin_calificar] = jovenes_no_calificados

        return listado_retorno

    def obtener_numero_de_experiencias_del_equipo(self) -> 'List(Dict)':
        """
        Retorna una lista de diccionarios con la informacion de todos los jovenes miembros de un equipo por su 
        numero de experiencias

        Retorna:
            List(Dict): [{'category': experiencia(str), cantidad:(int)},...]
        """
        from kairos.insignias.models import Tribus
        from django.db.models import Count

        tribus = Tribus.objects.all()
        diccionario_jovenes = {
            'Club juvenil': dict(),
            'Voluntariado': dict(),
            'Colaborador': dict(),
            'Independiente': dict(),
            'Emprendimiento': dict(),
        }

        for tribu in tribus:
            cantidad_clubes = self.miembros.filter(clubes_del_joven__tribu=tribu).count()
            cantidad_voluntariados = self.miembros.filter(voluntariados_del_joven__tribu=tribu).count()
            cantidad_laborales = self.miembros.filter(experiencias_laborales_del_joven__tribu=tribu).count()
            cantidad_independientes = self.miembros.filter(trabajos_independientes_del_joven__tribu=tribu).count()
            cantidad_emprendimientos = self.miembros.filter(emprendimientos_del_joven__tribu=tribu).count()

            if cantidad_clubes > 0:
                diccionario_jovenes['Club juvenil'].update({'cantidad': cantidad_clubes})
            if cantidad_voluntariados > 0:
                diccionario_jovenes['Voluntariado'].update({'cantidad': cantidad_voluntariados})
            if cantidad_laborales > 0:
                diccionario_jovenes['Colaborador'].update({'cantidad': cantidad_laborales})
            if cantidad_independientes > 0:
                diccionario_jovenes['Independiente'].update({'cantidad': cantidad_independientes})
            if cantidad_emprendimientos > 0:
                diccionario_jovenes['Emprendimiento'].update({'cantidad': cantidad_emprendimientos})

        lista_retorno = []
        for experiencia, tribus in diccionario_jovenes.items():
            tribus.update({'category': experiencia})
            lista_retorno.append(tribus)
        print(lista_retorno)
        return lista_retorno

    @staticmethod
    def obtener_mis_solicitudes(joven):
        if (joven.equipos_solicitudes.exists() == True):
            return joven.equipos_solicitudes.all()
        return None

    @staticmethod
    def obtener_mis_equipos(joven):
        if joven.equipos_solicitudes.all().exists() == True:
            equipos = joven.miembros_equipos.all() | joven.equipos_solicitudes.all()
            return equipos.distinct()
        return joven.miembros_equipos.all()

    @staticmethod
    def obtener_equipos_favoritos(usuario):
        equipos = Equipo.objects.all()
        mis_favoritos = equipos.filter(calificacion_equipo__usuario=usuario,
                                        calificacion_equipo__calificacion=1)
        return mis_favoritos

    def cambio_de_lider(self, miembro):
        self.lider = miembro
        self.save()

    @staticmethod
    def obtener_sugeridos(usuario):
        from kairos.insignias.models import Roles
        from django.db.models import Count

        conjunto_de_equipos_sugeridos = []

        if (not usuario.miembros_equipos.all() == True) and (not usuario.equipos_solicitudes.all()):
            mi_perfil_favorito = usuario.joven_dueno_perfil.filter(favorito=True)

            if mi_perfil_favorito.exists():
                mi_perfil_favorito = mi_perfil_favorito.first()
                mi_tribu = mi_perfil_favorito.tribu_deseada
                equipos = Equipo.objects.exclude(calificacion_equipo__usuario=usuario, calificacion_equipo__calificacion=0)
                obtener_equipos = equipos.exclude(lider=usuario).annotate(cantidad=Count("tribus")).distinct()
                for equipo in obtener_equipos:
                    if (mi_tribu in equipo.tribus.all()):
                        conjunto_de_equipos_sugeridos.append(equipo)
        return conjunto_de_equipos_sugeridos

    @staticmethod
    def buscador(query=None, equipos=None):
        from django.db.models import Q

        queries = query.split(' ')
        for q in queries:
            equipos = equipos.filter(
                Q(nombre__icontains=q) | Q(pais__name__icontains=q) | Q(region__name__icontains=q)
                | Q(ciudad__name__icontains=q) | Q(descripcion__icontains=q) | Q(objetivos__icontains=q)).distinct()
        return equipos

@receiver(post_save, sender=Equipo, dispatch_uid="minificar_imagen_equipo")
def comprimir_imagen_equipo(sender, **kwargs):
    from kairos.core.utils import comprimir_imagen
    if kwargs["instance"].logo:
        comprimir_imagen(kwargs["instance"].logo.path)
    if kwargs['instance'].foto_del_equipo:
        comprimir_imagen(kwargs["instance"].foto_del_equipo.path)

class Proyecto(models.Model):
    nombre = models.CharField(max_length=100, verbose_name="nombre del proyecto*")
    anio = models.PositiveIntegerField(verbose_name="Año de creación del proyecto*")
    descripcion = models.TextField(verbose_name="descripción del proyecto*")
    link = models.CharField(max_length=600, verbose_name="página web (url)", blank=True, null=True)
    equipo = models.ForeignKey('equipos.Equipo', related_name="proyectos_del_equipo", on_delete=models.PROTECT)
    history = HistoricalRecords()

    @staticmethod
    def buscar(id_proyecto):
        try:
            proyecto = Proyecto.objects.get(id=id_proyecto)
            return proyecto
        except Proyecto.DoesNotExist:
            return None

    def informacion_completa(self):
        if (len(self.nombre) > 0) and (len(str(self.anio)) > 0) and (len(self.descripcion) > 0):
            return True
        return False

    @staticmethod
    def verificar_existecia(equipo, listado_proyectos):
        proyectos = equipo.proyectos_del_equipo.all()
        set_de_listado_proyectos = set(listado_proyectos)
        set_de_proyectos_en_bd = set(list(proyectos))
        set_diferencia = set_de_proyectos_en_bd.difference(set_de_listado_proyectos)
        for proyecto_diferente in set_diferencia:
            if proyecto_diferente in proyectos:
                proyecto_diferente.delete()
        return True


class CalificacionEquipo(models.Model):
    equipo = models.ForeignKey('equipos.Equipo', on_delete=models.CASCADE, verbose_name='equipo calificado',
                                  related_name='calificacion_equipo')
    usuario = models.ForeignKey('usuarios.Usuario', on_delete=models.CASCADE, verbose_name='usuario que califica',
                                related_name='usuario_calificador_equipo')
    calificacion = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                               verbose_name='calificación: 1 me gusta, 0 no me gusta')
    history = HistoricalRecords()


class SolicitudesEquipos(models.Model):
    equipo = models.ForeignKey('equipos.Equipo', related_name='solicitudes_del_equipo', on_delete=models.CASCADE)
    joven = models.ForeignKey('usuarios.Joven', related_name='solicitudes_del_joven', on_delete=models.CASCADE)
    estado = models.CharField(max_length=15, verbose_name='estado')
    history = HistoricalRecords()

    @staticmethod
    def es_solicitud_enviada(equipo, joven):
        try:
            solicitud = SolicitudesEquipos.objects.get(equipo=equipo, joven=joven)
            if solicitud.estado == 'Postulado':
                return True
            else:
                return False
        except SolicitudesEquipos.DoesNotExist:
            return False
        return True

    @staticmethod
    def existe_solitud(equipo, joven):
        try:
            solicitud = SolicitudesEquipos.objects.get(equipo=equipo, joven=joven)
        except SolicitudesEquipos.DoesNotExist:
            return False
        return True