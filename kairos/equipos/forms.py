# Modulos Django
from django import forms

# Modulos de plugin externos
from cities_light.models import City, Country, Region
from django_select2.forms import Select2MultipleWidget, ModelSelect2Widget

# Modulos de otras apps
from kairos.equipos.models import *

# Modulos internos de la app

class EquipoModelForm(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = ["nombre", "descripcion", "objetivos", "pais", "region", "ciudad", "direccion", "logo",
                  "foto_del_equipo", "tribus", "tipo", "url_instagram", "url_facebook", "url_twitter", "url_mail", "url_pagina_web"]

        widgets = {
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'objetivos': forms.Textarea(
                attrs={
                    'rows': 3,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
            'pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'region': ModelSelect2Widget(
                model=Region,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains'],
                dependent_fields={'pais': 'country_id'},
                max_results=100,
            ),
            'ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains', 'search_names__icontains', 'id__icontains'],
                dependent_fields={'region': 'region_id'},
                max_results=100,
            ),
            'tribus': Select2MultipleWidget(

            ),
            'tipo': Select2MultipleWidget(

            ),
        }

class ProyectoModelForm(forms.ModelForm):

    class Meta:
        model = Proyecto
        fields = ["nombre", "anio", "descripcion", "link"]

        widgets = {
            'descripcion': forms.Textarea(
                attrs={
                    'rows': 5,
                    'cols': 22,
                    'style': 'resize:none;'
                }
            ),
        }

    def __init__(self, *args, **kwargs):
        super(ProyectoModelForm, self).__init__(*args, **kwargs)
        self.fields["anio"].label = "Año del proyecto"

class InvitacionesModelForm(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = ["solicitudes"]

        widgets = {
            'solicitudes': Select2MultipleWidget(
            ),
        }
        
    def __init__(self, *args, **kwargs):
        user = kwargs.pop("usuario")
        super(InvitacionesModelForm, self).__init__(*args, **kwargs)
        miembros = self.instance.miembros.all()
        self.fields["solicitudes"].queryset = Joven.objects.exclude(id__in=miembros)

class FormularioInvitacionJovenAEquipo(forms.Form):

    invitar_por_correo = forms.CharField(
        label="Invitar por correo"
    )

    def __init__(self, *args, **kwargs):
        super(FormularioInvitacionJovenAEquipo, self).__init__(*args, **kwargs)
        self.fields['invitar_por_correo'].required = False


