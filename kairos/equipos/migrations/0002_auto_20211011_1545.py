# Generated by Django 3.0.2 on 2021-10-11 15:45

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('equipos', '0001_initial'),
        ('insignias', '0002_auto_20211011_1545'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('usuarios', '0001_initial'),
        ('cities_light', '0008_city_timezone'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicitudesequipos',
            name='joven',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='solicitudes_del_joven', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='proyecto',
            name='equipo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='proyectos_del_equipo', to='equipos.Equipo'),
        ),
        migrations.AddField(
            model_name='historicalsolicitudesequipos',
            name='equipo',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='equipos.Equipo'),
        ),
        migrations.AddField(
            model_name='historicalsolicitudesequipos',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalsolicitudesequipos',
            name='joven',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='usuarios.Joven'),
        ),
        migrations.AddField(
            model_name='historicalproyecto',
            name='equipo',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='equipos.Equipo'),
        ),
        migrations.AddField(
            model_name='historicalproyecto',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalequipo',
            name='ciudad',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cities_light.City', verbose_name='¿En qué ciudad se encuentra?*'),
        ),
        migrations.AddField(
            model_name='historicalequipo',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalequipo',
            name='lider',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='usuarios.Joven', verbose_name='líder del equipo'),
        ),
        migrations.AddField(
            model_name='historicalequipo',
            name='pais',
            field=models.ForeignKey(blank=True, db_constraint=False, default=1, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cities_light.Country', verbose_name='¿En qué país se encuentra?*'),
        ),
        migrations.AddField(
            model_name='historicalequipo',
            name='region',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cities_light.Region', verbose_name='¿En qué región se encuentra?*'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionequipo',
            name='equipo',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='equipos.Equipo', verbose_name='equipo calificado'),
        ),
        migrations.AddField(
            model_name='historicalcalificacionequipo',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalcalificacionequipo',
            name='usuario',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='usuario que califica'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='ciudad',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='ciudad_del_equipo', to='cities_light.City', verbose_name='¿En qué ciudad se encuentra?*'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='lider',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='equipos_del_lider', to='usuarios.Joven', verbose_name='líder del equipo'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='miembros',
            field=models.ManyToManyField(related_name='miembros_equipos', to='usuarios.Joven', verbose_name='miembros del equipo'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='pais',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, related_name='pais_del_equipo', to='cities_light.Country', verbose_name='¿En qué país se encuentra?*'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='region',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='region_del_equipo', to='cities_light.Region', verbose_name='¿En qué región se encuentra?*'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='solicitudes',
            field=models.ManyToManyField(blank=True, related_name='equipos_solicitudes', through='equipos.SolicitudesEquipos', to='usuarios.Joven', verbose_name='personas que van a ser parte del equipo'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='tipo',
            field=models.ManyToManyField(related_name='equipos_tipos', to='insignias.TipoDeEquipo', verbose_name='seleccionar tipo de equipo*'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='tribus',
            field=models.ManyToManyField(related_name='equipos_tribus', to='insignias.Tribus', verbose_name='seleccionar tribu*'),
        ),
        migrations.AddField(
            model_name='calificacionequipo',
            name='equipo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calificacion_equipo', to='equipos.Equipo', verbose_name='equipo calificado'),
        ),
        migrations.AddField(
            model_name='calificacionequipo',
            name='usuario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='usuario_calificador_equipo', to=settings.AUTH_USER_MODEL, verbose_name='usuario que califica'),
        ),
    ]
