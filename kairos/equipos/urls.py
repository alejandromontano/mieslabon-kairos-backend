# Modulos Django
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "equipos"

urlpatterns = [
    path('gestionar', view=GestionDeEquiposLider.as_view(), name='gestionar_equipos'),
    path('registrar', view=EquipoRegistrar.as_view(), name='registrar'),
    path('<int:id_equipo>/registrar-proyecto', view=RegistroDeProyectos, name='registrar_proyecto'),
    path('<int:id_equipo>/enviar-invitaciones', view=RegistroDeInvitaciones, name='enviar_invitaciones'),
    path('cancelar-invitacion/<int:id_joven>/<int:id_equipo>', view=CancelarInvitacion, name='cancelar_invitacion'),

    path('todos-los-equipos', view=EquipoConsultar.as_view(), name='consultar_todos_los_equipos'),
    path('modificar-equipo/<int:id_equipo>', view=ModificarEquipo.as_view(), name='modificar_equipo'),
    path('modificar-proyecto/<int:id_proyecto>', view=ModificarProyecto.as_view(), name='modificar_proyecto'),
    path('gestion-solicitudes', view=GestionDeSolicitudesEquipos.as_view(), name='gestionar_solicitudes'),

    path('detalle-perfil/<int:id_equipo>', view=EquipoDetalle.as_view(), name='detalle_equipo'),
    path('quienes-somos/<int:id_equipo>', view=QuienesSomosEquipoDetalle.as_view(), name='quienes_somos'),
    path('miembros/<int:id_equipo>', view=MiembrosEquipoDetalle.as_view(), name='miembros'),
    path('estado/<int:id_equipo>', view=EstadoEquipoDetalle.as_view(), name='estado'),

    path("api/cargar-calificaciones", view=ApiCargarCalificaciones.as_view(), name="api_cargar_calificaciones"),
    path("api/cargar-calificacion", view=ApiCargarCalificacion.as_view(), name="api_cargar_calificacion"),
    path("api/calificar", view=ApiCalificarContenido.as_view(), name="api_calificar"),
    path("api/validar-equipos", view=ApiValidacionEquipos.as_view(), name="api_validar_equipos"),    

    path('postularme/<int:id_joven>/<int:id_equipo>', view=Postularme, name='postularme'),
    path('aceptar-postulacion/<int:id_joven>/<int:id_equipo>', view=AceptarPostulacion, name='aceptar_postulacion'),
    path('aceptar-invitacion/<int:id_joven>/<int:id_equipo>', view=AceptarInvitacion, name='aceptar_invitacion'),
    path('expulsar-miembro/<int:id_joven>/<int:id_equipo>', view=EliminarDelEquipo, name='expulsar_miembro'),
    path('rechazar-postulacion/<int:id_joven>/<int:id_equipo>', view=RechazarPostulacion, name='rechazar_postulacion'),
    path('salir-del-equipo/<int:id_joven>/<int:id_equipo>', view=SalirDelEquipo, name='salir_del_equipo'),
    path('rechazar-invitacion/<int:id_joven>/<int:id_equipo>', view=RechazarInvitacion, name='rechazar_invitacion'),
    path('retirar-postulacion/<int:id_joven>/<int:id_equipo>', view=RetirarPostulacion, name='retirar_postulacion'),
    path('cambiar-lider/<int:id_joven>/<int:id_equipo>', view=CambiarLider, name='cambio_de_lider'),

    path('tablero-competencias-cognitivas/<int:id_equipo>', view=TableroEquipoCompetenciasCognitivas.as_view(), name='competencias_cognitivas'),
    path('tablero-competencias-habilidades-sociales/<int:id_equipo>', view=TableroEquipoCompetenciasHabilidadesSociales.as_view(), name='habilidades_sociales'),
    path('tablero-competencias-aptitudes-e-intereses/<int:id_equipo>', view=TableroEquipoCompetenciasAptitudesEIntereses.as_view(), name='aptitudes_e_intereses'),
    path('tablero-competencias-experiencias/<int:id_equipo>', view=TableroEquipoCompetenciasExperiencias.as_view(), name='competencias_experiencias'),

    path('rechazar-invitacion-organizacion/<int:id_organizacion>/<int:id_equipo>', view=RechazarInvitacionOrganizacion, name='rechazar_invitacion_de_organizacion'),
    path('aceptar-invitacion-organizacion/<int:id_organizacion>/<int:id_equipo>', view=AceptarInvitacionOrganizacion, name='aceptar_invitacion_de_organizacion'),
    path('rechazar-invitacion-reto/<int:id_reto>/<int:id_equipo>', view=RechazarInvitacionReto, name='rechazar_invitacion_de_reto'),
    path('aceptar-invitacion-reto/<int:id_reto>/<int:id_equipo>', view=AceptarInvitacionReto, name='aceptar_invitacion_de_reto'),
]
