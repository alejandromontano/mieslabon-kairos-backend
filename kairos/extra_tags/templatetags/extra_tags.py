from django import template
from django.utils import timezone
from datetime import datetime
from dateutil.relativedelta import relativedelta
from kairos.respuestas_formularios.models import RespuestasJoven
from kairos.usuarios.models import Joven, Usuario
from kairos.retos.models import Retos
from kairos.guia.models import CalificacionRecurso
from kairos.retos.models import DetalleEquipoReto
import requests
from django.db.models import Q
register = template.Library()


@register.filter(name='joven_termino_cuestionarios')
def joven_termino_cuestionarios(id_joven):  # value: organizacion - contenido
    usuario = Usuario.objects.get(pk=id_joven)
    if usuario.tipo=='Joven':
        perfil_deseado = Joven.objects.get(pk=id_joven).joven_dueno_perfil.all()
        if len(perfil_deseado) > 0:
            return True
        else:
            return False
    else:
        return True

@register.filter(name='joven_termino_formularios')
def joven_termino_formularios(id_joven):  # value: organizacion - contenido
    usuario = Usuario.objects.get(pk=id_joven)
    if usuario.tipo=='Joven':
        joven = Joven.objects.get(pk=id_joven)
        formularios_joven = joven.tanda_formularios.formularios.filter(activo=True)
        formularios_disponibles = formularios_joven.filter(
            ~Q(respuestasjoven__joven=joven.id) | Q(respuestasjoven__joven=joven.id, respuestasjoven__completas=False)
        )
        if formularios_disponibles.count() == 0:
            return True
        else:
            return False
    else:
        return True


@register.filter
def edad(value):  # value: joven
    hoy = datetime.now().date()
    return relativedelta(hoy, value.fecha_de_nacimiento).years

@register.filter
def cambiar_espacio_por_mas(value):  # value: string
    return value.replace(" ", "+")


@register.filter(name='total_tribus_roles_niveles')
def total_tribus_roles_niveles(value):  # value: organizacion - contenido
    return value.tribus_relacionadas.all().count() + value.roles.all().count() + value.niveles_conocimiento.all().count()

@register.simple_tag(name='reto_abierto')
def reto_abierto(id_reto, id_joven):  # value: organizacion - contenido
    from kairos.usuarios.models import Joven
    from kairos.equipos.models import Equipo

    reto = Retos.objects.get(pk=id_reto)
    org = reto.organizacion
    try:
        joven = Joven.objects.get(pk=id_joven)
    except Joven.DoesNotExist:
        return False

    equipos = Equipo.objects.filter(lider=joven)

    equipo_miembro = False
    if 'Equipos' in reto.grupos_permitidos:
        for equipo in equipos:
            if equipo_miembro == False:
                for organizacion in reto.organizaciones_permitidas.all():
                    if equipo in organizacion.equipos.all():
                        equipo_miembro = True

                if equipo in org.equipos.all():
                    es_miembro = True

    es_miembro = False
    if 'Jóvenes' in reto.grupos_permitidos:        
        for orga in reto.organizaciones_permitidas.all():
            if orga.joven_es_miembro(joven):
                es_miembro = True

        if org.joven_es_miembro(joven):
            es_miembro = True

    if reto.permitir_todos == False:
        if es_miembro == True or equipo_miembro == True:
            return True
        else:
            return False
    else:
        return True

@register.simple_tag(name='esta_participante_en_reto')
def esta_participante_en_reto(id_reto, id_participante):
    from kairos.retos.models import DetalleJovenReto, DetalleEquipoReto
    from kairos.equipos.models import Equipo
    
    reto = Retos.objects.get(pk=id_reto)
    if 'Equipos' in reto.grupos_permitidos:
        equipos = Equipo.objects.filter(miembros__pk__in=[id_participante])            
        detalles = DetalleEquipoReto.objects.filter(equipo__in=equipos, reto__pk=id_reto)
    else:
        detalles = DetalleJovenReto.objects.filter(joven__pk=id_participante, reto__pk=id_reto) 

    if detalles.count() > 0:
        return True
    else:
        return False

@register.simple_tag(name='obtener_calificaciones_joven')
def obtener_calificaciones_joven(id_joven_calificado, detalle):
    from kairos.retos.models import DetalleJovenReto, Competencia, CompetenciaCalificaciones
    from collections import Counter

    try:
        competencia = CompetenciaCalificaciones.objects.get(calificaciones__id_joven=id_joven_calificado, calificaciones__id_equipo=detalle.equipo.pk, calificaciones__id_reto=detalle.reto.pk)        
        return Counter(competencia.calificaciones['calificacion'].values())
    except CompetenciaCalificaciones.DoesNotExist:
        pass

@register.filter(name='obtener_valor_diccionario')
def obtener_valor_diccionario(diccionario, llave):    
    if diccionario and llave in diccionario:
        return diccionario.get(llave)
    else:
        return 0

@register.simple_tag(name='esta_calificado')
def esta_calificado(id_reto, id_participante):  # value: organizacion - contenido
    from kairos.usuarios.models import Joven
    from kairos.equipos.models import Equipo
    from kairos.retos.models import Retos, DetalleEquipoReto, DetalleJovenReto

    reto = Retos.objects.get(pk=id_reto)


    if 'Equipos' in reto.grupos_permitidos:
        equipo = Equipo.buscar(id_participante)
        return DetalleEquipoReto.objects.get(reto__pk=id_reto, equipo__pk=id_participante).calificado
    else:
        joven = Joven.objects.get(pk=id_participante)
        return DetalleJovenReto.objects.get(reto__pk=id_reto, joven__pk=id_participante).calificado

@register.simple_tag(name='tiene_calificaciones')
def tiene_calificaciones(id_reto, id_participante):  # value: organizacion - contenido
    from kairos.usuarios.models import Joven
    from kairos.equipos.models import Equipo
    from kairos.retos.models import Retos, DetalleEquipoReto, DetalleJovenReto

    reto = Retos.objects.get(pk=id_reto)

    if 'Equipos' in reto.grupos_permitidos:
        equipo = Equipo.buscar(id_participante)
        detalle = DetalleEquipoReto.objects.get(reto__pk=id_reto, equipo__pk=id_participante)
        preguntas = detalle.calificacion_pregunta_equipo_reto.all()
        actos = detalle.calificacion_acto_equipo_reto.all()
    else:
        joven = Joven.objects.get(pk=id_participante)
        detalle = DetalleJovenReto.objects.get(reto__pk=id_reto, joven__pk=id_participante)
        preguntas = detalle.calificacion_pregunta_joven_reto.all()
        actos = detalle.calificacion_acto_joven_reto.all()
    
    if preguntas.count() == 0 and actos.count() == 0:
        return False
    
    return True



@register.filter(name='reto_en_curso')
def reto_en_curso(id_reto):  # value: organizacion - contenido
    retos = Retos.objects.get(pk=id_reto)
    hora_actual = timezone.now()
    hora_inicio = retos.fecha_inicio
    if hora_actual > hora_inicio:
        return True
    else:
        return False

@register.filter(name='reto_finalizado')
def reto_finalizado(id_reto):  # value: organizacion - contenido
    retos = Retos.objects.get(pk=id_reto)
    hora_actual = timezone.now()
    hora_fin = retos.fecha_fin
    if hora_actual > hora_fin:
        return True
    else:
        return False

@register.filter(name='usuario_completo_cuestionarios')
def usuario_completo_cuestionarios(id_joven):  # value: organizacion - contenido
    from kairos.respuestas_formularios.models import RespuestasJoven
    from django.db.models import Count
    from kairos.usuarios.models import Joven
    completos = RespuestasJoven.objects.filter(joven=id_joven, completas=True).distinct('formulario').count()
    joven = Joven.objects.get(pk=id_joven)
    if joven.joven_dueno_perfil.all().count() == 0:
        return False
    else:
        return True
    


@register.simple_tag(name='opcion_seleccionada')
def opcion_seleccionada(id_reto, id_detalle, id_pregunta, id_opcion):  # value: organizacion - contenido
    from kairos.respuestas_formularios.models import RespuestasJoven
    from kairos.retos.models import DetalleEquipoReto, DetalleJovenReto, Retos, CalificacionPreguntaJoven, CalificacionPreguntaEquipo
    from django.db.models import Count


    try:
        reto = Retos.objects.get(pk=id_reto)

        if reto.grupos_permitidos == 'Jóvenes':
            detalle = DetalleJovenReto.objects.get(pk=id_detalle)
            calificacion = CalificacionPreguntaJoven.objects.get(detalle_joven_reto=detalle, pregunta__id=id_pregunta)
        else:
            detalle = DetalleEquipoReto.objects.get(pk=id_detalle)
            calificacion = CalificacionPreguntaEquipo.objects.get(detalle_equipo_reto=detalle, pregunta__id=id_pregunta)


        if calificacion.opcion is None:
            return False
        else:
            if calificacion.opcion.id == id_opcion:
                return True
            else:
                return False
    except:
            False


@register.simple_tag(name='respuesta_contestada')
def respuesta_contestada(id_reto, id_detalle, id_pregunta):  # value: organizacion - contenido
    from kairos.respuestas_formularios.models import RespuestasJoven
    from kairos.retos.models import DetalleEquipoReto, DetalleJovenReto, Retos, CalificacionPreguntaJoven, CalificacionPreguntaEquipo
    from django.db.models import Count


    reto = Retos.objects.get(pk=id_reto)
    try:
        if reto.grupos_permitidos == 'Jóvenes':
            detalle = DetalleJovenReto.objects.get(pk=id_detalle)
            calificacion = CalificacionPreguntaJoven.objects.get(detalle_joven_reto=detalle, pregunta__id=id_pregunta)
        else:
            detalle = DetalleEquipoReto.objects.get(pk=id_detalle)
            calificacion = CalificacionPreguntaEquipo.objects.get(detalle_equipo_reto=detalle, pregunta__id=id_pregunta)
        if calificacion.envio_pregunta == True:
            return True
        elif calificacion.fecha_finalizacion is None:
            if calificacion.opcion is None:
                return False
            else:
                return True
        elif (calificacion.fecha_finalizacion < timezone.now()):
            return True

    except:
            False


@register.filter(name='corregir_url')
def corregir_url(url):  # value: organizacion - contenido

    if 'https://' in url or 'http://' in url:
        return url
    else:
        try:
            url_aux = 'https://' + url
            r = requests.get(url_aux)
            if r.status_code == 200:
                return url_aux

            url_aux = 'http://' + url
            r = requests.get(url_aux)
            if r.status_code == 200:
                return url_aux

        except:
            url_aux = 'https://' + url

        return url_aux


@register.filter(name='corregir_url')
def reto_finalizado(url):  # value: organizacion - contenido

    if 'https://' in url or 'http://' in url:
        return url
    else:
        try:
            url_aux = 'https://' + url
            r = requests.get(url_aux)
            if r.status_code == 200:
                return url_aux

            url_aux = 'http://' + url
            r = requests.get(url_aux)
            if r.status_code == 200:
                return url_aux

        except:
            url_aux = 'https://' + url

        return url_aux


@register.filter
def chaside(value, key):  # value: puntajeCHASIDE, key: 'C' o 'A' etc
    res = '<b>Aptitud:</b> ' + str(value[key]['aptitud']) + \
          '<br><b>Interés:</b> ' + str(value[key]['interes']) + \
          '<br><b>Resultado:</b> ' + str(value[key]['interes'])
    return res


@register.filter
def d48(value, key):  # value: resultadoD48, key: 'resultado'... etc
    return value[key]


@register.filter
def sm(value, key):  # value: resultadoSM, key: 'resultado'... etc
    return value[key]


@register.filter
def sterrett(value, key):  # value: puntajeCHASIDE, key: 'C' o 'A' etc
    res = '<b>Puntaje:</b> ' + str(value['competencias'][key]['puntaje']) + \
          '<br><b>Resultado:</b> ' + str(value['competencias'][key]['resultado'])
    return res


@register.filter
def sterrettglobal(value):  # value: puntajeCHASIDE, key: 'C' o 'A' etc
    res = value['global']
    return res

@register.filter
def liderazgo(value, key):  # value: puntajeCHASIDE, key: 'C' o 'A' etc
    res = '<b>Promedio:</b> ' + str(value['competencias'][key]['promedio']) + \
          '<br><b>Resultado:</b> ' + str(value['competencias'][key]['resultado'])
    return res


@register.filter
def liderazgoDimensiones(value):  # value: puntajeCHASIDE, key: 'C' o 'A' etc
    res = '<b>Laissez Faire:</b> ' + str(value['dimensiones']['laizzes_faire']) + \
          '<br><b>Liderazgo Transaccional:</b> ' + str(value['dimensiones']['transaccional']) + \
          '<br><b>Liderazgo Transformacional::</b> ' + str(value['dimensiones']['transformacional'])
    return res




@register.filter
def classname(obj):
    return obj.__class__.__name__.lower()


@register.filter
def index(List, i):
    return List[int(i)]


@register.filter
def tipo_logueo(tipo_logueo):
    if tipo_logueo == 0:
        return "<span class='label label-lime'>INICIO SESIÓN EXITOSO<span>"
    elif tipo_logueo == 1:
        return "<span class='label label-lime'>CIERRE SESIÓN EXITOSO<span>"
    elif tipo_logueo == 2:
        return "<span class='label label-danger'>INICIO SESIÓN FALLIDO<span>"


@register.filter
def formulario_contestado(value, pk_joven):
    joven = Joven.objects.get(pk=pk_joven)
    try:
        contestada = RespuestasJoven.objects.values('completas').get(joven=pk_joven, formulario=value.id,
                                                                     tanda=joven.tanda_formularios)
        if contestada['completas']:
            return True
        else:
            return False
    except:
        return False

@register.filter
def cuarto_pertenece_respuestas_equipo(calificacion_equipo:list, cuarto:'Cuarto') -> bool:
    """
    Evalua si un cuarto pertece a los cuartos constestados por un Equipo.

    Args:
        calificacion_equipo (list): Lista calificaciones equipo
        cuarto (Cuarto): Cuarto a evaluar

    Returns:
        bool: True si pertenece, False si NO pertenece
    """
    
    id_cuartos = list(calificacion_equipo.values_list('acto__cuartos_actos__pk', flat=True).distinct('acto__cuartos_actos__pk'))
    if cuarto.pk in id_cuartos:
        return True
    return False

@register.filter
def cuarto_pertenece_respuestas_joven(calificacion_joven:list, cuarto:'Cuarto') -> bool:
    """
    Evalua si un cuarto pertece a los cuartos constestados por un Equipo.

    Args:
        calificacion_joven (list): Lista calificaciones joven
        cuarto (Cuarto): Cuarto a evaluar

    Returns:
        bool: True si pertenece, False si NO pertenece
    """
    
    id_cuartos = list(calificacion_joven.values_list('acto__cuartos_actos__pk', flat=True).distinct('acto__cuartos_actos__pk'))
    if cuarto.pk in id_cuartos:
        return True
    return False





# Retorna el valor de la respuesta (el value de radio) para pre-seleccionarla en el formulario
@register.simple_tag
def respuesta_correcta(*args, **kwargs):  # value => obj pregunta
    joven = Joven.objects.get(pk=int(kwargs['pk_joven']))
    try:
        respuestas = RespuestasJoven.objects.get(joven=joven.pk, formulario=int(kwargs['pk_formulario']),
                                                 tanda=joven.tanda_formularios.pk)
        return respuestas.data[str(kwargs['pk_pregunta'])]
    except:
        return None


# Retorna el valor de las respuesta superior en domino (el value de los s) para pre-seleccionarla en el formulario
@register.simple_tag
def respuesta_correcta_superior_domino(*args, **kwargs):  # value => obj pregunta
    joven = Joven.objects.get(pk=int(kwargs['pk_joven']))
    try:
        respuestas = RespuestasJoven.objects.get(joven=joven.pk, formulario=int(kwargs['pk_formulario']),
                                                 tanda=joven.tanda_formularios.pk)
        return respuestas.data[str(kwargs['pk_pregunta'])][0]
    except:
        return None


# Retorna el valor de las respuesta superior en domino (el value de los s) para pre-seleccionarla en el formulario
@register.simple_tag
def respuesta_correcta_inferior_domino(*args, **kwargs):  # value => obj pregunta
    joven = Joven.objects.get(pk=int(kwargs['pk_joven']))
    try:
        respuestas = RespuestasJoven.objects.get(joven=joven.pk, formulario=int(kwargs['pk_formulario']),
                                                 tanda=joven.tanda_formularios.pk)
        return respuestas.data[str(kwargs['pk_pregunta'])][1]
    except:
        return None

@register.filter
def verbose_name(value):
    from kairos.equipos.models import Equipo

    return Equipo._meta.get_field(value).verbose_name.title()

@register.filter
def filtrar_si_es_invitado(equipo, joven):
    return equipo.solicitudes_del_equipo.filter(estado="Invitado", joven=joven).exists()

@register.filter
def filtrar_estado_equipo_postulado(equipo, joven):
    return equipo.solicitudes_del_equipo.filter(estado="Postulado", joven=joven).exists()

@register.filter
def miembros_del_equipo(equipo):
    return equipo.miembros.all()

@register.filter
def solicitudes_del_equipo(equipo):
    return equipo.solicitudes_del_equipo.filter(estado="Postulado")

@register.filter
def miembro_del_equipo(equipo, usuario):
    if usuario.tipo == 'Joven':
        joven = Joven.objects.get(username=usuario.username)
        if joven in equipo.miembros.all():
            return True
    return False

@register.filter
def promedio_calificacion_recurso(value):
    calificaciones = CalificacionRecurso.objects.filter(recurso=value.pk)
    suma = 0
    cals = len(calificaciones)
    if not cals == 0:
        for calificacion in calificaciones:
            suma = suma + calificacion.calificacion
        return int(suma / len(calificaciones))
    else:
        return 0

@register.simple_tag
def verificar_estado(equipo, reto, miembro):
    try:
        detalle = DetalleEquipoReto.objects.get(equipo=equipo, reto=reto)
    except DetalleEquipoReto.DoesNotExist:
        pass
    if detalle:
        miembros = detalle.miembros_activos
        if miembros:
            if str(miembro.id) in miembros:
                return True
    return False

