from django.apps import AppConfig


class ExtraTagsConfig(AppConfig):
    name = 'kairos.extra_tags'
