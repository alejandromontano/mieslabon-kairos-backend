from django.apps import AppConfig


class RespuestasFormulariosConfig(AppConfig):
    name = 'kairos.respuestas_formularios'
