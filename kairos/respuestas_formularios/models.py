# Modulos Django
from django.db import models
from django.contrib.postgres.fields import JSONField

# Modulos de plugin externos
from simple_history.models import HistoricalRecords

# Modulos de otras apps

# Modulos internos de la app

class RespuestasJoven(models.Model):
    joven = models.ForeignKey('usuarios.Joven', on_delete=models.CASCADE, verbose_name="joven que respondió el formulario", related_name='respuestas_joven')
    formulario = models.ForeignKey('formularios.Formularios', on_delete=models.CASCADE, verbose_name="formulario que respondió")
    tanda = models.ForeignKey('tandas_formularios.TandasFormularios', on_delete=models.CASCADE,
                              verbose_name="tanda de formularios a la que pertenece el formulario*")
    data = JSONField(null=True)  # {pk_pregunta1: respuesta, pk_pregunta2: respuesta}
    texto_leido = models.BooleanField(default=False)
    tiempo_gastado = models.DurationField(verbose_name="tiempo que le ha tomado al joven responder el formulario",
                                          null=True)
    creado = models.DateTimeField(auto_now_add=True)
    fecha_inicio = models.DateTimeField(null=True, blank=True)
    modificado = models.DateTimeField(auto_now=True)
    completas = models.BooleanField(verbose_name="¿El joven ya respondió todas las preguntas?", default=False)
    history = HistoricalRecords()

    @staticmethod
    def cantidad_respuestas_completas(joven: 'Joven') -> 'int':
        '''
            Filtra por joven y completas y retorna la cantidad de respuestas completadas.

            Parámetros:
                joven (Joven): Objecto Jóven.

            Retorno:
                cantidad_formulario_completos (int): cantidad de respuestas completadas
        '''

        cantidad_respuestas_completadas = RespuestasJoven.objects.filter(joven=joven.id, completas=True).distinct('formulario').count()
        return cantidad_respuestas_completadas
