from django.apps import AppConfig


class JuradosConfig(AppConfig):
    name = 'jurados'
