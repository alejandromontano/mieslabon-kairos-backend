# Modulos Django
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import ListView, CreateView, UpdateView, DetailView, TemplateView

# Modulos de plugin externos


# Modulos de otras apps
from kairos.retos.models import Retos, DetalleEquipoReto, CalificacionActoEquipo, DetalleJovenReto, CalificacionActoJoven
from kairos.equipos.models import Equipo
from kairos.usuarios.models import Joven

# Modulos internos de la app
from .models import *
from .forms import *

class ListadoDeRetos(LoginRequiredMixin, ListView):
    model = Jurado
    context_object_name = "retos"
    template_name = "jurados/listado_de_retos.html"

    def get_queryset(self):
        jurado = Jurado.buscar(self.request.user.id)
        retos = jurado.retos_jurados.all()
        return retos

class ListadoDeJovenes(LoginRequiredMixin, ListView):
    model = Joven
    context_object_name = "jovenes"
    template_name = "jurados/listado_de_jovenes.html"

    def get_queryset(self):
        from django.db.models import Q
        reto = Retos.buscar(self.kwargs['id_reto'])

        jurado = Jurado.objects.get(id=self.request.user.pk)
        try:
            detalle = DetalleJovenReto.objects.filter(jurado=jurado, reto=reto)
            lista_jovenes_asignados = []
            for joven in detalle.values('joven'):
                lista_jovenes_asignados.append(joven['joven'])

            if reto.fecha_fin < timezone.now():
                jovenes = Joven.objects.filter(Q(pk__in=lista_jovenes_asignados))
            else:
                jovenes = reto.jovenes.none()
            return jovenes
        except DetalleJovenReto.DoesNotExist:
            return reto.jovenes.none()
                

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        reto = Retos.buscar(self.kwargs['id_reto'])
        
        if reto.fecha_fin < timezone.now():
            jovenes = reto.jovenes.all()
            context['reto'] = reto
        else:
            jovenes = reto.jovenes.none()
            context['mensaje'] = 'El Reto aún no ha Finalizado'
        return context

class ListadoDeEquipos(LoginRequiredMixin, ListView):
    model = Equipo
    context_object_name = "equipos"
    template_name = "jurados/listado_de_equipos.html"

    def get_queryset(self):
        from django.db.models import Q
        reto = Retos.buscar(self.kwargs['id_reto'])

        jurado = Jurado.objects.get(id=self.request.user.pk)
        try:
            detalle = DetalleEquipoReto.objects.filter(jurado=jurado, reto=reto)
            lista_equipos_asignados = []
            for equipo in detalle.values('equipo'):
                lista_equipos_asignados.append(equipo['equipo'])

            if reto.fecha_fin < timezone.now():
                equipos = Equipo.objects.filter(Q(pk__in=lista_equipos_asignados))
            else:
                equipos = reto.equipos.none()
            return equipos
        except DetalleEquipoReto.DoesNotExist:
            return reto.equipos.none()
                

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        reto = Retos.buscar(self.kwargs['id_reto'])
        
        if reto.fecha_fin < timezone.now():
            equipos = reto.equipos.all()
            context['reto'] = reto
        else:
            equipos = reto.equipos.none()
            context['mensaje'] = 'El Reto aún no ha Finalizado'
        return context

class CalificarEquipo(LoginRequiredMixin, CreateView):
    template_name = "jurados/calificar_equipo.html"
    model = RetroalimentacionJuradoEquipo
    form_class = RetroalimentacionJuradoEquipoFrom
    
    def form_valid(self, form):
        from kairos.core.utils import crear_contrasenia
        retroalimentacion = form.save(commit=False)
        reto = Retos.buscar(self.kwargs['id_reto'])
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        detalle_equipo = DetalleEquipoReto.objects.get(reto=reto, equipo=equipo)

        retroalimentacion.detalle = detalle_equipo
        retroalimentacion.save()
        form.save()
       
        messages.success(self.request, "Retroalimentación Guardada")
        return redirect('jurados:listado_equipos', id_reto=reto.pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        reto = Retos.buscar(self.kwargs['id_reto'])
        equipo = Equipo.buscar(self.kwargs['id_equipo'])
        detalle_equipo = DetalleEquipoReto.objects.get(reto=reto, equipo=equipo)
        calificacion_equipo = CalificacionActoEquipo.objects.filter(detalle_equipo_reto=detalle_equipo)
        
        context['id_participante'] = equipo.pk
        context['calificacion_equipo'] = calificacion_equipo
        context['detalle_equipo'] = detalle_equipo
        context['reto'] = reto
        context['equipo'] = equipo
        return context

class CalificarJoven(LoginRequiredMixin, CreateView):
    template_name = "jurados/calificar_joven.html"
    model = RetroalimentacionJuradoJoven
    form_class = RetroalimentacionJuradoJovenFrom

    def form_valid(self, form):
        from kairos.core.utils import crear_contrasenia
        retroalimentacion = form.save(commit=False)
        reto = Retos.buscar(self.kwargs['id_reto'])
        joven = Joven.buscar(self.kwargs['id_joven'])
        detalle = DetalleJovenReto.objects.get(reto=reto, joven=joven)

        retroalimentacion.detalle = detalle
        retroalimentacion.save()
        form.save()
       
        messages.success(self.request, "Retroalimentación Guardada")
        return redirect('jurados:listado_jovenes', id_reto=reto.pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        reto = Retos.buscar(self.kwargs['id_reto'])
        joven = Joven.buscar(self.kwargs['id_joven'])
        detalle = DetalleJovenReto.objects.get(reto=reto, joven=joven)
        calificaciones = CalificacionActoJoven.objects.filter(detalle_joven_reto=detalle)
        context['id_participante'] = joven.pk
        context['calificaciones'] = calificaciones
        context['detalle'] = detalle
        context['reto'] = reto
        context['joven'] = joven
        return context