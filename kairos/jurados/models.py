from django.db import models
from kairos.usuarios.models import Usuario


# Create your models here.
class Jurado(Usuario):

    def agregar_reto(self, reto):
        self.retos_jurados.add(reto)

    @staticmethod
    def buscar(id_jurado):
        try:
            jurado = Jurado.objects.get(id=id_jurado)
            return jurado
        except Jurado.DoesNotExist:
            return None


class RetroalimentacionJuradoEquipo(models.Model):
    detalle = models.ForeignKey('retos.DetalleEquipoReto', related_name='retroalimentacion_detalle_equipo', on_delete=models.PROTECT, null=True)
    retroalimentacion = models.CharField(max_length=100)
    
class RetroalimentacionJuradoJoven(models.Model):
    detalle = models.ForeignKey('retos.DetalleJovenReto', related_name='retroalimentacion_detalle_joven', on_delete=models.PROTECT, null=True)
    retroalimentacion = models.CharField(max_length=100)
