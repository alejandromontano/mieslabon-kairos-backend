# Modulos Django
from django import forms


# Modulos de plugin externos


# Modulos de otras apps


# Modulos internos de la app
from .models import RetroalimentacionJuradoEquipo, RetroalimentacionJuradoJoven

class RetroalimentacionJuradoEquipoFrom(forms.ModelForm):

    def __init__ ( self, *args, **kwargs ):
        super(RetroalimentacionJuradoEquipoFrom, self).__init__( *args, **kwargs )
        self.fields['retroalimentacion'].label = False

    class Meta:
        model = RetroalimentacionJuradoEquipo
        fields = ['retroalimentacion']
        widgets = {
            'retroalimentacion': forms.Textarea(
                attrs={
                    'rows': 5,
                    'cols': 100,
                    'style': 'resize:none;',
                    'placeholder':'Comentarios del Jurado'
                }
            ),
        }

class RetroalimentacionJuradoJovenFrom(forms.ModelForm):

    def __init__ ( self, *args, **kwargs ):
        super(RetroalimentacionJuradoJovenFrom, self).__init__( *args, **kwargs )
        self.fields['retroalimentacion'].label = False

    class Meta:
        model = RetroalimentacionJuradoJoven
        fields = ['retroalimentacion']
        widgets = {
            'retroalimentacion': forms.Textarea(
                attrs={
                    'rows': 5,
                    'cols': 100,
                    'style': 'resize:none;',
                    'placeholder':'Comentarios del Jurado'
                }
            ),
        }

