
# Modulos Django
from django.urls import path

# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from . import views

app_name = "jurados"

urlpatterns = [
    path('listado-retos', views.ListadoDeRetos.as_view(), name='listado_retos'),
    path('listado-equipos/<int:id_reto>', views.ListadoDeEquipos.as_view(), name='listado_equipos'),
    path('listado-joven/<int:id_reto>', views.ListadoDeJovenes.as_view(), name='listado_jovenes'),
    path('calificar-equipo/<int:id_reto>/<int:id_equipo>', views.CalificarEquipo.as_view(), name='calificar_equipo'),    
    path('calificar-joven/<int:id_reto>/<int:id_joven>', views.CalificarJoven.as_view(), name='calificar_joven'),

    #path('retroalimentacion-joven/<int:id_reto>/<int:id_detalle>', views.CalificarEquipo.as_view(), name='retroalimentacion_joven'),
]