# Modulos Django
from django import forms
from django.db import models

from django.db.models import Case, When

# Modulos de plugin externos
from cities_light.models import Country, City, Region
from django_select2.forms import Select2MultipleWidget, Select2Widget, ModelSelect2Widget

# Modulos de otras apps
from kairos.insignias.models import NivelesDeConocimiento, Tribus, RutaConocimiento, Familia
from kairos.usuarios.models import SalarioPais

# Modulos internos de la app
from .models import PerfilesDeseados

class JovenPerfilDeseadoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(JovenPerfilDeseadoForm, self).__init__(*args, **kwargs)
        niveles_de_conocimiento = NivelesDeConocimiento.objects.order_by('nombre')
        self.fields['tribu_deseada'].queryset = Tribus.objects.filter(activo=True)
        self.fields['nivel_conocimiento_deseado'].queryset = niveles_de_conocimiento
        self.fields['interes_pais'].initial = Country.objects.get(id=253)
        self.fields['interes_ciudad'].initial = City.objects.get(id=1112000)

    DESEOS_APRENDIZAJE = (
        ("1", "Me gustaría estudiar en un centro educativo una carrera formal (colegio - Universidad o instituto)"),
        ("2", "Me gustaría estudiar más por cursos cortos prácticos o certificaciones rápidas, de modo presencial"),
        ("3", "Me gusta el autoaprendizaje, soy bueno con cursos en línea y mejor si son gratuitos"),
        ("4", "Me gusta aprender más haciendo, busco voluntariados o trabajos donde pueda aprender a hacer cosas "
              "que me gusten"),
    )

    deseos_aprendizaje = forms.MultipleChoiceField(
        choices=DESEOS_APRENDIZAJE,
        widget=Select2MultipleWidget(),
        required=True,
        label="¿Cómo te gustaría aprender?*"
    )

    pais = forms.ModelChoiceField(
        required=False,
        queryset=Country.objects.all(),
        label="Elije el país para consultar el salario mínimo:",
        widget=ModelSelect2Widget(
            model=Country,
            search_fields=['name__icontains', 'slug__icontains', 'id__icontains','continent__icontains'],
            max_results=100,
            queryset=Country.objects.all(),
        )
    )
    class Meta:
        model = PerfilesDeseados
        fields = (
        'tribu_deseada', 'familia', 'rutas_conocimiento', 'nivel_conocimiento_deseado', 'interes_voluntariado', 'interes_trabajo',
        'interes_independiente', 'interes_emprendedor', 'interes_pais', 'interes_ciudad',
        'necesidad_ingresos', 'deseos_aprendizaje', 'capacidad_inversion_formacion', 'nombre_perfil', 'favorito', 'foto_perfil')
        widgets = {
            'interes_pais': ModelSelect2Widget(
                model=Country,
                search_fields=['name__icontains', 'slug__icontains', 'id__icontains', 'continent__icontains'],
                max_results=100,
            ),
            'interes_ciudad': ModelSelect2Widget(
                model=City,
                search_fields=['name__icontains'],
                dependent_fields={'interes_pais': 'country_id'},
                max_results=100,
            ),
            'familia': ModelSelect2Widget(
                model=Familia,
                search_fields=['nombre__icontains'],
                dependent_fields={'tribu_deseada': 'tribu'},
                max_results=100,
            ),
            'rutas_conocimiento': ModelSelect2Widget(
                model=RutaConocimiento,
                search_fields=['nombre__icontains'],
                dependent_fields={'familia': 'familia_id'},
                max_results=100,
            ),

            'nivel_conocimiento_deseado': Select2Widget(),
            'necesidad_ingresos': Select2Widget(),
            'capacidad_inversion_formacion': Select2Widget(),
            'interes_voluntariado': Select2Widget(),
            'interes_trabajo': Select2Widget(),
            'interes_independiente': Select2Widget(),
            'interes_emprendedor': Select2Widget(),
            'favorito': Select2Widget(),
            'pais': Select2Widget(),
        }
        help_texts = {
            'tribu_deseada': 'Las insignias representan las diferentes áreas del conocimiento. ',
            'deseos_aprendizaje': 'A continuación encontrarás diferentes alternativas de aprendizaje, '
                                  'elige las dos que más te gustan y se ajustan a ti',

            'nombre_perfil': 'Acabas de crear un perfil con el que te sueñas, ¿cómo lo nombrarías?, uno que recuerdes fácil y lo '
                             'puedas asociar con los intereses que seleccionaste en este espacio.',
            'favorito': 'Si eliges este perfil como favorito encontrarás en tu perfil diferentes rutas de conocimiento,'
                        ' retos y oportunidades que están asociados a los intereses que seleccionaste en este perfil',
            'rutas_conocimiento': 'A continuación encontrarás una lista con diferentes tipos de conocimiento o '
                                  'campos de estudio, elige la que te gusta más. Si no te gusta ninguna, déjalo en blanco.'
        }
