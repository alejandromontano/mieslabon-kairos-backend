# Modulos Django
from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MaxValueValidator, MinValueValidator

# Modulos de plugin externos
from cities_light.models import Country, City
from simple_history.models import HistoricalRecords

# Modulos de otras apps
from kairos.insignias.models import Tribus, NivelesDeConocimiento

# Modulos internos de la app

DESEOS_APRENDIZAJE = (
    ("1", "Me gustaría estudiar en un centro educativo una carrera formal (colegio - Universidad o instituto)"),
    ("2", "Me gustaría estudiar más por cursos cortos prácticos o certificaciones rápidas, de modo presencial"),
    ("3", "Me gusta el autoaprendizaje, soy bueno con cursos en línea y mejor si son gratuitos"),
    ("4", "Me gusta aprender más haciendo, busco voluntariados o trabajos donde pueda aprender a hacer cosas "
          "que me gusten"),
)


def ruta_archivos_foto_perfil(instance, filename):
    return "perfiles_deseados/{}".format(
        filename.encode('ascii', 'ignore'),
    )


class PerfilesDeseados(models.Model):
    SI_NO = (
        (True, "Sí"),
        (False, "No")
    )
    joven = models.ForeignKey('usuarios.Joven', related_name="joven_dueno_perfil", on_delete=models.CASCADE)
    NECESIDAD_INGRESOS = (
        ("1", "No tengo necesidad de ingresos, mi familia tiene recursos para mi mantenimiento"),
        ("2", "No tengo necesidad de ingresos, pero quiero en los próximos años ver oportunidades para mí, "
              "pues no deseo estudiar por ahora"),
        ("3", "No tengo necesidad pero me gustaria trabajar y estudiar"),
        ("4", "Quiero trabajar y no quiero estudiar por ahora"),
        ("5", "Necesito trabajar para mantenerme y quiero estudiar"),
        ("6", "Necesito trabajar pues además de mi, tengo obligaciones a cargo")
    )
    CAPACIDAD_INVERSION = (
        ("1", "Menos de 1 Salario mínimo legal vigente en tu país"),
        ("2", "Entre 1 y 3 Salario mínimo legal vigente en tu país"),
        ("3", "Entre 3 y 5 Salario mínimo legal vigente en tu país"),
        ("4", "Entre 5 y 9 Salario mínimo legal vigente en tu país"),
        ("5", "Más de 9 Salario mínimo legal vigente en tu país")
    )
    tribu_deseada = models.ForeignKey('insignias.Tribus', on_delete=models.CASCADE,
                                      verbose_name='A continuación encontrarás las diferentes insignias y deberás '
                                                   'elegir la que más te guste*')
    nivel_conocimiento_deseado = models.ForeignKey('insignias.NivelesDeConocimiento', on_delete=models.CASCADE,
                                                   verbose_name='¿Qué nivel de cualificación deseas alcanzar en la tribu en los próximos tres años?*')
    familia = models.ForeignKey('insignias.Familia', on_delete=models.CASCADE,
                                verbose_name='Familia de la tribu', null=True, blank=True)
    rutas_conocimiento = models.ForeignKey('insignias.RutaConocimiento', on_delete=models.CASCADE,
                                           verbose_name='Campo de la tribu', null=True, blank=True)
    interes_voluntariado = models.BooleanField(verbose_name='¿Te gustaría ser voluntario en tu tribu?', choices=SI_NO,
                                               default=False)
    interes_trabajo = models.BooleanField(
        verbose_name='¿Te gustaría adquirir experiencia como colaborador en tu tribu?', choices=SI_NO, default=False)
    interes_independiente = models.BooleanField(verbose_name='¿Quisieras conocer cuáles son los servicios que '
                                                             'ofrecen las personas que se desempeñan como '
                                                             'independientes en tu tribu?', choices=SI_NO,
                                                default=False)
    interes_emprendedor = models.BooleanField(verbose_name='¿Te gustaría aprender sobre emprendimiento y saber '
                                                           'cómo puedes hacerlo en tu tribu?', choices=SI_NO,
                                              default=False)
    interes_pais = models.ForeignKey(Country, on_delete=models.PROTECT, related_name='interes_pais_del_joven',
                                     verbose_name="¿Te gustaría adquirir experiencia en otro país? ¿En cuál?",
                                     null=False, blank=False)
    interes_ciudad = models.ForeignKey(City, on_delete=models.PROTECT, related_name='interes_ciudad_del_joven',
                                       verbose_name="¿En qué ciudad te gustaría adquirir tu experiencia?",
                                       null=False, blank=False)
    necesidad_ingresos = models.CharField(max_length=10, choices=NECESIDAD_INGRESOS,
                                          verbose_name='Encontremos juntos las mejores opciones ¿Cuáles son '
                                                       'tus necesidades económicas actuales?*')
    deseo_aprendizaje = ArrayField(models.CharField(max_length=50, blank=True, null=True), size=2,
                                   choices=DESEOS_APRENDIZAJE,
                                   verbose_name='¿Cómo te gustaría aprender?*')
    capacidad_inversion_formacion = models.CharField(max_length=10, choices=CAPACIDAD_INVERSION,
                                                     verbose_name='¿Cuánto podrías invertir en tu preparación académica'
                                                                  ' semestralmente?*')
    nombre_perfil = models.CharField(max_length=100, verbose_name='nombre del perfil*')
    favorito = models.BooleanField(verbose_name='¿Deseas elegir este escenario deseado como tu favorito?',
                                   choices=SI_NO, default=False)

    decision = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                           verbose_name='calificación: 1 tomar, 0 sin tomar')
    creado = models.DateTimeField(auto_now_add=True)
    foto_perfil = models.FileField(upload_to=ruta_archivos_foto_perfil, null=True, blank=True)
    history = HistoricalRecords()

    @staticmethod
    def get_queryset(query=None, perfiles=None):
        queries = query.split(' ')
        for q in queries:
            tribus = Tribus.objects.filter(nombre__icontains=q)
            niveles_conocimiento = NivelesDeConocimiento.objects.filter(nombre__icontains=q)
            perfiles = perfiles.filter(Q(nombre_perfil__icontains=q) | Q(capacidad_inversion_formacion__icontains=q)
                                       | Q(deseo_aprendizaje__icontains=q) | Q(necesidad_ingresos__icontains=q)
                                       | Q(tribu_deseada__in=tribus) | Q(rutas_conocimiento__nombre__icontains=q)
                                       | Q(nivel_conocimiento_deseado__in=niveles_conocimiento)
                                       | Q(interes_pais__name__icontains=q) | Q(
                interes_ciudad__name__icontains=q)).distinct()
        return perfiles

    @staticmethod
    def existe_perfil_deseado_joven(joven: 'Joven') -> 'bool':
        '''
            Filtra por joven y retorna True si existe perfil deseasdo sino False.

            Parámetros:
                joven (Joven): Usuario joven que se utilizará para buscar existensia de perfil deseado.
            Retorno:
                Si existe: True
                Si no existe: False
        '''
        existe_perfil_deseado = PerfilesDeseados.objects.filter(joven=joven).exists()
        return existe_perfil_deseado

    @staticmethod
    def cruce_capacidad_inversion_nencesidad_ingresos():

        from django.db.models import Q, Count

        lista_capacidad_inversion = []
        for necesidad_inversion in PerfilesDeseados.objects.exclude(
                Q(capacidad_inversion_formacion__isnull=True)).values('capacidad_inversion_formacion').distinct():
            lista_capacidad_inversion.append(necesidad_inversion)

        lista_necesidad_ingresos = []
        for perfil_necesidad in PerfilesDeseados.objects.exclude(Q(necesidad_ingresos__isnull=True)).values(
                'necesidad_ingresos').distinct():
            lista_necesidad_ingresos.append(perfil_necesidad)

        # [{'category': 'P', 'si': 1, 'no': 1},
        #  {'category': 'T', 'si': 1, 'no': 1}]
        CAPACIDAD_INVERSION = (
            ("1", "Menos de 1 Salario mínimo legal vigente en tu país"),
            ("2", "Entre 1 y 3 Salario mínimo legal vigente en tu país"),
            ("3", "Entre 3 y 5 Salario mínimo legal vigente en tu país"),
            ("4", "Entre 5 y 9 Salario mínimo legal vigente en tu país"),
            ("5", "Más de 9 Salario mínimo legal vigente en tu país")
        )
        NECESIDAD_INGRESOS = (
            ("1", "No tengo necesidad de ingresos, mi familia tiene recursos para mi mantenimiento"),
            ("2", "No tengo necesidad de ingresos, pero quiero en los próximos años ver oportunidades para mí, "
                  "pues no deseo estudiar por ahora"),
            ("3", "No tengo necesidad pero me gustaria trabajar y estudiar"),
            ("4", "Quiero trabajar y no quiero estudiar por ahora"),
            ("5", "Necesito trabajar para mantenerme y quiero estudiar"),
            ("6", "Necesito trabajar pues además de mi, tengo obligaciones a cargo")
        )

    def deseos_aprendizaje(self):
        deseos = []
        DESEOS = dict(DESEOS_APRENDIZAJE)
        for deseo in self.deseo_aprendizaje:
            deseos.append(DESEOS[deseo])
        return deseos

    @staticmethod
    def buscar_perfil(joven: 'Joven'):
        '''Busca perfiles deseados asociados al joven y retorna un Queryset de PerfilesDeseados'''

        perfiles = PerfilesDeseados.objects.filter(joven=joven)
        return perfiles

    @staticmethod
    def obtener_perfil_decisiones(joven: 'Joven', decision: int) -> 'Queryset<Contenido>':
        '''
            Filtra por joven, decision y retorna Queryset de PerfilesDeseado.

            Parámetros:
                joven (Joven): Usuario joven que se utilizará para buscar existensia de perfil deseado.
                decision (int): Entero entre el valor (0 y 1) 1 si ha tomado la decision, 0 de otro modo.
            Retorno:
                QuerySet<Contenido>
        '''
        perfiles = PerfilesDeseados.objects.filter(joven=joven, decision=decision)
        return perfiles.distinct()


@receiver(post_save, sender=PerfilesDeseados, dispatch_uid="minificar_imagen_usuario")
def comprimir_imagen_usuario(sender, **kwargs):
    from kairos.core.utils import comprimir_imagen
    if kwargs["instance"].foto_perfil:
        comprimir_imagen(kwargs["instance"].foto_perfil.path)
