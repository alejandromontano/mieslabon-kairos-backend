# Modulos Django
from django.urls import path
# Modulos de plugin externos

# Modulos de otras apps

# Modulos internos de la app
from .views import *

app_name = "perfiles_deseados"

urlpatterns = [
    path('consultar/', PerfilesJovenesConsultar.as_view(), name='consultar_perfiles_jovenes'),
    path('consultar/<int:pk>', PerfilesDeseadosConsultar.as_view(),
         name='consultar_perfiles_deseados_jovenes'),
    path('crear', CrearPerfilDeseado.as_view(), name='crear_perfil_deseado'),
    path('mis-perfiles', MisPerfilesDeseadosConsultar.as_view(), name='mis_perfiles_deseados'),
    path('detalle/<int:pk>', PerfilDeseadoDetalle.as_view(), name='mi_perfil_deseado_detalle'),
    path('modificar/<int:pk>', ModificarPerfilDeseado.as_view(), name='mi_perfil_deseado_modificar'),
    path("api/favorito", view=ApiFavoritoPerfilDeseado.as_view(), name="api_favorito_perfil_deseado"),
    path("api/decision", view=ApiDecisionPerfilDeseado.as_view(), name="api_decision_perfil_deseado"),
    path('consultar-salario/', view=consultar_salario, name='consultar_salario'),
]