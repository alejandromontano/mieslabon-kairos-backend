# Modulos Django
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView, CreateView, UpdateView, ListView, DetailView
from django.shortcuts import render, redirect

# Modulos de plugin externos
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse
import json

# Modulos de otras apps
from kairos.core.mixins import MensajeMixin
from kairos.usuarios.models import Joven, SalarioPais

# Modulos internos de la app
from .forms import *

class PerfilesJovenesConsultar(LoginRequiredMixin, ListView):
    model = Joven
    template_name = "usuarios/jovenes/perfiles_listado.html"
    context_object_name = "jovenes"

    def get_queryset(self):
        jovenes = Joven.objects.all()
        return jovenes


class PerfilesDeseadosConsultar(LoginRequiredMixin, ListView):
    model = PerfilesDeseados
    template_name = "usuarios/jovenes/perfil_deseado_listado.html"
    context_object_name = "perfiles"

    def get_context_data(self, *args, **kwargs):
        context = super(PerfilesDeseadosConsultar, self).get_context_data(**kwargs)
        context['joven'] = Joven.objects.get(pk=int(self.kwargs['pk']))
        return context

    def get_queryset(self):
        perfiles = PerfilesDeseados.objects.filter(joven_id=int(self.kwargs['pk']))
        return perfiles


class CrearPerfilDeseado(LoginRequiredMixin, MensajeMixin, CreateView):
    model = PerfilesDeseados
    form_class = JovenPerfilDeseadoForm
    template_name = 'usuarios/jovenes/perfiles/registrar.html'
    success_url = reverse_lazy('perfiles_deseados:crear_perfil_deseado')
    mensaje_exito = 'Perfil deseado guardado correctamente!'
    mensaje_error = 'Ha ocurrido un error al guardar el perfil deseado, por favor intente de nuevo!'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect('inicio')

        if not request.user.tipo == "Joven":
            messages.error(request, "Usted no está autorizado para crear un perfil!")
            return redirect("inicio")
        return super(CrearPerfilDeseado, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        import os

        perfil = form.save(commit=False)

        imagen_anterior = perfil.foto_perfil
        if not self.request.POST.get('imagen-clear') is None and imagen_anterior:
            if os.path.isfile(imagen_anterior.path):
                os.remove(imagen_anterior.path)
            perfil.foto_perfil = ""

        joven = Joven.objects.get(pk=self.request.user.pk)
        form.instance.joven = joven
        form.instance.deseo_aprendizaje = form.cleaned_data['deseos_aprendizaje']
        if form.cleaned_data['favorito'] == True:
            mis_perfiles = PerfilesDeseados.objects.filter(joven=joven, favorito=True)
            for mi_perfil in mis_perfiles:
                mi_perfil.favorito = False
                mi_perfil.save()

        return super(CrearPerfilDeseado, self).form_valid(form)

    def form_invalid(self, form):        
        return super(CrearPerfilDeseado, self).form_invalid(form)


class ModificarPerfilDeseado(LoginRequiredMixin, MensajeMixin, UpdateView):
    model = PerfilesDeseados
    form_class = JovenPerfilDeseadoForm
    template_name = 'usuarios/jovenes/perfiles/modificar.html'
    mensaje_exito = 'Perfil deseado modificado correctamente!'
    mensaje_error = 'Ha ocurrido un error al modificar el perfil deseado, por favor intente de nuevo!'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, "Usted no está autorizado para realizar esta acción")
            return redirect('inicio')

        if not request.user.tipo == "Joven":
            messages.error(request, "Usted no está autorizado para modificar el perfil!")
            return redirect("inicio")
        return super(ModificarPerfilDeseado, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        import os

        perfil = form.save(commit=False)

        imagen_anterior = perfil.foto_perfil
        if not self.request.POST.get('imagen-clear') is None and imagen_anterior:
            if os.path.isfile(imagen_anterior.path):
                os.remove(imagen_anterior.path)
            perfil.foto_perfil = ""

        joven = Joven.objects.get(pk=self.request.user.pk)
        form.instance.deseo_aprendizaje = form.cleaned_data['deseos_aprendizaje']
        if form.cleaned_data['favorito'] == True:
            mis_perfiles = PerfilesDeseados.objects.filter(joven=joven, favorito=True)
            for mi_perfil in mis_perfiles:
                mi_perfil.favorito = False
                mi_perfil.save()
        return super(ModificarPerfilDeseado, self).form_valid(form)

    def get_context_data(self, **kwargs):
        data = super(ModificarPerfilDeseado, self).get_context_data(**kwargs)
        data['perfil'] = PerfilesDeseados.objects.get(pk=self.object.pk)
        return data

    def get_initial(self):
        initial = super(ModificarPerfilDeseado, self).get_initial()
        initial['deseos_aprendizaje'] = PerfilesDeseados.objects.get(pk=self.object.pk).deseo_aprendizaje
        return initial

    def get_success_url(self):
        return reverse_lazy("perfiles_deseados:mi_perfil_deseado_detalle", kwargs={'pk': self.object.pk})

class MisPerfilesDeseadosConsultar(LoginRequiredMixin, ListView):
    template_name = "usuarios/jovenes/perfiles/listado.html"
    paginate_by = 12

    def get_queryset(self):
        return PerfilesDeseados.objects.filter(joven_id=self.request.user.pk)

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator
        context = super(MisPerfilesDeseadosConsultar, self).get_context_data(**kwargs)

        usuario_actual = self.obtener_usuario()

        pagination = Paginator(list(PerfilesDeseados.objects.filter(joven_id=self.request.user.pk)), self.paginate_by)

        context['perfiles'] = pagination.page(context['page_obj'].number)
        context['joven'] = usuario_actual
        return context

    def get_success_url(self):
        if self.request.user.es_superusuario:
            return reverse_lazy('contenido:listado_oportunidades')
        else:
            return reverse_lazy('contenido:listado_mis_oportunidades_creadas')

    def obtener_usuario(self):
        usuario = Joven.objects.get(id=self.request.user.id)
        return usuario

class PerfilDeseadoDetalle(DetailView):
    model = PerfilesDeseados
    template_name = "usuarios/jovenes/perfiles/detalle.html"
    context_object_name = "perfil"


class ApiFavoritoPerfilDeseado(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_favorito = data["pk_favorito"]
        perfil_favorito = PerfilesDeseados.objects.get(pk=int(pk_favorito))
        if perfil_favorito.favorito:
            perfil_favorito.favorito = False
            perfil_favorito.save()
        else:
            mis_perfiles = PerfilesDeseados.objects.filter(joven=perfil_favorito.joven, favorito=True)
            for mi_perfil in mis_perfiles:
                mi_perfil.favorito = False
                mi_perfil.save()
            perfil_favorito.favorito = True
            perfil_favorito.save()
        return Response({})

class ApiDecisionPerfilDeseado(APIView):
    def post(self, request):
        json_text = request.data.get('json_text')
        data = json.loads(json_text)
        pk_favorito = data["pk_modelo"]
        perfil_favorito = PerfilesDeseados.objects.get(pk=int(pk_favorito))

        if perfil_favorito.decision == 1:
            perfil_favorito.decision = 0
            perfil_favorito.save()
        else:
            mis_perfiles = PerfilesDeseados.objects.filter(joven=perfil_favorito.joven, decision=1)
            if mis_perfiles.count() == 3:
                return Response({"mensaje": "limite"})
            perfil_favorito.decision = 1
            perfil_favorito.save()
        return Response({})


def consultar_salario(request):
    if request.is_ajax():
        id_pais = request.POST.get('pais_id')
        if SalarioPais.objects.filter(pais_id=id_pais):
            salario = SalarioPais.objects.get(pais_id=id_pais)
            response = {
                'salario': str(salario),
            }
            return JsonResponse(response)

        else:
            response = {
                'salario': "No existe salario para este país, comuníquese  con el Administrador.",
            }
            return JsonResponse(response)





