from django.apps import AppConfig


class PerfilesDeseadosConfig(AppConfig):
    name = 'kairos.perfiles_deseados'
