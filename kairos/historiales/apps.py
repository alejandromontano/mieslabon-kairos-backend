from django.apps import AppConfig


class HistorialesConfig(AppConfig):
    name = 'kairos.historiales'
