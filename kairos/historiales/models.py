# Modulos Django
from django.db import models

# Modulos de plugin externos
from simple_history.models import HistoricalRecords

# Modulos de otras apps
from kairos.usuarios.models import Joven
from kairos.tandas_formularios.models import TandasFormularios
# Modulos internos de la app

class RegistroTandasJoven(models.Model):  # Registrar después de asignarla al joven
    tanda = models.ForeignKey('tandas_formularios.TandasFormularios', on_delete=models.CASCADE, verbose_name="tanda asignada al joven*")
    joven = models.ForeignKey('usuarios.Joven', on_delete=models.CASCADE, verbose_name="joven*")
    fecha_asignacion = models.DateTimeField(auto_now_add=True)
    fecha_terminacion = models.DateTimeField(verbose_name="fecha en que terminó de responder todos los formularios",
                                             null=True)
    tiempo_gastado = models.DurationField(verbose_name="tiempo que se gastó el joven en completar todos los "
                                                       "formularios de la tanda", null=True)
    disponible = models.BooleanField(verbose_name="¿La tanda de formularios se encuentra disponible para el joven?",
                                     default=True)
    history = HistoricalRecords()

    @staticmethod
    def registro_tandas_filtro_joven(joven: Joven) -> 'Queryset<RegistroTandasJoven>':
        '''
            Filtra por joven y retorna los registros de tandas que pertenezcan al Joven.

            Parámetros:
                joven (Joven): Joven que se usará para filtrar y retornar los registros de tanda
            Retorno:
                Queryset de RegistroTandasJoven
        '''
        registro_tandas = RegistroTandasJoven.objects.filter(joven=joven)
        return registro_tandas

    @staticmethod
    def crear_registro_tandas_joven(tanda:TandasFormularios, joven: Joven) -> 'RegistroTandasJoven':
        '''
            Crea objeto RegistroTandasJoven con tanda y joven y retorna objeto creado.

            Parámetros:
                joven (Joven): Joven que se usará crear objeto RegistroTandasJoven
            Retorno:
                TandasFormularios
        '''
        registro_tandas = RegistroTandasJoven.objects.create(tanda=tanda, joven=joven)
        return registro_tandas
